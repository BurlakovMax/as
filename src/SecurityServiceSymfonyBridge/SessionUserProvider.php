<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use App\Security\Application\AuthorizationService;
use App\Security\Application\SessionData;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class SessionUserProvider implements UserProviderInterface
{
    private AuthorizationService $authorizationService;

    /** @var SessionUser[] */
    private array $cache;

    public function __construct(AuthorizationService $authorizationService)
    {
        $this->authorizationService = $authorizationService;
    }

    /**
     * @inheritdoc
     */
    public function loadUserByUsername($username, SessionData $data = null)
    {
        if (isset($this->cache[$username])) {
            return $this->cache[$username];
        }

        if ($data === null) {
            $data = $this->authorizationService->authorize($username);

            if ($data === null) {
                throw new UsernameNotFoundException();
            }
        }

        return $this->cache[$username] = new SessionUser($data);
    }

    /**
     * @inheritdoc
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    /**
     * @inheritdoc
     */
    public function supportsClass($class): bool
    {
        return $class === SessionUser::class;
    }
}
