<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use Symfony\Component\HttpFoundation\Request;

final class HeaderTokenExtractor implements TokenExtractor
{
    /** @var string */
    private $prefix;

    /** @var string */
    private $name;

    public function __construct(string $prefix = 'Bearer', string $name = 'Authorization')
    {
        $this->prefix = $prefix;
        $this->name = $name;
    }

    public function has(Request $request): bool
    {
        return $this->extract($request) !== null;
    }

    public function get(Request $request): string
    {
        return $this->extract($request);
    }

    private function extract(Request $request): ?string
    {
        if (!$request->headers->has($this->name)) {
            return null;
        }

        $value = trim($request->headers->get($this->name));

        if ($this->prefix === '') {
            return $value;
        }

        if (strpos($value, ' ') !== false) {
            [$valuePrefix, $valueToken] = explode(' ', $value, 2);
            if (strcasecmp($valuePrefix, $this->prefix) === 0) {
                return $valueToken;
            }
        }

        return null;
    }
}
