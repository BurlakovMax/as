<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use Symfony\Component\HttpFoundation\Request;

interface TokenExtractor
{
    /**
     * Whether request has token
     */
    public function has(Request $request): bool;

    /**
     * Get token string
     */
    public function get(Request $request): string;
}
