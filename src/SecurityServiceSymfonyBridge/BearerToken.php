<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\Token\GuardTokenInterface;

final class BearerToken extends AbstractToken implements GuardTokenInterface
{
    /** @var string */
    private $token;

    /** @var \DateTimeImmutable */
    private $expiresAt;

    public function __construct(string $token, UserInterface $user, \DateTimeImmutable $expiresAt)
    {
        parent::__construct($user->getRoles());
        $this->setUser($user);
        $this->token = $token;
        $this->expiresAt = $expiresAt;
    }

    public function getCredentials()
    {
        return $this->token;
    }

    public function isAuthenticated(): bool
    {
        return $this->expiresAt > new \DateTimeImmutable();
    }
}
