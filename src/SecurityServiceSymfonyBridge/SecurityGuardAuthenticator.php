<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use App\Security\Application\AuthorizationService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

final class SecurityGuardAuthenticator extends AbstractGuardAuthenticator
{
    /** @var TokenExtractor */
    private $tokenExtractor;

    /** @var AuthorizationService */
    private $authorizationService;

    /** @var AuthenticationResponseBuilder */
    private $responseBuilder;

    public function __construct(
        TokenExtractor $tokenExtractor,
        AuthorizationService $authorizationService,
        AuthenticationResponseBuilder $noAuthResponseBuilder
    )
    {
        $this->tokenExtractor = $tokenExtractor;
        $this->authorizationService = $authorizationService;
        $this->responseBuilder = $noAuthResponseBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request): bool
    {
        return $this->getTokenExtractor()->has($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        return $this->getTokenExtractor()->get($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        if (is_string($credentials)) {
            return $userProvider->loadUserByUsername($credentials);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $authException): ?Response
    {
        return $this->responseBuilder->authenticationFailure($authException);
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return $this->responseBuilder->unauthorized($authException);
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        if (is_string($credentials)) {
            return $this->authorizationService->authorize($credentials) !== null;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \RuntimeException If there is no pre-authenticated token previously stored
     */
    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        if (!$user instanceof SessionUser) {
            throw new \InvalidArgumentException(
                sprintf('The first argument of the "%s()" method must be an instance of "%s".', __METHOD__, SessionUser::class)
            );
        }

        return new BearerToken($user->getUserData()->getToken(), $user, $user->getUserData()->getExpiresAt());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsRememberMe(): bool
    {
        return false;
    }

    private function getTokenExtractor(): TokenExtractor
    {
        return $this->tokenExtractor;
    }
}
