<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge\config;

use LazyLemurs\SymfonyExtensions\Bundle;

final class SecurityServiceSymfonyBridgeBundle extends Bundle
{

}
