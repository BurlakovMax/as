<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

interface AuthenticationResponseBuilder
{
    public function unauthorized(AuthenticationException $e = null): Response;

    public function authenticationFailure(AuthenticationException $e): Response;
}
