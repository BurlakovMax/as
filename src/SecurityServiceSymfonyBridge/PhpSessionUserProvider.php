<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use App\Security\Application\AccountData;
use App\Security\Application\AccountQueryProcessor;
use LazyLemurs\Structures\Email;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class PhpSessionUserProvider implements UserProviderInterface
{
    private AccountQueryProcessor $accountQueryProcessor;

    public function __construct(AccountQueryProcessor $accountQueryProcessor)
    {
        $this->accountQueryProcessor = $accountQueryProcessor;
    }

    /**
     * @throws \App\Security\Domain\AccountNotFound
     */
    public function loadUserByUsername($username)
    {
        return $this->accountQueryProcessor->getByEmail(new Email($username));
    }

    /**
     * @throws \App\Security\Domain\AccountNotFound
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof AccountData) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @inheritdoc
     */
    public function supportsClass($class): bool
    {
        return $class === AccountData::class;
    }
}