<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use App\Security\Application\AuthenticationService;
use App\Security\Domain\InvalidCredentials;
use LazyLemurs\Structures\Email;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

final class SignInFormAuthenticator extends AbstractFormLoginAuthenticator
{
    private AuthenticationService $authenticationService;

    private RouterInterface $router;

    private CsrfTokenManagerInterface $csrfTokenManager;

    private FlashBagInterface $session;

    public function __construct(
        AuthenticationService $authenticationService,
        RouterInterface $router,
        CsrfTokenManagerInterface $csrfTokenManager,
        FlashBagInterface $session
    ) {
        $this->authenticationService = $authenticationService;
        $this->router = $router;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->session = $session;
    }

    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'auth'
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'csrf_token' => $request->request->get('_csrf_token'),
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
        ];

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    /**
     * @throws \Throwable
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);

        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException('Invalid Csrf Token');
        }

        if (!is_array($credentials)) {
            throw new \InvalidArgumentException(
                sprintf('The arguments must not be an empty')
            );
        }

        if (!isset($credentials['email'])) {
            throw new \InvalidArgumentException(
                sprintf('The first argument E-mail must not be a empty')
            );
        }

        try {
            return $this->authenticationService->authenticateAccount(new Email($credentials['email']), $credentials['password']);
        } catch (\App\Security\Domain\AuthenticationException $exception) {
            if ($exception instanceof InvalidCredentials) {
                throw new AuthenticationException(
                    'Login or password is incorrect'
                );
            }

            throw new AuthenticationException(
                $exception->getMessage()
            );
        }
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $user->isWorker();
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->session->add('error', $exception->getMessage());

        return new RedirectResponse(
            $this->router->generate('login')
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->router->generate('account_list'));
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate('login', [
            'origin' => $request->headers->get('referer')
        ]));
    }

    public function supportsRememberMe()
    {
        return parent::supportsRememberMe();
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('login');
    }
}
