<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

final class RestfulApiAuthenticationResponseBuilder implements AuthenticationResponseBuilder
{
    public function unauthorized(AuthenticationException $e = null): Response
    {
        return
            new JsonResponse(
                $this->buildError(
                    Response::HTTP_UNAUTHORIZED,
                    Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
                ),
                Response::HTTP_UNAUTHORIZED
            );
    }

    public function authenticationFailure(AuthenticationException $e): Response
    {
        return
            new JsonResponse(
                $this->buildError(
                    Response::HTTP_UNAUTHORIZED,
                    Response::$statusTexts[Response::HTTP_UNAUTHORIZED]
                ),
                Response::HTTP_UNAUTHORIZED
            );
    }

    private function buildError(int $code, string $message): array
    {
        return
            [
                'error' => [
                    'code' => $code,
                    'message' => $message
                ],
            ]
        ;
    }
}
