<?php

declare(strict_types=1);

namespace App\SecurityServiceSymfonyBridge;

use App\Security\Application\SessionData;
use Symfony\Component\Security\Core\User\UserInterface;

final class SessionUser implements UserInterface
{
    private SessionData $userData;

    public function __construct(SessionData $userData)
    {
        $this->userData = $userData;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles(): array
    {
        return $this->getUserData()->getRoles();
    }

    public function getPassword(): ?string
    {
        return null;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->getUserData()->getEmail()->getValue();
    }

    public function eraseCredentials(): void
    {
        /*_*/
    }

    public function getUserData(): SessionData
    {
        return $this->userData;
    }

    public function __toString(): string
    {
        return $this->getUserData()->getEmail()->getValue();
    }
}
