<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

use LazyLemurs\Exceptions\NotFoundException;

final class MessageNotFound extends NotFoundException
{

}