<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

use LazyLemurs\Structures\PhoneNumber;

interface SmsProvider
{
    /**
     * @throws SmsSendError
     */
    public function send(PhoneNumber $number, string $text): ?string;
}