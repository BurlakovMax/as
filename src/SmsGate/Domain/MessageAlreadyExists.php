<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

use LazyLemurs\Exceptions\Exception;

final class MessageAlreadyExists extends Exception
{

}