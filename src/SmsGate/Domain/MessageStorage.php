<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

use Ramsey\Uuid\UuidInterface;

interface MessageStorage
{
    public function add(Message $message): void;

    public function findByExternalId(string $externalId): ?Message;

    public function findById(UuidInterface $messageId): ?Message;

    public function findByIdAndLock(UuidInterface $messageId): ?Message;

    public function findUnsentAndLock(): ?Message;
}