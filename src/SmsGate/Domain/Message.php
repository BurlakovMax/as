<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

use App\Core\Domain\AttemptCounter;
use App\Core\Domain\MaxAttemptReached;
use DateTimeImmutable;
use LazyLemurs\Structures\PhoneNumber;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\SmsGate\Infrastructure\Persistence\MessageDoctrineRepository")
 * @ORM\Table(name="sms_gate_message")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $messageId;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $externalId;

    /**
     * @ORM\Column(type="timestamp", length=11)
     */
    private DateTimeImmutable $sentAt;

    /**
     * @ORM\Column(type="timestamp", length=11, nullable=true)
     */
    private ?DateTimeImmutable $deliveredAt = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isDelivered;

    /**
     * @ORM\Column(type="phone_number")
     */
    private PhoneNumber $phoneNumber;

    /**
     * @ORM\Column(type="string", length=511)
     */
    private string $text;

    /**
     * @ORM\Embedded(class="App\Core\Domain\AttemptCounter", columnPrefix=false)
     */
    private AttemptCounter $attemptCounter;

    public function __construct(
        UuidInterface $messageId,
        PhoneNumber $phoneNumber,
        string $text,
        ?string $externalId = null
    ) {
        $this->messageId = $messageId;
        $this->externalId = $externalId;
        $this->sentAt = new DateTimeImmutable();
        $this->isDelivered = false;
        $this->phoneNumber = $phoneNumber;
        $this->text = $text;
        $this->attemptCounter = new AttemptCounter();
    }

    public function updateDeliveryStatus(bool $isDelivered): void
    {
        $this->isDelivered = $isDelivered;
        $this->deliveredAt = new DateTimeImmutable();
    }

    /**
     * @param SmsProvider $provider
     * @throws MaxAttemptReached
     */
    public function sendViaProvider(SmsProvider $provider): void
    {
        try {
            $this->setSuccessSendAttempt(
                $provider->send($this->getPhoneNumber(), $this->getText())
            );
        } catch (SmsSendError $e) {
            $this->attemptCounter->nextAttempt();
        }
    }

    private function getMessageId(): UuidInterface
    {
        return $this->messageId;
    }

    private function getPhoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }

    private function getText(): string
    {
        return $this->text;
    }

    private function setSuccessSendAttempt(?string $externalId): void
    {
        $this->externalId = $externalId;
    }
}
