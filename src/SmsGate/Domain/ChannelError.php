<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

final class ChannelError extends \Exception implements SmsSendError
{

}