<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

use App\Core\Domain\MaxAttemptReached;
use LazyLemurs\Structures\PhoneNumber;
use Ramsey\Uuid\UuidInterface;

final class MessageService
{
    /** @var MessageStorage */
    private $storage;

    /** @var SmsProvider */
    private $provider;

    public function __construct(MessageStorage $storage, SmsProvider $provider)
    {
        $this->storage = $storage;
        $this->provider = $provider;
    }

    /**
     * @throws MessageNotFound
     * @throws MessageAlreadyExists
     */
    public function sendSync(UuidInterface $messageId, PhoneNumber $number, string $text): void
    {
        $this->sendAsync($messageId, $number, $text);

        try {
            $this->sendById($messageId);
        } catch (SmsSendError $e) {
            /*_*/
        }
    }

    /**
     * @throws MessageAlreadyExists
     */
    public function sendAsync(UuidInterface $messageId, PhoneNumber $number, string $text): void
    {
        if ($this->storage->findById($messageId)) {
            throw new MessageAlreadyExists();
        }

        $this->storage->add(
            new Message($messageId, $number, $text)
        );
    }

    /**
     * @throws MessageNotFound
     * @throws \Exception
     */
    public function sendById(UuidInterface $messageId): void
    {
        $message = $this->getMessage($messageId);
        $this->sendViaProvider($message);
    }

    /**
     * @throws MessageNotFound
     * @throws \Exception
     */
    public function sendUnsent(): void
    {
        $message = $this->storage->findUnsentAndLock();
        if (!$message) {
            throw new MessageNotFound();
        }

        $this->sendViaProvider($message);
    }

    /**
     * @throws \Exception
     */
    public function updateDeliveryStatus(string $externalId, bool $isDelivered): void
    {
        $message = $this->storage->findByExternalId($externalId);

        if ($message) {
            $this->doUpdateDeliveryStatus($message, $isDelivered);
        }
    }

    /**
     * @param Message $message
     * @throws \Exception
     */
    private function sendViaProvider(Message $message): void
    {
        try {
            $message->sendViaProvider($this->provider);
        } catch (MaxAttemptReached $e) {
            $this->doUpdateDeliveryStatus($message, false);
        }
    }

    /**
     * @param Message $message
     * @param bool $isDelivered
     * @throws \Exception
     */
    private function doUpdateDeliveryStatus(Message $message, bool $isDelivered): void
    {
        $message->updateDeliveryStatus($isDelivered);
    }

    /**
     * @param UuidInterface $messageId
     * @return Message
     *
     * @throws MessageNotFound
     */
    private function getMessage(UuidInterface $messageId): Message
    {
        $message = $this->storage->findByIdAndLock($messageId);
        if (!$message) {
            throw new MessageNotFound();
        }

        return $message;
    }
}