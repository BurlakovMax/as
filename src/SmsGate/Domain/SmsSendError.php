<?php

declare(strict_types=1);

namespace App\SmsGate\Domain;

interface SmsSendError extends \Throwable
{

}