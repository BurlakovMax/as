<?php

declare(strict_types=1);

namespace App\SmsGate\Infrastructure\Persistence;

use App\SmsGate\Domain\Message;
use App\SmsGate\Domain\MessageStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\UuidInterface;

final class MessageDoctrineRepository extends EntityRepository implements MessageStorage
{
    public function add(Message $message): void
    {
        $this->getEntityManager()->persist($message);
    }

    public function findByExternalId(string $externalId): ?Message
    {
        return $this->findOneBy(
            [
                'externalId' => $externalId
            ]
        );
    }

    public function findById(UuidInterface $messageId): ?Message
    {
        return $this->find($messageId);
    }

    public function findByIdAndLock(UuidInterface $messageId): ?Message
    {
        return $this->find($messageId, LockMode::PESSIMISTIC_WRITE);
    }

    /**
     * @return Message|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function findUnsentAndLock(): ?Message
    {
        $message =
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->isNull('externalId'))
                        ->andWhere(Criteria::expr()->lte('attemptCounter.nextAttemptAt' , time()))
                        ->orderBy(['attemptCounter.nextAttemptAt' => Criteria::DESC])
                        ->setMaxResults(1)
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                    ->getOneOrNullResult();

        return $message;
    }
}