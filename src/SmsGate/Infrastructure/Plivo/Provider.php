<?php

declare(strict_types=1);

namespace App\SmsGate\Infrastructure\Plivo;

use LazyLemurs\Structures\PhoneNumber;
use App\SmsGate\Domain\ChannelError;
use App\SmsGate\Domain\SmsProvider;
use Plivo\Exceptions\PlivoRestException;
use Plivo\Resources\Message\MessageCreateResponse;
use Plivo\RestClient;
use Psr\Log\LoggerInterface;

final class Provider implements SmsProvider
{
    private string $from;

    private RestClient $restClient;

    private LoggerInterface $logger;

    public function __construct(
        string $from,
        RestClient $restClient,
        LoggerInterface $logger
    )
    {
        $this->from = $from;
        $this->restClient = $restClient;
        $this->logger = $logger;
    }

    /**
     * @throws ChannelError
     */
    public function send(PhoneNumber $number, string $text): ?string
    {
        try {
            /** @var MessageCreateResponse $response */
            $response = $this->restClient->messages->create(
                $this->from,
                [$number->getValue()],
                $text
            );

            return $response->getMessageUuid()[0] ?? null;
        } catch (PlivoRestException $e) {
            $this->logError($e);
            throw new ChannelError($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function logError(\Exception $e): void
    {
        $this->logger->error($e->getMessage());
    }
}