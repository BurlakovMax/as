<?php

declare(strict_types=1);

namespace App\SmsGate\Infrastructure\ClickSend;

use App\SmsGate\Domain\ChannelError;
use App\SmsGate\Domain\SmsProvider;
use ClickSend\Api\SMSApi;
use ClickSend\Configuration;
use ClickSend\Model\SmsMessage;
use ClickSend\Model\SmsMessageCollection;
use GuzzleHttp\Client;
use LazyLemurs\Structures\PhoneNumber;
use Psr\Log\LoggerInterface;

final class Provider implements SmsProvider
{
    private string $username;

    private string $apiKey;

    private string $from;

    private LoggerInterface $logger;

    public function __construct(
        string $username,
        string $apiKey,
        string $from,
        LoggerInterface $logger
    ) {
        $this->username = $username;
        $this->apiKey = $apiKey;
        $this->from = $from;
        $this->logger = $logger;
    }

    public function send(PhoneNumber $number, string $text): ?string
    {
        $config = Configuration::getDefaultConfiguration()
            ->setUsername($this->username)
            ->setPassword($this->apiKey);

        $apiInstance = new SMSApi(new Client(), $config);
        $message = new SmsMessage();
        $message->setFrom($this->from);
        $message->setBody($text);
        $message->setSource('sdk');

//        if (getenv('APP_ENV') !== 'prod') {
//            $message->setTo('61411111111');
//        } else {
            $message->setTo($number->getValue());
//        }

        $messages = new SmsMessageCollection();
        $messages->setMessages([ $message ]);

        try {
            $result = $apiInstance->smsSendPost($messages);
            $response = json_decode($result, true);

            if ($response['http_code'] === 200 && $response['data']['messages'][0]['status'] === 'SUCCESS') {
                return (string) $response['data']['messages'][0]['message_id'] ?? null;
            }

            throw new \Exception(
                'SMS did not send, phone: ' . $message->getTo(), ' text: ' . $message->getBody()
            );
        } catch (\Exception $e) {
            $this->logError($e);
            throw new ChannelError($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function logError(\Exception $e): void
    {
        $this->logger->error($e->getMessage());
    }
}