<?php

declare(strict_types=1);

namespace App\SmsGate\Application;

use LazyLemurs\TransactionManager\TransactionManager;
use LazyLemurs\Structures\PhoneNumber;
use App\SmsGate\Domain\MessageService;
use Ramsey\Uuid\UuidInterface;

final class SmsCommandService
{
    /** @var TransactionManager */
    private $transactionalManager;

    /** @var MessageService */
    private $messageService;

    public function __construct(TransactionManager $transactionalManager, MessageService $messageService)
    {
        $this->transactionalManager = $transactionalManager;
        $this->messageService = $messageService;
    }

    /**
     * @throws \Throwable
     */
    public function sendSync(UuidInterface $messageId, PhoneNumber $number, string $text): void
    {
        $this->transactionalManager->transactional(function () use ($messageId, $number, $text) {
            $this->messageService->sendSync($messageId, $number, $text);
        });
    }

    /**
     * @throws \Throwable
     */
    public function sendAsync(UuidInterface $messageId, PhoneNumber $number, string $text): void
    {
        $this->transactionalManager->transactional(function () use ($messageId, $number, $text) {
            $this->messageService->sendAsync($messageId, $number, $text);
        });
    }

    /**
     * @throws \Throwable
     */
    public function updateDeliveryStatus(string $externalId, bool $isDelivered): void
    {
        $this->transactionalManager->transactional(function () use ($externalId, $isDelivered) {
            $this->messageService->updateDeliveryStatus($externalId, $isDelivered);
        });
    }

    /**
     * @throws \Throwable
     */
    public function sendUnsent(): void
    {
        $this->transactionalManager->transactional(function () {
            $this->messageService->sendUnsent();
        });
    }
}