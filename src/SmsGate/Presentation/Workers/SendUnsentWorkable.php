<?php

declare(strict_types=1);

namespace App\SmsGate\Presentation\Workers;

use LazyLemurs\Workers\Workable;
use App\SmsGate\Application\SmsCommandService;
use App\SmsGate\Domain\MessageNotFound;
use App\SmsGate\Domain\SmsSendError;
use Psr\Log\LoggerInterface;

final class SendUnsentWorkable implements Workable
{
    /** @var SmsCommandService */
    private $application;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(SmsCommandService $application, LoggerInterface $logger)
    {
        $this->application = $application;
        $this->logger = $logger;
    }

    /**
     * @return bool true of processed, false otherwise
     * @throws \Throwable
     */
    public function process(): bool
    {
        try {
            $this->application->sendUnsent();
            return true;
        } catch (SmsSendError | MessageNotFound $e) {
            $this->logger->debug($e->getMessage());
            return false;
        }
    }
}