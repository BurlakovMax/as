<?php

declare(strict_types=1);

namespace App\SmsGate\Presentation\Commands;

use LazyLemurs\Structures\PhoneNumber;
use App\SmsGate\Application\SmsCommandService;
use Ramsey\Uuid\Uuid;
use LazyLemurs\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SendSmsCommand extends Command
{
    private SmsCommandService $application;

    public function __construct(SmsCommandService $application)
    {
        parent::__construct('sms-gate:send');
        $this->application = $application;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Sends SMS')
            ->setHelp('This command sends SMS')
            ->addArgument('messageId', InputArgument::REQUIRED, 'Message ID (UUID)')
            ->addArgument('to', InputArgument::REQUIRED, 'Phone number')
            ->addArgument('text', InputArgument::REQUIRED, 'Message')
        ;
    }

    protected function doExecute(InputInterface $input, OutputInterface $output): void
    {
        try {
            $this->application->sendSync(
                Uuid::fromString($input->getArgument('messageId')),
                new PhoneNumber($input->getArgument('to')),
                $input->getArgument('text')
            );
        } catch (\Throwable  $e) {
            $output->writeln($e->getMessage());
        }
    }
}