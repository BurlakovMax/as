<?php

declare(strict_types=1);

namespace App\Frontend\Security\Presentation\Api\Rest\Version1;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;

final class InvalidateTokenController extends AbstractSecureController
{
    /**
     * @Route("/logout", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Logout",
     *     tags={"auth"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="return nothing"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function invalidateToken(): JsonResponse
    {
        $this->ensureAuthorized();

        $this->getAuthenticationService()->invalidateToken($this->getToken()->getCredentials());

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
