<?php

declare(strict_types=1);

namespace App\Frontend\Security\Presentation\Api\Rest\Version1;

use App\Core\Application\CaptchaVerifier;
use App\Security\Application\AccountCommandService;
use LazyLemurs\Structures\Email;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

final class CreateAccountController extends AbstractController
{
    /**
     * @Route("/sign_up", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="create account",
     *     tags={"account", "security"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="confirmation Id",
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function createAccount(Request $request): JsonResponse
    {
        $email = new Email($request->request->get('email'));
        $username = $request->request->get('username');
        $password = $request->request->get('password');
//        $verificationCode = $paramFetcher->get('verificationCode', true);
//
//        if (!$this->getCaptchaVerifier()->verify($verificationCode)) {
//            throw new InvalidCaptcha();
//        }

        $confirmation = $this->getUserCommandService()->register(
            $email,
            $username,
            $password,
            $request->getClientIp()
        );

        return
            $this->json(
                $confirmation,
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/sign_up_confirm", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="confirm create account",
     *     tags={"account", "security"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Nothing"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function confirmEmail(Request $request): RedirectResponse
    {
        $confirmationId = $request->query->get('confirmationId');
        $confirmationCode = $request->query->get('confirmationCode');

        $this->getUserCommandService()->confirmRegistration(
            $confirmationId,
            $confirmationCode
        );

        return $this->redirect($_ENV['APP_URL'] . '/account/signin');
    }

    private function getUserCommandService(): AccountCommandService
    {
        return $this->container->get(AccountCommandService::class);
    }

    private function getCaptchaVerifier(): CaptchaVerifier
    {
        return $this->container->get(CaptchaVerifier::class);
    }
}
