<?php

declare(strict_types=1);

namespace App\Frontend\Security\Presentation\Api\Rest\Version1;

use App\Security\Application\AccountCommandService;
use LazyLemurs\Structures\Email;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

final class RecoverAccountController extends AbstractController
{
    /**
     * @Route("/password_reset", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="request password reset",
     *     tags={"account", "security"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="confirmation Id",
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function requestPasswordReset(Request $request): JsonResponse
    {
        $email = new Email($request->request->get('email', true));

        $confirmationId = $this->getUserCommandService()->requestPasswordReset($email);

        return
            $this->json(
                [
                    'confirmationId' => $confirmationId,
                ],
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/confirm_password_reset", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="confirm password reset",
     *     tags={"account", "security"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Nothing"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function confirmPasswordReset(Request $request): JsonResponse
    {
        $confirmationId = $request->request->get('confirmationId');
        $confirmationCode = $request->request->get('confirmationCode');
        $password = $request->request->get('password');

        $this->getUserCommandService()->confirmPasswordReset(
            $confirmationId,
            $confirmationCode,
            $password
        );

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    private function getUserCommandService(): AccountCommandService
    {
        return $this->container->get(AccountCommandService::class);
    }
}
