<?php

declare(strict_types=1);

namespace App\Frontend\Security\Presentation\Api\Rest\Version1;

use App\Security\Application\AuthenticationService;
use LazyLemurs\Structures\Email;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

final class AuthenticateController extends AbstractController
{
    /**
     * @Route("/login", methods={"POST"}, name="logins")
     *
     * @SWG\Post(
     *     summary="Login",
     *     tags={"auth"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Token",
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function authenticate(Request $request): JsonResponse
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        $token = $this->getAuthenticationService()->authenticateAccountWithToken(
            new Email($email),
            $password
        );

        return
            $this->json(
                $token,
                Response::HTTP_OK
            )
        ;
    }

    private function getAuthenticationService(): AuthenticationService
    {
        return $this->container->get(AuthenticationService::class);
    }
}
