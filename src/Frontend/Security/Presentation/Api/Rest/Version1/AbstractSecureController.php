<?php

declare(strict_types=1);

namespace App\Frontend\Security\Presentation\Api\Rest\Version1;

use App\Security\Application\AuthenticationService;
use App\Security\Domain\Unauthorized;
use App\SecurityServiceSymfonyBridge\BearerToken;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

abstract class AbstractSecureController extends AbstractController
{
    final protected function getAuthenticationService(): AuthenticationService
    {
        return $this->container->get(AuthenticationService::class);
    }

    final protected function getTokenStorage(): TokenStorageInterface
    {
        return $this->container->get('security.token_storage');
    }

    /**
     * @throws Unauthorized
     */
    final protected function getToken(): TokenInterface
    {
        $token = $this->getTokenStorage()->getToken();

        if (!$token instanceof BearerToken) {
            throw new Unauthorized();
        }

        return $token;
    }

    /**
     * @throws Unauthorized
     */
    final protected function ensureAuthorized(string $role = null): void
    {
        $token = $this->getToken();
        if (!$token->isAuthenticated()) {
            throw new Unauthorized();
        }

        if ($role && !in_array($role, $token->getRoleNames(), true)) {
            throw new Unauthorized();
        }
    }
}
