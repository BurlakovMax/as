<?php

declare(strict_types=1);

namespace App\Frontend\Twig\Support;

use App\Contacts\Application\ContactQueryProcessor;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;

final class ContactFormMessageCounter
{
    private ContactQueryProcessor $contactQueryProcessor;

    public function __construct(ContactQueryProcessor $contactQueryProcessor)
    {
        $this->contactQueryProcessor = $contactQueryProcessor;
    }

    public function count(): int
    {
        $searchQuery = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('t.read', false),
            ],
            [],
            LimitationQueryImmutable::create(null, null)
        );

        return $this->contactQueryProcessor->countBySearchQuery($searchQuery);
    }
}
