<?php

declare(strict_types=1);

namespace App\Frontend\Twig\Support\QuantityList\Structures;

use App\Frontend\Twig\Support\LinkOpenType;

final class LinkWithQuantity
{
    private string $name;
    private string $url;
    private int $count;
    private LinkOpenType $linkOpenType;

    public function __construct(string $name, string $url, int $count, LinkOpenType $linkOpenType)
    {
        $this->name = $name;
        $this->url = $url;
        $this->count = $count;
        $this->linkOpenType = $linkOpenType;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getLinkOpenType(): string
    {
        return $this->linkOpenType->getRawValue();
    }
}
