<?php

declare(strict_types=1);

namespace App\Frontend\Twig\Support\QuantityList;

use App\Core\Application\Search\FilterQuery;
use App\Escorts\Application\EscortQueryProcessor;
use App\Escorts\Application\EscortTypeConverter;
use App\Escorts\Application\ManForManEscortQueryProcessor;
use App\Escorts\Application\TypesData;
use App\Forums\Application\ForumLinkQueryProcessor;
use App\Forums\Application\ForumQueryProcessor;
use App\Frontend\Twig\Support\LinkOpenType;
use App\Frontend\Twig\Support\QuantityList\Structures\LinkWithQuantity;
use App\Locations\Application\CityData;
use App\Places\Application\PlaceQueryProcessor;
use App\Places\Application\PlaceTypeQueryProcessor;

final class QuantityListBuilder
{
    private EscortQueryProcessor $escortQueryProcessor;
    private PlaceQueryProcessor $placeQueryProcessor;
    private PlaceTypeQueryProcessor $placeTypeQueryProcessor;
    private ManForManEscortQueryProcessor $manForManEscortQueryProcessor;
    private ForumQueryProcessor $forumQueryProcessor;
    private ForumLinkQueryProcessor $forumLinkQueryProcessor;
    private string $appUrl;

    public function __construct(
        EscortQueryProcessor $escortQueryProcessor,
        ManForManEscortQueryProcessor $manForManEscortQueryProcessor,
        PlaceQueryProcessor $placeQueryProcessor,
        PlaceTypeQueryProcessor $placeTypeQueryProcessor,
        ForumQueryProcessor $forumQueryProcessor,
        ForumLinkQueryProcessor $forumLinkQueryProcessor,
        string $appUrl
    ) {
        $this->escortQueryProcessor = $escortQueryProcessor;
        $this->manForManEscortQueryProcessor = $manForManEscortQueryProcessor;
        $this->placeQueryProcessor = $placeQueryProcessor;
        $this->placeTypeQueryProcessor = $placeTypeQueryProcessor;
        $this->forumQueryProcessor = $forumQueryProcessor;
        $this->forumLinkQueryProcessor = $forumLinkQueryProcessor;
        $this->appUrl = $appUrl;
    }

    /**
     * @return LinkWithQuantity[]
     */
    public function getEscortTypes(CityData $cityData): array
    {
        $categories = [];

        $escortTypes = [TypesData::female(), TypesData::shemale(), TypesData::bodyRubs()];

        foreach ($escortTypes as $escortType) {
            $slug = EscortTypeConverter::valueToSlug($escortType);

            $categories[] = new LinkWithQuantity(
                EscortTypeConverter::valueToName($escortType),
                $this->getFullUrl($cityData->getUrl(), $slug),
                $this->escortQueryProcessor->countByFilterQuery(
                    [
                        FilterQuery::createEqFilter('locations.locationId', $cityData->getId()),
                        FilterQuery::createEqFilter('t.type', $escortType),
                        FilterQuery::createEqFilter('t.sponsorAttributes.sponsor', false),
                    ]
                ),
                LinkOpenType::inCurrentWindow()
            );
        }

        $categories[] = $this->getM4MCategory($cityData);

        return $categories;
    }

    private function getM4MCategory(CityData $cityData): LinkWithQuantity
    {
        $m4mType = $this->manForManEscortQueryProcessor->getCurrentM4mLink(null, $cityData);

        return new LinkWithQuantity(
            $m4mType->getName(),
            $m4mType->getUrl(),
            0,
            LinkOpenType::inNewWindow()
        );
    }

    /**
     * @return LinkWithQuantity[]
     */
    public function getPlaceTypes(CityData $cityData): array
    {
        $categories = [];

        $placeTypeIds = $this->placeQueryProcessor->getTypeIdsByLocationId($cityData->getId());
        $placeTypes = $this->placeTypeQueryProcessor->getByIds($placeTypeIds);

        foreach ($placeTypes as $placeType) {
            $categories[] = new LinkWithQuantity(
                $placeType->getName(),
                $this->getFullUrl($cityData->getUrl(), $placeType->getUrl()),
                $this->placeQueryProcessor->count($placeType->getId(), [$cityData->getId()]),
                LinkOpenType::inCurrentWindow()
            );
        }

        return $categories;
    }

    /**
     * @return LinkWithQuantity[]
     */
    public function getForums(CityData $cityData): array
    {
        $forums = $this->forumQueryProcessor->getByLocationId($cityData->getId());

        $forumsWithIdInKey = [];
        foreach ($forums as $forum) {
            $forumsWithIdInKey[$forum->getId()] = $forum;
        }

        $forumLinks = $this->forumLinkQueryProcessor->getByIds(array_keys($forumsWithIdInKey));

        $categories = [];
        foreach ($forumLinks as $link) {
            $categories[] = new LinkWithQuantity(
                $link->getName(),
                $this->getFullUrl($cityData->getUrl(), $link->getUrl()),
                $forumsWithIdInKey[$link->getId()]->getCountTopics(),
                LinkOpenType::inCurrentWindow()
            );
        }

        return $categories;
    }

    private function getFullUrl(string $cityUrl, string $slug): string
    {
        return $this->appUrl . $cityUrl . $slug;
    }
}
