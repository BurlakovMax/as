<?php

declare(strict_types=1);

namespace App\Frontend\Twig\Support\NavigationSlides\Structures;

use App\Frontend\Twig\Support\LinkOpenType;

final class Slide
{
    private string $name;
    private string $url;
    private string $image;
    private LinkOpenType $linkOpenType;

    public function __construct(string $name, string $url, string $image, LinkOpenType $linkOpenType)
    {
        $this->name = $name;
        $this->url = $url;
        $this->image = $image;
        $this->linkOpenType = $linkOpenType;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getLinkOpenType(): string
    {
        return $this->linkOpenType->getRawValue();
    }
}
