<?php

declare(strict_types=1);

namespace App\Frontend\Twig\Support\NavigationSlides;

use App\Escorts\Application\EscortTypeConverter;
use App\Escorts\Application\TypesData;
use App\Frontend\Twig\Support\LinkOpenType;
use App\Frontend\Twig\Support\NavigationSlides\Structures\Slide;
use App\Locations\Application\CityData;
use App\Places\Application\PlaceQueryProcessor;
use App\Places\Application\PlaceTypeQueryProcessor;
use App\Escorts\Application\ManForManEscortQueryProcessor;

final class NavigationSlidesBuilder
{
    private PlaceQueryProcessor $placeQueryProcessor;
    private PlaceTypeQueryProcessor $placeTypeQueryProcessor;
    private ManForManEscortQueryProcessor $manForManEscortQueryProcessor;
    private string $appUrl;
    private string $imagesCategoryDir;
    private string $imagesCategoryExt;

    public function __construct(
        PlaceQueryProcessor $placeQueryProcessor,
        PlaceTypeQueryProcessor $placeTypeQueryProcessor,
        ManForManEscortQueryProcessor $manForManEscortQueryProcessor,
        string $appUrl,
        string $imagesCategoryDir,
        string $imagesCategoryExt
    ) {
        $this->placeQueryProcessor = $placeQueryProcessor;
        $this->placeTypeQueryProcessor = $placeTypeQueryProcessor;
        $this->manForManEscortQueryProcessor = $manForManEscortQueryProcessor;
        $this->appUrl = $appUrl;
        $this->imagesCategoryDir = $imagesCategoryDir;
        $this->imagesCategoryExt = $imagesCategoryExt;
    }

    /**
     * @return Slide[]
     */
    public function getSlides(CityData $cityData): array
    {
        return array_merge($this->getEscortTypesSlides($cityData), $this->getPlaceTypesSlides($cityData));
    }

    /**
     * @return Slide[]
     */
    private function getEscortTypesSlides(CityData $cityData): array
    {
        $slides = [];

        $escortTypes = [TypesData::female(), TypesData::shemale(), TypesData::bodyRubs()];

        foreach ($escortTypes as $escortType) {
            $slug = EscortTypeConverter::valueToSlug($escortType);

            $slides[] = new Slide(
                EscortTypeConverter::valueToName($escortType),
                $this->appUrl . $cityData->getUrl() . $slug,
                $this->getImageUrl($slug),
                LinkOpenType::inCurrentWindow()
            );
        }

        $slides[] = $this->getM4MSlide($cityData);

        return $slides;
    }

    /**
     * @return Slide[]
     */
    private function getPlaceTypesSlides(CityData $cityData): array
    {
        $slides = [];

        $placeTypeIds = $this->placeQueryProcessor->getTypeIdsByLocationId($cityData->getId());
        $placeTypes = $this->placeTypeQueryProcessor->getByIds($placeTypeIds);

        foreach ($placeTypes as $placeType) {
            $slides[] = new Slide(
                $placeType->getName(),
                $this->appUrl . $cityData->getUrl() . $placeType->getUrl(),
                $this->getImageUrl($placeType->getUrl()),
                LinkOpenType::inCurrentWindow()
            );
        }

        return $slides;
    }

    private function getM4MSlide(CityData $cityData): Slide
    {
        $m4mType = $this->manForManEscortQueryProcessor->getCurrentM4mLink(null, $cityData);

        return new Slide(
            $m4mType->getName(),
            $m4mType->getUrl(),
            $this->getImageUrl('m4m'),
            LinkOpenType::inNewWindow()
        );
    }

    private function getImageUrl(string $slug): string
    {
        return $this->appUrl . $this->imagesCategoryDir . $slug . '.' . $this->imagesCategoryExt;
    }
}
