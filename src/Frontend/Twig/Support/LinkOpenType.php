<?php

declare(strict_types=1);

namespace App\Frontend\Twig\Support;

use LitGroup\Enumerable\Enumerable;

final class LinkOpenType extends Enumerable
{
    private const
        HTML_A_TAG_TARGET_BLANK = '_blank',
        HTML_A_TAG_TARGET_SELF  = '_self'
    ;

    public static function inNewWindow(): self
    {
        return self::createEnum(self::HTML_A_TAG_TARGET_BLANK);
    }

    public static function inCurrentWindow(): self
    {
        return self::createEnum(self::HTML_A_TAG_TARGET_SELF);
    }
}
