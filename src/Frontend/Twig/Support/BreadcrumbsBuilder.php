<?php

declare(strict_types=1);

namespace App\Frontend\Twig\Support;

use App\Escorts\Application\EscortData;
use App\Escorts\Application\EscortQueryProcessor;
use App\Escorts\Application\EscortTypeConverter;
use App\Locations\Application\CityData;
use App\Locations\Application\CityQueryProcessor;
use App\Locations\Application\CountryQueryProcessor;
use App\Locations\Application\StateData;
use App\Locations\Application\StateQueryProcessor;
use App\Places\Application\PlaceData;
use App\Places\Application\PlaceQueryProcessor;
use App\Places\Application\PlaceTypeData;

final class BreadcrumbsBuilder
{
    private EscortQueryProcessor $escortQueryProcessor;

    private PlaceQueryProcessor $placeQueryProcessor;

    private CityQueryProcessor $cityQueryProcessor;

    private StateQueryProcessor $stateQueryProcessor;

    private CountryQueryProcessor $countryQueryProcessor;

    private array $links;

    public function __construct(
        EscortQueryProcessor $escortQueryProcessor,
        PlaceQueryProcessor $placeQueryProcessor,
        CityQueryProcessor $cityQueryProcessor,
        StateQueryProcessor $stateQueryProcessor,
        CountryQueryProcessor $countryQueryProcessor
    ) {
        $this->escortQueryProcessor = $escortQueryProcessor;
        $this->cityQueryProcessor = $cityQueryProcessor;
        $this->countryQueryProcessor = $countryQueryProcessor;
        $this->placeQueryProcessor = $placeQueryProcessor;
        $this->stateQueryProcessor = $stateQueryProcessor;
    }

    public function getLinksByCityAndPlaceAndPlaceType(CityData $city, PlaceData $place, PlaceTypeData $placeTypeData): array
    {
        $this->createLocationPath($city);

        $this->links[] = [
            'name' => $placeTypeData->getName(),
            'url' => $city->getUrl() . $placeTypeData->getUrl()
        ];

        $this->links[] = ['name' => $place->getName(), 'url' => '#'];

        return $this->links;
    }

    public function getLinksByCityAndEscortId(CityData $city, EscortData $escort): array
    {
        $this->createLocationPath($city);

        $this->links[] = [
            'name' => EscortTypeConverter::valueToName($escort->getRawType()),
            'url'  => $city->getUrl() . EscortTypeConverter::valueToSlug($escort->getRawType())
        ];
        $this->links[] = ['name' => $escort->getPhone(), 'url' => '#'];

        return $this->links;
    }

    public function getLinksByCityAndTypeName(CityData $city, ?string $type): array
    {
        $this->createLocationPath($city);

        if (null !== $type) {
            $this->links[] = [
                'name' => $type,
                'url'  => '#'
            ];
        }

        return $this->links;
    }
    
    private function createLocationPath(CityData $city): void
    {
        $this->links[] = ['name' => 'Home', 'url' => '/'];
        $this->generateLinkForCountry($city->getCountryId());

        if (null !== $city->getStateId()) {
            $this->generateLinkForState($city->getStateId());
        }

        $this->links[] = ['name' => $city->getName(), 'url' => $city->getUrl()];
    }

    public function getLinksForState(StateData $state, ?string $type = null): array
    {
        $this->links[] = ['name' => 'Home', 'url' => '/'];
        $this->generateLinkForCountry($state->getCountryId());
        $this->generateLinkForState($state->getId());

        if (null !== $type) {
            $this->links[] = [
                'name' => $type,
                'url'  => '#'
            ];
        }

        return $this->links;
    }

    private function generateLinkForCountry(int $countryId): void
    {
        $country = $this->countryQueryProcessor->getById($countryId);
        $this->links[] = ['name' => $country->getCode() === 'US' ? 'USA' : $country->getCode(), 'url' => '#'];
    }

    private function generateLinkForState(int $stateId): void
    {
        $state = $this->stateQueryProcessor->getById($stateId);
        $this->links[] = ['name' => $state->getCode(), 'url' => $state->getUrl()];
    }
}
