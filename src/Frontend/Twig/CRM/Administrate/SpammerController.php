<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Administrate;

use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\SpammerCommandProcessor;
use App\Security\Application\SpammerCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Frontend\Processors\SpammerQueryProcessor;

final class SpammerController extends AbstractController
{
    use SpammerQueryProcessor;
    use SpammerCommandProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);
        if ($request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'), ['t.email', 't.ipAddress']);
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            [],
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $spammers = $this->getSpammerQueryProcessor()->getBySearchQuery($search);
        $total = $this->getSpammerQueryProcessor()->countBySearchQuery($search);

        return $this->render('crm/administrate/spammers.html.twig', [
            'spammers' => $spammers,
            'total'   => $total,
            'limit'   => 15,
        ]);
    }

    public function add(Request $request): Response
    {
        $this->getSpammerCommandProcessor()->add(
            new SpammerCommand(
                $request->request->get('email'),
                $request->request->get('ipAddress'),
                $this->getUser()->getId(),
                $request->request->has('isStrict')
            )
        );

        $this->addFlash('success', 'Successfully added to spam list');

        return $this->redirectToRoute('spammer_list');
    }

    public function remove(int $id): Response
    {
        $this->getSpammerCommandProcessor()->remove($id);

        $this->addFlash('success', 'Spammer was removed from list');

        return $this->redirectToRoute('spammer_list');
    }
}
