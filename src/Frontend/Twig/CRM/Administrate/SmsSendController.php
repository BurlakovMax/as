<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Administrate;

use App\Frontend\Processors\SmsCommandService;
use LazyLemurs\Structures\PhoneNumber;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class SmsSendController extends AbstractController
{
    use SmsCommandService;

    public function sendSms(Request $request): Response
    {
        if ('POST' === $request->getMethod()) {
            $this->getSmsCommandService()->sendSync(
                Uuid::uuid4(),
                new PhoneNumber($request->request->get('phone')),
                $request->request->get('text')
            );

            $this->addFlash('success', 'Sms message was successfully send');

            return $this->redirectToRoute('manual_sms');
        }

        return $this->render('crm/administrate/manual-sms-form.html.twig');
    }
}
