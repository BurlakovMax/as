<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Administrate;

use App\Frontend\Processors\EmailCommandProcessor;

use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class EmailSendController extends AbstractController
{
    use EmailCommandProcessor;

    public function sendEmail(Request $request): Response
    {
        if ('POST' === $request->getMethod()) {

            $this->getEmailCommandProcessor()->sendSyncByRawValues(
                Uuid::uuid4(),
                new Email($request->request->get('emailSender')),
                new Email($request->request->get('emailTo')),
                $request->request->get('body'),
                $request->request->get('subject'),
                '' !== $request->request->get('replyTo') ? $request->request->get('replyTo') : null
            );

            $this->addFlash('success', 'Email was successfully send');

            return $this->render('crm/administrate/manual-email-form.html.twig');
        }

        return $this->render('crm/administrate/manual-email-form.html.twig');
    }
}
