<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM;

use LazyLemurs\Commander\Commander;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class CommandFiller extends AbstractController
{
    private function getCommander(): Commander
    {
        return $this->container->get(Commander::class);
    }

    /**
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    final protected function fill(object $command): void
    {
        $this->getCommander()->fill($command);
    }
}
