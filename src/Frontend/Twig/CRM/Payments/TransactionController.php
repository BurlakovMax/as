<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Payments;

use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\PaymentCommandProcessor;
use App\Frontend\Processors\TransactionQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class TransactionController extends AbstractController
{
    use TransactionQueryProcessor;
    use PaymentCommandProcessor;

    public function index(Request $request): Response
    {
        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [],
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'desc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $transactions = $this->getTransactionQueryProcessor()->getBySearchQuery($search);
        $total = $this->getTransactionQueryProcessor()->countBySearchQuery($search);

        return $this->render('crm/payments/transactions.html.twig', [
            'transactions' => $transactions,
            'total'    => $total,
            'limit'   => 15,
        ]);
    }

    public function refund(Request $request): RedirectResponse
    {
        $this->getPaymentCommandProcessor()->refund(
            $request->query->getInt('paymentId'),
            $request->query->getInt('transactionId'),
            $request->query->getInt('amount'),
            $this->getUser()->getId(),
            null
        );

        $this->addFlash('success', 'Refund was send to SecureOnPay');

        return $this->redirectToRoute('transaction_list');
    }
}
