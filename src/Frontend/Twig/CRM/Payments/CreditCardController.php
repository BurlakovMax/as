<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Payments;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\BannedCreditCardCommandProcessor;
use App\Frontend\Processors\BannedCreditCartQueryProcessor;
use App\Frontend\Processors\CreditCardQueryProcessor;
use App\Payments\Application\BannedCreditCardCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreditCardController extends AbstractController
{
    use CreditCardQueryProcessor;
    use BannedCreditCardCommandProcessor;
    use BannedCreditCartQueryProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);

        $filters = [];

        if ($request->query->has('cc_id') && !empty($request->query->getInt('cc_id'))) {
            $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('cc_id'));
        }

        if ($request->query->has('accountId') && !empty($request->query->getInt('accountId'))) {
            $filters[] = FilterQuery::createEqFilter('t.accountId', $request->query->getInt('accountId'));
        }

        if ($request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'),
                [
                    't.cardholderName.firstName',
                    't.cardholderName.lastName',
                    't.city',
                ]
            );
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $cards = $this->getCreditCardQueryProcessor()->getBySearchQuery($search);
        $total = $this->getCreditCardQueryProcessor()->countBySearchQuery($search);

        return $this->render(
            'crm/payments/creditCards.html.twig',
            [
                'cards' => $cards,
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }

    public function bannedCardsList(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);

        $filters = [];

        if ($request->query->has('cc_id') && !empty($request->query->getInt('cc_id'))) {
            $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('cc_id'));
        }

        if ($request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'),
                [
                    't.city',
                    't.cardholderName.firstName',
                    't.cardholderName.lastName',
                ]
            );
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $cards = $this->getBannedCreditCartQueryProcessor()->getListBySearchQuery($search);
        $total = $this->getBannedCreditCartQueryProcessor()->getCountBySearchQuery($search);

        return $this->render(
            'crm/payments/bannedCreditCards.html.twig',
            [
                'cards' => $cards,
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }

    public function ban(int $id, Request $request): Response
    {
        $card = $this->getCreditCardQueryProcessor()->get($id);

        if ('POST' === $request->getMethod()) {
            $this->getBannedCreditCardCommandProcessor()->add(
                new BannedCreditCardCommand(
                    $card->getCardNumber(),
                    $card->getExpiryMonth(),
                    $card->getExpiryYear(),
                    $card->getCvc2(),
                    $card->getCardholderFirstname(),
                    $card->getCardholderLastname(),
                    $card->getAddress(),
                    $card->getZipcode(),
                    $card->getCity(),
                    $card->getState(),
                    $card->getCountry(),
                    $request->request->get('banReason'),
                    $card->getTotal() ?? 0
                )
            );

            $this->addFlash('danger', 'Card ' . $card->getCardNumber() . ' was banned!');

            return $this->redirectToRoute('creditCards_list');
        }

        return $this->render('crm/payments/ban.html.twig', [
            'card' => $card,
        ]);
    }
}
