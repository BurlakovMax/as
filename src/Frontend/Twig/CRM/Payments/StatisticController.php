<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Payments;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\PaymentItemQueryProcessor;
use App\Payments\Application\StatisticData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class StatisticController extends AbstractController
{
    use PaymentItemQueryProcessor;
    use CityQueryProcessor;

    public function index(Request $request): Response
    {
        $filters = [];
        $response = [];

        if ($request->query->has('from') &&
            $request->query->get('from') !== '' &&
            $request->query->has('to') &&
            $request->query->get('to') !== '') {

                $filters[] = new FilterQuery(
                    't.createdAt',
                    ComparisonType::gte(),
                    (new \DateTimeImmutable($request->query->get('from')))->getTimestamp()
                );
                $filters[] = new FilterQuery(
                    't.createdAt',
                    ComparisonType::lte(),
                    (new \DateTimeImmutable($request->query->get('to')))->getTimestamp()
                );

            $search = new SearchQuery(
                new FuzzySearchQuery(null, []),
                $filters,
                [
                    new OrderQuery(
                        $request->get('property') ?? 't.createdAt',
                        Ordering::getValueOf($request->get('ordering') ?? 'desc')
                    ),
                ],
                null
            );

            $statistics = $this->getPaymentItemQueryProcessor()->getBySearchQuery($search);

            array_map(
                function (int $cityId, StatisticData $statisticData) use (&$response) {
                    return $response[$this->getCityQueryProcessor()->getById($cityId)->getName()] = $statisticData;
                },
                array_keys($statistics), $statistics
            );
        }

        return $this->render('crm/payments/statistics.html.twig', [
            'statistics' => $response,
        ]);
    }
}