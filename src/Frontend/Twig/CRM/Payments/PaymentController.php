<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Payments;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\PaymentQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PaymentController extends AbstractController
{
    use PaymentQueryProcessor;

    public function index(Request $request): Response
    {
        $filters = [];
        if ($request->query->has('accountId') && $request->query->getInt('accountId') !== 0) {
            $filters[] = FilterQuery::createEqFilter('t.accountId', $request->query->getInt('accountId'));
        }

        if ($request->query->has('creditCardId') && $request->query->getInt('creditCardId') !== 0) {
            $filters[] = FilterQuery::createEqFilter('t.cardId', $request->query->getInt('creditCardId'));
        }

        if ($request->query->has('createdAt') && $request->query->get('createdAt') !== '') {
            $filters[] = new FilterQuery('t.createdAt', ComparisonType::gte(), $request->query->get('createdAt'));
        }
        // TODO: add filters search

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $payments = $this->getPaymentQueryProcessor()->getBySearchQuery($search);
        $total = $this->getPaymentQueryProcessor()->countBySearchQuery($search);

        // TODO: create full lists for options
        $status_options = [
          'A' => 'Accepted',
          'D' => 'Denied',
        ];
        $type_options = [
          'yes' => 'Yes',
          'no' => 'No',
        ];

        return $this->render('crm/payments/payments.html.twig', [
            'payments' => $payments,
            'type_options' => $type_options,
            'status_options' => $status_options,
            'total'    => $total,
            'limit'   => 15,
        ]);
    }
}
