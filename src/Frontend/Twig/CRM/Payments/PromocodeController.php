<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Payments;

use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\PromocodeCommandProcessor;
use App\Frontend\Processors\PromocodeQueryProcessor;
use App\Payments\Application\CreatePromocodeCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PromocodeController extends AbstractController
{
    use PromocodeQueryProcessor;
    use PromocodeCommandProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);

        $filters = [];

        if ($request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'),
                [
                    't.code',
                ]
            );
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $promocodes = $this->getPromocodeQueryProcessor()->getListBySearchQuery($search);
        $total = $this->getPromocodeQueryProcessor()->countBySearchQuery($search);

        return $this->render('crm/payments/promocodes.html.twig', [
            'promocodes' => $promocodes,
            'total'   => $total,
            'limit'   => 15,
        ]);
    }

    public function add(Request $request): Response
    {
        if ('POST' === $request->getMethod()) {
            $this->getPromocodeCommandProcessor()->create(
                new CreatePromocodeCommand(
                    $request->request->get('code'),
                    $request->request->get('section'),
                    $request->request->getInt('codeCount') ?? 1,
                    $request->request->get('autoRenewal') ?? false,
                    $request->request->get('accountLimit') ?? 0,
                    $request->request->get('escortLocationLimit') ?? 0,
                    $request->request->get('autoRepost') ?? 0,
                    $request->request->get('recurring') ?? 0,
                    $this->getUser()->getId(),
                    $request->request->getBoolean('freeEscort') ?? false,
                    $request->request->get('discountAmount') ?? 0,
                    $request->request->getInt('discountPercentage') ?? 0,
                    $request->request->get('classifiedUp') ?? 0,
                    $request->request->getInt('fixedPrice') ?? 0,
                    false,
                    $request->request->get('isTracking') ?? false,
                    $request->request->get('isCityThumbnail') ?? false,
                    $request->request->get('isSideSponsor') ?? false,
                    $request->request->get('sponsor') ?? false,
                    $request->request->get('codeWorkingDays') ?? 30,
                    $request->request->get('escortType') ?? 0
                )
            );

            return $this->redirectToRoute('promocode_list');
        }

        return $this->render('crm/payments/popups/promocodeAdd.html.twig');
    }

    public function delete(int $id): RedirectResponse
    {
        $this->getPromocodeCommandProcessor()->delete($id, $this->getUser()->getId());

        return $this->redirectToRoute('promocode_list');
    }
}
