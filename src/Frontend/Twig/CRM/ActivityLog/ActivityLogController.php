<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\ActivityLog;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\ActivityLogQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ActivityLogController extends AbstractController
{
    use ActivityLogQueryProcessor;

    public function index(Request $request): Response
    {
        $filters = [];
        if ($request->query->has('initiator') && $request->query->getInt('initiator') !== 0) {
            $filters[] = FilterQuery::createEqFilter('t.initiator', $request->query->getInt('initiator'));
        }

        if ($request->query->has('type') && $request->query->get('type') !== '') {
            $filters[] = FilterQuery::createEqFilter('t.type', $request->query->get('type'));
        }

        if ($request->query->has('item') && $request->query->getInt('item') !== 0) {
            $filters[] = FilterQuery::createEqFilter('t.item', $request->query->getInt('item'));
        }

        if ($request->query->has('action') && $request->query->get('action') !== '') {
            $filters[] = FilterQuery::createEqFilter('t.action', $request->query->get('action'));
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $activityLogs = $this->getActivityLogQueryProcessor()->getBySearchQuery($search);
        $total = $this->getActivityLogQueryProcessor()->countBySearchQuery($search);

        return $this->render('crm/activity_log/activity-logs.html.twig', [
            'activity_logs' => $activityLogs,
            'total' => $total,
            'limit' => 15,
        ]);
    }
}