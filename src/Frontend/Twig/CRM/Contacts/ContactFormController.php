<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Contacts;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\ContactQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

final class ContactFormController extends AbstractController
{
    use ContactQueryProcessor;

    public function index(): Response
    {
        $searchQuery = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('t.read', false),
            ],
            [],
            LimitationQueryImmutable::create(null, null)
        );

        return $this->render('crm/contacts/contacts.html.twig', [
           'contacts' => $this->getContactQueryProcessor()->getListBySearchQuery($searchQuery),
        ]);
    }

    public function show(int $id): Response
    {
        return $this->render('crm/contacts/contact-full-view.html.twig', [
           'contact' => $this->getContactQueryProcessor()->getById($id),
        ]);
    }
}
