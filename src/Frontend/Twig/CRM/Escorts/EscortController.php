<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Escorts;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Escorts\Application\BodyType;
use App\Escorts\Application\CupSize;
use App\Escorts\Application\EscortSideData;
use App\Escorts\Application\EscortSponsorData;
use App\Escorts\Application\EscortStickyData;
use App\Escorts\Application\EscortTypeConverter;
use App\Escorts\Application\Ethnicity;
use App\Escorts\Application\EyeColor;
use App\Escorts\Application\HairColor;
use App\Escorts\Application\Kitty;
use App\Escorts\Application\StatusConverter;
use App\Escorts\Application\UpdateEscortAdminCommand;
use App\Escorts\Application\UpdateEscortCommand;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortCommandProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortSideQueryProcessor;
use App\Frontend\Processors\EscortSponsorQueryProcessor;
use App\Frontend\Processors\EscortStickyQueryProcessor;
use App\Frontend\Processors\ImageUploadProcessor;
use App\Frontend\Processors\PaymentQueryProcessor;
use App\Frontend\Twig\CRM\CommandFiller;
use App\Locations\Application\LocationTreeBuilder;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class EscortController extends CommandFiller
{
    use EscortQueryProcessor;
    use EscortSideQueryProcessor;
    use EscortStickyQueryProcessor;
    use EscortSponsorQueryProcessor;
    use PaymentQueryProcessor;
    use CityQueryProcessor;
    use EscortCommandProcessor;
    use ImageUploadProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);

        if ($request->query->has('search') || $request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery(
                $request->get('search'),
                [
                    't.phone',
                    't.email',
                    't.title',
                    't.content',
                ]
            );
        }

        $filters = [];
        if ($request->get('escortId')) {
          $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('escortId'));
        }

        if ($request->get('status')) {
            $filters[] = FilterQuery::createEqFilter('t.status', $request->query->getInt('status'));
        }

        if ($request->get('type')) {
            $filters[] = FilterQuery::createEqFilter('t.type', $request->query->getInt('type'));
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $escorts = $this->getEscortQueryProcessor()->getBySearchQuery($search);
        $total   = $this->getEscortQueryProcessor()->countBySearchQuery($search);

        $statuses = [];
        foreach (StatusConverter::listOfStatuses() as $status) {
            foreach ($status as $k => $v) {
                $statuses[$k] = $status[$k]['name'];
            }
        }

        $types = [];
        foreach (EscortTypeConverter::listOfTypes() as $type) {
            foreach ($type as $k => $v) {
                $types[$k] = $type[$k]['name'];
            }
        }

        return $this->render(
            'crm/escorts/escorts.html.twig', [
            'escorts' => $escorts,
            'type_options' => $types,
            'status_options' => $statuses,
            'total'   => $total,
            'limit'   => 15,
        ]);
    }

    public function delete(int $id): RedirectResponse
    {
        $this->getEscortCommandProcessor()->removeEscort($id);
        $this->addFlash('success', 'Escort ID: ' . $id . ' was mark for deletion');

        return $this->redirectToRoute('escort_list');
    }

    public function update(int $id, Request $request): Response
    {
        if ('POST' === $request->getMethod()) {
            $command = new UpdateEscortAdminCommand($id);
            $this->fill($command);

            $this->getEscortCommandProcessor()->adminUpdate($command);

            $this->addFlash('success', 'Escort ID: ' . $id . ' was successfully updated!');

            $this->redirectToRoute('escort_edit', ['id' => $id]);
        }

        $sponsorSearch = new SearchQuery(
            new FuzzySearchQuery(null, []),
            array_merge(
                [
                    FilterQuery::createEqFilter('escortId', $id),
                ],
            ),
            [],
            LimitationQueryImmutable::create(null, null)
        );

        $cityThumbnail = $this->getEscortSponsorQueryProcessor()->getBySearchQuery($sponsorSearch);
        $sideSponsor = $this->getEscortSideQueryProcessor()->getBySearchQuery($sponsorSearch);
        $stickies = $this->getEscortStickyQueryProcessor()->getByEscortId($id);

        return $this->render('crm/escorts/escort-edit.html.twig', [
            'escort' => $this->getEscortQueryProcessor()->getById($id),
            'typeList' => EscortTypeConverter::listOfTypes(),
            'statuses' => StatusConverter::listOfStatuses(),
            'cityThumbnail' => !empty($cityThumbnail) ? $this->getSponsorsWithLocation($cityThumbnail) : [],
            'sideSponsor' => !empty($sideSponsor) ? $this->getSponsorsWithLocation($sideSponsor) : [],
            'stickies' => !empty($stickies) ? $this->getSponsorsWithLocation($stickies) : [],
            'locationsTree' => $this->getLocationTreeBuilder()->getLocationTree(true),
        ]);
    }

    public function detailsEdit(int $id, Request $request): Response
    {
        $escort = $this->getEscortQueryProcessor()->getById($id);

        if ('POST' === $request->getMethod()) {
            $command = new UpdateEscortCommand($id, $escort->getAccountId());
            $this->fill($command);

            $images = [];

            if ($request->files->has('images') && !empty($request->files->get('images'))) {
                foreach ($request->files->get('images') as $image) {
                    $uploadedImage = $this->getImageUploadProcessor()->save(
                        (Uuid::uuid4())->toString(),
                        $image
                    );
                    $images[] = $uploadedImage->getName();
                }
            }

            $command->attachImages($images);

            $this->getEscortCommandProcessor()->update($command);

            $this->addFlash('success', 'Escort ID: ' . $id . ' was successfully updated!');

            return $this->redirectToRoute('escort_edit_details', ['id' => $escort->getId()]);
        }

        return $this->render('crm/escorts/escort-edit-step3.html.twig', [
            'escort' => $escort,
            'ethnicity' => Ethnicity::getRawListWithValue(),
            'eyeColor' => EyeColor::getRawListWithValue(),
            'kitty' => Kitty::getRawListWithValue(),
            'hairColor' => HairColor::getRawListWithValue(),
            'cupSize' => CupSize::getRawListWithValue(),
            'bodyType' => BodyType::getRawListWithValue(),
        ]);
    }

    public function changeLocation(int $id, Request $request): RedirectResponse
    {
        $this->getEscortCommandProcessor()->changeLocation(
            $id,
            $request->request->getInt('fromLocation'),
            $request->request->getInt('toLocation')
        );

        $this->addFlash('success', 'Escort ID: ' . $id . ' location was changed');

        return $this->redirectToRoute('escort_edit', ['id' => $id]);
    }

    /**
     * @param EscortStickyData[]|EscortSideData[]|EscortSponsorData[] $escortSponsorsData
     * @return EscortSponsorsWithLocation[]
     */
    private function getSponsorsWithLocation(array $escortSponsorsData): array
    {
        return array_map(function ($escort): EscortSponsorsWithLocation {
            return new EscortSponsorsWithLocation($escort, $this->getCityQueryProcessor()->getById($escort->getLocationId()));
        }, $escortSponsorsData);
    }

    private function getLocationTreeBuilder(): LocationTreeBuilder
    {
        return $this->container->get(LocationTreeBuilder::class);
    }
}
