<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Escorts;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\PhoneBlacklistCommandProcessor;
use App\Frontend\Processors\PhoneBlacklistQueryProcessor;
use LazyLemurs\Structures\PhoneNumber;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PhoneBlacklistController extends AbstractController
{
    use PhoneBlacklistQueryProcessor;
    use PhoneBlacklistCommandProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);

        $filters = [];

        if ($request->query->has('id') && !empty($request->query->getInt('id'))) {
            $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('id'));
        }

        if ($request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'),
                [
                    't.phone',
                ]
            );
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $phones = $this->getPhoneBlacklistQueryProcessor()->getListBySearchQuery($search);
        $total = $this->getPhoneBlacklistQueryProcessor()->countBySearchQuery($search);

        return $this->render(
            'crm/escorts/phoneBlacklist.html.twig',
            [
                'phones' => $phones,
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }

    public function add(Request $request): Response
    {
        if ('POST' === $request->getMethod()) {
            $this->getPhoneBlacklistCommandProcessor()->add(
                new PhoneNumber($request->request->get('phone')),
                $request->request->get('reason'),
                $this->getUser()->getId()
            );

            $this->addFlash('success', 'Phone number ' . $request->request->get('phone') . ' added to black list');

            return $this->redirectToRoute('phone_black_list');
        }

        return $this->render('crm/escorts/popups/add-phone-to-blacklist.html.twig');
    }

    public function remove(int $id): RedirectResponse
    {
        $this->getPhoneBlacklistCommandProcessor()->remove($id);

        $this->addFlash('success', 'Phone was removed from black list');

        return $this->redirectToRoute('phone_black_list');
    }
}
