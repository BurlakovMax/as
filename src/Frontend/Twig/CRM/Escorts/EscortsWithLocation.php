<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Escorts;

use App\Escorts\Application\EscortData;
use App\Escorts\Application\EscortSideData;
use App\Escorts\Application\EscortStickyData;
use App\Locations\Application\CityData;

final class EscortsWithLocation
{
    /**
     * @var EscortData|EscortSideData|EscortStickyData
     */
    private object $escort;

    private CityData $city;

    public function __construct(object $escortData, CityData $cityData)
    {
        $this->escort = $escortData;
        $this->city = $cityData;
    }

    /**
     * @return EscortData|EscortSideData|EscortStickyData
     */
    public function getEscort(): object
    {
        return $this->escort;
    }

    public function getCity(): CityData
    {
        return $this->city;
    }
}
