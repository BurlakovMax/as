<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Escorts;

use App\Frontend\Processors\EscortClickStatisticQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class EscortClickStatisticController extends AbstractController
{
    use EscortClickStatisticQueryProcessor;

    public function index(Request $request): Response
    {
        if ($request->query->has('escortId')) {

            $statistics = $this->getEscortClickStatisticQueryProcessor()->getByEscortId(
                $request->query->getInt('escortId'),
                new \DateTimeImmutable($request->query->get('dataFrom')),
                new \DateTimeImmutable($request->query->get('dataTo'))
            );

            return $this->render('crm/escorts/escort-click-statistic.html.twig', [
                'statistics' => $statistics,
            ]);
        }

        return $this->render('crm/escorts/escort-click-statistic.html.twig', [
            'statistics' => [],
        ]);
    }
}
