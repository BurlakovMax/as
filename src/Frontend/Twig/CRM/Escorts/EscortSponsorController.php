<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Escorts;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Escorts\Application\EscortTypeConverter;
use App\Escorts\Application\StatusConverter;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortSponsorQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class EscortSponsorController extends AbstractController
{
    use EscortSponsorQueryProcessor;
    use CityQueryProcessor;

    public function index(Request $request): Response
    {
        $filters = [];
        if ($request->query->get('escortId')) {
            $filters[] = FilterQuery::createEqFilter('t.escortId', $request->query->getInt('escortId'));
        }

        if ($request->query->has('status')) {
            $filters[] = FilterQuery::createEqFilter('t.status', $request->query->getInt('status'));
        }

        if ($request->query->get('type')) {
            $filters[] = FilterQuery::createEqFilter('t.type', $request->query->getInt('type'));
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $escortSponsors = $this->getEscortSponsorQueryProcessor()->getBySearchQuery($search);
        $total = $this->getEscortSponsorQueryProcessor()->countBySearchQuery($search);

        $statuses = [];
        foreach (StatusConverter::listOfStatuses() as $status) {
            foreach ($status as $k => $v) {
                $statuses[$k] = $status[$k]['name'];
            }
        }

        $types = [];
        foreach (EscortTypeConverter::listOfTypes() as $type) {
            foreach ($type as $k => $v) {
                $types[$k] = $type[$k]['name'];
            }
        }

        return $this->render(
            'crm/escorts/escort-sponsors.html.twig',
            [
                'escort_sponsors' => array_map(function ($escort): EscortsWithLocation {
                    return new EscortsWithLocation($escort, $this->getCityQueryProcessor()->getById($escort->getLocationId()));
                }, $escortSponsors),
                'type_options' => $types,
                'status_options' => $statuses,
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }
}
