<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Escorts;

use App\Escorts\Application\EscortSideData;
use App\Escorts\Application\EscortSponsorData;
use App\Escorts\Application\EscortStickyData;
use App\Locations\Application\CityData;

final class EscortSponsorsWithLocation
{
    /**
     * @var EscortStickyData|EscortSideData|EscortSponsorData
     */
    private object $sponsor;

    private CityData $city;

    public function __construct(object $sponsor, CityData $cityData)
    {
        $this->sponsor = $sponsor;
        $this->city = $cityData;
    }

    /**
     * @return  EscortStickyData|EscortSideData|EscortSponsorData
     */
    public function getSponsor(): object
    {
        return $this->sponsor;
    }

    public function getCity(): CityData
    {
        return $this->city;
    }
}
