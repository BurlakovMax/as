<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Escorts;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\PhoneWhitelistQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PhoneWhitelistController extends AbstractController
{
    use PhoneWhitelistQueryProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);

        $filters = [];

        if ($request->query->has('id') && !empty($request->query->getInt('id'))) {
            $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('id'));
        }

        if ($request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'),
                [
                    't.phone',
                ]
            );
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $phones = $this->getPhoneWhitelistQueryProcessor()->getListBySearchQuery($search);
        $total = $this->getPhoneWhitelistQueryProcessor()->countBySearchQuery($search);

        return $this->render(
            'crm/escorts/phoneWhitelist.html.twig',
            [
                'phones' => $phones,
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }
}
