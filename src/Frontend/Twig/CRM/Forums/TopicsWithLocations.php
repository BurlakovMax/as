<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Forums;

use App\Forums\Application\TopicData;
use App\Locations\Application\CityData;

final class TopicsWithLocations
{
    private TopicData $topic;

    private CityData $city;

    public function __construct(TopicData $topicData, CityData $cityData)
    {
        $this->topic = $topicData;
        $this->city = $cityData;
    }

    public function getTopic(): TopicData
    {
        return $this->topic;
    }

    public function getCity(): CityData
    {
        return $this->city;
    }
}
