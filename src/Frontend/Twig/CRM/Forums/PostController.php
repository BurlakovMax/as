<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Forums;

use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\PostCommandProcessor;
use App\Frontend\Processors\PostQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PostController extends AbstractController
{
    use PostQueryProcessor;
    use PostCommandProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);
        if ($request->query->has('search') || $request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'), ['t.text']);
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            [],
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $posts = $this->getPostQueryProcessor()->getListBySearchQuery($search);
        $total = $this->getPostQueryProcessor()->countBySearchQuery($search);

        return
            $this->render(
                'crm/forums/posts.html.twig',
                 [
                    'posts' => $posts,
                    'total'  => $total,
                    'limit'  => 15,
                 ]
            )
        ;
    }

    public function delete(int $id): RedirectResponse
    {
        $this->getPostCommandProcessor()->delete($id);

        $this->addFlash('warning', 'Post ID: ' . $id . ' was deleted!');

        return $this->redirectToRoute('post_list');
    }
}
