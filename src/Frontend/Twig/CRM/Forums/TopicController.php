<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Forums;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\PostQueryProcessor;
use App\Frontend\Processors\TopicCommandProcessor;
use App\Frontend\Processors\TopicQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;

final class TopicController extends AbstractController
{
    use TopicQueryProcessor;
    use CityQueryProcessor;
    use TopicCommandProcessor;
    use PostQueryProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);
        if ($request->query->has('search') || $request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'), ['t.title']);
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            $fuzzy,
            [],
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $topics = $this->getTopicQueryProcessor()->getListBySearchQuery($search);
        $total  = $this->getTopicQueryProcessor()->countBySearchQuery($search);

        return
            $this->render(
                'crm/forums/topics.html.twig',
                [
                    'topics' => array_map(function ($topic): TopicsWithLocations {
                        return new TopicsWithLocations($topic, $this->getCityQueryProcessor()->getById($topic->getLocationId()));
                    }, $topics),
                    'total'  => $total,
                    'limit'  => 15,
                ]
            )
        ;
    }

    public function delete(int $id): RedirectResponse
    {
        $posts = $this->getPostQueryProcessor()->getListBySearchQuery(
            new SearchQuery(
                new FuzzySearchQuery(null, []),
                [
                    FilterQuery::createEqFilter('topicId', $id),
                ],
                [],
                LimitationQueryImmutable::create(null, null)
            )
        );

        $this->getTopicCommandProcessor()->delete($id, $posts);

        $this->addFlash('warning', 'Topic ID: ' . $id . ' was deleted with all its posts!');

        return $this->redirectToRoute('topic_list');
    }
}
