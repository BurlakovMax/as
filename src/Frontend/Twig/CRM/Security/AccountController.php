<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Security;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\AccountCommandService;
use App\Frontend\Processors\AccountQueryProcessor;
use App\Frontend\Processors\EmailCommandProcessor;
use App\Frontend\Processors\SessionCommandProcessor;
use App\Frontend\Twig\CRM\CommandFiller;
use App\Security\Application\AccountRoles;
use App\Security\Application\AccountStatus;
use App\Security\Application\AccountUpdateCommand;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

final class AccountController extends CommandFiller
{
    use AccountQueryProcessor;
    use AccountCommandService;
    use EmailCommandProcessor;
    use SessionCommandProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);
        if ($request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery($request->get('search'), ['t.username', 't.email']);
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $filters = [];
        if ($request->get('accountId')) {
            $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('accountId'));
        }

        if ($request->get('status')) {
            switch ($request->get('status')) {
                case AccountStatus::active():
                    $filters[] = FilterQuery::createEqFilter('t.isApproved', true);
                    $filters[] = FilterQuery::createEqFilter('t.deletedOn', null);
                    $filters[] = FilterQuery::createEqFilter('t.isBanned', false);
                    break;
                case AccountStatus::deleted():
                    $filters[] = new FilterQuery('t.deletedOn', ComparisonType::gte(), new \DateTimeImmutable('1990-01-01'));
                    break;
                case AccountStatus::banned():
                    $filters[] = FilterQuery::createEqFilter('t.isBanned', true);
                    $filters[] = FilterQuery::createEqFilter('t.deletedOn', null);
                    break;
            }
        }

        if ($request->get('role')) {
            switch ($request->get('role')) {
                case AccountRoles::user():
                    $filters[] = FilterQuery::createEqFilter('t.advertiser', false);
                    $filters[] = FilterQuery::createEqFilter('t.agency', false);
                    $filters[] = FilterQuery::createEqFilter('t.worker', false);
                    break;
                case AccountRoles::advertiser():
                    $filters[] = FilterQuery::createEqFilter('t.advertiser', true);
                    break;
                case AccountRoles::agency():
                    $filters[] = FilterQuery::createEqFilter('t.agency', true);
                    break;
                case AccountRoles::worker():
                    $filters[] = FilterQuery::createEqFilter('t.worker', true);
                    break;
            }
        }

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $accounts = $this->getAccountQueryProcessor()->getBySearchQuery($search);
        $total = $this->getAccountQueryProcessor()->countBySearchQuery($search);

        $statuses = array_map(
            function (AccountStatus $status) {
                return $status->getRawValue();
            },
            AccountStatus::getValues()
        );

        $roles = array_map(
            function (AccountRoles $roles) {
                return $roles->getRawValue();
            },
            AccountRoles::getValues()
        );

        return $this->render(
            'crm/accounts/accounts.html.twig',
            [
                'accounts' => $accounts,
                'status_options' => $statuses,
                'role_options'   => $roles,
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }

    public function edit(int $id, Request $request): Response
    {
        if ($request->getMethod() === 'POST') {
            $command = new AccountUpdateCommand($id);
            $this->fill($command);

            $this->getAccountCommandService()->update($command);

            $this->addFlash('success', 'Account ID: ' . $id . ' was updated');

            return $this->render('crm/accounts/account_edit.html.twig',[
                'account' => $this->getAccountQueryProcessor()->getAccount($id),
            ]);
        }

        return $this->render('crm/accounts/account_edit.html.twig',[
            'account' => $this->getAccountQueryProcessor()->getAccount($id),
        ]);
    }

    public function view(int $id): Response
    {
        return $this->render('crm/accounts/popups/view.html.twig',[
            'account' => $this->getAccountQueryProcessor()->getAccount($id),
        ]);
    }

    public function ban(int $id, Request $request): Response
    {
        $account = $this->getAccountQueryProcessor()->getAccount($id);

        if ('POST' === $request->getMethod()) {

            if ($account->isBanned()) {
                $this->getAccountCommandService()->ban(
                    $account->getId(),
                    false,
                    null
                );

                $this->addFlash('success', 'Account ID: ' . $id . ' was unbanned');

                return $this->redirectToRoute('account_list');
            }

            $this->getAccountCommandService()->ban(
                $account->getId(),
                true,
                $request->request->get('banReason')
            );

            $this->addFlash('success', 'Account ID: ' . $id . ' was banned');

            return $this->redirectToRoute('account_list');
        }

        return $this->render('crm/accounts/popups/ban.html.twig', [
            'account' => $account,
        ]);
    }

    public function whiteList(int $id): Response
    {
        $account = $this->getAccountQueryProcessor()->getAccount($id);

        if ($account->isWhitelisted()) {
            $this->getAccountCommandService()->whiteList($id, false);

            $this->addFlash('success', 'Account ID: ' . $id . ' was remove from whitelist');

            return $this->redirectToRoute('account_list');
        }

        $this->getAccountCommandService()->whiteList($id, true);

        $this->addFlash('success', 'Account ID: ' . $id . ' was added to whitelist');

        return $this->redirectToRoute('account_list');
    }

    public function delete(int $id, Request $request): Response
    {
        $account = $this->getAccountQueryProcessor()->getAccount($id);

        if ('POST' === $request->getMethod()) {
            $this->getAccountCommandService()->delete($id, $request->request->get('deleteReason'));

            if ($request->request->has('notifyUser')) {
                $this->getEmailCommandProcessor()->sendSyncByRawValues(
                    Uuid::uuid4(),
                    new Email(getenv('EMAIL_SEND_FROM')),
                    $account->getEmail(),
                    $request->request->get('notificationText'),
                    'Your account was deleted',
                    null
                );
            }

            $this->addFlash('success', 'Account ID: ' . $id . ' was marked to be deleted');

            return $this->redirectToRoute('account_list');
        }

        return $this->render('crm/accounts/popups/delete.html.twig', [
            'account' => $account
        ]);
    }

    public function forceLogout(int $id): RedirectResponse
    {
        $this->getAccountCommandService()->forceLogout($id);

        return $this->redirectToRoute('account_list');
    }

    public function impersonate(int $id): RedirectResponse
    {
        $token = $this->getSessionCommandProcessor()->add($id);

        $response = new RedirectResponse($_ENV['APP_URL'] . '/account');
        $response->headers->setCookie(new Cookie('auth', $token, strtotime('tomorrow'), null, null, null, false));

        return $response;
    }
}
