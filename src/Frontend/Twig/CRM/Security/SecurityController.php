<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

final class SecurityController extends AbstractController
{
    public function login(): Response
    {
        return $this->render('crm/login.html.twig');
    }

    public function auth(): Response
    {

    }

    public function logout(): Response
    {

    }
}