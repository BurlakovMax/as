<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Places;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\PlaceReviewCommandProcessor;
use App\Frontend\Processors\PlaceReviewQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PlaceReviewController extends AbstractController
{
    use PlaceReviewQueryProcessor;
    use PlaceReviewCommandProcessor;

    public function index(Request $request): Response
    {
        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;
        $filters = [];

        if ($request->query->has('reviewId') && !empty($request->query->getInt('reviewId'))) {
            $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('reviewId'));
        }

        if ($request->query->has('accountId') && !empty($request->query->getInt('accountId'))) {
            $filters[] = FilterQuery::createEqFilter('t.accountId', $request->query->getInt('accountId'));
        }

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        return $this->render('crm/places/place-reviews.html.twig', [
            'reviews' => $this->getPlaceReviewQueryProcessor()->getBySearchQuery($search),
            'total' => $this->getPlaceReviewQueryProcessor()->countBySearchQuery($search),
            'limit' => 15,
        ]);
    }

    public function delete(int $id): RedirectResponse
    {
        $this->getPlaceReviewCommandProcessor()->delete($id);

        $this->addFlash('warning', 'Review ID: ' . $id . ' was deleted!');

        return $this->redirectToRoute('review_list');
    }
}
