<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Places;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\PlaceAttributeQueryProcessor;
use App\Frontend\Processors\PlaceCommandProcessor;
use App\Frontend\Processors\PlaceQueryProcessor;
use App\Frontend\Processors\PlaceTypeQueryProcessor;
use App\Frontend\Twig\CRM\CommandFiller;
use App\Places\Application\PlaceAttributeCommand;
use App\Places\Application\PlaceStatus;
use App\Places\Application\UpdatePlaceCommand;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PlaceController extends CommandFiller
{
    use PlaceQueryProcessor;
    use CityQueryProcessor;
    use PlaceTypeQueryProcessor;
    use PlaceAttributeQueryProcessor;
    use PlaceCommandProcessor;

    public function index(Request $request): Response
    {
        $fuzzy = new FuzzySearchQuery(null, []);
        if ($request->query->has('search') || $request->get('search') !== '') {
            $fuzzy = new FuzzySearchQuery(
                $request->get('search'),
                [
                    't.name',
                    't.address.address1',
                    't.address.address2',
                ]
            );
        }

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $filters = [];
        if ($request->get('placeId')) {
            $filters[] = FilterQuery::createEqFilter('t.id', $request->query->getInt('placeId'));
        }

        if ($request->get('status')) {
            switch ($request->get('status')) {
              case PlaceStatus::active():
                  $filters[] = new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNull(), null);
                  break;
              case PlaceStatus::deleted():
                  $filters[] = new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNotNull(), null);
                  break;
            }
        }

        if ($request->get('type')) {
            $filters[] = FilterQuery::createEqFilter('t.typeId', $request->query->getInt('type'));
        }

        $search = new SearchQuery(
            $fuzzy,
            $filters,
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $places = $this->getPlaceQueryProcessor()->getBySearchQuery($search);
        $types = $this->getPlaceTypeQueryProcessor()->getAll();
        $total = $this->getPlaceQueryProcessor()->countBySearchQuery($search);

        $statuses = array_map(
            function (PlaceStatus $status) {
                return $status->getRawValue();
            },
            PlaceStatus::getValues()
        );

        $placeTypes = [];
        foreach ($types as $type) {
            $placeTypes[$type->getId()] = $type->getName();
        }

        return $this->render(
            'crm/places/places.html.twig',
            [
                'places' => array_map(function ($place): PlacesWithLocation {
                    return new PlacesWithLocation($place, $this->getCityQueryProcessor()->getById($place->getLocationId()));
                }, $places),
                'types' => $types,
                'type_options' => $placeTypes,
                'status_options' => $statuses,
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }

    public function missingImagesList(Request $request): Response
    {
        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('t.image', null)
            ],
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, 15)
        );

        $places = $this->getPlaceQueryProcessor()->getBySearchQuery($search);
        $total = $this->getPlaceQueryProcessor()->countBySearchQuery($search);

        return $this->render(
            'crm/places/places-missing-image.html.twig',
            [
                'places' => array_map(function ($place): PlacesWithLocation {
                    return new PlacesWithLocation($place, $this->getCityQueryProcessor()->getById($place->getLocationId()));
                }, $places),
                'types' => $this->getPlaceTypeQueryProcessor()->getAll(),
                'total'   => $total,
                'limit'   => 15,
            ]
        );
    }

    public function duplicatesList(Request $request): Response
    {
        $offset = $request->query->has('offset') ? $request->query->getInt('offset') : 0;
        $limit = 15;

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNull(), null),
                new FilterQuery('t.address.address1', ComparisonType::isNotNull(), null),
                new FilterQuery('t.address.address1', ComparisonType::neq(), '')
            ],
            [
                new OrderQuery(
                    $request->get('property') ?? 't.id',
                    Ordering::getValueOf($request->get('ordering') ?? 'asc')
                ),
            ],
            LimitationQueryImmutable::create($offset, $limit)
        );

        $groupingFields = [
            't.name',
            't.locationId',
            't.address.address1',
            't.contact.phone1'
        ];

        $placeDuplicates = $this->getPlaceQueryProcessor()->getDuplicates($search, $groupingFields);
        $placeDuplicatesCount = $this->getPlaceQueryProcessor()->getDuplicatesCount($search, $groupingFields);

        $placeDuplicatesWithCity = array_map(function (array $placeDuplicate) use ($search) {
            $city = $this->getCityQueryProcessor()->getById($placeDuplicate['locationId']);
            $placeDuplicate['cityName'] = $city->getName();

            $placeDuplicate['places'] = $this->getPlacesByNameLocationIdAddressPhone(
                $placeDuplicate['name'],
                $placeDuplicate['locationId'],
                $placeDuplicate['address'],
                $placeDuplicate['phone']
            );

            return $placeDuplicate;
        }, $placeDuplicates);

        return $this->render(
            'crm/places/places-duplicates.html.twig',
            [
                'placeDuplicates' => $placeDuplicatesWithCity,
                'placeDuplicatesCount' => $placeDuplicatesCount,
                'limit' => $limit
            ]
        );
    }

    public function edit(int $id, Request $request): Response
    {
        if ('POST' === $request->getMethod()) {
            $command = new UpdatePlaceCommand($id);
            $this->fill($command);
            $attributes = [];

            if (!empty($request->request->get('placeAttributes'))) {
                foreach ($request->request->get('placeAttributes') as $attributeId => $value) {
                    $attributes[] = new PlaceAttributeCommand($attributeId, $value, $value);
                }
            }

            $command->setPlaceAttributes($attributes);

            $this->getPlaceCommandProcessor()->update($command);

            $this->addFlash('success', 'Place ID: ' . $id . ' was updated');

            return $this->redirectToRoute('place_edit', ['id' => $id]);
        }

        $place = $this->getPlaceQueryProcessor()->getById($id);
        $attributes = $this->getPlaceAttributeQueryProcessor()->getByPlaceId($id);
        $placeTypeAttributes = $this->getPlaceAttributeQueryProcessor()->getByPlaceType($place->getTypeId());

        return $this->render('crm/places/place-edit.html.twig', [
            'place' => $place,
            'types' => $this->getPlaceTypeQueryProcessor()->getAll(),
            'attributes' => $attributes,
            'placeTypeAttributes' => $placeTypeAttributes,
        ]);
    }

    public function forceDelete(int $id): RedirectResponse
    {
        $this->getPlaceCommandProcessor()->forceDelete($id);

        $this->addFlash('success', 'Place ID: ' . $id . ' was FORCE deleted');

        return $this->redirectToRoute('place_list');
    }

    public function delete(int $id): RedirectResponse
    {
        $this->getPlaceCommandProcessor()->delete($id);

        $this->addFlash('success', 'Place ID: ' . $id . ' was mark for deletion');

        return $this->redirectToRoute('place_list');
    }

    public function checkedSuccess(int $id): RedirectResponse
    {
        $this->getPlaceCommandProcessor()->checkedSuccess($id, $this->getUser()->getId());

        $this->addFlash('success', 'Place ID: ' . $id . ' was successfully checked');

        return $this->redirectToRoute('place_list');
    }

    public function skipped(int $id): RedirectResponse
    {
        $this->getPlaceCommandProcessor()->skipped($id, $this->getUser()->getId());

        $this->addFlash('warning', 'Place ID: ' . $id . ' was skipped!');

        return $this->redirectToRoute('place_list');
    }

    public function phoneNotAnswered(int $id): RedirectResponse
    {
        $this->getPlaceCommandProcessor()->phoneNotAnswered($id, $this->getUser()->getId());

        $this->addFlash('warning', 'Place ID: ' . $id . ' phone not answered!!');

        return $this->redirectToRoute('place_list');
    }

    /**
     * @return \App\Places\Application\PlaceData[]
     */
    private function getPlacesByNameLocationIdAddressPhone(
        string $name,
        int $locationId,
        string $address,
        ?string $phone
    ): array
    {
        $searchQuery = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNull(), null),
                FilterQuery::createEqFilter('t.name', $name),
                FilterQuery::createEqFilter('t.locationId', $locationId),
                FilterQuery::createEqFilter('t.address.address1', $address),
                FilterQuery::createEqFilter('t.contact.phone1', $phone),
            ],
            [],
            LimitationQueryImmutable::create(null, null)
        );

        return $this->getPlaceQueryProcessor()->getBySearchQuery($searchQuery);
    }
}
