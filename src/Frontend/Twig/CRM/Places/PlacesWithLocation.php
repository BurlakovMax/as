<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Places;

use App\Locations\Application\CityData;
use App\Places\Application\PlaceData;

final class PlacesWithLocation
{
    private PlaceData $place;

    private CityData $city;

    public function __construct(PlaceData $placeData, CityData $cityData)
    {
        $this->place = $placeData;
        $this->city = $cityData;
    }

    public function getPlace(): PlaceData
    {
        return $this->place;
    }

    public function getCity(): CityData
    {
        return $this->city;
    }
}
