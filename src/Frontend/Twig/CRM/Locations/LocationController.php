<?php

declare(strict_types=1);

namespace App\Frontend\Twig\CRM\Locations;

use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\CountryQueryProcessor;
use App\Frontend\Processors\StateQueryProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class LocationController extends AbstractController
{
    use CountryQueryProcessor;
    use StateQueryProcessor;
    use CityQueryProcessor;

    private $countries;
    private $countriesTotal;

    private $countriesKeys;

    public function country(): Response
    {
        $offset = isset($_REQUEST['offset']) ? (int) $_REQUEST['offset'] : 0;
        $limit = 15;

        $fuzzy = null;
        if (!empty($_REQUEST['search'])) {
          $fuzzy = new FuzzySearchQuery($_REQUEST['search'], ['t.name']);
        }

        $this->getCountries($limit, $offset, $fuzzy);

        return $this->render('crm/locations/country.html.twig', [
           'locations' => $this->countries,
           'total' => $this->countriesTotal,
           'limit' => $limit,
        ]);
    }

    public function state(Request $request): Response
    {
        $this->getCountries();

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;
        $limit = 15;

        $fuzzy = null;
        if ($request->get('search') !== '') {
          $fuzzy = new FuzzySearchQuery($request->get('search'), ['t.name']);
        }

        $states = $this->getStateQueryProcessor()->getListByFilterQuery($this->getSearchQuery($offset, $limit, $fuzzy));
        $total = $this->getStateQueryProcessor()->countByFilterQuery($this->getSearchQuery($offset, $limit, $fuzzy));

        foreach ($states as &$state) {
          $state->countryName = isset($this->countriesKeys[$state->getCountryId()])? $this->countriesKeys[$state->getCountryId()] : '';
        }

        return $this->render('crm/locations/state.html.twig', [
           'locations' => $states,
           'total'   => $total,
           'limit'   => $limit,
        ]);
    }

    public function city(Request $request): Response
    {

        $offset = $request->query->has('offset') ? $request->query->getInt('offset'): 0;
        $limit = 15;

        $fuzzy = null;
        if ($request->get('search') !== '') {
          $fuzzy = new FuzzySearchQuery($request->get('search'), ['t.name']);
        }

        $cities = $this->getCityQueryProcessor()->getListByFilterQuery($this->getSearchQuery($offset, $limit, $fuzzy));
        $total = $this->getCityQueryProcessor()->countByFilterQuery($this->getSearchQuery($offset, $limit, $fuzzy));

        return $this->render('crm/locations/city.html.twig', [
           'locations' => $cities,
           'total'   => $total,
           'limit'   => $limit,
        ]);
    }


    private function getCountries($limit = null, $offset = null, $fuzzy = null): array
    {

      $this->countries = $this->getCountryQueryProcessor()->getListByFilterQuery($this->getSearchQuery($offset, $limit, $fuzzy));
      $this->countriesTotal = $this->getCountryQueryProcessor()->countByFilterQuery($this->getSearchQuery($offset, $limit, $fuzzy));

      foreach ($this->countries as $country) {
        $this->countriesKeys[$country->getId()] = $country->getName();
      }

      return $this->countries;
    }

    private function getSearchQuery($offset = 0, $limit = 0, $fuzzy = null): object
    {
      if (empty($fuzzy)) {
        $fuzzy = new FuzzySearchQuery(null, []);
      }

      $searchQuery = new SearchQuery(
        $fuzzy,
        [],
        [],
        LimitationQueryImmutable::create($offset, $limit)
      );

      return $searchQuery;
    }

}
