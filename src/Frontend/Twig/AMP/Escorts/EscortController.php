<?php

declare(strict_types=1);

namespace App\Frontend\Twig\AMP\Escorts;

use App\Escorts\Application\EscortData;
use App\Frontend\Processors\BreadcrumbsBuilder;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortClickStatisticCommandProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Twig\AMP\AMPController;
use App\Locations\Application\CityData;
use Symfony\Component\HttpFoundation\Response;

final class EscortController extends AMPController
{
    use EscortQueryProcessor;
    use EscortClickStatisticCommandProcessor;
    use CityQueryProcessor;
    use BreadcrumbsBuilder;

    public function index(string $stateName, string $cityName, int $id): Response
    {
        $city = $this->getCity($stateName, $cityName);
        $escort = $this->getEscortQueryProcessor()->getById($id);

        $this->getEscortClickStatisticCommandProcessor()->addClick($escort->getId(), $city->getId());

        return $this->render('amp/escorts/escort.html.twig', [
            'title' => $this->getPageTitle('escort', $city, $escort),
            'description' => $this->getPageDescription('escort', $city, $escort),
            'keywords' => $this->getPageKeywords('escort', $city, $escort),
            'breadcrumbs' => $this->getBreadcrumbs($city, $escort),
            'city' => $city,
            'escort' => $escort,
        ]);
    }

    /**
     * @return <string, string>[][]
     */
    private function getBreadcrumbs(CityData $city, EscortData $escort): array
    {
        return $this->getBreadcrumbsBuilder()
            ->getLinksByCityAndEscortId($city, $escort);
    }
}
