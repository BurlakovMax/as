<?php

declare(strict_types=1);

namespace App\Frontend\Twig\AMP\Escorts;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\BreadcrumbsBuilder;
use LitGroup\Enumerable\Enumerable;
use App\Escorts\Application\EscortData;
use App\Escorts\Application\EscortTypeConverter;
use App\Escorts\Application\StatusData;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortStickyQueryProcessor;
use App\Frontend\Processors\NavigationSlidesBuilder;
use App\Frontend\Twig\AMP\AMPController;
use App\Locations\Application\CityData;
use Symfony\Component\HttpFoundation\Response;

final class EscortTypeController extends AMPController
{
    use CityQueryProcessor;
    use NavigationSlidesBuilder;
    use EscortQueryProcessor;
    use EscortStickyQueryProcessor;
    use BreadcrumbsBuilder;

    private const MAX_ESCORTS_COUNT_PER_PAGE = 10;

    public function index(string $stateName, string $cityName, string $escortType): Response
    {
        $city = $this->getCity($stateName, $cityName);
        $escortTypeEnum = EscortTypeConverter::valueToEnum($escortType);
        $typeName = EscortTypeConverter::valueToName($escortTypeEnum->getRawValue());
        $escortSearchQuery = $this->getEscortSearchQuery($city, $escortTypeEnum);
        $typeData = [
          'id' => $escortTypeEnum->getRawValue(),
          'name' => $typeName,
        ];

        return $this->render('amp/escorts/escort-type.html.twig', [
            'title' => $this->getPageTitle('escortType', $city, false, $typeData),
            'description' => $this->getPageDescription('escortType', $city, false, $typeData),
            'keywords' => $this->getPageKeywords('escortType', $city, false, $typeData),
            'breadcrumbs' => $this->getBreadcrumbs($city, $typeName),
            'city' => $city,
            'slides' => $this->getNavigationSlidesBuilder()->getSlides($city),
            'selectedSlideName' => $typeName,
            'escortsTotalCount' => $this->getEscortQueryProcessor()->countBySearchQuery($escortSearchQuery),
            'typeName' => $typeName,
            'typeSlug' => $escortType,
            'ads' => $this->getEscortQueryProcessor()
                ->getEscortAdvertisingByTypeAndLocationId(
                    $escortTypeEnum,
                    $city->getId()
                ),
            'sponsors' => $this->getSponsors($city, $escortTypeEnum),
            'escorts' => $this->getEscortQueryProcessor()->getBySearchQuery($escortSearchQuery),
            'maxEscortsCountPerPage' => self::MAX_ESCORTS_COUNT_PER_PAGE,
        ]);
    }

    /**
     * @return EscortData[]
     */
    private function getSponsors(CityData $city, Enumerable $escortTypeEnum): array
    {
        $escortIds = $this->getEscortStickyQueryProcessor()
            ->getStickyEscortIds($escortTypeEnum->getRawValue(), $city->getId());

        return $this->getEscortQueryProcessor()->getByIds($escortIds);
    }

    private function getEscortSearchQuery(CityData $city, Enumerable $escortTypeEnum): SearchQuery
    {
        return new SearchQuery(
            new FuzzySearchQuery(null, []),
            $this->getEscortSearchFilter($city, $escortTypeEnum),
            [
                new OrderQuery('t.postedAt', Ordering::desc()),
            ],
            LimitationQueryImmutable::create(0, self::MAX_ESCORTS_COUNT_PER_PAGE)
        );
    }

    /**
     * @return FilterQuery[]
     */
    private function getEscortSearchFilter(CityData $city, Enumerable $escortTypeEnum): array
    {
        $filter = [];
        $filter[] = FilterQuery::createEqFilter('locations.locationId', $city->getId());
        $filter[] = FilterQuery::createEqFilter('t.sponsorAttributes.sponsor', false);
        $filter[] = FilterQuery::createEqFilter('t.sponsorAttributes.sponsorMobile', false);
        $filter[] = FilterQuery::createEqFilter('t.status', StatusData::active());
        $filter[] = new FilterQuery('t.deletedAt', ComparisonType::isNull(), null);
        $filter[] = FilterQuery::createEqFilter('type', $escortTypeEnum->getRawValue());

        return $filter;
    }

    /**
     * @return <string, string>[][]
     */
    private function getBreadcrumbs(CityData $city, string $typeName): array
    {
        return $this->getBreadcrumbsBuilder()
            ->getLinksByCityAndTypeName($city, $typeName);
    }
}
