<?php

declare(strict_types=1);

namespace App\Frontend\Twig\AMP\Index;

use App\Frontend\Twig\AMP\AMPController;
use App\Locations\Application\LocationTreeBuilder;
use Symfony\Component\HttpFoundation\Response;

final class IndexController extends AMPController
{
    public function index(): Response
    {

        return $this->render('amp/index/index.html.twig', [
            'title' => $this->getPageTitle('index'),
            'description' => $this->getPageDescription('index'),
            'keywords' => $this->getPageKeywords('index'),
            'locationsTree' => $this->getLocationTreeBuilder()->getLocationTree(true),
        ]);
    }

    private function getLocationTreeBuilder(): LocationTreeBuilder
    {
      return $this->container->get(LocationTreeBuilder::class);
    }
}
