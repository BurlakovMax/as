<?php

declare(strict_types=1);

namespace App\Frontend\Twig\AMP\Locations;

use App\Frontend\Processors\BreadcrumbsBuilder;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\NavigationSlidesBuilder;
use App\Frontend\Processors\QuantityListBuilder;
use App\Frontend\Twig\AMP\AMPController;
use App\Locations\Application\CityData;
use Symfony\Component\HttpFoundation\Response;

final class CityController extends AMPController
{
    use CityQueryProcessor;
    use QuantityListBuilder;
    use NavigationSlidesBuilder;
    use EscortQueryProcessor;
    use BreadcrumbsBuilder;

    private const MAX_SPONSORS_COUNT = 50;

    public function index(string $stateName, string $cityName): Response
    {
        $city = $this->getCity($stateName, $cityName);

        $quantityListBuilder = $this->getQuantityListBuilder();

        $sponsors = $this->getEscortQueryProcessor()
            ->getEscortSponsorsByLocationId(
                $city->getId(),
                self::MAX_SPONSORS_COUNT
            )
        ;

        return $this->render('amp/locations/city.html.twig', [
            'title' => $this->getPageTitle('city', $city),
            'description' => $this->getPageDescription('city', $city),
            'keywords' => $this->getPageKeywords('city', $city),
            'breadcrumbs' => $this->getBreadcrumbs($city),
            'city' => $city,
            'slides' => $this->getNavigationSlidesBuilder()->getSlides($city),
            'quantityList' => [
                'escortTypes' => $quantityListBuilder->getEscortTypes($city),
                'placeTypes' => $quantityListBuilder->getPlaceTypes($city),
                'forums' => $quantityListBuilder->getForums($city),
            ],
            'sponsors' => $sponsors,
        ]);
    }

    /**
     * @return <string, string>[][]
     */
    private function getBreadcrumbs(CityData $city): array
    {
        return $this->getBreadcrumbsBuilder()->getLinksByCityAndTypeName($city, null);
    }
}
