<?php

declare(strict_types=1);

namespace App\Frontend\Twig\AMP;

use App\Locations\Application\CityData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AMPController extends AbstractController
{
    public $titles = [
      1=>'{city} Escorts - {city} Female Escorts - Female Escorts in {city} - {state} Call Girls',
      2=>'{city} Shemale Escorts, Transvestites and Transsexuals in {state}',
      3=>'{city} Male Escorts for Women M4W in {state}',
      4=>'{city} Gay Escorts for Men M4M in {state}',
      5=>'{city} Escort Services and Agencies in {state}',
      6=>'{city} Body Rubs in {state}',
      'city'=>'{city} {country} Escorts, Strip Clubs, Massage Parlors and Sex Shops',
      'escort'=>'{phone} {name} {city}, {country} {type}',
      'place'=>'{title} - {type} - {city} | {phone}',
      'index'=>'Find Escorts, Strip Clubs, Sex Shops | Adult Search Engine',
    ];
  
    public $descriptions = [
      1=>'Find {city} escorts, {city} female escorts, female escorts in {city}, new listings posted daily, including pics, prices, reviews and extra search filters.',
      2=>'Shemale escorts in {city}. We have the hottest shemales, transvestites and transsexual escorts in {city}!',
      3=>'Male for Female escorts in {city}. Browse through listings of hot male escorts who are eager to please women. You can also view pics, prices and reviews.',
      4=>'Male for Male Escorts in {city}. Find gay and bisexual male escorts who are not scared to accept gay for pay offers and erotic services.',
      5=>'A complete list of featured escort agencies in {city}.  Find escort services in {city} and view their prices, pictures and reviews.',
      6=>'Treat yourself to a sensual body rub in {city}. Here you can browse through {city} body rubs listings and view prices, pics and reviews from the top services providers.',
      'city'=>'{city} Female Escorts, {city} TS/TV Shemale Escorts, {city} Erotic Massage Parlors, {city} Strip Clubs, {city} Sex Shops, {city} Body Rubs',
      'placeType'=>'{city} Female Escorts, {city} TS/TV Shemale Escorts, {city} Erotic Massage Parlors, {city} Strip Clubs, {city} Sex Shops, {city} Body Rubs',
      'escort'=>'{title}. {name} in {type}, {phone} {city}, {state}',
      'place'=>'{name} - {title} {address}, {city}, {country}. {type}',
      'index'=>'Find escorts, female escorts, shemale escorts with new listings posted daily, including pics, prices, reviews and extra search filters.',
    ];
  
    public $keywords = [
      1=>'{city} Escorts, {city} Female Escorts, female escorts in {city}, {city} escort search, {city} call girls, {city} {state} escorts, incall, outcall, prices',
      2=>'{city} Shemale Escorts, {city} Transvestites, {city} Transsexuals, Incall, Outcall, Prices, Escort Search',
      3=>'{city} Male for Female escorts, Male for Female escorts in {city}, Incall, Outcall, Prices, Escort Search',
      5=>'{city} Escort Agencies, {city} Female Escorts, Incall, Outcall, Prices, Escort Search',
      6=>'{city} Body Rubs, Erotic Massage in {city}, Incall, Outcall, Prices',
      'city'=>'{city} Female Escorts, {city} TS/TV Shemale Escorts, {city} Erotic Massage Parlors, {city} Strip Clubs, {city} Sex Shops, {city} Body Rubs',
      'placeType'=>'{city} Female Escorts, {city} TS/TV Shemale Escorts, {city} Erotic Massage Parlors, {city} Strip Clubs, {city} Sex Shops, {city} Body Rubs',
      'escort'=>'{phone}, {name}, {city}, {age} years old, {ethnicity}',
      'place'=>'{city} Female Escorts, {city} TS/TV Shemale Escorts, {city} Erotic Massage Parlors, {city} Strip Clubs, {city} Sex Shops, {city} Body Rubs',
      'index'=>'escorts, female escorts, shemale escorts, adult search engine',
    ];
  
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $parameters = array_merge($parameters, [
            'canonical' => $this->getCanonicalUrl(),
        ]);

        return parent::render($view, $parameters, $response);
    }

    public function getPageTitle(string $page, CityData $city = null, $data = false, $type = false): string
    {
      switch ($page) {
        case 'city':
          return str_replace(['{city}', '{country}'], [$city->getName(), $city->getCountryName()], $this->titles[$page]);
        case 'escort':
          return str_replace(['{phone}', '{name}', '{city}', '{country}', '{type}'], [$data->getPhone(), $data->getFirstName(), $city->getName(), $city->getCountryName(), $data->getTYpe()], $this->titles[$page]);
        case 'escortType':
          if (isset($this->titles[$type['id']])) {
            return str_replace(['{city}', '{state}'], [$city->getName(), $city->getStateName()], $this->titles[$type['id']]);
          }
        case 'place':
          return str_replace(['{title}', '{type}', '{city}', '{phone}'], [$data->getName(), $type->getName(), $city->getName(), $data->getPhone1()], $this->titles[$page]);
        case 'placeType':
          return $type->getName().' in '.$city->getName().' and Happy Endings '.$city->getCode();
        default:
          return $this->titles['index'];
      }
    }

    public function getPageDescription(string $page, CityData $city = null, $data = false, $type = false): string
    {
      switch ($page) {
        case 'city':
        case 'placeType':
          return str_replace(['{city}', '{country}'], [$city->getName(), $city->getCountryName()], $this->descriptions[$page]);
        case 'escort':
          return str_replace(['{title}', '{type}', '{name}', '{phone}', '{city}', '{state}'], [$data->getTitle(), $data->getType(), $data->getFirstName(), $data->getPhone(), $city->getName(), $city->getStateName()], $this->descriptions[$page]);
        case 'escortType':
          if (isset($this->descriptions[$type['id']])) {
            return str_replace(['{city}', '{state}'], [$city->getName(), $city->getStateName()], $this->descriptions[$type['id']]);
          }
        case 'place':
          $address = $data->getAddress1();
          if ($addr2 = $data->getAddress2()) {
            $address .= ', '.$addr2;
          }
          return str_replace(['{name}', '{title}', '{address}', '{city}', '{country}', '{type}'], [$data->getName(), '', $address, $city->getName(), $city->getCountryName(), $type->getName()], $this->descriptions[$page]);
        default:
          return $this->descriptions['index'];
      }
    }

    public function getPageKeywords(string $page, CityData $city = null, $data = false, $type = false): string
    {
      switch ($page) {
        case 'city':
        case 'place':
        case 'placeType':
          return str_replace(['{city}', '{country}'], [$city->getName(), $city->getCountryName()], $this->keywords[$page]);
        case 'escort':
          return str_replace(['{phone}', '{name}', '{city}', '{age}', '{ethnicity}'], [$data->getPhone(), $data->getFirstName(), $city->getName(), $data->getAge(), $data->getEthnicity()], $this->keywords[$page]);
        case 'escortType':
        if (isset($this->keywords[$type['id']])) {
          return str_replace(['{city}', '{state}'], [$city->getName(), $city->getStateName()], $this->keywords[$type['id']]);
        }
        default:
          return $this->keywords['index'];
      }
    }

    private function getCanonicalUrl(): string
    {
        /** @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $pathInfoWithoutAmpPrefix = str_replace('/amp', '', $request->getPathInfo());

        return $this->container->getParameter('app.url') . $pathInfoWithoutAmpPrefix;
    }
}
