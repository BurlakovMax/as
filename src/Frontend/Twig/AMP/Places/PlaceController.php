<?php

declare(strict_types=1);

namespace App\Frontend\Twig\AMP\Places;

use App\Frontend\Open\Places\Presentation\Api\Structures\WorkingHours;
use App\Frontend\Processors\AccountQueryProcessor;
use App\Frontend\Processors\BreadcrumbsBuilder;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\PlaceAttributeQueryProcessor;
use App\Frontend\Processors\PlaceQueryProcessor;
use App\Frontend\Processors\PlaceReviewCommentQueryProcessor;
use App\Frontend\Processors\PlaceReviewQueryProcessor;
use App\Frontend\Processors\PlaceTypeQueryProcessor;
use App\Frontend\Twig\AMP\AMPController;
use App\Locations\Application\CityData;
use App\Places\Application\PlaceAttributeData;
use App\Places\Application\PlaceData;
use App\Places\Application\PlaceTypeData;
use Symfony\Component\HttpFoundation\Response;

final class PlaceController extends AMPController
{
    use CityQueryProcessor;
    use PlaceQueryProcessor;
    use PlaceTypeQueryProcessor;
    use PlaceAttributeQueryProcessor;
    use PlaceReviewQueryProcessor;
    use PlaceReviewCommentQueryProcessor;
    use AccountQueryProcessor;
    use BreadcrumbsBuilder;

    public function index(string $stateName, string $cityName, string $placeType, int $id): Response
    {
        $city = $this->getCity($stateName, $cityName);
        $place = $this->getPlaceQueryProcessor()->getById($id);
        $placeType = $this->getPlaceTypeQueryProcessor()->getByUrl($placeType);
        $placeAttributes = $this->getPlaceAttributeQueryProcessor()->getByPlaceId($id);
        $placeReviews = $this->getPlaceReviewQueryProcessor()->getWithAuthorsAndCommentsByPlaceId($id);

        return $this->render('amp/places/place.html.twig', [
            'title' => $this->getPageTitle('place', $city, $place, $placeType),
            'description' => $this->getPageDescription('place', $city, $place, $placeType),
            'keywords' => $this->getPageKeywords('place', $city, $place, $placeType),
            'breadcrumbs' => $this->getBreadcrumbs($city, $place, $placeType),
            'city' => $city,
            'place' => $place,
            'placeType' => $placeType,
            'attributes' => $this->getAttributes($placeAttributes),
            'information' => $this->getInformation($placeAttributes),
            'workingHours' => $this->getWorkingHours($placeAttributes),
            'rates' => $this->getRates($placeAttributes),
            'reviews' => $placeReviews,
        ]);
    }

    /**
     * @return <string, string>[][]
     */
    private function getBreadcrumbs(CityData $city, PlaceData $place, PlaceTypeData $placeType): array
    {
        return $this->getBreadcrumbsBuilder()
            ->getLinksByCityAndPlaceAndPlaceType(
                $city,
                $place,
                $placeType
            );
    }

    /**
     * @return PlaceAttributeData[]
     */
    private function getRates(array $placeAttributes): array
    {
        $rates = [];
        foreach ($placeAttributes as $attribute) {
            if ($attribute->getType() === 'fee' && $attribute->getValue() !== 'NULL') {
                $rates[] = $attribute;
            }
        }

        return $rates;
    }

    /**
     * @return PlaceAttributeData[]
     */
    private function getAttributes(array $placeAttributes): array
    {
        $attributeWithOptions = [];
        foreach ($placeAttributes as $attribute) {
            if ($attribute->getType() === 'enum' && $attribute->getValue() !== 0) {
                $attributeWithOptions[] = $attribute;
            }
        }

        return $attributeWithOptions;
    }

    /**
     * @return PlaceAttributeData[]
     */
    private function getInformation(array $placeAttributes): array
    {
        $information = [];
        foreach ($placeAttributes as $attribute) {
            if ($attribute->getType() === 'boolean' || $attribute->getType() === 'boolean3') {
                $information[] = $attribute;
            }
        }

        return$information;
    }

    /**
     * @return WorkingHours[]
     */
    private function getWorkingHours(array $placeAttributes): array
    {
        $rawWorkingHours = [];
        foreach ($placeAttributes as $attribute) {
            if ($attribute->getType() === 'hours') {
                $rawWorkingHours[$attribute->getName()] = $attribute->getValue();
            }
        }

        $workingHoursByDay = $this->getPlaceAttributeQueryProcessor()->workingHoursByDay($rawWorkingHours);

        $hours = [];
        foreach ($workingHoursByDay as $dayOfWeek => $workingHour) {
            $period = explode(' - ', $workingHour);
            if (count($period) === 2) {
                $hours[] = new WorkingHours($dayOfWeek, $period[0], $period[1]);
                continue;
            }

            $hours[] = new WorkingHours($dayOfWeek);
        }

        return $hours;
    }
}
