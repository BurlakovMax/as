<?php

declare(strict_types=1);

namespace App\Frontend\Twig\AMP\Places;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\Processors\BreadcrumbsBuilder;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\NavigationSlidesBuilder;
use App\Frontend\Processors\PlaceQueryProcessor;
use App\Frontend\Processors\PlaceTypeQueryProcessor;
use App\Frontend\Twig\AMP\AMPController;
use App\Locations\Application\CityData;
use App\Places\Application\ValueOfEditable;
use App\Places\Application\PlaceTypeData;
use Symfony\Component\HttpFoundation\Response;

final class PlaceTypeController extends AMPController
{
    use CityQueryProcessor;
    use NavigationSlidesBuilder;
    use PlaceTypeQueryProcessor;
    use PlaceQueryProcessor;
    use BreadcrumbsBuilder;

    private const MAX_PLACES_COUNT_PER_PAGE = 10;

    public function index(string $stateName, string $cityName, string $placeType): Response
    {
        $city = $this->getCity($stateName, $cityName);
        $placeTypeData = $this->getPlaceTypeQueryProcessor()->getByUrl($placeType);
        $placeSearchQuery = $this->getPlaceSearchQuery($city->getId(), $placeTypeData->getId());

        return $this->render('amp/places/place-type.html.twig', [
            'title' => $this->getPageTitle('placeType', $city, false, $placeTypeData),
            'description' => $this->getPageDescription('placeType', $city, false, $placeTypeData),
            'keywords' => $this->getPageKeywords('placeType', $city, false, $placeTypeData),
            'breadcrumbs' => $this->getBreadcrumbs($city, $placeTypeData),
            'city' => $city,
            'slides' => $this->getNavigationSlidesBuilder()->getSlides($city),
            'selectedSlideName' => $placeTypeData->getName(),
            'placeType' => $placeTypeData,
            'places' => $this->getPlaceQueryProcessor()->getBySearchQuery($placeSearchQuery),
            'placesTotalCount' => $this->getPlaceQueryProcessor()->countBySearchQuery($placeSearchQuery),
            'maxPlacesCountPerPage' => self::MAX_PLACES_COUNT_PER_PAGE,
        ]);
    }

    /**
     * @return <string, string>[][]
     */
    private function getBreadcrumbs(CityData $city, PlaceTypeData $placeType): array
    {
        return $this->getBreadcrumbsBuilder()
            ->getLinksByCityAndTypeName($city, $placeType->getName());
    }

    private function getPlaceSearchQuery(int $cityId, int $placeTypeId): SearchQuery
    {
        $filters = [];
        $filters[] = FilterQuery::createEqFilter('t.typeId', $placeTypeId);
        $filters[] = FilterQuery::createEqFilter('t.locationId', $cityId);
        $filters[] = FilterQuery::createEqFilter('t.editable', ValueOfEditable::enabled());
        $filters[] = new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNull(), null);

        return new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filters,
            [],
            LimitationQueryImmutable::create(0, self::MAX_PLACES_COUNT_PER_PAGE)
        );
    }
}
