<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Escorts\Application\BodyType;
use App\Escorts\Application\CupSize;
use App\Escorts\Application\Ethnicity;
use App\Escorts\Application\EyeColor;
use App\Escorts\Application\HairColor;
use App\Escorts\Application\Kitty;
use App\Escorts\Application\Language;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class OptionListController extends AbstractController
{
    /**
     * @Route("/options/ethnicity", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of options for escort",
     *     tags={"options"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort characteristic options key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="options_list",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEthnicityList(): JsonResponse
    {
        return $this->json(
            Ethnicity::getRawListWithValue(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/options/eye_color", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of options for escort",
     *     tags={"options"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort characteristic options key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="options_list",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEyeColorList(): JsonResponse
    {
        return $this->json(
            EyeColor::getRawListWithValue(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/options/kitty", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of options for escort",
     *     tags={"options"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort characteristic options key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="options_list",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getKittyList(): JsonResponse
    {
        return $this->json(
            Kitty::getRawListWithValue(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/options/cupsize", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of options for escort",
     *     tags={"options"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort characteristic options key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="options_list",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getCupSizeList(): JsonResponse
    {
        return $this->json(
            CupSize::getRawListWithValue(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/options/hair_color", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of options for escort",
     *     tags={"options"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort characteristic options key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="options_list",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getHairColorList(): JsonResponse
    {
        return $this->json(
            HairColor::getRawListWithValue(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/options/build", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of options for escort",
     *     tags={"options"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort characteristic options key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="options_list",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getBuildList(): JsonResponse
    {
        return $this->json(
            BodyType::getRawListWithValue(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/options/languages", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of options for escort",
     *     tags={"options"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort characteristic options key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="options_list",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getLanguageList(): JsonResponse
    {
        return $this->json(
            Language::getRawListWithValue(),
            Response::HTTP_OK
        );
    }
}