<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Escorts\Application\PhoneVerificationCommandProcessor;
use App\Frontend\Processors\PhoneVerificationQueryProcessor;
use LazyLemurs\Structures\PhoneNumber;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PhoneVerificationController extends AbstractController
{
    use PhoneVerificationQueryProcessor;

    /**
     * @Route("/escorts/request_phone_confirmation", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="request escort phone confirmation",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function requestPhoneConfirmation(Request $request): JsonResponse
    {
        $this->getCommandProcessor()->requestPhoneConfirmation(
            new PhoneNumber($request->request->get('phone')),
            $request->request->getInt('id'),
            null
        );

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/confirm_phone", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="confirm escort phone",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function confirmPhone(Request $request): JsonResponse
    {
        $this->getCommandProcessor()->confirmPhone(
            new PhoneNumber($request->request->get('phone', true)),
            (string) $request->request->get('code'),
            $request->request->getInt('id'),
            null
        );

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/is_phone_verified", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Check is phone already confirmed or not",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="True or false"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function isPhoneConfirmed(Request $request): JsonResponse
    {
        return $this->json(
            $this->getPhoneVerificationQueryProcessor()->isPhoneVerified(
                new PhoneNumber($request->query->get('phone'))
            ),
            Response::HTTP_OK
        );
    }

    private function getCommandProcessor(): PhoneVerificationCommandProcessor
    {
        return $this->container->get(PhoneVerificationCommandProcessor::class);
    }
}
