<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Core\Application\CaptchaVerifier;
use App\Core\Application\InvalidCaptcha;
use App\Escorts\Application\EscortCommandProcessor;
use LazyLemurs\Structures\Email;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EmailController extends AbstractController
{
    /**
     * @Route("/emailEscort", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Send email to escort advertiser",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="return nothing"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function emailEscort(Request $request): JsonResponse
    {
        $escortId = $request->request->getInt('escortId');
        $email = new Email($request->request->get('email'));
        $message = $request->request->get('message');
        $verificationCode = $request->request->get('verificationCode');

        if (!$this->getCaptchaVerifier()->verify($verificationCode)) {
            throw new InvalidCaptcha();
        }

        $this->getCommandProcessor()->sendEmail($escortId, $email, $message);

        return
            $this->json(
                null,
                Response::HTTP_NO_CONTENT
            )
        ;
    }

    private function getCommandProcessor(): EscortCommandProcessor
    {
        return $this->container->get(EscortCommandProcessor::class);
    }

    private function getCaptchaVerifier(): CaptchaVerifier
    {
        return $this->container->get(CaptchaVerifier::class);
    }
}
