<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Frontend\Processors\AccountQueryProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortStickyQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortStickyController extends AbstractController
{
    use EscortQueryProcessor;
    use EscortStickyQueryProcessor;
    use AccountQueryProcessor;

    /**
     * @Route("/escorts/escort_sticky_price", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort sticky price",
     *     tags={"payments", "open"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort sticky price",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortStickyPrice::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPrices(Request $request): JsonResponse
    {
        $accountId = null;
        if ($request->query->has('accountId')) {
            $accountId = $request->query->getInt('accountId');
        }

        $prices = $this->getEscortStickyQueryProcessor()->getPrice(
            $request->query->getInt('type'),
            $this->getAccountQueryProcessor()->isAgency($accountId),
            ...$this->getEscortQueryProcessor()->getLocationsByIds(
                ...array_map(fn(string $id): int => (int)$id, $request->query->get('locationIds'))
            )
        );

        return $this->json(
            new ListResponse(count($prices), $prices),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/can_buy_escort_sticky", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Check can buy escort sticky",
     *     tags={"payments", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="LocationId list available or not available buy escort sticky",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\CityAvailable::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function canBuy(Request $request): JsonResponse
    {
        $availables = $this->getEscortStickyQueryProcessor()->canBuy(
            $request->query->getInt('type'),
            ...array_map(fn(string $id): int => (int)$id, $request->query->get('locationIds'))
        );

        return $this->json(
            $availables,
            Response::HTTP_OK
        );
    }
}