<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Frontend\Processors\EscortSideQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortSideController extends AbstractController
{
    use EscortSideQueryProcessor;

    /**
     * @Route("/escorts/escort_side_price", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort side price",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort side price",
     *         @SWG\Schema(
     *             @SWG\Property(property="price", type="string")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPrice(): JsonResponse
    {
        return $this->json(
            (float) $this->getEscortSideQueryProcessor()->getPrice(false),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/can_buy_escort_side", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Check can buy escort side",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="LocationId list available or not available buy escort side",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\CityAvailable::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function canBuy(Request $request): JsonResponse
    {
        $availables = $this->getEscortSideQueryProcessor()->canBuy(
            $request->query->getInt('type'),
            array_map(fn(string $id): int => (int)$id, $request->query->get('locationIds'))
        );

        return $this->json(
            $availables,
            Response::HTTP_OK
        );
    }
}