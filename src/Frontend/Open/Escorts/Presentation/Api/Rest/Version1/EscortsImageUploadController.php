<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Frontend\Processors\ImageUploadProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Ramsey\Uuid\Uuid;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortsImageUploadController extends AbstractController
{
    use ImageUploadProcessor;

    /**
     * @Route("/escorts/image_upload", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="upload image",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Image",
     *         @SWG\Schema(ref=@Model(type=App\Escorts\Application\ImageUploadedData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function upload(Request $request): JsonResponse
    {
        return $this->json(
            $this->getImageUploadProcessor()->save(
                (Uuid::uuid4())->toString(),
                $request->files->get('image')
            ),
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/escorts/image_delete", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="remove image",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Image"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function remove(Request $request): JsonResponse
    {
        $this->getImageUploadProcessor()->remove($request->get('id'));

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }
}