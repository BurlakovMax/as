<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Frontend\Processors\EscortSponsorQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortSponsorController extends AbstractController
{
    use EscortSponsorQueryProcessor;

    /**
     * @Route("/escorts/escort_sponsor_price", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort sponsor price",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort sponsor price",
     *         @SWG\Schema(
     *             @SWG\Property(property="price", type="string")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPrice(): JsonResponse
    {
        return $this->json(
            (float) $this->getEscortSponsorQueryProcessor()->getPrice(false),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/can_buy_escort_sponsor", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Check can buy escort sponsor",
     *     tags={"payments", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="LocationId list available or not available buy escort sponsor",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\CityAvailable::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function canBuy(Request $request): JsonResponse
    {
        $availables = $this->getEscortSponsorQueryProcessor()->canBuy(
            array_map(fn(string $id): int => (int)$id, $request->query->get('locationIds'))
        );

        return $this->json(
            $availables,
            Response::HTTP_OK
        );
    }
}