<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Escorts\Application\EscortData;
use App\Escorts\Application\EscortStickyData;
use App\Escorts\Application\EscortTypeConverter;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortStickyQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class SponsorController extends AbstractController
{
    use EscortQueryProcessor;
    use EscortStickyQueryProcessor;

    /**
     * @Route("/getEscortSponsorsByLocationId", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get Escort Sponsors By LocationId",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getEscortSponsorsByLocationId(Request $request): JsonResponse
    {
        $sponsors = $this->getEscortQueryProcessor()->getEscortSponsorsByLocationId(
            $request->query->getInt('locationId'),
            $request->query->getInt('limit')
        );

        return $this->json(
            new ListResponse(count($sponsors), $sponsors),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/cities/{id}/escort_types/{type}/sponsors", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get Escort Sponsors By City Id",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getAll(int $id, string $type): JsonResponse
    {
        $escortIds = $this->getEscortStickyQueryProcessor()->getStickyEscortIds(
            EscortTypeConverter::valueToEnum($type)->getRawValue(),
            $id
        );

        $sponsors = $this->getEscortQueryProcessor()->getByIds($escortIds);

        return $this->json(
            new ListResponse(count($sponsors), $sponsors),
            Response::HTTP_OK
        );
    }
}
