<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Frontend\Processors\VideoUploadProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Ramsey\Uuid\Uuid;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortsVideoUploadController extends AbstractController
{
    use VideoUploadProcessor;

    /**
     * @Route("/escorts/video_upload", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="upload video",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Video",
     *         @SWG\Schema(ref=@Model(type=App\Escorts\Application\VideoUploadData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Exception
     */
    public function uploadVideo(Request $request): JsonResponse
    {
        return $this->json(
            $this->getVideoUploadProcessor()->save(
                (Uuid::uuid4())->toString(),
                $request->files->get('video')
            ),
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/escorts/video_delete", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="remove video",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Video"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function remove(Request $request): JsonResponse
    {
        $this->getVideoUploadProcessor()->remove($request->get('id'));

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }
}