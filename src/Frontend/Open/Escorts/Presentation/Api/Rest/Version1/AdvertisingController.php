<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Escorts\Application\EscortData;
use App\Escorts\Application\EscortTypeConverter;
use App\Frontend\Processors\EscortQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AdvertisingController extends AbstractController
{
    use EscortQueryProcessor;

    /**
     * @Route("/cities/{id}/escort_types/{type}/advertising", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get Escort Advertising By City Id",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getAll(int $id, string $type): JsonResponse
    {
        $advertising = $this->getEscortQueryProcessor()
            ->getEscortAdvertisingByTypeAndLocationId(
                EscortTypeConverter::valueToEnum($type),
                $id
            )
        ;

        return $this->json(
            new ListResponse(count($advertising), $advertising),
            Response::HTTP_OK
        );
    }
}
