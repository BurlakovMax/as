<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\Frontend\Controller\AbstractRestController;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Escorts\Application\CreateEscortCommand;
use App\Escorts\Application\EscortNotFound;
use App\Escorts\Application\EscortTypeConverter;
use App\Escorts\Application\Ethnicity;
use App\Escorts\Application\EyeColor;
use App\Escorts\Application\HairColor;
use App\Escorts\Application\Language;
use App\Escorts\Application\Payments;
use App\Escorts\Application\Services;
use App\Escorts\Application\StatusData;
use App\Escorts\Application\TypesData;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\Status;
use App\Frontend\Open\Escorts\Presentation\Api\Structures\QuantityByType;
use App\Frontend\Processors\BreadcrumbsBuilder;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortClickStatisticCommandProcessor;
use App\Frontend\Processors\EscortCommandProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\ImageUploadProcessor;
use App\Locations\Application\CityNotFound;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortsController extends AbstractRestController
{
    use OffsetLimitParams;
    use EscortQueryProcessor;
    use EscortCommandProcessor;
    use ImageUploadProcessor;
    use BreadcrumbsBuilder;
    use CityQueryProcessor;
    use EscortClickStatisticCommandProcessor;

    /**
     * @Route("/escorts/types", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escorts Types",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Schema(
     *             @SWG\Property(property="types", type="array", @SWG\Items(type="string"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getType(): JsonResponse
    {
        return $this->json(
            EscortTypeConverter::listOfTypes(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/price", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escorts price",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Schema(
     *             @SWG\Property(property="price", type="string")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPrice(): JsonResponse
    {
        return $this->json(
            $this->getEscortQueryProcessor()->getEscortPrice(),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/ids", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort ids",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Schema(
     *             @SWG\Property(property="ids", type="array", @SWG\Items(type="integer"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getIds(Request $request): JsonResponse
    {
        $filter = [];
        if ($request->query->get('status') === 'deleted') {
            $filter[] = new FilterQuery('t.status', ComparisonType::in(), [ StatusData::expired(), StatusData::requestedToDelete() ]);
            $filter[] = new FilterQuery('t.deletedAt', ComparisonType::isNotNull(), null);
        } else {
            $filter[] = FilterQuery::createEqFilter('t.status', StatusData::active());
            $filter[] = new FilterQuery('t.deletedAt', ComparisonType::isNull(), null);
        }

        if ($request->query->has('update')) {
            $filter[]  = new FilterQuery(
                't.updatedAt',
                ComparisonType::lte(),
                $request->query->get('update')
            );
        }

        $filter[] = FilterQuery::createEqFilter('locations.locationId', $request->query->getInt('locationId'));
        $filter[] = FilterQuery::createEqFilter('type',EscortTypeConverter::valueToEnum($request->query->get('type'))->getRawValue());

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filter,
            [
                new OrderQuery('t.postedAt', Ordering::desc()),
            ],
            $this->getLimitation($request)
        );

        return $this->json(
            new ListResponse(
                $this->getEscortQueryProcessor()->countIdsBySearchQuery($search),
                array_map(
                    function (array $id): int {
                        return $id['id'];
                    },
                    $this->getEscortQueryProcessor()->getIdsBySearchQuery($search)
                )
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort by Id",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *         @SWG\Schema(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscort(int $id, Request $request): JsonResponse
    {
        $escort = $this->getEscortQueryProcessor()->getById($id);

        $this->getEscortClickStatisticCommandProcessor()->addClick(
            $escort->getId(),
            $request->query->getInt('locationId')
        );

        return
            $this->json(
                $escort,
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/escorts/{id}/breadcrumbs", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort breadcrumbs by Id",
     *     tags={"escorts", "breadcrumbs"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Breadcrumbs",
     *         @SWG\Schema(
     *             @SWG\Property(property="breadcrumbs", type="array", @SWG\Items(type="string"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     * @throws EscortNotFound
     * @throws CityNotFound
     */
    public function getEscortBreadcrumbs(int $id, Request $request): JsonResponse
    {
        $escort = $this->getEscortQueryProcessor()->getById($id);

        $city = $this->getCityQueryProcessor()->getById($request->query->getInt('locationId'));

        return $this->json(
            [
                'links' => $this->getBreadcrumbsBuilder()->getLinksByCityAndEscortId($city, $escort),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/getEscortsTypesWithQuantityByLocationId", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort allow types by LocationId",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort allow types",
     *         @SWG\Schema(
     *              @SWG\Property(property="types", type="array", @SWG\Items(type="integer"))
     *          )
     *
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscortsTypesWithQuantityByLocationId(Request $request): JsonResponse
    {
        $escorts = array_map(
            function (int $rawType) use ($request) {
                return
                    new QuantityByType(
                        $rawType,
                        EscortTypeConverter::valueToName($rawType),
                        EscortTypeConverter::valueToSlug($rawType),
                        EscortTypeConverter::valueToSlug($rawType),
                        $this->getEscortQueryProcessor()->countByFilterQuery(
                            [
                                FilterQuery::createEqFilter('locations.locationId', $request->query->getInt('locationId')),
                                FilterQuery::createEqFilter('t.type', $rawType),
                                FilterQuery::createEqFilter('t.sponsorAttributes.sponsor', false),
                            ]
                        )
                    );
            },
            [ TypesData::female(), TypesData::shemale(), TypesData::bodyRubs() ]
        );


        return
            $this->json(
                new ListResponse(count($escorts), $escorts),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/getEscortsByLocationId", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escorts by LocationId",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Schema(
     *             @SWG\Property(property="escorts", type="array",
     *                  @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *             )
     *          )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getListBySearchQuery(Request $request): JsonResponse
    {
        $escorts = $this->getEscortQueryProcessor()->getBySearchQuery(
            new SearchQuery(
                new FuzzySearchQuery(null, []),
                [
                    FilterQuery::createEqFilter('locations.locationId', $request->query->getInt('locationId')),
                    new FilterQuery('thumbnail', ComparisonType::neq(), ''),
                ],
                [
                    new OrderQuery('t.type', Ordering::asc()),
                    new OrderQuery('t.id', Ordering::desc()),
                ],
                $this->getLimitation($request)
            )
        );

        return
            $this->json(
                new ListResponse(count($escorts), $escorts),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/getEscortsByIds", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escorts list by Id",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Schema(
     *             @SWG\Property(property="escorts", type="array",
     *                  @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *             )
     *          )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscortsByIds(Request $request): JsonResponse
    {
        return
            $this->json(
                $this->getEscortQueryProcessor()->getByIds(
                    array_map(function (string $id): int { return (int)$id; }, $request->query->get('id'))
                ),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/escorts", methods={"GET"}, name="escorts")
     *
     * @SWG\Get(
     *     summary="Get escorts list by query params",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Schema(
     *             @SWG\Property(property="escorts", type="array",
     *                  @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *             )
     *          )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscortsByCriteria(Request $request): JsonResponse
    {
        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $this->createFilter($request),
            [
                new OrderQuery('t.postedAt', Ordering::desc()),
            ],
            $this->getLimitation($request)
        );

        return $this->json(
            new ListResponse(
                $this->getEscortQueryProcessor()->countBySearchQuery($search),
                $this->getEscortQueryProcessor()->getBySearchQuery($search),
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @return FilterQuery[]
     */
    private function createFilter(Request $request): array
    {
        $filter = [];
        $filter[] = FilterQuery::createEqFilter('locations.locationId', $request->query->getInt('locationId'));
        $filter[] = FilterQuery::createEqFilter('t.sponsorAttributes.sponsor', false);
        $filter[] = FilterQuery::createEqFilter('t.sponsorAttributes.sponsorMobile', false);

        if ($request->query->get('status') === 'deleted') {
            $filter[] = new FilterQuery('t.status', ComparisonType::in(), [ StatusData::expired(), StatusData::requestedToDelete() ]);
            $filter[] = new FilterQuery('t.deletedAt', ComparisonType::isNotNull(), null);
        } else {
            $filter[] = FilterQuery::createEqFilter('t.status', StatusData::active());
            $filter[] = new FilterQuery('t.deletedAt', ComparisonType::isNull(), null);
        }

        if ($request->query->has('update')) {
            $filter[]  = new FilterQuery(
                't.updatedAt',
                ComparisonType::lte(),
                $request->query->get('update')
            );
        }

        if ($request->query->has('type')) {
            $filter[] = FilterQuery::createEqFilter(
                'type',
                EscortTypeConverter::valueToEnum($request->query->get('type'))->getRawValue()
            );
        }

        if ($request->query->has('ethnicity')) {
            $filter[] = FilterQuery::createEqFilter(
                'ethnicity',
                $request->query->get('ethnicity')
            );
        }

        if ($request->query->has('languages')) {
            $languages = array_map(fn(string $lang): int => (int)$lang, $request->query->get('languages'));
            $filter[] = new FilterQuery('languageIds', ComparisonType::in(), $languages);
        }

        if ($request->query->has('hairColor')) {
            $filter[] = FilterQuery::createEqFilter(
                'hairColor',
                $request->query->get('hairColor')
            );
        }

        if ($request->query->has('eyeColor')) {
            $filter[] = FilterQuery::createEqFilter(
                'eyeColor',
                $request->query->get('eyeColor')
            );
        }

        if ($request->query->has('service')) {
            $filter[] = FilterQuery::createEqFilter($request->query->get('service'), 1);
        }

        if ($request->query->has('payment')) {
            $filter[] = FilterQuery::createEqFilter($request->query->get('payment'), 1);
        }

        return $filter;
    }

    /**
     * @Route("/getEscortsSideByTypeAndLocationId", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escorts Side By Type And LocationId",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Schema(
     *             @SWG\Property(property="escorts", type="array",
     *                  @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *             )
     *          )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscortsSideByTypeAndLocationId(Request $request): JsonResponse
    {
        $sponsors = $this->getEscortQueryProcessor()->getEscortsSideByTypeAndLocationId(
            EscortType::getValueOf($request->query->get('type')),
            $request->query->getInt('locationId')
        );

        return $this->json(
            new ListResponse(count($sponsors), $sponsors),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Add escort model",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Escort id and link token",
     *         @SWG\Schema(ref=@Model(type=App\Escorts\Application\CreateEscortResultData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function createEscort(Request $request): JsonResponse
    {
        $command = new CreateEscortCommand(
            (int) $request->get('accountId') ?? null,
        );

        $this->fill($command);
        $command->setBodyType($request->get('bodyType'));
        $command->setEmailVisibility($request->get('emailVisibility'));
        $command->setEthnicity($request->get('ethnicity'));
        $command->setEyeColor($request->get('eyeColor'));
        $command->setHairColor($request->get('hairColor'));
        $command->setKitty($request->get('kitty'));

        $cityThumbnails = [];
        $sideSponsors = [];
        $stickySponsorsWeek = [];
        $stickySponsorsMonth = [];

        foreach ($request->request->get('locationIds') as $locationId) {
            if ($request->request->has('homesp' . $locationId)) {
                $cityThumbnails[] = $request->request->getInt('homesp' . $locationId);
            }

            if ($request->request->has('side_sponsor_' . $locationId)) {
                $sideSponsors[] = $request->request->getInt('side_sponsor_' . $locationId);
            }

            if ($request->request->has('sticky_sponsor_week_' . $locationId)) {
                $stickySponsorsWeek[] = $request->request->getInt('sticky_sponsor_week_' . $locationId);
            }

            if ($request->request->has('sticky_sponsor_month_' . $locationId)) {
                $stickySponsorsMonth[] = $request->request->getInt('sticky_sponsor_month_' . $locationId);
            }
        }

        $command->setStickySponsorWeek($stickySponsorsWeek);
        $command->setStickySponsorMoth($stickySponsorsMonth);
        $command->setSideSponsorLocation($sideSponsors);
        $command->setCityThumbnailLocation($cityThumbnails);

        return $this->json(
            $this->getEscortCommandProcessor()->create($command),
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/escort_filters", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get list of escort filters on city page",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort filters key and value",
     *         @SWG\Schema(
     *             @SWG\Property(
     *             property="filters_category",
     *             type="array",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="value", type="integer", description="Raw filters value, that need to be send to endpoints"),
     *                 @SWG\Property(property="name", type="string", description="Human readble value of filters")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function escortFilters(): JsonResponse
    {
        $filters = [];

        $filtersValues = [
            'Ethnicity' => Ethnicity::getList(),
            'EyeColor' => EyeColor::getList(),
            'HairColor' => HairColor::getList(),
            'Language' => Language::getList(),
            'Payments' => Payments::getList(),
            'Services' => Services::getList(),
        ];

        foreach ($filtersValues as $name => $filtersValue) {
           foreach ($filtersValue as $key => $value) {
               $filters[$name][] = [
                'value' => $key,
                'name' => $value,
               ];
           }
        }

        return $this->json(
            $filters,
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/link_escort_to_account", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Link escort to current account",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function linkToAccount(Request $request): JsonResponse
    {
        $this->getEscortCommandProcessor()->linkToAccount(
            $request->request->getInt('accountId'),
            $request->request->getInt('escortId'),
        );

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }
}
