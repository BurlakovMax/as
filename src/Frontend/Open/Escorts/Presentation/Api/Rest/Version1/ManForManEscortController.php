<?php

declare(strict_types=1);

namespace App\Frontend\Open\Escorts\Presentation\Api\Rest\Version1;

use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\ManForManEscortQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ManForManEscortController extends AbstractController
{
    use CityQueryProcessor;
    use ManForManEscortQueryProcessor;

    /**
     * @Route("/cities/{id}/m4mescorts", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get Man For Man Escort by cityId",
     *     tags={"escorts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\ManForManEscortData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getByCityId(int $id): JsonResponse
    {
        $city = $this->getCityQueryProcessor()->getById($id);

        return
            $this->json(
                $this->getManForManEscortQueryProcessor()->getCurrentM4mLink(null, $city),
                Response::HTTP_OK
            )
        ;
    }
}