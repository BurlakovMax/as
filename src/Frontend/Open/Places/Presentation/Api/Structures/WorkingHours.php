<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Structures;

final class WorkingHours
{
    private string $dayOfWeek;

    private ?string $from;

    private ?string $to;

    public function __construct(string $dayOfWeek, ?string $from = null, ?string $to = null)
    {
        $this->dayOfWeek = $dayOfWeek;
        $this->from = $from;
        $this->to = $to;
    }

    public function getDayOfWeek(): string
    {
        return $this->dayOfWeek;
    }

    public function getFrom(): ?string
    {
        return $this->from;
    }

    public function getTo(): ?string
    {
        return $this->to;
    }
}