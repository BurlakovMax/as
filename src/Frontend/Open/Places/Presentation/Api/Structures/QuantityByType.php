<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Structures;

final class QuantityByType
{
    private int $id;

    private string $name;

    private string $url;

    private int $count;

    public function __construct(int $id, string $name, string $url, int $count)
    {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->count = $count;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}