<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\Frontend\Controller\AbstractRestController;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Frontend\Processors\BreadcrumbsBuilder;
use App\Frontend\Processors\BusinessOwnerQueryProcessor;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\PlaceQueryProcessor;
use App\Frontend\Processors\PlaceTypeQueryProcessor;
use App\Places\Application\CreatePlaceCommand;
use App\Places\Application\PlaceAttributeCommand;
use App\Places\Application\PlaceCommandProcessor;
use App\Places\Application\PlaceNotFound;
use App\Places\Domain\Editable;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlacesController extends AbstractRestController
{
    use PlaceQueryProcessor;
    use OffsetLimitParams;
    use PlaceTypeQueryProcessor;
    use BreadcrumbsBuilder;
    use CityQueryProcessor;
    use BusinessOwnerQueryProcessor;

    private function getCommandProcessor(): PlaceCommandProcessor
    {
        return $this->container->get(PlaceCommandProcessor::class);
    }

    /**
     * @Route("/places/owner_prices", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Prices for palce owner",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Place owner advertise prices",
     *         @SWG\Schema(
     *              @SWG\Property(property="types", type="array", @SWG\Items(type="string"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getOwnerPackagePrices(): JsonResponse
    {
        return $this->json(
            [
                'premiumOwnerPackageMonth' =>
                    [
                        'amount' => $this->getBusinessOwnerQueryProcessor()->getPremiumOwnerPackageMonth(),
                        'recurringPeriod' =>$this->getBusinessOwnerQueryProcessor()->getRecurringPeriodInMonth(),
                    ],
                'premiumOwnerPackageYear' =>
                    [
                        'amount' => $this->getBusinessOwnerQueryProcessor()->getPremiumOwnerPackageYear(),
                        'recurringPeriod' =>$this->getBusinessOwnerQueryProcessor()->getRecurringPeriodInYear(),
                    ],
                'regularOwnerPackageYear' =>
                    [
                        'amount' => $this->getBusinessOwnerQueryProcessor()->getRegularOwnerPackageYear(),
                        'recurringPeriod' =>$this->getBusinessOwnerQueryProcessor()->getRecurringPeriodInYear(),
                    ]
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place by Id",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Place",
     *         @SWG\Schema(ref=@Model(type=App\Places\Application\PlaceData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws PlaceNotFound
     */
    public function getPlace(int $id): JsonResponse
    {
        return
            $this->json(
                $this->getPlaceQueryProcessor()->getById($id),
                Response::HTTP_OK
            );
    }

    /**
     * @Route("/places/{id}/breadcrumbs", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get place breadcrumbs by Id",
     *     tags={"escorts", "breadcrumbs"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Breadcrumbs",
     *         @SWG\Schema(
     *             @SWG\Property(property="breadcrumbs", type="array", @SWG\Items(type="string"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPlaceBreadcrumbs(int $id): JsonResponse
    {
        $place = $this->getPlaceQueryProcessor()->getById($id);
        $placeType = $this->getPlaceTypeQueryProcessor()->getById($place->getTypeId());

        return $this->json(
            [
                'links' => $this->getBreadcrumbsBuilder()->getLinksByCityAndPlaceAndPlaceType(
                    $this->getCityQueryProcessor()->getById($place->getLocationId()),
                    $place,
                    $placeType
                )
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places", methods={"GET"}, name="places")
     *
     * @SWG\Get(
     *     summary="get places",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Places",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPlaces(Request $request): JsonResponse
    {
        $filters = [];
        $filters[] = FilterQuery::createEqFilter('t.typeId', $request->query->getInt('typeId'));
        $filters[] = FilterQuery::createEqFilter('t.locationId', $request->query->getInt('locationId'));
        if ($request->query->get('status') === 'deleted') {
            $filters[] = new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNotNull(), null);
        } else {
            $filters[] = FilterQuery::createEqFilter('t.editable', Editable::enabled());
            $filters[] = new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNull(), null);
        }

        if ($request->query->has('update')) {
            $filters[]  = new FilterQuery(
                't.datesOfChanges.updatedAt',
                ComparisonType::lte(),
                $request->query->get('update')
            );
        }

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filters,
            [],
            $this->getLimitation($request)
        );

        return $this->json(
            new ListResponse(
                $this->getPlaceQueryProcessor()->countBySearchQuery($search),
                $this->getPlaceQueryProcessor()->getBySearchQuery($search),
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="create place",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Place Id",
     *         @SWG\Schema(type="Id")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function create(Request $request): JsonResponse
    {
        $command = new CreatePlaceCommand();
        $this->fill($command);

        $attributes = [];

        if (!empty($request->request->get('placeAttributes'))) {
            foreach ($request->request->get('placeAttributes') as $attribute) {
                $attributes[] = new PlaceAttributeCommand(
                    $attribute['attribute_id'],
                    (string) $attribute['value'],
                    isset($attribute['value_text']) ? $attribute['value_text'] : null
                );
            }
        }

        $command->setPlaceAttributes($attributes);

        $placeId = $this->getCommandProcessor()->create($command);

        return $this->json(
            ['id' => $placeId],
            Response::HTTP_CREATED,
        );
    }
}
