<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Frontend\Open\Places\Presentation\Api\Structures\WorkingHours;
use App\Frontend\Processors\PlaceAttributeQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceAttributesController extends AbstractController
{
    use PlaceAttributeQueryProcessor;

    /**
     * @Route("/places/{id}/rates", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place rates by PlaceId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceAttributes",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceAttributeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getRatesByPlaceId(int $id): JsonResponse
    {
        $placeAttribute = $this->getPlaceAttributeQueryProcessor()->getByPlaceId($id);

        $rates = [];
        foreach ($placeAttribute as $attribute) {
            if ($attribute->getType() === 'fee' && $attribute->getValue() !== 'NULL') {
                $rates[] = $attribute;
            }
        }

        return $this->json(
            new ListResponse(
                count($rates),
                $rates
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places/{id}/working_hours", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place working hours by PlaceId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceAttributes",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceAttributeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getWorkingHoursByPlaceId(int $id): JsonResponse
    {
        $placeAttribute = $this->getPlaceAttributeQueryProcessor()->getByPlaceId($id);

        $rawWorkingHours = [];
        foreach ($placeAttribute as $attribute) {
            if ($attribute->getType() === 'hours') {
                $rawWorkingHours[$attribute->getName()] = $attribute->getValue();
            }
        }

        $workingHoursByDay = $this->getPlaceAttributeQueryProcessor()->workingHoursByDay($rawWorkingHours);

        $hours = [];
        foreach ($workingHoursByDay as $dayOfWeek => $workingHour) {
            $period = explode(' - ', $workingHour);
            if (count($period) === 2) {
                $hours[] = new WorkingHours($dayOfWeek, $period[0], $period[1]);
                continue;
            }

            $hours[] = new WorkingHours($dayOfWeek);
        }

        return $this->json(
            new ListResponse(
                count($hours),
                $hours
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places/{id}/information", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place information by PlaceId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceAttributes",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceAttributeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getInformationByPlaceId(int $id): JsonResponse
    {
        $placeAttribute = $this->getPlaceAttributeQueryProcessor()->getByPlaceId($id);

        $information = [];
        foreach ($placeAttribute as $attribute) {
            if ($attribute->getType() === 'boolean' || $attribute->getType() === 'boolean3') {
                $information[] = $attribute;
            }
        }

        return $this->json(
            new ListResponse(
                count($information),
                $information
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places/{id}/attributes", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place attributes by PlaceId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceAttributes",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceAttributeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getAttributeWithOptionsByPlaceId(int $id): JsonResponse
    {
        $placeAttribute = $this->getPlaceAttributeQueryProcessor()->getByPlaceId($id);

        $attributeWithOptions = [];
        foreach ($placeAttribute as $attribute) {
            if ($attribute->getType() === 'enum' && $attribute->getValue() !== 0) {
                $attributeWithOptions[] = $attribute;
            }
        }

        return $this->json(
            new ListResponse(
                count($attributeWithOptions),
                $attributeWithOptions
            ),
            Response::HTTP_OK
        );
    }
}