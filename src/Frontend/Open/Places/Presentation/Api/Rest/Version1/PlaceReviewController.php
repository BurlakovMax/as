<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Frontend\Open\Places\Presentation\Api\Structures\ReviewAuthors;
use App\Frontend\Processors\AccountQueryProcessor;
use App\Frontend\Processors\PlaceReviewCommentQueryProcessor;
use App\Frontend\Processors\PlaceReviewQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceReviewController extends AbstractController
{
    use PlaceReviewQueryProcessor;
    use AccountQueryProcessor;
    use PlaceReviewCommentQueryProcessor;

    /**
     * @Route("/places/{placesId}/reviews/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place reviews by id",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Place reviews",
     *         @SWG\Schema(ref=@Model(type=App\Places\Application\PlaceReviewWithAuthorAndComments::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getById(int $placesId, int $id): JsonResponse
    {
        $reviewWithAuthorAndComments = $this->getPlaceReviewQueryProcessor()->getWithAuthorAndCommentsById($id);
        return $this->json($reviewWithAuthorAndComments, Response::HTTP_OK);
    }

    /**
     * @Route("/places/{id}/reviews", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place reviews by PlaceId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Place reviews",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceReviewWithAuthorAndComments::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getByPlaceId(int $id): JsonResponse
    {
        $reviewsWithAuthorsAndComments =
            $this->getPlaceReviewQueryProcessor()
                ->getWithAuthorsAndCommentsByPlaceId($id)
        ;

        return $this->json(
            new ListResponse(count($reviewsWithAuthorsAndComments), $reviewsWithAuthorsAndComments),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places/{id}/review_authors", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place review authors by PlaceId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Place reviews",
     *         @SWG\Items(ref=@Model(type=App\Frontend\Open\Places\Presentation\Api\Structures\ReviewAuthors::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getReviewsAuthorByPlaceId(int $id): JsonResponse
    {
        $reviews = $this->getPlaceReviewQueryProcessor()->getByPlaceId($id);

        $reviewAuthors = [];
        foreach ($reviews as $review) {
            $account = $this->getAccountQueryProcessor()->getAccount($review->getAccountId());
            $reviewAuthors[] = new  ReviewAuthors(
                $review->getAccountId(),
                $account->getId(),
                $account->getName(),
                $account->getAvatar()
            );
        }

        return $this->json(
            new ListResponse(count($reviewAuthors), $reviewAuthors),
            Response::HTTP_OK
        );
    }
}
