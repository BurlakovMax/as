<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Frontend\Processors\PlaceReviewCommentQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceReviewCommentController extends AbstractController
{
    use PlaceReviewCommentQueryProcessor;

    /**
     * @Route("/places/{id}/reviews/{reviewId}/comments", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place comments by reviewId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceReviewComments",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceReviewCommentData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getPlaceTypes(int $id, int $reviewId): JsonResponse
    {
        $placeReviewComments = $this->getPlaceReviewCommentQueryProcessor()->getByReviewId($reviewId);

        return $this->json(
            new ListResponse(
                count($placeReviewComments),
                $placeReviewComments
            ),
            Response::HTTP_OK
        );
    }
}