<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Rest\Version1;

use App\Places\Application\ImageUploadProcessor;
use Ramsey\Uuid\Uuid;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceImageUploadController extends AbstractController
{
    private function getCommandProcessor(): ImageUploadProcessor
    {
        return $this->container->get(ImageUploadProcessor::class);
    }

    /**
     * @Route("/placeImageUpload", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="upload image",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Image"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \LazyLemurs\FileUploader\MimeTypeIsNotSupported
     * @throws \Exception
     */
    public function uploadImage(Request $request): JsonResponse
    {
        return $this->json(
            [
                'image' => $this->getCommandProcessor()->save(
                    (Uuid::uuid4())->toString(),
                    $request->files->get('image')
                )
            ],
            Response::HTTP_CREATED
        );
    }
}