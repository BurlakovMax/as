<?php

declare(strict_types=1);

namespace App\Frontend\Open\Places\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Frontend\Open\Places\Presentation\Api\Structures\QuantityByType;
use App\Frontend\Processors\PlaceQueryProcessor;
use App\Frontend\Processors\PlaceTypeQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceTypesController extends AbstractController
{
    use PlaceTypeQueryProcessor;
    use PlaceQueryProcessor;

    /**
     * @Route("/placeTypes/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place type by Id",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceType",
     *         @SWG\Schema(ref=@Model(type=App\Places\Application\PlaceTypeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \App\Places\Application\PlaceTypeNotFound
     */
    public function getPlaceType(int $id): JsonResponse
    {
        return
            $this->json(
                $this->getPlaceTypeQueryProcessor()->getById($id),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/placeTypes", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place types",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceTypes",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceTypeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPlaceTypes(): JsonResponse
    {
        $placeTypes = $this->getPlaceTypeQueryProcessor()->getAll();

        return $this->json(
            new ListResponse(
                count($placeTypes),
                $placeTypes
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/getPlaceTypesByIds", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get place types by Ids",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceType",
     *         @SWG\Items(ref=@Model(type=App\Places\Application\PlaceTypeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getPlaceTypesByIds(Request $request): JsonResponse
    {
        return
            $this->json(
                $this->getPlaceTypeQueryProcessor()->getByIds(
                    array_map(function (string $id): int { return (int)$id; }, $request->query->get('id'))
                ),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/getPlacesTypesWithQuantityByLocationId", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get places allowTypeIds by LocationId",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="AllowTypeIds",
     *         @SWG\Items(type="int")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPlacesTypesWithQuantityByLocationId(Request $request): JsonResponse
    {
        $placeTypeIds = $this->getPlaceQueryProcessor()->getTypeIdsByLocationId($request->query->getInt('locationId'));
        $placeTypes = $this->getPlaceTypeQueryProcessor()->getByIds($placeTypeIds);

        $response = [];
        foreach ($placeTypes as $placeType) {
            $response[] = new QuantityByType(
                $placeType->getId(),
                $placeType->getName(),
                $placeType->getUrl(),
                $this->getPlaceQueryProcessor()->count($placeType->getId(), [ $request->query->getInt('locationId') ])
            );
        }

        return
            $this->json(
                new ListResponse(count($response), $response),
                Response::HTTP_OK
            )
        ;
    }
}