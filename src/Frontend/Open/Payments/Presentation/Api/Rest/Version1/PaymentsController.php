<?php

declare(strict_types=1);

namespace App\Frontend\Open\Payments\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Controller\AbstractRestController;
use App\Frontend\Processors\PaymentQueryProcessor;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PaymentsController extends AbstractRestController
{
    use PaymentQueryProcessor;

    /**
     * @Route("/payments/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get Payment By Id",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Payments",
     *         @SWG\Schema(@SWG\Property(property="result", type="string"))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getById(int $id): JsonResponse
    {
        $payment = $this->getPaymentQueryProcessor()->get($id);

        return $this->json(['result' => $payment->getResult()], Response::HTTP_OK);
    }
}