<?php

declare(strict_types=1);

namespace App\Frontend\Open\Payments\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Controller\AbstractRestController;
use App\Frontend\Processors\AccountQueryProcessor;
use App\Frontend\Processors\BusinessOwnerQueryProcessor;
use App\Frontend\Processors\CountryQueryProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortSideQueryProcessor;
use App\Frontend\Processors\EscortSponsorQueryProcessor;
use App\Frontend\Processors\EscortStickyQueryProcessor;
use App\Frontend\Processors\PaymentCommandProcessor;
use App\Frontend\Processors\PromocodeQueryProcessor;
use App\Frontend\Processors\TransactionCommandProcessor;
use App\Frontend\Processors\TransactionQueryProcessor;
use App\Payments\Application\CreateCreditCardCommand;
use App\Payments\Application\CreatePaymentCommand;
use App\Payments\Application\CreateTransactionCommand;
use App\Payments\Domain\ServiceType;
use App\Security\Application\AccountData;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreatePaymentController extends AbstractRestController
{
    use TransactionQueryProcessor;
    use PaymentCommandProcessor;
    use CountryQueryProcessor;
    use AccountQueryProcessor;
    use EscortQueryProcessor;
    use EscortSponsorQueryProcessor;
    use EscortSideQueryProcessor;
    use EscortStickyQueryProcessor;
    use BusinessOwnerQueryProcessor;
    use PromocodeQueryProcessor;
    use TransactionCommandProcessor;

    /**
     * @Route("/payments/pay", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Create payment",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Payments",
     *         @SWG\Schema(
     *             @SWG\Property(property="id", type="integer", format="int32")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function pay(Request $request): JsonResponse
    {
        $this->validateEscortTypes($request);

        $account = $this->getAccountQueryProcessor()->getAccount($request->request->getInt('accountId'));
        $locationIds = $request->request->get('locationIds') ? array_map(
            fn(string $locationId) => (int) $locationId, $request->request->get('locationIds')
        ) : [];
        $totalCoast = $this->calculateTotalCoast($request, $account, ...$locationIds);

        $createCreditCardCommand = new CreateCreditCardCommand(
            $account->getId(),
            (string) $request->request->get('ccCc'),
            (string) $request->request->get('ccMonth'),
            (string) $request->request->get('ccYear'),
            (string) $request->request->get('ccCvc2'),
            (string) $request->request->get('ccFirstname'),
            (string) $request->request->get('ccLastname'),
            (string) $request->request->get('ccAddress'),
            (string) $request->request->get('ccZipcode'),
            (string) $request->request->get('ccCity'),
            (string) $request->request->get('ccState'),
            (string) $request->request->get('ccCountry'),
        );

        $createPaymentCommand = new CreatePaymentCommand(
            $account->getId(),
            $account->getEmail(),
            $totalCoast,
            $locationIds,
            $request->request->getInt('escortId'),
            $request->request->getInt('placeId'),
            $_SERVER['REMOTE_ADDR']
        );

        $command = $this->makePayment($request, $createPaymentCommand, $createCreditCardCommand);
        $this->getTransactionCommandProcessor()->save($command);

        return
            $this->json(
                [
                    'id' => $command->getPaymentId()
                ],
                Response::HTTP_CREATED
            );
    }

    /**
     * @Route("/payments/total_cost", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Calculate total cost on backend",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Payments",
     *         @SWG\Schema(
     *             @SWG\Property(property="id", type="integer", format="int32")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getTotalCost(Request $request): JsonResponse
    {
        $account = $this->getAccountQueryProcessor()->getAccount($request->query->getInt('accountId'));
        $locationIds = $request->query->get('locationIds') ? array_map(
            fn(string $locationId) => (int) $locationId, $request->query->get('locationIds')
        ) : [];
        return $this->json(['totalCost' => $this->calculateTotalCoast($request, $account, ...$locationIds)]);
    }

    /**
     * @throws \Throwable
     */
    private function makePayment(
        Request $request,
        CreatePaymentCommand $paymentCommand,
        CreateCreditCardCommand $creditCardCommand
    ): CreateTransactionCommand {

        switch ($request->request->get('paymentType')) {
            case ServiceType::escort():
                return $this->getPaymentCommandProcessor()->createEscortPayment(
                    $paymentCommand,
                    $creditCardCommand
                );
            case ServiceType::escortSponsor():
                return $this->getPaymentCommandProcessor()->createEscortSponsorPayment(
                    $paymentCommand,
                    $creditCardCommand
                );
            case ServiceType::escortSticky():
                $escortStickyMonthLocationIds = $request->request->get('escortStickyMonthLocationIds') ? array_map(
                    fn(string $locationId) => (int) $locationId, $request->request->get('escortStickyMonthLocationIds')
                ) : [];

                $escortStickyWeekLocationIds = $request->request->get('escortStickyWeekLocationIds') ? array_map(
                    fn(string $locationId) => (int) $locationId, $request->request->get('escortStickyWeekLocationIds')
                ) : [];

                return $this->getPaymentCommandProcessor()->createEscortStickyPayment(
                    $paymentCommand,
                    $creditCardCommand,
                    $escortStickyMonthLocationIds,
                    $escortStickyWeekLocationIds,
                    null !== $request->request->get('recurringPeriod')
                );
            case ServiceType::advertise():
                return $this->getPaymentCommandProcessor()->createAdvertisePayment(
                    $paymentCommand,
                    $creditCardCommand
                );
            case ServiceType::escortSide():
                return $this->getPaymentCommandProcessor()->createEscortSidePayment(
                    $paymentCommand,
                    $creditCardCommand
                );
            case ServiceType::placeowner():
                return $this->getPaymentCommandProcessor()->createPlaceownerPayment(
                    $paymentCommand,
                    $creditCardCommand,
                    $request->request->get('recurringPeriod')
                );
            case ServiceType::topUps():
                return $this->getPaymentCommandProcessor()->createTopUpsPayment(
                    $paymentCommand,
                    $creditCardCommand,
                    $request->request->getInt('topUps')
                );
            case ServiceType::combo():
                $escortSideLocationIds = $request->request->get('escortSideLocationIds') ? array_map(
                    fn(string $locationId) => (int) $locationId, $request->request->get('escortSideLocationIds')
                ) : [];

                $escortSponsorLocationIds = $request->request->get('escortSponsorLocationIds') ? array_map(
                    fn(string $locationId) => (int) $locationId, $request->request->get('escortSponsorLocationIds')
                ) : [];

                $escortStickyMonthLocationIds = $request->request->get('escortStickyMonthLocationIds') ? array_map(
                    fn(string $locationId) => (int) $locationId, $request->request->get('escortStickyMonthLocationIds')
                ) : [];

                $escortStickyWeekLocationIds = $request->request->get('escortStickyWeekLocationIds') ? array_map(
                    fn(string $locationId) => (int) $locationId, $request->request->get('escortStickyWeekLocationIds')
                ) : [];

                return $this->getPaymentCommandProcessor()->createCombo(
                    $paymentCommand,
                    $creditCardCommand,
                    $escortSideLocationIds,
                    $escortSponsorLocationIds,
                    $escortStickyMonthLocationIds,
                    $escortStickyWeekLocationIds
                );

            default:
                throw new \RuntimeException('Payment type not specified');
        }
    }

    private function calculateTotalCoast(Request $request, AccountData $account, int ...$locationIds): float
    {
        switch ($request->get('paymentType')) {
            case ServiceType::escortSponsor():
                $total = (float) $this->getEscortSponsorQueryProcessor()->calculateTotalCoast(
                    $account->isAgency(),
                    $locationIds
                );

                if (!empty($request->get('promocode'))) {
                    $total = $this->calculateWithPromocode($request->get('promocode'), $total);
                }
                return $total;
            case ServiceType::escortSide():
                 $total = (float) $this->getEscortSideQueryProcessor()->calculateTotalCoast(
                    $account->isAgency(),
                    $locationIds
                );

                if (!empty($request->get('promocode'))) {
                    $total = $this->calculateWithPromocode($request->get('promocode'), $total);
                }
                return $total;
            case ServiceType::escort():
                $total = (float) $this->getEscortQueryProcessor()->getEscortPrice() * count($locationIds);
                if (!empty($request->get('promocode'))) {
                    $total = $this->calculateWithPromocode($request->get('promocode'), $total);
                }
                return $total;
            case ServiceType::escortSticky():
                $total = $this->calculateTotalCoastEscortSticky($request, $account->isAgency());
                if (!empty($request->get('promocode'))) {
                    $total = $this->calculateWithPromocode($request->get('promocode'), $total);
                }
                return $total;
//            case ServiceType::advertise():
            case ServiceType::placeowner():
                $total = (float) $this->getBusinessOwnerQueryProcessor()->calculateTotalCoast(
                    (int) $request->get('placeId'),
                    $request->get('ownerPackage'),
                    (int) $request->get('recurringPeriod')
                );
                if (!empty($request->get('promocode'))) {
                    $total = $this->calculateWithPromocode($request->get('promocode'), $total);
                }
                return $total;
            case ServiceType::topUps():
                $total = (float) $request->get('topUps') *
                    $this->getEscortQueryProcessor()->getEscortTopUpPrice($account->isAgency());

                if (!empty($request->get('promocode'))) {
                    $total = $this->calculateWithPromocode($request->get('promocode'), $total);
                }
                return $total;
            case ServiceType::combo():
                $totalCoast = (float) $this->getEscortQueryProcessor()->getEscortPrice() * count($locationIds);

                if (is_array($request->get('escortSideLocationIds'))) {
                    $ids = array_map(
                        fn(string $locationId) => (int) $locationId, $request->get('escortSideLocationIds')
                    );
                    $totalCoast += (float) $this->getEscortSideQueryProcessor()->calculateTotalCoast(
                        $account->isAgency(), $ids
                    );
                }

                if (is_array($request->get('escortSponsorLocationIds'))) {
                    $ids = array_map(
                        fn(string $locationId) => (int) $locationId, $request->get('escortSponsorLocationIds')
                    );
                    $totalCoast += (float) $this->getEscortSponsorQueryProcessor()->calculateTotalCoast(
                        $account->isAgency(), $ids
                    );
                }

                $totalCoast += $this->calculateTotalCoastEscortSticky($request, $account->isAgency());

                if (!empty($request->get('promocode'))) {
                    $totalCoast = $this->calculateWithPromocode($request->get('promocode'), $totalCoast);
                }

                return $totalCoast;
            default:
                throw new \InvalidArgumentException('Payment type not specified');
        }
    }

    private function calculateTotalCoastEscortSticky(Request $request, bool $isAgency): float
    {
        $totalCoast = 0.0;
        if (!empty($request->get('escortStickyMonthLocationIds'))) {
            $ids = array_map(
                fn(string $locationId) => (int) $locationId, $request->get('escortStickyMonthLocationIds')
            );
            $escort = $this->getEscortQueryProcessor()->getById((int)$request->get('escortId'));
            $days = [];
            foreach ($ids as $locationId) {
                $days[$locationId] = 30;
            }
            $totalCoast += (float) $this->getEscortStickyQueryProcessor()->calculateTotalPrice(
                $escort->getId(),
                $escort->getRawType(),
                $isAgency,
                $ids,
                $days
            );
        }

        if (!empty($request->get('escortStickyWeekLocationIds'))) {
            $ids = array_map(
                fn(string $locationId) => (int) $locationId, $request->get('escortStickyWeekLocationIds')
            );
            $escort = $this->getEscortQueryProcessor()->getById((int)$request->get('escortId'));
            $days = [];
            foreach ($ids as $locationId) {
                $days[$locationId] = 7;
            }
            $totalCoast += (float) $this->getEscortStickyQueryProcessor()->calculateTotalPrice(
                $escort->getId(),
                $escort->getRawType(),
                $isAgency,
                $ids,
                $days
            );
        }

        return $totalCoast;
    }

    private function calculateWithPromocode(string $code, float $totalCost): float
    {
        $promoCode = $this->getPromocodeQueryProcessor()->getActiveByCode($code);

        if (0 !== $promoCode->getFixedPrice()) {
            $totalCost -= (float) $promoCode->getFixedPrice();
        }

        if (0 !== $promoCode->getDiscountPercentage()) {
            $totalCost -= (float) ($totalCost * $promoCode->getDiscountPercentage()) / 100;
        }

        if (false !== $promoCode->isFreeEscortAvailable()) {
            $totalCost -= $totalCost;
        }

        if (0 > $totalCost) {
            $totalCost = 0;
        }

        return $totalCost;
    }

    private function validateEscortTypes(Request $request): void
    {
        switch ($request->request->get('paymentType')) {
            case ServiceType::escortSponsor():
            case ServiceType::escortSide():
            case ServiceType::escort():
            case ServiceType::combo():
            case ServiceType::escortSticky():
                $parameters = ['escortId', 'locationIds'];
                break;
            case ServiceType::advertise():
                $parameters = ['amount'];
                break;
            case ServiceType::placeowner():
                $parameters = ['placeId', 'recurringPeriod'];
                break;
            case ServiceType::topUps():
                $parameters = ['topUps'];
                break;
            default:
                throw new \InvalidArgumentException('Payment type not specified');
        }

        foreach ($parameters as $parameter) {
            if (!$request->request->has($parameter)) {
                throw new \InvalidArgumentException($parameter . ' parameter not found');
            }
        }
    }
}
