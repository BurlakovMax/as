<?php

declare(strict_types=1);

namespace App\Frontend\Open\Payments\Presentation\Api\Rest\Version1;

use App\Payments\Application\CreateTransactionCommand;
use App\Payments\Application\TransactionCommandProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CallbackController extends AbstractController
{
    /**
     * @Route("/callback/payment", methods={"POST"})
     */
    public function receive(Request $request): JsonResponse
    {
        $command = new CreateTransactionCommand(
            $request->request->get('id'),
            $request->request->get('merchantId'),
            $request->request->get('providerId'),
            $request->request->get('customerId'),
            $request->request->get('cardId'),
            $request->request->get('createdAt'),
            $request->request->get('status'),
            $request->request->get('changedAt'),
            $request->request->get('description'),
            $request->request->get('amount'),
            $request->request->get('currency'),
            $request->request->get('action'),
            $request->request->get('externalId'),
            $request->request->get('response'),
            $request->request->get('errorReason'),
        );
        $this->getPaymentCommandProcessor()->save($command);

        return $this->json(['result' => 'OK'], Response::HTTP_OK);
    }

    private function getPaymentCommandProcessor(): TransactionCommandProcessor
    {
        return $this->container->get(TransactionCommandProcessor::class);
    }
}
