<?php

declare(strict_types=1);

namespace App\Frontend\Open\Support\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Controller\AbstractRestController;
use App\Support\Application\CreateContactFormCommand;
use App\Support\Application\ContactFormCommandProcessor;
use LazyLemurs\Structures\Ip;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ContactFormController extends AbstractRestController
{
    private function getCommandProcessor(): ContactFormCommandProcessor
    {
        return $this->container->get(ContactFormCommandProcessor::class);
    }

    /**
     * @Route("/contact_form", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="create contact form",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="ContactForm id",
     *         @SWG\Schema(type="Id")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function create(Request $request): JsonResponse
    {
        $command = new CreateContactFormCommand(
            new Ip($request->getClientIp()),
            $request->headers->get('User-Agent')
        );

        $this->fill($command);
        $id = $this->getCommandProcessor()->create($command);

        return $this->json(
            ['id' => $id],
            Response::HTTP_CREATED,
        );
    }
}