<?php

declare(strict_types=1);

namespace App\Frontend\Open\Breadcrumbs\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Escorts\Application\EscortTypeConverter;
use App\Frontend\Processors\BreadcrumbsBuilder;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\PlaceQueryProcessor;
use App\Frontend\Processors\PlaceTypeQueryProcessor;
use App\Locations\Application\CityData;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class BreadcrumbsController extends AbstractController
{
    use EscortQueryProcessor;
    use PlaceQueryProcessor;
    use PlaceTypeQueryProcessor;
    use CityQueryProcessor;
    use BreadcrumbsBuilder;

    /**
     * @Route("/getBreadcrumbCityLinks", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get Breadcrumb City Links By LocationId And Url",
     *     tags={"breadcrumbs"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Forums",
     *         @SWG\Items(ref=@Model(type=App\Frontend\Open\Breadcrumbs\Presentation\Api\Structures\Link::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getBreadcrumbCityLinks(Request $request): JsonResponse
    {
        $city = $this->getCityQueryProcessor()->getById($request->query->getInt('locationId'));

        switch ($request->get('type')) {
            case 'category':
                return $this->createLinksByCategory($request, $city);
            case 'city':
                return $this->createLinksByCity($city);
            case 'escort':
                return $this->createLinksByEscort($request, $city);
            case 'place':
                return $this->createLinksByPlace($request, $city);
            case 'placeCategory':
                return $this->createLinksByPlaceCategory($request, $city);
            default:
                throw new \InvalidArgumentException('Type is unsupported');
        }
    }

    /**
     * @throws \App\Locations\Application\CityNotFound
     */
    private function createLinksByCategory(Request $request, CityData $city): JsonResponse
    {
        if (!$request->query->has('category')) {
            throw new \InvalidArgumentException('Category is NULL');
        }

        $typeName = EscortTypeConverter::valueToName($request->query->getInt('category'));

        $links = $this->getBreadcrumbsBuilder()->getLinksByCityAndTypeName($city, $typeName);

        return
            $this->json(
                new ListResponse(count($links), $links),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @throws \App\Locations\Application\CityNotFound
     */
    private function createLinksByPlaceCategory(Request $request, CityData $city): JsonResponse
    {
        if (!$request->query->has('placeCategory')) {
            throw new \InvalidArgumentException('Category is NULL');
        }

        $placeType = $this->getPlaceTypeQueryProcessor()->getByUrl($request->query->get('placeCategory'));
        $links = $this->getBreadcrumbsBuilder()->getLinksByCityAndTypeName($city, $placeType->getName());

        return
            $this->json(
                new ListResponse(count($links), $links),
                Response::HTTP_OK
            )
        ;
    }

    private function createLinksByCity(CityData $city): JsonResponse
    {
        return
            $this->json(
                [
                    'breadcrumb_links' => $this->getBreadcrumbsBuilder()->getLinksByCityAndTypeName($city, null)
                ],
                Response::HTTP_OK
            )
        ;
    }

    private function createLinksByEscort(Request $request, CityData $city): JsonResponse
    {
        if (!$request->query->has('id')) {
            throw new \InvalidArgumentException('ID is NULL');
        }

        $escort = $this->getEscortQueryProcessor()->getById($request->query->getInt('id'));

        return
            $this->json(
                [
                    'breadcrumb_links' => $this->getBreadcrumbsBuilder()->getLinksByCityAndEscortId(
                        $city,
                        $escort
                    )
                ],
                Response::HTTP_OK
            )
        ;
    }

    private function createLinksByPlace(Request $request, CityData $city): JsonResponse
    {
        if (!$request->query->has('id')) {
            throw new \InvalidArgumentException('ID is NULL');
        }

        $place = $this->getPlaceQueryProcessor()->getById($request->query->getInt('id'));
        $placeType = $this->getPlaceTypeQueryProcessor()->getById($place->getTypeId());

        return
            $this->json(
                [
                    'breadcrumb_links' => $this->getBreadcrumbsBuilder()->getLinksByCityAndPlaceAndPlaceType(
                        $city,
                        $place,
                        $placeType
                    )
                ],
                Response::HTTP_OK
            )
        ;
    }
}
