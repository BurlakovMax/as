<?php

declare(strict_types=1);

namespace App\Frontend\Open\Locations\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Locations\Application\StateQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class StatesController extends AbstractController
{
    use OffsetLimitParams;

    private function getQueryProcessor(): StateQueryProcessor
    {
        return $this->container->get(StateQueryProcessor::class);
    }

    /**
     * @Route("/states/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get state by Id",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="State",
     *         @SWG\Schema(ref=@Model(type=App\Locations\Application\StateData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \App\Locations\Application\StateNotFound
     */
    public function getState(int $id): JsonResponse
    {
        return
            $this->json(
                $this->getQueryProcessor()->getById($id),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/states", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get states",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="States",
     *         @SWG\Items(ref=@Model(type=App\Locations\Application\StateData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \App\Locations\Application\StateBadRequest
     */
    public function getStates(Request $request): JsonResponse
    {
        $search = new SearchQuery(
            new FuzzySearchQuery($request->query->get('query'), ['t.name']),
            $this->createFilter($request),
            [
                new OrderQuery(
                    $request->query->get('property') ?? 't.id',
                    Ordering::getValueOf($request->query->get('ordering') ?? 'asc')
                ),
            ],
            $this->getLimitation($request)
        );

        return $this->json(
            new ListResponse(
                $this->getQueryProcessor()->countByFilterQuery($search),
                $this->getQueryProcessor()->getListByFilterQuery($search)
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @return FilterQuery[]
     */
    private function createFilter(Request $request): array
    {
        $filter = [];

        if ($request->query->has('countryId')) {
            $filter[] = FilterQuery::createEqFilter('countryId', $request->query->get('countryId'));
        }

        if ($request->query->has('active')) {
            $filter[] = FilterQuery::createEqFilter('active', $request->query->get('active'));
        }

        return $filter;
    }
}
