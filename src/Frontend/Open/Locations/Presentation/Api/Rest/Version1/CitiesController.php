<?php

declare(strict_types=1);

namespace App\Frontend\Open\Locations\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Locations\Application\CityQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CitiesController extends AbstractController
{
    use OffsetLimitParams;
    use \App\Frontend\Processors\CityQueryProcessor;

    const COUNTRY_WITH_BEST_CITIES = [
        'USA' => 16046,
        'Canada' => 16047,
        'England' => 41973
    ];

    private function getQueryProcessor(): CityQueryProcessor
    {
        return $this->container->get(CityQueryProcessor::class);
    }

    /**
     * @Route("/cities/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get city by Id",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="City",
     *         @SWG\Schema(ref=@Model(type=App\Locations\Application\CityData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \App\Locations\Application\CityNotFound
     */
    public function getCityById(int $id): JsonResponse
    {
        return
            $this->json(
                $this->getQueryProcessor()->getById($id),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/city_by_url/{stateName}/{cityName}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get city by Url",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="City",
     *         @SWG\Schema(ref=@Model(type=App\Locations\Application\CityData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \App\Locations\Application\CityNotFound
     */
    public function getCityByUrl(string $stateName, string $cityName): JsonResponse
    {
        return $this->json($this->getCity($stateName, $cityName), Response::HTTP_OK);
    }

    /**
     * @Route("/cities", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get cities",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Cities",
     *     @SWG\Items(ref=@Model(type=App\Locations\Application\CityData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \App\Locations\Application\CityBadRequest
     */
    public function getCities(Request $request): JsonResponse
    {
        $search = new SearchQuery(
            new FuzzySearchQuery($request->query->get('query'), ['t.name']),
            $this->createFilter($request),
            [
                new OrderQuery(
                    $request->query->get('property') ?? 't.id',
                    Ordering::getValueOf($request->query->get('ordering') ?? 'asc')
                ),
            ],
            $this->getLimitation($request)
        );

        return $this->json(
            new ListResponse(
                $this->getQueryProcessor()->countByFilterQuery($search),
                $this->getQueryProcessor()->getListByFilterQuery($search)
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/topCities", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="top cities",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Cities",
     *         @SWG\Items(ref=@Model(type=App\Locations\Application\CityData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getTopCities(): JsonResponse
    {
        return $this->json($this->getQueryProcessor()->getTopCities(), Response::HTTP_OK);
    }

    /**
     * @Route("/popularCities", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="popular cities",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Cities",
     *         @SWG\Items(ref=@Model(type=App\Locations\Application\CityData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getPopularCitiesListByParentId(Request $request): JsonResponse
    {
        $cities = $this->getQueryProcessor()->getPopularCitiesListByParentId(
            $request->query->getInt('stateId')
        );

        foreach ($cities as $key => $city) {
            if (in_array($city->getCountryId(), self::COUNTRY_WITH_BEST_CITIES) && !$city->getIsPopular()) {
                unset($cities[$key]);
            }
        }

        return $this->json(
            new ListResponse(
                count($cities),
                $cities
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @return FilterQuery[]
     */
    private function createFilter(Request $request): array
    {
        $filter = [];

        if ($request->query->has('cityIds')) {
            $ids = array_map(fn(string $id): int => (int) $id, $request->query->get('cityIds'));
            $filter[] = new FilterQuery('t.id', ComparisonType::in(), $ids);
        }

        if ($request->query->has('countryId')) {
            $filter[] = FilterQuery::createEqFilter('countryId', $request->query->getInt('countryId'));
        }

        if ($request->query->has('stateId')) {
            $filter[] = FilterQuery::createEqFilter('stateId', $request->query->getInt('stateId'));
        }

        if ($request->query->has('active')) {
            $filter[] = FilterQuery::createEqFilter('active', (bool)$request->query->get('active'));
        }

        return $filter;
    }
}
