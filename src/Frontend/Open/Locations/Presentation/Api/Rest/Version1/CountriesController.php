<?php

declare(strict_types=1);

namespace App\Frontend\Open\Locations\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Frontend\Processors\CountryQueryProcessor;
use App\Locations\Application\LocationTreeBuilder;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CountriesController extends AbstractController
{
    use CountryQueryProcessor;
    use OffsetLimitParams;

    /**
     * @Route("/countries/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get country by Id",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Country",
     *         @SWG\Schema(ref=@Model(type=App\Locations\Application\CountryData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \App\Locations\Application\CountryNotFound
     */
    public function getCountry(int $id): JsonResponse
    {
        return
            $this->json(
                $this->getCountryQueryProcessor()->getById($id),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/countries", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get countries",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Countries",
     *         @SWG\Items(ref=@Model(type=App\Locations\Application\CountryData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getCountries(Request $request): JsonResponse
    {
        $search = new SearchQuery(
            new FuzzySearchQuery($request->query->get('query'), ['t.name']),
            $this->createFilter($request),
            [
                new OrderQuery(
                    $request->query->get('property') ?? 't.name',
                    Ordering::getValueOf($request->query->get('ordering') ?? 'asc')
                ),
            ],
            $this->getLimitation($request)
        );

        return $this->json(
            new ListResponse(
                $this->getCountryQueryProcessor()->countByFilterQuery($search),
                $this->getCountryQueryProcessor()->getListByFilterQuery($search)
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/countries_with_state", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get countries with state tree",
     *     tags={"locations"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Countries",
     *     @SWG\Items(ref=@Model(type=App\Locations\Application\TreeCountryData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getCountriesTreeWithState(): JsonResponse
    {
        return
            $this->json(
              $this->getLocationTreeBuilder()->getLocationTree(true),
                Response::HTTP_OK
            );
    }

    /**
     * @return FilterQuery[]
     */
    private function createFilter(Request $request): array
    {
        $filter = [];

        if ($request->query->has('active')) {
            $filter[] = FilterQuery::createEqFilter('active', $request->query->get('active'));
        }

        return $filter;
    }

    private function getLocationTreeBuilder(): LocationTreeBuilder
    {
        return $this->container->get(LocationTreeBuilder::class);
    }
}
