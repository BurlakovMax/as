<?php

declare(strict_types=1);

namespace App\Frontend\Open\Accounts\Presentation\API\Rest\Version1;

use App\Escorts\Application\EscortTypeConverter;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AccountController extends AbstractController
{
    use EscortQueryProcessor;
    use CityQueryProcessor;

    /**
     * @Route("/accounts/{id}/last_ad", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get last AD by account ID",
     *     tags={"accounts"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Accounts",
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getLastAd(int $id): JsonResponse
    {
        $escort = $this->getEscortQueryProcessor()->getLastActiveByAccountId($id);
        $city = $this->getCityQueryProcessor()->getById($escort->getLocationIds()[0]);

        return $this->json(
            $city->getUrl() . EscortTypeConverter::valueToSlug($escort->getRawType()) . '/' . $escort->getId(),
            Response::HTTP_OK
        );
    }
}