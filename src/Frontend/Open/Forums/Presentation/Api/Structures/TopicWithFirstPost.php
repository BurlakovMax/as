<?php

declare(strict_types=1);

namespace App\Frontend\Open\Forums\Presentation\Api\Structures;

use App\Forums\Application\PostData;
use App\Forums\Application\TopicData;

final class TopicWithFirstPost
{
    private TopicData $topic;

    private PostData $post;

    public function __construct(TopicData $topic, PostData $post)
    {
        $this->topic = $topic;
        $this->post = $post;
    }

    public function getTopic(): TopicData
    {
        return $this->topic;
    }

    public function getPost(): PostData
    {
        return $this->post;
    }
}