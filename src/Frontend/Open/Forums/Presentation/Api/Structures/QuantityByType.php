<?php

declare(strict_types=1);

namespace App\Frontend\Open\Forums\Presentation\Api\Structures;

final class QuantityByType
{
    private int $id;

    private int $locationId;

    private string $name;

    private string $url;

    private int $count;

    public function __construct(int $id, int $locationId, string $name, string $url, int $count)
    {
        $this->id = $id;
        $this->locationId = $locationId;
        $this->name = $name;
        $this->url = $url;
        $this->count = $count;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}