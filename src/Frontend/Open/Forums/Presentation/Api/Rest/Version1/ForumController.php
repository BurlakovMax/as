<?php

declare(strict_types=1);

namespace App\Frontend\Open\Forums\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Frontend\Open\Forums\Presentation\Api\Structures\QuantityByType;
use App\Frontend\Processors\ForumLinkQueryProcessor;
use App\Frontend\Processors\ForumQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ForumController extends AbstractController
{
    use ForumQueryProcessor;
    use ForumLinkQueryProcessor;

    /**
     * @Route("/getForumsByLocationId", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get forums list by LocationId",
     *     tags={"forums"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Forums",
     *         @SWG\Items(ref=@Model(type=App\Forums\Application\ForumData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getForumsByLocationId(Request $request): JsonResponse
    {
        $forums = $this->getForumQueryProcessor()->getByLocationId($request->query->getInt('locationId'));

        $forumsWithIdInKey = [];
        foreach ($forums as $forum) {
            $forumsWithIdInKey[$forum->getId()] = $forum;
        }

        $forumLinks = $this->getForumLinkQueryProcessor()->getByIds(array_keys($forumsWithIdInKey));

        $response = [];
        foreach ($forumLinks as $link) {
            $response[] = new QuantityByType(
                $link->getId(),
                $request->query->getInt('locationId'),
                $link->getName(),
                $link->getUrl(),
                $forumsWithIdInKey[$link->getId()]->getCountTopics()
            );
        }

        return
            $this->json(
                new ListResponse(count($response), $response),
                Response::HTTP_OK
            )
        ;
    }
}
