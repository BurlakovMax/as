<?php

declare(strict_types=1);

namespace App\Frontend\Open\Forums\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Forums\Application\PostQueryProcessor;
use App\Forums\Application\TopicQueryProcessor;
use App\Frontend\Open\Forums\Presentation\Api\Structures\TopicWithFirstPost;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class TopicController extends AbstractController
{
    use OffsetLimitParams;

    private function getQueryProcessor(): TopicQueryProcessor
    {
        return $this->container->get(TopicQueryProcessor::class);
    }

    private function getPostQueryProcessor(): PostQueryProcessor
    {
        return $this->container->get(PostQueryProcessor::class);
    }

    /**
     * @Route("/forumTopics/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get a forumTopic",
     *     tags={"forumTopics"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Forum topic",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getTopic(int $id): JsonResponse
    {
        $topic = $this->getQueryProcessor()->get($id);
        $query = $this->getFirstPost($topic->getId(), $topic->getLocationId());
        $posts = $this->getPostQueryProcessor()->getListBySearchQuery($query);

        return
            $this->json(
                new TopicWithFirstPost($topic, $posts[0]),
                Response::HTTP_OK
            )
        ;
    }

    private function getFirstPost(int $topicId, int $locationId): SearchQuery
    {
        return
            new SearchQuery(
                new FuzzySearchQuery(null, []),
                [
                    FilterQuery::createEqFilter(
                        'topicId',
                        $topicId
                    ),
                    FilterQuery::createEqFilter(
                        'locationId',
                        $locationId
                    )
                ],
                [
                    new OrderQuery('t.createdAt', Ordering::asc())
                ],
                LimitationQueryImmutable::create(0, 1)
            )
        ;
    }

    /**
     * @Route("/forumTopics", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get forum topics list",
     *     tags={"forums"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Forum topics",
     *         @SWG\Items(ref=@Model(type=App\Forums\Application\TopicData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getTopics(Request $request): JsonResponse
    {
        $query = $this->createQuery($request);

        return
            $this->json(
                new ListResponse(
                    $this->getQueryProcessor()->countBySearchQuery($query),
                    $this->getQueryProcessor()->getListBySearchQuery($query)
                ),
                Response::HTTP_OK
            )
        ;
    }

    private function createQuery(Request $request): SearchQuery
    {
        $filters = [];

        if ($request->query->has('forumId')) {
            $filters[] = FilterQuery::createEqFilter(
                'forumId',
                $request->query->getInt('forumId')
            );
        }

        if ($request->query->has('locationId')) {
            $filters[] = FilterQuery::createEqFilter(
                'locationId',
                $request->query->getInt('locationId')
            );
        }

        return new SearchQuery(
            new FuzzySearchQuery(
                $request->query->get('query'),
                [
                    't.title',
                ]
            ),
            $filters,
            [
                new OrderQuery('t.createdAt', Ordering::desc())
            ],
            $this->getLimitation($request)
        );
    }
}
