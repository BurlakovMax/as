<?php

declare(strict_types=1);

namespace App\Frontend\Open\Forums\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Forums\Application\PostQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PostController extends AbstractController
{
    use OffsetLimitParams;

    private function getQueryProcessor(): PostQueryProcessor
    {
        return $this->container->get(PostQueryProcessor::class);
    }

    /**
     * @Route("/forumPosts", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get forum posts list",
     *     tags={"forums"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Forum posts",
     *         @SWG\Items(ref=@Model(type=App\Forums\Application\PostData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getPosts(Request $request): JsonResponse
    {
        $query = $this->createQuery($request);
        $posts = $this->getQueryProcessor()->getListBySearchQuery($query);

        if ($request->query->getInt('offset') === 0) {
            array_shift($posts);
        }

        return
            $this->json(
                new ListResponse(
                    $this->getQueryProcessor()->countBySearchQuery($query) - 1,
                    array_reverse($posts)
                ),
                Response::HTTP_OK
            )
        ;
    }

    private function createQuery(Request $request): SearchQuery
    {
        $filters = [];

        if ($request->query->has('topicId')) {
            $filters[] = FilterQuery::createEqFilter(
                'topicId',
                $request->query->getInt('topicId')
            );
        }

        if ($request->query->has('locationId')) {
            $filters[] = FilterQuery::createEqFilter(
                'locationId',
                $request->query->getInt('locationId')
            );
        }

        if ($request->query->getInt('offset') === 0) {
            $limit = LimitationQueryImmutable::create(
                $request->query->has('offset') ? $request->query->getInt('offset') : null,
                $request->query->has('limit') ? $request->query->getInt('limit') + 1 : null
            );
        } else {
            $limit = $this->getLimitation($request);
        }

        return new SearchQuery(
            new FuzzySearchQuery(
                $request->query->get('query'),
                [
                    't.text',
                ]
            ),
            $filters,
            [
                new OrderQuery('t.createdAt', Ordering::asc())
            ],
            $limit
        );
    }
}
