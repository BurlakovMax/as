<?php

declare(strict_types=1);

namespace App\Frontend\PC\Places\Presentation\Api\Rest\Structures;

use App\Locations\Application\CityData;
use App\Places\Application\PlaceData;

final class PlaceWithCity
{
    private PlaceData $place;
    private CityData $city;

    public function __construct(PlaceData $place, CityData $city)
    {
        $this->place = $place;
        $this->city = $city;
    }

    public function getPlace(): PlaceData
    {
        return $this->place;
    }

    public function getCity(): CityData
    {
        return $this->city;
    }
}