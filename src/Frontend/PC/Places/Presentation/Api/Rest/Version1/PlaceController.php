<?php

declare(strict_types=1);

namespace App\Frontend\PC\Places\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Frontend\PC\Places\Presentation\Api\Rest\Structures\PlaceWithCity;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\CityQueryProcessor;
use App\Places\Application\AccessControl;
use App\Places\Application\ImageUploadProcessor;
use App\Places\Application\PlaceAttributeCommand;
use App\Places\Application\PlaceCommandProcessor;
use App\Places\Application\PlaceData;
use App\Places\Application\PlaceQueryProcessor;
use App\Places\Application\UpdatePlaceCommand;
use App\Places\Domain\Editable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceController extends SecurityCommand
{
    use CityQueryProcessor;
    use OffsetLimitParams;

    /**
     * @Route("/places", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get places by account id",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Places",
     *         @SWG\Items(ref=@Model(type=App\Frontend\PC\Places\Presentation\Api\Rest\Structures\PlaceWithCity::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPlaces(Request $request): JsonResponse
    {
        $filters = [];
        $filters[] = FilterQuery::createEqFilter('t.ownerId', $this->getAccount()->getAccountId());
        if ($request->query->get('status') === 'deleted') {
            $filters[] = new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNotNull(), null);
        } else {
            $filters[] = FilterQuery::createEqFilter('t.editable', Editable::enabled());
            $filters[] = new FilterQuery('t.datesOfChanges.deletedAt', ComparisonType::isNull(), null);
        }

        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            $filters,
            [],
            $this->getLimitation($request)
        );

        $places = $this->getQueryProcessor()->getBySearchQuery($search);

        $cityIds = array_map(fn(PlaceData $place): int => $place->getLocationId(), $places);
        $rawCities = $this->getCityQueryProcessor()->getByIds(...$cityIds);
        $cities = [];
        foreach ($rawCities as $city) {
            $cities[$city->getId()] = $city;
        }

        $placesWithCities = [];
        foreach ($places as $place) {
            if (isset($cities[$place->getLocationId()])) {
                $placesWithCities[] = new PlaceWithCity($place, $cities[$place->getLocationId()]);
            }
        }

        return $this->json(
            new ListResponse(
                $this->getQueryProcessor()->countBySearchQuery($search),
                $placesWithCities,
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/places/{id}", methods={"PUT"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Put(
     *     summary="Update Place",
     *     tags={"places", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function update(int $id, Request $request): JsonResponse
    {
        $this->getAccessControl()->checkAccessToPlaceAsOwner(
            $id,
            $this->getAccount()->getAccountId()
        );

        $command = new UpdatePlaceCommand($id);
        $this->fill($command);

        $attributes = [];

        if (!empty($request->request->get('placeAttributes'))) {
            foreach ($request->request->get('placeAttributes') as $attribute) {
                $attributes[] = new PlaceAttributeCommand(
                    $attribute['attribute_id'],
                    (string) $attribute['value'],
                    isset($attribute['value_text']) ? $attribute['value_text'] : null
                );
            }
        }

        $command->setPlaceAttributes($attributes);

        $this->getCommandProcessor()->update($command);

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/places/{id}/images", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="Delete place images",
     *     tags={"places", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function deleteImages(int $id): JsonResponse
    {
        $this->getAccessControl()->checkAccessToPlaceAsOwner(
            $id,
            $this->getAccount()->getAccountId()
        );

        $place = $this->getQueryProcessor()->getById($id);

        $this->getImageUploadProcessor()->removeByName($place->getImage());
        $this->getImageUploadProcessor()->removeByName($place->getThumbnail());

        $this->getCommandProcessor()->removeImage($id);

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    private function getQueryProcessor(): PlaceQueryProcessor
    {
        return $this->container->get(PlaceQueryProcessor::class);
    }

    private function getCommandProcessor(): PlaceCommandProcessor
    {
        return $this->container->get(PlaceCommandProcessor::class);
    }

    private function getImageUploadProcessor(): ImageUploadProcessor
    {
        return $this->container->get(ImageUploadProcessor::class);
    }

    private function getAccessControl(): AccessControl
    {
        return $this->container->get(AccessControl::class);
    }
}