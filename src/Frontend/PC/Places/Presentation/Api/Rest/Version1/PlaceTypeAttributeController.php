<?php

declare(strict_types=1);

namespace App\Frontend\PC\Places\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Frontend\PC\SecurityCommand;
use App\Places\Application\PlaceAttributeQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceTypeAttributeController extends SecurityCommand
{
    private function getQueryProcessor(): PlaceAttributeQueryProcessor
    {
        return $this->container->get(PlaceAttributeQueryProcessor::class);
    }

    /**
     * @Route("/placeTypeAttributes", methods={"GET"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Get(
     *     summary="Get place attribute by PlaceType Id",
     *     tags={"places", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *         @SWG\Schema(ref=@Model(type=App\Places\Application\PlaceTypeAttributeData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPlaceTypeAttributes(Request $request): JsonResponse
    {
        $attributes = $this->getQueryProcessor()->getByPlaceType($request->query->getInt('placeTypeId'));

        return $this->json(
            new ListResponse(
                count($attributes),
                $attributes
            ),
            Response::HTTP_OK
        );
    }
}