<?php

declare(strict_types=1);

namespace App\Frontend\PC\Places\Presentation\Api\Rest\Version1;

use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\AccessControl\PlacesAccessControl;
use App\Frontend\Processors\PlaceReviewCommandProcessor;
use App\Places\Application\CreatePlaceReviewCommand;
use App\Places\Application\UpdatePlaceReviewCommand;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceReviewController extends SecurityCommand
{
    use PlacesAccessControl;
    use PlaceReviewCommandProcessor;

    /**
     * @Route("/placeReviews", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="add place review",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Place review Id",
     *         @SWG\Schema(type="integer", format="int32")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function create(Request $request): JsonResponse
    {
        $this->getPlacesAccessControl()->checkAccessToPlace($request->request->getInt('placeId'));

        $command = new CreatePlaceReviewCommand(
            $request->request->getInt('placeId'),
            $this->getAccount()->getAccountId(),
            $request->request->getInt('providerId'),
            $request->request->get('comment'),
            $request->request->getInt('star'),
            Request::createFromGlobals()->getClientIp()
        );

        return $this->json(
            [
                'id' => $this->getPlaceReviewCommandProcessor()->add($command)
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/placeReviews/{id}", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="update place review",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function update(int $id, Request $request): JsonResponse
    {
        $this->getPlacesAccessControl()->checkAccessToPlaceReviewByAccount($id, $this->getAccount()->getAccountId());

        $this->getPlaceReviewCommandProcessor()->update(
            new UpdatePlaceReviewCommand(
                $id,
                $request->request->getInt('providerId'),
                $request->request->get('comment'),
                $request->request->getInt('star')
            )
        );

        return $this->json(null, Response::HTTP_OK);
    }

    /**
     * @Route("/placeReviews/{id}", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="remove place review",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function delete(int $id): JsonResponse
    {
        $this->getPlacesAccessControl()->checkAccessToPlaceReviewByAccount($id, $this->getAccount()->getAccountId());
        $this->getPlaceReviewCommandProcessor()->delete($id);

        return $this->json(null, Response::HTTP_OK);
    }
}