<?php

declare(strict_types=1);

namespace App\Frontend\PC\Places\Presentation\Api\Rest\Version1;

use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\AccessControl\PlacesAccessControl;
use App\Frontend\Processors\PlaceReviewCommentCommandProcessor;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceReviewCommentController extends SecurityCommand
{
    use PlacesAccessControl;
    use PlaceReviewCommentCommandProcessor;

    /**
     * @Route("/places/{id}/reviews/{reviewId}/comments", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="add place review comment",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Comment Id",
     *         @SWG\Schema(type="integer", format="int32")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function create(int $id, int $reviewId, Request $request): JsonResponse
    {
        $this->getPlacesAccessControl()->checkAccessToPlaceReview($reviewId);

        return $this->json(
            [
                'id' => $this->getPlaceReviewCommentCommandProcessor()->add(
                            $this->getAccount()->getAccountId(),
                            $reviewId,
                            $request->request->get('comment')
                        )
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/places/{placeId}/reviews/{reviewId}/comments/{id}", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="remove place review comment",
     *     tags={"places"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function delete(int $placeId, int $reviewId, int $id): JsonResponse
    {
        $this->getPlacesAccessControl()->checkAccessToPlaceReviewCommentByAccount(
            $id,
            $this->getAccount()->getAccountId()
        );
        $this->getPlaceReviewCommentCommandProcessor()->delete($id);

        return $this->json(null, Response::HTTP_OK);
    }
}