<?php

declare(strict_types=1);

namespace App\Frontend\PC\Places\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Places\Application\PlaceReviewProvider\CreatePlaceReviewProviderCommand;
use App\Places\Application\PlaceReviewProvider\PlaceReviewProviderCommandProcessor;
use App\Frontend\PC\SecurityCommand;
use App\Places\Application\PlaceReviewProvider\PlaceReviewProviderQueryProcessor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;

final class PlaceReviewProviderController extends SecurityCommand
{
    private function getQueryProcessor(): PlaceReviewProviderQueryProcessor
    {
        return $this->container->get(PlaceReviewProviderQueryProcessor::class);
    }

    private function getCommandProcessor(): PlaceReviewProviderCommandProcessor
    {
        return $this->container->get(PlaceReviewProviderCommandProcessor::class);
    }

    /**
     * @Route("/placeReviewsProviders", methods={"GET"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Get(
     *     summary="Get PlaceProviders by PlaceId",
     *     tags={"placeReviewProviders", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="PlaceReviewProviders",
     *         @SWG\Items(ref=@Model(type=App\Places\Domain\PlaceReviewProvider\PlaceReviewProvider::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getByPlaceId(Request $request): JsonResponse
    {
        $providers = $this->getQueryProcessor()->getByPlaceId(
            $request->query->getInt('placeId')
        );

        return $this->json(
            new ListResponse(
                count($providers),
                $providers
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/placeReviewsProviders", methods={"POST"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Post(
     *     summary="Add new provider to place",
     *     tags={"placeReviewProviders", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="PlaceReviewProvider id",
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function create(): JsonResponse
    {
        $command = new CreatePlaceReviewProviderCommand();
        $this->fill($command);

        return $this->json(
            [
                'id' => $this->getCommandProcessor()->add($command),
            ],
            Response::HTTP_CREATED
        );
    }
}