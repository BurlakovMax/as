<?php

declare(strict_types=1);

namespace App\Frontend\PC\Payments\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\CreditCardQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreditCardController extends SecurityCommand
{
    use OffsetLimitParams;
    use CreditCardQueryProcessor;

    /**
     * @Route("/creditCards", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get account credit cards",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="credit cards",
     *         @SWG\Items(ref=@Model(type=App\Payments\Application\CreditCardData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getCreditCards(Request $request): JsonResponse
    {
        $query = $this->createQuery($request);

        $creditCardsCount = $this->getCreditCardQueryProcessor()->countByAccountId($this->getAccount()->getAccountId());
        $creditCards = $this->getCreditCardQueryProcessor()->getBySearchQuery($query);

        return $this->json(
            new ListResponse($creditCardsCount, $creditCards),
            Response::HTTP_OK
        );
    }

    private function createQuery(Request $request): SearchQuery
    {
        return new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
            ],
            [],
            $this->getLimitation($request)
        );
    }
}
