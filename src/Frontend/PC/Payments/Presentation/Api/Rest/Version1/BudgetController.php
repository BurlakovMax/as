<?php

declare(strict_types=1);

namespace App\Frontend\PC\Payments\Presentation\Api\Rest\Version1;

use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\BudgetQueryProcessor;
use App\Payments\Application\BudgetNotFound;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class BudgetController extends SecurityCommand
{
    use BudgetQueryProcessor;

    /**
     * @Route("/budget", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get current account budget amount",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Budget amount",
     *         @SWG\Schema(
     *             @SWG\Property(property="amount", type="float")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getByAccountId(): JsonResponse
    {
        try {
            $budget = $this->getBudgetQueryProcessor()->getByAccountId($this->getAccount()->getAccountId());
            return $this->json($budget->getAmount(), Response::HTTP_OK);
        } catch (BudgetNotFound $exception) {
            return $this->json(0, Response::HTTP_OK);
        }
    }
}
