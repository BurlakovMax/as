<?php

declare(strict_types=1);

namespace App\Frontend\PC\Payments\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\CreditCardQueryProcessor;
use App\Frontend\Processors\TransactionQueryProcessor;
use App\Payments\Application\CreditCardData;
use App\Payments\Application\CreditCardNotFound;
use App\Payments\Application\TransactionDetailsData;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class TransactionController extends SecurityCommand
{
    use TransactionQueryProcessor;
    use OffsetLimitParams;
    use CreditCardQueryProcessor;

    /**
     * @Route("/transactions", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get transactions",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Transactions",
     *         @SWG\Items(ref=@Model(type=App\Payments\Application\TransactionData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getTransactions(Request $request): JsonResponse
    {
        $transactionDetails = [];
        $transactions = $this->getTransactionQueryProcessor()->getBySearchQuery($this->createQuery($request));
        foreach ($transactions as $transaction) {
            $transactionDetails[] = new TransactionDetailsData(
                $transaction,
                $this->getCard($transaction->getCardId())
            );
        }

        return $this->json(
            new ListResponse(
                $this->getTransactionQueryProcessor()->countByAccountId($this->getAccount()->getAccountId()),
                $transactionDetails
            ),
            Response::HTTP_OK
        );
    }

    private function createQuery(Request $request): SearchQuery
    {
        return new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('payment.accountId', $this->getAccount()->getAccountId()),
            ],
            [
                new OrderQuery(
                    't.' . $request->query->get('property'),
                    Ordering::getValueOf($request->query->get('ordering'))
                )
            ],
            $this->getLimitation($request)
        );
    }

    private function getCard(?int $cardId): ?CreditCardData
    {
        $card = null;
        try {
            if (null !== $cardId) {
                $card = $this->getCreditCardQueryProcessor()->get($cardId);
            }
        } catch (CreditCardNotFound $exception) {/*_*/}

        return $card;
    }
}
