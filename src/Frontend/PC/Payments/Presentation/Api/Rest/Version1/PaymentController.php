<?php

declare(strict_types=1);

namespace App\Frontend\PC\Payments\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\AccessControl\PaymentsAccessControl;
use App\Frontend\Processors\CreditCardQueryProcessor;
use App\Frontend\Processors\PaymentCommandProcessor;
use App\Frontend\Processors\PaymentQueryProcessor;
use App\Payments\Application\CreditCardData;
use App\Payments\Application\CreditCardNotFound;
use App\Payments\Application\PaymentDetailsData;
use App\Payments\Domain\SubscriptionStatus;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

final class PaymentController extends SecurityCommand
{
    use OffsetLimitParams;
    use PaymentQueryProcessor;
    use PaymentCommandProcessor;
    use CreditCardQueryProcessor;
    use PaymentsAccessControl;

    /**
     * @Route("/payments/{id}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get Payment By Id",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Payments",
     *         @SWG\Schema(ref=@Model(type=App\Payments\Application\PaymentDetailsData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getById(int $id): JsonResponse
    {
        $this->getPaymentsAccessControl()->checkAccessToPayment(
            $this->getAccount()->getAccountId(),
            $id
        );

        $payment = $this->getPaymentQueryProcessor()->get($id);

        return $this->json(
            new PaymentDetailsData(
                $payment, $this->getCard($payment->getCardId())
            ),
            Response::HTTP_OK
        );

    }

    private function getCard(?int $cardId): ?CreditCardData
    {
        $card = null;
        try {
            if (null !== $cardId) {
                $card = $this->getCreditCardQueryProcessor()->get($cardId);
            }
        } catch (CreditCardNotFound $exception) {/*_*/}

        return $card;
    }

    /**
     * @Route("/payments", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get payments",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Payments",
     *         @SWG\Items(ref=@Model(type=App\Payments\Application\PaymentDetailsData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPayments(Request $request): JsonResponse
    {
        $payments = $this->getPaymentQueryProcessor()->getBySearchQuery($this->createQuery($request));
        $paymentsCount = $this->getPaymentQueryProcessor()->countBySearchQuery($this->createQuery($request));

        $paymentDetails = [];
        foreach ($payments as $payment) {
            $paymentDetails[] = new PaymentDetailsData($payment, $this->getCard($payment->getCardId()));
        }

        return $this->json(
            new ListResponse(
                $paymentsCount,
                $paymentDetails
            ),
            Response::HTTP_OK
        );
    }

    private function createQuery(Request $request): SearchQuery
    {
        return new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                FilterQuery::createEqFilter('subscriptionStatus', SubscriptionStatus::live()),
            ],
            [
                new OrderQuery(
                    't.' . $request->query->get('property'),
                    Ordering::getValueOf($request->query->get('ordering'))
                )
            ],
            $this->getLimitation($request)
        );
    }

    /**
     * @Route("/subscriptions/{id}/cancel", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="Cancel active subscription",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *      response=204,
     *      description="Nothing",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws Throwable
     */
    public function cancel(int $id): JsonResponse
    {
        $this->getPaymentsAccessControl()->checkAccessToPayment(
            $this->getAccount()->getAccountId(),
            $id
        );

        $this->getPaymentCommandProcessor()->cancel($id);

        return
            $this->json(
                null,
                Response::HTTP_NO_CONTENT
            )
        ;
    }
}
