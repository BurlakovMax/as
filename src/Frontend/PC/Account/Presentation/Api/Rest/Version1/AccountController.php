<?php

declare(strict_types=1);

namespace App\Frontend\PC\Account\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\SearchQuery;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\AccountCommandService;
use App\Frontend\Processors\AccountQueryProcessor;
use App\Frontend\Processors\AccountWireCommandProcessor;
use App\Frontend\Processors\AccountWireUploadProcessor;
use App\Frontend\Processors\AvatarUploadProcessor;
use App\Frontend\Processors\NotificationSettingsCommandProcessor;
use App\Frontend\Processors\NotificationSettingsQueryProcessor;
use App\Frontend\Processors\PlaceReviewQueryProcessor;
use App\Frontend\Processors\PostQueryProcessor;
use App\Security\Application\AccountWireCreateCommand;
use App\Security\Domain\AccountNotFound;
use LazyLemurs\Structures\Email;
use Nelmio\ApiDocBundle\Annotation\Model;
use Ramsey\Uuid\Uuid;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AccountController extends SecurityCommand
{
    use PlaceReviewQueryProcessor;
    use PostQueryProcessor;
    use AccountQueryProcessor;
    use AccountCommandService;
    use AvatarUploadProcessor;
    use NotificationSettingsQueryProcessor;
    use NotificationSettingsCommandProcessor;
    use AccountWireCommandProcessor;
    use AccountWireUploadProcessor;

    /**
     * @Route("/account", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get current account",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Account",
     *         @SWG\Schema(
     *              @SWG\Property(property="account", ref=@Model(type=App\Security\Application\AccountData::class)))
     *         ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws AccountNotFound
     */
    public function getAccountInfo(): JsonResponse
    {
        $account = $this->getAccountQueryProcessor()->getAccount(
            $this->getAccount()->getAccountId()
        );

        return
            $this->json(
                $account,
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @todo Обязятельно перенести в свой модуль и поменять путь к методу /places/reviews_count
     * @Route("/account/reviews_count", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get current account reviews count",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Account reviews count",
     *         @SWG\Schema(
     *             @SWG\Property(property="count", type="integer", format="int32")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getAccountReviewCount(): JsonResponse
    {
        $reviewCount = $this->getPlaceReviewQueryProcessor()->countBySearchQuery(
            new SearchQuery(
                new FuzzySearchQuery(null, []),
                [
                    FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                ],
                [],
                null
            )
        );

        return $this->json($reviewCount, Response::HTTP_OK);
    }

    /**
     * @todo Обязятельно перенести в свой модуль и поменять путь к методу /forums/posts_count
     * @Route("/account/posts_count", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get current account forum posts count",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Account forum posts count",
     *         @SWG\Schema(
     *             @SWG\Property(property="count", type="integer", format="int32")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getForumPostsCount(): JsonResponse
    {
        $forumPostsCount = $this->getPostQueryProcessor()->countBySearchQuery(
            new SearchQuery(
                new FuzzySearchQuery(null, []),
                [
                    FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                ],
                [],
                null
            )
        );

        return $this->json($forumPostsCount, Response::HTTP_OK);
    }

    /**
     * @Route("/account/settings", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get current account settings",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Account settings",
     *         @SWG\Schema(
     *             @SWG\Property(property="settings", type="array", @SWG\Items(type="string"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getAccountSettings(): JsonResponse
    {
        return $this->json(
            $this
                ->getNotificationSettingsQueryProcessor()
                ->findByAccountId($this->getAccount()->getAccountId()),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/account/change_username", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="change username account",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function changeUsername(Request $request): JsonResponse
    {
        $this->getAccountCommandService()->updateUsername(
            $this->getAccount()->getAccountId(),
            $request->request->get('username'),
        );

        return
            $this->json(
                null,
                Response::HTTP_NO_CONTENT
            )
        ;
    }

    /**
     * @Route("/account/change_settings", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="change account settings",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function changeSettings(Request $request): JsonResponse
    {
        $accountId = $this->getAccount()->getAccountId();
        $notifications = $this
            ->getNotificationSettingsQueryProcessor()
            ->findByAccountId($accountId);

        $this->getNotificationSettingsCommandProcessor()->changeSettings(
            $accountId,
            $notifications,
            $request->request->all());

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/account/change_email", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="change email account",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function changeEmail(Request $request): JsonResponse
    {
        return
            $this->json(
                [
                    'confirmationId' => $this->getAccountCommandService()->changeEmail(
                        $this->getAccount()->getAccountId(),
                        new Email($request->request->get('email'))
                    )
                ],
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/account/request_email_confirmation", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Request email confirmation code",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Confirmation ID",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function requestEmailConfirmation(Request $request): JsonResponse
    {
        return
            $this->json(
                [
                    'confirmationId' => $this->getAccountCommandService()->requestConfirmationCode(
                        new Email($request->request->get('email')),
                        $this->getAccount()->getAccountId()
                    )
                ],
                Response::HTTP_OK
            );
    }

    /**
     * @Route("/account/set_email_confirmed", methods={"POST"})
     *
     * @SWG\Put(
     *     summary="confirm email account",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function setEmailConfirmed(Request $request): JsonResponse
    {
        $this->getAccountCommandService()->setEmailConfirmed(
            $this->getAccount()->getAccountId(),
            $request->request->get('confirmationId'),
            $request->request->get('confirmationCode')
        );

        return
            $this->json(
                null,
                Response::HTTP_NO_CONTENT
            )
        ;
    }

    /**
     * @Route("/account/change_password", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="change account password",
     *     tags={"account", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function changePassword(Request $request): JsonResponse
    {
        $this->getAccountCommandService()->updatePassword(
            $request->request->get('password', true),
            $this->getAccount()->getAccountId(),
            $request->request->get('oldPassword', true)
        );

        return
            $this->json(
                null,
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/account/avatar_upload", methods={"POST"})
     *
     * @throws \Throwable
     */
    public function avatarUpload(Request $request): JsonResponse
    {
        $image = $this->getAvatarUploadProcessor()->save(
            (Uuid::uuid4())->toString(),
            $request->files->get('image')
        );

        $this->getAccountCommandService()->setAvatar(
            $this->getAccount()->getAccountId(),
            $image
        );

        return $this->json(
            null,
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/account/wire", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Account wire form",
     *     tags={"account"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Nothing",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function accountWire(Request $request): JsonResponse
    {
        $this->getAccountWireCommandProcessor()->add(
            new AccountWireCreateCommand(
                $this->getAccount()->getAccountId(),
                $request->request->get('name'),
                $request->request->get('amount'),
                $request->request->get('bankName'),
                $request->request->get('image')
            )
        );

        return $this->json(
            null,
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/account/wire_upload", methods={"POST"})
     *
     * @throws \Throwable
     */
    public function wireUpload(Request $request): JsonResponse
    {
        $image = $this->getAccountWireUploadProcessor()->save(
            (Uuid::uuid4())->toString(),
            $request->files->get('image')
        );

        return $this->json(
            $image,
            Response::HTTP_CREATED
        );
    }
}
