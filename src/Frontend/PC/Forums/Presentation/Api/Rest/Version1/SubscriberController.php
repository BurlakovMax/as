<?php

declare(strict_types=1);

namespace App\Frontend\PC\Forums\Presentation\Api\Rest\Version1;

use App\Forums\Application\SubscriberCommandProcessor;
use App\Forums\Application\SubscriberQueryProcessor;
use App\Frontend\PC\SecurityCommand;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

final class SubscriberController extends SecurityCommand
{
    private function getCommandProcessor(): SubscriberCommandProcessor
    {
        return $this->container->get(SubscriberCommandProcessor::class);
    }

    private function getQueryProcessor(): SubscriberQueryProcessor
    {
        return $this->container->get(SubscriberQueryProcessor::class);
    }

    /**
     * @Route("/subscribers", methods={"POST"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Post(
     *     summary="Add subscriber model",
     *     tags={"subscribers", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="subscriber Id",
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws Throwable
     */
    public function create(Request $request): JsonResponse
    {
        return $this->json(
            [
                'id' => $this
                    ->getCommandProcessor()
                    ->subscribe(
                        $request->request->getInt('topicId'),
                        $this->getAccount()->getAccountId()
                    ),
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/subscribers", methods={"DELETE"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Delete(
     *     summary="Delete subscriber model",
     *     tags={"subscribers", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws Throwable
     */
    public function remove(Request $request): JsonResponse
    {
        $this
            ->getCommandProcessor()
            ->unsubscribe(
                $request->request->getInt('topicId'),
                $this->getAccount()->getAccountId()
            )
        ;

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/subscribers", methods={"GET"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Get(
     *     summary="get subscriber by topicId",
     *     tags={"subscribers", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Subscriber",
     *         @SWG\Schema(ref=@Model(type=App\Forums\Application\SubscriberData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getSubscriber(Request $request): JsonResponse
    {
        return
            $this->json(
                $this
                    ->getQueryProcessor()
                    ->getByTopicIdAndAccountId(
                        $request->query->getInt('topicId'),
                        $this->getAccount()->getAccountId()
                    ),
                Response::HTTP_OK
            )
        ;
    }
}
