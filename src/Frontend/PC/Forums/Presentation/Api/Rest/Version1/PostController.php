<?php

declare(strict_types=1);

namespace App\Frontend\PC\Forums\Presentation\Api\Rest\Version1;

use App\Forums\Application\AccessControl;
use App\Forums\Application\CreatePostCommand;
use App\Forums\Application\PostCommandProcessor;
use App\Forums\Application\UpdatePostCommand;
use App\Frontend\PC\SecurityCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

final class PostController extends SecurityCommand
{
    private function getAccessControl(): AccessControl
    {
        return $this->container->get(AccessControl::class);
    }

    private function getCommandProcessor(): PostCommandProcessor
    {
        return $this->container->get(PostCommandProcessor::class);
    }

    /**
     * @Route("/forumPosts", methods={"POST"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Post(
     *     summary="Add forumPost model",
     *     tags={"forumPosts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="forumPost Id",
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws Throwable
     */
    public function create(): JsonResponse
    {
        $command = new CreatePostCommand($this->getAccount()->getAccountId());
        $this->fill($command);

        return $this->json(
            [
                'id' => $this->getCommandProcessor()->create($command),
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/forumPosts/{id}", methods={"PUT"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Put(
     *     summary="Update a forumPost",
     *     tags={"forumPosts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws Throwable
     */
    public function update(int $id): JsonResponse
    {
        $command = new UpdatePostCommand($id, $this->getAccount()->getAccountId());
        $this->fill($command);

        $this->getAccessControl()->checkAccessToPost($command->getId(), $command->getAccountId());
        $this->getCommandProcessor()->update($command);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
