<?php

declare(strict_types=1);

namespace App\Frontend\PC\Forums\Presentation\Api\Rest\Version1;

use App\Forums\Application\AccessControl;
use App\Forums\Application\CreateTopicCommand;
use App\Forums\Application\TopicCommandProcessor;
use App\Forums\Application\UpdateTopicCommand;
use App\Frontend\PC\SecurityCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class TopicController extends SecurityCommand
{
    private function getAccessControl(): AccessControl
    {
        return $this->container->get(AccessControl::class);
    }

    private function getCommandProcessor(): TopicCommandProcessor
    {
        return $this->container->get(TopicCommandProcessor::class);
    }

    /**
     * @Route("/forumTopics", methods={"POST"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Post(
     *     summary="Add forumTopic model",
     *     tags={"forumTopics", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="forumTopic Id",
     *         @SWG\Schema(type="integer")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function create(): JsonResponse
    {
        $command = new CreateTopicCommand($this->getAccount()->getAccountId());
        $this->fill($command);

        $this->getAccessControl()->checkAccessToForum(
            $command->getForumId(),
            $command->getLocationId()
        );

        return $this->json(
            [
                'id' => $this->getCommandProcessor()->create($command),
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/forumTopics/{id}", methods={"PUT"})
     * @Security("is_granted('ROLE_USER')")
     *
     * @SWG\Put(
     *     summary="Update a forumTopic",
     *     tags={"forumTopics", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function update(int $id): JsonResponse
    {
        $command = new UpdateTopicCommand($id);
        $this->fill($command);

        $this->getAccessControl()->checkAccessToTopic(
            $command->getId(),
            $this->getAccount()->getAccountId()
        );

        $this->getCommandProcessor()->update($command);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
