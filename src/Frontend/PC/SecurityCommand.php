<?php

declare(strict_types=1);

namespace App\Frontend\PC;

use App\Core\Frontend\Controller\AbstractRestController;
use App\Security\Application\SessionData;
use App\SecurityServiceSymfonyBridge\BearerToken;
use App\SecurityServiceSymfonyBridge\SessionUser;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

abstract class SecurityCommand extends AbstractRestController
{
    final protected function getAccount(): SessionData
    {
        $token = $this->getTokenStorage()->getToken();
        if (!$token instanceof BearerToken) {
            throw new BadCredentialsException();
        }

        /** @var SessionUser $user */
        $user = $token->getUser();

        return $user->getUserData();
    }

    final private function getTokenStorage(): TokenStorageInterface
    {
        return $this->container->get('security.token_storage');
    }
}
