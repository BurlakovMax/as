<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures;

final class TopUpPrices
{
    /** @var int[] */
    private array $topUps;

    private float $price;

    public function __construct(array $topUps, float $price)
    {
        $this->topUps = $topUps;
        $this->price = $price;
    }

    /**
     * @return int[]
     */
    public function getTopUps(): array
    {
        return $this->topUps;
    }

    public function getPrice(): float
    {
        return $this->price;
    }
}