<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures;

use App\Escorts\Application\LocationData;

final class EscortSponsorPrices
{
    private float $price;

    /** @var LocationData[] */
    private array $allowLocations;

    public function __construct(float $price, array $allowLocations)
    {
        $this->price = $price;
        $this->allowLocations = $allowLocations;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return LocationData[]
     */
    public function getAllowLocations(): array
    {
        return $this->allowLocations;
    }
}