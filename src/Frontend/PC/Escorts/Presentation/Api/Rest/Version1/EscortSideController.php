<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Version1;

use App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures\EscortSidePrices;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortSideQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortSideController extends SecurityCommand
{
    use EscortQueryProcessor;
    use EscortSideQueryProcessor;

    /**
     * @Route("/escorts/{id}/escort_side_price", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort side price",
     *     tags={"payments", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort side price",
     *         @SWG\Items(ref=@Model(type=App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures\EscortSidePrices::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPrice(int $id): JsonResponse
    {
        return $this->json(
            new EscortSidePrices(
                (float) $this->getEscortSideQueryProcessor()->getPrice($this->getAccount()->isAgency()),
                $this->getEscortQueryProcessor()->getLocations($id, $this->getAccount()->getAccountId())
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/{id}/can_buy_escort_side", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Check can buy escort side",
     *     tags={"payments", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="LocationId list available or not available buy escort side",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\CityAvailable::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function canBuy(int $id, Request $request): JsonResponse
    {
        $availables = $this->getEscortSideQueryProcessor()->canBuy(
            $request->query->getInt('type'),
            array_map(fn(string $id): int => (int)$id, $request->query->get('locationIds')),
            $id
        );

        return $this->json(
            $availables,
            Response::HTTP_OK
        );
    }
}