<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Version1;

use App\Escorts\Application\PhoneVerificationCommandProcessor;
use App\Frontend\PC\SecurityCommand;
use LazyLemurs\Structures\PhoneNumber;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PhoneVerificationController extends SecurityCommand
{
    /**
     * @Route("/escorts/request_phone_confirmation", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="request escort phone confirmation",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function requestEscortPhoneConfirmation(Request $request): JsonResponse
    {
        $this->getCommandProcessor()->requestPhoneConfirmation(
            new PhoneNumber($request->request->get('phone', true)),
            $request->request->getInt('id'),
            $this->getAccount()->getAccountId()
        );

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/confirm_phone", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="confirm escort phone",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function confirmPhone(Request $request): JsonResponse
    {
        $this->getCommandProcessor()->confirmPhone(
            new PhoneNumber($request->request->get('phone')),
            (string) $request->request->get('code'),
            $request->request->getInt('id'),
            $this->getUser()->getUserId()
        );

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    private function getCommandProcessor(): PhoneVerificationCommandProcessor
    {
        return $this->container->get(PhoneVerificationCommandProcessor::class);
    }
}
