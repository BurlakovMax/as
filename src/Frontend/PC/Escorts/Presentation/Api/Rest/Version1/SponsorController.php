<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Version1;

use App\Core\Frontend\Application\Data\ListResponse;
use App\Escorts\Application\EscortStickyQueryProcessor;
use App\Frontend\PC\SecurityCommand;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class SponsorController extends SecurityCommand
{
    private function getQueryProcessor(): EscortStickyQueryProcessor
    {
        return $this->container->get(EscortStickyQueryProcessor::class);
    }

    /**
     * @Route("/sponsor/{escortId}", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="get sponsors",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Sponsors",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortStickyData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     */
    public function getSponsorByEscortId(int $escortId): JsonResponse
    {
        $sponsors = $this->getQueryProcessor()->getByEscortId($escortId);

        return $this->json(
            new ListResponse(count($sponsors), $sponsors),
            Response::HTTP_OK
        );
    }

}