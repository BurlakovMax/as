<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Version1;

use App\Core\ApplicationSupport\AccessControl\AccessForbidden;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortWaitingListCommandProcessor;
use App\Frontend\Processors\EscortWaitingListQueryProcessor;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortStickyController extends SecurityCommand
{
    use EscortQueryProcessor;
    use EscortWaitingListCommandProcessor;
    use EscortWaitingListQueryProcessor;

    /**
     * @Route("/escorts/{id}/add_to_waiting_list", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Add escort to Waiting list",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Null response",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function addToWaitingList(int $id, Request $request): JsonResponse
    {
        $this->checkAccess($id, $request->request->getInt('locationId'));

        $this->getEscortWaitingListCommandProcessor()->create(
            $this->getAccount()->getAccountId(),
            $request->request->getInt('locationId'),
            $request->request->getInt('type'),
        );

        return $this->json(
            null,
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/escorts/{id}/has_in_waiting_list", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Has escort in Waiting list",
     *     tags={"payments"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Waiting list",
     *         @SWG\Schema(
     *             @SWG\Property(property="has", type="boolean")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function hasInWaitingList(int $id, Request $request): JsonResponse
    {
        $this->checkAccess($id, $request->query->getInt('locationId'));

        $has = $this->getEscortWaitingListQueryProcessor()->hasBy(
            $this->getAccount()->getAccountId(),
            $request->query->getInt('locationId'),
            $request->query->getInt('type'),
        );

        return $this->json(
            [ 'has' => $has ],
            Response::HTTP_CREATED
        );
    }

    private function checkAccess(int $escortId, int $locationId): void
    {
        $escort = $this->getEscortQueryProcessor()->getActiveByIdAndAccountIdForBuy(
            $escortId,
            $this->getAccount()->getAccountId(),
            [ $locationId ]
        );

        if (!$escort->isShemale() || !$this->getAccount()->isAgency()) {
            throw new AccessForbidden();
        }
    }
}