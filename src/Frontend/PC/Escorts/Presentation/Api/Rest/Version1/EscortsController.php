<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Version1;

use App\Core\Application\Search\ComparisonType;
use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\Ordering;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Frontend\Application\Data\ListResponse;
use App\Escorts\Application\AccessControl;
use App\Core\PresentationSupport\Rest\Params\OffsetLimitParams;
use App\Escorts\Application\CreateEscortCommand;
use App\Escorts\Application\StatusData;
use App\Escorts\Application\UpdateEscortCommand;
use App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures\TopUpPrices;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\CityQueryProcessor;
use App\Frontend\Processors\EscortCommandProcessor;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortSideQueryProcessor;
use App\Frontend\Processors\EscortSponsorQueryProcessor;
use App\Frontend\Processors\EscortStickyQueryProcessor;
use App\Frontend\Processors\ImageUploadProcessor;
use App\Frontend\Processors\VideoUploadProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortsController extends SecurityCommand
{
    use EscortQueryProcessor;
    use EscortCommandProcessor;
    use ImageUploadProcessor;
    use VideoUploadProcessor;
    use CityQueryProcessor;
    use OffsetLimitParams;
    use EscortSponsorQueryProcessor;
    use EscortSideQueryProcessor;
    use EscortStickyQueryProcessor;

    /**
     * @Route("/escorts", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escorts list",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escorts",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\EscortData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscorts(Request $request): JsonResponse
    {
        $query =  new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                new FilterQuery('t.deletedAt', ComparisonType::isNull(), null),
            ],
            [
              new OrderQuery('t.id', Ordering::desc())
            ],
            LimitationQueryImmutable::create(null, null),
        );

        if ($request->query->has('status')) {
            switch ($request->query->get('status')) {
                case 'active':
                    $query = new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                            new FilterQuery('t.status', ComparisonType::in(), [ StatusData::active(), StatusData::invisible() ]),
                        ],
                        [],
                        LimitationQueryImmutable::create(null, null),
                    );

                    break;
                case 'expired':
                    $query = new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                            new FilterQuery('t.status', ComparisonType::in(), [ StatusData::expired(), StatusData::requestedToDelete() ]),
                        ],
                        [],
                        LimitationQueryImmutable::create(null, null),
                    );

                    break;
            }
        }

        $escorts = $this->getEscortQueryProcessor()->getBySearchQuery($query);

        return
            $this->json(
                new ListResponse(count($escorts), $escorts),
                Response::HTTP_OK
            )
        ;
    }

    /**
     * @Route("/escorts/counters", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escorts counters by status",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Account Escorts counters by status",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscortCount(): JsonResponse
    {
        return $this->json(
            [
                'active' => $this->getEscortQueryProcessor()->countBySearchQuery(
                    new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                            new FilterQuery('t.deletedAt', ComparisonType::isNull(), null),
                            new FilterQuery('t.status', ComparisonType::in(), [ StatusData::active(), StatusData::invisible() ]),
                        ],
                        [],
                        LimitationQueryImmutable::create(null, null),
                    )
                ),
                'expired' => $this->getEscortQueryProcessor()->countBySearchQuery(
                    new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                            new FilterQuery('t.deletedAt', ComparisonType::isNull(), null),
                            new FilterQuery('t.status', ComparisonType::in(), [ StatusData::expired() ]),
                        ],
                        [],
                        LimitationQueryImmutable::create(null, null),
                    )
                ),
                'inVerification' => $this->getEscortQueryProcessor()->countBySearchQuery(
                    new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                            new FilterQuery('t.deletedAt', ComparisonType::isNull(), null),
                            new FilterQuery('t.status', ComparisonType::in(), [ StatusData::inVerification() ]),
                        ],
                        [],
                        LimitationQueryImmutable::create(null, null),
                    )
                ),
                'processOfCreation' => $this->getEscortQueryProcessor()->countBySearchQuery(
                    new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('accountId', $this->getAccount()->getAccountId()),
                            new FilterQuery('t.deletedAt', ComparisonType::isNull(), null),
                            new FilterQuery('t.status', ComparisonType::in(), [ StatusData::processOfCreation() ]),
                        ],
                        [],
                        LimitationQueryImmutable::create(null, null),
                    )
                ),
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Add escort model",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Escort id and link token",
     *         @SWG\Schema(ref=@Model(type=App\Escorts\Application\CreateEscortResultData::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function createEscort(): JsonResponse
    {
        $command = new CreateEscortCommand(
            $this->getAccount()->getAccountId(),
        );

        $this->fill($command);

        return $this->json(
            $this->getEscortCommandProcessor()->create($command),
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/escorts/{id}", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="Update escort model",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function updateEscort(int $id, Request $request): JsonResponse
    {
        $accountId = $this->getAccount()->getAccountId();
        $this->getAccessControl()->checkAccessToEscort($id, $accountId);
        $stateIds = [];

        if ($request->request->has('locationIds') && !empty($request->get('locationIds'))) {
            foreach ($request->request->get('locationIds') as $location) {
                $stateIds[$location] = $this->getCityQueryProcessor()->getById((int)$location)->getStateId();
            }
        }

        $command = new UpdateEscortCommand($id, $accountId);
        $this->fill($command);

        $command->setBodyType($request->get('bodyType'));
        $command->setEmailVisibility($request->get('emailVisibility'));
        $command->setEthnicity($request->get('ethnicity'));
        $command->setEyeColor($request->get('eyeColor'));
        $command->setHairColor($request->get('hairColor'));
        $command->setKitty($request->get('kitty'));
        $command->setStateIds($stateIds);

        $this->getEscortCommandProcessor()->update($command);

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/{id}/thumbnail", methods={"PUT"})
     *
     * @SWG\Put(
     *     summary="Update escort thumbnail",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function updateEscortThumbnail(int $id,  Request $request): JsonResponse
    {
        $accountId = $this->getAccount()->getAccountId();
        $this->getAccessControl()->checkAccessToEscort($id, $accountId);

        $this->getEscortCommandProcessor()->setThumbnail($id, $request->request->get('thumbnail'));

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/{id}/move_to_top", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Move Escort to top and remove 1 credit from Account",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function moveEscortToTop(int $id): JsonResponse
    {
        $accountId = $this->getAccount()->getAccountId();
        $this->getAccessControl()->checkAccessToEscort($id, $accountId);

        $this->getEscortCommandProcessor()->moveToTop($id);

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/top_ups_price", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort TopUps price",
     *     tags={"payments", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="TopUps price",
     *         @SWG\Items(ref=@Model(type=App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures\TopUpPrices::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getTopUpPrices(): JsonResponse
    {
        return $this->json(
            new TopUpPrices(
                $this->getEscortQueryProcessor()->getTopUpsOptions(),
                (float) $this->getEscortQueryProcessor()->getEscortTopUpPrice($this->getAccount()->isAgency())
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/{id}/change_location", methods={"PUT"})
     *
     * @SWG\Post(
     *     summary="Move Escort to another city",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function changeLocation(int $id, Request $request): JsonResponse
    {
        $accountId = $this->getAccount()->getAccountId();
        $this->getAccessControl()->checkAccessToEscort($id, $accountId);

        $this->getEscortCommandProcessor()->changeLocation(
            $id,
            $request->request->getInt('fromCityId'),
            $request->request->getInt('toCityId')
        );

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/{id}/duplicate", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Duplicate Escort",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=201,
     *         description="Duplicate Escort"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function duplicate(int $id, Request $request): JsonResponse
    {
        $accountId = $this->getAccount()->getAccountId();
        $this->getAccessControl()->checkAccessToEscort($id, $accountId);

        return
            $this->json(
                $this->getEscortCommandProcessor()->duplicate(
                    $id,
                    $request->request->get('type')
                ),
                Response::HTTP_CREATED
            )
        ;
    }

    /**
     * @Route("/link_escort_to_account", methods={"POST"})
     *
     * @SWG\Post(
     *     summary="Link escort to current account",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     *
     * @throws \Throwable
     */
    public function linkToAccount(Request $request): JsonResponse
    {
        $this->getEscortCommandProcessor()->linkToAccount(
            $this->getAccount()->getAccountId(),
            $request->request->getInt('escortId'),
        );

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/{id}", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="Set escort to delete list",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function delete(int $id): JsonResponse
    {
        $this->getAccessControl()->checkAccessToEscort(
            $id,
            $this->getAccount()->getAccountId()
        );

        $this->getEscortCommandProcessor()->removeEscort($id);

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/{id}/images", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="Delete escort images",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function deleteImages(int $id, Request $request): JsonResponse
    {
        $this->getAccessControl()->checkAccessToEscort(
            $id,
            $this->getAccount()->getAccountId()
        );

        $this->getEscortCommandProcessor()->removeImages($id, $request->request->get('images'));
        foreach ($request->request->get('images') as $image) {
            $this->getImageUploadProcessor()->removeByName($image);
        }

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/{id}/videos", methods={"DELETE"})
     *
     * @SWG\Delete(
     *     summary="Delete escort videos",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=204,
     *         description="Null response"
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function deleteVideos(int $id, Request $request): JsonResponse
    {
        $this->getAccessControl()->checkAccessToEscort(
            $id,
            $this->getAccount()->getAccountId()
        );

        $this->getEscortCommandProcessor()->removeVideos($id, $request->request->get('videos'));
        foreach ($request->request->get('videos') as $video) {
            $this->getVideoUploadProcessor()->removeByName($video);
        }
        foreach ($request->request->get('images') as $image) {
            $this->getVideoUploadProcessor()->removeByName($image);
        }

        return $this->json(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route("/escorts/{id}/sponsorship", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort sponsorship by escort ID",
     *     tags={"escorts", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort sponsorship data",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getEscortSponsorship(int $id, Request $request): JsonResponse
    {
        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('t.escortId', $id),
            ],
            [],
            $this->getLimitation($request)
        );

        return $this->json(
            [
                'escortSponsorLocationIds' => $this->getEscortSponsorQueryProcessor()->getBySearchQuery($search),
                'escortSideLocationIds' => $this->getEscortSideQueryProcessor()->getBySearchQuery($search),
                'escortStickyWeekLocationIds' => $this->getEscortStickyQueryProcessor()->getBySearchQuery(
                    new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('t.escortId', $id),
                            FilterQuery::createEqFilter('t.period.inDays', 7),
                        ],
                        [],
                        $this->getLimitation($request)
                    )
                ),
                'escortStickyMonthLocationIds' => $this->getEscortStickyQueryProcessor()->getBySearchQuery(
                    new SearchQuery(
                        new FuzzySearchQuery(null, []),
                        [
                            FilterQuery::createEqFilter('t.escortId', $id),
                            FilterQuery::createEqFilter('t.period.inDays', 30),
                        ],
                        [],
                        $this->getLimitation($request)
                    )
                ),
            ],
            Response::HTTP_OK
        );
    }

    private function getAccessControl(): AccessControl
    {
        return $this->container->get(AccessControl::class);
    }
}
