<?php

declare(strict_types=1);

namespace App\Frontend\PC\Escorts\Presentation\Api\Rest\Version1;

use App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures\EscortSponsorPrices;
use App\Frontend\PC\SecurityCommand;
use App\Frontend\Processors\EscortQueryProcessor;
use App\Frontend\Processors\EscortSponsorQueryProcessor;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EscortSponsorController extends SecurityCommand
{
    use EscortQueryProcessor;
    use EscortSponsorQueryProcessor;

    /**
     * @Route("/escorts/{id}/escort_sponsor_price", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Get escort sponsor price",
     *     tags={"payments", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Escort sponsor price",
     *         @SWG\Items(ref=@Model(type=App\Frontend\PC\Escorts\Presentation\Api\Rest\Structures\EscortSponsorPrices::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function getPrice(int $id): JsonResponse
    {
        return $this->json(
            new EscortSponsorPrices(
                (float) $this->getEscortSponsorQueryProcessor()->getPrice($this->getAccount()->isAgency()),
                $this->getEscortQueryProcessor()->getLocations($id, $this->getAccount()->getAccountId())
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/escorts/{id}/can_buy_escort_sponsor", methods={"GET"})
     *
     * @SWG\Get(
     *     summary="Check can buy escort side",
     *     tags={"payments", "pc"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="LocationId list available or not available buy escort sponsor",
     *         @SWG\Items(ref=@Model(type=App\Escorts\Application\CityAvailable::class))
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             @SWG\Property(property="code", type="integer", format="int32")
     *         )
     *     )
     * )
     */
    public function canBuy(int $id, Request $request): JsonResponse
    {
        $availables = $this->getEscortSponsorQueryProcessor()->canBuy(
            array_map(fn(string $id): int => (int)$id, $request->query->get('locationIds')),
            $id
        );

        return $this->json(
            $availables,
            Response::HTTP_OK
        );
    }
}