<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\PhoneBlacklistQueryProcessor as QueryProcessor;

trait PhoneBlacklistQueryProcessor
{
    final protected function getPhoneBlacklistQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
