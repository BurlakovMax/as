<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\AccountCommandService as CommandService;

trait AccountCommandService
{
    final protected function getAccountCommandService(): CommandService
    {
        return $this->container->get(CommandService::class);
    }
}