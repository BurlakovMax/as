<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use \App\Payments\Application\PaymentQueryProcessor as QueryProcessor;

trait PaymentQueryProcessor
{
    final protected function getPaymentQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}