<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Forums\Application\TopicCommandProcessor as CommandProcessor;

trait TopicCommandProcessor
{
    final protected function getTopicCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}