<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use \App\Payments\Application\TransactionQueryProcessor as QueryProcessor;

trait TransactionQueryProcessor
{
    final protected function getTransactionQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}