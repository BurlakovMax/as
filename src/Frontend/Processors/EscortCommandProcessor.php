<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortCommandProcessor as CommandProcessor;

trait EscortCommandProcessor
{
    final protected function getEscortCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}