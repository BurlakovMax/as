<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\PromocodeCommandProcessor as CommandProcessor;

trait PromocodeCommandProcessor
{
    final protected function getPromocodeCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
