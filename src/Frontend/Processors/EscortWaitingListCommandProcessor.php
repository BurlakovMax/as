<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortWaitingListCommandProcessor as QueryProcessor;

trait EscortWaitingListCommandProcessor
{
    final protected function getEscortWaitingListCommandProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}