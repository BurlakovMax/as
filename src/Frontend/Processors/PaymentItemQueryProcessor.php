<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\PaymentItemQueryProcessor as QueryProcessor;

trait PaymentItemQueryProcessor
{
    final protected function getPaymentItemQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}