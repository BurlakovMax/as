<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\ImageUploadProcessor as UploadProcessor;

trait ImageUploadProcessor
{
    final protected function getImageUploadProcessor(): UploadProcessor
    {
        return $this->container->get(UploadProcessor::class);
    }
}