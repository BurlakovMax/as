<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\BannedCreditCardQueryProcessor as QueryProcessor;

trait BannedCreditCartQueryProcessor
{
    final protected function getBannedCreditCartQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
