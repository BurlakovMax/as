<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\ActivityLog\Application\QueryProcessor;

trait ActivityLogQueryProcessor
{
    final protected function getActivityLogQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}