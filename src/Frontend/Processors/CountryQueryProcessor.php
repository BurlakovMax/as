<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Locations\Application\CountryQueryProcessor as QueryProcessor;

trait CountryQueryProcessor
{
    final protected function getCountryQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}