<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceReviewCommentCommandProcessor as CommandProcessor;

trait PlaceReviewCommentCommandProcessor
{
    final protected function getPlaceReviewCommentCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}