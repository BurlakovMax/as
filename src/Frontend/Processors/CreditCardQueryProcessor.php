<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\CreditCardQueryProcessor as QueryProcessor;

trait CreditCardQueryProcessor
{
    final protected function getCreditCardQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}