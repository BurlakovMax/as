<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\BudgetQueryProcessor as QueryProcessor;

trait BudgetQueryProcessor
{
    final protected function getBudgetQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
