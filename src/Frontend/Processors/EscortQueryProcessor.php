<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortQueryProcessor as QueryProcessor;

trait EscortQueryProcessor
{
    final protected function getEscortQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}