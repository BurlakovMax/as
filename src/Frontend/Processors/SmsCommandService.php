<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\SmsGate\Application\SmsCommandService as CommandService;

trait SmsCommandService
{
    final protected function getSmsCommandService(): CommandService
    {
        return $this->container->get(CommandService::class);
    }
}
