<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Forums\Application\ForumLinkQueryProcessor as QueryProcessor;

trait ForumLinkQueryProcessor
{
    final protected function getForumLinkQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}