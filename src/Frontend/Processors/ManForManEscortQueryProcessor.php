<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\ManForManEscortQueryProcessor as QueryProcessor;

trait ManForManEscortQueryProcessor
{
    final protected function getManForManEscortQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
