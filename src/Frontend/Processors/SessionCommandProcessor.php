<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\SessionCommandProcessor as CommandProcessor;

trait SessionCommandProcessor
{
    final protected function getSessionCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}