<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceReviewCommentQueryProcessor as CommandProcessor;

trait PlaceReviewCommentQueryProcessor
{
    final protected function getPlaceReviewCommentQueryProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}