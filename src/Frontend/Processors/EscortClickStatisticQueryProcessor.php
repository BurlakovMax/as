<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortClickStatisticQueryProcessor as QueryProcessor;

trait EscortClickStatisticQueryProcessor
{
    final protected function getEscortClickStatisticQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
