<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\PhoneVerificationQueryProcessor as QueryProcessor;

trait PhoneVerificationQueryProcessor
{
    final protected function getPhoneVerificationQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}