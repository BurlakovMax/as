<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\VideoUploadProcessor as UploadProcessor;

trait VideoUploadProcessor
{
    final protected function getVideoUploadProcessor(): UploadProcessor
    {
        return $this->container->get(UploadProcessor::class);
    }
}