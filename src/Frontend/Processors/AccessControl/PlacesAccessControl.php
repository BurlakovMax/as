<?php

declare(strict_types=1);

namespace App\Frontend\Processors\AccessControl;

use App\Places\Application\AccessControl;

trait PlacesAccessControl
{
    final protected function getPlacesAccessControl(): AccessControl
    {
        return $this->container->get(AccessControl::class);
    }
}