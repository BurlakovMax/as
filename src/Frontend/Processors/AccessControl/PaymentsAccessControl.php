<?php

declare(strict_types=1);

namespace App\Frontend\Processors\AccessControl;

use App\Payments\Application\AccessControl;

trait PaymentsAccessControl
{
    final protected function getPaymentsAccessControl(): AccessControl
    {
        return $this->container->get(AccessControl::class);
    }
}