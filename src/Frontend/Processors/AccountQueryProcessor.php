<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\AccountQueryProcessor as QueryProcessor;

trait AccountQueryProcessor
{
    final protected function getAccountQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}