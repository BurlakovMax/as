<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceCommandProcessor as CommandProcessor;

trait PlaceCommandProcessor
{
    final protected function getPlaceCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
