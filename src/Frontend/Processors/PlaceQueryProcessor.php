<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceQueryProcessor as QueryProcessor;

trait PlaceQueryProcessor
{
    final protected function getPlaceQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}