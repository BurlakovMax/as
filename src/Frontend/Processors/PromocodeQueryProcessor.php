<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\PromocodeQueryProcessor as QueryProcessor;

trait PromocodeQueryProcessor
{
    final protected function getPromocodeQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
