<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\PhoneWhitelistCommandProcessor as CommandProcessor;

trait PhoneWhitelistCommandProcessor
{
    final protected function getPhoneWhitelistCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
