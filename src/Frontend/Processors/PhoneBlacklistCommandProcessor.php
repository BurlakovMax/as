<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\PhoneBlacklistCommandProcessor as CommandProcessor;

trait PhoneBlacklistCommandProcessor
{
    final protected function getPhoneBlacklistCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
