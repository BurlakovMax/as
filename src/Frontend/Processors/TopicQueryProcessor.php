<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Forums\Application\TopicQueryProcessor as QueryProcessor;

trait TopicQueryProcessor
{
    final protected function getTopicQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}