<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use \App\Payments\Application\PaymentCommandProcessor as QueryProcessor;

trait PaymentCommandProcessor
{
    final protected function getPaymentCommandProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}