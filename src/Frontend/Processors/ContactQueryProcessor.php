<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Contacts\Application\ContactQueryProcessor as QueryProcessor;

trait ContactQueryProcessor
{
    final protected function getContactQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
