<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\SpammerCommandProcessor as CommandProcessor;

trait SpammerCommandProcessor
{
    final protected function getSpammerCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
