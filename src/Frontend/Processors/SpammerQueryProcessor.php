<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\SpammerQueryProcessor as QueryProcessor;

trait SpammerQueryProcessor
{
    final protected function getSpammerQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
