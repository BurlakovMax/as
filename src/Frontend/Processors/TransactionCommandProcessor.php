<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\TransactionCommandProcessor as CommandProcessor;

trait TransactionCommandProcessor
{
    final protected function getTransactionCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}