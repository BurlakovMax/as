<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\NotificationSettingsQueryProcessor as QueryProcessor;

trait NotificationSettingsQueryProcessor
{
    final protected function getNotificationSettingsQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}