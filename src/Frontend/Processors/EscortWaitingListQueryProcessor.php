<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortWaitingListQueryProcessor as QueryProcessor;

trait EscortWaitingListQueryProcessor
{
    final protected function getEscortWaitingListQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}