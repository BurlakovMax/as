<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\AvatarUploadProcessor as UploadProcessor;

trait AvatarUploadProcessor
{
    final protected function getAvatarUploadProcessor(): UploadProcessor
    {
        return $this->container->get(UploadProcessor::class);
    }
}