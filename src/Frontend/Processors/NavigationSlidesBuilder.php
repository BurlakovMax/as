<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Frontend\Twig\Support\NavigationSlides\NavigationSlidesBuilder as Builder;

trait NavigationSlidesBuilder
{
    final protected function getNavigationSlidesBuilder(): Builder
    {
        return $this->container->get(Builder::class);
    }
}
