<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Locations\Application\StateQueryProcessor as QueryProcessor;

trait StateQueryProcessor
{
    final protected function getStateQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}