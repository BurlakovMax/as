<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\AccountWireUploadProcessor as UploadProcessor;

trait AccountWireUploadProcessor
{
    final protected function getAccountWireUploadProcessor(): UploadProcessor
    {
        return $this->container->get(UploadProcessor::class);
    }
}
