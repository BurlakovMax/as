<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Frontend\Twig\Support\BreadcrumbsBuilder as Builder;

trait BreadcrumbsBuilder
{
    final protected function getBreadcrumbsBuilder(): Builder
    {
        return $this->container->get(Builder::class);
    }
}