<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortClickStatisticCommandProcessor as CommandProcessor;

trait EscortClickStatisticCommandProcessor
{
    final protected function getEscortClickStatisticCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
