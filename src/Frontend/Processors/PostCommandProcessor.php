<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Forums\Application\PostCommandProcessor as CommandProcessor;

trait PostCommandProcessor
{
    final protected function getPostCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}