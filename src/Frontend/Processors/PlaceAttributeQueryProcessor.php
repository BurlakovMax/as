<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceAttributeQueryProcessor as QueryProcessor;

trait PlaceAttributeQueryProcessor
{
    final protected function getPlaceAttributeQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}