<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortStickyQueryProcessor as QueryProcessor;

trait EscortStickyQueryProcessor
{
    final protected function getEscortStickyQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}