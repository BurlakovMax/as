<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Forums\Application\ForumQueryProcessor as QueryProcessor;

trait ForumQueryProcessor
{
    final protected function getForumQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}