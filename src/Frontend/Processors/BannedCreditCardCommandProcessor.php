<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Payments\Application\BannedCreditCardCommandProcessor as CommandProcessor;

trait BannedCreditCardCommandProcessor
{
    final protected function getBannedCreditCardCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
