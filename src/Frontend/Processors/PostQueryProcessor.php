<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Forums\Application\PostQueryProcessor as QueryProcessor;

trait PostQueryProcessor
{
    final protected function getPostQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}