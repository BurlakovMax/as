<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceTypeQueryProcessor as QueryProcessor;

trait PlaceTypeQueryProcessor
{
    final protected function getPlaceTypeQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}