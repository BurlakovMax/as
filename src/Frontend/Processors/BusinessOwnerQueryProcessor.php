<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\BusinessOwner\BusinessOwnerQueryProcessor as QueryProcessor;

trait BusinessOwnerQueryProcessor
{
    final protected function getBusinessOwnerQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}