<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Locations\Application\TopCityQueryProcessor as QueryProcessor;

trait TopCityQueryProcessor
{
    final protected function getTopCityQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}