<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Frontend\Twig\Support\QuantityList\QuantityListBuilder as Builder;

trait QuantityListBuilder
{
    final protected function getQuantityListBuilder(): Builder
    {
        return $this->container->get(Builder::class);
    }
}
