<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Locations\Application\CityData;
use App\Locations\Application\CityQueryProcessor as QueryProcessor;

trait CityQueryProcessor
{
    final protected function getCityQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }

    /**
     * @throws \App\Locations\Application\CityNotFound
     */
    final protected function getCity(?string $state, string $city): CityData
    {
        $url = null === $state ? $city : '/' . $state . '/' . $city . '/';
        return $this->getCityQueryProcessor()->getByUrl($url);
    }
}