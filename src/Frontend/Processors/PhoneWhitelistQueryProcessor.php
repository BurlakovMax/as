<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\PhoneWhitelistQueryProcessor as QueryProcessor;

trait PhoneWhitelistQueryProcessor
{
    final protected function getPhoneWhitelistQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}
