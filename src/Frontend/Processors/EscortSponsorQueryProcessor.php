<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortSponsorQueryProcessor as QueryProcessor;

trait EscortSponsorQueryProcessor
{
    final protected function getEscortSponsorQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}