<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\EmailGate\Application\EmailCommandProcessor as CommandProcessor;

trait EmailCommandProcessor
{
    final protected function getEmailCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
