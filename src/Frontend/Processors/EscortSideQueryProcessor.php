<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Escorts\Application\EscortSideQueryProcessor as QueryProcessor;

trait EscortSideQueryProcessor
{
    final protected function getEscortSideQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}