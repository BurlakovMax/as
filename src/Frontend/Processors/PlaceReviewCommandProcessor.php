<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceReviewCommandProcessor as QueryProcessor;

trait PlaceReviewCommandProcessor
{
    final protected function getPlaceReviewCommandProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}