<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Places\Application\PlaceReviewQueryProcessor as QueryProcessor;

trait PlaceReviewQueryProcessor
{
    final protected function getPlaceReviewQueryProcessor(): QueryProcessor
    {
        return $this->container->get(QueryProcessor::class);
    }
}