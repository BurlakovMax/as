<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\NotificationSettingsCommandProcessor as CommandProcessor;

trait NotificationSettingsCommandProcessor
{
    final protected function getNotificationSettingsCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}