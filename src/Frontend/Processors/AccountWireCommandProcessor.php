<?php

declare(strict_types=1);

namespace App\Frontend\Processors;

use App\Security\Application\AccountWireCommandProcessor as CommandProcessor;

trait AccountWireCommandProcessor
{
    final protected function getAccountWireCommandProcessor(): CommandProcessor
    {
        return $this->container->get(CommandProcessor::class);
    }
}
