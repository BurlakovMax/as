<?php

declare(strict_types=1);

namespace App\Frontend\Mocks;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CryptoPayController extends AbstractController
{
    /**
     * @Route("/mock", methods={"POST", "GET"})
     */
    public function mock(Request $request): JsonResponse
    {
        switch ($request->request->get('method')) {
            case "getnewaddress":
                return $this->getnewaddress(); break;
//            case "listtransactions": return $this->listtransactions(); break;
//            case "gettransaction": return $this->gettransaction($params[0]); break;
//            case "getbalance": return $this->getbalance(); break;
//            case "getreceivedbylabel": return $this->getreceivedbylabel(); break;
//            case "walletpassphrase": return $this->success(""); break;
//            case "sendtoaddress": return $this->sendtoaddress(); break;
//            case "walletlock": return $this->success(""); break;
        }
    }

    private function getnewaddress(): JsonResponse
    {
        return $this->success('1b' . md5((string)time()));
    }

    private function success(string $result): JsonResponse
    {
        return $this->json([
            'jsonrpc' => '2.0',
            'result' => $result,
            'error' => null,
            'id' => 0,
        ], Response::HTTP_OK);
    }
}