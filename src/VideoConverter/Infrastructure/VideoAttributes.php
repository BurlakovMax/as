<?php

declare(strict_types=1);

namespace App\VideoConverter\Infrastructure;

final class VideoAttributes
{
    private int $width;

    private int $height;

    private int $duration;

    private int $rotation;

    public function __construct(int $width, int $height, int $duration, int $rotation)
    {
        $this->width = $width;
        $this->height = $height;
        $this->duration = $duration;
        $this->rotation = $rotation;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getRotation(): int
    {
        return $this->rotation;
    }
}