<?php

declare(strict_types=1);

namespace App\VideoConverter\Infrastructure;

use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

final class FfmpegConverter implements VideoConverter
{
    private int $maxWidth;

    private int $maxHeight;

    private int $maxDuration;

    private string $maxRate;

    private string $bufferSize;

    private string $processingPath;

    private string $currentDir;

    private string $rootDir;

    private string $publicDir;

    private string $tmpDir;

    private LoggerInterface $logger;

    public function __construct(
        int $maxWidth,
        int $maxHeight,
        int $maxDuration,
        string $maxRate,
        string $bufferSize,
        string $processingPath,
        string $rootDir,
        string $publicDir,
        LoggerInterface $logger
    ) {
        $this->maxWidth = $maxWidth;
        $this->maxHeight = $maxHeight;
        $this->maxDuration = $maxDuration;
        $this->maxRate = $maxRate;
        $this->bufferSize = $bufferSize;
        $this->processingPath = $processingPath;
        $this->rootDir = $rootDir;
        $this->publicDir = $publicDir;
        $this->logger = $logger;
        $this->currentDir = getcwd();

        $this->configure();
    }

    private function configure(): void
    {
        $subDir = Uuid::uuid4();
        $tmpDir = $this->processingPath . $subDir->toString();
        mkdir($tmpDir, 0777, true);
        chdir($tmpDir);
        $this->tmpDir = $tmpDir;
    }

    private function createTargetDir(string $saveToPath): void
    {
        if (is_dir($saveToPath)) {
            return;
        }

        mkdir($saveToPath, 0777, true);
    }

    private function createTmpFilePath(string $name): string
    {
        $tmpFilename = 'tmp_' . $name . '.mp4';
        return $this->tmpDir . '/' . $tmpFilename;
    }

    private function clearArtefactsAfterProcessed(string $name, string $srcPathToUploadedFile): void
    {
        @unlink($srcPathToUploadedFile);

        chdir($this->currentDir);
        @rmdir($this->tmpDir);

        @unlink("./ffmpeg2pass-0.log");
        @unlink("./ffmpeg2pass-0.log.mbtree");
    }

    public function convert(string $name, string $srcPathToUploadedFile, string $saveToPath): void
    {
        $fullSrcPathToUploadedFile = $this->rootDir . $this->publicDir . $srcPathToUploadedFile;
        $fullSrcPathToSave = $this->rootDir . $this->publicDir . $saveToPath;

        $this->createTargetDir($fullSrcPathToSave);

        $attributes = $this->getVideoAttributes($name, $fullSrcPathToUploadedFile);
        $duration = $this->getDuration($attributes->getDuration());

        $this->convertFirstTime($fullSrcPathToUploadedFile, $attributes, $duration);
        $this->convertSecondTime($name, $fullSrcPathToUploadedFile, $attributes, $duration);

        $this->replaceResult($name, $fullSrcPathToSave);
        $this->createScreenshot($name, $fullSrcPathToSave, $attributes);

        $this->clearArtefactsAfterProcessed($name, $fullSrcPathToUploadedFile);
    }

    private function convertFirstTime(string $srcPathToUploadedFile, VideoAttributes $attributes, int $duration): void
    {
        $command = 'ffmpeg -y -i ' . $srcPathToUploadedFile . ' -vcodec libx264 -vprofile main -preset slow -vf scale=' . $attributes->getWidth() . ':' . $attributes->getHeight() . ' -b:v ' . $this->maxRate . ' -maxrate ' . $this->maxRate . ' -bufsize ' . $this->bufferSize . ' -t ' . $duration . ' -threads 0 -pass 1 -an -f mp4 /dev/null 2>&1';
        exec($command, $output, $ret);
        if ($ret) {
            $this->logger->error('FfmpegConverter: convertFirstTime, command=' . $command . ', output=' . print_r($output, true));
        }
    }

    private function convertSecondTime(string $name, string $srcPathToUploadedFile, VideoAttributes $attributes, int $duration): void
    {
        $command = 'ffmpeg -y -i ' . $srcPathToUploadedFile . ' -vcodec libx264 -vprofile main -preset slow -vf scale=' . $attributes->getWidth() . ':' . $attributes->getHeight() . ' -b:v ' . $this->maxRate . ' -maxrate ' . $this->maxRate . ' -bufsize ' . $this->bufferSize . ' -t ' . $duration . ' -threads 0 -pass 2 -acodec aac -ar 44100 -f mp4 -strict -2 ' . $this->createTmpFilePath($name) . ' 2>&1';
        exec($command, $output, $ret);
        if ($ret) {
            $this->logger->error('FfmpegConverter: convertSecondTime, command=' . $command . ', output=' . print_r($output, true));
        }
    }

    private function replaceResult(string $name, string $saveToPath): void
    {
        $command = 'qt-faststart ' . $this->createTmpFilePath($name) . ' ' . $saveToPath . '/' . $name . '.mp4 > /dev/null';
        exec($command, $output, $ret);
        if ($ret) {
            $this->logger->error('FfmpegConverter: replaceResult, command=' . $command . ', output=' . print_r($output, true));
        }
    }

    private function createScreenshot(string $name, string $saveToPath, VideoAttributes $attributes): void
    {
        $command = 'ffmpeg -y -ss 1 -i ' . $saveToPath . '/' . $name . '.mp4 -vframes 1 -q:v 2 -vf scale=' . $attributes->getWidth() . ':' . $attributes->getHeight() . ' ' . $saveToPath . '/' . $name . '.jpg 2>&1';
        exec($command, $output, $ret);
        if ($ret) {
            $this->logger->error('FfmpegConverter: createScreenshot, command=' . $command . ', output=' . print_r($output, true));
        }
    }

    private function getVideoAttributes(string $name, string $srcPathToUploadedFile): VideoAttributes
    {
        $command = 'ffprobe -i ' . $srcPathToUploadedFile . ' -show_streams';
        exec($command, $output, $ret);

        if ($ret) {
            $this->logger->error('FfmpegConverter: ffprobe failed: command=' . $command);
        }

        $width = 0;
        $height = 0;
        $duration = 0;
        $rotation = 0;
        foreach ($output as $line) {
            if (substr($line, 0, 6) == "width=") {
                $width = intval(substr($line, 6));
            }

            if (substr($line, 0, 7) == "height=") {
                $height = intval(substr($line, 7));
            }

            if (substr($line, 0, 9) == "duration=") {
                $duration = intval(substr($line, 9));
            }

            if (substr($line, 0, 11) == "TAG:rotate=") {
                $rotation = intval(substr($line, 11));
            }
        }

        $video = new VideoAttributes($width, $height, $duration, $rotation);

        if (!$width || !$height) {
            $this->logger->error(
                'FfmpegConverter: invalid width or height: width=' . $width . ', height=' . $height . ', file=' . $srcPathToUploadedFile
            );
        }

        if ($this->needRotate($rotation)) {
            $video = new VideoAttributes($height, $width, $duration, $rotation);
        }

        if ($this->needDetermineTarget($width, $height)) {
            [$width, $height] = $this->determineTarget($width, $height);
            $video = new VideoAttributes($width, $height, $duration, $rotation);
        }

        return $video;
    }

    /**
     * @return array|int[]
     */
    private function determineTarget(int $width, int $height): array
    {
        $widthRatio = $width / $this->maxWidth;
        $heightRatio = $height / $this->maxHeight;

        if ($widthRatio > $heightRatio) {

            $newWidth = $this->maxWidth;
            $newHeight = round($height / $widthRatio);

            if ($newHeight % 2 === 1) {
                $newHeight -= 1;
            }

            return [
                (int) $newWidth,
                (int) $newHeight
            ];
        }

        if ($heightRatio > $widthRatio) {

            $newWidth = round($width / $heightRatio);
            if ($newWidth % 2 === 1) {
                $newWidth -= 1;
            }

            $newHeight = $this->maxHeight;

            return [
                (int) $newWidth,
                (int) $newHeight
            ];

        }

        return [
            $this->maxWidth,
            $this->maxHeight
        ];
    }

    private function getDuration(int $duration): int
    {
        if ($duration > $this->maxDuration) {
            return $this->maxDuration;
        }

        return $duration;
    }

    private function needDetermineTarget(int $width, int $height): bool
    {
        return ($width > $this->maxWidth) || ($height > $this->maxHeight);
    }

    private function needRotate(int $rotation): bool
    {
        return $rotation == 90 || $rotation == 270;
    }
}