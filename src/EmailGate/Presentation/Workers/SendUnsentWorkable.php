<?php

declare(strict_types=1);

namespace App\EmailGate\Presentation\Workers;

use LazyLemurs\Workers\Workable;
use App\EmailGate\Application\EmailCommandProcessor;
use App\EmailGate\Domain\MessageNotFound;
use Psr\Log\LoggerInterface;

final class SendUnsentWorkable implements Workable
{
    /** @var EmailCommandProcessor */
    private $application;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(EmailCommandProcessor $application, LoggerInterface $logger)
    {
        $this->application = $application;
        $this->logger = $logger;
    }

    /**
     * @throws \Throwable
     */
    public function process(): bool
    {
        try {
            $this->application->sendUnsent();
            return true;
        } catch (MessageNotFound $e) {
            $this->logger->debug($e->getMessage());
            return false;
        }
    }
}