<?php

declare(strict_types=1);

namespace App\EmailGate\Presentation\Commands;

use LazyLemurs\Console\Command;
use LazyLemurs\Structures\Email;
use App\EmailGate\Application\EmailCommandProcessor;
use App\EmailGate\Domain\EmailData;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SendEmailCommand extends Command
{
    private EmailCommandProcessor $application;

    public function __construct(EmailCommandProcessor $application)
    {
        parent::__construct('email-gate:send');
        $this->application = $application;
    }

    protected function configure()
    {
        parent::configure();
        $this
            ->setDescription('Sends email')
            ->setHelp('This command sends email')
            ->addArgument('messageId', InputArgument::REQUIRED, 'Message ID (UUID)')
            ->addArgument('from', InputArgument::REQUIRED, 'Sender email')
            ->addArgument('to', InputArgument::REQUIRED, 'To email')
            ->addArgument('text', InputArgument::REQUIRED, 'Message')
            ->addArgument('subject', InputArgument::REQUIRED, 'Subject');
    }

    /**
     * @throws \Throwable
     */
    protected function doExecute(InputInterface $input, OutputInterface $output)
    {
        $this->application->sendSync(
            new EmailData(
                Uuid::fromString($input->getArgument('messageId')),
                new Email($input->getArgument('from')),
                new Email($input->getArgument('to')),
                $input->getArgument('text'),
                $input->getArgument('subject')
            )
        );
    }
}