<?php

declare(strict_types=1);

namespace App\EmailGate\Domain;

use Ramsey\Uuid\UuidInterface;

interface MessageStorage
{
    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Message $message): void;

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(Message $message): void;

    public function getByIdAndLock(UuidInterface $messageId): ?Message;

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getUnsentAndLock(): ?Message;
}