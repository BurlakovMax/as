<?php

declare(strict_types=1);

namespace App\EmailGate\Domain;

use LazyLemurs\Structures\Email;

final class MessageDetails implements \JsonSerializable
{
    private Email $from;

    private Email $to;

    private ?Email $replyTo;

    private string $body;

    private string $subject;

    public function __construct(
        Email $from,
        Email $to,
        string $subject,
        string $body,
        ?Email $replyTo = null
    ) {
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->body = $body;
        $this->replyTo = $replyTo;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            new Email($data['from']),
            new Email($data['to']),
            (string) $data['subject'],
            (string) $data['body'],
            isset($data['replyTo']) ? new Email($data['replyTo']) : null,
        );
    }

    /**
     * @return mixed[]
     */
    public function jsonSerialize(): array
    {
        $data = [
            'from' => $this->from->getValue(),
            'to' => $this->to->getValue(),
            'body' => $this->body,
            'subject' => $this->subject,
        ];

        if ($this->replyTo !== null) {
            $data['replyTo'] = $this->replyTo->getValue();
        }

        return $data;
    }

    public function getFrom(): Email
    {
        return $this->from;
    }

    public function getTo(): Email
    {
        return $this->to;
    }

    public function getReplyTo(): ?Email
    {
        return $this->replyTo;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }
}
