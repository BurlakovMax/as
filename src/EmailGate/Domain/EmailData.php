<?php

declare(strict_types=1);

namespace App\EmailGate\Domain;

use LazyLemurs\Structures\Email;
use Ramsey\Uuid\UuidInterface;

final class EmailData
{
    /**
     * @SWG\Property(type="string")
     */
    private UuidInterface $messageId;

    private Email $emailSender;

    private ?Email $replyTo;

    private Email $emailTo;

    private string $body;

    private string $subject;

    public function __construct(
        UuidInterface $messageId,
        Email $emailSender,
        Email $emailTo,
        string $body,
        string $subject,
        ?Email $replyTo = null
    ) {
        $this->messageId = $messageId;
        $this->emailSender = $emailSender;
        $this->emailTo = $emailTo;
        $this->body = $body;
        $this->subject = $subject;
        $this->replyTo = $replyTo;
    }

    public function getMessageId(): UuidInterface
    {
        return $this->messageId;
    }

    public function getEmailSender(): Email
    {
        return $this->emailSender;
    }

    public function getReplyTo(): ?Email
    {
        return $this->replyTo;
    }

    public function getEmailTo(): Email
    {
        return $this->emailTo;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }
}
