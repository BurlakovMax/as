<?php

declare(strict_types=1);

namespace App\EmailGate\Domain;

use App\Core\Domain\AttemptCounter;
use App\Core\Domain\MaxAttemptReached;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\EmailGate\Infrastructure\Persistence\MessageDoctrineRepository")
 * @ORM\Table(name="email_gate_message")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $messageId;

    /**
     * @ORM\Column(type="message_details", nullable=false)
     */
    private MessageDetails $details;

    /**
     * @ORM\Embedded(class="App\Core\Domain\AttemptCounter", columnPrefix=false)
     */
    private AttemptCounter $attemptCounter;

    /**
     * @ORM\Column(type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $sentAt;

    public function __construct(
        UuidInterface $messageId,
        MessageDetails $details
    ) {
        $this->messageId = $messageId;
        $this->details = $details;
        $this->attemptCounter = new AttemptCounter();
        $this->sentAt = new \DateTimeImmutable();
    }

    public function getMessageId(): UuidInterface
    {
        return $this->messageId;
    }

    public function getDetails(): MessageDetails
    {
        return $this->details;
    }

    public function getSentAt(): ?\DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function isSent(): bool
    {
        return $this->getSentAt() instanceof \DateTimeImmutable;
    }

    /**
     * @throws MaxAttemptReached
     */
    public function nextAttempt(): void
    {
        $this->attemptCounter->nextAttempt();
    }
}
