<?php

declare(strict_types=1);

namespace App\EmailGate\Domain;

use App\Core\Domain\MaxAttemptReached;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\UuidInterface;

final class MessageSender
{
    private MessageStorage $storage;

    private EmailProvider $emailProvider;

    public function __construct(
        MessageStorage $storage,
        EmailProvider $emailProvider
    ) {
        $this->storage = $storage;
        $this->emailProvider = $emailProvider;
    }

    /**
     * @throws MessageNotFound
     * @throws MessageAlreadyExists
     * @throws \Doctrine\ORM\ORMException
     */
    public function sendSyncByRawValues(
        UuidInterface $messageId,
        Email $emailSender,
        Email $emailTo,
        string $body,
        string $subject,
        ?Email $replyTo = null
    ): void {
        $emailData = new EmailData(
            $messageId,
            $emailSender,
            $emailTo,
            $body,
            $subject,
            $replyTo
        );
        $this->sendAsync($emailData);
        $this->sendById($emailData->getMessageId());
    }

    /**
     * @throws MessageNotFound
     * @throws MessageAlreadyExists
     * @throws \Doctrine\ORM\ORMException
     */
    public function sendSync(EmailData $emailData): void
    {
        $this->sendAsync($emailData);
        $this->sendById($emailData->getMessageId());
    }

    /**
     * @throws MessageAlreadyExists
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function sendAsync(EmailData $emailData): void
    {
        if ($this->storage->getByIdAndLock($emailData->getMessageId())) {
            throw new MessageAlreadyExists();
        }
        $message = new Message(
            $emailData->getMessageId(),
            new MessageDetails(
                $emailData->getEmailSender(),
                $emailData->getEmailTo(),
                $emailData->getSubject(),
                $emailData->getBody(),
                $emailData->getReplyTo(),
            )
        );

        $this->storage->add($message);
    }

    /**
     * @throws MessageNotFound
     * @throws \Exception
     */
    public function sendById(UuidInterface $messageId): void
    {
        $message = $this->getMessage($messageId);
        $this->sendViaProvider($message, $this->emailProvider);
    }

    /**
     * @throws MessageNotFound
     * @throws \Exception
     */
    public function sendUnsent(): void
    {
        $message = $this->storage->getUnsentAndLock();
        if (!$message) {
            throw new MessageNotFound();
        }

        $this->sendViaProvider($message, $this->emailProvider);
    }

    /**
     * @throws MaxAttemptReached
     */
    private function sendViaProvider(Message $message, EmailProvider $provider): void
    {
        try {
            $provider->send(
                new EmailData(
                    $message->getMessageId(),
                    $message->getDetails()->getFrom(),
                    $message->getDetails()->getTo(),
                    $message->getDetails()->getBody(),
                    $message->getDetails()->getSubject(),
                    $message->getDetails()->getReplyTo(),
                )
            );
        } catch (EmailSendError $e) {
            $message->nextAttempt();
        }
    }

    /**
     * @throws MessageNotFound
     */
    private function getMessage(UuidInterface $messageId): Message
    {
        $message = $this->storage->getByIdAndLock($messageId);
        if (!$message) {
            throw new MessageNotFound();
        }

        return $message;
    }
}
