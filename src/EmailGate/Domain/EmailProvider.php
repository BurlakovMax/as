<?php

declare(strict_types=1);

namespace App\EmailGate\Domain;

interface EmailProvider
{
    /**
     * @throws EmailSendError
     */
    public function send(EmailData $data): void;
}
