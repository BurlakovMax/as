<?php

declare(strict_types=1);

namespace App\EmailGate\Domain;

use LazyLemurs\Exceptions\NotFoundException;

final class MessageNotFound extends NotFoundException
{

}