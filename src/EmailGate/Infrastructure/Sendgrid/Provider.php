<?php

declare(strict_types=1);

namespace App\EmailGate\Infrastructure\Sendgrid;

use App\EmailGate\Domain\EmailData;
use App\EmailGate\Domain\EmailProvider;
use App\EmailGate\Domain\EmailSendError;
use Psr\Log\LoggerInterface;
use SendGrid\Mail\Mail;

final class Provider implements EmailProvider
{
    private string $key;

    private LoggerInterface $logger;

    public function __construct(string $key, LoggerInterface $logger)
    {
        $this->key = $key;
        $this->logger = $logger;
    }

    /**
     * @throws EmailSendError
     * @throws \SendGrid\Mail\TypeException
     */
    public function send(EmailData $data): void
    {
        $this->logger->debug("Start sending Email. Id: {$data->getMessageId()->toString()}");

        $email = $this->buildMail($data);
        $sendGrid = new \SendGrid($this->key);

        try {
            $sendGrid->send($email);
        } catch (\Exception $e) {
            $error = sprintf(
                'Error on attempt sending Email. Id: %s. Message: %s',
                $data->getMessageId()->toString(),
                $e->getMessage()
            );
            $this->logger->error($error);
            throw new EmailSendError($error);
        }
    }

    /**
     * @throws \SendGrid\Mail\TypeException
     */
    private function buildMail(EmailData $data): Mail
    {
        $email = new Mail();
        $email->setFrom($data->getEmailSender()->getValue());
        $email->addTo($data->getEmailTo()->getValue());
        $email->setSubject($data->getSubject());
        $email->addContent('text/plain', $data->getBody());

        if ($data->getReplyTo() !== null) {
            $email->setReplyTo($data->getReplyTo()->getValue());
        }

        return $email;
    }
}
