<?php

declare(strict_types=1);

namespace App\EmailGate\Infrastructure\Persistence\Mapping;

use App\EmailGate\Domain\MessageDetails;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class MessageDetailsType extends Type
{
    public function getName(): string
    {
        return 'message_details_type';
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getClobTypeDeclarationSQL(['length' => 65535]);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?MessageDetails
    {
        if (null === $value) {
            return null;
        }

        return MessageDetails::fromArray(
            json_decode($value, true)
        );
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof MessageDetails) {
            throw new \InvalidArgumentException(sprintf('Value must be %s type', MessageDetails::class));
        }

        return json_encode($value);
    }
}
