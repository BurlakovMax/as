<?php

declare(strict_types=1);

namespace App\EmailGate\Infrastructure\Persistence;

use App\EmailGate\Domain\Message;
use App\EmailGate\Domain\MessageStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\UuidInterface;

final class MessageDoctrineRepository extends EntityRepository implements MessageStorage
{
    /**
     * @inheritdoc
     */
    public function add(Message $message): void
    {
        $this->getEntityManager()->persist($message);
    }

    /**
     * @inheritdoc
     */
    public function delete(Message $message): void
    {
        $this->getEntityManager()->remove($message);
    }

    public function getByIdAndLock(UuidInterface $messageId): ?Message
    {
        return $this->find($messageId, LockMode::PESSIMISTIC_WRITE);
    }

    /**
     * @inheritdoc
     */
    public function getUnsentAndLock(): ?Message
    {
        return $this->createQueryBuilder('t')
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->isNull('sentAt'))
                    ->andWhere(Criteria::expr()->lte('attemptCounter.nextAttemptAt' , new \DateTimeImmutable()))
                    ->orderBy(['attemptCounter.nextAttemptAt' => Criteria::DESC])
                    ->setMaxResults(1)
            )
            ->getQuery()
            ->setLockMode(LockMode::PESSIMISTIC_WRITE)
            ->getOneOrNullResult();
    }
}