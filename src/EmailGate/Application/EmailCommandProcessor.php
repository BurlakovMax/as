<?php

declare(strict_types=1);

namespace App\EmailGate\Application;

use LazyLemurs\Structures\Email;
use LazyLemurs\TransactionManager\TransactionManager;
use App\EmailGate\Domain\EmailData;
use App\EmailGate\Domain\MessageNotFound;
use App\EmailGate\Domain\MessageSender;
use Ramsey\Uuid\UuidInterface;

final class EmailCommandProcessor
{
    /** @var TransactionManager */
    private $transactionManager;

    /** @var MessageSender */
    private $messageSender;

    public function __construct(TransactionManager $transactionManager, MessageSender $messageSender)
    {
        $this->transactionManager = $transactionManager;
        $this->messageSender = $messageSender;
    }

    /**
     * @throws \Throwable
     */
    public function sendSync(EmailData $emailData): void
    {
        $this->transactionManager->transactional(function () use ($emailData) {
            $this->messageSender->sendSync($emailData);
        });
    }

    /**
     * @throws \Throwable
     */
    public function sendAsync(EmailData $emailData): void
    {
        $this->transactionManager->transactional(function () use ($emailData) {
            $this->messageSender->sendAsync($emailData);
        });
    }

    /**
     * @throws \Throwable
     */
    public function sendSyncByRawValues(
        UuidInterface $messageId,
        Email $emailSender,
        Email $emailTo,
        string $body,
        string $subject,
        ?Email $replyTo = null
    ): void {
        $this->transactionManager->transactional(
            function () use ($messageId, $emailSender, $emailTo, $body, $subject, $replyTo): void {
                $this->messageSender->sendSyncByRawValues(
                    $messageId, $emailSender, $emailTo, $body, $subject, $replyTo
                );
            });
    }

    /**
     * @throws MessageNotFound
     * @throws \Throwable
     */
    public function sendUnsent(): void
    {
        $this->transactionManager->transactional(function () {
            $this->messageSender->sendUnsent();
        });
    }
}
