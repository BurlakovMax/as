<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class Verification
{
    /**
     * @ORM\Column(name="verification_id", type="integer", nullable=true)
     */
    private ?int $verificationId;

    /**
     * @ORM\Column(name="verified_stamp", type="integer", nullable=true)
     */
    private ?int $verifiedStamp;

    /**
     * @ORM\Column(name="verified_by", type="integer", nullable=true)
     */
    private ?int $verifiedBy;

    public function getVerificationId(): ?int
    {
        return $this->verificationId;
    }

    public function getVerifiedStamp(): ?int
    {
        return $this->verifiedStamp;
    }

    public function getVerifiedBy(): ?int
    {
        return $this->verifiedBy;
    }
}
