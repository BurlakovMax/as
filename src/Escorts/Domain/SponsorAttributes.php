<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class SponsorAttributes
{
    /**
     * @ORM\Column(type="integer", options={"default"=0})
     */
    private bool $sponsor;

    /**
     * @ORM\Column(name="sponsor_mobile", type="integer", options={"default"=0})
     */
    private bool $sponsorMobile;

    /**
     * @ORM\Column(name="sponsor_position", type="integer", nullable=true, options={"default"=99})
     */
    private ?int $sponsorPosition;

    /**
     * @ORM\Column(name="sponsor_loc_id", type="integer", nullable=true)
     */
    private ?int $sponsorLocId;

    public function __construct(
        bool $sponsor,
        bool $sponsorMobile,
        ?int $sponsorPosition,
        ?int $sponsorLocId
    ) {
        $this->sponsor = $sponsor;
        $this->sponsorMobile = $sponsorMobile;
        $this->sponsorPosition = $sponsorPosition;
        $this->sponsorLocId = $sponsorLocId;
    }

    public function getSponsor(): bool
    {
        return $this->sponsor;
    }

    public function getSponsorMobile(): bool
    {
        return $this->sponsorMobile;
    }

    public function getSponsorPosition(): ?int
    {
        return $this->sponsorPosition;
    }

    public function getSponsorLocId(): ?int
    {
        return $this->sponsorLocId;
    }
}
