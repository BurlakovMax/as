<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\EscortSponsorDoctrineRepository")
 * @ORM\Table(name="classifieds_sponsor")
 */
class EscortSponsor
{
    /**
     * @ORM\Column(name="id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", name="loc_id", length=8)
     */
    private int $locationId;

    /**
     * @ORM\Column(type="integer", length=8)
     */
    private int $stateId;

    /**
     * @ORM\Column(type="integer", name="post_id", length=11)
     */
    private int $escortId;

    /**
     * @ORM\Column(type="integer", name="day", length=5)
     */
    private int $expireInDays;

    /**
     * @ORM\Column(type="escort_type")
     */
    private EscortType $type;

    /**
     * @ORM\Column(name="expire_stamp", type="timestamp", length=11)
     */
    private \DateTimeImmutable $expireAt;

    /**
     * @ORM\Column(name="done", type="status_type")
     */
    private Status $status;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $notified;

    /**
     * @throws \Exception
     */
    public function __construct(
        int $locationId,
        int $stateId,
        int $escortId,
        int $expireInDays,
        EscortType $type
    ) {
        $this->id = 0;
        $this->locationId = $locationId;
        $this->stateId = $stateId;
        $this->escortId = $escortId;
        $this->expireInDays = $expireInDays;
        $this->type = $type;
        $this->expireAt = new \DateTimeImmutable('+' . $expireInDays . 'days');
        $this->status = Status::active();
        $this->notified = false;
    }

    public function setProcessOfCreationStatus(): void
    {
        $this->status = Status::processOfCreation();
    }

    /**
     * @throws \Exception
     */
    public function renew(int $expireInDays): void
    {
        if ($this->getExpireAt() > new \DateTimeImmutable()) {
            $this->expireAt = $this->getExpireAt()->modify('+' . $expireInDays . 'days');
        } else {
            $this->expireAt = new \DateTimeImmutable('+' . $expireInDays . 'days');
        }

        $this->status = Status::active();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getExpireInDays(): int
    {
        return $this->expireInDays;
    }

    public function getType(): EscortType
    {
        return $this->type;
    }

    public function getExpireAt(): \DateTimeImmutable
    {
        return $this->expireAt;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }
}
