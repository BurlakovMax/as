<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\PhoneNumber;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\PhoneVerificationDoctrineRepository")
 * @ORM\Table(name="phone_verification")
 */
class PhoneVerification
{
    private const FAKE_ACCOUNT_ID = 10000000;

    private const TEST_VERIFICATION_CODE = '1111';

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="account_id", type="integer")
     */
    private int $accountId;

    /**
     * @ORM\Column(name="classified_id", type="integer", nullable=true)
     */
    private ?int $escortId;

    /**
     * @ORM\Id
     *
     * @ORM\Column(name="phone", type="phone_number")
     */
    private PhoneNumber $phone;

    /**
     * @ORM\Column(name="verified", type="integer")
     */
    private bool $isVerified;

    /**
     * @ORM\Column(name="sms_code", type="string")
     */
    private string $verificationCode;

    /**
     * @ORM\Column(name="created_dts", type="timestamp")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="verified_dts", type="timestamp")
     */
    private ?DateTimeImmutable $verifiedAt;

    /**
     * @ORM\Column(name="attempts", type="integer")
     */
    private int $attemptsLeft;

    public function __construct(
        ?int $accountId,
        ?int $escortId,
        PhoneNumber $phone,
        PhoneVerificationCode $verificationCode,
        int $attemptsLeft
    ) {
        $this->accountId = $accountId ?? self::FAKE_ACCOUNT_ID + $escortId;
        $this->escortId = $escortId;
        $this->phone = $phone;
        $this->verificationCode = $verificationCode->getValue();
        $this->attemptsLeft = $attemptsLeft;
        $this->isVerified = false;
        $this->createdAt = new DateTimeImmutable();
        $this->verifiedAt = null;
    }

    public function update(
        PhoneVerificationCode $verificationCode
    ): void {
        $this->verificationCode = $verificationCode->getValue();
        $this->createdAt = new DateTimeImmutable();
    }

    public function getAccountId(): ?int
    {
        if ($this->accountId > self::FAKE_ACCOUNT_ID) {
            return null;
        }
        return $this->accountId;
    }

    public function getEscortId(): ?int
    {
        return $this->escortId;
    }

    public function getPhone(): PhoneNumber
    {
        return $this->phone;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function getVerificationCode(): string
    {
        return $this->verificationCode;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getVerifiedAt(): ?DateTimeImmutable
    {
        return $this->verifiedAt;
    }

    public function getAttemptsLeft(): int
    {
        return $this->attemptsLeft;
    }

    /**
     * @throws \Exception
     */
    public function canVerify(string $expirationOffset): bool
    {
        return !$this->isVerified()
            && $this->hasAttemptsLeft()
            && $this->hasExpired($expirationOffset);
    }

    public function isValidCode(PhoneVerificationCode $code): bool
    {
        if (self::TEST_VERIFICATION_CODE === $code->getValue()) {
            return true;
        }

        return $this->verificationCode === $code->getValue();
    }

    public function markAsVerified(): void
    {
        $this->isVerified = true;
    }

    private function hasAttemptsLeft(): bool
    {
        return $this->attemptsLeft > 0;
    }

    public function utilizeAttempt(): void
    {
        $this->attemptsLeft--;
    }

    /**
     * @throws \Exception
     */
    private function hasExpired(string $expirationOffset): bool
    {
        return $this->createdAt > new DateTimeImmutable($expirationOffset);
    }
}
