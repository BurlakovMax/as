<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\Application\Search\SearchQuery;

interface EscortSideReadStorage
{
    /**
     * @return EscortSide[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function getByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSide;

    public function getByEscortId(int $escortId): ?EscortSide;

    /**
     * @return int[]
     */
    public function getEscortIdsByTypeAndLocationId(EscortType $type, int $locationId): array;

    public function countByTypeAndLocationId(EscortType $type, int $locationId): int;
}