<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface EscortWaitingListReadStorage
{
    /**
     * @return EscortWaitingList[]
     */
    public function getBy(int $accountId, int $locationId, EscortType $type): array;
}