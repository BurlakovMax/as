<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

trait EscortPresentation
{
    /**
     * @ORM\Column(type="smallint", length=4, nullable=true)
     */
    private ?int $age;

    /**
     * @ORM\Column(type="ethnicity", nullable=true)
     */
    private ?Ethnicity $ethnicity;

    /**
     * @ORM\Column(name="language", type="string", length=100, nullable=true)
     */
    private ?string $languageIds;

    /**
     * @ORM\Column(name="height_feet", type="smallint", options={"default"=0})
     */
    private int $heightFeet;

    /**
     * @ORM\Column(name="height_inches", type="smallint", options={"default"=0})
     */
    private int $heightInches;

    /**
     * @ORM\Column(type="smallint", options={"default"=0})
     */
    private int $weight;

    /**
     * @ORM\Column(type="eye_color", name="eyecolor")
     */
    private EyeColor $eyeColor;

    /**
     * @ORM\Column(type="hair_color", name="haircolor")
     */
    private HairColor $hairColor;

    /**
     * @ORM\Column(name="build", type="body_type")
     */
    private BodyType $bodyType;

    /**
     * @ORM\Column(name="measure_1", type="smallint")
     */
    private int $bust;

    /**
     * @ORM\Column(name="measure_2", type="smallint")
     */
    private int $waist;

    /**
     * @ORM\Column(name="measure_3", type="smallint")
     */
    private int $hip;

    /**
     * @ORM\Column(name="cupsize", type="smallint")
     */
    private int $cupSize;

    /**
     * @ORM\Column(name="penis_size", type="smallint")
     */
    private int $penisSize;

    /**
     * @ORM\Column(name="kitty", type="kitty")
     */
    private Kitty $kitty;

    /**
     * @ORM\Column(name="pornstar", type="boolean")
     */
    private bool $isPornstar;

    /**
     * @ORM\Column(name="pregnant", type="boolean")
     */
    private bool $isPregnant;

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function getEthnicity(): ?Ethnicity
    {
        return $this->ethnicity;
    }

    /**
     * @return Language[]
     */
    public function getLanguageIds(): array
    {
        if ($this->languageIds === null || $this->languageIds === '') {
            return [];
        }

        return array_map(
            static function (string $language): Language {
                return Language::getValueOf($language);
            },
            explode(',', $this->languageIds)
        );
    }

    public function getHeightFeet(): int
    {
        return $this->heightFeet;
    }

    public function getHeightInches(): int
    {
        return $this->heightInches;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getEyeColor(): ?EyeColor
    {
        if ($this->eyeColor->equals(EyeColor::notSet())) {
            return null;
        }

        return $this->eyeColor;
    }

    public function getHairColor(): ?HairColor
    {
        if ($this->hairColor->equals(HairColor::notSet())) {
            return null;
        }

        return $this->hairColor;
    }

    public function getBodyType(): ?BodyType
    {
        if ($this->bodyType->equals(BodyType::none())) {
            return null;
        }

        return $this->bodyType;
    }

    public function getBust(): int
    {
        return $this->bust;
    }

    public function getWaist(): int
    {
        return $this->waist;
    }

    public function getHip(): int
    {
        return $this->hip;
    }

    public function getCupSize(): int
    {
        return $this->cupSize;
    }

    public function getPenisSize(): ?int
    {
        if ($this->getType() !== EscortType::shemale()) {
            return null;
        }

        return $this->penisSize;
    }

    public function getKitty(): ?Kitty
    {
        if ($this->kitty->equals(Kitty::none())) {
            return null;
        }

        if (!in_array($this->getType(), [EscortType::female(), EscortType::bodyRubs()], true)) {
            return null;
        }

        return $this->kitty;
    }

    public function getIsPornstar(): bool
    {
        return $this->isPornstar;
    }

    public function getIsPregnant(): bool
    {
        return $this->isPregnant;
    }
}
