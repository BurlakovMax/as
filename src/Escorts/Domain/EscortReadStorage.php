<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\SearchQuery;

interface EscortReadStorage
{
    public function get(int $id): ?Escort;

    public function getActiveByIdAndAccountId(int $id, int $accountId): ?Escort;

    public function getLastActiveByAccountId(int $accountId): ?Escort;

    /**
     * @return int[]
     */
    public function getIdsBySearchQuery(SearchQuery $query): array;

    public function countIdsBySearchQuery(SearchQuery $query): int;

    /**
     * @return Escort[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    /**
     * @param int[] $ids
     * @return Escort[]
     */
    public function getByIds(array $ids): array;

    /**
     * @param FilterQuery[] $filters
     */
    public function countByFilterQuery(array $filters): int;

}