<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class VideoConvertedStatus extends Enumerable
{
    public static function notConverted(): self
    {
        return self::createEnum(0);
    }

    public static function error(): self
    {
        return self::createEnum(1);
    }

    public static function successfully(): self
    {
        return self::createEnum(2);
    }
}