<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\Structures\PhoneNumber;

interface PhoneVerificationStorage
{
    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function get(PhoneNumber $phone, ?int $accountId, ?int $escortId): ?PhoneVerification;

    /**
     * @return PhoneVerification[]
     */
    public function getByPhoneNumberAndVerified(PhoneNumber $phoneNumber): array;

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAndLock(PhoneNumber $phone, ?int $accountId, ?int $escortId): ?PhoneVerification;

    public function add(PhoneVerification $phoneVerification): void;
}
