<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class Ethnicity extends Enumerable
{
    public static function notSet(): self
    {
        return self::createEnum(0);
    }

    public static function asian(): self
    {
        return self::createEnum(1);
    }

    public static function black(): self
    {
        return self::createEnum(2);
    }

    public static function hispanic(): self
    {
        return self::createEnum(3);
    }

    public static function white(): self
    {
        return self::createEnum(4);
    }

    public static function eastIndian(): self
    {
        return self::createEnum(5);
    }

    public static function mixed(): self
    {
        return self::createEnum(7);
    }

    public static function middleEastern(): self
    {
        return self::createEnum(22);
    }
}
