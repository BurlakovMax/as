<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\Application\Search\SearchQuery;

interface EscortStickyReadStorage
{
    /**
     * @return EscortSticky[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    /**
     * @return EscortSticky[]
     */
    public function getListBy(int $type, int $locationId): array;

    /**
     * @return EscortSticky[]
     */
    public function getByEscortId(int $escortId): array;

    public function countByTypeAndLocationId(EscortType $type, int $locationId): int;

    public function getByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSticky;
}