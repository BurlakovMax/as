<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Escorts\Application\UpdateEscortAdminCommand;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\EscortDoctrineRepository")
 * @ORM\Table(name="classifieds")
 */
class Escort
{
    use EscortAvailability;
    use EscortContactInfo;
    use EscortPresentation;

    /**
     * @ORM\Column(name="id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=8, nullable=true)
     */
    private ?int $stateId = null;

    /**
     * @ORM\Column(type="escort_type")
     */
    private EscortType $type;

    /**
     * @ORM\Column(type="integer", length=11, nullable=false)
     */
    private ?int $accountId;

    /**
     * @ORM\Column(name="done", type="status_type", nullable=true)
     */
    private ?Status $status;

    /**
     * @ORM\Column(type="timestamp", length=11, options={"unsigned"=true}, nullable=true)
     */
    private ?\DateTimeImmutable $expireStamp = null;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private string $title;

    /**
     * @ORM\Column(name="date", type="datetime_immutable")
     */
    private \DateTimeImmutable $postedAt;

    /**
     * @ORM\Column(name="time_next", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $renewAt = null;

    /**
     * @ORM\Column(type="text", length=65535)
     */
    private string $content;

    /**
     * @ORM\Column(type="string", length=50, name="thumb", nullable=true)
     */
    private ?string $thumbnail = null;

    /**
     * @ORM\Column(type="string", length=244, name="multiple_thumbs", nullable=true)
     */
    private ?string $multipleThumbnails = null;

    /**
     * @ORM\Column(type="integer", length=3, nullable=true)
     */
    private ?int $available = null;

    /**
     * @ORM\Column(name="payment_visa", type="boolean")
     */
    private bool $isVisaAccepted;

    /**
     * @ORM\Column(name="payment_amex", type="boolean")
     */
    private bool $isAmexAccepted;

    /**
     * @ORM\Column(name="payment_dis", type="boolean")
     */
    private bool $isDiscoverAccepted;

    /**
     * @var FullName
     *
     * @ORM\Embedded(class="App\Escorts\Domain\FullName", columnPrefix=false)
     */
    private $fullName;

    /**
     * @ORM\Column(name="auto_renew", type="integer", options={"default"=0})
     */
    private int $repostLeft;

    /**
     * @ORM\Column(name="auto_repost", type="boolean", nullable=true, options={"default"=0})
     */
    private ?bool $autoRenew;

    /**
     * Renew frequency in days
     *
     * @ORM\Column(name="auto_renew_fr", type="integer", options={"default"=3})
     */
    private ?int $autoRenewFrequency;

    /**
     * Time of renew ad, number of minutes after midnight
     *
     * @ORM\Column(name="auto_renew_time", type="smallint", options={"default"=0})
     */
    private ?int $autoRenewTime;

    /**
     * @var SponsorAttributes
     *
     * @ORM\Embedded(class="SponsorAttributes", columnPrefix=false)
     */
    private $sponsorAttributes;

    /**
     * @ORM\Column(name="allowlinks", type="boolean", options={"default"=0})
     */
    private bool $allowLinks;

    /**
     * @ORM\Column(name="verified", type="boolean")
     */
    private bool $verified;

    /**
     * @ORM\Column(name="verify_call_sent", type="integer")
     */
    private ?int $verifyCallSent = null;

    /**
     * @ORM\Column(name="created", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $createdAt = null;

    /**
     * @ORM\Column(name="updated", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $updatedAt = null;

    /**
     * @ORM\Column(name="deleted", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $deletedAt = null;

    /**
     * @ORM\Column(name="direct_link", type="string", length=255, nullable=true)
     */
    private ?string $directLink = null;

    /**
     * @ORM\Column(name="ter", type="string", length=20, nullable=true)
     */
    private ?string $terId = null;

    /**
     * @ORM\Column(name="ter_check_stamp", type="integer", nullable=true)
     */
    private ?int $terCheckStamp = null;

    /**
     * @ORM\Column(name="ter_check_priority", type="boolean", nullable=true)
     */
    private ?bool $terCheckPriority = null;

    /**
     * @ORM\Column(name="ter_check_unapproved", type="integer", nullable=true)
     */
    private ?int $terCheckUnapproved = null;

    /**
     * @var Verification
     *
     * @ORM\Embedded(class="App\Escorts\Domain\Verification", columnPrefix=false)
     */
    private $verification;

    /**
     * @var EscortLocation[] | Collection
     *
     * @ORM\OneToMany(
     *   targetEntity="App\Escorts\Domain\EscortLocation",
     *   mappedBy="escort",
     *   cascade={"all"},
     *   orphanRemoval=true
     * )
     */
    private Collection $locations;

    /**
     * @var EscortImage[] | Collection
     *
     * @ORM\OneToMany(
     *   targetEntity="App\Escorts\Domain\EscortImage",
     *   mappedBy="escort",
     *   cascade={"all"},
     *   orphanRemoval=true
     * )
     */
    private Collection $images;

    /**
     * @var EscortVideo[] | Collection
     *
     * @ORM\OneToMany(
     *   targetEntity="App\Escorts\Domain\EscortVideo",
     *   mappedBy="escort",
     *   cascade={"all"},
     *   orphanRemoval=true
     * )
     */
    private Collection $videos;

    /**
     * @param int[] $locationIds
     * @param int[] $languageIds
     * @param int[] $stateIds
     * @throws \Exception
     */
    public function __construct(
        ?int $accountId,
        EscortType $type,
        array $locationIds,
        array $stateIds,
        string $firstName,
        ?string $middleName,
        ?string $lastName,
        string $title,
        string $content,
        bool $isAvailableForMen,
        bool $isAvailableForWomen,
        bool $isAvailableForCouple,
        bool $isAvailableForGroup,
        bool $isAvailableForBlack,
        bool $isIncallEnabled,
        ?int $incallRateHalfHour,
        ?int $incallRateOneHour,
        ?int $incallRateTwoHours,
        ?int $incallRateOvernight,
        bool $isOutcallEnabled,
        ?int $outcallRateHalfHour,
        ?int $outcallRateOneHour,
        ?int $outcallRateTwoHours,
        ?int $outcallRateOvernight,
        bool $isVisiting,
        $visitingFrom,
        $visitingTo,
        ?string $placeDescription,
        bool $gfe,
        bool $gfeLimited,
        bool $tantra,
        bool $fetishDominant,
        bool $fetishSubmissive,
        bool $fetishSwitch,
        ?string $fetish,
        ?string $phone,
        ?string $email,
        EmailVisibility $emailVisibility,
        ?string $website,
        ?string $facebook,
        ?string $twitter,
        ?string $telegram,
        ?string $google,
        ?string $instagram,
        ?string $wechat,
        ?string $whatsapp,
        int $age,
        ?Ethnicity $ethnicity,
        array $languageIds,
        int $heightFeet,
        int $heightInches,
        int $weight,
        HairColor $hairColor,
        EyeColor $eyeColor,
        BodyType $bodyType,
        int $bust,
        int $waist,
        int $hip,
        int $cupSize,
        ?int $penisSize,
        Kitty $kitty,
        bool $isPornstar,
        bool $isPregnant,
        ?string $terId,
        bool $isVisaAccepted,
        bool $isAmexAccepted,
        bool $isDiscoverAccepted,
        array $images,
        array $videos,
        int $repostLeft,
        bool $autoRenew,
        ?int $autoRenewFrequency,
        ?int $autoRenewTime,
        bool $allowLinks
    ) {
        $this->id = 0;
        $this->accountId = $accountId ?? 0;
        $this->type = $type;
        $this->fullName = new FullName(
            $firstName,
            $middleName,
            $lastName,
        );
        $this->title = $title;
        $this->content = $content;
        $this->isAvailableForMen = $isAvailableForMen;
        $this->isAvailableForWomen = $isAvailableForWomen;
        $this->isAvailableForCouple = $isAvailableForCouple;
        $this->isAvailableForGroup = $isAvailableForGroup;
        $this->isAvailableForBlack = $isAvailableForBlack;
        $this->incall = new Incall(
            $isIncallEnabled,
            $incallRateHalfHour,
            $incallRateOneHour,
            $incallRateTwoHours,
            $incallRateOvernight,
        );
        $this->outcall = new Outcall(
            $isOutcallEnabled,
            $outcallRateHalfHour,
            $outcallRateOneHour,
            $outcallRateTwoHours,
            $outcallRateOvernight,
        );
        $this->visitingDetails = new Visiting(
            $isVisiting,
            $visitingFrom,
            $visitingTo,
        );
        $this->placeDescription = $placeDescription;
        $this->gfe = $gfe;
        $this->gfeLimited = $gfeLimited;
        $this->tantra = $tantra;
        $this->fetishDominant = $fetishDominant;
        $this->fetishSubmissive = $fetishSubmissive;
        $this->fetishSwitch = $fetishSwitch;
        $this->fetish = $fetish;
        $this->phone = $phone;
        $this->email = $email;
        $this->emailVisibility = $emailVisibility;
        $this->website = $website;
        $this->facebook = $facebook;
        $this->twitter = $twitter;
        $this->telegram = $telegram;
        $this->google = $google;
        $this->instagram = $instagram;
        $this->wechat = $wechat;
        $this->whatsapp = $whatsapp;
        $this->age = $age;
        $this->ethnicity = $ethnicity;
        $this->languageIds = implode(
            ',',
            array_map(
                static function($id): int {
                    return Language::getValueOf($id)->getRawValue();
                },
                $languageIds
            )
        );
        $this->heightFeet = $heightFeet;
        $this->heightInches = $heightInches;
        $this->weight = $weight;
        $this->hairColor = $hairColor;
        $this->eyeColor = $eyeColor;
        $this->bodyType = $bodyType;
        $this->bust = $bust;
        $this->waist = $waist;
        $this->hip = $hip;
        $this->cupSize = $cupSize;
        $this->penisSize = $penisSize ?? 0;
        $this->kitty = $kitty;
        $this->isPornstar = $isPornstar;
        $this->isPregnant = $isPregnant;
        $this->terId = $terId;
        $this->isVisaAccepted = $isVisaAccepted;
        $this->isAmexAccepted = $isAmexAccepted;
        $this->isDiscoverAccepted = $isDiscoverAccepted;
        $this->repostLeft = $repostLeft ?? 0;
        $this->autoRenew = $autoRenew ?? false;
        $this->autoRenewFrequency = $autoRenewFrequency ?? 3;
        $this->autoRenewTime = $autoRenewTime ?? 0;
        $this->allowLinks = $allowLinks;
        $this->postedAt = new \DateTimeImmutable();
        $this->createdAt = new \DateTimeImmutable();
        $this->verified = false;
        $this->sponsorAttributes = new SponsorAttributes(
            false,
            false,
            null,
            null,
        );
        $this->status = Status::processOfCreation();

        $this->locations = new ArrayCollection(
            array_map(
                function ($locationId) use ($stateIds) {
                    return new EscortLocation(
                        $this,
                        $stateIds[$locationId] ?? 0,
                        $locationId,
                        $this->type,
                        $this->postedAt,
                        $this->status,
                    );
                },
                $locationIds
            )
        );

        $this->images = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->attachImages($images);
        $this->attachVideos($videos);
    }

    /**
     * @param int[] $locationIds
     * @param int[] $languageIds
     * @param int[] $stateIds
     * @throws \Exception
     */
    public function update(
        array $locationIds,
        array $stateIds,
        string $firstName,
        ?string $middleName,
        ?string $lastName,
        string $title,
        string $content,
        bool $isAvailableForMen,
        bool $isAvailableForWomen,
        bool $isAvailableForCouple,
        bool $isAvailableForGroup,
        bool $isAvailableForBlack,
        bool $isIncallEnabled,
        ?int $incallRateHalfHour,
        ?int $incallRateOneHour,
        ?int $incallRateTwoHours,
        ?int $incallRateOvernight,
        bool $isOutcallEnabled,
        ?int $outcallRateHalfHour,
        ?int $outcallRateOneHour,
        ?int $outcallRateTwoHours,
        ?int $outcallRateOvernight,
        bool $isVisiting,
        $visitingFrom,
        $visitingTo,
        ?string $placeDescription,
        bool $gfe,
        bool $gfeLimited,
        bool $tantra,
        bool $fetishDominant,
        bool $fetishSubmissive,
        bool $fetishSwitch,
        ?string $fetish,
        ?string $phone,
        ?string $email,
        EmailVisibility $emailVisibility,
        ?string $website,
        ?string $facebook,
        ?string $twitter,
        ?string $telegram,
        ?string $google,
        ?string $instagram,
        ?string $wechat,
        ?string $whatsapp,
        int $age,
        ?Ethnicity $ethnicity,
        array $languageIds,
        int $heightFeet,
        int $heightInches,
        int $weight,
        HairColor $hairColor,
        EyeColor $eyeColor,
        BodyType $bodyType,
        int $bust,
        int $waist,
        int $hip,
        int $cupSize,
        ?int $penisSize,
        Kitty $kitty,
        bool $isPornstar,
        bool $isPregnant,
        ?string $terId,
        bool $isVisaAccepted,
        bool $isAmexAccepted,
        bool $isDiscoverAccepted,
        array $images,
        array $videos,
        int $repostLeft,
        bool $autoRenew,
        ?int $autoRenewFrequency,
        ?int $autoRenewTime,
        bool $allowLinks
    ): void {
        $this->fullName = new FullName(
            $firstName,
            $middleName,
            $lastName,
        );
        $this->title = $title;
        $this->content = $content;
        $this->isAvailableForMen = $isAvailableForMen;
        $this->isAvailableForWomen = $isAvailableForWomen;
        $this->isAvailableForCouple = $isAvailableForCouple;
        $this->isAvailableForGroup = $isAvailableForGroup;
        $this->isAvailableForBlack = $isAvailableForBlack;
        $this->incall = new Incall(
            $isIncallEnabled,
            $incallRateHalfHour,
            $incallRateOneHour,
            $incallRateTwoHours,
            $incallRateOvernight,
        );
        $this->outcall = new Outcall(
            $isOutcallEnabled,
            $outcallRateHalfHour,
            $outcallRateOneHour,
            $outcallRateTwoHours,
            $outcallRateOvernight,
        );
        $this->visitingDetails = new Visiting(
            $isVisiting,
            $visitingFrom,
            $visitingTo,
        );
        $this->placeDescription = $placeDescription;
        $this->gfe = $gfe;
        $this->gfeLimited = $gfeLimited;
        $this->tantra = $tantra;
        $this->fetishDominant = $fetishDominant;
        $this->fetishSubmissive = $fetishSubmissive;
        $this->fetishSwitch = $fetishSwitch;
        $this->fetish = $fetish;
        $this->phone = $phone;
        $this->email = $email;
        $this->emailVisibility = $emailVisibility;
        $this->website = $website;
        $this->facebook = $facebook;
        $this->twitter = $twitter;
        $this->telegram = $telegram;
        $this->google = $google;
        $this->instagram = $instagram;
        $this->wechat = $wechat;
        $this->whatsapp = $whatsapp;
        $this->age = $age;
        $this->ethnicity = $ethnicity;
        $this->languageIds = implode(
            ',',
            array_map(
                static function ($id): int {
                    return Language::getValueOf($id)->getRawValue();
                },
                $languageIds
            )
        );
        $this->heightFeet = $heightFeet;
        $this->heightInches = $heightInches;
        $this->weight = $weight;
        $this->hairColor = $hairColor;
        $this->eyeColor = $eyeColor;
        $this->bodyType = $bodyType;
        $this->bust = $bust;
        $this->waist = $waist;
        $this->hip = $hip;
        $this->cupSize = $cupSize;
        $this->penisSize = $penisSize ?? 0;
        $this->kitty = $kitty;
        $this->isPornstar = $isPornstar;
        $this->isPregnant = $isPregnant;
        $this->terId = $terId;
        $this->isVisaAccepted = $isVisaAccepted;
        $this->isAmexAccepted = $isAmexAccepted;
        $this->isDiscoverAccepted = $isDiscoverAccepted;
        $this->repostLeft = $repostLeft;
        $this->autoRenew = $autoRenew;
        $this->autoRenewFrequency = $autoRenewFrequency ?? 3;
        $this->autoRenewTime = $autoRenewTime ?? 0;
        $this->allowLinks = $allowLinks;
        $this->updatedAt = new \DateTimeImmutable();
        $this->verified = false;
        $this->sponsorAttributes = new SponsorAttributes(
            false,
            false,
            null,
            null,
        );
        $this->status = Status::processOfCreation();

        if (!empty($locationIds)) {
            $this->locations = new ArrayCollection(
                array_map(
                    function ($locationId) use ($stateIds) {
                        return new EscortLocation(
                            $this,
                            $stateIds[$locationId],
                            $locationId,
                            $this->type,
                            $this->postedAt,
                            $this->status,
                        );
                    },
                    $locationIds
                )
            );
        }

        $this->attachImages($images);
        $this->attachVideos($videos);
    }

    public function adminUpdate(UpdateEscortAdminCommand $command): void
    {
        $this->accountId = $command->getAccountId();
        $this->status = $command->getStatus();
        $this->type = $command->getType();
        $this->expireStamp = $command->getExpireStamp();
        $this->sponsorAttributes = new SponsorAttributes(
            $command->isSponsor(),
            $command->isSponsorMobile(),
            $command->getSponsorPosition(),
            null,
        );
    }

    /**
     * @param int[] $locationIds
     */
    public function hasLocationsByLocationIds(array $locationIds): bool
    {
        $ids = [];
        foreach ($this->getLocations() as $location) {
            $ids[] = $location->getLocationId();
        }

        foreach ($locationIds as $locationId) {
            if (!in_array($locationId, $ids)) {
                return false;
            }
        }

        return true;
    }

    public function activate(): void
    {
        $this->status = Status::active();
        $this->postedAt = new \DateTimeImmutable();
    }

    public function delete(): void
    {
        $this->status = Status::requestedToDelete();
        $this->deletedAt = new \DateTimeImmutable();
    }

    /**
     * @throws \Exception
     */
    public function updateAfterPaidService(int $expireInDays, \DateTimeImmutable $renewAt): void
    {
        if ($this->getExpireStamp() > new \DateTimeImmutable()) {
            $this->expireStamp = $this->getExpireStamp()->modify('+' . $expireInDays . 'days');
        } else {
            $this->expireStamp = new \DateTimeImmutable('+' . $expireInDays . 'days');
        }

        $this->renewAt = $renewAt;
    }

    public function removeImages(array $images): void
    {
        if (empty($images)) {
            throw new EmptyImages();
        }

        foreach ($this->getImages() as $escortImage) {
            if (in_array($escortImage->getFilename(), $images)) {
                $this->images->removeElement($escortImage);
            }
        }
    }

    public function attachThumbnail(string $thumbnailName): void
    {
        $this->thumbnail = $thumbnailName;
    }

    /**
     * @param string[] $images
     * @throws \Exception
     */
    private function attachImages(array $images): void
    {
        if (empty($images)) {
            throw new EmptyImages();
        }

        foreach ($images as $image) {
            $item = new EscortImage(
                $this,
                $image,
                new \DateTimeImmutable()
            );

            if (!$this->isImageAlreadyExists($image)) {
                $this->addImage($item);
            }
        }
    }

    private function isImageAlreadyExists(string $imageName): bool
    {
        foreach ($this->images as $escortImage) {
            if ($escortImage->getFilename() === $imageName) {
                return true;
            }
        }

        return false;
    }

    public function removeVideos(array $videos): void
    {
        if (empty($videos)) {
            throw new EmptyVideos();
        }

        foreach ($this->getVideos() as $escortVideo) {
            if (in_array($escortVideo->getFilename(), $videos)) {
                $this->videos->removeElement($escortVideo);
            }
        }
    }

    private function attachVideos(array $videos): void
    {
        foreach ($videos as $video) {
            [ $filename ] = explode('.', $video);

            $item = new EscortVideo(
                $this->accountId,
                $this,
                $filename . '.mp4',
                $filename . '.jpg',
                null,
                null
            );

            if (!$this->isVideoAlreadyExists($video)) {
                $this->addVideo($item);
            }
        }
    }

    private function isVideoAlreadyExists(string $videoName): bool
    {
        foreach ($this->videos as $video) {
            if ($video->getFilename() === $videoName) {
                return true;
            }
        }

        return false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStateId(): ?int
    {
        return $this->stateId;
    }

    public function isShemale(): bool
    {
        return $this->getType()->equals(EscortType::shemale());
    }

    public function getType(): EscortType
    {
        return $this->type;
    }

    /**
     * @return EscortLocation[]
     */
    public function getLocations(): array
    {
        return $this->locations->toArray();
    }

    public function activateLocationByLocationId(int $locationId): void
    {
        foreach ($this->getLocations() as $location) {
            if ($locationId === $location->getLocationId()) {
                $location->activate();
            }
        }
    }

    public function activateLocations(): void
    {
        foreach ($this->getLocations() as $location) {
            $location->activate();
        }
    }

    /**
     * @return int[]
     */
    public function getLocationIds(): array
    {
        $ids = [];
        foreach ($this->getLocations() as $location) {
            $ids[] = $location->getLocationId();
        }

        return $ids;
    }

    public function getAccountId(): ?int
    {
        if ($this->accountId === 0) {
            return null;
        }

        return $this->accountId;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function getExpireStamp(): ?\DateTimeImmutable
    {
        return $this->expireStamp;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPostedAt(): \DateTimeImmutable
    {
        return $this->postedAt;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function getMultipleThumbnails(): ?string
    {
        return $this->multipleThumbnails;
    }

    public function getAvailable(): ?int
    {
        return $this->available;
    }

    public function isVisaAccepted(): bool
    {
        return $this->isVisaAccepted;
    }

    public function isAmexAccepted(): bool
    {
        return $this->isAmexAccepted;
    }

    public function isDiscoverAccepted(): bool
    {
        return $this->isDiscoverAccepted;
    }

    public function getFirstName(): ?string
    {
        return $this->getFullName()->getFirstName();
    }

    public function getMiddleName(): ?string
    {
        return $this->getFullName()->getMiddleName();
    }

    public function getLastName(): ?string
    {
        return $this->getFullName()->getLastName();
    }

    private function getFullName(): FullName
    {
        return $this->fullName;
    }

    /**
     * @return EscortImage[]
     */
    public function getImages(): array
    {
        return $this->images->toArray();
    }

    /**
     * @return EscortVideo[]
     */
    public function getVideos(): array
    {
        return $this->videos->toArray();
    }

    public function getRepostLeft(): int
    {
        return $this->repostLeft;
    }

    public function getAutoRenew(): bool
    {
        if ($this->autoRenew === null) {
            return false;
        }

        return $this->autoRenew;
    }

    public function getAutoRenewFrequency(): int
    {
        return $this->autoRenewFrequency;
    }

    public function getAutoRenewTime(): int
    {
        return $this->autoRenewTime;
    }

    public function getSponsor(): bool
    {
        return $this->getSponsorAttributes()->getSponsor();
    }

    public function getSponsorMobile(): bool
    {
        return $this->getSponsorAttributes()->getSponsorMobile();
    }

    public function getSponsorPosition(): ?int
    {
        return $this->getSponsorAttributes()->getSponsorPosition();
    }

    public function getSponsorLocId(): ?int
    {
        return $this->getSponsorAttributes()->getSponsorLocId();
    }

    private function getSponsorAttributes(): SponsorAttributes
    {
        return $this->sponsorAttributes;
    }

    public function getAllowLinks(): bool
    {
        return $this->allowLinks;
    }

    public function getVerified(): bool
    {
        return $this->verified;
    }

    public function getVerifyCallSent(): ?int
    {
        return $this->verifyCallSent;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function getDirectLink(): ?string
    {
        return $this->directLink;
    }

    public function getTerId(): ?string
    {
        if ($this->terId === '') {
            return null;
        }

        return $this->terId;
    }

    public function getTerCheckStamp(): ?int
    {
        return $this->terCheckStamp;
    }

    public function getTerCheckPriority(): ?bool
    {
        return $this->terCheckPriority;
    }

    public function getTerCheckUnapproved(): ?int
    {
        return $this->terCheckUnapproved;
    }

    public function getVerificationId(): ?int
    {
        return $this->getVerification()->getVerificationId();
    }

    public function getVerifiedStamp(): ?int
    {
        return $this->getVerification()->getVerifiedStamp();
    }

    public function getVerifiedBy(): ?int
    {
        return $this->getVerification()->getVerifiedBy();
    }

    private function getVerification(): Verification
    {
        return $this->verification;
    }

    public function setAccountId(int $accountId): void
    {
        $this->accountId = $accountId;
    }

    public function addLocation(EscortLocation $location): void
    {
        $this->locations->add($location);
    }

    private function removeLocation(EscortLocation $location): void
    {
        $this->locations->removeElement($location);
    }

    public function addImage(EscortImage $image): void
    {
        $this->images->add($image);
    }

    public function addVideo(EscortVideo $video): void
    {
        $this->videos->add($video);
    }

    public function setType(EscortType $type): void
    {
        $this->type = $type;
    }

    public function setInActive(): void
    {
        $this->status = Status::processOfCreation();
    }

    /**
     * @throws AlreadySetCity
     * @throws CityNotFound
     */
    public function changeLocation(int $fromLocationId, City $city): void
    {
        /** @var EscortLocation $toLocation */
        $toLocation = $this->locations->filter(
            function (EscortLocation $location) use ($city) {
                return $location->getLocationId() === $city->getId();
            }
        )->first();

        if ($toLocation) {
            throw new AlreadySetCity();
        }

        /** @var EscortLocation $fromLocation */
        $fromLocation = $this->locations->filter(
            function (EscortLocation $location) use ($fromLocationId) {
                return $location->getLocationId() === $fromLocationId;
            }
        )->first();

        if (!$fromLocation) {
            throw new CityNotFound();
        }

        $this->removeLocation($fromLocation);

        $this->addLocation(
            new EscortLocation(
                $this,
                $city->getStateId(),
                $city->getId(),
                $this->type,
                $this->postedAt,
                $this->status
            )
        );
    }
}
