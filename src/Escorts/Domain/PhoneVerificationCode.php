<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

final class PhoneVerificationCode
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @throws \Exception
     */
    public static function generate(): self
    {
        return new self((string) random_int(1000, 9999));
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
