<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class HairColor extends Enumerable
{
    public static function notSet(): self
    {
        return self::createEnum(0);
    }

    public static function lightBrown(): self
    {
        return self::createEnum(1);
    }

    public static function blonde(): self
    {
        return self::createEnum(2);
    }

    public static function darkBrown(): self
    {
        return self::createEnum(3);
    }

    public static function black(): self
    {
        return self::createEnum(4);
    }

    public static function auburnRed(): self
    {
        return self::createEnum(5);
    }

    public static function grayWhite(): self
    {
        return self::createEnum(6);
    }

    public static function wild(): self
    {
        return self::createEnum(7);
    }
}
