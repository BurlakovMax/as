<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class Status extends Enumerable
{
    public static function requestedToDelete(): self
    {
        return self::createEnum(-2);
    }

    public static function processOfCreation(): self
    {
        return self::createEnum(-1);
    }

    public static function expired(): self
    {
        return self::createEnum(0);
    }

    public static function active(): self
    {
        return self::createEnum(1);
    }

    public static function invisible(): self
    {
        return self::createEnum(2);
    }

    public static function inVerification(): self
    {
        return self::createEnum(3);
    }
}