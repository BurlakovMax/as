<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="classifieds_loc")
 */
class EscortLocation
{
    /**
     * @ORM\Column(name="id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer", length=8)
     */
    private int $stateId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Escorts\Domain\Escort")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private Escort $escort;

    /**
     * @ORM\Column(name="loc_id", type="integer", length=8)
     */
    private int $locationId;

    /**
     * @ORM\Column(name="type", type="escort_type")
     */
    private EscortType $type;

    /**
     * @ORM\Column(name="updated", type="datetime_immutable")
     */
    private \DateTimeImmutable $updatedAt;

    /**
     * @ORM\Column(name="done", type="status_type", length=4)
     */
    private Status $status;

    public function __construct(
        Escort $escort,
        int $stateId,
        int $locationId,
        EscortType $type,
        \DateTimeImmutable $updatedAt,
        Status $status
    ) {
        $this->id = null;
        $this->stateId = $stateId;
        $this->escort = $escort;
        $this->locationId = $locationId;
        $this->type = $type;
        $this->updatedAt = $updatedAt;
        $this->status = $status;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getType(): EscortType
    {
        return $this->type;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function activate(): void
    {
        $this->status = Status::active();
        $this->updatedAt = new \DateTimeImmutable();
    }
}
