<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\PhoneNumber;

/**
 * @ORM\Entity(repositoryClass="App\Escorts\Infrastructure\Persistence\PhoneWhitelistDoctrineRepository")
 * @ORM\Table(name="phone_whitelist")
 */
class PhoneWhitelist
{
    /**
     * @ORM\Column(name="id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="phone", type="phone_number")
     */
    private PhoneNumber $phone;

    /**
     * @ORM\Column(name="source", type="string", length=50)
     */
    private string $dataSource;

    /**
     * @ORM\Column(name="added_stamp", type="timestamp", length=11)
     */
    private \DateTimeImmutable $createdAt;

    public function __construct(PhoneNumber $phone, string $dataSource)
    {
        $this->id = 0;
        $this->phone = $phone;
        $this->dataSource = $dataSource;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPhone(): PhoneNumber
    {
        return $this->phone;
    }

    public function getDataSource(): string
    {
        return $this->dataSource;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
