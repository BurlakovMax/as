<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class FullName
{
    /**
     * @ORM\Column(name="firstname", type="string", length=50, nullable=true)
     */
    private ?string $firstName;

    /**
     * @ORM\Column(name="middlename", type="string", length=50, nullable=true)
     */
    private ?string $middleName;

    /**
     * @ORM\Column(name="lastname", type="string", length=50, nullable=true)
     */
    private ?string $lastName;

    public function __construct(
        ?string $firstName,
        ?string $middleName,
        ?string $lastName
    ) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}
