<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

trait EscortContactInfo
{
    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private ?string $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $email;

    /**
     * @ORM\Column(name="reply", type="email_visibility")
     */
    private EmailVisibility $emailVisibility;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private ?string $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $twitter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $telegram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $google;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $instagram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $wechat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $whatsapp;

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getEmailVisibility(): EmailVisibility
    {
        return $this->emailVisibility;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getTelegram(): ?string
    {
        return $this->telegram;
    }

    public function getGoogle(): ?string
    {
        return $this->google;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function getWechat(): ?string
    {
        return $this->wechat;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }
}
