<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LitGroup\Enumerable\Enumerable;

final class EmailVisibility extends Enumerable
{
    public static function anonymous(): self
    {
        return self::createEnum(1);
    }

    public static function disabled(): self
    {
        return self::createEnum(2);
    }

    public static function visible(): self
    {
        return self::createEnum(3);
    }

    public function __toString(): string
    {
        return (string) $this->getRawValue();
    }
}
