<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class CupSize extends Enumerable
{
    public static function A(): self
    {
        return self::createEnum(1);
    }

    public static function B(): self
    {
        return self::createEnum(2);
    }

    public static function C(): self
    {
        return self::createEnum(3);
    }

    public static function D(): self
    {
        return self::createEnum(4);
    }

    public static function DD(): self
    {
        return self::createEnum(5);
    }

    public static function DDD(): self
    {
        return self::createEnum(6);
    }

    public static function plainHuge(): self
    {
        return self::createEnum(7);
    }

    public static function none(): self
    {
        return self::createEnum(0);
    }
}
