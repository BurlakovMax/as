<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\Structures\PhoneNumber;
use Symfony\Component\Messenger\MessageBusInterface;

final class PhoneVerificationService
{
    private PhoneVerificationStorage $phoneVerificationStorage;

    private int $attemptCount;

    private string $expirationOffset;

    private MessageBusInterface $bus;

    public function __construct(
        PhoneVerificationStorage $phoneVerificationStorage,
        int $attemptCount,
        string $expirationOffset,
        MessageBusInterface $bus
    ) {
        $this->phoneVerificationStorage = $phoneVerificationStorage;
        $this->attemptCount = $attemptCount;
        $this->expirationOffset = $expirationOffset;
        $this->bus = $bus;
    }

    /**
     * @throws \Exception
     */
    public function requestPhoneConfirmation(PhoneNumber $phone, ?int $escortId, ?int $accountId): void
    {
        $code = PhoneVerificationCode::generate();
        $verification = $this->phoneVerificationStorage->getAndLock($phone, $accountId, $escortId);

        if (null !== $verification) {
            try {
                $verification->update($code);

                $this->sendVerificationCode($phone, $code);
            } catch (\Throwable $exception) {
                throw new InvalidConfirmation();
            }
        } else {
            try {
                $this->phoneVerificationStorage->add(
                    new PhoneVerification(
                        $accountId,
                        $escortId,
                        $phone,
                        $code,
                        $this->attemptCount
                    )
                );

                $this->sendVerificationCode($phone, $code);
            } catch (\Throwable $exception) {
                throw new InvalidConfirmation();
            }
        }
    }

    /**
     * @throws InvalidConfirmation
     * @throws AccountAndEscortIsEmpty
     * @throws InvalidConfirmationCode
     * @throws \Exception
     */
    public function isValid(PhoneNumber $phone, PhoneVerificationCode $code, ?int $accountId, ?int $escortId): void
    {
        if (null === $accountId && null === $escortId) {
            throw new AccountAndEscortIsEmpty();
        }

        $phoneVerification = $this->phoneVerificationStorage->get($phone, $accountId, $escortId);

        if (null === $phoneVerification) {
            throw new InvalidConfirmation();
        }

        if (!$phoneVerification->isValidCode($code)) {
            throw new InvalidConfirmationCode();
        }

        if (!$phoneVerification->canVerify($this->expirationOffset)) {
            throw new InvalidConfirmation();
        }
    }

    /**
     * @throws InvalidConfirmation
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Exception
     */
    public function confirmPhone(PhoneNumber $phone, ?int $accountId, ?int $escortId): void
    {
        $this->getAndLock($phone, $accountId, $escortId)->markAsVerified();
    }

    /**
     * @throws InvalidConfirmation
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function utilizeConfirmationAttempt(PhoneNumber $phone, ?int $accountId, ?int $escortId): void
    {
        $this->getAndLock($phone, $accountId, $escortId)->utilizeAttempt();
    }

    /**
     * @throws InvalidConfirmation
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Exception
     */
    private function getAndLock(PhoneNumber $phone, ?int $accountId, ?int $escortId): PhoneVerification
    {
        $phoneVerification = $this->phoneVerificationStorage->getAndLock($phone, $accountId, $escortId);

        if ($phoneVerification === null) {
            throw new InvalidConfirmation();
        }

        return $phoneVerification;
    }

    private function sendVerificationCode(PhoneNumber $phone, PhoneVerificationCode $code): void
    {
        $this->bus->dispatch(new PhoneConfirmationRequested($phone, $code->getValue()));
    }
}
