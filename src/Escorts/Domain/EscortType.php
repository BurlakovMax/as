<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class EscortType extends Enumerable
{
    public static function female(): self
    {
        return self::createEnum(1);
    }

    public static function shemale(): self
    {
        return self::createEnum(2);
    }

    /** @deprecated */
    public static function male(): self
    {
        return self::createEnum(3);
    }

    /** @deprecated */
    public static function maleForMale(): self
    {
        return self::createEnum(4);
    }

    /** @deprecated */
    public static function escortAgencies(): self
    {
        return self::createEnum(5);
    }

    public static function bodyRubs(): self
    {
        return self::createEnum(6);
    }

    /** @deprecated */
    public static function maleForMaleBodyRubs(): self
    {
        return self::createEnum(7);
    }

    /** @deprecated */
    public static function strippers(): self
    {
        return self::createEnum(12);
    }

    /** @deprecated */
    public static function BDSMFetishEscorts(): self
    {
        return self::createEnum(13);
    }

    /** @deprecated */
    public static function helpWantedAds(): self
    {
        return self::createEnum(14);
    }

    /** @deprecated */
    public static function webcamSites(): self
    {
        return self::createEnum(15);
    }

    /** @deprecated */
    public static function phoneAndWeb(): self
    {
        return self::createEnum(16);
    }
}
