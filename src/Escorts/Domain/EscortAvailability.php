<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use Doctrine\ORM\Mapping as ORM;

trait EscortAvailability
{
    /**
     * @ORM\Column(name="avl_men", type="boolean")
     */
    private bool $isAvailableForMen;

    /**
     * @ORM\Column(name="avl_women", type="boolean")
     */
    private bool $isAvailableForWomen;

    /**
     * @ORM\Column(name="avl_couple", type="boolean")
     */
    private bool $isAvailableForCouple;

    /**
     * @ORM\Column(name="avl_group", type="boolean")
     */
    private bool $isAvailableForGroup;

    /**
     * @ORM\Column(name="avl_black", type="boolean")
     */
    private bool $isAvailableForBlack;

    /**
     * @var Incall
     *
     * @ORM\Embedded(class="Incall", columnPrefix=false)
     */
    private $incall;

    /**
     * @var Outcall
     *
     * @ORM\Embedded(class="Outcall", columnPrefix=false)
     */
    private $outcall;

    /**
     * @var Visiting
     *
     * @ORM\Embedded(class="App\Escorts\Domain\Visiting", columnPrefix=false)
     */
    private $visitingDetails;

    /**
     * @ORM\Column(name="location", type="string", length=50, nullable=true)
     */
    private ?string $placeDescription;

    /**
     * @ORM\Column(name="gfe", type="boolean")
     */
    private bool $gfe;

    /**
     * @ORM\Column(name="gfe_limited", type="boolean")
     */
    private bool $gfeLimited;

    /**
     * @ORM\Column(name="tantra", type="boolean")
     */
    private bool $tantra;

    /**
     * @ORM\Column(name="fetish_dominant", type="boolean")
     */
    private bool $fetishDominant;

    /**
     * @ORM\Column(name="fetish_submissive", type="boolean")
     */
    private bool $fetishSubmissive;

    /**
     * @ORM\Column(name="fetish_swith", type="boolean")
     */
    private bool $fetishSwitch;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private ?string $fetish;

    public function isAvailableForMen(): bool
    {
        return $this->isAvailableForMen;
    }

    public function isAvailableForWomen(): bool
    {
        return $this->isAvailableForWomen;
    }

    public function isAvailableForCouple(): bool
    {
        return $this->isAvailableForCouple;
    }

    public function isAvailableForGroup(): bool
    {
        return $this->isAvailableForGroup;
    }

    public function isAvailableForBlack(): bool
    {
        return $this->isAvailableForBlack;
    }

    public function isIncallEnabled(): bool
    {
        return $this->getIncall()->getIsEnabled();
    }

    public function getIncallRateHalfHour(): int
    {
        return $this->getIncall()->getIncallRateHalfHour();
    }

    public function getIncallRateOneHour(): int
    {
        return $this->getIncall()->getIncallRateOneHour();
    }

    public function getIncallRateTwoHours(): int
    {
        return $this->getIncall()->getIncallRateTwoHours();
    }

    public function getIncallRateOvernight(): int
    {
        return $this->getIncall()->getIncallRateOvernight();
    }

    public function getIncallRate(): ?int
    {
        return $this->getIncall()->getIncallRate();
    }

    public function isOutcallEnabled(): bool
    {
        return $this->getOutcall()->getIsEnabled();
    }

    public function getOutcallRateHalfHour(): int
    {
        return $this->getOutcall()->getOutcallRateHalfHour();
    }

    public function getOutcallRateOneHour(): int
    {
        return $this->getOutcall()->getOutcallRateOneHour();
    }

    public function getOutcallRateTwoHours(): int
    {
        return $this->getOutcall()->getOutcallRateTwoHours();
    }

    public function getOutcallRateOvernight(): int
    {
        return $this->getOutcall()->getOutcallRateOvernight();
    }

    public function getOutcallRate(): ?int
    {
        return $this->getOutcall()->getOutcallRate();
    }

    public function isVisiting(): bool
    {
        return $this->getVisitingDetails()->isVisiting();
    }

    public function getVisitingFrom(): ?\DateTimeImmutable
    {
        return $this->getVisitingDetails()->getVisitingFrom();
    }

    public function getVisitingTo(): ?\DateTimeImmutable
    {
        return $this->getVisitingDetails()->getVisitingTo();
    }

    public function getPlaceDescription(): ?string
    {
        return $this->placeDescription;
    }

    public function getGfe(): bool
    {
        return $this->gfe;
    }

    public function getGfeLimited(): bool
    {
        return $this->gfeLimited;
    }

    public function getTantra(): bool
    {
        return $this->tantra;
    }

    public function getFetishDominant(): bool
    {
        return $this->fetishDominant;
    }

    public function getFetishSubmissive(): bool
    {
        return $this->fetishSubmissive;
    }

    public function getFetishSwitch(): bool
    {
        return $this->fetishSwitch;
    }

    public function getFetish(): ?string
    {
        return $this->fetish;
    }

    private function getIncall(): Incall
    {
        return $this->incall;
    }

    private function getOutcall(): Outcall
    {
        return $this->outcall;
    }

    private function getVisitingDetails(): Visiting
    {
        return $this->visitingDetails;
    }

}
