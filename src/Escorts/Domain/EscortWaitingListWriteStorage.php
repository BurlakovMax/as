<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface EscortWaitingListWriteStorage
{
    public function add(EscortWaitingList $escortWaitingList): void;

    public function getAndLockByAccountIdAndLocationIdAndType(int $accountId, int $locationId, EscortType $type): ?EscortWaitingList;
}