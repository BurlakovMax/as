<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\DomainEvents\DomainEvent;
use LazyLemurs\Structures\PhoneNumber;

final class PhoneConfirmationRequested extends DomainEvent
{
    private PhoneNumber $phone;

    private string $confirmationCode;

    public function __construct(PhoneNumber $phone, string $confirmationCode)
    {
        parent::__construct();
        $this->phone = $phone;
        $this->confirmationCode = $confirmationCode;
    }

    public function getPhone(): PhoneNumber
    {
        return $this->phone;
    }

    public function getConfirmationCode(): string
    {
        return $this->confirmationCode;
    }
}
