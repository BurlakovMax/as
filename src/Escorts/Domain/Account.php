<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\Structures\Email;

final class Account
{
    private int $id;

    /**
     * @var string[]
     */
    private array $roles;

    private Email $email;

    private string $name;

    private int $forumPostCount;

    private ?bool $isWhitelisted;

    private ?string $avatar;

    private ?int $topUps;

    /**
     * @param array|string[] $roles
     */
    public function __construct(
        int $id,
        $roles,
        Email $email,
        string $name,
        int $forumPostCount,
        ?bool $isWhitelisted,
        ?string $avatar,
        ?int $topUps
    ) {
        $this->id = $id;
        $this->roles = $roles;
        $this->email = $email;
        $this->name = $name;
        $this->isWhitelisted = $isWhitelisted;
        $this->avatar = $avatar;
        $this->topUps = $topUps;
        $this->forumPostCount = $forumPostCount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getIsWhitelisted(): ?bool
    {
        return $this->isWhitelisted;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function getTopUps(): ?int
    {
        return $this->topUps;
    }

    public function getForumPostCount(): int
    {
        return $this->forumPostCount;
    }
}