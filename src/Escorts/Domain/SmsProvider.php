<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use LazyLemurs\Structures\PhoneNumber;

interface SmsProvider
{
    /**
     * @throws \Throwable
     */
    public function send(PhoneNumber $number, string $text): void;
}