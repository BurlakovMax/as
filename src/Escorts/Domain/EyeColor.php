<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

use App\Core\DomainSupport\Enumerable;

final class EyeColor extends Enumerable
{
    public static function notSet(): self
    {
        return self::createEnum(0);
    }

    public static function blue(): self
    {
        return self::createEnum(1);
    }

    public static function brown(): self
    {
        return self::createEnum(2);
    }

    public static function green(): self
    {
        return self::createEnum(3);
    }

    public static function hazel(): self
    {
        return self::createEnum(4);
    }

    public static function black(): self
    {
        return self::createEnum(5);
    }

    public static function gray(): self
    {
        return self::createEnum(6);
    }
}
