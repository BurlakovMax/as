<?php

declare(strict_types=1);

namespace App\Escorts\Domain;

interface AccountServiceProviderInterface
{
    public function removeTopUps(int $accountId, int $topUps): void;
}
