<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\EventSubscribers;

use App\Escorts\Domain\SmsProvider;
use App\Escorts\Domain\PhoneConfirmationRequested;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class PhoneConfirmationSubscriber implements MessageHandlerInterface
{
    private SmsProvider $smsProvider;

    public function __construct(SmsProvider $smsProvider)
    {
        $this->smsProvider = $smsProvider;
    }

    /**
     * @throws \Throwable
     */
    public function __invoke(PhoneConfirmationRequested $event): void
    {
        $this->smsProvider->send(
            $event->getPhone(),
            sprintf('AdultSearch confirmation code: %s', $event->getConfirmationCode())
        );
    }
}