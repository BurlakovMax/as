<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\EventSubscribers;

use App\Escorts\Domain\FileUploaded;
use App\Escorts\Domain\VideoConverter;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class VideoConverterSubscriber implements MessageHandlerInterface
{
    private VideoConverter $videoConverter;

    public function __construct(VideoConverter $videoConverter)
    {
        $this->videoConverter = $videoConverter;
    }

    /**
     * @throws \Throwable
     */
    public function __invoke(FileUploaded $event): void
    {
        $this->videoConverter->convert(
            $event->getName(),
            $event->getPath(),
            $event->getSaveToPath()
        );
    }
}