<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Escorts\Domain\PhoneWhitelist;
use App\Escorts\Domain\PhoneWhitelistStorage;
use Doctrine\ORM\EntityRepository;

final class PhoneWhitelistDoctrineRepository extends EntityRepository implements PhoneWhitelistStorage
{
    /**
     * @return PhoneWhitelist[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.phone',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    public function getCountBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.phone',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(PhoneWhitelist $phone): void
    {
        $this->getEntityManager()->persist($phone);
    }
}
