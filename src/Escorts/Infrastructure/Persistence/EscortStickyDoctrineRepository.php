<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Escorts\Domain\EscortSticky;
use App\Escorts\Domain\EscortStickyReadStorage;
use App\Escorts\Domain\EscortStickyWriteStorage;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\Status;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

final class EscortStickyDoctrineRepository extends EntityRepository implements EscortStickyReadStorage, EscortStickyWriteStorage
{
    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(EscortSticky $escortSticky): void
    {
        $this->getEntityManager()->persist($escortSticky);
    }

    public function getAndLock(int $id): ?EscortSticky
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    /**
     * @return EscortSticky[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @return EscortSticky[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getListBy(int $type, int $locationId): array
    {
        return $this->createQuery()
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                    ->andWhere(Criteria::expr()->eq('type', $type))
            )
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return EscortSticky[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByEscortId(int $escortId): array
    {
        return $this->createQuery()
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->eq('escortId', $escortId))
            )
            ->getQuery()
            ->getResult();
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countByTypeAndLocationId(EscortType $type, int $locationId): int
    {
        $count =
            $this->createQuery()
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(Criteria::expr()->eq('type', $type))
                )
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int) $count['count'];
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSticky
    {
        return
            $this->createQuery()
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('escortId', $escortId))
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\Query\QueryException
     */
    private function createQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->eq('t.status', Status::active()))
            )
        ;
    }
}