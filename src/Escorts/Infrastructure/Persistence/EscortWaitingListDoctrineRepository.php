<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\EscortWaitingList;
use App\Escorts\Domain\EscortWaitingListReadStorage;
use App\Escorts\Domain\EscortWaitingListWriteStorage;
use App\Escorts\Domain\StatusEscortWaitingList;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;

final class EscortWaitingListDoctrineRepository extends EntityRepository implements EscortWaitingListReadStorage, EscortWaitingListWriteStorage
{
    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(EscortWaitingList $escortWaitingList): void
    {
        $this->getEntityManager()->persist($escortWaitingList);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAndLockByAccountIdAndLocationIdAndType(int $accountId, int $locationId, EscortType $type): ?EscortWaitingList
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('accountId', $accountId))
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(Criteria::expr()->eq('type', $type))
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return EscortWaitingList[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBy(int $accountId, int $locationId, EscortType $type): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('accountId', $accountId))
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(Criteria::expr()->eq('type', $type))
                        ->andWhere(
                            Criteria::expr()->orX(
                                Criteria::expr()->eq('t.status', StatusEscortWaitingList::pending()),
                                Criteria::expr()->eq('t.status', StatusEscortWaitingList::reserved())
                            )
                        )
                )
                ->getQuery()
                ->getResult()
            ;
    }
}