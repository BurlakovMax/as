<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Escorts\Domain\EscortLinkToken;
use App\Escorts\Domain\EscortLinkTokenStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\UuidInterface;

final class EscortLinkTokenDoctrineRepository extends EntityRepository implements EscortLinkTokenStorage
{
    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function get(int $escortId, UuidInterface $token): ?EscortLinkToken
    {
        return $this->createQueryBuilder('t')
            ->addCriteria(
                Criteria::create()
                    ->andWhere(Criteria::expr()->eq('t.escortId', $escortId))
                    ->andWhere(Criteria::expr()->eq('t.token', $token->getBytes()))
            )
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(EscortLinkToken $linkToken): void
    {
        $this->getEntityManager()->persist($linkToken);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(EscortLinkToken $linkToken): void
    {
        $this->getEntityManager()->remove($linkToken);
    }
}
