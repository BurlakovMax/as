<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\HairColor;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class HairColorType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return HairColor::class;
    }

    public function getName()
    {
        return 'hair_color';
    }
}