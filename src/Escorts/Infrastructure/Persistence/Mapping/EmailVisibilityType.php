<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\EmailVisibility;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class EmailVisibilityType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return EmailVisibility::class;
    }

    public function getName(): string
    {
        return 'email_visibility';
    }
}
