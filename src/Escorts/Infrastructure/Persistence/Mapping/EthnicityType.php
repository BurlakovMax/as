<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\Ethnicity;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class EthnicityType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Ethnicity::class;
    }

    public function getName()
    {
        return 'ethnicity';
    }
}