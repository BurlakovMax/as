<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence\Mapping;

use App\Escorts\Domain\VideoConvertedStatus;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class VideoConvertedStatusType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return VideoConvertedStatus::class;
    }

    public function getName()
    {
        return 'video_converted_status_type';
    }
}