<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Escorts\Domain\EscortSponsor;
use App\Escorts\Domain\EscortSponsorReadStorage;
use App\Escorts\Domain\EscortSponsorWriteStorage;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\Status;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;

final class EscortSponsorDoctrineRepository extends EntityRepository implements EscortSponsorReadStorage, EscortSponsorWriteStorage
{
    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(EscortSponsor $escortSponsor): void
    {
        $this->getEntityManager()->persist($escortSponsor);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAndLockByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSponsor
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('escortId', $escortId))
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(
                            Criteria::expr()->in(
                                't.status',
                                [
                                    Status::active(),
                                    Status::invisible(),
                                    Status::processOfCreation(),
                                ]
                            )
                        )
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return EscortSponsor[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByEscortId(int $escortId): ?EscortSponsor
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('escortId', $escortId))
                        ->andWhere(Criteria::expr()->gte('status', Status::active()))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByEscortIdAndLocationId(int $escortId, int $locationId): ?EscortSponsor
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('escortId', $escortId))
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return int[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getEscortIdsByLocationId(int $locationId): array
    {
        $sql = "
            (SELECT 1 as sortkey, n.images, l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.ethnicity
                 FROM classifieds_sponsor n
                          INNER JOIN classifieds c on n.post_id = c.id
                          INNER JOIN location_location l on n.loc_id = l.loc_id
                 WHERE c.deleted IS NULL AND c.done = 1 AND n.expire_stamp > UNIX_TIMESTAMP() and n.done = 1 and n.loc_id = :locationId
                 ORDER BY rand()
                 LIMIT 8)
                UNION ALL
                (SELECT 2 as sortkey, '' as images, l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.ethnicity
                 FROM classifieds c
                          INNER JOIN (classifieds_loc n INNER JOIN location_location l on n.loc_id = l.loc_id) on c.id = n.post_id
                 WHERE c.deleted IS NULL AND c.done = 1 AND c.total_loc < 5 AND n.loc_id = :locationId AND c.thumb <> ''
                 ORDER BY c.type ASC, c.id DESC
                 LIMIT 8)
                ORDER BY sortkey ASC
        ";

        $connection = $this->getEntityManager()->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->bindValue('locationId', $locationId);
        $stmt->execute();
        $row = $stmt->fetchAll();

        if (count($row) === 0) {
            return [];
        }

        $res = [];
        foreach ($row as $item) {
            $res[] = (int)$item['id'];
        }

        return $res;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countActiveByLocationId(int $locationId): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(Criteria::expr()->eq('status', Status::active()))
                )
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int) $count['count'];
    }
}