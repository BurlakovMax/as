<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\FileUpload;

use App\Escorts\Domain\FileUpload;
use LazyLemurs\FileUploader\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class VideoFileUpload implements FileUpload
{
    private int $maxFileSize;

    private string $rootDir;

    private string $publicDir;

    private string $dir;

    /** @var array|string[] */
    private array $supportMimeTypes;

    /**
     * @param string[] $supportMimeTypes
     */
    public function __construct(
        int $maxFileSize,
        string $rootDir,
        string $publicDir,
        string $dir,
        array $supportMimeTypes
    ) {
        $this->maxFileSize = $maxFileSize;
        $this->rootDir = $rootDir;
        $this->publicDir = $publicDir;
        $this->dir = $dir;
        $this->supportMimeTypes = $supportMimeTypes;
    }

    /**
     * @throws \LazyLemurs\FileUploader\MimeTypeIsNotSupported
     * @throws UploadFileSizeIsTooBig
     */
    public function save(string $name, UploadedFile $file): string
    {
        if ($file->getSize() > $this->maxFileSize) {
            throw new UploadFileSizeIsTooBig();
        }

        $video = new File(
            $name,
            $this->rootDir,
            $this->publicDir,
            $this->dir,
            $this->supportMimeTypes,
            $file
        );

        $video->save();

        return $video->getDirWithFile();
    }
}