<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Adapter;

use LazyLemurs\Structures\PhoneNumber;
use App\Escorts\Domain\SmsProvider;
use App\SmsGate\Domain\MessageService;
use Ramsey\Uuid\Uuid;

final class SmsGateAdapter implements SmsProvider
{
    private MessageService $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function send(PhoneNumber $number, string $text): void
    {
        $this->messageService->sendSync(Uuid::uuid4(), $number, $text);
    }
}