<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Adapter;

use App\Escorts\Domain\TopCityReadStorage;
use App\Locations\Application\TopCityQueryProcessor;

final class TopCityProvider implements TopCityReadStorage
{
    private TopCityQueryProcessor $topCityQueryProcessor;

    public function __construct(TopCityQueryProcessor $topCityQueryProcessor)
    {
        $this->topCityQueryProcessor = $topCityQueryProcessor;
    }

    public function isBigCity(int $id): bool
    {
        return $this->topCityQueryProcessor->isBigCity($id);
    }
}