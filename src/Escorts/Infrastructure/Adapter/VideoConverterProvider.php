<?php

declare(strict_types=1);

namespace App\Escorts\Infrastructure\Adapter;

use App\VideoConverter\Infrastructure\VideoConverter as Converter;
use App\Escorts\Domain\VideoConverter;

final class VideoConverterProvider implements VideoConverter
{
    private Converter $videoConverter;

    public function __construct(Converter $videoConverter)
    {
        $this->videoConverter = $videoConverter;
    }

    public function convert(string $name, string $srcPathToUploadedFile, string $saveToPath): void
    {
        $this->videoConverter->convert($name, $srcPathToUploadedFile, $saveToPath);
    }
}