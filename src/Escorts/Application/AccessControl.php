<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\AccessControl\AccessForbidden;

final class AccessControl
{
    private EscortQueryProcessor $escortQueryProcessor;

    public function __construct(EscortQueryProcessor $escortQueryProcessor)
    {
        $this->escortQueryProcessor = $escortQueryProcessor;
    }

    /**
     * @throws AccessForbidden
     * @throws EscortNotFound
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function checkAccessToEscort(int $id, int $accountId): void
    {
        $escort = $this->escortQueryProcessor->getById($id);

        if ($escort->getAccountId() !== $accountId) {
            throw new AccessForbidden();
        }
    }
}
