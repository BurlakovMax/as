<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\Search\SearchQuery;
use App\Escorts\Domain\AllowOnlyAgency;
use App\Escorts\Domain\City;
use App\Escorts\Domain\CityReadStorage;
use App\Escorts\Domain\EscortAlreadyPurchased;
use App\Escorts\Domain\EscortSticky;
use App\Escorts\Domain\EscortStickyDurationNotSelected;
use App\Escorts\Domain\EscortStickyReadStorage;
use App\Escorts\Domain\EscortType;
use App\Escorts\Domain\TopCityReadStorage;

final class EscortStickyQueryProcessor
{
    private const WEEK = 7;

    private const MONTH = 30;

    private EscortStickyReadStorage $escortStickyReadStorage;

    private CityReadStorage $cityReadStorage;

    private TopCityReadStorage $topCityReadStorage;

    private int $expireInDays;

    private int $maximumNumberPerCity;

    public function __construct(
        EscortStickyReadStorage $escortStickyReadStorage,
        CityReadStorage $cityReadStorage,
        TopCityReadStorage $topCityReadStorage,
        int $expireInDays,
        int $maximumNumberPerCity
    ) {
        $this->escortStickyReadStorage = $escortStickyReadStorage;
        $this->cityReadStorage = $cityReadStorage;
        $this->topCityReadStorage = $topCityReadStorage;
        $this->expireInDays = $expireInDays;
        $this->maximumNumberPerCity = $maximumNumberPerCity;
    }

    /**
     * @return EscortStickyData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $list = $this->escortStickyReadStorage->getBySearchQuery($query);

        return $this->mapListToData($list);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->escortStickyReadStorage->countBySearchQuery($query);
    }

    /**
     * @param EscortSticky[] $list
     * @return EscortStickyData[]
     */
    private function mapListToData(array $list): array
    {
        return array_map(
            function (EscortSticky $escortSticky): EscortStickyData {
                return $this->mapToData($escortSticky);
            },
            $list
        );
    }

    /**
     * @return EscortStickyData[]
     */
    public function getListBy(int $type, int $locationId): array
    {
        $escortStickies = $this->escortStickyReadStorage->getListBy($type, $locationId);
        shuffle($escortStickies);

        return array_map([ $this, 'mapToData' ], $escortStickies);
    }

    /**
     * @return int[]
     */
    public function getStickyEscortIds(int $type, int $locationId): array
    {
        $escortStickies = $this->getListBy($type, $locationId);

        $escortIds = [];
        foreach ($escortStickies as $escortSticky) {
            $escortIds[] = $escortSticky->getEscortId();
        }

        return $escortIds;
    }

    /**
     * @return EscortStickyData[]
     */
    public function getByEscortId(int $escortId): array
    {
        $escortStickies = $this->escortStickyReadStorage->getByEscortId($escortId);

        return array_map([$this, 'mapToData'], $escortStickies);
    }

    private function mapToData(EscortSticky $sponsor): EscortStickyData
    {
        return new EscortStickyData($sponsor);
    }

    private function hasByLocationId(int $escortId, int $locationId): bool
    {
        $locationIds = array_map(fn(EscortStickyData $escort): int => $escort->getLocationId(), $this->getByEscortId($escortId));
        return in_array($locationId, $locationIds);
    }

    private function hasActiveByLocationId(int $escortId, int $locationId): bool
    {
        $escorts = $this->getByEscortId($escortId);
        if (empty($escorts)) {
            return false;
        }

        $locationIds = [];
        foreach ($escorts as $escort) {
            if (!$escort->isExpired()) {
                $locationIds[] = $escort->getLocationId();
            }
        }

        return in_array($locationId, $locationIds);
    }

    /**
     * @param string[] $checkedDaysForLocation
     */
    private function hasDurationForCity(array $checkedDaysForLocation, int $locationId): bool
    {
        return in_array((int) $checkedDaysForLocation[$locationId], [7, 14, 30]);
    }

    /**
     * @param string[] $locationIds
     * @param string[] $checkedDaysForLocation
     *
     * @throws EscortAlreadyPurchased
     * @throws EscortStickyDurationNotSelected
     */
    public function calculateTotalPrice(
        int $escortId,
        int $escortType,
        bool $isAgency,
        array $locationIds,
        array $checkedDaysForLocation
    ): string {

        $amount = 0;
        foreach ($locationIds as $locationId) {
            $city = $this->cityReadStorage->getById((int) $locationId);

            $hasActiveEscortSticky = $this->hasActiveByLocationId($escortId, $city->getId());
            if ($hasActiveEscortSticky) {
                throw new EscortAlreadyPurchased();
            }

            if (!$this->hasDurationForCity($checkedDaysForLocation, $city->getId())) {
                throw new EscortStickyDurationNotSelected();
            }

            $amount += $this->calculatePrice(
                $escortType,
                $city->getId(),
                (int) $checkedDaysForLocation[ $locationId ],
                $this->topCityReadStorage->isBigCity($city->getId()),
                $isAgency
            );
        }

        if ($amount === 0) {
            throw new \RuntimeException('Invalid total price, please contact support@adultsearch.com');
        }

        return (string) $amount;
    }

    /**
     * @param int ...$locationIds
     * @return CityAvailable[]
     */
    public function canBuy(int $type, int ...$locationIds): array
    {
        $availables = [];
        foreach ($locationIds as $locationId) {
            $numberPerCity = $this->escortStickyReadStorage->countByTypeAndLocationId(
                EscortType::getValueOf($type),
                $locationId
            );

            if ($this->maximumNumberPerCity <= $numberPerCity) {
                $availables[] = new CityAvailable($locationId, false);
                continue;
            }

            if ($this->hasByLocationId($type, $locationId)) {
                $availables[] = new CityAvailable($locationId, false);
                continue;
            }

            $availables[] = new CityAvailable($locationId, true);
        }

        return $availables;
    }

    /**
     * @param City ...$cities
     * @return EscortStickyPrice[]
     */
    public function getPrice(int $type, bool $isAgency, City ...$cities): array
    {
        $price = [];
        foreach ($cities as $city) {
            $price[] = new EscortStickyPrice(
                $this->calculatePrice(
                    $type,
                    $city->getId(),
                    self::MONTH,
                    $this->topCityReadStorage->isBigCity($city->getId()),
                    $isAgency
                ),
                $this->calculatePrice(
                    $type,
                    $city->getId(),
                    self::WEEK,
                    $this->topCityReadStorage->isBigCity($city->getId()),
                    $isAgency
                ),
                $city->getId()
            );
        }

        return $price;
    }

    /**
     * @todo Create table with price
     */
    private function calculatePrice(int $escortType, int $locationId, int $periodInDays, bool $isBigCity, bool $isAgency): int
    {
        // $locationId 18308 is NYC
        switch (EscortType::getValueOf($escortType)){
            case EscortType::female():
                if (!$isAgency) {
                    throw new AllowOnlyAgency();
                }

                if ($periodInDays === 7) {
                    if ($locationId === 18308) {
                        return 500;
                    }

                    return $isBigCity ? 350 : 100;
                }

                if ($locationId === 18308) {
                    return 1500;
                }

                return $isBigCity ? 1000 : 400;

            case EscortType::shemale():
                if ($periodInDays === 7) {
                    return 50;
                }

                return 150;

            case EscortType::bodyRubs():
                if ($periodInDays === 7) {
                    return $isBigCity ? 100 : 75;
                }

                return $isBigCity ? 350 : 250;
            default:
                return 0;
        }
    }

    public function getRecurringPeriod(): int
    {
        return $this->expireInDays;
    }
}
