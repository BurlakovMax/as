<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\CacheProcessor;
use App\Escorts\Domain\FileUpload;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class ImageUploadProcessor
{
    private const TTL = 3600;

    private FileUpload $fileUpload;

    private CacheProcessor $cache;

    private string $rootDir;

    private string $publicDir;

    private string $dir;

    public function __construct(FileUpload $fileUpload, CacheProcessor $cache, string $rootDir, string $publicDir, string $dir)
    {
        $this->fileUpload = $fileUpload;
        $this->cache = $cache;
        $this->rootDir = $rootDir;
        $this->publicDir = $publicDir;
        $this->dir = $dir;
    }

    public function save(string $name, UploadedFile $uploadedFile): ImageUploadedData
    {
        $id = Uuid::uuid4()->toString();
        $url = $this->fileUpload->save($name, $uploadedFile);
        $this->cache->writeToCacheWithTTL($id, $url, self::TTL);

        $chunks = explode('/', $url);

        return
            new ImageUploadedData(
                array_pop($chunks),
                $url,
                $id
            )
        ;
    }

    /**
     * @throws ImageUploadedNotFound
     */
    public function remove(string $id): void
    {
        $image = $this->cache->readFromCache($id);

        if (null === $image) {
            throw new ImageUploadedNotFound();
        }

        unlink($this->rootDir . $this->publicDir. $image);
    }

    public function removeByName(string $name): void
    {
        if (file_exists($this->rootDir . $this->publicDir . $this->dir . $name)) {
            unlink($this->rootDir . $this->publicDir . $this->dir . $name);
        }
    }
}