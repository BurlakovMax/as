<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\City;
use App\Escorts\Domain\EscortLocation;

final class EscortLocationData
{
    private EscortLocation $escortLocation;

    private City $city;

    public function __construct(EscortLocation $escortLocation, City $city)
    {
        $this->escortLocation = $escortLocation;
        $this->city = $city;
    }

    public function getId(): ?int
    {
        return $this->getEscortLocation()->getId();
    }

    public function getUrl(): string
    {
        return $this->getCity()->getUrl();
    }

    public function getCountryName(): string
    {
        return $this->getCity()->getCountryName();
    }

    public function getCityName(): string
    {
        return $this->getCity()->getName();
    }

    public function getStateName(): ?string
    {
        return $this->getCity()->getStateName();
    }

    public function getStateId(): int
    {
        return  $this->getEscortLocation()->getStateId();
    }

    public function getLocationId(): int
    {
        return $this->getEscortLocation()->getLocationId();
    }

    public function getType(): int
    {
        return $this->getEscortLocation()->getType()->getRawValue();
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->getEscortLocation()->getUpdatedAt();
    }

    public function getStatus(): int
    {
        return $this->getEscortLocation()->getStatus()->getRawValue();
    }

    private function getEscortLocation(): EscortLocation
    {
        return $this->escortLocation;
    }

    private function getCity(): City
    {
        return $this->city;
    }
}
