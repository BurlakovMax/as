<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\PhoneBlacklist;
use LazyLemurs\Structures\PhoneNumber;

final class PhoneBlacklistData
{
    private int $id;

    private PhoneNumber $phone;

    private string $reason;

    private \DateTimeImmutable $createdAt;

    private int $createdBy;

    public function __construct(PhoneBlacklist $phoneList)
    {
        $this->id = $phoneList->getId();
        $this->phone = $phoneList->getPhone();
        $this->reason = $phoneList->getReason();
        $this->createdAt = $phoneList->getCreatedAt();
        $this->createdBy = $phoneList->getCreatedBy();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPhone(): PhoneNumber
    {
        return $this->phone;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): int
    {
        return $this->createdBy;
    }
}
