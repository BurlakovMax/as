<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\EmailGate\Domain\EmailData;
use App\Escorts\Domain\AccountReadStorage;
use App\Escorts\Domain\City;
use App\Escorts\Domain\CityReadStorage;
use App\Escorts\Domain\EmailProvider;
use App\Escorts\Domain\Escort;
use App\Escorts\Domain\EscortDoesNotContainsCity;
use App\Escorts\Domain\EscortReadStorage;
use App\Escorts\Domain\EscortService;
use App\Escorts\Domain\EscortSide;
use App\Escorts\Domain\EscortSideReadStorage;
use App\Escorts\Domain\EscortSideWriteStorage;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;

final class EscortSideCommandProcessor
{
    private EscortSideWriteStorage $escortSideWriteStorage;

    private EscortSideReadStorage $escortSideReadStorage;

    private EscortReadStorage $escortReadStorage;

    private EscortService $escortService;

    private CityReadStorage $cityReadStorage;

    private AccountReadStorage $accountReadStorage;

    private EmailProvider $emailProvider;

    private int $expireInDays;

    private string $fromEmail;

    public function __construct(
        EscortSideWriteStorage $escortSideWriteStorage,
        EscortSideReadStorage $escortSideReadStorage,
        EscortReadStorage $escortReadStorage,
        EscortService $escortService,
        CityReadStorage $cityReadStorage,
        AccountReadStorage $accountReadStorage,
        EmailProvider $emailProvider,
        int $expireInDays,
        string $fromEmail
    ) {
        $this->escortSideWriteStorage = $escortSideWriteStorage;
        $this->escortSideReadStorage = $escortSideReadStorage;
        $this->escortReadStorage = $escortReadStorage;
        $this->escortService = $escortService;
        $this->cityReadStorage = $cityReadStorage;
        $this->accountReadStorage = $accountReadStorage;
        $this->emailProvider = $emailProvider;
        $this->expireInDays = $expireInDays;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @throws EscortDoesNotContainsCity
     * @throws EscortNotFound
     * @throws \Throwable
     */
    public function activateAfterPaid(int $escortId, int $locationId): void
    {
        $escort = $this->escortReadStorage->get($escortId);

        if (null === $escort) {
            throw new EscortNotFound();
        }

        if (!$escort->hasLocationsByLocationIds([$locationId])) {
            throw new EscortDoesNotContainsCity();
        }

        $city = $this->cityReadStorage->getById($locationId);
        $escortSide = $this->escortSideReadStorage->getByEscortIdAndLocationId($escort->getId(), $locationId);

        if (null === $escortSide) {
            $this->create($escort, $city);
        } else {
            $this->renew($escort, $city);
        }
    }

    /**
     * @throws \Throwable
     */
    private function create(Escort $escort, City $city): void
    {
        $this->escortSideWriteStorage->add(
            new EscortSide(
                $city->getId(),
                $city->getStateId(),
                $escort->getId(),
                $this->expireInDays,
                $escort->getType()
            )
        );

        $this->updateAfterPaidService($escort);
    }

    /**
     * @throws \Throwable
     */
    private function renew(Escort $escort, City $city): void
    {
        $escortSide = $this->escortSideWriteStorage->getAndLockByEscortIdAndLocationId($escort->getId(), $city->getId());
        $escortSide->renew($this->expireInDays);
        $this->updateAfterPaidService($escort);
    }

    private function updateAfterPaidService(Escort $escort): void
    {
        $escort->updateAfterPaidService($this->expireInDays, new \DateTimeImmutable('+' . $this->expireInDays . 'days'));

        $this->emailProvider->sendSync(
            new EmailData(
                Uuid::uuid4(),
                new Email($this->fromEmail),
                $this->accountReadStorage->getAccount($escort->getAccountId())->getEmail(),
                'Receipt for your advertisement',
                'Receipt for your advertisement',
                new Email($this->fromEmail)
            )
        );
    }

    public function getRecurringPeriod(): int
    {
        return $this->expireInDays;
    }
}