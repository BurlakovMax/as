<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\PhoneWhitelist;
use App\Escorts\Domain\PhoneWhitelistStorage;
use LazyLemurs\Structures\PhoneNumber;
use LazyLemurs\TransactionManager\TransactionManager;

final class PhoneWhitelistCommandProcessor
{
    private PhoneWhitelistStorage $storage;

    private TransactionManager $transactionManager;

    public function __construct(PhoneWhitelistStorage $storage, TransactionManager $transactionManager)
    {
        $this->storage = $storage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function add(PhoneNumber $phoneNumber, string $dataSource): void
    {
        $this->transactionManager->transactional(function () use ($phoneNumber, $dataSource): void {
           $this->storage->add(new PhoneWhitelist($phoneNumber, $dataSource));
        });
    }
}
