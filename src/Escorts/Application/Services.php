<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use Ds\Map;

final class Services extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put('terId', 'TER Reviewed');
        $map->put('isPornstar', 'Pornstar');
        $map->put('tantra', 'Tantra Massage');
        $map->put('isVisiting', 'Visiting');
        $map->put('featured', 'Reatured');

        return $map;
    }
}