<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortType;

final class TypesData
{
    public static function female(): int
    {
        return EscortType::female()->getRawValue();
    }

    public static function shemale(): int
    {
        return EscortType::shemale()->getRawValue();
    }

    public static function bodyRubs(): int
    {
        return EscortType::bodyRubs()->getRawValue();
    }
}