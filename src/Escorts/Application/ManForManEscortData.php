<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use Swagger\Annotations as SWG;

final class ManForManEscortData
{
    /**
     * @SWG\Property()
     */
    private string $url;

    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private int $count;

    public function __construct(int $id, string $url, string $name, int $count)
    {
        $this->id = $id;
        $this->url = $url;
        $this->name = $name;
        $this->count = $count;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}