<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\PhoneWhitelist;
use LazyLemurs\Structures\PhoneNumber;

final class PhoneWhitelistData
{
    private int $id;

    private PhoneNumber $phone;

    private $dataSource;

    private \DateTimeImmutable $createdAt;

    public function __construct(PhoneWhitelist $phoneList)
    {
        $this->id = $phoneList->getId();
        $this->phone = $phoneList->getPhone();
        $this->dataSource = $phoneList->getDataSource();
        $this->createdAt = $phoneList->getCreatedAt();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPhone(): PhoneNumber
    {
        return $this->phone;
    }

    public function getDataSource()
    {
        return $this->dataSource;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
