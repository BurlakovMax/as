<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Locations\Application\CityData;
use App\Locations\Application\StateData;

final class ManForManEscortQueryProcessor
{
    public function getCurrentM4mLink(?StateData $state = null, ?CityData $city = null): ManForManEscortData
    {
        $cities = [
            'atlanta', 'austin', 'boston', 'brooklyn', 'chicago', 'cincinnati', 'cleveland', 'columbus', 'connecticut', 'dfw', 'denver', 'detroit', 'dubai', 'fort-lauderdale', 'fresno', 'houston', 'las-vegas', 'london', 'long-island', 'los-angeles', 'miami', 'minneapolis', 'montreal', 'new-jersey', 'new-orleans', 'new-york', 'orange-county', 'orlando', 'philadelphia', 'phoenix', 'pittsburgh', 'portland', 'queens', 'raleigh', 'sacramento', 'san-antonio', 'san-diego', 'san-francisco', 'san-jose', 'seattle', 'tampa', 'toronto', 'vancouver', 'live-video-meeting', 'washington-dc', 'live-in', 'live-video-meeting', 'bronx', 'brooklyn', 'connecticut', 'hamptons', 'hudson-valley', 'long-island', 'new-jersey', 'new-york', 'queens', 'staten-island', 'westchester', 'bakersfield', 'barstow', 'inland-empire', 'long-beach', 'los-angeles', 'orange-county', 'palm-springs', 'palmdale', 'san-diego', 'san-fernando', 'san-gabriel-valley', 'san-luis-obispo', 'santa-barbara', 'ventura', 'victorville', 'fresno', 'merced', 'modesto', 'monterey', 'napa-sonoma', 'north-bay', 'oakland', 'sacramento', 'san-francisco', 'san-jose', 'santa-cruz', 'stockton', 'visalia', 'boca-raton', 'daytona-beach', 'fort-lauderdale', 'fort-myers', 'gainesville', 'jacksonville', 'key-west', 'lakeland', 'marion-county', 'melbourne-florida', 'miami', 'naples-florida', 'orlando', 'pensacola', 'punta-gorda', 'sarasota', 'tallahassee', 'tampa', 'titusville', 'treasure-coast', 'west-palm-beach', 'abilene', 'amarillo', 'austin', 'beaumont', 'brownsville', 'college-station', 'corpus-christi', 'dfw', 'el-paso', 'houston', 'killeen', 'longview', 'lubbock', 'mcallen', 'midland-odessa', 'san-antonio', 'tyler', 'victoria-texas', 'waco', 'wichita-falls', 'abbotsford', 'barrie', 'calgary-edmonton', 'halifax', 'hamilton-ontario', 'kelowna', 'kingston-ontario', 'kitchener', 'london-ontario', 'montreal', 'new-brunswick', 'niagara-falls', 'oshawa', 'ottawa', 'prince-edward-island', 'quebec-city', 'sherbrooke', 'st-johns', 'sudbury', 'toronto', 'vancouver', 'victoria', 'windsor', 'winnipeg', 'alabama', 'anchorage', 'flagstaff', 'phoenix', 'tucson', 'yavapai-county', 'arkansas', 'aspen', 'boulder', 'colorado-springs', 'denver', 'fort-collins', 'grand-junction', 'greeley', 'hartford', 'new-haven', 'new-london', 'connecticut', 'delaware', 'washington-dc', 'albany-georgia', 'athens-ga', 'atlanta', 'augusta', 'columbus-ga', 'gainesville-ga', 'macon', 'rome-georgia', 'savannah', 'valdosta', 'honolulu', 'idaho', 'aurora-naperville', 'bloomington', 'champaign-il', 'chicago', 'decatur-il', 'joliet', 'peoria', 'rockford', 'springfield-il', 'bloomington-indiana', 'elkhart', 'evansville', 'fort-wayne', 'indianapolis', 'kokomo-indiana', 'michigan-city', 'muncie', 'northwest-indiana', 'south-bend', 'terre-haute', 'des-moines', 'quad-cities', 'waterloo-cedar-falls', 'kansas-city', 'lawrence-kansas', 'topeka', 'wichita', 'bowling-green-ky', 'elizabethtown', 'lexington', 'louisville', 'alexandria-louisiana', 'baton-rouge', 'houma', 'lafayette-la', 'lake-charles', 'monroe-louisiana', 'new-orleans', 'shreveport', 'maine', 'baltimore', 'cumberland-maryland', 'hagerstown', 'lexington-park', 'salisbury', 'suburban-maryland', 'boston', 'framingham', 'lowell', 'provincetown', 'springfield-ma', 'worcester', 'ann-arbor', 'battle-creek', 'bay-city', 'detroit', 'flint', 'grand-rapids', 'jackson-mi', 'lansing', 'monroe-michigan', 'muskegon', 'saginaw', 'saugatuck', 'traverse-city', 'duluth', 'mankato', 'minneapolis', 'st-cloud', 'mississippi', 'cape-girardeau', 'columbia-missouri', 'jefferson-city', 'joplin', 'kansas-city', 'springfield-mo', 'st-louis', 'lincoln-ne', 'omaha', 'carson-city', 'lake-tahoe', 'las-vegas', 'reno', 'new-hampshire', 'atlantic-city', 'camden', 'new-jersey', 'trenton', 'albuquerque', 'las-cruces', 'santa-fe', 'albany', 'buffalo', 'hudson-valley', 'long-island', 'new-york', 'plattsburgh', 'poughkeepsie', 'rochester-ny', 'schenectady', 'syracuse', 'westchester', 'asheville', 'burlington-north-carolina', 'charlotte', 'fayetteville-nc', 'greensboro', 'hickory', 'jacksonville-north-carolina', 'raleigh', 'wilmington', 'winston-salem', 'north-dakota', 'akron', 'bowling-green-ohio', 'canton', 'cincinnati', 'cleveland', 'columbus', 'dayton', 'lima-ohio', 'mansfield-ohio', 'toledo', 'lawton', 'oklahoma-city', 'tahlequah', 'tulsa', 'eugene', 'grants-pass', 'medford', 'portland', 'salem', 'allentown', 'altoona', 'bloomsburg-pennsylvania', 'erie-pa', 'gettysburg', 'harrisburg', 'lancaster-pa', 'philadelphia', 'pittsburgh', 'reading', 'scranton', 'state-college-pennsylvania', 'york', 'ponce', 'san-juan-puerto-rico', 'providence', 'south-carolina', 'south-dakota', 'chattanooga', 'knoxville', 'memphis', 'morristown', 'nashville', 'ogden', 'provo', 'salt-lake-city', 'st-george', 'vermont', 'blacksburg', 'norfolk', 'northern-virginia', 'richmond', 'roanoke', 'virginia-beach', 'bellingham', 'bremerton', 'everett', 'olympia', 'seattle', 'spokane', 'tacoma', 'tri-cities-wa', 'vancouver-washington', 'walla-walla', 'wenatchee', 'west-virginia', 'appleton', 'fond-du-lac', 'green-bay', 'kenosha', 'la-crosse', 'madison', 'milwaukee', 'oshkosh', 'wausau', 'wyoming', 'hong-kong', 'macau', 'shanghai', 'ahmedabad', 'bangalore', 'chennai', 'delhi', 'hyderabad', 'jaipur', 'mumbai', 'jakarta', 'kuala-lumpur', 'davao-city', 'manila', 'quezon-city', 'singapore', 'brussels', 'prague', 'copenhagen', 'mareille', 'paris', 'berlin', 'bremen', 'cologne', 'dusseldorf', 'frankfurt', 'hamburg', 'munich', 'budapest', 'milan', 'turin', 'riga', 'amsterdam', 'rotterdam', 'oslo', 'lisbon', 'barcelona', 'madrid', 'bern', 'geneva', 'zurich', 'istanbul', 'birmingham', 'leeds', 'liverpool', 'london', 'manchester', 'bogota', 'cartagena', 'medellin', 'cancun', 'mexico', 'monterrey', 'playa-del-carmen', 'puerto-vallarta', 'lima', 'adelaide', 'brisbane', 'canberra', 'gold-coast', 'melbourne', 'newcastle-nsw', 'perth', 'sydney', 'auckland', 'christchurch', 'hamilton-new-zealand', 'accra', 'south-africa', 'cayman-islands', 'guyana', 'jamaica', 'abu-dhabi', 'bahrain', 'beirut', 'cairo', 'doha', 'dubai', 'kuwait', 'riyadh', 'tel-aviv'];

        $suffix = '';
        $cityName = '';

        if (null !== $city) {
            $cityName = strtolower($city->getName());
        }

        $stateName = null !== $state ? strtolower($state->getName()) : '';
        $stateCode = null !== $state ? strtolower($state->getCode()) : '';

        if (($cityName === 'dallas' || $cityName === 'fort worth') && $stateName === 'texas') {
            $suffix = 'dfw';
        } elseif ($cityName === 'saint louis' && $stateName === 'missouri') {
            $suffix = 'st-louis';
        } elseif ($cityName === 'new york city' && $stateName === 'new york') {
            $suffix = 'new-york';
        } elseif (in_array($cityName . '-' . $stateName, $cities, true)) {
            $suffix = $cityName . '-' . $stateName;
        } elseif (in_array($cityName . '-' . $stateCode, $cities, true)) {
            $suffix = $cityName . '-' . $stateCode;
        } elseif (in_array($cityName, $cities, true)) {
            $suffix = $cityName;
        }

        $url = $suffix !== '' ? "https://www.mintboys.com/male-escorts/{$suffix}" : 'https://www.mintboys.com';

        return new ManForManEscortData(
            $city->getId(),
            $url . '?utm_source=adultsearch&utm_medium=category&utm_campaign=m4m',
            'M4M Escorts',
            0
        );
    }
}