<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\Status;

final class StatusConverter
{
    public static function valueToName(int $value): string
    {
        switch ($value) {
            case -2:
                return 'Requested to delete';
            case -1:
                return 'Process of creation';
            case 0:
                return 'Expired';
            case 1:
                return 'Active';
            case 2:
                return 'Invisible';
            case 3:
                return 'In verification';
            default:
                return 'Female Escorts';
        }
    }

    /**
     * @return <string, string>
     */
    public static function listOfStatuses(): array
    {
        return array_map(
            function (Status $status): array {
                return [
                    $status->getRawValue() => ["name" => static::valueToName($status->getRawValue())],
                ];
            },
            [
                Status::requestedToDelete(),
                Status::processOfCreation(),
                Status::expired(),
                Status::active(),
                Status::invisible(),
                Status::inVerification(),
            ]
        );
    }
}
