<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortSticky;
use Swagger\Annotations as SWG;

final class EscortStickyData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $escortId;

    /**
     * @SWG\Property()
     */
    private int $locationId;

    /**
     * @SWG\Property()
     */
    private ?int $type;

    /**
     * @SWG\Property()
     */
    private ?string $typeInString;

    /**
     * @SWG\Property()
     */
    private ?int $position;

    /**
     * @SWG\Property()
     */
    private ?int $inDays;

    /**
     * @SWG\Property(type="date")
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @SWG\Property(type="date")
     */
    private ?\DateTimeImmutable $expireAt;

    /**
     * @SWG\Property()
     */
    private bool $isExpired;

    /**
     * @SWG\Property()
     */
    private int $status;

    /**
     * @SWG\Property()
     */
    private string $statusInString;

    /**
     * @SWG\Property()
     */
    private bool $notified;

    public function __construct(EscortSticky $escortSticky)
    {
        $this->id = $escortSticky->getId();
        $this->escortId = $escortSticky->getEscortId();
        $this->locationId = $escortSticky->getLocationId();
        $this->type = $escortSticky->getType() ? $escortSticky->getType()->getRawValue() : null;
        $this->typeInString = $escortSticky->getType() ? EscortTypeConverter::valueToName($escortSticky->getType()->getRawValue()) : null;
        $this->position = $escortSticky->getPosition();
        $this->inDays = $escortSticky->getInDays();
        $this->createdAt = $escortSticky->getCreatedAt();
        $this->expireAt = $escortSticky->getExpireAt();
        $this->isExpired = $escortSticky->isExpired();
        $this->status = $escortSticky->getStatus()->getRawValue();
        $this->statusInString = StatusConverter::valueToName($escortSticky->getStatus()->getRawValue());
        $this->notified = $escortSticky->getNotified();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function getTypeInString(): ?string
    {
        return $this->typeInString;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function getInDays(): ?int
    {
        return $this->inDays;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getExpireAt(): ?\DateTimeImmutable
    {
        return $this->expireAt;
    }

    public function isExpired(): bool
    {
        return $this->isExpired;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusInString(): string
    {
        return $this->statusInString;
    }

    public function getNotified(): bool
    {
        return $this->notified;
    }
}
