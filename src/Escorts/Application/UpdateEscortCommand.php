<?php

declare(strict_types=1);

namespace App\Escorts\Application;

final class UpdateEscortCommand extends EscortCommand
{
    private int $id;

    public function __construct(
        int $id,
        ?int $accountId
    ) {
        parent::__construct($accountId);

        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
