<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\DomainSupport\Enumerable;
use App\Escorts\Domain\EscortType;

final class EscortTypeConverter
{
    public static function valueToName(int $value): string
    {
        switch ($value) {
            case 2:
                return 'Shemale Escorts';
            case 6:
                return 'Body Rubs';
            default:
                return 'Female Escorts';
        }
    }

    public static function valueToSlug(int $value): string
    {
        switch ($value) {
            case 2:
                return 'tstv-shemale-escorts';
            case 6:
                return 'body-rubs';
            default:
                return 'female-escorts';
        }
    }

    public static function valueToEnum(string $value): Enumerable
    {
        switch ($value) {
            case 'tstv-shemale-escorts':
                return EscortType::shemale();
            case 'body-rubs':
                return EscortType::bodyRubs();
            case 'female-escorts':
                return EscortType::female();
            default:
                throw new \InvalidArgumentException('EscortType is unsupported');
        }
    }

    /**
     * @return <string, string>
     */
    public static function listOfTypes(): array
    {
        return array_map(
            function (EscortType $type): array {
                return [
                    $type->getRawValue() => [
                        'id' => $type->getRawValue(),
                        'slug' => static::valueToSlug($type->getRawValue()),
                        'name' => static::valueToName($type->getRawValue()),
                    ]
                ];
            },
            [
                EscortType::female(),
                EscortType::shemale(),
                EscortType::bodyRubs(),
            ]
        );
    }
}
