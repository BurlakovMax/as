<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\Search\SearchQuery;
use App\Escorts\Domain\EscortSponsor;
use App\Escorts\Domain\EscortSponsorAlreadyPurchased;
use App\Escorts\Domain\EscortSponsorReadStorage;

final class EscortSponsorQueryProcessor
{
    private EscortSponsorReadStorage $escortSponsorReadStorage;

    private int $maximumNumberPerCity;

    private string $price;

    public function __construct(
        EscortSponsorReadStorage $escortSponsorReadStorage,
        int $maximumNumberPerCity,
        string $price
    ) {
        $this->escortSponsorReadStorage = $escortSponsorReadStorage;
        $this->maximumNumberPerCity = $maximumNumberPerCity;
        $this->price = $price;
    }

    /**
     * @return EscortSponsorData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $list = $this->escortSponsorReadStorage->getBySearchQuery($query);

        return $this->mapListToData($list);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->escortSponsorReadStorage->countBySearchQuery($query);
    }

    /**
     * @param EscortSponsor[] $list
     * @return EscortSponsorData[]
     */
    private function mapListToData(array $list): array
    {
        return array_map(
            function (EscortSponsor $escortSticky): EscortSponsorData {
                return $this->mapToData($escortSticky);
            },
            $list
        );
    }

    private function mapToData(EscortSponsor $escortSponsor): EscortSponsorData
    {
        return new EscortSponsorData($escortSponsor);
    }

    /**
     * @param int[] $locationIds
     * @return CityAvailable[]
     *
     * @throws EscortSponsorAlreadyPurchased
     */
    public function canBuy(array $locationIds, ?int $escortId = null): array
    {
        $availables = [];
        foreach ($locationIds as $locationId) {
            $numberOfEscortSponsorsPerCity = $this->escortSponsorReadStorage->countActiveByLocationId((int) $locationId);

            if ($this->maximumNumberPerCity < $numberOfEscortSponsorsPerCity) {
                $availables[] = new CityAvailable($locationId, false);
                continue;
            }

            $availables[] = new CityAvailable($locationId, true);
        }

        if (null === $escortId) {
            return $availables;
        }

        if ($this->has($escortId)) {
            throw new EscortSponsorAlreadyPurchased();
        }

        return $availables;
    }

    private function has(int $escortId): bool
    {
        $escortSponsor= $this->escortSponsorReadStorage->getByEscortId($escortId);

        return $escortSponsor !== null;
    }

    public function getPrice(bool $isAgency): string
    {
        if ($isAgency) {
            return (string) ((int) $this->price * 2);
        }

        return $this->price;
    }

    /**
     * @param int[] $locationIds
     */
    public function calculateTotalCoast(bool $isAgency, array $locationIds): string
    {
        return (string) ((int) $this->getPrice($isAgency) * count($locationIds));
    }
}
