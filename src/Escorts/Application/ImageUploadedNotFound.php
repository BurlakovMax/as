<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use LazyLemurs\Exceptions\NotFoundException;

final class ImageUploadedNotFound extends NotFoundException
{

}