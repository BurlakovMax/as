<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortClickStatistic;
use App\Escorts\Domain\EscortClickStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class EscortClickStatisticCommandProcessor
{
    private EscortClickStorage $storage;

    private TransactionManager $transactionManager;

    public function __construct(EscortClickStorage $storage, TransactionManager $transactionManager)
    {
        $this->storage = $storage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function addClick(int $escortId, int $escortLocationId): void
    {
        $this->transactionManager->transactional(function () use ($escortId, $escortLocationId): void
        {
            $date = (new \DateTimeImmutable())->format('Y-m-d');
            $stat = $this->storage->getAndLockById($escortId, $escortLocationId, $date);

            if (null === $stat) {
                $this->storage->add(new EscortClickStatistic(
                    $escortId,
                    $escortLocationId,
                    $date
                ));

            } else {
                $stat->addClick();
            }
        });
    }
}
