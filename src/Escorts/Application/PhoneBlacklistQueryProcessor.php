<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\Search\SearchQuery;
use App\Escorts\Domain\PhoneBlacklist;
use App\Escorts\Domain\PhoneBlacklistStorage;

final class PhoneBlacklistQueryProcessor
{
    private PhoneBlacklistStorage $storage;

    public function __construct(PhoneBlacklistStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return PhoneBlacklistData[]
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        return $this->mapListToData($this->storage->getListBySearchQuery($query));
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->storage->getCountBySearchQuery($query);
    }

    /**
     * @param PhoneBlacklist[]
     * @return PhoneBlacklistData[]
     */
    private function mapListToData(array $phoneList): array
    {
        return array_map(fn($phone): PhoneBlacklistData => new PhoneBlacklistData($phone), $phoneList);
    }
}
