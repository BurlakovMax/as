<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use Ds\Map;
use App\Escorts\Domain\BodyType as Enum;

final class BodyType extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put(Enum::tiny(), 'Tiny');
        $map->put(Enum::slim(), 'Slim');
        $map->put(Enum::athletic(), 'Athletic');
        $map->put(Enum::average(), 'Average');
        $map->put(Enum::curvy(), 'Curvy');
        $map->put(Enum::bbw(), 'BBW');
        $map->put(Enum::none(), 'Not set');

        return $map;
    }

    public static function getRawListWithValue(): array
    {
        $enums = [
            Enum::tiny(),
            Enum::slim(),
            Enum::athletic(),
            Enum::average(),
            Enum::curvy(),
            Enum::bbw(),
            Enum::none(),
        ];

        $types = [];
        foreach ($enums as $enum) {
            if (Enum::none()->equals($enum)) {
                continue;
            }

            $types[] = [
                'value' => $enum->getRawValue(),
                'name' => static::getList()->get($enum),
            ];
        }

        return $types;
    }
}