<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\PhoneVerificationStorage;
use LazyLemurs\Structures\PhoneNumber;

final class PhoneVerificationQueryProcessor
{
    private PhoneVerificationStorage $phoneVerificationStorage;

    public function __construct(PhoneVerificationStorage $phoneVerificationStorage)
    {
        $this->phoneVerificationStorage = $phoneVerificationStorage;
    }

    public function isPhoneVerified(PhoneNumber $phoneNumber): bool
    {
        $phoneNumber = $this->phoneVerificationStorage->getByPhoneNumberAndVerified($phoneNumber);

        if (empty($phoneNumber)) {
            return false;
        }

        return true;
    }
}
