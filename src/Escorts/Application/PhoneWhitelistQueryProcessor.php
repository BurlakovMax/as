<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\Application\Search\SearchQuery;
use App\Escorts\Domain\PhoneWhitelist;
use App\Escorts\Domain\PhoneWhitelistStorage;

final class PhoneWhitelistQueryProcessor
{
    private PhoneWhitelistStorage $storage;

    public function __construct(PhoneWhitelistStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return PhoneWhitelistData[]
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        return $this->mapListToData($this->storage->getListBySearchQuery($query));
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->storage->getCountBySearchQuery($query);
    }

    /**
     * @param PhoneWhitelist[]
     * @return PhoneWhitelistData[]
     */
    private function mapListToData(array $phoneWhiteList): array
    {
        return array_map(fn($phone): PhoneWhitelistData => new PhoneWhitelistData($phone), $phoneWhiteList);
    }
}
