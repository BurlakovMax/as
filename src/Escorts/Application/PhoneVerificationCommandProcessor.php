<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\InvalidConfirmationCode;
use App\Escorts\Domain\PhoneVerificationCode;
use App\Escorts\Domain\PhoneVerificationService;
use LazyLemurs\Structures\PhoneNumber;
use LazyLemurs\TransactionManager\TransactionManager;

final class PhoneVerificationCommandProcessor
{
    private PhoneVerificationService $phoneVerificationService;

    private TransactionManager $transactionManager;

    public function __construct(
        PhoneVerificationService $phoneVerificationStorage,
        TransactionManager $transactionManager
    ) {
        $this->phoneVerificationService = $phoneVerificationStorage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function requestPhoneConfirmation(PhoneNumber $phone, ?int $escortId, ?int $accountId): void
    {
        $this->transactionManager->transactional(function () use ($phone, $escortId, $accountId): void {
            $this->phoneVerificationService->requestPhoneConfirmation($phone, $escortId, $accountId);
        });
    }

    /**
     * @throws \Throwable
     */
    public function confirmPhone(PhoneNumber $phone, string $code, ?int $escortId, ?int $accountId): void
    {
        $exception = null;
        try {
            $this->phoneVerificationService->isValid($phone, new PhoneVerificationCode($code), $accountId, $escortId);
        } catch (InvalidConfirmationCode $e) {
            $exception = $e;
        }

        $this->transactionManager->transactional(function () use ($exception, $accountId, $escortId, $phone): void {
            if (null !== $exception) {
                $this->phoneVerificationService->utilizeConfirmationAttempt($phone, $accountId, $escortId);
            } else {
                $this->phoneVerificationService->confirmPhone($phone, $accountId, $escortId);
            }
        });

        if (null !== $exception) {
            throw $exception;
        }
    }
}
