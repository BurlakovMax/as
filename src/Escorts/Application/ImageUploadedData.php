<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use Swagger\Annotations as SWG;

final class ImageUploadedData
{
    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private string $url;

    /**
     * @SWG\Property()
     */
    private string $id;

    public function __construct(string $name, string $url, string $id)
    {
        $this->name = $name;
        $this->url = $url;
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getId(): string
    {
        return $this->id;
    }
}