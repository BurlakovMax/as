<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\Status;

final class StatusData
{
    public static function requestedToDelete(): int
    {
        return Status::requestedToDelete()->getRawValue();
    }

    public static function processOfCreation(): int
    {
        return Status::processOfCreation()->getRawValue();
    }

    public static function expired(): int
    {
        return Status::expired()->getRawValue();
    }

    public static function active(): int
    {
        return Status::active()->getRawValue();
    }

    public static function invisible(): int
    {
        return Status::invisible()->getRawValue();
    }

    public static function inVerification(): int
    {
        return Status::inVerification()->getRawValue();
    }
}