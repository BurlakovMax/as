<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\EscortClickStatistic;

final class EscortClickStatisticData
{
    private int $escortId;

    private int $locationId;

    private string $date;

    private int $clickCount;

    public function __construct(EscortClickStatistic $statistic)
    {
        $this->escortId = $statistic->getEscortId();
        $this->locationId = $statistic->getLocationId();
        $this->date = $statistic->getDate();
        $this->clickCount = $statistic->getClickCount();
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getClickCount(): int
    {
        return $this->clickCount;
    }
}
