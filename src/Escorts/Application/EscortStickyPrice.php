<?php

declare(strict_types=1);

namespace App\Escorts\Application;

final class EscortStickyPrice
{
    private int $monthlyPrice;

    private int $weeklyPrice;

    private int $locationId;

    public function __construct(int $monthlyPrice, int $weeklyPrice, int $locationId)
    {
        $this->monthlyPrice = $monthlyPrice;
        $this->weeklyPrice = $weeklyPrice;
        $this->locationId = $locationId;
    }

    public function getMonthlyPrice(): int
    {
        return $this->monthlyPrice;
    }

    public function getWeeklyPrice(): int
    {
        return $this->weeklyPrice;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }
}