<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use App\Escorts\Domain\Ethnicity as Enum;
use Ds\Map;

final class Ethnicity extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put(Enum::notSet(), 'Not-set');
        $map->put(Enum::asian(), 'Asian');
        $map->put(Enum::black(), 'Black');
        $map->put(Enum::hispanic(), 'Hispanic');
        $map->put(Enum::white(), 'White');
        $map->put(Enum::eastIndian(), 'East-indian');
        $map->put(Enum::mixed(), 'Mixed');
        $map->put(Enum::middleEastern(), 'Middle-eastern');

        return $map;
    }

    public static function getRawListWithValue(): array
    {
        $enums = [
            Enum::notSet(),
            Enum::asian(),
            Enum::black(),
            Enum::hispanic(),
            Enum::white(),
            Enum::eastIndian(),
            Enum::mixed(),
            Enum::middleEastern(),
        ];

        $types = [];
        foreach ($enums as $enum) {
            if (Enum::notSet()->equals($enum)) {
                continue;
            }

            $types[] = [
                'value' => $enum->getRawValue(),
                'name' => static::getList()->get($enum),
            ];
        }

        return $types;
    }
}