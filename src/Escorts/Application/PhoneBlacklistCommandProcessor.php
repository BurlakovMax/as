<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Escorts\Domain\PhoneBlacklist;
use App\Escorts\Domain\PhoneBlacklistStorage;
use App\Escorts\Domain\PhoneNotInBlacklist;
use LazyLemurs\Structures\PhoneNumber;
use LazyLemurs\TransactionManager\TransactionManager;

final class PhoneBlacklistCommandProcessor
{
    private PhoneBlacklistStorage $storage;

    private TransactionManager $transactionManager;

    public function __construct(PhoneBlacklistStorage $storage, TransactionManager $transactionManager)
    {
        $this->storage = $storage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function add(PhoneNumber $phoneNumber, string $reason, int $createdBy): void
    {
        $this->transactionManager->transactional(function () use ($phoneNumber, $reason, $createdBy): void {
           $this->storage->add(new PhoneBlacklist($phoneNumber, $reason, $createdBy));
        });
    }

    /**
     * @throws \Throwable
     */
    public function remove(int $id): void
    {
        $this->transactionManager->transactional(function () use ($id): void
        {
            $phone = $this->storage->getById($id);

            if (null === $phone) {
                throw new PhoneNotInBlacklist();
            }

            $this->storage->remove($phone);
        });
    }
}
