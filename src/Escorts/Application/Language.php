<?php

declare(strict_types=1);

namespace App\Escorts\Application;

use App\Core\ApplicationSupport\ListOfEnumerable;
use App\Escorts\Domain\Language as Enum;
use Ds\Map;

final class Language extends ListOfEnumerable
{
    public static function getList(): Map
    {
        $map = new Map();

        $map->put(Enum::albanian(), 'Albanian');
        $map->put(Enum::arabic(), 'Arabic');
        $map->put(Enum::armenian(), 'Armenian');
        $map->put(Enum::belarusian(), 'Belarusian');
        $map->put(Enum::bosnian(), 'Bosnian');
        $map->put(Enum::celtic(), 'Celtic');
        $map->put(Enum::chineseSimplified(), 'ChineseSimplified');
        $map->put(Enum::chineseTraditional(), 'ChineseTraditional');
        $map->put(Enum::croatian(), 'Croatian');
        $map->put(Enum::czech(), 'Czech');
        $map->put(Enum::danish(), 'Danish');
        $map->put(Enum::dutch(), 'Dutch');
        $map->put(Enum::english(), 'English');
        $map->put(Enum::finnish(), 'Finnish');
        $map->put(Enum::french(), 'French');
        $map->put(Enum::german(), 'German');
        $map->put(Enum::greek(), 'Greek');
        $map->put(Enum::italian(), 'Italian');
        $map->put(Enum::japanese(), 'Japanese');
        $map->put(Enum::korean(), 'Korean');
        $map->put(Enum::lao(), 'Lao');
        $map->put(Enum::latvian(), 'Latvian');
        $map->put(Enum::lithuanian(), 'Lithuanian');
        $map->put(Enum::persian(), 'Persian');
        $map->put(Enum::polish(), 'Polish');
        $map->put(Enum::portuguese(), 'Portuguese');
        $map->put(Enum::norwegian(), 'Norwegian');
        $map->put(Enum::russian(), 'Russian');
        $map->put(Enum::serbian(), 'Serbian');
        $map->put(Enum::spanish(), 'Spanish');
        $map->put(Enum::swedish(), 'Swedish');
        $map->put(Enum::thai(), 'Thai');
        $map->put(Enum::turkish(), 'Turkish');
        $map->put(Enum::ukranian(), 'Ukranian');
        $map->put(Enum::vietnamese(), 'Vietnamese');

        return $map;
    }

    public static function getRawListWithValue(): array
    {
        return array_map(
            function (Enum $type): array {
                return [
                    'value' => $type->getRawValue(),
                    'name' => static::getList()->get($type),
                ];
            },
            [
                Enum::albanian(),
                Enum::arabic(),
                Enum::armenian(),
                Enum::belarusian(),
                Enum::bosnian(),
                Enum::celtic(),
                Enum::chineseSimplified(),
                Enum::chineseTraditional(),
                Enum::croatian(),
                Enum::czech(),
                Enum::danish(),
                Enum::dutch(),
                Enum::english(),
                Enum::finnish(),
                Enum::french(),
                Enum::german(),
                Enum::greek(),
                Enum::italian(),
                Enum::japanese(),
                Enum::korean(),
                Enum::lao(),
                Enum::latvian(),
                Enum::lithuanian(),
                Enum::persian(),
                Enum::polish(),
                Enum::portuguese(),
                Enum::norwegian(),
                Enum::russian(),
                Enum::serbian(),
                Enum::spanish(),
                Enum::swedish(),
                Enum::thai(),
                Enum::turkish(),
                Enum::ukranian(),
                Enum::vietnamese(),
            ]
        );
    }
}