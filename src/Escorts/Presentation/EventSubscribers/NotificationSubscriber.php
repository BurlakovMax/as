<?php

declare(strict_types=1);

namespace App\Escorts\Presentation\EventSubscribers;

use App\EmailGate\Domain\EmailData;
use App\Escorts\Domain\EmailProvider;
use App\Escorts\Domain\EmailToEscortSent;
use LazyLemurs\DomainEvents\DomainEvent;
use LazyLemurs\DomainEvents\DomainEventSubscriber;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;

final class NotificationSubscriber implements DomainEventSubscriber
{
    private EmailProvider $emailProvider;

    private string $fromEmail;

    public function __construct(EmailProvider $emailProvider, string $fromEmail)
    {
        $this->emailProvider = $emailProvider;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return string[] Names of event's classes.
     */
    public function getListenedEventClasses(): array
    {
        return [
            EmailToEscortSent::class,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function handleEvent(DomainEvent $event): void
    {
        switch (true) {
            case $event instanceof EmailToEscortSent:
                $emailData = new EmailData(
                    Uuid::uuid4(),
                    new Email($this->fromEmail),
                    $event->getEmail(),
                    $event->getMessage(),
                    'You have a message from a customer',
                    $event->getReplyTo()
                );

                $this->emailProvider->sendSync($emailData);

                break;
            default:
                throw new \RuntimeException(sprintf('Event not supported. %s', get_class($event)));
        }
    }
}
