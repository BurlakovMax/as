<?php

declare(strict_types=1);

namespace App\Forums\Infrastructure\Persistence;

use App\Forums\Domain\Forum;
use App\Forums\Domain\ForumReadStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\QueryException;

final class ForumDoctrineRepository extends EntityRepository implements ForumReadStorage
{
    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function getByIdAndLocationId(int $id, int $locationId): ?Forum
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                        ->andWhere(Criteria::expr()->eq('id', $id))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return Forum[]
     * @throws QueryException
     */
    public function getByLocationId(int $locationId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('locationId', $locationId))
                )
                ->orderBy('t.countTopics', Criteria::DESC)
                ->getQuery()
                ->getResult()
            ;
    }
}
