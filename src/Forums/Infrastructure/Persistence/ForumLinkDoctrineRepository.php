<?php

declare(strict_types=1);

namespace App\Forums\Infrastructure\Persistence;

use App\Forums\Domain\ForumLink;
use App\Forums\Domain\ForumLinkReadStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

final class ForumLinkDoctrineRepository extends EntityRepository implements ForumLinkReadStorage
{
    public function get(int $forumId): ?ForumLink
    {
        return $this->find($forumId);
    }

    public function getByUrl(string $url): ?ForumLink
    {
        return $this->findOneBy(['url' => $url]);
    }

    /**
     * @param int[] $ids
     * @return ForumLink[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByIds(array $ids): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->in('id', $ids))
                )
                ->getQuery()
                ->getResult()
            ;
    }
}