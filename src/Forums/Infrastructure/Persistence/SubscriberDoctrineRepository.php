<?php

declare(strict_types=1);

namespace App\Forums\Infrastructure\Persistence;

use App\Forums\Domain\Subscriber;
use App\Forums\Domain\SubscriberReadStorage;
use App\Forums\Domain\SubscriberWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class SubscriberDoctrineRepository extends EntityRepository implements SubscriberReadStorage, SubscriberWriteStorage
{
    public function add(Subscriber $subscriber): void
    {
        $this->getEntityManager()->persist($subscriber);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws ORMException
     */
    public function remove(Subscriber $subscriber): void
    {
        $this->getEntityManager()->remove($subscriber);
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function getByTopicIdAndAccountId(int $topicId, int $accountId): ?Subscriber
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('topicId', $topicId))
                        ->andWhere(Criteria::expr()->eq('accountId', $accountId))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }

    /**
     * @return Subscriber[]
     * @throws QueryException
     */
    public function getByTopicId(int $topicId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('topicId', $topicId))
                )
                ->getQuery()
                ->getResult()
            ;
    }
}
