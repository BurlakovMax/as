<?php

declare(strict_types=1);

namespace App\Forums\Infrastructure\Adapter;

use App\Forums\Domain\Account;
use App\Forums\Domain\AccountStorage;
use App\Security\Application\AccountQueryProcessor;
use App\Security\Domain\AccountNotFound;
use LazyLemurs\Structures\Email;

final class AccountAdapter implements AccountStorage
{
    private AccountQueryProcessor $userQueryProcessor;

    public function __construct(AccountQueryProcessor $userQueryProcessor)
    {
        $this->userQueryProcessor = $userQueryProcessor;
    }

    /**
     * @throws AccountNotFound
     */
    public function get(int $accountId): Account
    {
        $user = $this->userQueryProcessor->getAccount($accountId);

        return new Account(
            $user->getId(),
            $user->getName(),
            new Email($user->getUsername())
        );
    }
}
