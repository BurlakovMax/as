<?php

declare(strict_types=1);

namespace App\Forums\Infrastructure\EventSubscribers;

use App\EmailGate\Domain\EmailData;
use App\Forums\Application\SubscriberNotFound;
use App\Forums\Application\SubscriberQueryProcessor;
use App\Forums\Domain\AccountStorage;
use App\Forums\Domain\EmailProvider;
use App\Forums\Domain\PostCreated;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class PostCreatedSubscriber implements MessageHandlerInterface
{
    private SubscriberQueryProcessor $subscriberQueryProcessor;

    private AccountStorage $accountStorage;

    private EmailProvider $emailProvider;

    private string $fromEmail;

    public function __construct(
        SubscriberQueryProcessor $subscriberQueryProcessor,
        AccountStorage $accountStorage,
        EmailProvider $emailProvider,
        string $fromEmail
    ) {
        $this->subscriberQueryProcessor = $subscriberQueryProcessor;
        $this->accountStorage = $accountStorage;
        $this->emailProvider = $emailProvider;
        $this->fromEmail = $fromEmail;
    }

    public function __invoke(PostCreated $event): void
    {
        $subscribers = $this->subscriberQueryProcessor->getByTopicId($event->getTopicId());

        if (empty($subscribers)) {
            return;
        }

        foreach ($subscribers as $subscriber) {

            if ($event->getAccountId() === $subscriber->getAccountId()) {
                continue;
            }

            $this->emailProvider->sendSync(
                new EmailData(
                    Uuid::uuid4(),
                    new Email($this->fromEmail),
                    $this->accountStorage->get($subscriber->getAccountId())->getEmail(),
                    'You are received a new post',
                    'New Forum Message on AdultSearch.com'
                )
            );
        }
    }
}
