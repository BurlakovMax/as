<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\ForumLink;
use App\Forums\Domain\ForumLinkReadStorage;

final class ForumLinkQueryProcessor
{
    private ForumLinkReadStorage $forumLinkReadStorage;

    public function __construct(ForumLinkReadStorage $forumLinkReadStorage)
    {
        $this->forumLinkReadStorage = $forumLinkReadStorage;
    }

    /**
     * @throws ForumLinkNotFound
     */
    public function get(int $forumId): ForumLinkData
    {
        $forumLink = $this->forumLinkReadStorage->get($forumId);

        if (null === $forumLink) {
            throw new ForumLinkNotFound();
        }

        return $this->mapToData($forumLink);
    }

    /**
     * @throws ForumLinkNotFound
     */
    public function getByUrl(string $url): ForumLinkData
    {
        $forumLink = $this->forumLinkReadStorage->getByUrl($url);

        if (null === $forumLink) {
            throw new ForumLinkNotFound();
        }

        return $this->mapToData($forumLink);
    }

    /**
     * @param int[] $ids
     * @return ForumLinkData[]
     */
    public function getByIds(array $ids): array
    {
        return array_map([ $this, 'mapToData' ], $this->forumLinkReadStorage->getByIds($ids));
    }

    private function mapToData(ForumLink $forumLink): ForumLinkData
    {
        return new ForumLinkData($forumLink);
    }
}