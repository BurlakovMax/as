<?php

declare(strict_types=1);

namespace App\Forums\Application;

use LazyLemurs\Commander\Property;

final class UpdateTopicCommand
{
    private int $id;

    /**
     * @Property(type="?string")
     */
    private ?string $title;

    /**
     * @Property(type="?int")
     */
    private ?int $placeId;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getPlaceId(): ?int
    {
        return $this->placeId;
    }
}