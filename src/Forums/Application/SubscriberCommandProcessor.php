<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\SubscriberService;
use LazyLemurs\TransactionManager\TransactionManager;
use Throwable;

final class SubscriberCommandProcessor
{
    private TransactionManager $transactionManager;

    private SubscriberService $subscriberService;

    public function __construct(
        TransactionManager $transactionManager,
        SubscriberService $subscriberService
    ) {
        $this->transactionManager = $transactionManager;
        $this->subscriberService = $subscriberService;
    }

    /**
     * @throws Throwable
     */
    public function subscribe(int $topicId, int $accountId): int
    {
        return $this->transactionManager->transactional(function () use ($topicId, $accountId): int {
            return $this->subscriberService->subscribe($topicId, $accountId);
        });
    }

    /**
     * @throws Throwable
     */
    public function unsubscribe(int $topicId, int $accountId): void
    {
        $this->transactionManager->transactional(function () use ($topicId, $accountId): void {
            $this->subscriberService->unsubscribe($topicId, $accountId);
        });
    }
}
