<?php

declare(strict_types=1);

namespace App\Forums\Application;

use LazyLemurs\Commander\Property;

abstract class PostCommand
{
    private int $accountId;

    /**
     * @Property(type="string")
     */
    private string $text;

    public function __construct(int $accountId)
    {
        $this->accountId = $accountId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
