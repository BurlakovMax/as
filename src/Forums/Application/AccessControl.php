<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Core\ApplicationSupport\AccessControl\AccessForbidden;

final class AccessControl
{
    private ForumQueryProcessor $forumQueryProcessor;

    private TopicQueryProcessor $topicQueryProcessor;

    private PostQueryProcessor $postQueryProcessor;

    public function __construct(
        ForumQueryProcessor $forumQueryProcessor,
        TopicQueryProcessor $topicQueryProcessor,
        PostQueryProcessor $postQueryProcessor
    ) {
        $this->forumQueryProcessor = $forumQueryProcessor;
        $this->topicQueryProcessor = $topicQueryProcessor;
        $this->postQueryProcessor = $postQueryProcessor;
    }

    /**
     * @throws ForumNotFound
     */
    public function checkAccessToForum(int $forumId, int $locationId): void
    {
        try {
            $this->forumQueryProcessor->getByIdAndLocationId($forumId, $locationId);
        } catch (ForumNotFound $e) {
            throw $e;
        }
    }

    /**
     * @throws AccessForbidden
     * @throws TopicNotFound
     */
    public function checkAccessToTopic(int $topicId, int $accountId): void
    {
        $topic = $this->topicQueryProcessor->get($topicId);

        if ($topic->getAccountId() !== $accountId) {
            throw new AccessForbidden();
        }
    }

    /**
     * @throws AccessForbidden
     * @throws PostNotFound
     */
    public function checkAccessToPost(int $postId, int $accountId): void
    {
        $post = $this->postQueryProcessor->get($postId);

        if ($post->getAccountId() !== $accountId) {
            throw new AccessForbidden();
        }
    }
}
