<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\PostService;
use LazyLemurs\TransactionManager\TransactionManager;
use Throwable;

final class PostCommandProcessor
{
    private TransactionManager $transactionManager;

    private PostService $postService;

    public function __construct(
        TransactionManager $transactionManager,
        PostService $postService
    ) {
        $this->transactionManager = $transactionManager;
        $this->postService = $postService;
    }

    /**
     * @throws Throwable
     */
    public function create(CreatePostCommand $command): int
    {
        return $this->transactionManager->transactional(function () use ($command): int {
            return $this->postService->create($command);
        });
    }

    /**
     * @throws Throwable
     */
    public function update(UpdatePostCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
            $this->postService->update($command);
        });
    }

    /**
     * @throws Throwable
     */
    public function delete(int $id): void
    {
        $this->transactionManager->transactional(function () use ($id): void {
            $this->postService->delete($id);
        });
    }
}
