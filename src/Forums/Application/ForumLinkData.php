<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\ForumLink;

final class ForumLinkData
{
    private ForumLink $forumLink;

    public function __construct(ForumLink $forumLink)
    {
        $this->forumLink = $forumLink;
    }

    public function getId(): int
    {
        return $this->getForumLink()->getId();
    }

    public function getName(): string
    {
        return $this->getForumLink()->getName();
    }

    public function getUrl(): string
    {
        return $this->getForumLink()->getUrl();
    }

    private function getForumLink(): ForumLink
    {
        return $this->forumLink;
    }
}