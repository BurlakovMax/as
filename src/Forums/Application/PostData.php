<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\Account;
use App\Forums\Domain\Post;
use DateTimeImmutable;
use LazyLemurs\Structures\Email;
use Swagger\Annotations as SWG;

final class PostData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $topicId;

    /**
     * @SWG\Property()
     */
    private ?int $accountId;

    /**
     * @SWG\Property()
     */
    private string $accountName;

    /**
     * @SWG\Property()
     */
    private Email $accountEmail;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private ?int $forumId;

    /**
     * @SWG\Property()
     */
    private int $locationId;

    /**
     * @SWG\Property()
     */
    private ?string $userNameWhosEdited;

    /**
     * @SWG\Property()
     */
    private DateTimeImmutable $lastEditedAt;

    /**
     * @SWG\Property()
     */
    private int $countOfEdited;

    /**
     * @SWG\Property()
     */
    private ?string $reasonOfEdit;

    /**
     * @SWG\Property()
     */
    private string $text;

    public function __construct(Post $post, Account $account)
    {
        $this->id = $post->getId();
        $this->topicId = $post->getTopicId();
        $this->accountId = $post->getAccountId();
        $this->accountName = $account->getName();
        $this->accountEmail = $account->getEmail();
        $this->createdAt = $post->getCreatedAt();
        $this->forumId = $post->getForumId();
        $this->locationId = $post->getLocationId();
        $this->userNameWhosEdited = $post->getUserNameWhosEdited();
        $this->lastEditedAt = $post->getLastEditedAt();
        $this->countOfEdited = $post->getCountOfEdited();
        $this->reasonOfEdit = $post->getReasonOfEdit();
        $this->text = $post->getText();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTopicId(): int
    {
        return $this->topicId;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function getAccountName(): string
    {
        return $this->accountName;
    }

    public function getAccountEmail(): Email
    {
        return $this->accountEmail;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getForumId(): ?int
    {
        return $this->forumId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getUserNameWhosEdited(): ?string
    {
        return $this->userNameWhosEdited;
    }

    public function getLastEditedAt(): DateTimeImmutable
    {
        return $this->lastEditedAt;
    }

    public function getCountOfEdited(): int
    {
        return $this->countOfEdited;
    }

    public function getReasonOfEdit(): ?string
    {
        return $this->reasonOfEdit;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
