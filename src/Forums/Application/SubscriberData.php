<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\Subscriber;
use Swagger\Annotations as SWG;

final class SubscriberData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $topicId;

    /**
     * @SWG\Property()
     */
    private int $accountId;

    public function __construct(Subscriber $subscriber)
    {
        $this->id = $subscriber->getId();
        $this->topicId = $subscriber->getTopicId();
        $this->accountId= $subscriber->getAccountId();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTopicId(): int
    {
        return $this->topicId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }
}
