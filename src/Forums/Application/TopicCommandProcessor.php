<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\Post;
use App\Forums\Domain\PostWriteStorage;
use App\Forums\Domain\Topic;
use App\Forums\Domain\TopicWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;
use Throwable;

final class TopicCommandProcessor
{
    private TransactionManager $transactionManager;

    private TopicWriteStorage $topicWriteStorage;

    private PostWriteStorage $postWriteStorage;

    public function __construct(
        TransactionManager $transactionManager,
        TopicWriteStorage $topicWriteStorage,
        PostWriteStorage $postWriteStorage
    ) {
        $this->transactionManager = $transactionManager;
        $this->topicWriteStorage = $topicWriteStorage;
        $this->postWriteStorage = $postWriteStorage;
    }

    /**
     * @throws Throwable
     */
    public function create(CreateTopicCommand $command): int
    {
        $topic = new Topic(
            $command->getTitle(),
            $command->getAccountId(),
            $command->getForumId(),
            $command->getLocationId(),
            $command->getPlaceId()
        );

        $this->transactionManager->transactional(function () use ($topic, $command) {
            $this->topicWriteStorage->add($topic);

            $post = new Post(
                $topic->getId(),
                $topic->getAccountId(),
                $topic->getLocationId(),
                $topic->getForumId(),
                $command->getText()
            );
            $this->postWriteStorage->add($post);
            $topic
                ->updateLastPost(
                    $post->getId(),
                    $post->getAccountId(),
                    $post->getCreatedAt()
                )
            ;
        });

        return $topic->getId();
    }

    /**
     * @throws Throwable
     */
    public function update(UpdateTopicCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command) {
            $this
                ->get(
                    $command->getId()
                )
                ->update(
                    $command->getPlaceId(),
                    $command->getTitle()
                )
            ;
        });
    }

    /**
     * @param PostData[] $posts
     *
     * @throws Throwable
     */
    public function delete(int $id, array $posts): void
    {
        $this->transactionManager->transactional(function () use ($id, $posts): void {

            $topic = $this->get($id);

            if (!empty($posts)) {
                foreach ($posts as $post) {
                    $post = $this->postWriteStorage->getAndLock($post->getId());
                    $this->postWriteStorage->delete($post);
                }
            }

            $this->topicWriteStorage->delete($topic);
        });
    }

    /**
     * @throws TopicNotFound
     */
    private function get(int $id): Topic
    {
        $topic = $this->topicWriteStorage->getAndLock($id);

        if (null === $topic) {
            throw new TopicNotFound();
        }

        return $topic;
    }
}
