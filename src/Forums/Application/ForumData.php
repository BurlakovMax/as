<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Forums\Domain\Forum;
use Swagger\Annotations as SWG;

final class ForumData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $locationId;

    /**
     * @SWG\Property()
     */
    private int $stateId;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private ?string $description;

    /**
     * @SWG\Property()
     */
    private int $countPosts;

    /**
     * @SWG\Property()
     */
    private int $countTopics;

    /**
     * @SWG\Property()
     */
    private int $lastPostId;

    /**
     * @SWG\Property()
     */
    private int $lastPosterId;

    /**
     * @SWG\Property()
     */
    private int $lastPostedAt;

    /**
     * @SWG\Property()
     */
    private int $parent;

    /**
     * @SWG\Property()
     */
    private int $order;

    public function __construct(Forum $forum)
    {
        $this->id = $forum->getId();
        $this->locationId = $forum->getLocationId();
        $this->stateId = $forum->getStateId();
        $this->name = $forum->getName();
        $this->description = $forum->getDescription();
        $this->countPosts = $forum->getCountPosts();
        $this->countTopics = $forum->getCountTopics();
        $this->lastPostId = $forum->getLastPostId();
        $this->lastPosterId = $forum->getLastPosterId();
        $this->lastPostedAt = $forum->getLastPostedAt();
        $this->parent = $forum->getParent();
        $this->order = $forum->getOrder();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCountPosts(): int
    {
        return $this->countPosts;
    }

    public function getCountTopics(): int
    {
        return $this->countTopics;
    }

    public function getLastPostId(): int
    {
        return $this->lastPostId;
    }

    public function getLastPosterId(): int
    {
        return $this->lastPosterId;
    }

    public function getLastPostedAt(): int
    {
        return $this->lastPostedAt;
    }

    public function getParent(): int
    {
        return $this->parent;
    }

    public function getOrder(): int
    {
        return $this->order;
    }
}
