<?php

declare(strict_types=1);

namespace App\Forums\Application;

use App\Core\Application\Search\SearchQuery;
use App\Forums\Domain\Account;
use App\Forums\Domain\AccountStorage;
use App\Forums\Domain\Topic;
use App\Forums\Domain\TopicReadStorage;

final class TopicQueryProcessor
{
    private TopicReadStorage $topicReadStorage;

    private AccountStorage $accountStorage;

    public function __construct(TopicReadStorage $topicReadStorage, AccountStorage $accountStorage)
    {
        $this->topicReadStorage = $topicReadStorage;
        $this->accountStorage = $accountStorage;
    }

    /**
     * @throws TopicNotFound
     */
    public function get(int $id): TopicData
    {
        $topic = $this->topicReadStorage->get($id);

        if (null === $topic) {
            throw new TopicNotFound();
        }

        $account = $this->accountStorage->get($topic->getAccountId());

        return $this->mapToData($topic, $account);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->topicReadStorage->countBySearchQuery($query);
    }

    /**
     * @return TopicData[]
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        $rawTopics = $this->topicReadStorage->getListBySearchQuery($query);

        $topics = [];
        foreach ($rawTopics as $topic) {
            $topics[] = $this->mapToData($topic, $this->accountStorage->get($topic->getAccountId()));
        }

        return $topics;
    }

    private function mapToData(Topic $topic, Account $account): TopicData
    {
        return new TopicData($topic, $account);
    }
}
