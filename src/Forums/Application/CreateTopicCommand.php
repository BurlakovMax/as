<?php

declare(strict_types=1);

namespace App\Forums\Application;

use LazyLemurs\Commander\Property;

final class CreateTopicCommand
{
    /**
     * @Property(type="string")
     */
    private string $title;

    /**
     * @Property(type="string")
     */
    private string $text;

    private int $accountId;

    /**
     * @Property(type="int")
     */
    private int $forumId;

    /**
     * @Property(type="int")
     */
    private int $locationId;

    /**
     * @Property(type="?int")
     */
    private ?int $placeId;

    public function __construct(int $accountId)
    {
        $this->accountId = $accountId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getForumId(): int
    {
        return $this->forumId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getPlaceId(): ?int
    {
        return $this->placeId;
    }
}
