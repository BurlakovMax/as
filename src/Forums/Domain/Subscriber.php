<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Forums\Infrastructure\Persistence\SubscriberDoctrineRepository")
 * @ORM\Table(name="forum_watch")
 */
class Subscriber
{
    /**
     * @ORM\Column(name="watch_id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private int $topicId;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private int $accountId;

    public function __construct(int $topicId, int $accountId)
    {
        $this->id = null;
        $this->topicId = $topicId;
        $this->accountId = $accountId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTopicId(): int
    {
        return $this->topicId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }
}
