<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\ORMException;

interface TopicWriteStorage
{
    /**
     * @throws ORMException
     */
    public function add(Topic $topic): void;

    public function getAndLock(int $id): ?Topic;

    public function delete(Topic $topic): void;
}
