<?php

declare(strict_types=1);

namespace App\Forums\Domain;

interface AccountStorage
{
    public function get(int $accountId): Account;
}
