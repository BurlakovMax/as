<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use App\Core\Application\Search\SearchQuery;

interface TopicReadStorage
{
    public function get(int $id): ?Topic;

    public function countBySearchQuery(SearchQuery $query): int;

    /**
     * @return Topic[]
     */
    public function getListBySearchQuery(SearchQuery $query): array;
}
