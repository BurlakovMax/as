<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\ORMException;

interface SubscriberWriteStorage
{
    /**
     * @throws ORMException
     */
    public function add(Subscriber $subscriber): void;

    /**
     * @throws ORMException
     */
    public function remove(Subscriber $subscriber): void;
}
