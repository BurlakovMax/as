<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use App\Core\Application\Search\SearchQuery;

interface PostReadStorage
{
    public function get(int $id): ?Post;

    /**
     * @return Post[]
     */
    public function getListBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;
}
