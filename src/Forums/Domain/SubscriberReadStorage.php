<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\QueryException;

interface SubscriberReadStorage
{
    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function getByTopicIdAndAccountId(int $topicId, int $accountId): ?Subscriber;

    /**
     * @return Subscriber[]
     */
    public function getByTopicId(int $topicId): array;
}
