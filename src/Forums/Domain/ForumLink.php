<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Forums\Infrastructure\Persistence\ForumLinkDoctrineRepository")
 * @ORM\Table(name="forum_name")
 */
class ForumLink
{
    /**
     * @ORM\Column(name="forum_id", type="integer", length=6)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", name="forum_name", length=150)
     */
    private string $name;


    /**
     * @ORM\Column(type="string", name="forum_url", length=100)
     */
    private string $url;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}