<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Forums\Infrastructure\Persistence\PostDoctrineRepository")
 * @ORM\Table(name="forum_post")
 */
class Post
{
    /**
     * @ORM\Column(name="post_id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private int $topicId;

    /**
     * @ORM\Column(type="integer", length=8, nullable=true)
     */
    private ?int $accountId;

    /**
     * @ORM\Column(type="timestamp", name="posted", nullable=true)
     */
    private ?DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    private ?int $forumId;

    /**
     * @ORM\Column(type="integer", name="loc_id", length=8)
     */
    private int $locationId;

    /**
     * @ORM\Column(type="string", name="post_edited", length=255, nullable=true)
     */
    private ?string $userNameWhosEdited;

    /**
     * @ORM\Column(type="datetime_immutable", name="post_lastedit_date")
     */
    private DateTimeImmutable $lastEditedAt;

    /**
     * @ORM\Column(type="integer", name="post_edit_count", length=3)
     */
    private int $countOfEdited;

    /**
     * @ORM\Column(type="string", name="post_edit_reason", length=255, nullable=true)
     */
    private ?string $reasonOfEdit;

    /**
     * @ORM\Column(type="text", name="post_text")
     */
    private string $text;

    public function __construct(
        int $topicId,
        int $accountId,
        int $locationId,
        int $forumId,
        string $text
    ) {
        $this->id = null;
        $this->userNameWhosEdited = null;
        $this->reasonOfEdit = null;
        $this->topicId = $topicId;
        $this->accountId = $accountId;
        $this->locationId = $locationId;
        $this->forumId = $forumId;
        $this->createdAt = new DateTimeImmutable();
        $this->lastEditedAt = new DateTimeImmutable();
        $this->countOfEdited = 0;
        $this->text = $text;
    }

    public function update(string $userNameWhosEdited, string $reasonOfEdit, string $text): void
    {
        $this->userNameWhosEdited = $userNameWhosEdited;
        $this->lastEditedAt = new DateTimeImmutable();
        ++$this->countOfEdited;
        $this->reasonOfEdit = $reasonOfEdit;
        $this->text = $text;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTopicId(): int
    {
        return $this->topicId;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getForumId(): ?int
    {
        return $this->forumId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getUserNameWhosEdited(): ?string
    {
        return $this->userNameWhosEdited;
    }

    public function getLastEditedAt(): DateTimeImmutable
    {
        return $this->lastEditedAt;
    }

    public function getCountOfEdited(): int
    {
        return $this->countOfEdited;
    }

    public function getReasonOfEdit(): ?string
    {
        return $this->reasonOfEdit;
    }

    public function getText(): string
    {
        return $this->text;
    }
}

