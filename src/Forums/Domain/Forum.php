<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Forums\Infrastructure\Persistence\ForumDoctrineRepository")
 * @ORM\Table(name="forum_forum")
 */
class Forum
{
    /**
     * @ORM\Column(name="forum_id", type="integer", length=5)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", name="loc_id", length=8)
     */
    private int $locationId;

    /**
     * @ORM\Column(type="integer", name="loc_parent", length=8)
     */
    private int $stateId;

    /**
     * @ORM\Column(type="string", name="forum_name", length=150)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", name="forum_desc", nullable=true)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="integer", name="forum_posts", length=8)
     */
    private int $countPosts;

    /**
     * @ORM\Column(type="integer", name="forum_topics", length=8)
     */
    private int $countTopics;

    /**
     * @ORM\Column(type="integer", name="forum_last_post_id", length=8)
     */
    private int $lastPostId;

    /**
     * @ORM\Column(type="integer", name="forum_last_poster_id", length=8)
     */
    private int $lastPosterId;

    /**
     * @ORM\Column(type="integer", name="forum_last_post_time", length=10)
     */
    private int $lastPostedAt;

    /**
     * @ORM\Column(type="integer", name="forum_parent", length=3)
     */
    private int $parent;

    /**
     * @ORM\Column(type="integer", name="forum_order", length=4)
     */
    private int $order;

    public function getId(): int
    {
        return $this->id;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCountPosts(): int
    {
        return $this->countPosts;
    }

    public function getCountTopics(): int
    {
        return $this->countTopics;
    }

    public function getLastPostId(): int
    {
        return $this->lastPostId;
    }

    public function getLastPosterId(): int
    {
        return $this->lastPosterId;
    }

    public function getLastPostedAt(): int
    {
        return $this->lastPostedAt;
    }

    public function getParent(): int
    {
        return $this->parent;
    }

    public function getOrder(): int
    {
        return $this->order;
    }
}
