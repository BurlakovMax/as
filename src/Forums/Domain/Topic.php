<?php

declare(strict_types=1);

namespace App\Forums\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Forums\Infrastructure\Persistence\TopicDoctrineRepository")
 * @ORM\Table(name="forum_topic")
 */
class Topic
{
    /**
     * @ORM\Column(name="topic_id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", name="topic_title", length=100, nullable=true)
     */
    private ?string $title;

    /**
     * @ORM\Column(type="integer", name="item_id", length=11, nullable=true)
     */
    private ?int $placeId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="datetime_immutable", name="topic_time")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="integer", name="topic_replies", length=5)
     */
    private int $countReplies;

    /**
     * @ORM\Column(type="integer", name="topic_last_post_id", length=10)
     */
    private int $lastPostId;

    /**
     * @ORM\Column(type="integer", name="topic_last_post_nick", length=8, nullable=true)
     */
    private ?int $lastPostAccountId;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    private ?int $forumId;

    /**
     * @ORM\Column(type="integer", name="loc_id", length=9)
     */
    private int $locationId;

    /**
     * @ORM\Column(type="timestamp", name="topic_last_post_time", nullable=true)
     */
    private ?DateTimeImmutable $lastPostCreatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $important;

    public function __construct(string $title, int $accountId, int $forumId, int $locationId, ?int $placeId)
    {
        $this->id = null;
        $this->title = $title;
        $this->placeId = $placeId;
        $this->accountId = $accountId;
        $this->forumId = $forumId;
        $this->locationId = $locationId;
        $this->createdAt = new DateTimeImmutable();
        $this->countReplies = 0;
        $this->lastPostId = 0;
        $this->lastPostAccountId = null;
        $this->lastPostCreatedAt = null;
        $this->important = null;
    }

    public function update(?int $placeId, ?string $title): void
    {
        $this->placeId = $placeId;

        if ($title !== null) {
            $this->title = $title;
        }
    }

    public function updateLastPost(int $postId, int $accountId, DateTimeImmutable $createdAt): void
    {
        if (0 !== $this->lastPostId && null !== $this->lastPostAccountId && null !== $this->lastPostCreatedAt) {
            ++$this->countReplies;
        }

        $this->lastPostId = $postId;
        $this->lastPostAccountId = $accountId;
        $this->lastPostCreatedAt = $createdAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getPlaceId(): ?int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCountReplies(): int
    {
        return $this->countReplies;
    }

    public function getLastPostId(): int
    {
        return $this->lastPostId;
    }

    public function getLastPostAccountId(): ?int
    {
        return $this->lastPostAccountId;
    }

    public function getForumId(): ?int
    {
        return $this->forumId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getLastPostCreatedAt(): ?DateTimeImmutable
    {
        return $this->lastPostCreatedAt;
    }

    public function getImportant(): ?bool
    {
        return $this->important;
    }
}
