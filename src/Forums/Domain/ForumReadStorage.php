<?php

declare(strict_types=1);

namespace App\Forums\Domain;

interface ForumReadStorage
{
    public function getByIdAndLocationId(int $id, int $locationId): ?Forum;

    /**
     * @return Forum[]
     */
    public function getByLocationId(int $locationId): array;
}
