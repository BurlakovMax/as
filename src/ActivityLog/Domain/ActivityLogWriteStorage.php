<?php

declare(strict_types=1);

namespace App\ActivityLog\Domain;

interface ActivityLogWriteStorage
{
    public function add(ActivityLog $activityLog): void;
}