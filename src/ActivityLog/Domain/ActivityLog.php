<?php

declare(strict_types=1);

namespace App\ActivityLog\Domain;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\ActivityLog\Infrastructure\Persistence\ActivityLogDoctrineRepository")
 * @ORM\Table(name="audit")
 */
class ActivityLog
{
    /**
     * @ORM\Column(type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="created", type="date_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="who", type="string", length=30)
     */
    private string $initiator;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private string $type;

    /**
     * @ORM\Column(name="p1", type="integer", length=11, nullable=true)
     */
    private ?int $item;

    /**
     * @ORM\Column(name="subtype", type="string", length=30, nullable=true)
     */
    private ?string $action;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $message;

    public function __construct(int $initiator, string $type, int $item, string $action, string $message)
    {
        $this->id = 0;
        $this->createdAt = new \DateTimeImmutable();
        $this->initiator = (string) $initiator;
        $this->type = $type;
        $this->item = $item;
        $this->action = $action;
        $this->message = $message;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getInitiator(): int
    {
        return (int) $this->initiator;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getItem(): ?int
    {
        return $this->item;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }
}