<?php

declare(strict_types=1);

namespace App\ActivityLog\Domain;

use App\Core\Application\Search\SearchQuery;

interface ActivityLogReadStorage
{
    /**
     * @return ActivityLog[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;
}