<?php

declare(strict_types=1);

namespace App\ActivityLog\Application;

use App\ActivityLog\Domain\ActivityLog;

final class ActivityLogData
{
    private ActivityLog $activityLog;

    public function __construct(ActivityLog $activityLog)
    {
        $this->activityLog = $activityLog;
    }

    public function getId(): int
    {
        return $this->getActivityLog()->getId();
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->getActivityLog()->getCreatedAt();
    }

    public function getInitiator(): int
    {
        return $this->getActivityLog()->getInitiator();
    }

    public function getType(): string
    {
        return $this->getActivityLog()->getType();
    }

    public function getItem(): ?int
    {
        return $this->getActivityLog()->getItem();
    }

    public function getAction(): ?string
    {
        return $this->getActivityLog()->getAction();
    }

    public function getMessage(): ?string
    {
        return $this->getActivityLog()->getMessage();
    }

    private function getActivityLog(): ActivityLog
    {
        return $this->activityLog;
    }
}