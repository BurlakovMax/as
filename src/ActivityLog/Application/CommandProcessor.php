<?php

declare(strict_types=1);

namespace App\ActivityLog\Application;

use App\ActivityLog\Domain\ActivityLog;
use App\ActivityLog\Domain\ActivityLogWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class CommandProcessor
{
    private TransactionManager $transactionManager;

    private ActivityLogWriteStorage $activityLogWriteStorage;

    public function __construct(TransactionManager $transactionManager, ActivityLogWriteStorage $activityLogWriteStorage)
    {
        $this->transactionManager = $transactionManager;
        $this->activityLogWriteStorage = $activityLogWriteStorage;
    }

    public function create(CreateActivityLogCommand $command): void
    {
        $activityLog = new ActivityLog(
            $command->getInitiator(),
            $command->getType(),
            $command->getItem(),
            $command->getAction(),
            $command->getMessage()
        );

        $this->transactionManager->transactional(function () use ($activityLog) {
            $this->activityLogWriteStorage->add($activityLog);
        });
    }
}