<?php

declare(strict_types=1);

namespace App\ActivityLog\Application;

final class CreateActivityLogCommand
{
    private int $initiator;

    private string $type;

    private int $item;

    private string $action;

    private string $message;

    public function __construct(int $initiator, string $type, int $item, string $action, string $message)
    {
        $this->initiator = $initiator;
        $this->type = $type;
        $this->item = $item;
        $this->action = $action;
        $this->message = $message;
    }

    public function getInitiator(): int
    {
        return $this->initiator;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getItem(): int
    {
        return $this->item;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}