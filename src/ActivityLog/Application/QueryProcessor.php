<?php

declare(strict_types=1);

namespace App\ActivityLog\Application;

use App\ActivityLog\Domain\ActivityLog;
use App\ActivityLog\Domain\ActivityLogReadStorage;
use App\Core\Application\Search\SearchQuery;

final class QueryProcessor
{
    private ActivityLogReadStorage $activityLogReadStorage;

    public function __construct(ActivityLogReadStorage $activityLogReadStorage)
    {
        $this->activityLogReadStorage = $activityLogReadStorage;
    }

    /**
     * @return ActivityLogData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        return array_map([$this, 'mapToData'], $this->activityLogReadStorage->getBySearchQuery($query));
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->activityLogReadStorage->countBySearchQuery($query);
    }

    private function mapToData(ActivityLog $activityLog): ActivityLogData
    {
        return new ActivityLogData($activityLog);
    }
}