<?php

declare(strict_types=1);

namespace App\Places\Application;

final class PlaceReviewAuthor
{
    private int $reviewId;

    private int $accountId;

    private string $name;

    private ?string $avatar;

    public function __construct(int $reviewId, int $accountId, string $name, ?string $avatar)
    {
        $this->reviewId = $reviewId;
        $this->accountId = $accountId;
        $this->name = $name;
        $this->avatar = $avatar;
    }

    public function getReviewId(): int
    {
        return $this->reviewId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }
}
