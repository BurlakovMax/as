<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceAttribute;
use App\Places\Domain\PlaceAttributeReadStorage;
use App\Places\Domain\PlaceTypeAttribute;
use App\Places\Domain\PlaceTypeAttributeReadStorage;
use Doctrine\ORM\Query\QueryException;

final class PlaceAttributeQueryProcessor
{
    private PlaceAttributeReadStorage $placeAttributeReadStorage;

    private PlaceTypeAttributeReadStorage $placeTypeAttributeReadStorage;

    public function __construct(
        PlaceAttributeReadStorage $placeAttributeReadStorage,
        PlaceTypeAttributeReadStorage $placeTypeAttributeReadStorage
    ) {
        $this->placeAttributeReadStorage = $placeAttributeReadStorage;
        $this->placeTypeAttributeReadStorage = $placeTypeAttributeReadStorage;
    }

    /**
     * @return PlaceAttributeData[]
     * @throws QueryException
     */
    public function getByPlaceId(int $placeId): array
    {
        return array_map([$this, 'mapToData'], $this->placeAttributeReadStorage->getByPlaceId($placeId));
    }

    /**
     * @return PlaceTypeAttributeData[]
     * @throws QueryException
     */
    public function getByPlaceType(int $placeTypeId): array
    {
        return array_map([$this, 'mapToPlaceTypeAttributeData'], $this->placeTypeAttributeReadStorage->getByPlaceType($placeTypeId));
    }

    public function count(): int
    {
        return $this->placeAttributeReadStorage->countAll();
    }

    private function mapToData(PlaceAttribute $placeAttribute): PlaceAttributeData
    {
        return new PlaceAttributeData($placeAttribute);
    }

    private function mapToPlaceTypeAttributeData(PlaceTypeAttribute $placeTypeAttribute): PlaceTypeAttributeData
    {
        return new PlaceTypeAttributeData($placeTypeAttribute);
    }

    public function workingHoursByDay(array $hours): array
    {
        $workingHours['Monday'] = $this->getHoursForDay(
            $hours['monday_from'] ?? null,
            $hours['monday_to'] ?? null
        );
        $workingHours['Tuesday'] = $this->getHoursForDay(
            $hours['tuesday_from'] ?? null,
            $hours['tuesday_to'] ?? null
        );
        $workingHours['Wednesday'] = $this->getHoursForDay(
            $hours['wednesday_from'] ?? null,
            $hours['wednesday_to'] ?? null
        );
        $workingHours['Thursday'] = $this->getHoursForDay(
            $hours['thursday_from'] ?? null,
            $hours['thursday_to'] ?? null
        );
        $workingHours['Friday'] = $this->getHoursForDay(
            $hours['friday_from'] ?? null,
            $hours['friday_to'] ?? null
        );
        $workingHours['Saturday'] = $this->getHoursForDay(
            $hours['saturday_from'] ?? null,
            $hours['saturday_to'] ?? null
        );
        $workingHours['Sunday'] = $this->getHoursForDay(
            $hours['sunday_from'] ?? null,
            $hours['sunday_to'] ?? null
        );

        return $workingHours;
    }

    private function getHoursForDay($open, $close): string
    {
        if ($open === 0 && $close === 1440) {
            return 'Open 24 Hours';
        } elseif ($open === 0 && $close === 720) {
            return 'Open 24 Hours';
        } elseif ($open === -1 && $close === -1) {
            return 'Closed';
        } elseif ('NULL' === $open || 'NULL' === $close) {
            return '---';
        }

        return $this->convertHours($open). ' - ' .$this->convertHours($close);
    }

    private function convertHours($workingTimeInMinutes): string
    {
        if ($workingTimeInMinutes > 1440) {
            $workingTimeInMinutes -= 1440;
        }

        $ampm = ($workingTimeInMinutes / 60 >= 24 || $workingTimeInMinutes / 60 < 12) ? 'AM' : 'PM';

        while ($workingTimeInMinutes > 720) {
            $workingTimeInMinutes -= 720;
        }

        $workingMinutes = $workingTimeInMinutes % 60 === 0 ? '00' : ($workingTimeInMinutes % 60);

        $workingHours = (int)($workingTimeInMinutes / 60) ?: '12';

        $workingTimeInHours = $workingHours . ':' . $workingMinutes;

        $workingTimeInMinutes = $workingTimeInMinutes ? $workingTimeInHours : '12:00';

        return $workingTimeInMinutes . $ampm;
    }

    public function fetchOptionsForAttribute(PlaceAttributeData $attributeData): string
    {
        if (null !== $attributeData->getOptions()) {
            $placeAttributeOptionMask = explode('|', $attributeData->getOptions());
            $result = [];
            foreach ($placeAttributeOptionMask as $item) {
                $placeAttributeOptions = explode('@', $item);
                $result[$placeAttributeOptions[0]] = $placeAttributeOptions[1];
            }

            return $result[$attributeData->getValue()] ?? '';
        }

        return '';
    }
}
