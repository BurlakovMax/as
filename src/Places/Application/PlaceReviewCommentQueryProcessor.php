<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\AccountReadStorage;
use App\Places\Domain\PlaceReviewComment;
use App\Places\Domain\PlaceReviewCommentReadStorage;
use Doctrine\ORM\Query\QueryException;

final class PlaceReviewCommentQueryProcessor
{
    private PlaceReviewCommentReadStorage $placeReviewCommentReadStorage;
    private AccountReadStorage $accountReadStorage;

    public function __construct(
        PlaceReviewCommentReadStorage $placeReviewCommentReadStorage,
        AccountReadStorage $accountReadStorage
    ) {
        $this->placeReviewCommentReadStorage = $placeReviewCommentReadStorage;
        $this->accountReadStorage = $accountReadStorage;
    }

    /**
     * @throws PlaceReviewCommentNotFound
     */
    public function get(int $id): PlaceReviewCommentData
    {
        $placeReviewComment = $this->placeReviewCommentReadStorage->get($id);

        if (null === $placeReviewComment) {
            throw new PlaceReviewCommentNotFound();
        }

        return $this->mapToData($placeReviewComment);
    }

    /**
     * @return PlaceReviewCommentWithAuthor[]
     * @throws QueryException
     */
    public function getWithAuthorsByReviewId(int $reviewId): array
    {
        $comments = $this->getByReviewId($reviewId);
        $commentsWithAuthors = [];

        foreach ($comments as $comment) {
            $commentAccount = $this->accountReadStorage->getAccount($comment->getAccountId());

            $commentsWithAuthors[] = new PlaceReviewCommentWithAuthor(
                $comment,
                new PlaceReviewAuthor(
                    $comment->getAccountId(),
                    $commentAccount->getId(),
                    $commentAccount->getName(),
                    $commentAccount->getAvatar()
                )
            );
        }

        return $commentsWithAuthors;
    }

    /**
     * @return PlaceReviewCommentData[]
     * @throws QueryException
     */
    public function getByReviewId(int $reviewId): array
    {
        return array_map([$this,'mapToData'], $this->placeReviewCommentReadStorage->getByReviewId($reviewId));
    }

    private function mapToData(PlaceReviewComment $placeReviewComment): PlaceReviewCommentData
    {
        return new PlaceReviewCommentData($placeReviewComment);
    }
}
