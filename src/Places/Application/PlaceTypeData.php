<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceType;
use Swagger\Annotations as SWG;

final class PlaceTypeData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private ?int $parentId;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private string $url;

    public function __construct(PlaceType $placeType)
    {
        $this->id = $placeType->getId();
        $this->parentId = $placeType->getParentId();
        $this->name = $placeType->getName();
        $this->url = $placeType->getUrl();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}