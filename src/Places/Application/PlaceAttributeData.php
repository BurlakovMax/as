<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceAttribute;
use Swagger\Annotations as SWG;

final class PlaceAttributeData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $placeId;

    /**
     * @SWG\Property()
     */
    private int $attributeId;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private ?string $value;

    /**
     * @SWG\Property()
     */
    private ?string $title;

    /**
     * @SWG\Property()
     */
    private ?string $type;

    /**
     * @SWG\Property()
     */
    private ?string $options;

    public function __construct(PlaceAttribute $placeAttribute)
    {
        $this->id = $placeAttribute->getId();
        $this->placeId = $placeAttribute->getPlaceId();
        $this->attributeId = $placeAttribute->getAttributeId();
        $this->name = $placeAttribute->getName();
        $this->value = $placeAttribute->getValue();
        $this->title = $placeAttribute->getTitle();
        $this->type = $placeAttribute->getType();
        $this->options = $placeAttribute->getOptions();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getAttributeId(): int
    {
        return $this->attributeId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getOptions(): ?string
    {
        return $this->options;
    }
}
