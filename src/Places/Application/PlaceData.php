<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\Place;
use DateTimeImmutable;
use Swagger\Annotations as SWG;

final class PlaceData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private ?int $typeId;

    /**
     * @SWG\Property()
     */
    private ?string $name;

    /**
     * @SWG\Property()
     */
    private ?int $locationId;

    /**
     * @SWG\Property()
     */
    private ?int $neighborId;

    /**
     * @SWG\Property()
     */
    private ?int $locationExactId;

    /**
     * @SWG\Property()
     */
    private ?string $address1;

    /**
     * @SWG\Property()
     */
    private ?string $address2;

    /**
     * @SWG\Property()
     */
    private ?string $zipCode;

    /**
     * @SWG\Property()
     */
    private ?string $phone1;

    /**
     * @SWG\Property()
     */
    private ?string $phone2;

    /**
     * @SWG\Property()
     */
    private ?string $fax;

    /**
     * @SWG\Property()
     */
    private $email;

    /**
     * @SWG\Property()
     */
    private ?string $website;

    /**
     * @SWG\Property()
     */
    private ?string $facebook;

    /**
     * @SWG\Property()
     */
    private ?string $twitter;

    /**
     * @SWG\Property()
     */
    private ?string $instagram;

    /**
     * @SWG\Property()
     */
    private ?string $whatsapp;

    /**
     * @SWG\Property()
     */
    private ?string $line;

    /**
     * @SWG\Property()
     */
    private ?string $wechat;

    /**
     * @SWG\Property()
     */
    private ?string $telegram;

    /**
     * @SWG\Property()
     */
    private ?string $latitude;

    /**
     * @SWG\Property()
     */
    private ?string $longitude;

    /**
     * @SWG\Property()
     */
    private ?string $image;

    /**
     * @SWG\Property()
     */
    private ?string $thumbnail;

    /**
     * @SWG\Property()
     */
    private int $review;

    /**
     * @SWG\Property()
     */
    private int $recommend;

    /**
     * @SWG\Property()
     */
    private float $overall;

    /**
     * @SWG\Property()
     */
    private int $lastReviewId;

    /**
     * @SWG\Property()
     */
    private ?int $ownerId;

    /**
     * @SWG\Property()
     */
    private $sponsorship;

    /**
     * @SWG\Property()
     */
    private $editable;

    /**
     * @SWG\Property()
     */
    private ?bool $fake;

    /**
     * @SWG\Property()
     */
    private ?string $workerComment;

    /**
     * @SWG\Property()
     */
    private ?int $oldId;

    /**
     * @SWG\Property(type="date")
     */
    private ?DateTimeImmutable $createdAt;

    /**
     * @SWG\Property(type="date")
     */
    private ?DateTimeImmutable $updatedAt;

    /**
     * @SWG\Property(type="date")
     */
    private ?DateTimeImmutable $deletedAt;

    /**
     * @SWG\Property()
     */
    private bool $isTest;

    /**
     * @SWG\Property()
     */
    private ?bool $empImported;

    /**
     * @SWG\Property()
     */
    private ?int $empId;

    /**
     * @SWG\Property()
     */
    private ?string $mapImage;

    /**
     * @SWG\Property(type="date")
     */
    private ?DateTimeImmutable $lastChecked;

    /**
     * @SWG\Property()
     */
    private ?int $lastCheckedBy;

    /**
     * @SWG\Property()
     */
    private ?string $source;

    public function __construct(Place $place)
    {
        $this->id = $place->getId();
        $this->typeId = $place->getTypeId();
        $this->name = $place->getName();
        $this->locationId = $place->getLocationId();
        $this->neighborId = $place->getNeighborId();
        $this->locationExactId = $place->getLocationExactId();
        $this->address1 = $place->getAddress1();
        $this->address2 = $place->getAddress2();
        $this->zipCode = $place->getZipCode();
        $this->phone1 = $place->getPhone1();
        $this->phone2 = $place->getPhone2();
        $this->fax = $place->getFax();
        $this->email = $place->getEmail() ? $place->getEmail()->getValue() : null;
        $this->website = $place->getWebSite();
        $this->facebook = $place->getFacebook();
        $this->twitter = $place->getTwitter();
        $this->instagram = $place->getInstagram();
        $this->whatsapp = $place->getWhatsapp();
        $this->line = $place->getLine();
        $this->wechat = $place->getWechat();
        $this->telegram = $place->getTelegram();
        $this->latitude = $place->getLatitude();
        $this->longitude = $place->getLongitude();
        $this->image = $place->getImage();
        $this->thumbnail = $place->getThumbnail();
        $this->review = $place->getReview();
        $this->recommend = $place->getRecommend();
        $this->overall = $place->getOverall();
        $this->lastReviewId = $place->getLastReviewId();
        $this->ownerId = $place->getOwnerId();
        $this->sponsorship = $place->getSponsorship()->getRawValue();
        $this->editable = $place->getEditable() ? $place->getEditable()->getRawValue() : null;
        $this->fake = $place->getFake();
        $this->workerComment = $place->getWorkerComment();
        $this->oldId = $place->getOldId();
        $this->createdAt = $place->getCreatedAt();
        $this->updatedAt = $place->getUpdatedAt();
        $this->deletedAt = $place->getDeletedAt();
        $this->empImported = $place->getEmpImported();
        $this->empId = $place->getEmpId();
        $this->mapImage = $place->getMapImage();
        $this->lastChecked = $place->getLastChecked();
        $this->lastCheckedBy = $place->getLastCheckedBy();
        $this->source = $place->getSource();
        $this->isTest = $place->isTest();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getLocationId(): ?int
    {
        return $this->locationId;
    }

    public function getNeighborId(): ?int
    {
        return $this->neighborId;
    }

    public function getLocationExactId(): ?int
    {
        return $this->locationExactId;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function getPhone1(): ?string
    {
        return $this->phone1;
    }

    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getWebSite(): ?string
    {
        return $this->website;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function getLine(): ?string
    {
        return $this->line;
    }

    public function getWechat(): ?string
    {
        return $this->wechat;
    }

    public function getTelegram(): ?string
    {
        return $this->telegram;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function getReview(): int
    {
        return $this->review;
    }

    public function getRecommend(): int
    {
        return $this->recommend;
    }

    public function getOverall(): float
    {
        return $this->overall;
    }

    public function getLastReviewId(): int
    {
        return $this->lastReviewId;
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    public function getSponsorship(): int
    {
        return $this->sponsorship;
    }

    public function getEditable(): ?int
    {
        return $this->editable;
    }

    public function getFake(): ?bool
    {
        return $this->fake;
    }

    public function getWorkerComment(): ?string
    {
        return $this->workerComment;
    }

    public function getOldId(): ?int
    {
        return $this->oldId;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return  $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return  $this->updatedAt;
    }

    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function getIsTest(): bool
    {
        return $this->isTest;
    }

    public function getEmpImported(): ?bool
    {
        return $this->empImported;
    }

    public function getEmpId(): ?int
    {
        return $this->empId;
    }

    public function getMapImage(): ?string
    {
        return $this->mapImage;
    }

    public function getLastChecked(): ?DateTimeImmutable
    {
        return $this->lastChecked;
    }

    public function getLastCheckedBy(): ?int
    {
        return $this->lastCheckedBy;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }
}
