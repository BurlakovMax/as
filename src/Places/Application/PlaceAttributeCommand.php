<?php

declare(strict_types=1);

namespace App\Places\Application;

use LazyLemurs\Commander\Property;

final class PlaceAttributeCommand
{
    /**
     * @Property()
     */
    private int $attributeId;

    /**
     * @Property()
     */
    private ?string $value;

    /**
     * @Property()
     */
    private ?string $text;

    public function __construct(int $id, ?string $value, ?string $text)
    {
        $this->attributeId = $id;
        $this->value = $value;
        $this->text = $text;
    }

    public function getAttributeId(): int
    {
        return $this->attributeId;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getText(): ?string
    {
        return $this->text;
    }
}
