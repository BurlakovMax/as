<?php

declare(strict_types=1);

namespace App\Places\Application\PlaceReviewProvider;

use App\Places\Domain\PlaceReviewProvider\PlaceReviewProvider;
use Swagger\Annotations as SWG;

final class PlaceReviewProviderData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private int $placeId;

    /**
     * @SWG\Property()
     */
    private int $imageUploadCount;

    public function __construct(PlaceReviewProvider $provider)
    {
        $this->id = $provider->getId();
        $this->name = $provider->getName();
        $this->placeId = $provider->getPlaceId();
        $this->imageUploadCount = $provider->getImageUploadCount();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getImageUploadCount(): int
    {
        return $this->imageUploadCount;
    }
}