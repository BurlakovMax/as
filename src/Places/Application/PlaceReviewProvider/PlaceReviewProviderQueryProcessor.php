<?php

declare(strict_types=1);

namespace App\Places\Application\PlaceReviewProvider;

use App\Places\Domain\PlaceReviewProvider\PlaceReviewProvider;
use App\Places\Domain\PlaceReviewProvider\PlaceReviewProviderReadStorage;
use Doctrine\ORM\Query\QueryException;

final class PlaceReviewProviderQueryProcessor
{
    private PlaceReviewProviderReadStorage $providerReadStorage;

    public function __construct(PlaceReviewProviderReadStorage $providerReadStorage)
    {
        $this->providerReadStorage = $providerReadStorage;
    }

    /**
     * @return PlaceReviewProviderData[]
     * @throws QueryException
     */
    public function getByPlaceId(int $placeId): array
    {
        return array_map([$this, 'mapToData'], $this->providerReadStorage->getByPlaceId($placeId));
    }

    private function mapToData(PlaceReviewProvider $provider): PlaceReviewProviderData
    {
        return new PlaceReviewProviderData($provider);
    }
}