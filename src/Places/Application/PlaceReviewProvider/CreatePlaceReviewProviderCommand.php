<?php

declare(strict_types=1);

namespace App\Places\Application\PlaceReviewProvider;

use LazyLemurs\Commander\Property;

final class CreatePlaceReviewProviderCommand
{
    /**
     * @Property(type="int")
     */
    private int $placeId;

    /**
     * @Property(type="string")
     */
    private string $providerName;

    /**
     * @Property(type="?int")
     */
    private ?int $imageUploadCount;

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getName(): string
    {
        return $this->providerName;
    }

    public function getImageUploadCount(): ?int
    {
        return $this->imageUploadCount;
    }
}