<?php

declare(strict_types=1);

namespace App\Places\Application\BusinessOwner;

use LazyLemurs\Exceptions\Exception;

final class PlaceHasPlaceowner extends Exception
{

}