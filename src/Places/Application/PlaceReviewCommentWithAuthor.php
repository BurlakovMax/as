<?php

declare(strict_types=1);

namespace App\Places\Application;

final class PlaceReviewCommentWithAuthor
{
    private PlaceReviewCommentData $comment;

    private PlaceReviewAuthor $author;

    public function __construct(PlaceReviewCommentData $comment, PlaceReviewAuthor $author)
    {
        $this->comment = $comment;
        $this->author = $author;
    }

    public function getComment(): PlaceReviewCommentData
    {
        return $this->comment;
    }

    public function getAuthor(): PlaceReviewAuthor
    {
        return $this->author;
    }
}
