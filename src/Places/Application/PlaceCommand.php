<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Core\Domain\Coordinates;
use LazyLemurs\Commander\Property;
use App\Places\Domain\Editable;
use App\Places\Domain\Sponsorship;
use LazyLemurs\Structures\Email;

abstract class PlaceCommand
{
    /**
     * @Property(type="?\App\Places\Application\PlaceAddressCommand", isStructure=true)
     */
    private ?PlaceAddressCommand $address;

    /**
     * @Property(type="?\App\Places\Application\PlaceContactDetailsCommand", isStructure=true)
     */
    private ?PlaceContactDetailsCommand $contactDetails;

    /**
     * @Property(type="?string")
     */
    private ?string $webSite;

    /**
     * @Property(type="?\App\Places\Application\PlaceSocialNetworksCommand", isStructure=true)
     */
    private ?PlaceSocialNetworksCommand $socialNetworks;

    /**
     * @Property(type="?string")
     */
    private ?string $image;

    /**
     * @Property(type="?int")
     */
    private ?int $review;

    /**
     * @Property(type="?int")
     */
    private ?int $recommend;

    /**
     * @Property(type="?float")
     */
    private ?float $overall;

    /**
     * @Property(type="?int")
     */
    private ?int $lastReviewId;

    /**
     * @Property(type="?int")
     */
    private ?int $ownerId;

    /**
     * @Property(type="?\App\Places\Domain\Sponsorship", constructor="getValueOf")
     */
    private ?Sponsorship $sponsorship;

    /**
     * @Property(type="?\App\Places\Domain\Editable", constructor="getValueOf")
     */
    private ?Editable $editable;

    /**
     * @Property(type="?bool")
     */
    private ?bool $fake;

    /**
     * @Property(type="?string")
     */
    private ?string $workerComment;

    /**
     * @Property(type="?bool")
     */
    private ?bool $test;

    /**
     * @Property(type="?bool")
     */
    private ?bool $empImported;

    /**
     * @Property(type="?int")
     */
    private ?int $empId;

    /**
     * @Property(type="?string")
     */
    private ?string $mapImage;

    /**
     * @Property(type="?string")
     */
    private ?string $source;

    /**
     * @Property(type="?\App\Core\Domain\Coordinates")
     */
    private ?Coordinates $coordinates;

    /**
     * @Property(type="?string")
     */
    private ?string $thumbnail;

    /**
     * @var PlaceAttributeCommand[]
     */
    private array $placeAttributes = [];

    public function getAddress(): ?PlaceAddressCommand
    {
        return $this->address;
    }

    public function getContactDetails(): ?PlaceContactDetailsCommand
    {
        return $this->contactDetails;
    }

    public function getEmail(): ?Email
    {
        return $this->getContactDetails()->getEmail();
    }

    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    public function getSocialNetworks(): ?PlaceSocialNetworksCommand
    {
        return $this->socialNetworks;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getReview(): ?int
    {
        return $this->review;
    }

    public function getRecommend(): ?int
    {
        return $this->recommend;
    }

    public function getOverall(): ?float
    {
        return $this->overall;
    }

    public function getLastReviewId(): ?int
    {
        return $this->lastReviewId;
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    public function getSponsorship(): ?Sponsorship
    {
        return $this->sponsorship;
    }

    public function getEditable(): ?Editable
    {
        return $this->editable;
    }

    public function getFake(): ?bool
    {
        return $this->fake;
    }

    public function getWorkerComment(): ?string
    {
        return $this->workerComment;
    }

    public function getTest(): ?bool
    {
        return $this->test;
    }

    public function getEmpImported(): ?bool
    {
        return $this->empImported;
    }

    public function getEmpId(): ?int
    {
        return $this->empId;
    }

    public function getMapImage(): ?string
    {
        return $this->mapImage;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function getAddress1(): ?string
    {
        return $this->getAddress()->getAddress1();
    }

    public function getAddress2(): ?string
    {
        return $this->getAddress()->getAddress2();
    }

    public function getZipCode(): ?string
    {
        return $this->getAddress()->getZipCode();
    }

    public function getPhone1(): ?string
    {
        return $this->getContactDetails()->getPhone1();
    }

    public function getPhone2(): ?string
    {
        return $this->getContactDetails()->getPhone2();
    }

    public function getFax(): ?string
    {
        return $this->getContactDetails()->getFax();
    }

    public function getFacebook(): ?string
    {
        return $this->getSocialNetworks()->getFacebook();
    }

    public function getTwitter(): ?string
    {
        return $this->getSocialNetworks()->getTwitter();
    }

    public function getInstagram(): ?string
    {
        return $this->getSocialNetworks()->getInstagram();
    }

    public function getWhatsapp(): ?string
    {
        return $this->getSocialNetworks()->getWhatsapp();
    }

    public function getLine(): ?string
    {
        return $this->getSocialNetworks()->getLine();
    }

    public function getWechat(): ?string
    {
        return $this->getSocialNetworks()->getWechat();
    }

    public function getTelegram(): ?string
    {
        return $this->getSocialNetworks()->getTelegram();
    }

    /**
     * @return  PlaceAttributeCommand[]
     */
    public function getPlaceAttributes(): array
    {
        return $this->placeAttributes;
    }

    /**
     * @param PlaceAttributeCommand[]
     */
    public function setPlaceAttributes(array $attributes): void
    {
        $this->placeAttributes = $attributes;
    }
}
