<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\Rating;

final class UpdatePlaceReviewCommand
{
    private int $id;

    private ?int $providerId;

    private ?string $comment;

    private ?Rating $star;

    public function __construct(
        int $id,
        ?int $providerId,
        ?string $comment,
        ?int $star
    ) {
        $this->id = $id;
        $this->providerId = $providerId;
        $this->comment = $comment;
        $this->star = $star !== null ? Rating::getValueOf($star) : null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProviderId(): ?int
    {
        return $this->providerId;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getStar(): ?Rating
    {
        return $this->star;
    }
}