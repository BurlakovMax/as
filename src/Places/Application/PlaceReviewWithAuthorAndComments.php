<?php

declare(strict_types=1);

namespace App\Places\Application;

final class PlaceReviewWithAuthorAndComments
{
    private PlaceReviewData $review;

    private PlaceReviewAuthor $author;

    private int $id;

    private int $placeId;

    /**
     * @var PlaceReviewCommentWithAuthor[]
     */
    private array $comments;

    /**
     * @param PlaceReviewCommentWithAuthor[] $comments
     */
    public function __construct(PlaceReviewData $review, PlaceReviewAuthor $author, array $comments)
    {
        $this->id = $review->getId();
        $this->placeId = $review->getPlaceId();
        $this->review = $review;
        $this->author = $author;
        $this->comments = $comments;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getReview(): PlaceReviewData
    {
        return $this->review;
    }

    public function getAuthor(): PlaceReviewAuthor
    {
        return $this->author;
    }

    /**
     * @return PlaceReviewCommentWithAuthor[]
     */
    public function getComments(): array
    {
        return $this->comments;
    }
}
