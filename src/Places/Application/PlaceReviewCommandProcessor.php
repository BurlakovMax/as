<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceReview;
use App\Places\Domain\PlaceReviewService;
use LazyLemurs\TransactionManager\TransactionManager;
use Throwable;

final class PlaceReviewCommandProcessor
{
    private TransactionManager $transactionManager;

    private PlaceReviewService $placeReviewService;

    public function __construct(TransactionManager $transactionManager, PlaceReviewService $placeReviewService)
    {
        $this->transactionManager = $transactionManager;
        $this->placeReviewService = $placeReviewService;
    }

    /**
     * @throws Throwable
     */
    public function add(CreatePlaceReviewCommand $command): int
    {
        $placeReview = new PlaceReview(
            $command->getPlaceId(),
            $command->getAccountId(),
            $command->getProviderId(),
            $command->getComment(),
            $command->getStar(),
            $command->getIp(),
        );

        $this->transactionManager->transactional(function () use ($placeReview): void {
            $this->placeReviewService->create($placeReview);
        });

        return $placeReview->getId();
    }

    /**
     * @throws Throwable
     */
    public function update(UpdatePlaceReviewCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
            $this->placeReviewService->update(
                $command->getId(),
                $command->getProviderId(),
                $command->getComment(),
                $command->getStar()
            );
        });
    }

    /**
     * @throws Throwable
     */
    public function delete(int $placeReviewId): void
    {
        $this->transactionManager->transactional(function () use ($placeReviewId): void {
            $this->placeReviewService->delete($placeReviewId);
        });
    }
}