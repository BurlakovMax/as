<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\FileUploadInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class ImageUploadProcessor
{
    private FileUploadInterface $fileUpload;

    private string $rootDir;

    private string $publicDir;

    private string $dir;

    public function __construct(FileUploadInterface $fileUpload, string $rootDir, string $publicDir, string $dir)
    {
        $this->fileUpload = $fileUpload;
        $this->rootDir = $rootDir;
        $this->publicDir = $publicDir;
        $this->dir = $dir;
    }

    public function save(string $name, UploadedFile $uploadedFile): string
    {
        return $this->fileUpload->save($name, $uploadedFile);
    }

    public function removeByName(string $name): void
    {
        if (file_exists($this->rootDir . $this->publicDir . $this->dir . $name)) {
            unlink($this->rootDir . $this->publicDir . $this->dir . $name);
        }
    }
}