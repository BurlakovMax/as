<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\Rating;

final class CreatePlaceReviewCommand
{
    private int $placeId;

    private int $accountId;

    private int $providerId;

    private string $comment;

    private Rating $star;

    private ?string $ip;

    public function __construct(
        int $placeId,
        int $accountId,
        int $providerId,
        string $comment,
        int $star,
        ?string $ip
    )
    {
        $this->placeId = $placeId;
        $this->accountId = $accountId;
        $this->providerId = $providerId;
        $this->comment = $comment;
        $this->star = Rating::getValueOf($star);
        $this->ip = $ip;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getProviderId(): int
    {
        return $this->providerId;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getStar(): Rating
    {
        return $this->star;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }
}