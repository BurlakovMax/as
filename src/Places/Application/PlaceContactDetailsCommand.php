<?php

declare(strict_types=1);

namespace App\Places\Application;

use LazyLemurs\Commander\Property;
use LazyLemurs\Structures\Email;

final class PlaceContactDetailsCommand
{
    /**
     * @Property(type="?string")
     */
    private ?string $phone1;

    /**
     * @Property(type="?string")
     */
    private ?string $phone2;

    /**
     * @Property(type="?string")
     */
    private ?string $fax;

    /**
     * @Property(type="?\LazyLemurs\Structures\Email")
     */
    private ?Email $email;

    public function getPhone1(): ?string
    {
        return $this->phone1;
    }

    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }
}