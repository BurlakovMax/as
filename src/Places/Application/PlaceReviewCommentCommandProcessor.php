<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceReviewComment;
use App\Places\Domain\PlaceReviewCommentReadStorage;
use App\Places\Domain\PlaceReviewCommentWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;
use Throwable;

final class PlaceReviewCommentCommandProcessor
{
    private TransactionManager $transactionManager;

    private PlaceReviewCommentWriteStorage $placeReviewCommentWriteStorage;

    private PlaceReviewCommentReadStorage $placeReviewCommentReadStorage;

    public function __construct(
        TransactionManager $transactionManager,
        PlaceReviewCommentWriteStorage $placeReviewCommentWriteStorage,
        PlaceReviewCommentReadStorage $placeReviewCommentReadStorage
    ) {
        $this->transactionManager = $transactionManager;
        $this->placeReviewCommentWriteStorage = $placeReviewCommentWriteStorage;
        $this->placeReviewCommentReadStorage = $placeReviewCommentReadStorage;
    }

    /**
     * @throws Throwable
     */
    public function add(int $userId, int $reviewId, string $comment): int
    {
        $placeReviewComment = new PlaceReviewComment($reviewId, $userId, $comment);

        $this->transactionManager->transactional(function () use ($placeReviewComment): void {
            $this->placeReviewCommentWriteStorage->add($placeReviewComment);
        });

        return $placeReviewComment->getId();
    }

    /**
     * @throws PlaceReviewCommentNotFound
     * @throws Throwable
     */
    public function delete(int $placeReviewCommentId): void
    {
        $placeReviewComment = $this->placeReviewCommentReadStorage->get($placeReviewCommentId);

        if (null === $placeReviewComment) {
            throw new PlaceReviewCommentNotFound();
        }

        $this->transactionManager->transactional(function () use ($placeReviewComment): void {
            $this->placeReviewCommentWriteStorage->delete($placeReviewComment);
        });
    }
}