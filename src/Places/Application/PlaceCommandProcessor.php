<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\Place;
use App\Places\Domain\PlaceAttributeService;
use App\Places\Domain\PlaceService;
use LazyLemurs\TransactionManager\TransactionManager;
use RuntimeException;
use Throwable;

final class PlaceCommandProcessor
{
    private TransactionManager $transactionManager;

    private PlaceService $placeService;

    private PlaceAttributeService $placeAttributeService;

    public function __construct(
        TransactionManager $transactionManager,
        PlaceService $placeService,
        PlaceAttributeService $placeAttributeService
    ) {
        $this->transactionManager = $transactionManager;
        $this->placeService = $placeService;
        $this->placeAttributeService = $placeAttributeService;
    }

    /**
     *
     * @throws Throwable
     * @throws RuntimeException
     */
    public function create(CreatePlaceCommand $createCommand): int
    {
        $place = new Place($createCommand);

        $this->transactionManager->transactional(function () use ($createCommand, $place): void {

            $placeId = $this->placeService->create($place);

            if (!empty($createCommand->getPlaceAttributes())) {
                foreach ($createCommand->getPlaceAttributes() as $attribute) {
                    $this->placeAttributeService->create(
                        $placeId,
                        $attribute->getAttributeId(),
                        $attribute->getValue(),
                        $attribute->getText(),
                    );
                }
            }
        });

        return $place->getId();
    }

    /**
     * @throws Throwable
     */
    public function update(UpdatePlaceCommand $updatePlaceCommand): void
    {
        $this->transactionManager->transactional(function () use ($updatePlaceCommand): void {
            $this->placeService->update($updatePlaceCommand);

            if (!empty($updatePlaceCommand->getPlaceAttributes())) {
                $this->placeAttributeService->update($updatePlaceCommand);
            }
        });
    }

    /**
     * @throws Throwable
     */
    public function assignOwner(int $id, int $ownerId, int $recurringPeriod): void
    {
        $this->transactionManager->transactional(function () use ($id, $ownerId, $recurringPeriod): void {
            $this->placeService->assignOwner($id, $ownerId, $recurringPeriod);
        });
    }

    /**
     * @throws Throwable
     */
    public function delete(int $id): void
    {
        $this->transactionManager->transactional(function () use ($id): void {
            $this->placeService->delete($id);
        });
    }

    /**
     * @throws Throwable
     */
    public function forceDelete(int $id): void
    {
        $this->transactionManager->transactional(function () use ($id): void {
            $this->placeService->forceDelete($id);
        });
    }

    public function checkedSuccess(int $placeId, int $accountId): void
    {
        $this->transactionManager->transactional(function () use ($placeId, $accountId): void {
            $this->placeService->checkedSuccess($placeId, $accountId);
        });
    }

    public function skipped(int $placeId, int $accountId): void
    {
        $this->transactionManager->transactional(function () use ($placeId, $accountId): void {
            $this->placeService->skipped($placeId, $accountId);
        });
    }

    public function phoneNotAnswered(int $placeId, int $accountId): void
    {
        $this->transactionManager->transactional(function () use ($placeId, $accountId): void {
            $this->placeService->phoneNotAnswered($placeId, $accountId);
        });
    }

    public function removeImage(int $placeId): void
    {
        $this->transactionManager->transactional(function () use ($placeId): void {
            $this->placeService->removeImage($placeId);
        });
    }
}
