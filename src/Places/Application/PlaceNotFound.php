<?php

declare(strict_types=1);

namespace App\Places\Application;

use LazyLemurs\Exceptions\NotFoundException;

final class PlaceNotFound extends NotFoundException
{

}