<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceReviewComment;
use DateTimeImmutable;
use Swagger\Annotations as SWG;

final class PlaceReviewCommentData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $reviewId;

    /**
     * @SWG\Property()
     */
    private int $accountId;

    /**
     * @SWG\Property(type="date")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private string $comment;

    /**
     * @SWG\Property()
     */
    private string $placeReviewComment;

    public function __construct(PlaceReviewComment $placeReviewComment)
    {
        $this->id = $placeReviewComment->getId();
        $this->reviewId = $placeReviewComment->getReviewId();
        $this->accountId = $placeReviewComment->getAccountId();
        $this->createdAt = $placeReviewComment->getCreatedAt();
        $this->comment = $placeReviewComment->getComment();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReviewId(): int
    {
        return $this->reviewId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    private function getPlaceReviewComment(): string
    {
        return $this->placeReviewComment;
    }
}