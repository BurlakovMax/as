<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Places\Domain\PlaceType;
use App\Places\Domain\PlaceTypeReadStorage;

final class PlaceTypeQueryProcessor
{
    private PlaceTypeReadStorage $placeTypeReadStorage;

    public function __construct(PlaceTypeReadStorage $placeTypeReadStorage)
    {
        $this->placeTypeReadStorage = $placeTypeReadStorage;
    }

    /**
     * @throws PlaceTypeNotFound
     */
    public function getById(int $id): PlaceTypeData
    {
        return $this->mapToData(
            $this->get($id)
        );
    }

    /**
     * @return PlaceTypeData[]
     */
    public function getAll(): array
    {
        return array_map([$this,'mapToData'], $this->placeTypeReadStorage->getAll());
    }

    /**
     * @param int[] $ids
     * @return PlaceTypeData[]
     */
    public function getByIds(array $ids): array
    {
        return array_map([ $this, 'mapToData' ], $this->placeTypeReadStorage->getByIds($ids));
    }

    /**
     * @throws PlaceTypeNotFound
     */
    public function getByUrl(string $url): PlaceTypeData
    {
        $placeType = $this->placeTypeReadStorage->getOneByUrl($url);

        if (!$placeType) {
            throw new PlaceTypeNotFound();
        }

        return $this->mapToData($placeType);
    }

    /**
     * @throws PlaceTypeNotFound
     */
    private function get(int $id): PlaceType
    {
        $placeType = $this->placeTypeReadStorage->get($id);

        if (!$placeType) {
            throw new PlaceTypeNotFound();
        }

        return $placeType;
    }

    private function mapToData(PlaceType $placeType): PlaceTypeData
    {
        return new PlaceTypeData($placeType);
    }
}