<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Core\ApplicationSupport\AccessControl\AccessForbidden;

final class AccessControl
{
    private PlaceQueryProcessor $placeQueryProcessor;

    private PlaceReviewQueryProcessor $placeReviewQueryProcessor;

    private PlaceReviewCommentQueryProcessor $placeReviewCommentQueryProcessor;

    public function __construct(
        PlaceQueryProcessor $placeQueryProcessor,
        PlaceReviewQueryProcessor $placeReviewQueryProcessor,
        PlaceReviewCommentQueryProcessor $placeReviewCommentQueryProcessor
    )
    {
        $this->placeQueryProcessor = $placeQueryProcessor;
        $this->placeReviewQueryProcessor = $placeReviewQueryProcessor;
        $this->placeReviewCommentQueryProcessor = $placeReviewCommentQueryProcessor;
    }

    /**
     * @throws AccessForbidden
     */
    public function checkAccessToPlace(int $placeId): void
    {
        try {
            $this->placeQueryProcessor->getById($placeId);
        } catch (PlaceNotFound $exception) {
            throw new AccessForbidden();
        }
    }

    /**
     * @throws AccessForbidden
     */
    public function checkAccessToPlaceReview(int $placeReviewId): PlaceReviewData
    {
        $placeReview = $this->placeReviewQueryProcessor->getById($placeReviewId);

        if (!$placeReview) {
            throw new AccessForbidden();
        }

        return $placeReview;
    }

    /**
     * @throws AccessForbidden
     */
    public function checkAccessToPlaceReviewByAccount(int $placeReviewId, int $accountId): void
    {
        $placeReview = $this->checkAccessToPlaceReview($placeReviewId);

        if ($placeReview->getAccountId() !== $accountId) {
            throw new AccessForbidden();
        }
    }

    /**
     * @throws AccessForbidden
     */
    public function checkAccessToPlaceReviewCommentByAccount(int $placeReviewCommentId, int $accountId): void
    {
        try {
            $placeReviewComment = $this->placeReviewCommentQueryProcessor->get($placeReviewCommentId);

            if ($placeReviewComment->getAccountId() !== $accountId) {
                throw new AccessForbidden();
            }

        } catch (PlaceReviewCommentNotFound $exception) {
            throw new AccessForbidden();
        }
    }

    /**
     * @throws AccessForbidden
     * @throws PlaceNotFound
     */
    public function checkAccessToPlaceAsOwner(int $placeId, int $accountId): void
    {
        $place = $this->placeQueryProcessor->getById($placeId);

        if ($place->getOwnerId() !== $accountId) {
            throw new AccessForbidden();
        }
    }
}