<?php

declare(strict_types=1);

namespace App\Places\Application;

use LazyLemurs\Commander\Property;

final class UpdatePlaceCommand extends PlaceCommand
{
    private int $id;

    /**
     * @Property(type="?string")
     */
    private ?string $name;

    /**
     * @Property(type="?int")
     */
    private ?int $locationId;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getLocationId(): ?int
    {
        return $this->locationId;
    }
}