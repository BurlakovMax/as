<?php

declare(strict_types=1);

namespace App\Places\Application;

use App\Core\Application\Search\SearchQuery;
use App\Places\Domain\AccountReadStorage;
use App\Places\Domain\PlaceReview;
use App\Places\Domain\PlaceReviewReadStorage;
use Doctrine\ORM\Query\QueryException;

final class PlaceReviewQueryProcessor
{
    private PlaceReviewReadStorage $placeReviewReadStorage;
    private AccountReadStorage $accountReadStorage;
    private PlaceReviewCommentQueryProcessor $placeReviewCommentQueryProcessor;

    public function __construct(
        PlaceReviewReadStorage $placeReviewReadStorage,
        AccountReadStorage $accountReadStorage,
        PlaceReviewCommentQueryProcessor $placeReviewCommentQueryProcessor
    ) {
        $this->placeReviewReadStorage = $placeReviewReadStorage;
        $this->accountReadStorage = $accountReadStorage;
        $this->placeReviewCommentQueryProcessor = $placeReviewCommentQueryProcessor;
    }

    public function getById(int $id): ?PlaceReviewData
    {
        $placeReview = $this->placeReviewReadStorage->get($id);
        return $placeReview ? $this->mapToData($placeReview) : null;
    }

    public function getWithAuthorAndCommentsById(int $id): ?PlaceReviewWithAuthorAndComments
    {
        $review = $this->getById($id);

        if ($review === null) {
            return null;
        }

        return $this->wrapWithAuthorAndComments($review);
    }

    /**
     * @return PlaceReviewData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $list = $this->placeReviewReadStorage->getListBySearchQuery($query);

        return $this->mapListToData($list);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->placeReviewReadStorage->countBySearchQuery($query);
    }

    /**
     * @return PlaceReviewWithAuthorAndComments[]
     * @throws QueryException
     */
    public function getWithAuthorsAndCommentsByPlaceId(int $placeId): array
    {
        $reviewsWithAuthorsAndComments = [];
        $reviews = $this->getByPlaceId($placeId);

        foreach ($reviews as $review) {
            $reviewsWithAuthorsAndComments[] = $this->wrapWithAuthorAndComments($review);
        }

        return $reviewsWithAuthorsAndComments;
    }

    /**
     * @return PlaceReviewData[]
     */
    public function getByPlaceId(int $placeId): array
    {
        return array_map([$this,'mapToData'], $this->placeReviewReadStorage->getByPlaceId($placeId));
    }

    public function count(): int
    {
        return $this->placeReviewReadStorage->countAll();
    }

    private function mapToData(PlaceReview $placeReview): PlaceReviewData
    {
        return new PlaceReviewData($placeReview);
    }

    /**
     * @param PlaceReview[] $list
     * @return PlaceReviewData[]
     */
    private function mapListToData(array $list): array
    {
        return array_map(
            function (PlaceReview $placeReview): PlaceReviewData {
                return $this->mapToData($placeReview);
            },
            $list
        );
    }

    private function wrapWithAuthorAndComments(PlaceReviewData $placeReviewData): PlaceReviewWithAuthorAndComments
    {
        $account = $this->accountReadStorage->getAccount($placeReviewData->getAccountId());
        $commentsWithAuthors =
            $this->placeReviewCommentQueryProcessor
                ->getWithAuthorsByReviewId($placeReviewData->getId())
        ;

        return new PlaceReviewWithAuthorAndComments(
            $placeReviewData,
            new PlaceReviewAuthor(
                $placeReviewData->getAccountId(),
                $account->getId(),
                $account->getName(),
                $account->getAvatar()
            ),
            $commentsWithAuthors
        );
    }
}
