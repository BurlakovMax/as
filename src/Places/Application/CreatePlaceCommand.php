<?php

declare(strict_types=1);

namespace App\Places\Application;

use LazyLemurs\Commander\Property;

final class CreatePlaceCommand extends PlaceCommand
{
    /**
     * @Property(type="int")
     */
    private int $typeId;

    /**
     * @Property(type="string")
     */
    private string $name;

    /**
     * @Property(type="int")
     */
    private int $locationId;

    public function getTypeId(): int
    {
        return $this->typeId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }
}