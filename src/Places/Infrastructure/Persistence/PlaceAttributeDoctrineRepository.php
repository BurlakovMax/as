<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Places\Domain\PlaceAttribute;
use App\Places\Domain\PlaceAttributeReadStorage;
use App\Places\Domain\PlaceAttributeWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\TransactionRequiredException;

final class PlaceAttributeDoctrineRepository extends EntityRepository implements PlaceAttributeReadStorage, PlaceAttributeWriteStorage
{
    /**
     * @return PlaceAttribute[]
     * @throws QueryException
     */
    public function getByPlaceId(int $placeId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('placeId', $placeId))
                )
                ->getQuery()
                ->getResult();
    }

    /**
     * @return PlaceAttribute[]
     *
     * @throws QueryException
     * @throws TransactionRequiredException
     */
    public function getByPlaceIdAndLock(int $placeId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('placeId', $placeId))
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getResult();
    }

    public function countAll(): int
    {
        return $this->count([]);
    }

    /**
     * @throws ORMException
     */
    public function create(PlaceAttribute $placeAttribute): void
    {
        $this->getEntityManager()->persist($placeAttribute);
        $this->getEntityManager()->flush();
    }
}