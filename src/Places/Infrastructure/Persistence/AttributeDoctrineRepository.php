<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Places\Domain\Attribute;
use App\Places\Domain\AttributeReadStorage;
use Doctrine\ORM\EntityRepository;

final class AttributeDoctrineRepository extends EntityRepository implements AttributeReadStorage
{
    public function get(int $id): ?Attribute
    {
       return $this->find($id);
    }
}