<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Places\Domain\PlaceTypeAttribute;
use App\Places\Domain\PlaceTypeAttributeReadStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\QueryException;

final class PlaceTypeAttributeDoctrineRepository extends EntityRepository implements PlaceTypeAttributeReadStorage
{
    /**
     * @return PlaceTypeAttribute[]
     * @throws QueryException
     */
    public function getByPlaceType(int $placeTypeId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('placeTypeId', $placeTypeId))
                )
                ->getQuery()
                ->getResult();
    }
}