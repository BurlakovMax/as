<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence\Mapping;

use App\Places\Domain\BusinessOwner\Status;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class BusinessOwnerStatusType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Status::class;
    }

    public function getName()
    {
        return 'business_owner_status';
    }
}