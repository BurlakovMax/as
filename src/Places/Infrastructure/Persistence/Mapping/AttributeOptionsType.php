<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence\Mapping;

use App\Places\Domain\AttributeOptions;
use LazyLemurs\DoctrineOdmTypes\ObjectArrayType;

final class AttributeOptionsType extends ObjectArrayType
{
    protected function getObjectClass(): string
    {
        return AttributeOptions::class;
    }

    public function getName()
    {
        return 'attribute_options';
    }
}