<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence\Mapping;

use App\Places\Domain\Rating;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class RatingType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Rating::class;
    }

    public function getName()
    {
        return 'rating';
    }
}