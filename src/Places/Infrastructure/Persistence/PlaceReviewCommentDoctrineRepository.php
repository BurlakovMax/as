<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Places\Domain\PlaceReviewComment;
use App\Places\Domain\PlaceReviewCommentReadStorage;
use App\Places\Domain\PlaceReviewCommentWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class PlaceReviewCommentDoctrineRepository extends EntityRepository implements PlaceReviewCommentReadStorage, PlaceReviewCommentWriteStorage
{
    /**
     * @throws ORMException
     */
    public function add(PlaceReviewComment $placeReviewComment): void
    {
        $this->getEntityManager()->persist($placeReviewComment);
    }

    /**
     * @throws ORMException
     */
    public function delete(PlaceReviewComment $placeReviewComment): void
    {
        $this->getEntityManager()->remove($placeReviewComment);
    }

    public function get(int $id): ?PlaceReviewComment
    {
        return $this->find($id);
    }

    /**
     * @return PlaceReviewComment[]
     * @throws QueryException
     */
    public function getByReviewId(int $reviewId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('reviewId', $reviewId))
                )
                ->getQuery()
                ->getResult()
            ;
    }
}