<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Persistence;

use App\Places\Domain\PlaceReviewProvider\PlaceReviewProvider;
use App\Places\Domain\PlaceReviewProvider\PlaceReviewProviderReadStorage;
use App\Places\Domain\PlaceReviewProvider\PlaceReviewProviderWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class PlaceReviewProviderDoctrineRepository extends EntityRepository implements PlaceReviewProviderReadStorage, PlaceReviewProviderWriteStorage
{
    /**
     * @return PlaceReviewProvider[]
     * @throws QueryException
     */
    public function getByPlaceId(int $placeId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('placeId', $placeId))
                )
                ->getQuery()
                ->getResult();
    }

    /**
     * @throws ORMException
     */
    public function create(PlaceReviewProvider $provider): void
    {
        $this->getEntityManager()->persist($provider);
        $this->getEntityManager()->flush();
    }
}