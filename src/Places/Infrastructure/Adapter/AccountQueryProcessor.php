<?php

declare(strict_types=1);

namespace App\Places\Infrastructure\Adapter;

use App\Places\Domain\Account;
use App\Places\Domain\AccountReadStorage;
use App\Security\Application\AccountQueryProcessor as QueryProcessor;

final class AccountQueryProcessor implements AccountReadStorage
{
    private QueryProcessor $queryProcessor;

    public function __construct(QueryProcessor $queryProcessor)
    {
        $this->queryProcessor = $queryProcessor;
    }

    /**
     * @throws \App\Security\Domain\AccountNotFound
     */
    public function getAccount(int $accountId): Account
    {
        $account = $this->queryProcessor->getAccount($accountId);

        return new Account(
            $account->getId(),
            $account->getName(),
            $account->getAvatar()
        );
    }
}
