<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\DomainSupport\Enumerable;

final class Sponsorship extends Enumerable
{
    public static function notSponsored(): self
    {
        return self::createEnum(0);
    }

    public static function sponsored(): self
    {
        return self::createEnum(1);
    }
}