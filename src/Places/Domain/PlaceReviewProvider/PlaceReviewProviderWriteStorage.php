<?php

declare(strict_types=1);

namespace App\Places\Domain\PlaceReviewProvider;

interface PlaceReviewProviderWriteStorage
{
    public function create(PlaceReviewProvider $provider): void;
}