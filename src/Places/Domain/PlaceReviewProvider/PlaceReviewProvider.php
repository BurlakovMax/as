<?php

declare(strict_types=1);

namespace App\Places\Domain\PlaceReviewProvider;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\PlaceReviewProviderDoctrineRepository")
 * @ORM\Table(name="place_review_provider")
 */
class PlaceReviewProvider
{
    /**
     * @ORM\Column(name="provider_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $providerName;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private int $placeId;

    /**
     * @ORM\Column(name="pic", type="integer", length=3)
     */
    private ?int $imageUploadCount;

    public function __construct(string $providerName, int $placeId, ?int $imageUploadCount)
    {
        $this->id = 0;
        $this->providerName = $providerName;
        $this->placeId = $placeId;
        $this->imageUploadCount = $imageUploadCount ?? 0;
    }

    public function increaseImageUploadCount(int $imageUploadCount): void
    {
        $this->imageUploadCount = $this->getImageUploadCount() + $imageUploadCount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->providerName;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getImageUploadCount(): int
    {
        return $this->imageUploadCount;
    }
}