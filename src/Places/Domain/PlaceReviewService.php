<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\Application\Search\FilterQuery;
use App\Core\Application\Search\FuzzySearchQuery;
use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;
use App\Places\Application\PlaceNotFound;
use App\Places\Application\PlaceReviewNotFound;
use DateTimeImmutable;
use Exception;
use LazyLemurs\DomainEvents\PublisherContainer;

final class PlaceReviewService
{
    private const REVIEW_LIMIT = 2;

    private PlaceReviewReadStorage $placeReviewReadStorage;

    private PlaceReviewWriteStorage $placeReviewWriteStorage;

    private PlaceWriteStorage $placeWriteStorage;

    public function __construct(
        PlaceReviewReadStorage $placeReviewReadStorage,
        PlaceReviewWriteStorage $placeReviewWriteStorage,
        PlaceWriteStorage $placeWriteStorage
    ) {
        $this->placeReviewReadStorage = $placeReviewReadStorage;
        $this->placeReviewWriteStorage = $placeReviewWriteStorage;
        $this->placeWriteStorage = $placeWriteStorage;
    }

    /**
     * @throws PlaceNotFound
     * @throws ReviewDailyLimitIsOver
     * @throws Exception
     */
    public function create(PlaceReview $placeReview): void
    {
        if ($this->isValidReviewLimit($placeReview) || $this->isValidateReviewLimitForIp($placeReview)) {
            throw new ReviewDailyLimitIsOver();
        }

        $this->placeReviewWriteStorage->add($placeReview);
        $this
            ->getPlace(
                $placeReview->getPlaceId()
            )
            ->addReviewRating(
                $placeReview->getId(),
                $placeReview->getStar()
            )
        ;

        PublisherContainer::instance()->publish(
            new PlaceReviewCreated(
                $placeReview->getId(),
                $placeReview->getPlaceId(),
                $placeReview->getAccountId(),
                $placeReview->getProviderId(),
                $placeReview->getComment()
            )
        );
    }

    /**
     * @throws PlaceNotFound
     */
    private function getPlace(int $placeId): Place
    {
        $place = $this->placeWriteStorage->getAndLock($placeId);

        if (!$place) {
            throw new PlaceNotFound();
        }

        return $place;
    }

    private function isValidReviewLimit(PlaceReview $placeReview): bool
    {
        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('accountId', $placeReview->getAccountId()),
                FilterQuery::createEqFilter('reviewDate', (new DateTimeImmutable())->format('Y-m-d')),
            ],
            [],
            LimitationQueryImmutable::create(0, 3)
        );

        $placeReviews = $this->placeReviewReadStorage->getListBySearchQuery($search);

        if (count($placeReviews) > self::REVIEW_LIMIT) {
            return true;
        }

        return false;
    }

    private function isValidateReviewLimitForIp(PlaceReview $placeReview): bool
    {
        $search = new SearchQuery(
            new FuzzySearchQuery(null, []),
            [
                FilterQuery::createEqFilter('ip', $placeReview->getIp()),
                FilterQuery::createEqFilter('reviewDate', (new DateTimeImmutable())->format('Y-m-d')),
            ],
            [],
            LimitationQueryImmutable::create(0, 3)
        );

        $placeReviews = $this->placeReviewReadStorage->getListBySearchQuery($search);

        if (count($placeReviews) > self::REVIEW_LIMIT) {
            return true;
        }

        return false;
    }

    /**
     * @throws PlaceNotFound
     * @throws PlaceReviewNotFound
     */
    public function update(
        int $placeReviewId,
        ?int $providerId,
        ?string $comment,
        ?Rating $star
    ): void {

        $placeReview = $this->getAndLockPlaceReview($placeReviewId);

        $this
            ->getPlace(
                $placeReview->getPlaceId()
            )
            ->editReviewRating(
                $placeReview->getStar(),
                $star
            )
        ;

        $placeReview->update($providerId, $comment, $star);
    }

    /**
     * @throws PlaceReviewNotFound
     */
    private function getAndLockPlaceReview(int $placeReviewId): PlaceReview
    {
        $placeReview = $this->placeReviewWriteStorage->getAndLock($placeReviewId);

        if (null === $placeReview) {
            throw new PlaceReviewNotFound();
        }

        return $placeReview;
    }

    /**
     * @throws PlaceReviewNotFound
     */
    public function delete(int $placeReviewId): void
    {
        $placeReview = $this->placeReviewReadStorage->get($placeReviewId);

        if (null === $placeReview) {
            throw new PlaceReviewNotFound();
        }

        $this->placeReviewWriteStorage->delete($placeReview);
    }
}
