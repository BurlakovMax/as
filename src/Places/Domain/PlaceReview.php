<?php

declare(strict_types=1);

namespace App\Places\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\PlaceReviewDoctrineRepository")
 * @ORM\Table(name="place_review")
 */
class PlaceReview
{
    /**
     * @var int
     *
     * @ORM\Column(name="review_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="id", type="integer", length=8)
     */
    private int $placeId;

    /**
     * @ORM\Column(type="integer", length=8)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $providerId;

    /**
     * @ORM\Column(name="reviewdate", type="date_immutable")
     */
    private DateTimeImmutable $reviewDate;

    /**
     * @ORM\Column(type="text")
     */
    private string $comment;

    /**
     * @ORM\Column(type="rating")
     */
    private Rating $star;

    /**
     * @ORM\Column(name="ip_address", type="string", length=50, nullable=true)
     */
    private ?string $ip;

    public function __construct(
        int $placeId,
        int $accountId,
        int $providerId,
        string $comment,
        Rating $star,
        ?string $ip
    ) {
        $this->placeId = $placeId;
        $this->accountId = $accountId;
        $this->providerId = $providerId;
        $this->reviewDate = new DateTimeImmutable();
        $this->comment = $comment;
        $this->star = $star;
        $this->ip = $ip;
    }

    public function update(
        ?int $providerId,
        ?string $comment,
        ?Rating $star
    ): void {

        if (null !== $providerId) {
            $this->providerId = $providerId;
        }

        if (null !== $comment) {
            $this->comment = $comment;
        }

        if (null !== $star) {
            $this->star = $star;
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getProviderId(): int
    {
        return $this->providerId;
    }

    public function getReviewDate(): DateTimeImmutable
    {
        return $this->reviewDate;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getStar(): Rating
    {
        return $this->star;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }
}