<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface PlaceReviewWriteStorage
{
    public function add(PlaceReview $placeReview): void;

    public function getAndLock(int $id): ?PlaceReview;

    public function delete(PlaceReview $placeReview): void;
}