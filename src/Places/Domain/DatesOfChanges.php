<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class DatesOfChanges
{
    /**
     * @ORM\Column(type="timestamp", name="created_stamp", nullable=true)
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="timestamp", name="updated", nullable=true)
     */
    private ?\DateTimeImmutable $updatedAt;

    /**
     * @ORM\Column(type="timestamp", name="deleted", nullable=true)
     */
    private ?\DateTimeImmutable $deletedAt;

    public function __construct(?int $createdAt, ?int $updatedAt, ?int $deletedAt)
    {
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->deletedAt = $deletedAt;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return  $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return  $this->updatedAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function delete(): void
    {
        $this->deletedAt = new \DateTimeImmutable();
    }
}