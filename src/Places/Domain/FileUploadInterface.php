<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileUploadInterface
{
    public function save(string $name, UploadedFile $file): string;
}