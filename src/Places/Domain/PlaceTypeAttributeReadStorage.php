<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Query\QueryException;

interface PlaceTypeAttributeReadStorage
{
    /**
     * @return PlaceTypeAttribute[]
     * @throws QueryException
     */
    public function getByPlaceType(int $placeTypeId): array;
}