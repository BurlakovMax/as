<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface PlaceWriteStorage
{
    public function getAndLock(int $id): ?Place;

    public function create(Place $place): void;

    public function forceDelete(Place $place): void;
}
