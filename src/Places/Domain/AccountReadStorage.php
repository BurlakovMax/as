<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface AccountReadStorage
{
    public function getAccount(int $accountId): Account;
}
