<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\SearchQuery;

interface PlaceReviewReadStorage
{
    /**
     * @return PlaceReview[]
     */
    public function getListBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function get(int $id): ?PlaceReview;

    /**
     * @return PlaceReview[]
     */
    public function getByPlaceId(int $placeId): array;

    public function countAll(): int;
}