<?php

declare(strict_types=1);

namespace App\Places\Domain;

final class Account
{
    private int $id;

    private string $name;

    private ?string $avatar;

    public function __construct(int $id, string $name, ?string $avatar)
    {
        $this->id = $id;
        $this->name = $name;
        $this->avatar = $avatar;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }
}
