<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\Domain\Coordinates;
use App\Core\DomainSupport\TimestampToDateTime;
use App\Places\Application\CreatePlaceCommand;
use App\Places\Application\UpdatePlaceCommand;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\Email;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\PlaceDoctrineRepository")
 * @ORM\Table(name="place")
 */
class Place
{
    use TimestampToDateTime;

    private const NOT_NULL_BY_DEFAULT = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="place_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="place_type_id", type="integer", length=11)
     */
    private ?int $typeId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $name;

    /**
     * @ORM\Column(name="loc_id", type="integer", nullable=true)
     */
    private ?int $locationId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $neighborId = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $locationExactId = null;

    /**
     * @ORM\Embedded(class="App\Places\Domain\Address", columnPrefix=false)
     */
    private ?Address $address = null;

    /**
     * @ORM\Embedded(class="App\Places\Domain\ContactDetails", columnPrefix=false)
     */
    private ?ContactDetails $contact = null;

    /**
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private ?string $webSite;

    /**
     * @ORM\Embedded(class="App\Places\Domain\SocialNetworks", columnPrefix=false)
     */
    private ?SocialNetworks $socialNetworks = null;

    /**
     * @ORM\Embedded(class="App\Core\Domain\Coordinates", columnPrefix=false)
     */
    private ?Coordinates $coordinates = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $image;

    /**
     * @ORM\Column(type="string", length=255, name="thumb", nullable=true)
     */
    private ?string $thumbnail = null;

    /**
     * @ORM\Column(type="integer", length=6)
     */
    private int $review;

    /**
     * @ORM\Column(type="integer", length=6)
     */
    private int $recommend;

    /**
     * @ORM\Column(type="float")
     */
    private float $overall;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $lastReviewId;

    /**
     * @ORM\Column(name="owner", type="integer", length=11)
     */
    private ?int $ownerId = null;

    /**
     * @ORM\Column(type="sponsorship", name="sponsor")
     */
    private Sponsorship $sponsorship;

    /**
     * @ORM\Column(type="editable", name="edit", nullable=true)
     */
    private ?Editable $editable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $fake = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $workerComment = null;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $oldId = null;

    /**
     * @ORM\Embedded(class="App\Places\Domain\DatesOfChanges", columnPrefix=false)
     */
    private ?DatesOfChanges $datesOfChanges = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $test;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $empImported = null;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $empId = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $mapImage = null;

    /**
     * @ORM\Column(name="last_checked_stamp", type="integer", length=11, nullable=true)
     */
    private ?int $lastChecked = null;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $lastCheckedBy = null;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $workerSkippedStamp = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $source = null;

    public function addReviewRating(int $reviewId, Rating $star): void
    {
        $this->review++;
        $this->recommend = $this->getRecommend() + $star->getRawValue();
        $this->lastReviewId = $reviewId;
        $this->calculateOverall();
    }

    public function editReviewRating(Rating $previousStar, Rating $newStar): void
    {
        $this->recommend = $this->getRecommend() - $previousStar->getRawValue() + $newStar->getRawValue();
        $this->calculateOverall();
    }

    private function calculateOverall(): void
    {
        $this->overall = $this->getRecommend() / $this->getReview();
    }

    public function assignOwner(int $businessOwnerId): void
    {
        $this->ownerId = $businessOwnerId;
        $this->sponsorship = Sponsorship::sponsored();
    }

    public function __construct(CreatePlaceCommand $createPlaceCommand)
    {
        $this->typeId = $createPlaceCommand->getTypeId();
        $this->name = $createPlaceCommand->getName();
        $this->locationId = $createPlaceCommand->getLocationId();

        if ($createPlaceCommand->getAddress() !== null) {
            $this->address = new Address(
                $createPlaceCommand->getAddress1(),
                $createPlaceCommand->getAddress2(),
                $createPlaceCommand->getZipCode()
            );
        }

        if ($createPlaceCommand->getContactDetails() !== null) {
            $this->contact = new ContactDetails(
                $createPlaceCommand->getPhone1(),
                $createPlaceCommand->getPhone2(),
                $createPlaceCommand->getEmail(),
                $createPlaceCommand->getFax()
            );
        }

        $this->webSite = $createPlaceCommand->getWebSite();

        if ($createPlaceCommand->getSocialNetworks() !== null) {
            $this->socialNetworks = new SocialNetworks(
                $createPlaceCommand->getFacebook(),
                $createPlaceCommand->getTwitter(),
                $createPlaceCommand->getInstagram(),
                $createPlaceCommand->getWhatsapp(),
                $createPlaceCommand->getLine(),
                $createPlaceCommand->getWechat(),
                $createPlaceCommand->getTelegram()
            );
        }

        $this->review = self::NOT_NULL_BY_DEFAULT;
        $this->recommend = self::NOT_NULL_BY_DEFAULT;
        $this->lastReviewId = self::NOT_NULL_BY_DEFAULT;
        $this->overall = self::NOT_NULL_BY_DEFAULT;
        $this->sponsorship = Sponsorship::notSponsored();
        $this->editable = Editable::newlyAdded();
        $this->image = $createPlaceCommand->getImage();
        $this->thumbnail = $createPlaceCommand->getThumbnail() ?? $createPlaceCommand->getImage();
        $this->test = false;
    }

    public function update(UpdatePlaceCommand $updatePlaceCommand): void
    {
        $this->name = $updatePlaceCommand->getName() ?? $this->getName();

        $this->locationId = $updatePlaceCommand->getLocationId() ?? $this->getLocationId();

        if ($updatePlaceCommand->getAddress() !== null) {
            $this->address = new Address(
                $updatePlaceCommand->getAddress1() ?? $this->getAddress1(),
                $updatePlaceCommand->getAddress2() ?? $this->getAddress2(),
                $updatePlaceCommand->getZipCode() ?? $this->getZipCode()
            );
        }

        if ($updatePlaceCommand->getContactDetails() !== null) {
            $this->contact = new ContactDetails(
                $updatePlaceCommand->getPhone1() ?? $this->getPhone1(),
                $updatePlaceCommand->getPhone2() ?? $this->getPhone2(),
                $updatePlaceCommand->getEmail() ?? $this->getEmail(),
                $updatePlaceCommand->getFax() ?? $this->getFax()
            );
        }

        $this->webSite = $updatePlaceCommand->getWebSite() ?? $this->getWebSite();

        if ($updatePlaceCommand->getSocialNetworks() !== null) {
            $this->socialNetworks = new SocialNetworks(
                $updatePlaceCommand->getFacebook() ?? $this->getFacebook(),
                $updatePlaceCommand->getTwitter() ?? $this->getTwitter(),
                $updatePlaceCommand->getInstagram() ?? $this->getInstagram(),
                $updatePlaceCommand->getWhatsapp() ?? $this->getWhatsapp(),
                $updatePlaceCommand->getLine() ?? $this->getLine(),
                $updatePlaceCommand->getWechat() ?? $this->getWechat(),
                $updatePlaceCommand->getTelegram() ?? $this->getTelegram()
            );
        }

        $this->image = $updatePlaceCommand->getImage();
    }

    public function delete(): void
    {
        $this->editable = Editable::requestedToBeRemove();
        $this->datesOfChanges->delete();
    }

    public function checkedSuccess(DateTimeImmutable $checkedTime, int $accountId): void
    {
        $this->lastChecked = $checkedTime->getTimestamp();
        $this->lastCheckedBy = $accountId;
    }

    public function phoneNotAnswered(DateTimeImmutable $skippedTime, int $accountId): void
    {
        $this->workerComment = 'Phone not answered - WorkerId:' . $accountId;
        $this->workerSkippedTime = $skippedTime->getTimestamp();
    }

    public function skipped(DateTimeImmutable $skippedTime, int $accountId): void
    {
        $this->workerComment = 'Skipped - WorkerId:' . $accountId;
        $this->workerSkippedTime = $skippedTime->getTimestamp();
    }

    public function removeImage(): void
    {
        $this->image = null;
        $this->thumbnail = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getLocationId(): ?int
    {
        return $this->locationId;
    }

    public function getNeighborId(): ?int
    {
        return $this->neighborId;
    }

    public function getLocationExactId(): ?int
    {
        return $this->locationExactId;
    }

    public function getAddress1(): ?string
    {
        return $this->getAddress()->getAddress1();
    }

    public function getAddress2(): ?string
    {
        return $this->getAddress()->getAddress2();
    }

    public function getZipCode(): ?string
    {
        return $this->getAddress()->getZipCode();
    }

    private function getAddress(): ?Address
    {
        return $this->address;
    }

    public function getPhone1(): ?string
    {
        return $this->getContact()->getPhone1();
    }

    public function getPhone2(): ?string
    {
        return $this->getContact()->getPhone2();
    }

    public function getFax(): ?string
    {
        return $this->getContact()->getFax();
    }

    public function getEmail(): ?Email
    {
        return $this->getContact()->getEmail();
    }

    private function getContact(): ContactDetails
    {
        return $this->contact;
    }

    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    public function getFacebook(): ?string
    {
        return $this->getSocialNetworks()->getFacebook();
    }

    public function getTwitter(): ?string
    {
        return $this->getSocialNetworks()->getTwitter();
    }

    public function getInstagram(): ?string
    {
        return $this->getSocialNetworks()->getInstagram();
    }

    public function getWhatsapp(): ?string
    {
        return $this->getSocialNetworks()->getWhatsapp();
    }

    public function getTelegram(): ?string
    {
        return $this->getSocialNetworks()->getTelegram();
    }

    public function getLine(): ?string
    {
        return $this->getSocialNetworks()->getLine();
    }

    public function getWechat(): ?string
    {
        return $this->getSocialNetworks()->getWechat();
    }

    private function getSocialNetworks(): SocialNetworks
    {
        return $this->socialNetworks;
    }

    public function getLatitude(): ?string
    {
        return $this->getCoordinates()->getLatitude();
    }

    public function getLongitude(): ?string
    {
        return $this->getCoordinates()->getLongitude();
    }

    private function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function getReview(): int
    {
        return $this->review;
    }

    public function getRecommend(): int
    {
        return $this->recommend;
    }

    public function getOverall(): float
    {
        return $this->overall;
    }

    public function getLastReviewId(): int
    {
        return $this->lastReviewId;
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    public function getSponsorship(): Sponsorship
    {
        return $this->sponsorship;
    }

    public function getEditable(): ?Editable
    {
        return $this->editable;
    }

    public function getFake(): ?bool
    {
        return $this->fake;
    }

    public function getWorkerComment(): ?string
    {
        return $this->workerComment;
    }

    public function getOldId(): ?int
    {
        return $this->oldId;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->getDatesOfChanges()->getCreatedAt();
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->getDatesOfChanges()->getUpdatedAt();
    }

    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->getDatesOfChanges()->getDeletedAt();
    }

    private function getDatesOfChanges(): DatesOfChanges
    {
        return $this->datesOfChanges;
    }

    public function isTest(): bool
    {
        return $this->test;
    }

    public function getEmpImported(): ?bool
    {
        return $this->empImported;
    }

    public function getEmpId(): ?int
    {
        return $this->empId;
    }

    public function getMapImage(): ?string
    {
        return $this->mapImage;
    }

    public function getLastChecked(): ?DateTimeImmutable
    {
        return $this->timestampToDateTime($this->lastChecked);
    }

    public function getLastCheckedBy(): ?int
    {
        return $this->lastCheckedBy;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }
}
