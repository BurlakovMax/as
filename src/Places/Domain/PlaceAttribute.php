<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Places\Application\PlaceAttributeCommand;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\PlaceAttributeDoctrineRepository")
 * @ORM\Table(name="place_attribute")
 */
class PlaceAttribute
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $placeId;

    /**
     * @ORM\OneToOne(targetEntity="App\Places\Domain\Attribute", cascade={"persist"})
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="attribute_id")
     */
    private Attribute $attribute;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $value;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $valueText;

    public function __construct(int $placeId, Attribute $attribute, ?string $value, ?string $valueText)
    {
        $this->id = 0;
        $this->placeId   = $placeId;
        $this->attribute = $attribute;
        $this->value = $value;
        $this->valueText = $valueText;
    }

    public function update(PlaceAttributeCommand $placeAttributeCommand): void
    {
        if ($placeAttributeCommand->getValue() !== null) {
            $this->value = $placeAttributeCommand->getValue();
        }

        if ($placeAttributeCommand->getText() !== null) {
            $this->valueText = $placeAttributeCommand->getText();
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAttributeId(): int
    {
        return $this->getAttribute()->getId();
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getName(): string
    {
        return $this->getAttribute()->getName();
    }

    public function getTitle(): string
    {
        return $this->getAttribute()->getTitle();
    }

    public function getType(): ?string
    {
        return $this->getAttribute()->getDataType();
    }

    public function getOptions(): ?string
    {
        return $this->getAttribute()->getOptions();
    }

    public function getValue(): ?string
    {
        if ($this->getAttribute()->isTextType()) {
            return $this->valueText;
        }

        return $this->value;
    }

    private function getAttribute(): Attribute
    {
        return $this->attribute;
    }
}