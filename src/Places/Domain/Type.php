<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\DomainSupport\Enumerable;

final class Type extends Enumerable
{
    public static function storeFront(): self
    {
        return self::createEnum(1);
    }

    public static function apartment(): self
    {
        return self::createEnum(2);
    }

    public static function house(): self
    {
        return self::createEnum(3);
    }
}