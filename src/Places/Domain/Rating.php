<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Core\DomainSupport\Enumerable;

final class Rating extends Enumerable
{
    public static function zeroStar(): self
    {
        return self::createEnum(0);
    }

    public static function oneStar(): self
    {
        return self::createEnum(1);
    }

    public static function twoStars(): self
    {
        return self::createEnum(2);
    }

    public static function threeStars(): self
    {
        return self::createEnum(3);
    }

    public static function fourStars(): self
    {
        return self::createEnum(4);
    }

    public static function fiveStars(): self
    {
        return self::createEnum(5);
    }
}