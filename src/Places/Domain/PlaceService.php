<?php

declare(strict_types=1);

namespace App\Places\Domain;

use App\Places\Application\PlaceNotFound;
use App\Places\Application\UpdatePlaceCommand;
use App\Places\Domain\BusinessOwner\BusinessOwner;
use App\Places\Domain\BusinessOwner\BusinessOwnerWriteStorage;

final class PlaceService
{
    private PlaceWriteStorage $placeWriteStorage;

    private PlaceReadStorage $placeReadStorage;

    private BusinessOwnerWriteStorage $businessOwnerWriteStorage;

    public function __construct(
        PlaceWriteStorage $placeWriteStorage,
        PlaceReadStorage $placeReadStorage,
        BusinessOwnerWriteStorage $businessOwnerWriteStorage
    )
    {
        $this->placeWriteStorage = $placeWriteStorage;
        $this->placeReadStorage  = $placeReadStorage;
        $this->businessOwnerWriteStorage = $businessOwnerWriteStorage;
    }

    public function create(Place $place): int
    {
        $this->placeWriteStorage->create($place);

        return $place->getId();
    }

    /**
     * @throws PlaceNotFound
     */
    public function update(UpdatePlaceCommand $updatePlaceCommand): int
    {
        $place = $this->getAndLock($updatePlaceCommand->getId());

        $place->update($updatePlaceCommand);

        return $place->getId();
    }

    public function assignOwner(int $placeId, int $ownerId, int $recurringPeriod): void
    {
        $this->getAndLock($placeId)->assignOwner($ownerId);
        $this->businessOwnerWriteStorage->add(
            new BusinessOwner($placeId, $ownerId, $recurringPeriod)
        );
    }

    /**
     * @throws PlaceNotFound
     */
    public function delete(int $id): void
    {
        $place = $this->placeReadStorage->getAndLock($id);

        $place->delete();
    }

    /**
     * @throws PlaceNotFound
     */
    public function forceDelete(int $id): void
    {
        $place = $this->getAndLock($id);

        $this->placeWriteStorage->forceDelete($place);
    }

    public function checkedSuccess(int $placeId, int $accountId): void
    {
        $this->getAndLock($placeId)->checkedSuccess(new \DateTimeImmutable(), $accountId);
    }

    public function skipped(int $placeId, int $accountId): void
    {
        $this->getAndLock($placeId)->skipped(new \DateTimeImmutable(), $accountId);
    }

    public function phoneNotAnswered(int $placeId, int $accountId): void
    {
        $this->getAndLock($placeId)->phoneNotAnswered(new \DateTimeImmutable(), $accountId);
    }

    /**
     * @throws PlaceNotFound
     */
    public function removeImage(int $id): void
    {
        $this->getAndLock($id)->removeImage();
    }

    private function getAndLock(int $id): Place
    {
        $place = $this->placeReadStorage->getAndLock($id);

        if (!$place) {
            throw new PlaceNotFound();
        }

        return $place;
    }
}
