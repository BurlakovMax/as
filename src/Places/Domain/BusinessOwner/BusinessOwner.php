<?php

declare(strict_types=1);

namespace App\Places\Domain\BusinessOwner;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\BusinessOwnerDoctrineRepository")
 * @ORM\Table(name="account_businessowner")
 */
class BusinessOwner
{
    /**
     * @ORM\Column(name="provider_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $placeId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(name="starts", type="datetime_immutable")
     */
    private \DateTimeImmutable $startAt;

    /**
     * @ORM\Column(name="expires", type="datetime_immutable")
     */
    private \DateTimeImmutable $expireAt;

    /**
     * @ORM\Column(name="day", type="integer", length=11)
     */
    private int $countInDays;

    /**
     * @ORM\Column(type="business_owner_status")
     */
    private Status $status;

    public function __construct(int $placeId, int $accountId, int $countInDays)
    {
        $this->id = 0;
        $this->placeId = $placeId;
        $this->accountId = $accountId;
        $this->startAt = new \DateTimeImmutable();
        $this->expireAt = new \DateTimeImmutable('+' . $countInDays . ' days');
        $this->countInDays = $countInDays;
        $this->status = Status::active();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlaceId(): int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getStartAt(): \DateTimeImmutable
    {
        return $this->startAt;
    }

    public function getExpireAt(): \DateTimeImmutable
    {
        return $this->expireAt;
    }

    public function getCountInDays(): int
    {
        return $this->countInDays;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }
}