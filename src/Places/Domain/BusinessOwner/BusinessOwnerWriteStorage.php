<?php

declare(strict_types=1);

namespace App\Places\Domain\BusinessOwner;

interface BusinessOwnerWriteStorage
{
    public function add(BusinessOwner $businessOwner): void;
}