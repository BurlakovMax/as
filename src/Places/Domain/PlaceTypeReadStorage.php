<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface PlaceTypeReadStorage
{
    public function get(int $id): ?PlaceType;

    /**
     * @return PlaceType[]
     */
    public function getAll(): array;

    /**
     * @param int[] $ids
     * @return PlaceType[]
     */
    public function getByIds(array $ids): array;

    public function getOneByUrl(string $url): ?PlaceType;
}