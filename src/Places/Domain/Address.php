<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class Address
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $address2;

    /**
     * @ORM\Column(name="zipcode", type="string", length=15, nullable=true)
     */
    private ?string $zipCode;

    public function __construct(?string $address1, ?string $address2, ?string $zipCode)
    {
        $this->address1 = $address1;
        $this->address2 = $address2;
        $this->zipCode  = $zipCode;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }
}