<?php

declare(strict_types=1);

namespace App\Places\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Places\Infrastructure\Persistence\PlaceTypeAttributeDoctrineRepository")
 * @ORM\Table(name="place_type_attributes")
 */
class PlaceTypeAttribute
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", length=11)
     */
    private int $placeTypeId;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", length=11)
     */
    private int $attributeId;

    /**
     * @var AttributeOptions[]
     *
     * @ORM\Column(type="attribute_options")
     */
    private array $options;

    public function getPlaceTypeId(): int
    {
        return $this->placeTypeId;
    }

    public function getAttributeId(): int
    {
        return $this->attributeId;
    }

    /**
     * @return AttributeOptions[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}