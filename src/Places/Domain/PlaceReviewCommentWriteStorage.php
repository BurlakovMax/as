<?php

declare(strict_types=1);

namespace App\Places\Domain;

interface PlaceReviewCommentWriteStorage
{
    public function add(PlaceReviewComment $placeReviewComment): void;

    public function delete(PlaceReviewComment $placeReviewComment): void;
}