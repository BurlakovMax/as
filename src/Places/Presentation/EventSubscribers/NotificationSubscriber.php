<?php

declare(strict_types=1);

namespace App\Places\Presentation\EventSubscribers;

use App\EmailGate\Domain\EmailData;
use App\Places\Domain\EmailProvider;
use App\Places\Domain\PlaceReviewCreated;
use LazyLemurs\DomainEvents\DomainEvent;
use LazyLemurs\DomainEvents\DomainEventSubscriber;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;
use Throwable;

final class NotificationSubscriber implements DomainEventSubscriber
{
    private EmailProvider $emailProvider;

    private Email $fromEmail;

    private Email $toEmail;

    public function __construct(EmailProvider $emailProvider, string $fromEmail, string $toEmail)
    {
        $this->emailProvider = $emailProvider;
        $this->fromEmail = new Email($fromEmail);
        $this->toEmail = new Email($toEmail);
    }

    /**
     * @return string[] Names of event's classes.
     */
    public function getListenedEventClasses(): array
    {
        return [
            PlaceReviewCreated::class,
        ];
    }

    /**
     * @throws Throwable
     */
    public function handleEvent(DomainEvent $event): void
    {
        switch (true) {
            case $event instanceof PlaceReviewCreated:
                $this->emailProvider->sendSync(
                    new EmailData(
                        Uuid::uuid4(),
                        $this->fromEmail,
                        $this->toEmail,
                        'PlaceReviewId:' . $event->getId() .
                        ', PlaceId: ' . $event->getPlaceId() .
                        ', AccountId: ' . $event->getAccountId() .
                        ', ProviderId: ' . $event->getProviderId() .
                        ', Comment: ' . $event->getComment(),
                        'New review'
                    )
                );
                break;
            default:
                throw new \RuntimeException("Event not supported. " . get_class($event));
        }
    }
}
