<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200116160208 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration add level and weight fields';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->getTable('location_location');

        $table->addColumn('level', 'integer', ['notnull' => true, 'default' => 0]);
        $table->addColumn('weight', 'integer', ['notnull' => true, 'default' => 0]);
    }

    public function down(Schema $schema): void
    {
        $table = $schema->getTable('location_location');

        $table->dropColumn('level');
        $table->dropColumn('weight');
    }
}
