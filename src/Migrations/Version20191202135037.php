<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191202135037 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('TRUNCATE TABLE email_gate_message');
        $this->addSql('ALTER TABLE email_gate_message DROP COLUMN `email_sender`');
        $this->addSql('ALTER TABLE email_gate_message DROP COLUMN `email_to`');
        $this->addSql('ALTER TABLE email_gate_message DROP COLUMN `subject`');
        $this->addSql('ALTER TABLE email_gate_message DROP COLUMN `body`');
        $this->addSql('ALTER TABLE email_gate_message ADD COLUMN `details` TEXT NOT NULL AFTER `message_id`');
    }

    public function down(Schema $schema): void
    {
    }
}
