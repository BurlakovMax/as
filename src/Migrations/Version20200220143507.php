<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200220143507 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE account_confirmation ADD value VARCHAR(255) DEFAULT NULL;');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE account_confirmation DROP COLUMN value');
    }
}
