<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200412100624 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add customerId and cardId to CreditCard';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('account_cc');
        $table->addColumn(
            'customer_id',
            'string',
            ['notnull' => false, 'length' => 36, 'comment' => 'Customer ID from payment gateway']
        );
        $table->addColumn(
            'card_id',
            'string',
            ['notnull' => false, 'length' => 36, 'comment' => 'Credit card ID from payment gateway']
        );
    }

    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('account_cc');
        $table->dropColumn('customer_id');
        $table->dropColumn('card_id');
    }
}
