<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200405083012 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE payment MODIFY last_operation varchar(36) NULL;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE payment MODIFY last_operation binary(16) NULL;');
    }
}
