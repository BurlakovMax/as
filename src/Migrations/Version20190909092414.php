<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190909092414 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE place SET address2 = NULL WHERE address2 = ""');
        $this->addSql('UPDATE place SET phone1 = NULL WHERE phone1 = ""');
        $this->addSql('UPDATE place SET phone2 = NULL WHERE phone2 = ""');
        $this->addSql('UPDATE place SET fax = NULL WHERE fax = ""');
        $this->addSql('UPDATE place SET email = NULL WHERE email = ""');
        $this->addSql('UPDATE place SET website = NULL WHERE website = ""');
        $this->addSql('UPDATE place SET facebook = NULL WHERE facebook = ""');
        $this->addSql('UPDATE place SET twitter = NULL WHERE twitter = ""');
        $this->addSql('UPDATE place SET instagram = NULL WHERE instagram = ""');
    }

    public function down(Schema $schema) : void
    {

    }
}
