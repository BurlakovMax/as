<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905151430 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE classifieds SET email = NULL WHERE email = ""');
        $this->addSql('UPDATE classifieds SET email = "Sexy51190@gmail.com" WHERE email = "Sexy51190&gmail."');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
