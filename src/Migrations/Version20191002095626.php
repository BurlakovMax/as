<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191002095626 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql(<<<sql
CREATE TABLE `account_session` (
    `token` char(32) NOT NULL DEFAULT '',
    `account_id` int(11) NOT NULL,
    `expires_at` int(11) NOT NULL,
    PRIMARY KEY (`token`),
    FOREIGN KEY (`account_id`)
        REFERENCES `account`(`account_id`)
        ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
sql
);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE account_session');
    }
}
