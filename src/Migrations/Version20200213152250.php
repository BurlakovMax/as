<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200213152250 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            UPDATE location_location SET full_url = loc_url
            WHERE loc_type = 3 AND country_id IN (16046,16047,41973,41756)'
        );
        $this->addSql('
            UPDATE location_location SET full_url = CONCAT(\'/\', country_sub, loc_url) 
            WHERE loc_type = 3 AND country_id NOT IN (16046,16047,41973,41756);
        ');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
