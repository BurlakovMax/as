<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200212100254 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add new field fo location_location to save new url';
    }

    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('location_location');
        $table->addColumn('full_url', 'string', ['notnull' => true, 'default' => '/']);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
