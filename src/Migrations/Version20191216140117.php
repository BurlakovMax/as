<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191216140117 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE classifieds_link_token (
            `classified_id` int(11) NOT NULL,
            `token` binary(16),
             PRIMARY KEY (classified_id, token)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down(Schema $schema) : void
    {
    }
}
