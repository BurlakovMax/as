<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191008134124 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE event (
          position int(11) NOT NULL AUTO_INCREMENT,
          id binary(16) NOT NULL, 
          name varchar(1023) NOT NULL, 
          published_at int(11)NOT NULL, 
          payload JSON NOT NULL, 
          PRIMARY KEY(position)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->addSql('CREATE INDEX ix_event_name ON event (name)');
        $this->addSql('CREATE INDEX ix_event_published_at ON event (published_at)');
        $this->addSql('CREATE UNIQUE INDEX uq_event_id ON event (id)');
        $this->addSql('CREATE TABLE event_consumer_position (
          consumer_name varchar(1023) NOT NULL, 
          value bigint(4) NOT NULL, 
          PRIMARY KEY(consumer_name)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down(Schema $schema) : void
    {

    }
}
