<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191029132725 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Set default value for classifieds.loc_id to 0 as it not used anymore';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `classifieds` MODIFY `loc_id` int(11) NOT NULL DEFAULT 0;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `classifieds` MODIFY `loc_id` int(11) NOT NULL;');
    }
}
