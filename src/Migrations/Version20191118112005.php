<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191118112005 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 48,
                                                                                   \'[{
                                                                                       "title": "Fetish",
                                                                                       "type": "bool",
                                                                                           "options": [
                                                                                           "Yes", "No"
                                                                                       ]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 49,
                                                                                   \'[{
                                                                                     "title": "Lingerie",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 50,
                                                                                   \'[{
                                                                                     "title": "Adult Toys",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 51,
                                                                                   \'[{
                                                                                     "title": "Adult DVDs",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 52,
                                                                                   \'[{
                                                                                     "title": "Peep Shows",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 53,
                                                                                   \'[{
                                                                                     "title": "Adult Books",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 54,
                                                                                   \'[{
                                                                                     "title": "Adult Video Arcade",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 55,
                                                                                   \'[{
                                                                                     "title": "Theaters",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 67,
                                                                                   \'[{
                                                                                     "title": "Parking",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 142,
                                                                                   \'[{
                                                                                     "title": "Glory Hole",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 145,
                                                                                   \'[{
                                                                                     "title": "LGBT",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes", "No"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 146,
                                                                                   \'[{
                                                                                     "title": "Buddy Booth Available",
                                                                                     "type": "bool",
                                                                                     "options": [
                                                                                       "Yes"
                                                                                     ]
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (2, 148,
                                                                                   \'[{
                                                                                     "title": "Coupon",
                                                                                     "type": "upload",
                                                                                     "options": []
                                                                                   }]\');
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM place_type_attributes where place_type_id = 2;');
    }
}
