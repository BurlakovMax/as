<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200609204418 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE account_businessowner MODIFY c SMALLINT(11) NULL;');
        $this->addSql('ALTER TABLE account_businessowner MODIFY active SMALLINT(4) NULL;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE account_businessowner MODIFY c SMALLINT(11) NOT NULL;');
        $this->addSql('ALTER TABLE account_businessowner MODIFY active SMALLINT(4) NOT NULL;');
    }
}
