<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200122152108 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Fix email in Place table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE place SET email = "aquafells2019@gmail.com" WHERE place_id = 41719;');
        $this->addSql('UPDATE place SET email = REPLACE(email, \' \', \'\')');
    }

    public function down(Schema $schema) : void
    {
    }
}
