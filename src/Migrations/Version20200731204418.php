<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200731204418 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE classifieds ADD telegram VARCHAR(255) NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE classifieds DROP telegram');
    }
}
