<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191009111652 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE email_gate_message (
          message_id binary(16) NOT NULL, 
          email_sender VARCHAR(255) NOT NULL, 
          email_to VARCHAR(255) NOT NULL, 
          subject VARCHAR(255) NOT NULL, 
          body TEXT NOT NULL, 
          sent_at int(11) DEFAULT NULL, 
          count int(11) NOT NULL, 
          next_attempt_at int(11) NOT NULL, 
          PRIMARY KEY(message_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE email_gate_message');
    }
}
