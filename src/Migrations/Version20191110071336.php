<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110071336 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `forum_topic` MODIFY `topic_last_post_time` int(10) DEFAULT NULL;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `forum_topic` MODIFY `topic_last_post_time` int(10) NOT NULL;');
    }
}
