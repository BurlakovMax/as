<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200819111129 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE place ADD telegram VARCHAR(255) NULL');
        $this->addSql('ALTER TABLE classifieds ADD wechat VARCHAR(255) NULL');
        $this->addSql('ALTER TABLE classifieds ADD whatsapp VARCHAR(255) NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE place DROP telegram');
        $this->addSql('ALTER TABLE classifieds DROP wechat');
        $this->addSql('ALTER TABLE classifieds DROP whatsapp');
    }
}