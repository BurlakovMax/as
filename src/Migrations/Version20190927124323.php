<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190927124323 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add column "has_states" to table "location_location" and set it to 1 for US, CA and UK';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE location_location ADD COLUMN has_states TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('UPDATE location_location SET has_states = 1 WHERE loc_id IN (16046, 16047, 41973)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE location_location DROP COLUMN has_states');
    }
}
