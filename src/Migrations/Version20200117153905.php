<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200117153905 extends AbstractMigration
{
    private int $europeId = 1000003;

    private int $latinId = 1000004;

    private int $middleId = 1000005;

    private int $asiaId = 1000006;

    private int $africaId = 1000007;

    private int $usaId = 16046;

    private int $canadaId = 16047;

    private int $ukId = 41973;

    private int $australiaId = 1000008;

    private array $europeIds = [
        41744,
        41757,
        41763,
        41764,
        41770,
        41776,
        41796,
        41799,
        41800,
        41810,
        41812,
        41816,
        41817,
        41823,
        41826,
        41841,
        41842,
        41850,
        41863,
        41868,
        41869,
        41870,
        41872,
        41878,
        41886,
        41887,
        41889,
        41897,
        41907,
        41918,
        41919,
        41923,
        41924,
        41936,
        41940,
        41941,
        41947,
        41953,
        41954,
        41971,
        41993,
        100559,
    ];
    private array $asiaIds = [
        41779,
        41787,
        41843,
        41844,
        41845,
        41846,
        41852,
        41858,
        41858,
        41871,
        41875,
        41888,
        41893,
        41895,
        41896,
        41899,
        41909,
        41916,
        41937,
        41939,
        41956,
        41959,
        41979,
        99903,
    ];

    private array $africaIds = [
        41780,
        41785,
        41795,
        41801,
        41806,
        41809,
        41811,
        41856,
        41866,
        41873,
        41882,
        41891,
        41902,
        41903,
        41944,
    ];

    private array $australiaIds = [
        41756,
        41788,
        41793,
        41804,
        41815,
        41819,
        41830,
        41857,
        41885,
        41900,
    ];

    private array $middleIds = [
        41798,
        41849,
        41854,
        41908,
        41921,
        41934,
        41966,
        41972,
    ];

    private array $latinIds = [
        41752,
        41759,
        41767,
        41769,
        41773,
        41773,
        41786,
        41790,
        41794,
        41797,
        41803,
        41805,
        41807,
        41818,
        41835,
        41836,
        41839,
        41851,
        41884,
        41901,
        41912,
        41915,
        41920,
        41964,
        41975,
        41978,
        41980,
        41981,
        41996,
        41997,
        100237,
    ];

    public function getDescription() : string
    {
        return 'Migration connects countries and parts of the world';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE location_location SET level = 0, weight = 100 where loc_id = :usaId', ['usaId' => $this->usaId]);
        $this->addSql('UPDATE location_location SET level = 0, weight = 200 where loc_id = :canadaId', ['canadaId' => $this->canadaId]);
        $this->addSql('UPDATE location_location SET level = 0, weight = 300 where loc_id = :ukId', ['ukId' => $this->ukId]);

        $this->addSql(
            'INSERT INTO location_location(loc_id, loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, weight, level) 
                            VALUES (:europeId, 0, 0, 0, 0, \'EU\', \'Europe\', \'/europe\', \'eu\', \'/eu/\', 400, 0)',
            ['europeId' => $this->europeId]
        );
        $this->addSql(
            'UPDATE location_location SET loc_parent = :europeId WHERE loc_id IN (' . implode(',', $this->europeIds) . ')',
            ['europeId' => $this->europeId]
        );

        $this->addSql(
            'INSERT INTO location_location(loc_id, loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, weight, level) 
                            VALUES (:latinId, 0, 0, 0, 0, \'LC\', \'Latin America & Caribbean\', \'/latin\', \'lc\', \'/lc/\', 500, 0)',
            ['latinId' => $this->latinId]
        );
        $this->addSql(
            'UPDATE location_location SET loc_parent = :latinId WHERE loc_id IN (' . implode(',', $this->latinIds) . ')',
            ['latinId' => $this->latinId]
        );

        $this->addSql(
            'INSERT INTO location_location(loc_id, loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, weight, level) 
                            VALUES (:middleId, 0, 0, 0, 0, \'ME\', \'The Middle East\', \'/middle\', \'me\', \'/me/\', 600, 0)',
            ['middleId' => $this->middleId]
        );
        $this->addSql(
            'UPDATE location_location SET loc_parent = :middleId WHERE loc_id IN (' . implode(',', $this->middleIds) . ')',
            ['middleId' => $this->middleId]
        );

        $this->addSql(
            'INSERT INTO location_location(loc_id, loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, weight, level) 
                            VALUES (:asiaId, 0, 0, 0, 0, \'AS\', \'Asia\', \'/asia\', \'as\', \'/as/\', 700, 0)',
            ['asiaId' => $this->asiaId]
        );
        $this->addSql(
            'UPDATE location_location SET loc_parent = :asiaId WHERE loc_id IN (' . implode(',', $this->asiaIds) . ')',
            ['asiaId' => $this->asiaId]
        );

        $this->addSql(
            'INSERT INTO location_location(loc_id, loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, weight, level) 
                            VALUES (:africaId, 0, 0, 0, 0, \'AF\', \'Africa\', \'/africa\', \'af\', \'/af/\', 800, 0)',
            ['africaId' => $this->africaId]
        );
        $this->addSql(
            'UPDATE location_location SET loc_parent = :africaId WHERE loc_id IN (' . implode(',', $this->africaIds) . ')',
            ['africaId' => $this->africaId]
        );

        $this->addSql(
            'INSERT INTO location_location(loc_id, loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, weight, level) 
                            VALUES (:australiaId, 0, 0, 0, 0, \'AU\', \'Australia & Oceania\', \'/australia\', \'au\', \'/au/\', 900, 0)',
            ['australiaId' => $this->australiaId]
        );
        $this->addSql(
            'UPDATE location_location SET loc_parent = :australiaId WHERE loc_id IN (' . implode(',', $this->australiaIds) . ')',
            ['australiaId' => $this->australiaId]
        );

        $this->addSql('UPDATE location_location SET level = 1 WHERE loc_type = 1 AND loc_id NOT IN (' . implode(',', [
                          $this->usaId, $this->canadaId, $this->ukId
                      ]) . ')');

        $this->addSql('UPDATE location_location SET level = 2 WHERE loc_type = 2');

        $this->addSql('UPDATE location_location SET level = 3 WHERE loc_type = 3');

        $this->addSql('UPDATE location_location SET level = 4 WHERE loc_type = 4');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql(
            'DELETE FROM location_location WHERE loc_id IN (' . implode(',', [
                $this->europeId,
                $this->latinId,
                $this->middleId,
                $this->asiaId,
                $this->africaId,
                $this->australiaId,
            ]) . ')');

        $this->addSql(
            'UPDATE location_location SET loc_parent = 0 WHERE loc_id IN ('. implode(',', $this->europeIds) . ')');
        $this->addSql(
            'UPDATE location_location SET loc_parent = 0 WHERE loc_id IN (' . implode(',', $this->latinIds) . ')');
        $this->addSql(
            'UPDATE location_location SET loc_parent = 0 WHERE loc_id IN (' . implode(',', $this->middleIds) . ')');
        $this->addSql(
            'UPDATE location_location SET loc_parent = 0 WHERE loc_id IN (' . implode(',', $this->asiaIds) . ')');
        $this->addSql(
            'UPDATE location_location SET loc_parent = 0 WHERE loc_id IN (' . implode(',', $this->africaIds) . ')');
        $this->addSql(
            'UPDATE location_location SET loc_parent = 0 WHERE loc_id IN (' . implode(',', $this->australiaIds) . ')');
    }
}
