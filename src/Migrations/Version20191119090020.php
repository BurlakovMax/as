<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119090020 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 18,
                                                                                   \'[{
                                                                                       "title": "Specify payments taken",
                                                                                       "type": "bool",
                                                                                           "options":["Electronic Cash"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 30,
                                                                                   \'[{
                                                                                       "title": "Specify payments taken",
                                                                                       "type": "bool",
                                                                                           "options":["American Express"]
                                                                                       }]\');      
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 31,
                                                                                   \'[{
                                                                                       "title": "Specify payments taken",
                                                                                       "type": "bool",
                                                                                           "options":["Discover Card"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 63,
                                                                                   \'[{
                                                                                       "title": "Specify payments taken",
                                                                                       "type": "bool",
                                                                                           "options":["JCB"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 78,
                                                                                   \'[{
                                                                                       "title": "Specify payments taken",
                                                                                       "type": "bool",
                                                                                           "options":["Visa"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 79,
                                                                                   \'[{
                                                                                       "title": "Specify payments taken",
                                                                                       "type": "bool",
                                                                                           "options":["Mastercard"]
                                                                                       }]\');      
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 21,
                                                                                   \'[{
                                                                                       "title": "Rate for 15 minutes",
                                                                                       "type": "select",
                                                                                           "options":[
"Free",500,1000,1500,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500, 9000,9500,10000,10500,11000,11500,
12000,12500,13000,13500,14000,14500,15000,15500,16000,16500,17000,17500,18000,18500,19000,19500,20000,20500,21000,21500,
22000,22500,23000,23500,24000,24500,25000,25500,26000,26500,27000,27500,28000,28500,29000,29500,30000,30500,31000,31500,
32000,32500,33000,33500,34000,34500,35000,35500,36000,36500,37000,37500,38000,38500,39000,39500,40000,40500,41000,41500,
42000,42500,43000,43500,44000,44500,45000,45500,46000,46500,47000,47500,48000,48500,49000,49500,50000]
                                                                                    }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 22,
                                                                                   \'[{
                                                                                       "title": "Rate for 30 minutes",
                                                                                       "type": "select",
                                                                                           "options":[
"Free",500,1000,1500,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500, 9000,9500,10000,10500,11000,11500,
12000,12500,13000,13500,14000,14500,15000,15500,16000,16500,17000,17500,18000,18500,19000,19500,20000,20500,21000,21500,
22000,22500,23000,23500,24000,24500,25000,25500,26000,26500,27000,27500,28000,28500,29000,29500,30000,30500,31000,31500,
32000,32500,33000,33500,34000,34500,35000,35500,36000,36500,37000,37500,38000,38500,39000,39500,40000,40500,41000,41500,
42000,42500,43000,43500,44000,44500,45000,45500,46000,46500,47000,47500,48000,48500,49000,49500,50000]
                                                                                    }]\');                                                                                     
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 23,
                                                                                   \'[{
                                                                                       "title": "Rate for 45 minutes",
                                                                                       "type": "select",
                                                                                           "options":[
"Free",500,1000,1500,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500, 9000,9500,10000,10500,11000,11500,
12000,12500,13000,13500,14000,14500,15000,15500,16000,16500,17000,17500,18000,18500,19000,19500,20000,20500,21000,21500,
22000,22500,23000,23500,24000,24500,25000,25500,26000,26500,27000,27500,28000,28500,29000,29500,30000,30500,31000,31500,
32000,32500,33000,33500,34000,34500,35000,35500,36000,36500,37000,37500,38000,38500,39000,39500,40000,40500,41000,41500,
42000,42500,43000,43500,44000,44500,45000,45500,46000,46500,47000,47500,48000,48500,49000,49500,50000]
                                                                                    }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 24,
                                                                                   \'[{
                                                                                       "title": "Rate for 60 minutes",
                                                                                       "type": "select",
                                                                                           "options":[
"Free",500,1000,1500,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500, 9000,9500,10000,10500,11000,11500,
12000,12500,13000,13500,14000,14500,15000,15500,16000,16500,17000,17500,18000,18500,19000,19500,20000,20500,21000,21500,
22000,22500,23000,23500,24000,24500,25000,25500,26000,26500,27000,27500,28000,28500,29000,29500,30000,30500,31000,31500,
32000,32500,33000,33500,34000,34500,35000,35500,36000,36500,37000,37500,38000,38500,39000,39500,40000,40500,41000,41500,
42000,42500,43000,43500,44000,44500,45000,45500,46000,46500,47000,47500,48000,48500,49000,49500,50000]
                                                                                    }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 56,
                                                                                   \'[{
                                                                                       "title": "Rate for 90 minutes",
                                                                                       "type": "select",
                                                                                           "options":[
"Free",500,1000,1500,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500, 9000,9500,10000,10500,11000,11500,
12000,12500,13000,13500,14000,14500,15000,15500,16000,16500,17000,17500,18000,18500,19000,19500,20000,20500,21000,21500,
22000,22500,23000,23500,24000,24500,25000,25500,26000,26500,27000,27500,28000,28500,29000,29500,30000,30500,31000,31500,
32000,32500,33000,33500,34000,34500,35000,35500,36000,36500,37000,37500,38000,38500,39000,39500,40000,40500,41000,41500,
42000,42500,43000,43500,44000,44500,45000,45500,46000,46500,47000,47500,48000,48500,49000,49500,50000]
                                                                                    }]\');                                                                                     
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 57,
                                                                                   \'[{
                                                                                       "title": "Rate for 120 minutes",
                                                                                       "type": "select",
                                                                                           "options":[
"Free",500,1000,1500,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000,10500,11000,11500,
12000,12500,13000,13500,14000,14500,15000,15500,16000,16500,17000,17500,18000,18500,19000,19500,20000,20500,21000,21500,
22000,22500,23000,23500,24000,24500,25000,25500,26000,26500,27000,27500,28000,28500,29000,29500,30000,30500,31000,31500,
32000,32500,33000,33500,34000,34500,35000,35500,36000,36500,37000,37500,38000,38500,39000,39500,40000,40500,41000,41500,
42000,42500,43000,43500,44000,44500,45000,45500,46000,46500,47000,47500,48000,48500,49000,49500,50000]
                                                                                    }]\');                                                                                  
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 28,
                                                                                   \'[{
                                                                                       "title": "Accepts Credit Cards",
                                                                                       "type": "bool",
                                                                                           "options":["Yes", "No"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 58,
                                                                                   \'[{
                                                                                       "title": "Table Shower",
                                                                                       "type": "bool",
                                                                                           "options":["Yes", "No"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 59,
                                                                                   \'[{
                                                                                       "title": "Table Shower Fee",
                                                                                       "type": "select",
                                                                                           "options":[
"Free",500,1000,1500,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000,10500,11000,11500,
12000,12500,13000,13500,14000,14500,15000,15500,16000,16500,17000,17500,18000,18500,19000,19500,20000,20500,21000,21500,
22000,22500,23000,23500,24000,24500,25000,25500,26000,26500,27000,27500,28000,28500,29000,29500,30000,30500,31000,31500,
32000,32500,33000,33500,34000,34500,35000,35500,36000,36500,37000,37500,38000,38500,39000,39500,40000,40500,41000,41500,
42000,42500,43000,43500,44000,44500,45000,45500,46000,46500,47000,47500,48000,48500,49000,49500,50000]
                                                                                    }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 60,
                                                                                   \'[{
                                                                                       "title": "Sauna",
                                                                                       "type": "bool",
                                                                                           "options":["Yes", "No"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 61,
                                                                                   \'[{
                                                                                       "title": "Jacuzzi",
                                                                                       "type": "bool",
                                                                                           "options":["Yes", "No"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 65,
                                                                                   \'[{
                                                                                       "title": "Private Parking",
                                                                                       "type": "bool",
                                                                                           "options":["Yes", "No"]
                                                                                       }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 148,
                                                                                   \'[{
                                                                                     "title": "Coupon",
                                                                                     "type": "upload",
                                                                                     "options": []
                                                                                   }]\');
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 170,
                                                                                   \'[{
                                                                                       "title": "Private Parking",
                                                                                       "type": "bool",
                                                                                           "options":["Yes", "No"]
                                                                                       }]\'); 
        INSERT into place_type_attributes (place_type_id, attribute_id, options) VALUES (15, 179,
                                                                                   \'[{
                                                                                       "title": "Masseuse",
                                                                                       "type": "bool",
                                                                                           "options":[
"Asian","Black","Brazilian","British","Cambodian","Caribbean","Chinese","Columbian","Crotian","Danish","Dominican",
"European","Filipino","Finnish(Finland)","French","French Canadian","German","Greek","Hawaiian","Hispanic","Hungarian",
"Indian","Irish","Italian","jamaican","Jamaican","Japanese","Korean","Laotian","Middle Eastern","Morrocan","Native Indian",
"Northern European","Norwegian","Polish","Portugese","Romanian","Russian","Serbian","Slovenian","Spanish","Swedish",
"Taiwanese","Thai","Trini","Ukrainian","Venezuelan","Vietnamese","White"]
                                                                                       }]\');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM place_type_attributes where place_type_id = 15;');
    }
}
