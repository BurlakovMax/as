<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191118105524 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE place_type_attributes (
             place_type_id int(11) NOT NULL,
             attribute_id int(11) NOT NULL,
             options JSON NOT NULL,
             PRIMARY KEY (place_type_id, attribute_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE place_type_attributes');
    }
}
