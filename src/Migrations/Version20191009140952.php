<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191009140952 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE sms_gate_message (
          message_id binary(16) NOT NULL, 
          sent_at int(11) NOT NULL, 
          delivered_at int(11) DEFAULT NULL, 
          is_delivered BOOLEAN NOT NULL, 
          phone_number VARCHAR(64) NOT NULL, 
          text VARCHAR(511) NOT NULL, 
          external_id VARCHAR(64) DEFAULT NULL, 
          count int(11) NOT NULL, 
          next_attempt_at int(11) NOT NULL, 
          PRIMARY KEY(message_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE sms_gate_message');
    }
}
