<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191228145727 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Добавляем поле последней операции в подписки';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE payment ADD last_operation binary(16) NULL;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE payment DROP COLUMN last_operation;');
    }
}
