<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110071819 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE forum_topic SET topic_last_post_time = null WHERE topic_last_post_time = 0;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('UPDATE forum_topic SET topic_last_post_time = 0 WHERE topic_last_post_time = null;');
    }
}
