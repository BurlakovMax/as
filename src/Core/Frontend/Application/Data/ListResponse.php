<?php

declare(strict_types=1);

namespace App\Core\Frontend\Application\Data;

final class ListResponse
{
    /** @var int */
    private $total;

    /**
     * @var mixed[]
     */
    private $list;

    public function __construct(int $total, array $list)
    {
        $this->total = $total;
        $this->list = $list;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return mixed[]
     */
    public function getList(): array
    {
        return $this->list;
    }
}