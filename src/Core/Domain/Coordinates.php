<?php

declare(strict_types=1);

namespace App\Core\Domain;

final class Coordinates
{
    /** @var string | null */
    private $latitude;

    /** @var string | null */
    private $longitude;

    public function __construct(?string $latitude, ?string $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }
}