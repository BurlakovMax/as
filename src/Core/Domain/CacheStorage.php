<?php

declare(strict_types=1);

namespace App\Core\Domain;

interface CacheStorage
{
    /**
     * @return mixed|null
     */
    public function read(string $key);

    /**
     * @param mixed $value
     */
    public function write(string $key, $value): void;

    public function writeWithTTL(string $key, $value, int $ttl): void;
}
