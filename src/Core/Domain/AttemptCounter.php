<?php

declare(strict_types=1);

namespace App\Core\Domain;

class AttemptCounter
{
    /** @var int */
    private $count;

    /** @var int */
    private $nextAttemptAt;

    final public function __construct()
    {
        $this->count = 1;
        $this->nextAttemptAt = time();
    }

    /**
     * @throws MaxAttemptReached
     */
    final public function nextAttempt(): void
    {
        $this->increaseCount();
        if ($this->maxAttemptReached()) {
            throw new MaxAttemptReached();
        }
    }

    protected function getMaxAttemptsNumber(): int
    {
        return 10;
    }

    protected function calculateNextAttemptTime(): \DateTimeImmutable
    {
        return new \DateTimeImmutable( pow(2, $this->getCount()) . 'seconds');
    }

    final protected function getCount(): int
    {
        return $this->count;
    }

    private function maxAttemptReached(): bool
    {
        return $this->getCount() > $this->getMaxAttemptsNumber();
    }

    private function increaseCount(): void
    {
        $this->count+= 1;
        $this->nextAttemptAt = $this->calculateNextAttemptTime()->getTimestamp();
    }
}