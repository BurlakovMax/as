<?php

declare(strict_types=1);

namespace App\Core\Application;

interface CaptchaVerifier
{
    public function verify(string $value): bool;

    public function getSiteKey(): string;
}
