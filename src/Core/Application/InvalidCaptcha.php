<?php

declare(strict_types=1);

namespace App\Core\Application;

use LazyLemurs\Exceptions\Exception;

final class InvalidCaptcha extends Exception
{
}
