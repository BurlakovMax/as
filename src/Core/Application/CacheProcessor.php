<?php

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\Domain\CacheStorage;

final class CacheProcessor
{
    private CacheStorage $storage;

    public function __construct(CacheStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return mixed|null
     */
    public function readFromCache(string $key)
    {
        return $this->storage->read($key);
    }

    /**
     * @param mixed $value
     */
    public function writeToCache(string $key, $value): void
    {
        $this->storage->write($key, $value);
    }

    /**
     * @param mixed $value
     */
    public function writeToCacheWithTTL(string $key, $value, int $ttl): void
    {
        $this->storage->writeWithTTL($key, $value, $ttl);
    }
}
