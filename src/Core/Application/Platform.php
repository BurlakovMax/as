<?php

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\DomainSupport\Enumerable;

final class Platform extends Enumerable
{
    public static function desktop(): self
    {
        return self::createEnum('desktop');
    }

    public static function mobile(): self
    {
        return self::createEnum('mobile');
    }
}