<?php

declare(strict_types=1);

namespace App\Core\Application\Search;

use App\Core\DomainSupport\Enumerable;

final class Ordering extends Enumerable
{
    public static function asc(): self
    {
        return self::createEnum('asc');
    }

    public static function desc(): self
    {
        return self::createEnum('desc');
    }
}