<?php

declare(strict_types=1);

namespace App\Core\ApplicationSupport\AccessControl;

use Throwable;

final class AccessForbidden extends \Exception
{
    public function __construct(Throwable $previous = null)
    {
        parent::__construct("Access forbidden", 403, $previous);
    }
}