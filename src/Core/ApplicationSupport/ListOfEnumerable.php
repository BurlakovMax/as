<?php

declare(strict_types=1);

namespace App\Core\ApplicationSupport;

use Ds\Map;

abstract class ListOfEnumerable
{
    public static function valueToName(/* mixed */ $type): string
    {
        return static::getList()->get($type);
    }

    /**
     * @return string[]
     */
    public static function getRawList(): array
    {
        return static::getList()->values()->toArray();
    }

    public abstract static function getList(): Map;
}