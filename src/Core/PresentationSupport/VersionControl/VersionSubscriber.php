<?php

declare(strict_types=1);

namespace App\Core\PresentationSupport\VersionControl;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class VersionSubscriber implements EventSubscriberInterface
{
    private const APPLICATION_VERSION = 'ETag';

    private string $currentVersion;

    public function __construct(string $currentVersion)
    {
        $this->currentVersion = $currentVersion;
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        $event->getResponse()->headers->set(self::APPLICATION_VERSION, $this->currentVersion);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE  => ['onKernelResponse', 0],
        ];
    }
}