<?php

declare(strict_types=1);

namespace App\Core\PresentationSupport\Rest\Params;

use App\Core\Application\Search\LimitationQueryImmutable;
use Symfony\Component\HttpFoundation\Request;

trait OffsetLimitParams
{
    final protected function getLimitation(Request $request): LimitationQueryImmutable
    {
        return LimitationQueryImmutable::create(
            $request->query->has('offset') ? $request->query->getInt('offset') : null,
            $request->query->has('limit') ? $request->query->getInt('limit') : null
        );
    }
}