<?php

declare(strict_types=1);

namespace App\Core\PresentationSupport\ErrorConverter;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ExceptionSubscriber implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        if (substr($event->getRequest()->getPathInfo(), 0, 8) !== '/api/v1/') {
            return;
        }

        $exception = $event->getThrowable();
        $response = new JsonResponse(
            [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ],
            $exception->getCode()
        );
        $event->setResponse($response);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION  => ['onKernelException', 0],
        ];
    }
}