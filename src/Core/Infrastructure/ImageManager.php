<?php

declare(strict_types=1);

namespace App\Core\Infrastructure;

final class ImageManager
{
    public static function resize(string $root, string $public, string $dirWithFile, int $width): string
    {
        $img = new \Imagick($root . $public . $dirWithFile);
        [$dirWithFile, $ext] = explode('.', $dirWithFile);
        $publicDirFile = $dirWithFile . '.jpg';
        $fullDirFile = $root . $public . $publicDirFile;
        $img->setImageFormat('jpeg');

        if ($img->getImageWidth() < $width) {
            $img->writeImage($fullDirFile);
            $img->destroy();

            return $publicDirFile;
        }

        $img->resizeImage($width, $width/2, \Imagick::FILTER_LANCZOS,1);
        $img->writeImage($fullDirFile);
        $img->destroy();

        return $publicDirFile;
    }

    public static function remove(string $file): void
    {
        if (file_exists($file)) {
            unlink($file);
        }
    }
}