<?php

declare(strict_types=1);

namespace App\Core\Infrastructure;

use App\Core\Application\CaptchaVerifier;

final class DummyCaptchaVerifier implements CaptchaVerifier
{
    private string $testValue;

    public function __construct(string $testValue)
    {
        $this->testValue = $testValue;
    }

    public function verify(string $value): bool
    {
        return $value === $this->testValue;
    }

    public function getSiteKey(): string
    {
        return 'test';
    }
}
