<?php

declare(strict_types=1);

namespace App\Core\Infrastructure;

use App\Core\Application\CaptchaVerifier;

final class ReCaptchaVerifier implements CaptchaVerifier
{
    private const URL = 'https://www.google.com/recaptcha/api/siteverify';

    private string $secret;

    private string $siteKey;

    public function __construct(string $secret, string $siteKey)
    {
        $this->secret = $secret;
        $this->siteKey = $siteKey;
    }

    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    public function verify(string $value): bool
    {
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request(
                'POST',
                self::URL,
                [
                    'form_params' => [
                        'secret' => $this->secret,
                        'response' => $value,
                    ],
                ],
            );

            $data = json_decode($response->getBody()->getContents());

            return isset($data->success) && $data->success === true;
        } catch (\GuzzleHttp\Exception\GuzzleException $exception) {
            return false;
        }
    }
}
