<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Persistence;

use App\Core\Domain\CacheStorage;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

final class FileCacheRepository implements CacheStorage
{
    private FilesystemAdapter $cache;

    public function __construct()
    {
        $this->cache = new FilesystemAdapter();
    }

    /**
     * @return mixed|null
     */
    public function read(string $key)
    {
        $cacheValue = $this->cache->getItem($key);
        return $cacheValue->isHit() ? $cacheValue->get() : null;
    }

    /**
     * @param mixed $value
     */
    public function write(string $key, $value): void
    {
        $cacheValue = $this->cache->getItem($key);
        $cacheValue->set($value);
        $this->cache->save($cacheValue);
    }

    public function writeWithTTL(string $key, $value, int $ttl): void
    {
        // TODO: Implement writeWithTTL() method.
    }
}