<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Persistence;

use App\Core\Domain\CacheStorage;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\CacheItem;

final class RedisCacheRepository implements CacheStorage
{
    private RedisAdapter $cache;

    public function __construct(string $connection)
    {
        $this->cache = new RedisAdapter(RedisAdapter::createConnection($connection));
    }

    /**
     * @return mixed|null
     */
    public function read(string $key)
    {
        $cacheValue = $this->cache->getItem($key);
        return $cacheValue->isHit() ? $cacheValue->get() : null;
    }

    /**
     * @param mixed $value
     */
    public function write(string $key, $value): void
    {
        /** @var CacheItem $cacheValue */
        $cacheValue = $this->cache->getItem($key);
        $cacheValue->set($value);
        $this->cache->save($cacheValue);
    }

    public function writeWithTTL(string $key, $value, int $ttl): void
    {
        /** @var CacheItem $cacheValue */
        $cacheValue = $this->cache->getItem($key);
        $cacheValue->set($value);
        $cacheValue->expiresAfter($ttl);
        $this->cache->save($cacheValue);
    }
}