<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Search\Doctrine\Expression;

use App\Core\Application\Search\FilterQuery;
use Doctrine\Common\Collections\Criteria;

final class CriteriaBuilder
{
    /**
     * @param FilterQuery[] $filters
     * @return \Doctrine\Common\Collections\Criteria
     */
    public static function createByFilters(array $filters): Criteria
    {
        $criteria = Criteria::create();

        foreach ($filters as $filter) {
            $criteria->andWhere(Filter::makeExpression($filter));
        }

        return $criteria;
    }
}