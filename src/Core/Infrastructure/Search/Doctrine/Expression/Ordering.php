<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Search\Doctrine\Expression;

use App\Core\Application\Search\OrderQuery;
use Doctrine\ORM\QueryBuilder;

final class Ordering
{
    /**
     * @param OrderQuery[] $orderQueries
     */
    public static function add(QueryBuilder $queryBuilder, array $orderQueries): void
    {
        foreach ($orderQueries as $orderQuery) {
            $queryBuilder->orderBy($orderQuery->getProperty(), $orderQuery->getOrdering());
        }
    }
}