<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Search\Doctrine\Expression;

use Doctrine\ORM\QueryBuilder;

final class Grouping
{
    /**
     * @param <string>[] $groupingFields
     */
    public static function add(QueryBuilder $queryBuilder, array $groupingFields): void
    {
        foreach ($groupingFields as $groupingField) {
            $queryBuilder->addGroupBy($groupingField);
        }
    }
}
