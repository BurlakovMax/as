<?php

declare(strict_types=1);

namespace App\Core\DomainSupport;

/* final */ class Enumerable extends \LitGroup\Enumerable\Enumerable
{
    public function __toString(): string
    {
        return (string) $this->getRawValue();
    }
}
