<?php

declare(strict_types=1);

namespace App\Core\DomainSupport;

trait TimestampToDateTime
{
    final protected function timestampToDateTime(?int $timestamp): ?\DateTimeImmutable
    {
        return $timestamp ? (new \DateTimeImmutable())->setTimestamp($timestamp) : null;
    }
}