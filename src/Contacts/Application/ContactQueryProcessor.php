<?php

declare(strict_types=1);

namespace App\Contacts\Application;

use App\Contacts\Domain\Contact;
use App\Contacts\Domain\ContactReadStorage;
use App\Core\Application\Search\SearchQuery;

final class ContactQueryProcessor
{
    private ContactReadStorage $contactReadStorage;

    public function __construct(ContactReadStorage $contactReadStorage)
    {
        $this->contactReadStorage = $contactReadStorage;
    }

    public function getById(int $id): ContactData
    {
        return $this->mapToData($this->contactReadStorage->getById($id));
    }

    public function getListBySearchQuery(SearchQuery $searchQuery): array
    {
        return $this->mapListToData($this->contactReadStorage->getListBySearchQuery($searchQuery));
    }

    public function countBySearchQuery(SearchQuery $searchQuery): int
    {
        return $this->contactReadStorage->countBySearchQuery($searchQuery);
    }

    private function mapToData(Contact $contact): ContactData
    {
        return new ContactData($contact);
    }

    /**
     * @param Contact[] $contacts
     * @return ContactData[]
     */
    private function mapListToData(array $contacts): array
    {
        return array_map(fn($contact): ContactData => new ContactData($contact), $contacts);
    }
}
