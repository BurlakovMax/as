<?php

declare(strict_types=1);

namespace App\Contacts\Application;

use LazyLemurs\Structures\Email;
use LazyLemurs\Structures\PhoneNumber;

final class CreateContactCommand
{
    private int $accountId;

    private Email $email;

    private ?string $ipAddress;

    private PhoneNumber $phone;

    private string $name;

    private ?string $text;

    private ?string $html;

    private string $url;

    private string $agent;

    public function __construct(
        int $accountId,
        Email $email,
        ?string $ipAddress,
        PhoneNumber $phone,
        string $name,
        ?string $text,
        ?string $html,
        string $url,
        string $agent
    ) {
        $this->accountId = $accountId;
        $this->email = $email;
        $this->ipAddress = $ipAddress;
        $this->phone = $phone;
        $this->name = $name;
        $this->text = $text;
        $this->html = $html;
        $this->url = $url;
        $this->agent = $agent;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function getPhone(): PhoneNumber
    {
        return $this->phone;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getAgent(): string
    {
        return $this->agent;
    }
}
