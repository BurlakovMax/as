<?php

declare(strict_types=1);

namespace App\Contacts\Application;

use App\Contacts\Domain\Contact;
use Swagger\Annotations as SWG;

final class ContactData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $accountId;

    /**
     * @SWG\Property()
     */
    private string $email;

    /**
     * @SWG\Property()
     */
    private ?string $ipAddress;

    /**
     * @SWG\Property()
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private string $phone;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private ?string $text;

    /**
     * @SWG\Property()
     */
    private ?string $html;

    /**
     * @SWG\Property()
     */
    private string $url;

    /**
     * @SWG\Property()
     */
    private string $agent;

    /**
     * @SWG\Property()
     */
    private ?bool $read;

    /**
     * @SWG\Property()
     */
    private ?int $assigned;

    /**
     * @SWG\Property()
     */
    private ?int $readBy;

    /**
     * @SWG\Property()
     */
    private ?string $respond;

    public function __construct(Contact $contact) {
        $this->id = $contact->getId();
        $this->accountId = $contact->getAccountId();
        $this->email = $contact->getEmail()->getValue();
        $this->ipAddress = $contact->getIpAddress();
        $this->createdAt = $contact->getCreatedAt();
        $this->phone = $contact->getPhone();
        $this->name = $contact->getName();
        $this->text = $contact->getText();
        $this->html = $contact->getHtml();
        $this->url = $contact->getUrl();
        $this->agent = $contact->getAgent();
        $this->read = $contact->getRead();
        $this->assigned = $contact->getAssigned();
        $this->readBy = $contact->getReadBy();
        $this->respond = $contact->getRespond();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getAgent(): string
    {
        return $this->agent;
    }

    public function getRead(): ?bool
    {
        return $this->read;
    }

    public function getAssigned(): ?int
    {
        return $this->assigned;
    }

    public function getReadBy(): ?int
    {
        return $this->readBy;
    }

    public function getRespond(): ?string
    {
        return $this->respond;
    }
}
