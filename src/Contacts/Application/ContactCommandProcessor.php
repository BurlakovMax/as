<?php

declare(strict_types=1);

namespace App\Contacts\Application;

use App\Contacts\Domain\Contact;
use App\Contacts\Domain\ContactWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class ContactCommandProcessor
{
    private TransactionManager $transactionManager;

    private ContactWriteStorage $storage;

    public function __construct(TransactionManager $transactionManager, ContactWriteStorage $storage)
    {
        $this->transactionManager = $transactionManager;
        $this->storage = $storage;
    }

    public function create(CreateContactCommand $command): int
    {
        $contact = new Contact($command);

        $this->transactionManager->transactional(function () use ($contact) {
            $this->storage->add($contact);
        });

        return $contact->getId();

    }
}