<?php

declare(strict_types=1);

namespace App\Contacts\Infrastructure\Persistence;

use App\Contacts\Domain\Contact;
use App\Contacts\Domain\ContactReadStorage;
use App\Contacts\Domain\ContactWriteStorage;
use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

final class ContactDoctrineRepository extends EntityRepository implements ContactWriteStorage, ContactReadStorage
{
    /**
     * @throws ORMException
     */
    public function add(Contact $contact): void
    {
        $this->getEntityManager()->persist($contact);
    }

    public function getById(int $id): Contact
    {
        return $this->find($id);
    }

    /**
     * @return Contact[]
     *
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getListBySearchQuery(SearchQuery $searchQuery): array
    {
        $queryBuilder = $this->createQueryBuilder('t')
                ->addCriteria(CriteriaBuilder::createByFilters($searchQuery->getFilters()))
                ->addCriteria(
                    Limitation::limitation($searchQuery->getLimitation())
                );

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $searchQuery): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($searchQuery->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }
}
