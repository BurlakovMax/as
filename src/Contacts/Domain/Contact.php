<?php

declare(strict_types=1);

namespace App\Contacts\Domain;

use App\Contacts\Application\CreateContactCommand;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\Email;
use LazyLemurs\Structures\PhoneNumber;

/**
 * @ORM\Entity(repositoryClass="App\Contacts\Infrastructure\Persistence\ContactDoctrineRepository")
 * @ORM\Table(name="contact")
 */
class Contact
{
    /**
     * @ORM\Column(name="contact_id", type="integer", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="account_id",type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="email", length=100)
     */
    private Email $email;

    /**
     * @ORM\Column(name="ip_address",type="string", length=50, nullable=true)
     */
    private ?string $ipAddress;

    /**
     * @ORM\Column(name="stamp",type="timestamp", nullable=true)
     */
    private ?DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $text;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $html;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private string $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $agent;

    /**
     * @ORM\Column(name="`read`", type="boolean", nullable=true)
     */
    private ?bool $read;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $assigned;

    /**
     * @ORM\Column(name="read_by",type="integer", length=11, nullable=true)
     */
    private ?int $readBy;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $respond;

    public function __construct(
        CreateContactCommand $command
    ) {
        $this->accountId = $command->getAccountId();
        $this->email = $command->getEmail();
        $this->ipAddress = $command->getIpAddress();
        $this->createdAt = new DateTimeImmutable();
        $this->phone = $command->getPhone()->getValue();
        $this->name = $command->getName();
        $this->text = $command->getText();
        $this->html = $command->getHtml();
        $this->url = $command->getUrl();
        $this->agent = $command->getAgent();
        $this->read = false;
        $this->assigned = 0;
        $this->readBy = null;
        $this->respond = null;
        $this->id = 0;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getAgent(): string
    {
        return $this->agent;
    }

    public function getRead(): ?bool
    {
        return $this->read;
    }

    public function getAssigned(): ?int
    {
        return $this->assigned;
    }

    public function getReadBy(): ?int
    {
        return $this->readBy;
    }

    public function getRespond(): ?string
    {
        return $this->respond;
    }
}
