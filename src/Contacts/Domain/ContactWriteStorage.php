<?php

declare(strict_types=1);

namespace App\Contacts\Domain;

interface ContactWriteStorage
{
    public function add(Contact $contact): void;
}