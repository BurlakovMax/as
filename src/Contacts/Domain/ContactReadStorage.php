<?php

declare(strict_types=1);

namespace App\Contacts\Domain;

use App\Core\Application\Search\SearchQuery;

interface ContactReadStorage
{
    public function getById(int $id): Contact;

    /**
     * @return Contact[]
     */
    public function getListBySearchQuery(SearchQuery $searchQuery): array;

    public function countBySearchQuery(SearchQuery $searchQuery): int;
}
