<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface RefundWriteStorage
{
    public function add(Refund $refund): void;
}