<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\Application\Search\SearchQuery;

interface TransactionReadStorage
{
    public function countByAccountId(int $accountId): int;

    /**
     * @return Transaction[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;
}
