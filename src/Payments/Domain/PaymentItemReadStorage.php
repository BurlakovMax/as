<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\Application\Search\SearchQuery;

interface PaymentItemReadStorage
{
    /**
     * @return PaymentItem[]
     */
    public function getBySearchQuery(SearchQuery $query): array;
}