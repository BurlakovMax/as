<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface EscortSponsorStorage
{
    public function activateAfterPaid(int $escortId, int $locationId): void;

    public function getRecurringPeriod(): int;
}