<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\Application\Search\SearchQuery;

interface CreditCardReadStorage
{
    public function get(int $id): ?CreditCard;

    /**
     * @return CreditCard[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function countByAccountId(int $accountId): int;

    public function getByCardParams(
        int $getAccountId,
        string $substr,
        string $expiryMonth,
        string $expiryYear
    ): ?CreditCard;
}
