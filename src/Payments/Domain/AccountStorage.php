<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface AccountStorage
{
    public function addTopUps(int $accountId, int $topUps): void;
}