<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreateEscortStickyPaymentItemCommand;
use App\Payments\Application\CreateEscortPaymentItemCommand;
use App\Payments\Application\CreateSideEscortPaymentItemCommand;
use App\Payments\Application\CreateEscortSponsorPaymentItemCommand;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\PaymentItemDoctrineRepository")
 * @ORM\Table(name="payment_item")
 */
class PaymentItem
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="classified_id", type="integer", length=11, nullable=true)
     */
    private ?int $escortId;

    /**
     * @ORM\Column(name="classified_status", type="escort_payment_status", nullable=true)
     */
    private ?EscortPaymentStatus $escortStatus;

    /**
     * @ORM\Column(name="loc_id", type="integer", length=11, nullable=true)
     */
    private ?int $locationId;

    /**
     * @ORM\Column(name="type", type="escort_payment_type", nullable=true)
     */
    private ?EscortPaymentType $subjectType;

    /**
     * @ORM\Column(type="boolean", length=3, nullable=true)
     */
    private ?bool $sponsor;

    /**
     * @ORM\Column(type="boolean", length=3, nullable=true)
     */
    private ?bool $side;

    /**
     * @ORM\Column(type="boolean", length=3, nullable=true)
     */
    private ?bool $sticky;

    /**
     * @ORM\Column(name="sticky_days", type="integer", length=11, nullable=true)
     */
    private ?int $stickyDays;

    /**
     * @ORM\Column(name="auto_renew", type="integer", length=11, nullable=true)
     */
    private ?int $topUps;

    /**
     * @ORM\Column(name="place_section", type="string", length=50, nullable=true)
     */
    private ?string $placeSection;

    /**
     * @ORM\Column(name="place_id", type="integer", length=11, nullable=true)
     */
    private ?int $placeId;

    /**
     * @ORM\Column(name="place_sponsor", type="boolean", nullable=true)
     */
    private ?bool $placeSponsor;

    /**
     * @ORM\ManyToOne(targetEntity="Payment", inversedBy="items", cascade={"persist", "merge"}, fetch="LAZY")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     */
    private Payment $payment;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp")
     */
    private DateTimeImmutable $createdAt;

    public function __construct(EscortPaymentType $type)
    {
        $this->id = 0;
        $this->createdAt = new DateTimeImmutable();
        $this->subjectType = $type;
        $this->sponsor = false;
        $this->side = false;
        $this->sticky = false;
        $this->stickyDays = null;
        $this->placeSponsor = false;
        $this->placeId = null;
        $this->placeSection = null;
        $this->topUps = 0;
    }

    public static function createAdvertisePaymentItem(): self
    {
        return new self(EscortPaymentType::advertise());
    }

    public static function createEscortPaymentItem(CreateEscortPaymentItemCommand $command, Payment $payment): self
    {
        $paymentItem = new self(EscortPaymentType::escort());
        $paymentItem->payment = $payment;
        $paymentItem->escortId = $command->getEscortId();
        $paymentItem->locationId = $command->getLocationId();
        $paymentItem->escortStatus = EscortPaymentStatus::new();
        $paymentItem->createdAt = new DateTimeImmutable();

        return $paymentItem;
    }

    public static function createEscortStickyPaymentItem(CreateEscortStickyPaymentItemCommand $command, Payment $payment): self
    {
        $paymentItem = new self(EscortPaymentType::escort());
        $paymentItem->payment = $payment;
        $paymentItem->escortId = $command->getEscortId();
        $paymentItem->locationId = $command->getLocationId();
        $paymentItem->sticky = true;
        $paymentItem->stickyDays = $command->getDays();
        $paymentItem->escortStatus = EscortPaymentStatus::upgrade();
        $paymentItem->createdAt = new DateTimeImmutable();

        return $paymentItem;
    }

    public static function createSidePaymentItem(CreateSideEscortPaymentItemCommand $command, Payment $payment): self
    {
        $paymentItem = new self(EscortPaymentType::escort());
        $paymentItem->payment = $payment;
        $paymentItem->escortId = $command->getEscortId();
        $paymentItem->locationId = $command->getLocationId();
        $paymentItem->side = true;
        $paymentItem->escortStatus = EscortPaymentStatus::upgrade();
        $paymentItem->createdAt = new DateTimeImmutable();

        return $paymentItem;
    }

    public static function createEscortSponsorPaymentItem(CreateEscortSponsorPaymentItemCommand $command, Payment $payment): self
    {
        $paymentItem = new self(EscortPaymentType::escort());
        $paymentItem->payment = $payment;
        $paymentItem->escortId = $command->getEscortId();
        $paymentItem->sponsor = true;
        $paymentItem->locationId = $command->getLocationId();
        $paymentItem->escortStatus = EscortPaymentStatus::upgrade();
        $paymentItem->createdAt = new DateTimeImmutable();

        return $paymentItem;
    }

    public static function createPlaceownerPaymentItem(int $placeId, Payment $payment): self
    {
        $paymentItem = new self(EscortPaymentType::placeowner());
        $paymentItem->payment = $payment;
        $paymentItem->placeId = $placeId;
        $paymentItem->locationId = null;
        $paymentItem->placeSponsor = true;
        $paymentItem->createdAt = new DateTimeImmutable();

        return $paymentItem;
    }

    public static function createTopUpsPaymentItem(int $topUps, Payment $payment): self
    {
        $paymentItem = new self(EscortPaymentType::topUps());
        $paymentItem->payment = $payment;
        $paymentItem->topUps = $topUps;
        $paymentItem->escortId = null;
        $paymentItem->locationId = null;
        $paymentItem->escortStatus = null;
        $paymentItem->createdAt = new DateTimeImmutable();

        return $paymentItem;
    }

    /**
     * @return ServiceType[]
     */
    public function getServiceTypes(): array
    {
        $types = [];

        if (null !== $this->placeId && null !== $this->placeSponsor) {
            $types[] = ServiceType::placeowner();
        }

        if ($this->sponsor) {
            $types[] = ServiceType::escortSponsor();
        }

        if ($this->side) {
            $types[] = ServiceType::escortSide();
        }

        if ($this->sticky) {
            $types[] = ServiceType::escortSticky();
        }

        if ($this->topUps > 0) {
            $types[] = ServiceType::topUps();
        }

        if ($this->getSubjectType()->equals(EscortPaymentType::escort())) {
            if (!$this->sponsor && !$this->side && !$this->sticky) {
                $types[] = ServiceType::escort();
            }
        }

        return $types;
    }

    public function getPlaceSection(): ?string
    {
        return $this->placeSection;
    }

    public function getPlaceId(): ?int
    {
        return $this->placeId;
    }

    public function getPlaceSponsor(): ?bool
    {
        return $this->placeSponsor;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getEscortId(): ?int
    {
        return $this->escortId;
    }

    public function getSubjectType(): ?EscortPaymentType
    {
        return $this->subjectType;
    }

    public function getSponsor(): ?bool
    {
        return $this->sponsor;
    }

    public function getSide(): ?bool
    {
        return $this->side;
    }

    public function getSticky(): ?bool
    {
        return $this->sticky;
    }

    public function getStickyDays(): ?int
    {
        return $this->stickyDays;
    }

    public function getTopUps(): ?int
    {
        return $this->topUps;
    }

    public function getLocationId(): ?int
    {
        return $this->locationId;
    }

    public function getEscortStatus(): ?EscortPaymentStatus
    {
        return $this->escortStatus;
    }
}
