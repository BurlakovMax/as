<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreateTransactionCommand;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Money\Currency;
use Money\Money;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\TransactionDoctrineRepository")
 * @ORM\Table(name="transaction")
 */
class Transaction
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="stamp", type="timestamp")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="payment_type")
     */
    private ?TransactionType $type;

    /**
     * @ORM\Column(name="trans_id", type="string", length=50)
     */
    private ?string $transId;

    /**
     * @ORM\ManyToOne(targetEntity="Payment", inversedBy="transactions", cascade={"persist", "merge"}, fetch="LAZY")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     */
    private Payment $payment;

    /**
     * @ORM\Column(type="float")
     */
    private ?float $amount;

    public function __construct(CreateTransactionCommand $command, Payment $payment)
    {
        $this->id = 0;
        $this->payment = $payment;
        $this->transId = $command->getExternalId();
        $this->type = TransactionType::normal();
        $this->amount = $command->getAmount();
        $this->createdAt = $command->getCreatedAt();
    }

    public function refund(int $amount): void
    {
        $newAmount = $this->amount - (float) number_format($amount / 100, 2, ".", "");
        if ($newAmount < 0) {
            $this->amount = 0;
            return;
        }

        $this->amount = $newAmount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getType(): ?TransactionType
    {
        return $this->type;
    }

    public function getTransId(): ?string
    {
        return $this->transId;
    }

    public function getAmount(): Money
    {
        return new Money($this->amount * 100, new Currency('USD'));
    }

    /**
     * @return PaymentItem[]|Collection
     */
    public function getItems(): Collection
    {
        return $this->getPayment()->getItems();
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getPaymentId(): int
    {
        return $this->getPayment()->getId();
    }
}
