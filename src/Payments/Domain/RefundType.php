<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class RefundType extends Enumerable
{
    public static function refund(): self
    {
        return self::createEnum('R');
    }

    public static function chargeback(): self
    {
        return self::createEnum('CH');
    }

    public static function undefined(): self
    {
        return self::createEnum('V');
    }
}