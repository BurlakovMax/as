<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface EscortSideStorage
{
    public function activateAfterPaid(int $escortId, int $locationId): void;

    public function getRecurringPeriod(): int;
}