<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class ServiceType extends Enumerable
{
    public static function escort(): self
    {
        return self::createEnum('escort');
    }

    public static function escortSponsor(): self
    {
        return self::createEnum('escortSponsor');
    }

    public static function escortSide(): self
    {
        return self::createEnum('escortSide');
    }

    public static function escortSticky(): self
    {
        return self::createEnum('escortSticky');
    }

    public static function topUps(): self
    {
        return self::createEnum('topUps');
    }

    public static function advertise(): self
    {
        return self::createEnum('advertise');
    }

    public static function placeowner(): self
    {
        return self::createEnum('placeowner');
    }

    public static function combo(): self
    {
        return self::createEnum('combo');
    }
}