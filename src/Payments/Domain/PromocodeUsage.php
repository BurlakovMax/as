<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\PromocodeUsageDoctrineRepository")
 * @ORM\Table(name="classifieds_promocodes_usage")
 */
class PromocodeUsage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $codeId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $ipAddress;

    /**
     * @ORM\Column(type="date", name="date")
     */
    private \DateTimeImmutable $usedAt;

    /**
     * @ORM\Column(type="float")
     */
    private float $paid;

    /**
     * @ORM\Column(type="float")
     */
    private float $saved;

    public function __construct(
        int $codeId,
        int $accountId,
        string $ipAddress,
        float $paid,
        float $saved
    ) {
        $this->codeId = $codeId;
        $this->accountId = $accountId;
        $this->ipAddress = $ipAddress;
        $this->usedAt = new \DateTimeImmutable();
        $this->paid = $paid;
        $this->saved = $saved;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCodeId(): int
    {
        return $this->codeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function getUsedAt(): \DateTimeImmutable
    {
        return $this->usedAt;
    }

    public function getPaid(): float
    {
        return $this->paid;
    }

    public function getSaved(): float
    {
        return $this->saved;
    }
}
