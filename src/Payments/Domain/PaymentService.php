<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreatePaymentCommand;
use App\Payments\Application\CreateTransactionCommand;
use App\Payments\Application\CreditCardNotFound;
use App\Payments\Application\CreditCardQueryProcessor;
use App\Payments\Application\PaymentWasNotCreated;
use App\Payments\Application\PaymentNotFound;

final class PaymentService
{
    private PaymentWriteStorage $paymentWriteStorage;

    private CreditCardQueryProcessor $creditCardQueryProcessor;

    private PaymentOperations $paymentOperations;

    public function __construct(
        PaymentWriteStorage $paymentWriteStorage,
        CreditCardQueryProcessor $creditCardQueryProcessor,
        PaymentOperations $paymentOperations
    ) {
        $this->paymentWriteStorage = $paymentWriteStorage;
        $this->creditCardQueryProcessor = $creditCardQueryProcessor;
        $this->paymentOperations = $paymentOperations;
    }

    public function createPayment(
        CreatePaymentCommand $createSubscriptionCommand,
        int $cardId
    ): Payment {
        return Payment::create($createSubscriptionCommand, $cardId);
    }

    /**
     * @throws CreditCardNotFound
     * @throws PaymentNotFound
     * @throws PaymentWasNotCreated
     */
    public function pay(int $paymentId): CreateTransactionCommand
    {
        $payment = $this->paymentWriteStorage->getAndLock($paymentId);
        if (null === $payment) {
            throw new PaymentNotFound("Payment with ID {$paymentId} not found.");
        }

        $card = $this->creditCardQueryProcessor->get($payment->getCardId());

        if (null !== $card->getCustomerId() && null !== $card->getCardId()) {
            $result = $this->paymentOperations->payWithCustomerData($payment, $card);
        } else {
            $result = $this->paymentOperations->pay($payment, $card);
        }

        $payment->sent($result->getId());
        $result->setPaymentId($payment->getId());

        return $result;
    }

    public function refund(Payment $payment, int $amount): CreateTransactionCommand
    {
        return $this->paymentOperations->refund($payment, $amount);
    }
}
