<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface PlaceStorage
{
    public function setBusinessOwner(int $placeId, int $businessOwnerId, int $recurringPeriod): void;
}