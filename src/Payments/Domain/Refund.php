<?php

declare(strict_types=1);

namespace App\Payments\Domain;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\RefundDoctrineRepository")
 * @ORM\Table(name="account_refund")
 */
class Refund
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private int $id;

    /**
     * @ORM\Column(type="refund_type", nullable=true)
     */
    private ?RefundType $type;

    /**
     * @ORM\Column(name="stamp", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $refundedAt;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $transactionId;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $authorId;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $paymentId;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $accountId;

    /**
     * @ORM\Column(name="cc_id", type="integer", length=11, nullable=true)
     */
    private ?int $creditCardId;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $reason;

    public function __construct(
        RefundType $type,
        int $transactionId,
        int $authorId,
        int $paymentId,
        int $accountId,
        int $creditCardId,
        int $amount,
        ?string $reason
    ) {
        $this->id = 0;
        $this->type = $type;
        $this->refundedAt = new \DateTimeImmutable();
        $this->transactionId = $transactionId;
        $this->authorId = $authorId;
        $this->paymentId = $paymentId;
        $this->accountId = $accountId;
        $this->creditCardId = $creditCardId;
        $this->amount = (float) number_format($amount / 100, 2, ".", "");
        $this->reason = $reason;
    }
}