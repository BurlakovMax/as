<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreateTransactionCommand;
use App\Payments\Application\CreatePaymentCommand;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\Email;
use Money\Currency;
use Money\Money;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\PaymentDoctrineRepository")
 * @ORM\Table(name="payment")
 */
class Payment
{
    private const EXPIRE_IN_DAYS_WITHOUT_RECURRING_PERIOD = 7;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private int $id;

    /**
     * @ORM\Column(name="account_id", type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(name="author_id", type="integer", length=11)
     */
    private int $authorId;

    /**
     * @ORM\Column(name="trans_id", type="string", nullable=true)
     */
    private ?string $transId;

    /**
     * @ORM\Column(type="float")
     */
    private float $amount;

    /**
     * @ORM\Column(type="email")
     */
    private Email $email;

    /**
     * @ORM\Column(type="string", name="ip_address", nullable=true)
     */
    private ?string $ipAddress;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $recurringAmount;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $recurringPeriod;

    /**
     * @ORM\Column(name="next_renewal_date", type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $nextRenewalDate;

    /**
     * @ORM\Column(name="subscription_status", type="subscription_status", nullable=true)
     */
    private ?SubscriptionStatus $subscriptionStatus;

    /**
     * @ORM\Column(name="result", type="payment_result", nullable=true)
     */
    private ?PaymentResult $result;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="sent_stamp", type="timestamp", nullable=true)
     */
    private ?DateTimeImmutable $sentAt;

    /**
     * @ORM\Column(name="responded_stamp", type="timestamp", nullable=true)
     */
    private ?DateTimeImmutable $respondedAt;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private ?string $lastOperation;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $declineCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $declineReason;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $response;

    /**
     * @ORM\OneToMany(targetEntity="PaymentItem", mappedBy="payment", cascade={"persist", "merge"}, fetch="LAZY")
     */
    private Collection $items;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="payment", cascade={"persist", "merge"}, fetch="LAZY")
     */
    private Collection $transactions;

    /**
     * @ORM\Column(name="cc_id", type="integer", length=11, nullable=true)
     */
    private ?int $cardId;

    public function __construct(
        int $accountId,
        int $authorId,
        float $amount,
        Email $email,
        string $ipAddress
    ) {
        $this->id = 0;
        $this->createdAt = new DateTimeImmutable();
        $this->sentAt = null;
        $this->respondedAt = null;
        $this->declineCode = null;
        $this->declineReason = null;
        $this->accountId = $accountId;
        $this->authorId = $authorId;
        $this->amount = $amount;
        $this->recurringAmount = null;
        $this->recurringPeriod = null;
        $this->email = $email;
        $this->result = PaymentResult::initial();;
        $this->ipAddress = $ipAddress;
        $this->subscriptionStatus = null;
        $this->nextRenewalDate = null;
        $this->transId = null;
        $this->lastOperation = null;
        $this->response = null;
        $this->cardId = null;
        $this->items = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public static function create(
        CreatePaymentCommand $createPaymentCommand,
        int $cardId
    ): self
    {
        $payment = new self(
            $createPaymentCommand->getAccountId(),
            $createPaymentCommand->getAccountId(),
            $createPaymentCommand->getAmount(),
            $createPaymentCommand->getEmail(),
            $createPaymentCommand->getIpAddress()
        );

        $payment->cardId = $cardId;

        return $payment;
    }

    public function setRecurring(int $recurring): void
    {
        $this->recurringPeriod = $recurring;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function getAuthorId(): ?int
    {
        return $this->authorId;
    }

    public function getTransId(): ?string
    {
        return $this->transId;
    }

    public function getAmount(): Money
    {
        return new Money($this->amount * 100, new Currency('USD'));
    }

    public function getRecurringAmount(): ?Money
    {
        return new Money($this->recurringAmount * 100, new Currency('USD'));
    }

    public function getNextRenewalDate(): ?DateTimeImmutable
    {
        return $this->nextRenewalDate;
    }

    public function getSubscriptionStatus(): ?SubscriptionStatus
    {
        return $this->subscriptionStatus;
    }

    public function getResult(): ?PaymentResult
    {
        return $this->result;
    }

    public function getSentAt(): ?DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function getRespondedAt(): ?DateTimeImmutable
    {
        return $this->respondedAt;
    }

    public function getDeclineCode(): ?string
    {
        return $this->declineCode;
    }

    public function getDeclineReason(): ?string
    {
        return $this->declineReason;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getRecurringPeriod(): ?int
    {
        return $this->recurringPeriod;
    }

    public function sent(string $lastOperation): void
    {
        $this->lastOperation = $lastOperation;
        $this->result = PaymentResult::sentToCcbill();
        $this->sentAt = new DateTimeImmutable();
    }

    public function cancel(): void
    {
        $this->subscriptionStatus = SubscriptionStatus::canceled();
    }

    public function cancelPayment(): void
    {
        if ($this->isSubscription()) {
            $this->cancel();
            $this->nextRenewalDate = null;
        }

        $this->result = PaymentResult::canceled();
    }

    private function accepted(): void
    {
        $this->result = PaymentResult::accepted();
    }

    private function denied(): void
    {
        $this->result = PaymentResult::denied();
    }

    public function getLastOperation(): ?string
    {
        return $this->lastOperation;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @return PaymentItem[]|Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(PaymentItem $item): void
    {
        $this->getItems()->add($item);
    }

    public function getCardId(): ?int
    {
        return $this->cardId;
    }

    /**
     * @return Transaction[]|Collection
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    private function updateFromTransaction(CreateTransactionCommand $command): void
    {
        $this->transId = $command->getExternalId();
        $this->response = json_encode($command->getResponse());
        $this->respondedAt = new DateTimeImmutable();
    }

    /**
     * @param string[] $response
     */
    public function updateErrorFromSecurionpay(CreateTransactionCommand $command): void
    {
        $this->updateFromTransaction($command);
        $this->denied();
        $this->declineCode = $command->getResponse()['code'];
        $this->declineReason = $command->getResponse()['message'];
    }

    public function updateSuccessfulFromTransaction(CreateTransactionCommand $command): void
    {
        $this->updateFromTransaction($command);
        $this->accepted();
        foreach ($this->getItems() as $item) {
            foreach ($item->getServiceTypes() as $serviceType) {
                switch ($serviceType) {
                    case ServiceType::escort():
                    case ServiceType::escortSponsor():
                    case ServiceType::escortSide():
                        $this->activateSubscription();
                        return;
                    case ServiceType::escortSticky():
                        if ($this->getRecurringPeriod() !== self::EXPIRE_IN_DAYS_WITHOUT_RECURRING_PERIOD) {
                            $this->activateSubscription();
                        }
                        return;
                }
            }
        }
    }

    private function activateSubscription(): void
    {
        $this->subscriptionStatus = SubscriptionStatus::live();
        $this->nextRenewalDate = null;
        if (null !== $this->getRecurringPeriod()) {
            $this->nextRenewalDate = new DateTimeImmutable('+' . $this->getRecurringPeriod() . ' days');
        }

        $this->recurringAmount = $this->amount;
    }

    public function isSubscription(): bool
    {
        return $this->getSubscriptionStatus() !== null;
    }

    public function canRefund(int $amount): bool
    {
        $amount = $this->convertIntToFloat($amount);
        if ($this->amount < 0.01) {
            return false;
        }

        if ($amount > ($this->amount + 0.001)) {
            return false;
        }

        return true;
    }

    public function canFullRefund(int $amount): bool
    {
        return $this->amount === $this->convertIntToFloat($amount);
    }

    private function convertIntToFloat(int $amount): float
    {
        return (float) number_format($amount / 100, 2, ".", "");
    }
}
