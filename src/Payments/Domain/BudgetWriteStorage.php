<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface BudgetWriteStorage
{
    public function create(Budget $budget): void;
}
