<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class EscortPaymentStatus extends Enumerable
{
    public static function new(): self
    {
        return self::createEnum('N');
    }

    public static function upgrade(): self
    {
        return self::createEnum('U');
    }

    public static function renewal(): self
    {
        return self::createEnum('R');
    }
}
