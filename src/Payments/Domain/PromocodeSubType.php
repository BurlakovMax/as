<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class PromocodeSubType extends Enumerable
{
    public static function femaleEscort(): self
    {
        return self::createEnum(1);
    }

    public static function shemaleEscort(): self
    {
        return self::createEnum(2);
    }

    public static function bodyRubs(): self
    {
        return self::createEnum(6);
    }

    public static function none(): self
    {
        return self::createEnum(0);
    }

    /** @deprecated */
    public static function helpWantedAds(): self
    {
        return self::createEnum(14);
    }
}
