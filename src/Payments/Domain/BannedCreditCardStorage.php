<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\Application\Search\SearchQuery;

interface BannedCreditCardStorage
{
    public function add(BannedCreditCard $bannedCreditCard): void;

    /**
     * @return BannedCreditCard[]
     */
    public function getListBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;
}
