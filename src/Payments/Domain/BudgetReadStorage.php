<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\Application\Search\SearchQuery;

interface BudgetReadStorage
{
    public function getByAccountId(int $accountId): ?Budget;

    /**
     * @return Budget[]
     */
    public function getBySearchQuery(SearchQuery $searchQuery): array;

    public function countBySearchQuery(SearchQuery $searchQuery): int;
}
