<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class CardholderName
{
    /**
     * @ORM\Column(name="firstname", type="string", length=50)
     */
    private string $firstName;

    /**
     * @ORM\Column(name="lastname", type="string", length=50)
     */
    private string $lastName;

    public function __construct(
        string $firstName,
        string $lastName
    ) {
        if ($firstName === '' || $lastName === '') {
            throw new \Exception('Cardholder can\'t have empty First or Last name');
        }
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getFullName(): string
    {
        return "{$this->firstName} {$this->lastName}";
    }
}
