<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreateBudgetCommand;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\PhoneNumber;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\BudgetDoctrineRepository")
 * @ORM\Table(name="advertise_budget")
 */
class Budget
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="account_id", type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(name="budget", type="float", length=11)
     */
    private float $amount;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    private int $recurring;

    /**
     * @ORM\Column(name="cc_id", type="integer", length=11, nullable=true)
     */
    private ?int $creditCardId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $recurringAmount;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $recurringTotal;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $recurringMax;

    /**
     * @ORM\Column(name="budget_notify", type="integer", length=11)
     */
    private int $notifyAmount;

    /**
     * @ORM\Column(name="last_notify_stamp", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $lastNotify;

    /**
     * @ORM\Column(name="nickname", type="string", length=30, nullable=true)
     */
    private ?string $nickName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private ?PhoneNumber $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $contact;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $notes;

    public function __construct(CreateBudgetCommand $command) {
        $this->accountId = $command->getAccountId();
        $this->amount = $command->getAmount();
        $this->recurring = $command->getRecurring();
        $this->creditCardId = $command->getCreditCardId();
        $this->recurringAmount = $command->getRecurringAmount();
        $this->recurringTotal = $command->getRecurringTotal();
        $this->recurringMax = $command->getRecurringMax();
        $this->notifyAmount = $command->getNotifyAmount();
        $this->lastNotify = null;
        $this->nickName = $command->getNickName();
        $this->name = $command->getName();
        $this->phone = $command->getPhone();
        $this->company = $command->getCompany();
        $this->contact = $command->getContact();
        $this->notes = $command->getNotes();
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getRecurring(): int
    {
        return $this->recurring;
    }

    public function getCreditCardId(): ?int
    {
        return $this->creditCardId;
    }

    public function getRecurringAmount(): int
    {
        return $this->recurringAmount;
    }

    public function getRecurringTotal(): int
    {
        return $this->recurringTotal;
    }

    public function getRecurringMax(): int
    {
        return $this->recurringMax;
    }

    public function getNotifyAmount(): int
    {
        return $this->notifyAmount;
    }

    public function getLastNotify(): ?\DateTimeImmutable
    {
        return $this->lastNotify;
    }

    public function getNickName(): ?string
    {
        return $this->nickName;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPhone(): ?PhoneNumber
    {
        return $this->phone;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }
}
