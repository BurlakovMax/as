<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreatePromocodeCommand;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\PromocodeDoctrineRepository")
 * @ORM\Table(name="classifieds_promocodes")
 */
class Promocode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private string $code;

    /**
     * @ORM\Column(type="promocode_category", length=20)
     */
    private PromocodeCategory $section;

    /**
     * @ORM\Column(name="subcat", type="promocode_subtype", length=11)
     */
    private PromocodeSubType $escortType;

    /**
     * @ORM\Column(type="integer", length=11, name="`left`")
     */
    private int $codeCount;

    /**
     * @ORM\Column(type="integer", length=4, name="can_auto_renew")
     */
    private bool $isAutoRenewEnable;

    /**
     * @ORM\Embedded(class="App\Payments\Domain\PromocodeType", columnPrefix=false)
     */
    private PromocodeType $type;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    private int $limitPerAccount;

    /**
     * @ORM\Column(type="integer", length=4, name="loclimit")
     */
    private int $escortLocationLimit;

    /**
     * @ORM\Embedded(class="App\Payments\Domain\PromocodeSponsorship", columnPrefix=false)
     */
    private PromocodeSponsorship $sponsorship;

    /**
     * @ORM\Column(type="integer", length=5, name="reposts")
     */
    private int $escortAutoRepost;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    private int $recurring;

    /**
     * @ORM\Column(type="datetime_immutable", name="added")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="timestamp", length=11, name="deleted")
     */
    private ?\DateTimeImmutable $deletedAt;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $createdBy;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private ?int $deletedBy;

    /**
     * @ORM\Column(name="day", type="integer", length=3)
     */
    private int $codeWorkingDays;

    public function __construct(
        CreatePromocodeCommand $command
    ) {
        $this->code = $command->getCode();
        $this->section = PromocodeCategory::getValueOf(mb_strtolower($command->getSection()));
        $this->codeCount = $command->getCodeCount();
        $this->isAutoRenewEnable = $command->isAutoRenewEnable();
        $this->type = new PromocodeType(
            $command->isFreeEscortAvailable(),
            $command->getDiscountAmount(),
            $command->getDiscountPercentage(),
            $command->getClassifiedUp(),
            $command->getFixedPrice(),
            $command->isTracking()
        );
        $this->limitPerAccount = $command->getLimitPerAccount();
        $this->escortLocationLimit = $command->getEscortLocationLimit();
        $this->sponsorship = new PromocodeSponsorship(
            $command->isCityThumbnail(),
            $command->isSideSponsor(),
            $command->isSponsor()
        );
        $this->escortAutoRepost = $command->getEscortAutoRepost();
        $this->recurring = $command->getRecuring();
        $this->createdAt = new \DateTimeImmutable();
        $this->deletedAt = null;
        $this->createdBy = $command->getCreatedBy();
        $this->deletedBy = null;
        $this->codeWorkingDays = $command->getCodeWorkingDays();
        $this->escortType = PromocodeSubType::getValueOf($command->getEscortType());
    }

    public function delete(int $deletedBy): void
    {
        $this->deletedAt = new \DateTimeImmutable();
        $this->deletedBy = $deletedBy;
    }

    public function isActive(): bool
    {
        if (
            null !== $this->deletedAt ||
            0 === $this->codeCount
        ) {
            return false;
        }

        return true;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getSection(): PromocodeCategory
    {
        return $this->section;
    }

    public function getCodeCount(): int
    {
        return $this->codeCount;
    }

    public function isAutoRenewEnable(): bool
    {
        return $this->isAutoRenewEnable;
    }

    public function getLimitPerAccount(): int
    {
        return $this->limitPerAccount;
    }

    public function getEscortLocationLimit(): int
    {
        return $this->escortLocationLimit;
    }

    public function getEscortAutoRepost(): int
    {
        return $this->escortAutoRepost;
    }

    public function getRecurring(): int
    {
        return $this->recurring;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy ?? null;
    }

    public function getDeletedBy(): ?int
    {
        return $this->deletedBy;
    }

    public function isCityThumbnail(): bool
    {
        return $this->getSponsorship()->isCityThumbnail();
    }

    public function isSideSponsor(): bool
    {
        return $this->getSponsorship()->isSideSponsor();
    }

    public function isSponsor(): bool
    {
        return $this->getSponsorship()->isSponsor();
    }

    private function getSponsorship(): PromocodeSponsorship
    {
        return $this->sponsorship;
    }

    public function isFreeEscortAvailable(): bool
    {
        return $this->getType()->isFreeEscortAvailable();
    }

    public function getDiscountAmount(): float
    {
        return $this->getType()->getDiscountAmount();
    }

    public function getDiscountPercentage(): float
    {
        return $this->getType()->getDiscountPercentage();
    }

    public function isClassifiedUp(): int
    {
        return $this->getType()->isClassifiedUp();
    }

    public function getFixedPrice(): float
    {
        return $this->getType()->getFixedPrice();
    }

    public function isTracking(): bool
    {
        return $this->getType()->isTracking();
    }

    private function getType(): PromocodeType
    {
        return $this->type;
    }

    public function getTypeName(): string
    {
        return $this->getType()->getPromocodeType();
    }

    public function getCodeWorkingDays(): int
    {
        return $this->codeWorkingDays;
    }

    public function getEscortType(): PromocodeSubType
    {
        return $this->escortType;
    }
}
