<?php

declare(strict_types=1);

namespace App\Payments\Domain\Logs;

interface LogWriteStorage
{
    public function add(Log $log): void;
}