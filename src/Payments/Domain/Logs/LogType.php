<?php

declare(strict_types=1);

namespace App\Payments\Domain\Logs;

use App\Core\DomainSupport\Enumerable;

final class LogType extends Enumerable
{
    public static function paid(): self
    {
        return self::createEnum('C');
    }

    public static function subscriptionCanceled(): self
    {
        return self::createEnum('SC');
    }

    public static function refunded(): self
    {
        return self::createEnum('R');
    }

    public static function chargeback(): self
    {
        return self::createEnum('CH');
    }

    public static function renewalFailed(): self
    {
        return self::createEnum('RF');
    }

    public static function renewalSuccess(): self
    {
        return self::createEnum('RS');
    }
}