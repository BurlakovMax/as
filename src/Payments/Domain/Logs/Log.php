<?php

declare(strict_types=1);

namespace App\Payments\Domain\Logs;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\Logs\LogDoctrineRepository")
 * @ORM\Table(name="payment_event")
 */
class Log
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", length=11)
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $paymentId;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $transactionId;

    /**
     * @ORM\Column(name="author_id", type="integer", length=11, nullable=true)
     */
    private ?int $accountId;

    /**
     * @ORM\Column(name="stamp", type="integer")
     */
    private int $createdAt;

    /**
     * @ORM\Column(type="log_type")
     */
    private LogType $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $message;

    private function __construct(
        int $paymentId,
        LogType $type,
        int $accountId,
        string $message,
        ?int $transactionId = null
    ) {
        $this->id = 0;
        $this->paymentId = $paymentId;
        $this->transactionId = $transactionId;
        $this->accountId = $accountId;
        $this->createdAt = time();//new \DateTimeImmutable();
        $this->type = $type;
        $this->message = $message;
    }

    public static function paid(
        int $paymentId,
        int $accountId,
        int $transactionId,
        string $message
    ): self {

        return
            new self(
                $paymentId,
                LogType::paid(),
                $accountId,
                $message,
                $transactionId
            )
        ;
    }

    public static function refund(
        int $paymentId,
        int $accountId,
        int $transactionId,
        string $message
    ): self {

        return
            new self(
                $paymentId,
                LogType::refunded(),
                $accountId,
                $message,
                $transactionId
            )
        ;
    }

    public static function subscriptionCanceled(
        int $paymentId,
        int $accountId,
        string $message
    ): self {

        return
            new self(
                $paymentId,
                LogType::paid(),
                $accountId,
                $message
            )
        ;
    }
}