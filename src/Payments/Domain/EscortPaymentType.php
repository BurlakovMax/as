<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class EscortPaymentType extends Enumerable
{
    public static function escort(): self
    {
        return self::createEnum('classified');
    }

    public static function topUps(): self
    {
        return self::createEnum('repost');
    }

    public static function placeowner(): self
    {
        return self::createEnum('businessowner');
    }

    public static function advertise(): self
    {
        return self::createEnum('advertise');
    }
}
