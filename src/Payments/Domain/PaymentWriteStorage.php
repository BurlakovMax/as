<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface PaymentWriteStorage
{
    public function add(Payment $subscription): void;

    public function getAndLockByLastOperation(string $lastOperation): ?Payment;

    public function getAndLock(int $id): ?Payment;
}