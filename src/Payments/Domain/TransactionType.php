<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class TransactionType extends Enumerable
{
    public static function normal(): self
    {
        return self::createEnum('N');
    }

    public static function wire(): self
    {
        return self::createEnum('W');
    }

    public static function manual(): self
    {
        return self::createEnum('M');
    }
}
