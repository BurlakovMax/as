<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface EscortStorage
{
    public function activate(int $escortId): void;

    public function getRecurringPeriod(): int;
}