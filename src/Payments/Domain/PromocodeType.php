<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class PromocodeType
{
    /**
     * @ORM\Column(name="free", type="integer", length=4)
     */
    private bool $freeEscortAvailable;

    /**
     * @ORM\Column(name="discount", type="float")
     */
    private ?float $discountAmount;

    /**
     * @ORM\Column(name="percentage", type="integer", length=11)
     */
    private ?float $discountPercentage;

    /**
     * @ORM\Column(name="addup", type="integer", length=6)
     */
    private int $classifiedUp;

    /**
     * @ORM\Column(name="fixedprice", type="float")
     */
    private ?float $fixedPrice;

    /**
     * @ORM\Column(type="integer", length=3)
     */
    private bool $tracking;

    public function __construct(
        bool $freeEscortAvailable,
        ?float $discountAmount,
        ?float $discountPercentage,
        int $classifiedUp,
        ?float $fixedPrice,
        bool $tracking
    ) {
        $this->freeEscortAvailable = $freeEscortAvailable;
        $this->discountAmount = $discountAmount;
        $this->discountPercentage = $discountPercentage;
        $this->classifiedUp = $classifiedUp;
        $this->fixedPrice = $fixedPrice;
        $this->tracking = $tracking;
    }

    public function isFreeEscortAvailable(): bool
    {
        return $this->freeEscortAvailable;
    }

    public function getDiscountAmount(): float
    {
        return $this->discountAmount;
    }

    public function getDiscountPercentage(): float
    {
        return $this->discountPercentage;
    }

    public function isClassifiedUp(): int
    {
        return $this->classifiedUp;
    }

    public function getFixedPrice(): float
    {
        return $this->fixedPrice;
    }

    public function isTracking(): bool
    {
        return $this->tracking;
    }

    public function getPromocodeType(): string
    {
        foreach ($this as $type => $value) {
            if (0 === $value) {
                continue;
            }
            return $type;
        }
    }

    public function getPromocodeDiscount(): int
    {
        foreach ($this as $value) {
            if (0 === $value) {
                continue;
            }
            return $value;
        }
    }
}
