<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface CreditCardWriteStorage
{
    public function add(CreditCard $creditCard): void;

    public function getAndLock(int $id): ?CreditCard;
}
