<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\BannedCreditCardCommand;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\BannedCreditCardDoctrineRepository")
 * @ORM\Table(name="account_cc_ban")
 */
class BannedCreditCard
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", length=11)
     */
    private int $id;

    /**
     * @ORM\Column(name="cc", type="string", length=16)
     */
    private string $cardNumber;

    /**
     * @ORM\Column(name="month", type="string", length=4)
     */
    private ?string $expiryMonth;

    /**
     * @ORM\Column(name="year", type="string", length=4)
     */
    private ?string $expiryYear;

    /**
     * @ORM\Column(name="cvc", type="string", length=4)
     */
    private ?string $cvc2;

    /**
     * @ORM\Embedded(class="App\Payments\Domain\CardholderName", columnPrefix=false)
     */
    private CardholderName $cardholderName;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private string $address;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private string $zipcode;

    /**
     * @ORM\Column(type="float")
     */
    private float $total;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $city;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $state;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $country;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $shared;

    /**
     * @ORM\Column(name="reason", type="text", nullable=true)
     */
    private ?string $banReason;

    /**
     * @ORM\Column(type="datetime_immutable", name="time")
     */
    private \DateTimeImmutable $bannedTime;

    public function __construct(BannedCreditCardCommand $command)
    {
        $this->cardNumber = $command->getCardNumber();
        $this->expiryMonth = $command->getExpiryMonth();
        $this->expiryYear = $command->getExpiryYear();
        $this->cvc2 = $command->getCvc2();
        $this->cardholderName = new CardholderName(
            $command->getFirstname(),
            $command->getLastname()
        );
        $this->address = $command->getAddress();
        $this->zipcode = $command->getZipcode();
        $this->total = $command->getTotal();
        $this->city = $command->getCity();
        $this->state = $command->getState();
        $this->country = $command->getCountry();
        $this->shared = false;
        $this->banReason = $command->getBanReason();
        $this->bannedTime = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    public function getExpiryMonth(): ?string
    {
        return $this->expiryMonth;
    }

    public function getExpiryYear(): ?string
    {
        return $this->expiryYear;
    }

    public function getCvc2(): ?string
    {
        return $this->cvc2;
    }

    public function getCardholderName(): CardholderName
    {
        return $this->cardholderName;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function isShared(): bool
    {
        return $this->shared;
    }

    public function getBanReason(): ?string
    {
        return $this->banReason;
    }

    public function getBannedTime(): \DateTimeImmutable
    {
        return $this->bannedTime;
    }
}
