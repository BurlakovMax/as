<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class PromocodeCategory extends Enumerable
{
    public static function escort(): self
    {
        return self::createEnum('classifieds');
    }

    public static function businessowner(): self
    {
        return self::createEnum('businessowner');
    }

    public static function escort2(): self
    {
        return self::createEnum('Classifieds');
    }

    public static function any(): self
    {
        return self::createEnum('any');
    }
}
