<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface PromocodeUsageReadStorage
{
    public function getByCodeIdAndAccountId(int $codeId, int $accountId): ?PromocodeUsage;
}
