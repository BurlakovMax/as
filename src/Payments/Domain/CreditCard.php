<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreateCreditCardCommand;
use App\Payments\Application\CreateTransactionCommand;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Payments\Infrastructure\Persistence\CreditCardDoctrineRepository")
 * @ORM\Table(name="account_cc")
 */
class CreditCard
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="cc_id", type="integer", length=11)
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=8)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="credit_card_type")
     */
    private ?CreditCardType $type;

    /**
     * @ORM\Column(name="cc", type="string", length=16)
     */
    private ?string $cardNumber;

    /**
     * @ORM\Column(name="expmonth", type="string", length=4)
     */
    private ?string $expiryMonth;

    /**
     * @ORM\Column(name="expyear", type="string", length=4)
     */
    private ?string $expiryYear;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private ?string $cvc2;

    /**
     * @ORM\Embedded(class="App\Payments\Domain\CardholderName", columnPrefix=false)
     */
    private CardholderName $cardholderName;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private string $address;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private string $zipcode;

    /**
     * @ORM\Column(type="float")
     */
    private ?float $total;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $city;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $state;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $country;

    /**
     * @ORM\Column(name="bin_info", type="text")
     */
    private ?string $binInfo;

    /**
     * @ORM\Column(name="gift_prepaid", type="integer", length=3)
     */
    private ?int $giftPrepaid;

    /**
     * @ORM\Column(name="securionpay_token", type="string", length=100)
     */
    private ?string $securionpayToken;

    /**
     * @ORM\Column(name="securionpay_customer_id", type="string", length=255)
     */
    private ?string $securionpayCustomerId;

    /**
     * @ORM\Column(name="securionpay_card_id", type="string", length=255)
     */
    private ?string $securionpayCardId;

    /**
     * @ORM\Column(name="customer_id", type="string", length=36, nullable=true)
     */
    private ?string $customerId;

    /**
     * @ORM\Column(name="card_id", type="string", length=36, nullable=true)
     */
    private ?string $cardId;

    /**
     * @ORM\Column(name="stripe_token", type="string", length=100)
     */
    private ?string $stripeToken;

    /**
     * @ORM\Column(name="image_front", type="string", length=255)
     */
    private ?string $imageFront;

    /**
     * @ORM\Column(name="image_back", type="string", length=255, nullable=true)
     */
    private ?string $imageBack;

    /**
     * @ORM\Column(name="approved_stamp", type="timestamp", nullable=true)
     */
    private ?DateTimeImmutable $approvedStamp;

    /**
     * @ORM\Column(name="approved_by", type="integer", length=11)
     */
    private ?int $approvedBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $shared;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $banned;

    /**
     * @ORM\Column(name="banned_stamp", type="timestamp")
     */
    private ?DateTimeImmutable $bannedStamp;

    /**
     * @ORM\Column(name="banned_by", type="integer", length=11)
     */
    private ?int $bannedBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $deleted;

    /**
     * @ORM\Column(name="deleted_stamp", type="timestamp")
     */
    private ?DateTimeImmutable $deletedStamp;

    /**
     * @ORM\Column(name="deleted_by", type="integer", length=11)
     */
    private ?int $deletedBy;

    public function __construct(CreateCreditCardCommand $command)
    {
        $this->id = 0;
        $this->accountId = $command->getAccountId();
        $this->type = null;
        $this->cardNumber = $command->getCardNumber();
        $this->expiryMonth = $command->getExpiryMonth();
        $this->expiryYear = $command->getExpiryYear();
        $this->cvc2 = $command->getCvc2();
        $this->cardholderName = new CardholderName(
            $command->getFirstname(),
            $command->getLastname(),
        );
        $this->address = $command->getAddress();
        $this->zipcode = $command->getZipcode();
        $this->total = null;
        $this->city = $command->getCity();
        $this->state = $command->getState();
        $this->country = $command->getCountry();
        $this->binInfo = null;
        $this->giftPrepaid = null;
        $this->securionpayToken = null;
        $this->securionpayCustomerId = null;
        $this->customerId = null;
        $this->cardId = null;
        $this->securionpayCardId = null;
        $this->stripeToken = null;
        $this->imageFront = null;
        $this->imageBack = null;
        $this->approvedBy = null;
        $this->shared = false;
        $this->banned = false;
        $this->bannedBy = null;
        $this->deleted = false;
        $this->deletedBy = null;
        $this->approvedStamp = null;
        $this->bannedStamp = null;
        $this->deletedStamp = null;
        $this->subscriptions = new ArrayCollection();
    }

    /**
     * @return Payment[]|Collection
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getType(): ?CreditCardType
    {
        return $this->type;
    }

    public function getCardNumber(): ?string
    {
        return $this->cardNumber;
    }

    public function getExpiryMonth(): ?string
    {
        return $this->expiryMonth;
    }

    public function getExpiryYear(): ?string
    {
        return $this->expiryYear;
    }

    public function getCvc2(): ?string
    {
        return $this->cvc2;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getBinInfo(): ?string
    {
        return $this->binInfo;
    }

    public function getGiftPrepaid(): ?int
    {
        return $this->giftPrepaid;
    }

    public function getSecurionpayToken(): ?string
    {
        return $this->securionpayToken;
    }

    public function getSecurionpayCustomerId(): ?string
    {
        return $this->securionpayCustomerId;
    }

    public function getSecurionpayCardId(): ?string
    {
        return $this->securionpayCardId;
    }

    public function getStripeToken(): ?string
    {
        return $this->stripeToken;
    }

    public function getImageFront(): ?string
    {
        return $this->imageFront;
    }

    public function getImageBack(): ?string
    {
        return $this->imageBack;
    }

    public function getApprovedStamp(): ?DateTimeImmutable
    {
        return $this->approvedStamp;
    }

    public function getApprovedBy(): ?int
    {
        return $this->approvedBy;
    }

    public function getShared(): bool
    {
        return $this->shared;
    }

    public function getBanned(): bool
    {
        return $this->banned;
    }

    public function getBannedStamp(): ?DateTimeImmutable
    {
        return $this->bannedStamp;
    }

    public function getBannedBy(): ?int
    {
        return $this->bannedBy;
    }

    public function getDeleted(): bool
    {
        return $this->deleted;
    }

    public function getDeletedStamp(): ?DateTimeImmutable
    {
        return $this->deletedStamp;
    }

    public function getDeletedBy(): ?int
    {
        return $this->deletedBy;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function getCardId(): ?string
    {
        return $this->cardId;
    }

    public function getCardholderName(): string
    {
        return $this->cardholderName->getFullName();
    }

    public function getFirstname(): string
    {
        return $this->cardholderName->getFirstName();
    }

    public function getLastname(): string
    {
        return $this->cardholderName->getLastName();
    }

    private function updateFromTransaction(CreateTransactionCommand $command): void
    {
        $this->customerId = $command->getCustomerId();
        $this->cardId = $command->getCardId();
        $this->cvc2 = 'xx' . substr($this->cvc2, -1);
        $this->cardNumber = substr($this->cardNumber, 0, 6) . 'xxxxxx' . substr($this->cardNumber, -4);
    }

    public function updateErrorFromTransaction(CreateTransactionCommand $command): void
    {
        $this->updateFromTransaction($command);
    }

    public function updateSuccessfulCustomerIdAndCardIdFromSecurionpay(CreateTransactionCommand $command): void
    {
        $this->updateFromTransaction($command);
        $this->securionpayCustomerId = $command->getResponse()['customerId'];
        $this->securionpayCardId = $command->getResponse()['card']['id'];
        $this->approvedStamp = new DateTimeImmutable();
    }
}
