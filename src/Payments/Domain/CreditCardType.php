<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class CreditCardType extends Enumerable
{
    public static function visa(): self
    {
        return self::createEnum('VI');
    }

    public static function americanExpress(): self
    {
        return self::createEnum('AM');
    }

    public static function masterCard(): self
    {
        return self::createEnum('MC');
    }

    public static function discover(): self
    {
        return self::createEnum('DI');
    }

    public static function JCB(): self
    {
        return self::createEnum('JC');
    }

    public static function maestro(): self
    {
        return self::createEnum('MA');
    }

    public static function dinersClub(): self
    {
        return self::createEnum('DC');
    }
}
