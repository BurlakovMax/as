<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Payments\Application\CreateTransactionCommand;
use App\Payments\Application\CreditCardData;

interface PaymentOperations
{
    public function pay(Payment $payment, CreditCardData $creditCard): CreateTransactionCommand;

    public function payWithCustomerData(Payment $payment, CreditCardData $creditCard): CreateTransactionCommand;

    public function refund(Payment $payment, int $amount): CreateTransactionCommand;
}
