<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface TransactionWriteStorage
{
    public function add(Transaction $transaction): void;

    public function getAndLock(int $id): ?Transaction;
}
