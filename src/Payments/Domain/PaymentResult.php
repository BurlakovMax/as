<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class PaymentResult extends Enumerable
{
    public static function initial(): self
    {
        return self::createEnum('I');
    }

    public static function sentToCcbill(): self
    {
        return self::createEnum('S');
    }

    public static function denied(): self
    {
        return self::createEnum('D');
    }

    public static function accepted(): self
    {
        return self::createEnum('A');
    }

    public static function canceled(): self
    {
        return self::createEnum('C');
    }

    public static function unknown(): self
    {
        return self::createEnum('U');
    }
}
