<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\DomainSupport\Enumerable;

final class SubscriptionStatus extends Enumerable
{
    public static function live(): self
    {
        return self::createEnum(1);
    }

    public static function canceled(): self
    {
        return self::createEnum(2);
    }
}
