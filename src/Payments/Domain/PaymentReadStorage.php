<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\Application\Search\SearchQuery;

interface PaymentReadStorage
{
    public function get(int $id): ?Payment;

    /**
     * @return Payment[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function countByAccountId(int $accountId): int;
}
