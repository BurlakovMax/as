<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface PromocodeUsageWriteStorage
{
    public function add(PromocodeUsage $promocodeUsage): void;
}
