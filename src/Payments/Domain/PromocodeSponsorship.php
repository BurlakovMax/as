<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class PromocodeSponsorship
{
    /**
     * @ORM\Column(name="citythumbnail", type="integer", length=3)
     */
    private bool $isCityThumbnail;

    /**
     * @ORM\Column(name="sidesponsor", type="integer", length=3)
     */
    private bool $isSideSponsor;

    /**
     * @ORM\Column(name="setassponsor", type="string", length=4)
     */
    private bool $isSponsor;

    public function __construct(bool $isCityThumbnail, bool $isSideSponsor, bool $isSponsor)
    {
        $this->isCityThumbnail = $isCityThumbnail;
        $this->isSideSponsor = $isSideSponsor;
        $this->isSponsor = $isSponsor;
    }

    public function isCityThumbnail(): bool
    {
        return $this->isCityThumbnail;
    }

    public function isSideSponsor(): bool
    {
        return $this->isSideSponsor;
    }

    public function isSponsor(): bool
    {
        return $this->isSponsor;
    }
}
