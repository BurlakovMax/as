<?php

declare(strict_types=1);

namespace App\Payments\Domain;

use App\Core\Application\Search\SearchQuery;

interface PromocodeStorage
{
    public function getById(int $id): ?Promocode;

    public function getByCode(string $code): ?Promocode;

    /**
     * @return Promocode[]
     */
    public function getListBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function delete(Promocode $promocode): void;

    public function add(Promocode $promocode): void;
}
