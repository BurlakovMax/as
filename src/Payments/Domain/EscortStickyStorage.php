<?php

declare(strict_types=1);

namespace App\Payments\Domain;

interface EscortStickyStorage
{
    public function activateAfterPaid(int $escortId, int $locationId, int $days): void;

    public function getRecurringPeriod(): int;
}