<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\PaymentItem;
use App\Payments\Domain\Transaction;
use App\Payments\Domain\TransactionType;
use DateTimeImmutable;
use Swagger\Annotations as SWG;

final class TransactionData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    private int $paymentId;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $createdAt;

    /**
     * @SWG\Property(type="string")
     */
    private ?TransactionType $type;

    /**
     * @SWG\Property()
     */
    private ?string $transId;

    /**
     * @SWG\Property()
     */
    private int $amount;

    /**
     * @SWG\Property()
     */
    private ?int $cardId;

    /**
     * @var PaymentItemData[]
     */
    private array $items;

    public function __construct(Transaction $transaction)
    {
        $this->id = $transaction->getId();
        $this->paymentId = $transaction->getPaymentId();
        $this->createdAt = $transaction->getCreatedAt();
        $this->type = $transaction->getType();
        $this->transId = $transaction->getTransId();
        $this->amount = (int)$transaction->getAmount()->getAmount();
        $this->cardId = $transaction->getPayment()->getCardId();
        $this->items = array_map(
            function (PaymentItem $item): PaymentItemData {
                return new PaymentItemData($item);
            },
            $transaction->getItems()->toArray()
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPaymentId(): int
    {
        return $this->paymentId;
    }

    public function getCardId(): ?int
    {
        return $this->cardId;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getType(): ?string
    {
        return $this->type ? $this->type->getRawValue() : null;
    }

    public function getTransId(): ?string
    {
        return $this->transId;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return PaymentItemData[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
