<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\Application\Search\SearchQuery;
use App\Payments\Domain\Transaction;
use App\Payments\Domain\TransactionReadStorage;

final class TransactionQueryProcessor
{
    private TransactionReadStorage $transactionReadStorage;

    public function __construct(TransactionReadStorage $transactionReadStorage)
    {
        $this->transactionReadStorage = $transactionReadStorage;
    }

    public function countByAccountId(int $accountId): int
    {
        return $this->transactionReadStorage->countByAccountId($accountId);
    }

    /**
     * @return TransactionData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        return array_map([ $this, 'mapToData' ], $this->transactionReadStorage->getBySearchQuery($query));
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->transactionReadStorage->countBySearchQuery($query);
    }

    private function mapToData(Transaction $transaction): TransactionData
    {
        return new TransactionData($transaction);
    }
}
