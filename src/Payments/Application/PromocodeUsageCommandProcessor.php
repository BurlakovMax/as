<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\PromocodeUsage;
use App\Payments\Domain\PromocodeUsageWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class PromocodeUsageCommandProcessor
{
    private PromocodeUsageWriteStorage $storage;

    private TransactionManager $transactionManager;

    public function __construct(PromocodeUsageWriteStorage $storage, TransactionManager $transactionManager)
    {
        $this->storage = $storage;
        $this->transactionManager = $transactionManager;
    }

    public function add(
        int $codeId,
        int $accountId,
        string $ipAddress,
        float $paid,
        float $saved
    ) {
        $this->transactionManager->transactional(function () use ($codeId, $accountId, $ipAddress, $paid, $saved): void {
           $this->storage->add(
               new PromocodeUsage(
                   $codeId,
                   $accountId,
                   $ipAddress,
                   $paid,
                   $saved
               ));
        });
    }
}
