<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\Application\Search\SearchQuery;
use App\Payments\Domain\EscortPaymentStatus;
use App\Payments\Domain\PaymentItem;
use App\Payments\Domain\PaymentItemReadStorage;

final class PaymentItemQueryProcessor
{
    private PaymentItemReadStorage $paymentItemReadStorage;

    public function __construct(PaymentItemReadStorage $paymentItemReadStorage)
    {
        $this->paymentItemReadStorage = $paymentItemReadStorage;
    }

    /**
     * @return StatisticData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $items = $this->paymentItemReadStorage->getBySearchQuery($query);
        if (empty($items)) {
            return [];
        }

        return $this->calculate($items);
    }

    /**
     * @param PaymentItem[] $items
     * @return StatisticData[]
     */
    private function calculate(array $items): array
    {
        $statistics = [];
        foreach ($items as $item) {
            if (!isset($statistics[$item->getLocationId()])) {
                $this->createStatistic($item,$statistics);
                continue;
            }

            $this->updateStatistic($item,$statistics);
        }

        return $statistics;
    }

    /**
     * @param StatisticData[] $statistics
     */
    private function createStatistic(PaymentItem $item, array &$statistics): void
    {
        switch ($item->getEscortStatus()) {
            case EscortPaymentStatus::new():
                $statistics[$item->getLocationId()] = new StatisticData(
                    1,
                    (float) $item->getPayment()->getAmount()->getAmount(),
                    0,
                    0,
                    0,
                    0,
                    1,
                    (float) $item->getPayment()->getAmount()->getAmount()
                );
                break;
            case EscortPaymentStatus::renewal():
                $statistics[$item->getLocationId()] = new StatisticData(
                    0,
                    0,
                    1,
                    (float) $item->getPayment()->getAmount()->getAmount(),
                    0,
                    0,
                    1,
                    (float) $item->getPayment()->getAmount()->getAmount()
                );
                break;
            case EscortPaymentStatus::upgrade():
                $statistics[$item->getLocationId()] = new StatisticData(
                    0,
                    0,
                    0,
                    0,
                    1,
                    (float) $item->getPayment()->getAmount()->getAmount(),
                    1,
                    (float) $item->getPayment()->getAmount()->getAmount()
                );
                break;
        }
    }

    /**
     * @param StatisticData[] $statistics
     */
    private function updateStatistic(PaymentItem $item, array &$statistics): void
    {
        /** @var StatisticData $statistic */
        $statistic = $statistics[$item->getLocationId()];
        switch ($item->getEscortStatus()) {
            case EscortPaymentStatus::new():
                $statistic->update(
                    $statistic->getCountOfNewEscorts() + 1,
                    $statistic->getAmountOfNewEscorts() + (float) $item->getPayment()->getAmount()->getAmount(),
                    $statistic->getRenewCount(),
                    $statistic->getRenewAmount(),
                    $statistic->getUpgradeCount(),
                    $statistic->getUpgradeAmount(),
                    $statistic->getTotalCount() + 1,
                    $statistic->getTotalAmount() + (float) $item->getPayment()->getAmount()->getAmount(),
                );
                break;
            case EscortPaymentStatus::renewal():
                $statistic->update(
                    $statistic->getCountOfNewEscorts(),
                    $statistic->getAmountOfNewEscorts(),
                    $statistic->getRenewCount() + 1,
                    $statistic->getRenewAmount() + (float) $item->getPayment()->getAmount()->getAmount(),
                    $statistic->getUpgradeCount(),
                    $statistic->getUpgradeAmount(),
                    $statistic->getTotalCount() + 1,
                    $statistic->getTotalAmount() + (float) $item->getPayment()->getAmount()->getAmount(),
                );
                break;
            case EscortPaymentStatus::upgrade():
                $statistic->update(
                    $statistic->getCountOfNewEscorts(),
                    $statistic->getAmountOfNewEscorts(),
                    $statistic->getRenewCount(),
                    $statistic->getRenewAmount(),
                    $statistic->getUpgradeCount() + 1,
                    $statistic->getUpgradeAmount() + (float) $item->getPayment()->getAmount()->getAmount(),
                    $statistic->getTotalCount() + 1,
                    $statistic->getTotalAmount() + (float) $item->getPayment()->getAmount()->getAmount(),
                );
                break;
        }
    }
}