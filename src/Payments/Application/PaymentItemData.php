<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\EscortPaymentType;
use App\Payments\Domain\ServiceType;
use App\Payments\Domain\PaymentItem;
use DateTimeImmutable;
use Swagger\Annotations as SWG;

final class PaymentItemData
{
    /**
     * @SWG\Property()
     */
    private ?int $id;

    /**
     * @SWG\Property()
     */
    private ?int $locationId;

    /**
     * @SWG\Property()
     */
    private ?int $escortId;

    /**
     * @SWG\Property(type="string")
     */
    private ?EscortPaymentType $subjectType;

    /**
     * @SWG\Property()
     */
    private ?bool $sponsor;

    /**
     * @SWG\Property()
     */
    private ?bool $side;

    /**
     * @SWG\Property()
     */
    private ?bool $sticky;

    /**
     * @SWG\Property()
     */
    private ?int $topUps;

    /**
     * @SWG\Property()
     */
    private ?string $placeSection;

    /**
     * @SWG\Property()
     */
    private ?int $placeId;

    /**
     * @SWG\Property()
     */
    private ?bool $placeSponsor;

    /**
     * @SWG\Property()
     */
    private ?int $amount;

    /**
     * @SWG\Property()
     */
    private DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $nextRenewalDate;

    /**
     * @var string[]
     */
    private array $serviceTypes;

    public function __construct(PaymentItem $item)
    {
        $this->id = $item->getId();
        $this->locationId = $item->getLocationId();
        $this->escortId = $item->getEscortId();
        $this->subjectType = $item->getSubjectType();
        $this->sponsor = $item->getSponsor();
        $this->side = $item->getSide();
        $this->sticky = $item->getSticky();
        $this->topUps = $item->getTopUps();
        $this->placeSection = $item->getPlaceSection();
        $this->serviceTypes = array_map(
            fn(ServiceType $type): string => ServiceTypeToUIConverter::getRawType($type), $item->getServiceTypes()
        );
        $this->placeId = $item->getPlaceId();
        $this->placeSponsor = $item->getPlaceSponsor();
        $this->createdAt = $item->getCreatedAt();
        $payment = $item->getPayment();
        if (null !== $payment) {
            $this->amount = (int)$payment->getAmount()->getAmount();
            $this->nextRenewalDate = $payment->getNextRenewalDate();
        }
    }

    public function getLocationId(): ?int
    {
        return $this->locationId;
    }

    /**
     * @return string[]
     */
    public function getServiceTypes(): array
    {
        return $this->serviceTypes;
    }

    public function getSponsor(): ?bool
    {
        return $this->sponsor;
    }

    public function getSide(): ?bool
    {
        return $this->side;
    }

    public function getSticky(): ?bool
    {
        return $this->sticky;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEscortId(): ?int
    {
        return $this->escortId;
    }

    public function getSubjectType(): EscortPaymentType
    {
        return $this->subjectType;
    }

    public function isSponsor(): ?bool
    {
        return $this->sponsor;
    }

    public function isSide(): ?bool
    {
        return $this->side;
    }

    public function isSticky(): ?bool
    {
        return $this->sticky;
    }

    public function getTopUps(): ?int
    {
        return $this->topUps;
    }

    public function getPlaceSection(): ?string
    {
        return $this->placeSection;
    }

    public function getPlaceId(): ?int
    {
        return $this->placeId;
    }

    public function getPlaceSponsor(): ?bool
    {
        return $this->placeSponsor;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getNextRenewalDate(): ?DateTimeImmutable
    {
        return $this->nextRenewalDate;
    }
}
