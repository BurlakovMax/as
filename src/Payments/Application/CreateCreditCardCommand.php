<?php

declare(strict_types=1);

namespace App\Payments\Application;

final class CreateCreditCardCommand
{
    private int $accountId;

    private string $cardNumber;

    private string $expiryMonth;

    private string $expiryYear;

    private string $cvc2;

    private string $firstname;

    private string $lastname;

    private string $address;

    private string $zipcode;

    private string $city;

    private string $state;

    private ?string $country;

    public function __construct(
        int $accountId,
        string $cardNumber,
        string $expiryMonth,
        string $expiryYear,
        string $cvc2,
        string $firstname,
        string $lastname,
        string $address,
        string $zipcode,
        string $city,
        string $state,
        ?string $country
    ) {
        $this->accountId = $accountId;
        $this->cardNumber = $cardNumber;
        $this->expiryMonth = $expiryMonth;
        $this->expiryYear = $expiryYear;
        $this->cvc2 = $cvc2;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->address = $address;
        $this->zipcode = $zipcode;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    public function getExpiryMonth(): string
    {
        return $this->expiryMonth;
    }

    public function getExpiryYear(): string
    {
        return $this->expiryYear;
    }

    public function getCvc2(): string
    {
        return $this->cvc2;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }
}
