<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\ServiceType;
use Ds\Map;

final class ServiceTypeToUIConverter
{
    public static function getRawType(ServiceType $type): string
    {
        $map = new Map();

        $map->put(ServiceType::escortSponsor(), 'City Thumbnail');
        $map->put(ServiceType::escortSide(), 'Side Sponsor');
        $map->put(ServiceType::escortSticky(), 'Sticky Upgrade');
        $map->put(ServiceType::topUps(), 'Top Ups');
        $map->put(ServiceType::escort(), 'Post Ad');
        $map->put(ServiceType::placeowner(), 'Place Owner');
        $map->put(ServiceType::advertise(), 'Advertise');
        $map->put(ServiceType::combo(), 'Combo');

        return $map->get($type);
    }
}