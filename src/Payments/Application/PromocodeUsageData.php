<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\PromocodeUsage;

final class PromocodeUsageData
{
    private int $id;

    private int $codeId;

    private int $accountId;

    private string $ipAddress;

    private \DateTimeImmutable $usedAt;

    private float $paid;

    private float $saved;

    public function __construct(PromocodeUsage $promocodeUsage)
    {
        $this->id = $promocodeUsage->getId();
        $this->codeId = $promocodeUsage->getCodeId();
        $this->accountId = $promocodeUsage->getAccountId();
        $this->ipAddress = $promocodeUsage->getIpAddress();
        $this->usedAt = $promocodeUsage->getUsedAt();
        $this->paid = $promocodeUsage->getPaid();
        $this->saved = $promocodeUsage->getSaved();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCodeId(): int
    {
        return $this->codeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function getUsedAt(): \DateTimeImmutable
    {
        return $this->usedAt;
    }

    public function getPaid(): float
    {
        return $this->paid;
    }

    public function getSaved(): float
    {
        return $this->saved;
    }
}
