<?php

declare(strict_types=1);

namespace App\Payments\Application;

use LazyLemurs\Exceptions\NotFoundException;

final class PromocodeUsageNotFound extends NotFoundException
{

}
