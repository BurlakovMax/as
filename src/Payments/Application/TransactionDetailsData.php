<?php

declare(strict_types=1);

namespace App\Payments\Application;

use Swagger\Annotations as SWG;

final class TransactionDetailsData
{
    /**
     * @SWG\Property()
     */
    private TransactionData $transaction;

    /**
     * @SWG\Property()
     */
    private ?CreditCardData $card;

    public function __construct(TransactionData $transaction, ?CreditCardData $card)
    {
        $this->transaction = $transaction;
        $this->card = $card;
    }

    public function getTransaction(): TransactionData
    {
        return $this->transaction;
    }

    public function getCard(): ?CreditCardData
    {
        return $this->card;
    }
}