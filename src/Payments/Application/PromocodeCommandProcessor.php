<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\Promocode;
use App\Payments\Domain\PromocodeStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class PromocodeCommandProcessor
{
    private TransactionManager $transactionManager;

    private PromocodeStorage $storage;

    public function __construct(TransactionManager $transactionManager, PromocodeStorage $storage)
    {
        $this->transactionManager = $transactionManager;
        $this->storage = $storage;
    }

    public function create(CreatePromocodeCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
            $code = $this->storage->getByCode($command->getCode());

            if (null !== $code) {
                throw new PromocodeAlreadyExist();
            }

            $this->storage->add(new Promocode($command));
        });
    }

    public function delete(int $id, int $deletedBy): void
    {
        $this->transactionManager->transactional(function () use ($id, $deletedBy): void {
            $promocode = $this->storage->getById($id);

            if (null === $promocode) {
                throw new PromocodeNotFound();
            }

            $promocode->delete($deletedBy);
        });
    }
}
