<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\Promocode;
use App\Payments\Domain\PromocodeCategory;

final class PromocodeData
{
    private int $id;

    private string $code;

    private PromocodeCategory $section;

    private int $codeCount;

    private bool $isAutoRenewEnable;

    private int $limitPerAccount;

    private int $escortLocationLimit;

    private int $escortAutoRepost;

    private int $recurring;

    private \DateTimeImmutable $createdAt;

    private ?\DateTimeImmutable $deletedAt;

    private ?int $createdBy;

    private ?int $deletedBy;

    private bool $isCityThumbnail;

    private bool $isSideSponsor;

    private bool $isSponsor;

    private bool $freeEscortAvailable;

    private float $discountAmount;

    private float $discountPercentage;

    private int $classifiedUp;

    private float $fixedPrice;

    private bool $tracking;

    private string $type;

    public function __construct(Promocode $promocode)
    {
        $this->id = $promocode->getId();
        $this->code = $promocode->getCode();
        $this->section = $promocode->getSection();
        $this->codeCount = $promocode->getCodeCount();
        $this->isAutoRenewEnable = $promocode->isAutoRenewEnable();
        $this->limitPerAccount = $promocode->getLimitPerAccount();
        $this->escortLocationLimit = $promocode->getEscortLocationLimit();
        $this->escortAutoRepost = $promocode->getEscortAutoRepost();
        $this->recurring = $promocode->getRecurring();
        $this->createdAt = $promocode->getCreatedAt();
        $this->deletedAt = $promocode->getDeletedAt();
        $this->createdBy = $promocode->getCreatedBy();
        $this->deletedBy = $promocode->getDeletedBy();
        $this->isCityThumbnail = $promocode->isCityThumbnail();
        $this->isSideSponsor = $promocode->isSideSponsor();
        $this->isSponsor = $promocode->isSponsor();
        $this->freeEscortAvailable = $promocode->isFreeEscortAvailable();
        $this->discountAmount = $promocode->getDiscountAmount();
        $this->discountPercentage = $promocode->getDiscountPercentage();
        $this->classifiedUp = $promocode->isClassifiedUp();
        $this->fixedPrice = $promocode->getFixedPrice();
        $this->tracking = $promocode->isTracking();
        $this->type = $promocode->getTypeName();
        $this->isActive = $promocode->isActive();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getSection(): PromocodeCategory
    {
        return $this->section;
    }

    public function getCodeCount(): int
    {
        return $this->codeCount;
    }

    public function isAutoRenewEnable(): bool
    {
        return $this->isAutoRenewEnable;
    }

    public function getLimitPerAccount(): int
    {
        return $this->limitPerAccount;
    }

    public function getEscortLocationLimit(): int
    {
        return $this->escortLocationLimit;
    }

    public function getEscortAutoRepost(): int
    {
        return $this->escortAutoRepost;
    }

    public function getRecurring(): int
    {
        return $this->recurring;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function getDeletedBy(): ?int
    {
        return $this->deletedBy;
    }

    public function isCityThumbnail(): bool
    {
        return $this->isCityThumbnail;
    }

    public function isSideSponsor(): bool
    {
        return $this->isSideSponsor;
    }

    public function isSponsor(): bool
    {
        return $this->isSponsor;
    }

    public function isFreeEscortAvailable(): bool
    {
        return $this->freeEscortAvailable;
    }

    public function getDiscountAmount(): float
    {
        return $this->discountAmount;
    }

    public function getDiscountPercentage(): float
    {
        return $this->discountPercentage;
    }

    public function isClassifiedUp(): int
    {
        return $this->classifiedUp;
    }

    public function getFixedPrice(): float
    {
        return $this->fixedPrice;
    }

    public function isTracking(): bool
    {
        return $this->tracking;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
