<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\PromocodeUsageReadStorage;

final class PromocodeUsageQueryProcessor
{
    private PromocodeUsageReadStorage $storage;

    public function __construct(PromocodeUsageReadStorage $storage)
    {
        $this->storage = $storage;
    }

    public function getByCodeIdAndAccountId(int $codeId, int $accountId): ?PromocodeUsageData
    {
        $codeUsage = $this->storage->getByCodeIdAndAccountId($codeId, $accountId);

        if (null === $codeUsage) {
            throw new PromocodeUsageNotFound();
        }

        return new PromocodeUsageData($codeUsage);
    }
}
