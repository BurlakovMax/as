<?php

declare(strict_types=1);

namespace App\Payments\Application;

use LazyLemurs\Commander\Property;

final class CreatePromocodeCommand
{
    /**
     * @Property()
     */
    private string $code;

    /**
     * @Property()
     */
    private string $section;

    /**
     * @Property()
     */
    private int $escortType;

    /**
     * @Property()
     */
    private int $codeCount;

    /**
     * @Property()
     */
    private bool $isAutoRenewEnable;

    /**
     * @Property()
     */
    private int $limitPerAccount;

    /**
     * @Property()
     */
    private int $escortLocationLimit;

    /**
     * @Property()
     */
    private int $escortAutoRepost;

    /**
     * @Property()
     */
    private int $recuring;

    /**
     * @Property()
     */
    private int $createdBy;

    /**
     * @Property()
     */
    private bool $freeEscortAvailable;

    /**
     * @Property()
     */
    private ?float $discountAmount;

    /**
     * @Property()
     */
    private ?float $discountPercentage;

    /**
     * @Property()
     */
    private int $classifiedUp;

    /**
     * @Property()
     */
    private ?float $fixedPrice;

    /**
     * @Property()
     */
    private bool $ecciePromo;

    /**
     * @Property()
     */
    private bool $tracking;

    /**
     * @Property()
     */
    private bool $isCityThumbnail;

    /**
     * @Property()
     */
    private bool $isSideSponsor;

    /**
     * @Property()
     */
    private bool $isSponsor;

    /**
     * @Property()
     */
    private int $codeWorkingDays;

    public function __construct(
        string $code,
        string $section,
        int $codeCount,
        bool $isAutoRenewEnable,
        int $limitPerAccount,
        int $escortLocationLimit,
        int $escortAutoRepost,
        int $recuring,
        int $createdBy,
        bool $freeEscortAvailable,
        ?float $discountAmount,
        ?float $discountPercentage,
        int $classifiedUp,
        ?float $fixedPrice,
        bool $ecciePromo,
        bool $tracking,
        bool $isCityThumbnail,
        bool $isSideSponsor,
        bool $isSponsor,
        int $codeWorkingDays,
        int $escortType
    ) {
        $this->code = $code;
        $this->section = $section;
        $this->codeCount = $codeCount;
        $this->isAutoRenewEnable = $isAutoRenewEnable;
        $this->limitPerAccount = $limitPerAccount;
        $this->escortLocationLimit = $escortLocationLimit;
        $this->escortAutoRepost = $escortAutoRepost;
        $this->recuring = $recuring;
        $this->createdBy = $createdBy;
        $this->freeEscortAvailable = $freeEscortAvailable;
        $this->discountAmount = $discountAmount;
        $this->discountPercentage = $discountPercentage;
        $this->classifiedUp = $classifiedUp;
        $this->fixedPrice = $fixedPrice;
        $this->ecciePromo = $ecciePromo;
        $this->tracking = $tracking;
        $this->isCityThumbnail = $isCityThumbnail;
        $this->isSideSponsor = $isSideSponsor;
        $this->isSponsor = $isSponsor;
        $this->codeWorkingDays = $codeWorkingDays;
        $this->escortType = $escortType;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getSection(): string
    {
        return $this->section;
    }

    public function getCodeCount(): int
    {
        return $this->codeCount;
    }

    public function isAutoRenewEnable(): bool
    {
        return $this->isAutoRenewEnable;
    }

    public function getLimitPerAccount(): int
    {
        return $this->limitPerAccount;
    }

    public function getEscortLocationLimit(): int
    {
        return $this->escortLocationLimit;
    }

    public function getEscortAutoRepost(): int
    {
        return $this->escortAutoRepost;
    }

    public function getRecuring(): int
    {
        return $this->recuring;
    }

    public function getCreatedBy(): int
    {
        return $this->createdBy;
    }

    public function isFreeEscortAvailable(): bool
    {
        return $this->freeEscortAvailable;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discountAmount;
    }

    public function getDiscountPercentage(): ?float
    {
        return $this->discountPercentage;
    }

    public function getClassifiedUp(): int
    {
        return $this->classifiedUp;
    }

    public function getFixedPrice(): ?float
    {
        return $this->fixedPrice;
    }

    public function isEcciePromo(): bool
    {
        return $this->ecciePromo;
    }

    public function isTracking(): bool
    {
        return $this->tracking;
    }

    public function isCityThumbnail(): bool
    {
        return $this->isCityThumbnail;
    }

    public function isSideSponsor(): bool
    {
        return $this->isSideSponsor;
    }

    public function isSponsor(): bool
    {
        return $this->isSponsor;
    }

    public function getCodeWorkingDays(): int
    {
        return $this->codeWorkingDays;
    }

    public function getEscortType(): int
    {
        return $this->escortType;
    }
}
