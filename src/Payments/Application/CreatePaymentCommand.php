<?php

declare(strict_types=1);

namespace App\Payments\Application;

use LazyLemurs\Structures\Email;

final class CreatePaymentCommand
{
    private int $accountId;

    private Email $email;

    private float $amount;

    /**
     * @var array|int[]
     */
    private array $locationIds;

    private ?int $escortId;

    private ?int $placeId;

    private ?string $ipAddress;

    /**
     * @param int[] $locationIds
     */
    public function __construct(
        int $accountId,
        Email $email,
        float $amount,
        array $locationIds,
        ?int $escortId,
        ?int $placeId,
        ?string $ipAddress
    ) {
        $this->accountId = $accountId;
        $this->email = $email;
        $this->amount = $amount;
        $this->locationIds = $locationIds;
        $this->escortId = $escortId;
        $this->placeId = $placeId;
        $this->ipAddress = $ipAddress;
    }

    public function getEscortId(): ?int
    {
        return $this->escortId;
    }

    public function getPlaceId(): ?int
    {
        return $this->placeId;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return int[]
     */
    public function getLocationIds(): array
    {
        return $this->locationIds;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }
}
