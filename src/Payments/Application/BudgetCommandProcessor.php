<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\Budget;
use App\Payments\Domain\BudgetWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class BudgetCommandProcessor
{
    private BudgetWriteStorage $writeStorage;

    private TransactionManager $transactionManager;

    public function __construct(BudgetWriteStorage $writeStorage)
    {
        $this->writeStorage = $writeStorage;
    }

    /**
     * @throws \Throwable
     */
    public function create(CreateBudgetCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command) {
           $this->writeStorage->create(new Budget($command));
        });
    }
}
