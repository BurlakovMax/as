<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\Application\Search\SearchQuery;
use App\Payments\Domain\BudgetReadStorage;

final class BudgetQueryProcessor
{
    private BudgetReadStorage $readStorage;

    public function __construct(BudgetReadStorage $readStorage)
    {
        $this->readStorage = $readStorage;
    }

    /**
     * @throws BudgetNotFound
     */
    public function getByAccountId(int $accountId): BudgetData
    {
        $budget = $this->readStorage->getByAccountId($accountId);

        if (null === $budget) {
            throw new BudgetNotFound();
        }

        return new BudgetData($budget);
    }

    /**
     * @return BudgetData[]
     */
    public function getBySearchQuery(SearchQuery $searchQuery): array
    {
        return array_map(fn($budget) => new BudgetData($budget), $this->readStorage->getBySearchQuery($searchQuery));
    }

    public function countBySearchQuery(SearchQuery $searchQuery): int
    {
        return $this->readStorage->countBySearchQuery($searchQuery);
    }
}
