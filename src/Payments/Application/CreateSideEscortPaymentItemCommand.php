<?php

declare(strict_types=1);

namespace App\Payments\Application;

final class CreateSideEscortPaymentItemCommand
{
    private int $escortId;

    private int $locationId;

    public function __construct(int $escortId, int $locationId)
    {
        $this->escortId = $escortId;
        $this->locationId = $locationId;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }
}
