<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\Application\Search\SearchQuery;
use App\Payments\Domain\Payment;
use App\Payments\Domain\PaymentReadStorage;

final class PaymentQueryProcessor
{
    private PaymentReadStorage $paymentReadStorage;

    public function __construct(PaymentReadStorage $paymentReadStorage)
    {
        $this->paymentReadStorage = $paymentReadStorage;
    }

    /**
     * @throws PaymentNotFound
     */
    public function get(int $id): PaymentData
    {
        $payment = $this->paymentReadStorage->get($id);

        if (null === $payment) {
            throw new PaymentNotFound();
        }

        return new PaymentData($payment);
    }

    /**
     * @return PaymentData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        return array_map([ $this, 'mapToData' ], $this->paymentReadStorage->getBySearchQuery($query));
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->paymentReadStorage->countBySearchQuery($query);
    }

    public function countByAccountId(int $accountId): int
    {
        return $this->paymentReadStorage->countByAccountId($accountId);
    }

    private function mapToData(Payment $payment): PaymentData
    {
        return new PaymentData($payment);
    }
}
