<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\CreditCard;
use DateTimeImmutable;
use Swagger\Annotations as SWG;

final class CreditCardData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private int $accountId;

    /**
     * @SWG\Property()
     */
    private ?string $type;

    /**
     * @SWG\Property()
     */
    private ?string $cardNumber;

    /**
     * @SWG\Property()
     */
    private ?string $expiryMonth;

    /**
     * @SWG\Property()
     */
    private ?string $expiryYear;

    /**
     * @SWG\Property()
     */
    private ?string $cvc2;

    /**
     * @SWG\Property()
     */
    private string $cardholderName;

    /**
     * @SWG\Property()
     */
    private string $cardHolderFirstname;

    /**
     * @SWG\Property()
     */
    private string $cardHolderLastname;

    /**
     * @SWG\Property()
     */
    private string $address;

    /**
     * @SWG\Property()
     */
    private string $zipcode;

    /**
     * @SWG\Property()
     */
    private ?float $total;

    /**
     * @SWG\Property()
     */
    private string $city;

    /**
     * @SWG\Property()
     */
    private string $state;

    /**
     * @SWG\Property()
     */
    private ?string $country;

    /**
     * @SWG\Property()
     */
    private ?string $binInfo;

    /**
     * @SWG\Property()
     */
    private ?int $giftPrepaid;

    /**
     * @SWG\Property()
     */
    private ?string $securionpayToken;

    /**
     * @SWG\Property()
     */
    private ?string $securionpayCustomerId;

    /**
     * @SWG\Property()
     */
    private ?string $securionpayCardId;

    /**
     * @SWG\Property()
     */
    private ?string $stripeToken;

    /**
     * @SWG\Property()
     */
    private ?string $imageFront;

    /**
     * @SWG\Property()
     */
    private ?string $imageBack;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $approvedStamp;

    /**
     * @SWG\Property()
     */
    private ?int $approvedBy;

    /**
     * @SWG\Property()
     */
    private bool $shared;

    /**
     * @SWG\Property()
     */
    private bool $banned;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $bannedStamp;

    /**
     * @SWG\Property()
     */
    private ?int $bannedBy;

    /**
     * @SWG\Property()
     */
    private bool $deleted;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $deletedStamp;

    /**
     * @SWG\Property()
     */
    private ?int $deletedBy;

    /**
     * @SWG\Property()
     */
    private ?string $customerId;

    /**
     * @SWG\Property()
     */
    private ?string $cardId;

    public function __construct(CreditCard $card)
    {
        $this->id = $card->getId();
        $this->accountId = $card->getAccountId();
        $this->type = $card->getType() !== null ? $card->getType()->getRawValue() : null;
        $this->cardNumber = $card->getCardNumber();
        $this->expiryMonth = $card->getExpiryMonth();
        $this->expiryYear = $card->getExpiryYear();
        $this->cvc2 = $card->getCvc2();
        $this->cardholderName = $card->getCardholderName();
        $this->cardHolderFirstname = $card->getFirstname();
        $this->cardHolderLastname = $card->getLastname();
        $this->address = $card->getAddress();
        $this->zipcode = $card->getZipcode();
        $this->total = $card->getTotal();
        $this->city = $card->getCity();
        $this->state = $card->getState();
        $this->country = $card->getCountry();
        $this->binInfo = $card->getBinInfo();
        $this->giftPrepaid = $card->getGiftPrepaid();
        $this->securionpayToken = $card->getSecurionpayToken();
        $this->securionpayCustomerId = $card->getSecurionpayCustomerId();
        $this->securionpayCardId = $card->getSecurionpayCardId();
        $this->stripeToken = $card->getStripeToken();
        $this->imageFront = $card->getImageFront();
        $this->imageBack = $card->getImageBack();
        $this->approvedStamp = $card->getApprovedStamp();
        $this->approvedBy = $card->getApprovedBy();
        $this->shared = $card->getShared();
        $this->banned = $card->getBanned();
        $this->bannedStamp = $card->getBannedStamp();
        $this->bannedBy = $card->getBannedBy();
        $this->deleted = $card->getDeleted();
        $this->deletedStamp = $card->getDeletedStamp();
        $this->deletedBy = $card->getDeletedBy();
        $this->customerId = $card->getCustomerId();
        $this->cardId = $card->getCardId();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getCardNumber(): ?string
    {
        return $this->cardNumber;
    }

    public function getExpiryMonth(): ?string
    {
        return $this->expiryMonth;
    }

    public function getExpiryYear(): ?string
    {
        return $this->expiryYear;
    }

    public function getCvc2(): ?string
    {
        return $this->cvc2;
    }

    public function getCardholderName(): string
    {
        return $this->cardholderName;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getBinInfo(): ?string
    {
        return $this->binInfo;
    }

    public function getGiftPrepaid(): ?int
    {
        return $this->giftPrepaid;
    }

    public function getSecurionpayToken(): ?string
    {
        return $this->securionpayToken;
    }

    public function getSecurionpayCustomerId(): ?string
    {
        return $this->securionpayCustomerId;
    }

    public function getSecurionpayCardId(): ?string
    {
        return $this->securionpayCardId;
    }

    public function getStripeToken(): ?string
    {
        return $this->stripeToken;
    }

    public function getImageFront(): ?string
    {
        return $this->imageFront;
    }

    public function getImageBack(): ?string
    {
        return $this->imageBack;
    }

    public function getApprovedStamp(): ?DateTimeImmutable
    {
        return $this->approvedStamp;
    }

    public function getApprovedBy(): ?int
    {
        return $this->approvedBy;
    }

    public function getShared(): bool
    {
        return $this->shared;
    }

    public function getBanned(): bool
    {
        return $this->banned;
    }

    public function getBannedStamp(): ?DateTimeImmutable
    {
        return $this->bannedStamp;
    }

    public function getBannedBy(): ?int
    {
        return $this->bannedBy;
    }

    public function getDeleted(): bool
    {
        return $this->deleted;
    }

    public function getDeletedStamp(): ?DateTimeImmutable
    {
        return $this->deletedStamp;
    }

    public function getDeletedBy(): ?int
    {
        return $this->deletedBy;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function getCardId(): ?string
    {
        return $this->cardId;
    }

    public function getCardholderFirstname(): string
    {
        return $this->cardHolderFirstname;
    }

    public function getCardholderLastname(): string
    {
        return $this->cardHolderLastname;
    }
}
