<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\Application\Search\SearchQuery;
use App\Payments\Domain\CreditCard;
use App\Payments\Domain\CreditCardReadStorage;

final class CreditCardQueryProcessor
{
    private CreditCardReadStorage $creditCardReadStorage;

    public function __construct(CreditCardReadStorage $creditCardReadStorage)
    {
        $this->creditCardReadStorage = $creditCardReadStorage;
    }

    public function get(int $id): CreditCardData
    {
        $card = $this->creditCardReadStorage->get($id);

        if (null === $card) {
            throw new CreditCardNotFound();
        }

        return $this->mapToData($card);
    }

    /**
     * @return CreditCardData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        return array_map([ $this, 'mapToData' ], $this->creditCardReadStorage->getBySearchQuery($query));
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->creditCardReadStorage->countBySearchQuery($query);
    }

    public function countByAccountId(int $accountId): int
    {
        return $this->creditCardReadStorage->countByAccountId($accountId);
    }

    private function mapToData(CreditCard $card): CreditCardData
    {
        return new CreditCardData($card);
    }
}
