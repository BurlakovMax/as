<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\BannedCreditCard;

final class BannedCreditCardData
{
    private int $id;

    private string $cardNumber;

    private ?string $expiryMonth;

    private ?string $expiryYear;

    private ?string $cvc2;

    private string $cardHolderName;

    private string $address;

    private string $zipcode;

    private ?string $city;

    private ?string $state;

    private ?string $country;

    private ?string $banReason;

    private float $total;

    private \DateTimeImmutable $banTime;

    public function __construct(BannedCreditCard $card) {
        $this->id = $card->getId();
        $this->cardNumber = $card->getCardNumber();
        $this->expiryMonth = $card->getExpiryMonth();
        $this->expiryYear = $card->getExpiryYear();
        $this->cvc2 = $card->getCvc2();
        $this->cardHolderName = $card->getCardholderName()->getFullName();
        $this->address = $card->getAddress();
        $this->zipcode = $card->getZipcode();
        $this->city = $card->getCity();
        $this->state = $card->getState();
        $this->country = $card->getCountry();
        $this->banReason = $card->getBanReason();
        $this->total = $card->getTotal();
        $this->banTime = $card->getBannedTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    public function getExpiryMonth(): ?string
    {
        return $this->expiryMonth;
    }

    public function getExpiryYear(): ?string
    {
        return $this->expiryYear;
    }

    public function getCvc2(): string
    {
        return $this->cvc2;
    }

    public function getCardholderName(): string
    {
        return $this->cardHolderName;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getBanReason(): ?string
    {
        return $this->banReason;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function getBannedTime(): \DateTimeImmutable
    {
        return $this->banTime;
    }
}
