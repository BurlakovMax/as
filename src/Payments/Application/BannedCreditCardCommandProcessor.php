<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\BannedCreditCard;
use App\Payments\Domain\BannedCreditCardStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class BannedCreditCardCommandProcessor
{
    private BannedCreditCardStorage $storage;

    private TransactionManager $transactionManager;

    public function __construct(BannedCreditCardStorage $storage, TransactionManager $transactionManager) {
        $this->storage = $storage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function add(BannedCreditCardCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
           $this->storage->add(new BannedCreditCard($command));
        });
    }
}
