<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\Application\Search\SearchQuery;
use App\Payments\Domain\BannedCreditCard;
use App\Payments\Domain\BannedCreditCardStorage;

final class BannedCreditCardQueryProcessor
{
    private BannedCreditCardStorage $storage;

    public function __construct(BannedCreditCardStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return BannedCreditCardData[]
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        return $this->mapListToData(
            $this->storage->getListBySearchQuery($query)
        );
    }

    public function getCountBySearchQuery(SearchQuery $query): int
    {
        return $this->storage->countBySearchQuery($query);
    }

    /**
     * @param BannedCreditCard[]
     * @return BannedCreditCardData[]
     */
    private function mapListToData(array $cards): array
    {
        return array_map(fn($card): BannedCreditCardData => new BannedCreditCardData($card), $cards);
    }
}
