<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\CreditCard;
use App\Payments\Domain\CreditCardReadStorage;
use App\Payments\Domain\CreditCardWriteStorage;
use App\Payments\Domain\EscortSideStorage;
use App\Payments\Domain\EscortSponsorStorage;
use App\Payments\Domain\EscortStickyStorage;
use App\Payments\Domain\EscortStorage;
use App\Payments\Domain\Logs\Log;
use App\Payments\Domain\Logs\LogWriteStorage;
use App\Payments\Domain\Payment;
use App\Payments\Domain\PaymentItem;
use App\Payments\Domain\PaymentService;
use App\Payments\Domain\PaymentWriteStorage;
use App\Payments\Domain\Refund;
use App\Payments\Domain\RefundType;
use App\Payments\Domain\RefundWriteStorage;
use App\Payments\Domain\TransactionWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class PaymentCommandProcessor
{
    private TransactionManager $transactionManager;

    private PaymentService $paymentService;

    private PaymentWriteStorage $paymentWriteStorage;

    private CreditCardWriteStorage $creditCardWriteStorage;

    private CreditCardReadStorage $creditCardReadStorage;

    private EscortStickyStorage $escortStickyStorage;

    private EscortStorage $escortStorage;

    private EscortSponsorStorage $escortSponsorStorage;

    private EscortSideStorage $escortSideStorage;

    private RefundWriteStorage $refundWriteStorage;

    private TransactionWriteStorage $transactionWriteStorage;

    private LogWriteStorage $logWriteStorage;

    public function __construct(
        TransactionManager $transactionManager,
        PaymentService $paymentService,
        PaymentWriteStorage $paymentWriteStorage,
        CreditCardWriteStorage $creditCardWriteStorage,
        CreditCardReadStorage $creditCardReadStorage,
        EscortStickyStorage $escortStickyStorage,
        EscortStorage $escortStorage,
        EscortSponsorStorage $escortSponsorStorage,
        EscortSideStorage $escortSideStorage,
        RefundWriteStorage $refundWriteStorage,
        TransactionWriteStorage $transactionWriteStorage,
        LogWriteStorage $logWriteStorage
    ) {
        $this->transactionManager = $transactionManager;
        $this->paymentService = $paymentService;
        $this->paymentWriteStorage = $paymentWriteStorage;
        $this->creditCardWriteStorage = $creditCardWriteStorage;
        $this->creditCardReadStorage = $creditCardReadStorage;
        $this->escortStickyStorage = $escortStickyStorage;
        $this->escortStorage = $escortStorage;
        $this->escortSponsorStorage = $escortSponsorStorage;
        $this->escortSideStorage = $escortSideStorage;
        $this->refundWriteStorage = $refundWriteStorage;
        $this->transactionWriteStorage = $transactionWriteStorage;
        $this->logWriteStorage = $logWriteStorage;
    }

    private function getCardId(CreateCreditCardCommand $command): int
    {
        $creditCard = $this->creditCardReadStorage->getByCardParams(
            $command->getAccountId(),
            substr($command->getCardNumber(), -4),
            $command->getExpiryMonth(),
            $command->getExpiryYear()
        );

        if (null === $creditCard) {
            $creditCard = new CreditCard($command);
            $this->creditCardWriteStorage->add($creditCard);
        }

        return $creditCard->getId();
    }

    private function createPayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand,
        bool $hasRecurring,
        ?int $recurring
    ): Payment {

        $cardId = $this->getCardId($createCreditCardCommand);

        $payment = $this->paymentService->createPayment($createPaymentCommand, $cardId);
        if ($hasRecurring) {
            $payment->setRecurring($recurring);
        }

        return $payment;
    }

    /**
     * @param int[] $escortSideLocationIds
     * @param int[] $escortSponsorLocationIds
     *
     * @throws \Throwable
     */
    public function createCombo(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand,
        array $escortSideLocationIds,
        array $escortSponsorLocationIds,
        array $escortStickyMonthLocationIds,
        array $escortStickyWeekLocationIds
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand,
            $escortSideLocationIds,
            $escortSponsorLocationIds,
            $escortStickyMonthLocationIds,
            $escortStickyWeekLocationIds
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                true,
                $this->escortStorage->getRecurringPeriod()
            );

            $this->setEscortPaymentItems(
                $payment,
                $createPaymentCommand->getEscortId(),
                ...$createPaymentCommand->getLocationIds()
            );

            if (!empty($escortSideLocationIds)) {
                $this->setEscortSidePaymentItems(
                    $payment,
                    $createPaymentCommand->getEscortId(),
                    ...$escortSideLocationIds
                );
            }

            if (!empty($escortSponsorLocationIds)) {
                $this->setEscortSponsorPaymentItems(
                    $payment,
                    $createPaymentCommand->getEscortId(),
                    ...$escortSponsorLocationIds
                );
            }

            if (!empty($escortStickyMonthLocationIds)) {
                $days = [];
                foreach ($escortStickyMonthLocationIds as $locationId) {
                    $days[$locationId] = 30;
                }

                $this->setEscortStickyPaymentItems(
                    $payment,
                    $createPaymentCommand->getEscortId(),
                    $days,
                    ...$escortStickyMonthLocationIds
                );
            }

            if (!empty($escortStickyWeekLocationIds)) {
                $days = [];
                foreach ($escortStickyWeekLocationIds as $locationId) {
                    $days[$locationId] = 7;
                }

                $this->setEscortStickyPaymentItems(
                    $payment,
                    $createPaymentCommand->getEscortId(),
                    $days,
                    ...$escortStickyWeekLocationIds
                );
            }

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    /**
     * @throws \Throwable
     */
    public function createEscortPayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                true,
                $this->escortStorage->getRecurringPeriod()
            );

            $this->setEscortPaymentItems(
                $payment,
                $createPaymentCommand->getEscortId(),
                ...$createPaymentCommand->getLocationIds()
            );

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    private function setEscortPaymentItems(Payment $payment, int $escortId, int ...$locationIds): void
    {
        foreach ($locationIds as $locationId) {
            $payment->addItem(
                PaymentItem::createEscortPaymentItem(
                    new CreateEscortPaymentItemCommand($escortId, $locationId),
                    $payment
                )
            );
        }
    }

    /**
     * @throws \Throwable
     */
    public function createEscortStickyPayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand,
        array $escortStickyMonthLocationIds,
        array $escortStickyWeekLocationIds,
        bool $recurring
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand,
            $escortStickyMonthLocationIds,
            $escortStickyWeekLocationIds,
            $recurring
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                $recurring,
                $this->escortStickyStorage->getRecurringPeriod()
            );

            if (!empty($escortStickyMonthLocationIds)) {
                $days = [];
                foreach ($escortStickyMonthLocationIds as $locationId) {
                    $days[$locationId] = 30;
                }

                $this->setEscortStickyPaymentItems(
                    $payment,
                    $createPaymentCommand->getEscortId(),
                    $days,
                    ...$escortStickyMonthLocationIds
                );
            }

            if (!empty($escortStickyWeekLocationIds)) {
                $days = [];
                foreach ($escortStickyWeekLocationIds as $locationId) {
                    $days[$locationId] = 7;
                }

                $this->setEscortStickyPaymentItems(
                    $payment,
                    $createPaymentCommand->getEscortId(),
                    $days,
                    ...$escortStickyWeekLocationIds
                );
            }

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    private function setEscortStickyPaymentItems(
        Payment $payment,
        int $escortId,
        array $days,
        int ...$locationIds
    ): void {

        foreach ($locationIds as $locationId) {
            if (!isset($days[$locationId])) {
                throw new \RuntimeException('You can\'t post sticky ad in this location');
            }

            $payment->addItem(
                PaymentItem::createEscortStickyPaymentItem(
                    new CreateEscortStickyPaymentItemCommand(
                        $escortId,
                        $locationId,
                        (int) $days[$locationId]
                    ),
                    $payment
                )
            );
        }
    }

    /**
     * @throws \Throwable
     */
    public function createEscortSponsorPayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                true,
                $this->escortSponsorStorage->getRecurringPeriod()
            );

            $this->setEscortSponsorPaymentItems(
                $payment,
                $createPaymentCommand->getEscortId(),
                ...$createPaymentCommand->getLocationIds()
            );

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    private function setEscortSponsorPaymentItems(Payment $payment, int $escortId, int ...$locationIds): void
    {
        foreach ($locationIds as $locationId) {
            $payment->addItem(
                PaymentItem::createEscortSponsorPaymentItem(
                    new CreateEscortSponsorPaymentItemCommand($escortId, $locationId),
                    $payment
                )
            );
        }
    }

    /**
     * @throws \Throwable
     */
    public function createEscortSidePayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                true,
                $this->escortSideStorage->getRecurringPeriod()
            );

            $this->setEscortSidePaymentItems(
                $payment,
                $createPaymentCommand->getEscortId(),
                ...$createPaymentCommand->getLocationIds()
            );

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    private function setEscortSidePaymentItems(Payment $payment, int $escortId, int ...$locationIds): void
    {
        foreach ($locationIds as $locationId) {
            $payment->addItem(
                PaymentItem::createSidePaymentItem(
                    new CreateSideEscortPaymentItemCommand($escortId, $locationId),
                    $payment
                )
            );
        }
    }

    /**
     * @throws \Throwable
     */
    public function createPlaceownerPayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand,
        int $recurringPeriod
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand,
            $recurringPeriod
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                true,
                $recurringPeriod
            );

            $payment->addItem(
                PaymentItem::createPlaceownerPaymentItem($createPaymentCommand->getPlaceId(), $payment)
            );

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    /**
     * @throws \Throwable
     */
    public function createTopUpsPayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand,
        $topUps
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand,
            $topUps
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                false,
                null
            );

            $payment->addItem(
                PaymentItem::createTopUpsPaymentItem($topUps, $payment)
            );

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    /**
     * @throws \Throwable
     */
    public function createAdvertisePayment(
        CreatePaymentCommand $createPaymentCommand,
        CreateCreditCardCommand $createCreditCardCommand
    ): CreateTransactionCommand {

        return $this->transactionManager->transactional(function() use (
            $createPaymentCommand,
            $createCreditCardCommand
        ): CreateTransactionCommand {

            $payment = $this->createPayment(
                $createPaymentCommand,
                $createCreditCardCommand,
                false,
                null
            );

            $payment->addItem(PaymentItem::createAdvertisePaymentItem());

            $this->paymentWriteStorage->add($payment);

            return $this->paymentService->pay($payment->getId());
        });
    }

    /**
     * @throws \Throwable
     */
    public function cancel(int $subscriptionId): void
    {
        $this->transactionManager->transactional(function() use ($subscriptionId) {
            $subscription = $this->paymentWriteStorage->getAndLock($subscriptionId);
            if (null === $subscription) {
                throw new PaymentNotFound("Subscription with ID {$subscriptionId} not found");
            }
            $subscription->cancel();

            $this->logWriteStorage->add(
                Log::subscriptionCanceled(
                    $subscription->getId(),
                    $subscription->getAccountId(),
                    sprintf('Subscription canceled, reason: client_cancel')
                )
            );
        });
    }

    public function refund(int $paymentId, int $transactionId, int $amount, int $authorId, ?string $reason): void
    {
        $this->transactionManager->transactional(function() use ($paymentId, $transactionId, $amount, $authorId, $reason) {
            $payment = $this->paymentWriteStorage->getAndLock($paymentId);

            if (null === $payment) {
                throw new PaymentNotFound();
            }

            if (!$payment->canRefund($amount)) {
                throw new RefundError();
            }

            $this->paymentService->refund($payment, $amount);

            if ($payment->canFullRefund($amount)) {
                $payment->cancelPayment();
            }

            $this->refundWriteStorage->add(
                new Refund(
                    RefundType::refund(),
                    $transactionId,
                    $authorId,
                    $payment->getId(),
                    $payment->getAccountId(),
                    $payment->getCardId(),
                    $amount,
                    $reason
                )
            );

            $transaction = $this->transactionWriteStorage->getAndLock($transactionId);

            if (null === $transaction) {
                throw new TransactionNotFound();
            }

            $transaction->refund($amount);

            $this->logWriteStorage->add(
                Log::refund(
                    $payment->getId(),
                    $payment->getAccountId(),
                    $transactionId,
                    sprintf('Refunded transaction %s in amount %s', $transactionId, $amount)
                )
            );
        });
    }
}
