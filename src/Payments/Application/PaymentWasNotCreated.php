<?php

declare(strict_types=1);

namespace App\Payments\Application;

use LazyLemurs\Exceptions\Exception;

final class PaymentWasNotCreated extends Exception
{

}