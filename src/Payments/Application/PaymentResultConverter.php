<?php

declare(strict_types=1);

namespace App\Payments\Application;

final class PaymentResultConverter
{
    public static function valueToName(string $value): string
    {
        switch ($value) {
            case 'I':
                return 'Initial';
            case 'S':
                return 'Sent to ccbill';
            case 'D':
                return 'Denied';
            case 'A':
                return 'Accepted';
            case 'C':
                return 'Canceled';
            case 'U':
                return 'Unknown';
        }
    }
}