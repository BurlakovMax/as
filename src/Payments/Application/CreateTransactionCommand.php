<?php

declare(strict_types=1);

namespace App\Payments\Application;

use DateTimeImmutable;

final class CreateTransactionCommand
{
    private ?string $id;

    private ?string $merchantId;

    private ?string $providerId;

    private ?string $customerId;

    private ?string $cardId;

    private DateTimeImmutable $createdAt;

    private ?string $status;

    private DateTimeImmutable $changedAt;

    private ?string $description;

    private ?int $amount;

    private ?string $currency;

    private ?string $action;

    private ?string $externalId;

    private array $response;

    private ?string $errorReason;

    private ?int $transId;

    private ?int $paymentId;

    public function __construct(
        ?string $id,
        ?string $merchantId,
        ?string $providerId,
        ?string $customerId,
        ?string $cardId,
        ?string $createdAt,
        ?string $status,
        ?string $changedAt,
        ?string $description,
        ?int $amount,
        ?string $currency,
        ?string $action,
        ?string $externalId,
        array $response,
        ?string $errorReason = null
    ) {
        $this->id = $id;
        $this->merchantId = $merchantId;
        $this->providerId = $providerId;
        $this->customerId = $customerId;
        $this->cardId = $cardId;
        $this->createdAt = new DateTimeImmutable($createdAt);
        $this->status = $status;
        $this->changedAt = new DateTimeImmutable($changedAt);
        $this->description = $description;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->action = $action;
        $this->response = $response;
        $this->externalId = $externalId;
        $this->errorReason = $errorReason;
        $this->paymentId = null;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }

    public function getProviderId(): ?string
    {
        return $this->providerId;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function getCardId(): ?string
    {
        return $this->cardId;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getChangedAt(): DateTimeImmutable
    {
        return $this->changedAt;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getAmount(): ?float
    {
        return (float)($this->amount / 100);
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function getErrorReason(): ?string
    {
        return $this->errorReason;
    }

    public function getTransId(): ?int
    {
        return $this->transId;
    }

    public function getPaymentId(): ?int
    {
        return $this->paymentId;
    }

    public function setPaymentId(int $paymentId): void
    {
        $this->paymentId = $paymentId;
    }
}
