<?php

declare(strict_types=1);

namespace App\Payments\Application;

final class StatisticData
{
    private int $countOfNewEscorts;

    private float $amountOfNewEscorts;

    private int $renewCount;

    private float $renewAmount;

    private int $upgradeCount;

    private float $upgradeAmount;

    private int $totalCount;

    private float $totalAmount;

    public function __construct(
        int $countOfNewEscorts,
        float $amountOfNewEscorts,
        int $renewCount,
        float $renewAmount,
        int $upgradeCount,
        float $upgradeAmount,
        int $totalCount,
        float $totalAmount
    ) {
        $this->countOfNewEscorts = $countOfNewEscorts;
        $this->amountOfNewEscorts = $amountOfNewEscorts;
        $this->renewCount = $renewCount;
        $this->renewAmount = $renewAmount;
        $this->upgradeCount = $upgradeCount;
        $this->upgradeAmount = $upgradeAmount;
        $this->totalCount = $totalCount;
        $this->totalAmount = $totalAmount;
    }

    public function update(
        int $countOfNewEscorts,
        float $amountOfNewEscorts,
        int $renewCount,
        float $renewAmount,
        int $upgradeCount,
        float $upgradeAmount,
        int $totalCount,
        float $totalAmount
    ) {
        $this->countOfNewEscorts = $countOfNewEscorts;
        $this->amountOfNewEscorts = $amountOfNewEscorts;
        $this->renewCount = $renewCount;
        $this->renewAmount = $renewAmount;
        $this->upgradeCount = $upgradeCount;
        $this->upgradeAmount = $upgradeAmount;
        $this->totalCount = $totalCount;
        $this->totalAmount = $totalAmount;
    }

    public function getCountOfNewEscorts(): int
    {
        return $this->countOfNewEscorts;
    }

    public function getAmountOfNewEscorts(): float
    {
        return $this->amountOfNewEscorts;
    }

    public function getRenewCount(): int
    {
        return $this->renewCount;
    }

    public function getRenewAmount(): float
    {
        return $this->renewAmount;
    }

    public function getUpgradeCount(): int
    {
        return $this->upgradeCount;
    }

    public function getUpgradeAmount(): float
    {
        return $this->upgradeAmount;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }
}