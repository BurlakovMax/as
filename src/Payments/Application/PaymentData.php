<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\Payment;
use App\Payments\Domain\PaymentItem;
use App\Payments\Domain\Transaction;
use Money\Money;
use Swagger\Annotations as SWG;
use DateTimeImmutable;

final class PaymentData
{
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private ?int $accountId;

    /**
     * @SWG\Property()
     */
    private ?int $authorId;

    /**
     * @SWG\Property()
     */
    private ?int $cardId;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $sentAt;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $respondedAt;

    /**
     * @SWG\Property()
     */
    private ?string $declineCode;

    /**
     * @SWG\Property()
     */
    private ?string $declineReason;

    /**
     * @SWG\Property()
     */
    private ?string $result;

    /**
     * @SWG\Property()
     */
    private ?string $resultInString;

    /**
     * @SWG\Property()
     */
    private ?string $transId;

    /**
     * @SWG\Property()
     */
    private Money $amount;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $nextRenewalDate;

    /**
     * @SWG\Property()
     */
    private ?int $subscriptionStatus;

    /**
     * @SWG\Property()
     */
    private ?DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private ?Money $recurringAmount;

    /**
     * @SWG\Property()
     */
    private ?int $recurringPeriod;

    /**
     * @SWG\Property()
     */
    private ?string $lastOperation;

    /**
     * @var PaymentItemData[]
     */
    private array $items;

    /**
     * @var TransactionData[]
     */
    private array $transactions;

    /**
     * @SWG\Property()
     */
    private bool $isSubscription;

    public function __construct(Payment $payment)
    {
        $this->id = $payment->getId();
        $this->accountId = $payment->getAccountId();
        $this->authorId = $payment->getAuthorId();
        $this->cardId = $payment->getCardId();
        $this->sentAt = $payment->getSentAt();
        $this->respondedAt = $payment->getRespondedAt();
        $this->declineCode = $payment->getDeclineCode();
        $this->declineReason = $payment->getDeclineReason();
        $this->result = null === $payment->getResult() ? null : $payment->getResult()->getRawValue();
        $this->resultInString = null === $payment->getResult() ? null : PaymentResultConverter::valueToName($payment->getResult()->getRawValue());
        $this->transId = $payment->getTransId();
        $this->amount = $payment->getAmount();
        $this->nextRenewalDate = $payment->getNextRenewalDate();
        $this->subscriptionStatus = $payment->getSubscriptionStatus() ?
            $payment->getSubscriptionStatus()->getRawValue() : null;
        $this->createdAt = $payment->getCreatedAt();
        $this->recurringAmount = $payment->getRecurringAmount();
        $this->recurringPeriod = $payment->getRecurringPeriod();
        $this->lastOperation = $payment->getLastOperation();
        $this->items = array_map(
            function (PaymentItem $item): PaymentItemData {
                return new PaymentItemData($item);
            },
            $payment->getItems()->toArray()
        );
        $this->transactions = array_map(
            function (Transaction $transaction): TransactionData {
                return new TransactionData($transaction);
            },
            $payment->getTransactions()->toArray()
        );
        $this->isSubscription = $payment->isSubscription();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function getAuthorId(): ?int
    {
        return $this->authorId;
    }

    public function getCardId(): ?int
    {
        return $this->cardId;
    }

    public function getSentAt(): ?DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function getRespondedAt(): ?DateTimeImmutable
    {
        return $this->respondedAt;
    }

    public function getDeclineCode(): ?string
    {
        return $this->declineCode;
    }

    public function getDeclineReason(): ?string
    {
        return $this->declineReason;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function getResultInString(): ?string
    {
        return $this->resultInString;
    }

    public function getTransId(): ?string
    {
        return $this->transId;
    }

    public function getAmount(): int
    {
        return (int)$this->amount->getAmount();
    }

    public function getNextRenewalDate(): ?DateTimeImmutable
    {
        return $this->nextRenewalDate;
    }

    public function getSubscriptionStatus(): ?int
    {
        return $this->subscriptionStatus;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getRecurringAmount(): ?int
    {
        return (int)$this->recurringAmount->getAmount();
    }

    public function getRecurringPeriod(): ?int
    {
        return $this->recurringPeriod;
    }

    public function getLastOperation(): ?string
    {
        return $this->lastOperation;
    }

    /**
     * @return PaymentItemData[]|array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return TransactionData[]|array
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }

    public function isSubscription(): bool
    {
        return $this->isSubscription;
    }
}
