<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\Budget;
use LazyLemurs\Structures\PhoneNumber;

final class BudgetData
{
    private int $accountId;

    private float $amount;

    private int $recurring;

    private ?int $creditCardId;

    private int $recurringAmount;

    private int $recurringTotal;

    private int $recurringMax;

    private int $notifyAmount;

    private ?\DateTimeImmutable $lastNotify;

    private ?string $nickName;

    private ?string $name;

    private ?PhoneNumber $phone;

    private ?string $company;

    private ?string $contact;

    private ?string $notes;

    public function __construct(Budget $budget) {
        $this->accountId = $budget->getAccountId();
        $this->amount = $budget->getAmount();
        $this->recurring = $budget->getRecurring();
        $this->creditCardId = $budget->getCreditCardId();
        $this->recurringAmount = $budget->getRecurringAmount();
        $this->recurringTotal = $budget->getRecurringTotal();
        $this->recurringMax = $budget->getRecurringMax();
        $this->notifyAmount = $budget->getNotifyAmount();
        $this->lastNotify = $budget->getLastNotify();
        $this->nickName = $budget->getNickName();
        $this->name = $budget->getName();
        $this->phone = $budget->getPhone();
        $this->company = $budget->getCompany();
        $this->contact = $budget->getContact();
        $this->notes = $budget->getNotes();
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getRecurring(): int
    {
        return $this->recurring;
    }

    public function getCreditCardId(): ?int
    {
        return $this->creditCardId;
    }

    public function getRecurringAmount(): int
    {
        return $this->recurringAmount;
    }

    public function getRecurringTotal(): int
    {
        return $this->recurringTotal;
    }

    public function getRecurringMax(): int
    {
        return $this->recurringMax;
    }

    public function getNotifyAmount(): int
    {
        return $this->notifyAmount;
    }

    public function getLastNotify(): ?\DateTimeImmutable
    {
        return $this->lastNotify;
    }

    public function getNickName(): ?string
    {
        return $this->nickName;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPhone(): ?PhoneNumber
    {
        return $this->phone;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }
}
