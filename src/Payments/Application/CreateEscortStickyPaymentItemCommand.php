<?php

declare(strict_types=1);

namespace App\Payments\Application;

final class CreateEscortStickyPaymentItemCommand
{
    private int $escortId;

    private int $locationId;

    private int $days;

    public function __construct(int $escortId, int $locationId, int $days)
    {
        $this->escortId = $escortId;
        $this->locationId = $locationId;
        $this->days = $days;
    }

    public function getEscortId(): int
    {
        return $this->escortId;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getDays(): int
    {
        return $this->days;
    }
}