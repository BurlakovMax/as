<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\ApplicationSupport\AccessControl\AccessForbidden;

final class AccessControl
{
    private PaymentQueryProcessor $paymentQueryProcessor;

    public function __construct(PaymentQueryProcessor $paymentQueryProcessor)
    {
        $this->paymentQueryProcessor = $paymentQueryProcessor;
    }

    /**
     * @throws AccessForbidden
     * @throws PaymentNotFound
     */
    public function checkAccessToPayment(int $accountId, int $paymentId): void
    {
       $subscription = $this->paymentQueryProcessor->get($paymentId);

       if ($subscription->getAccountId() !== $accountId) {
           throw new AccessForbidden();
       }
    }
}