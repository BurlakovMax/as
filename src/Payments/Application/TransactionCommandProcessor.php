<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Payments\Domain\AccountStorage;
use App\Payments\Domain\CreditCardWriteStorage;
use App\Payments\Domain\EscortSideStorage;
use App\Payments\Domain\EscortSponsorStorage;
use App\Payments\Domain\EscortStickyStorage;
use App\Payments\Domain\EscortStorage;
use App\Payments\Domain\Logs\Log;
use App\Payments\Domain\Logs\LogWriteStorage;
use App\Payments\Domain\PlaceStorage;
use App\Payments\Domain\Transaction;
use App\Payments\Domain\TransactionWriteStorage;
use App\Payments\Domain\ServiceType;
use App\Payments\Domain\PaymentItem;
use App\Payments\Domain\PaymentWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;
use Psr\Log\LoggerInterface;

final class TransactionCommandProcessor
{
    private TransactionWriteStorage $transactionWriteStorage;

    private PaymentWriteStorage $paymentWriteStorage;

    private CreditCardWriteStorage $creditCardWriteStorage;

    private AccountStorage $accountStorage;

    private EscortStorage $escortStorage;

    private EscortSideStorage $escortSideStorage;

    private EscortSponsorStorage $escortSponsorStorage;

    private EscortStickyStorage $escortStickyStorage;

    private PlaceStorage $placeStorage;

    private LogWriteStorage $logWriteStorage;

    private TransactionManager $transactionManager;

    private LoggerInterface $logger;

    public function __construct(
        TransactionWriteStorage $transactionWriteStorage,
        TransactionManager $transactionManager,
        PaymentWriteStorage $paymentWriteStorage,
        AccountStorage $accountStorage,
        EscortStorage $escortStorage,
        EscortSideStorage $escortSideStorage,
        EscortSponsorStorage $escortSponsorStorage,
        EscortStickyStorage $escortStickyStorage,
        PlaceStorage $placeStorage,
        LogWriteStorage $logWriteStorage,
        CreditCardWriteStorage $creditCardWriteStorage,
        LoggerInterface $logger
    ) {
        $this->transactionWriteStorage = $transactionWriteStorage;
        $this->paymentWriteStorage = $paymentWriteStorage;
        $this->creditCardWriteStorage = $creditCardWriteStorage;
        $this->accountStorage = $accountStorage;
        $this->escortStorage = $escortStorage;
        $this->escortSideStorage = $escortSideStorage;
        $this->escortSponsorStorage = $escortSponsorStorage;
        $this->escortStickyStorage = $escortStickyStorage;
        $this->placeStorage = $placeStorage;
        $this->logWriteStorage = $logWriteStorage;
        $this->transactionManager = $transactionManager;
        $this->logger = $logger;
    }

    /**
     * @throws \Throwable
     */
    public function save(CreateTransactionCommand $command): void
    {
        $this->transactionManager->transactional(function() use ($command): void {
            try {
                $payment = $this->paymentWriteStorage->getAndLockByLastOperation($command->getId());
                if (null === $payment) {
                    throw new PaymentNotFound("Subscription with Last Operation {$command->getId()} not found");
                }

                $card = $this->creditCardWriteStorage->getAndLock($payment->getCardId());
                if (null === $card) {
                    throw new CreditCardNotFound("Credit Card with ID {$payment->getCardId()} not found");
                }

                if (null !== $command->getErrorReason()) {
                    $payment->updateErrorFromSecurionpay($command);
                    $card->updateErrorFromTransaction($command);
                    return;
                }

                $card->updateSuccessfulCustomerIdAndCardIdFromSecurionpay($command);
                $payment->updateSuccessfulFromTransaction($command);

                $transaction = new Transaction($command, $payment);
                $this->transactionWriteStorage->add($transaction);

                foreach ($payment->getItems() as $item) {
                    $this->activateServiceAfterPaid($item);
                }

                $this->logWriteStorage->add(
                    Log::paid(
                        $payment->getId(),
                        $payment->getAccountId(),
                        $transaction->getId(),
                        sprintf('Paid #%s in amount %s', $transaction->getId(), $transaction->getAmount()->getAmount())
                    )
                );
            } catch (\Exception $exception) {
                $this->logger->error($exception->getMessage(), $exception->getTrace());
                throw $exception;
            }
        });
    }

    private function activateServiceAfterPaid(PaymentItem $item): void
    {
        foreach ($item->getServiceTypes() as $serviceType) {
            switch ($serviceType) {
                case ServiceType::escort():
                    $this->escortStorage->activate($item->getEscortId());
                    break;
                case ServiceType::escortSponsor():
                    $this->escortSponsorStorage->activateAfterPaid($item->getEscortId(), $item->getLocationId());
                    break;
                case ServiceType::escortSide():
                    $this->escortSideStorage->activateAfterPaid($item->getEscortId(), $item->getLocationId());
                    break;
                case ServiceType::escortSticky():
                    $this->escortStickyStorage->activateAfterPaid(
                        $item->getEscortId(),
                        $item->getLocationId(),
                        $item->getStickyDays()
                    );
                    break;
                case ServiceType::topUps():
                    $this->accountStorage->addTopUps($item->getPayment()->getAccountId(), $item->getTopUps());
                    break;
                case ServiceType::placeowner():
                    $this->placeStorage->setBusinessOwner(
                        $item->getPlaceId(),
                        $item->getPayment()->getAccountId(),
                        $item->getPayment()->getRecurringPeriod()
                    );
                    break;
                default:
                    throw new \RuntimeException("Service type %s is not supported. " . $serviceType);
            }
        }
    }
}