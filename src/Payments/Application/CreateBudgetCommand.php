<?php

declare(strict_types=1);

namespace App\Payments\Application;

use LazyLemurs\Structures\PhoneNumber;

final class CreateBudgetCommand
{
    private int $accountId;

    private float $amount;

    private int $recurring;

    private ?int $creditCardId;

    private int $recurringAmount;

    private int $recurringTotal;

    private int $recurringMax;

    private int $notifyAmount;

    private ?\DateTimeImmutable $lastNotify;

    private ?string $nickName;

    private ?string $name;

    private ?PhoneNumber $phone;

    private ?string $company;

    private ?string $contact;

    private ?string $notes;

    public function __construct(
        int $accountId,
        float $amount,
        int $recurring,
        int $recurringAmount,
        int $recurringTotal,
        int $recurringMax,
        int $notifyAmount,
        ?int $creditCardId,
        ?string $nickName,
        ?string $name,
        ?PhoneNumber $phone,
        ?string $company,
        ?string $contact,
        ?string $notes
    ) {
        $this->accountId = $accountId;
        $this->amount = $amount;
        $this->recurring = $recurring;
        $this->creditCardId = $creditCardId;
        $this->recurringAmount = $recurringAmount;
        $this->recurringTotal = $recurringTotal;
        $this->recurringMax = $recurringMax;
        $this->notifyAmount = $notifyAmount;
        $this->lastNotify = null;
        $this->nickName = $nickName;
        $this->name = $name;
        $this->phone = $phone;
        $this->company = $company;
        $this->contact = $contact;
        $this->notes = $notes;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getRecurring(): int
    {
        return $this->recurring;
    }

    public function getCreditCardId(): ?int
    {
        return $this->creditCardId;
    }

    public function getRecurringAmount(): int
    {
        return $this->recurringAmount;
    }

    public function getRecurringTotal(): int
    {
        return $this->recurringTotal;
    }

    public function getRecurringMax(): int
    {
        return $this->recurringMax;
    }

    public function getNotifyAmount(): int
    {
        return $this->notifyAmount;
    }

    public function getLastNotify(): ?\DateTimeImmutable
    {
        return $this->lastNotify;
    }

    public function getNickName(): ?string
    {
        return $this->nickName;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPhone(): ?PhoneNumber
    {
        return $this->phone;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }
}
