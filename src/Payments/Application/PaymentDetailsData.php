<?php

declare(strict_types=1);

namespace App\Payments\Application;

use Swagger\Annotations as SWG;

final class PaymentDetailsData
{
    /**
     * @SWG\Property()
     */
    private PaymentData $payment;

    /**
     * @SWG\Property()
     */
    private ?CreditCardData $card;

    public function __construct(PaymentData $payment, ?CreditCardData $card)
    {
        $this->payment = $payment;
        $this->card = $card;
    }

    public function getPayment(): PaymentData
    {
        return $this->payment;
    }

    public function getCard(): ?CreditCardData
    {
        return $this->card;
    }
}