<?php

declare(strict_types=1);

namespace App\Payments\Application;

use App\Core\Application\Search\SearchQuery;
use App\Payments\Domain\Promocode;
use App\Payments\Domain\PromocodeStorage;

final class PromocodeQueryProcessor
{
    private PromocodeStorage $storage;

    public function __construct(PromocodeStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return PromocodeData[]
     */
    public function getListBySearchQuery(SearchQuery $query): array
    {
        return $this->mapListToData($this->storage->getListBySearchQuery($query));
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->storage->countBySearchQuery($query);
    }

    public function getById(int $id): PromocodeData
    {
        $promoCode = $this->storage->getById($id);

        if (null === $promoCode) {
            throw new PromocodeNotFound();
        }

        return new PromocodeData($promoCode);
    }

    public function getByCode(string $code): PromocodeData
    {
        $promoCode = $this->storage->getByCode($code);

        if (null === $promoCode) {
            throw new PromocodeNotFound();
        }

        return new PromocodeData($promoCode);
    }

    public function getActiveByCode(string $code): PromocodeData
    {
        $promoCode = $this->storage->getByCode($code);

        if (null === $promoCode) {
            throw new PromocodeNotFound();
        }

        if (!$promoCode->isActive()) {
            throw new PromocodeNotActive();
        }

        return new PromocodeData($promoCode);
    }

    /**
     * @param Promocode[]
     * @return PromocodeData[]
     */
    private function mapListToData(array $promocodes): array
    {
        return array_map(fn($promocode): PromocodeData => new PromocodeData($promocode), $promocodes);
    }
}
