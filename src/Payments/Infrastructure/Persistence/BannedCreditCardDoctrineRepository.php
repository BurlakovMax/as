<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Payments\Domain\BannedCreditCard;
use App\Payments\Domain\BannedCreditCardStorage;
use Doctrine\ORM\EntityRepository;

class BannedCreditCardDoctrineRepository extends EntityRepository implements BannedCreditCardStorage
{
    public function getListBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.city',
                            't.cardholderName.firstName',
                            't.cardholderName.lastName',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.city',
                            't.cardholderName.firstName',
                            't.cardholderName.lastName',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(BannedCreditCard $bannedCreditCard): void
    {
        $this->getEntityManager()->persist($bannedCreditCard);
    }
}
