<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Payments\Domain\Promocode;
use App\Payments\Domain\PromocodeStorage;
use Doctrine\ORM\EntityRepository;

final class PromocodeDoctrineRepository extends EntityRepository implements PromocodeStorage
{
    public function getById(int $id): ?Promocode
    {
        return $this->find($id);
    }

    public function getByCode(string $code): ?Promocode
    {
        return $this->findOneBy(['code' => $code]);
    }

    public function getListBySearchQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.code',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(Limitation::limitation($query->getLimitation()))
                ->getQuery()
                ->getResult();
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.code',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(Promocode $promocode): void
    {
        $this->getEntityManager()->remove($promocode);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Promocode $promocode): void
    {
        $this->getEntityManager()->persist($promocode);
    }
}
