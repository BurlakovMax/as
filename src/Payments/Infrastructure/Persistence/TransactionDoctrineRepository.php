<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Payments\Domain\Transaction;
use App\Payments\Domain\TransactionReadStorage;
use App\Payments\Domain\TransactionWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\QueryException;

final class TransactionDoctrineRepository extends EntityRepository implements TransactionWriteStorage, TransactionReadStorage
{
    public function add(Transaction $transaction): void
    {
        $this->getEntityManager()->persist($transaction);
        $this->getEntityManager()->flush();
    }

    public function getAndLock(int $id): ?Transaction
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countByAccountId(int $accountId): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->innerJoin('t.payment', 'payment')
                ->addCriteria(
                    Criteria::create()
                        ->where(Criteria::expr()
                            ->eq('payment.accountId', $accountId))
                )
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @return Transaction[]
     * @throws QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');
        $builder->innerJoin('t.payment', 'payment');
        $builder
            ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
            ->addCriteria(Limitation::limitation($query->getLimitation()));

        Ordering::add($builder, $query->getOrderQueries());

        return
            $builder
                ->getQuery()
                ->getResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult();

        return (int)$count['count'];
    }
}
