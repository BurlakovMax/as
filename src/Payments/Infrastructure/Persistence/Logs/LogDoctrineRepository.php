<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Logs;

use App\Payments\Domain\Logs\Log;
use App\Payments\Domain\Logs\LogWriteStorage;
use Doctrine\ORM\EntityRepository;

final class LogDoctrineRepository extends EntityRepository implements LogWriteStorage
{
    public function add(Log $log): void
    {
        $this->getEntityManager()->persist($log);
    }
}