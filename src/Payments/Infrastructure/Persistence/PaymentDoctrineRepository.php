<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Payments\Domain\Payment;
use App\Payments\Domain\PaymentReadStorage;
use App\Payments\Domain\PaymentWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;

final class PaymentDoctrineRepository extends EntityRepository implements PaymentReadStorage,
                                                                          PaymentWriteStorage
{
    public function get(int $id): ?Payment
    {
        return $this->find($id);
    }

    /**
     * @return Payment[]
     * @throws QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');
        $builder
            ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
            ->addCriteria(Limitation::limitation($query->getLimitation()));

        Ordering::add($builder, $query->getOrderQueries());

        return
            $builder
                ->getQuery()
                ->getResult();
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult();

        return (int)$count['count'];
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countByAccountId(int $accountId): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->where(Criteria::expr()
                                    ->eq('accountId', $accountId))
                )
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws ORMException
     */
    public function add(Payment $subscription): void
    {
        $this->getEntityManager()->persist($subscription);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAndLockByLastOperation(string $lastOperation): ?Payment
    {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('lastOperation', $lastOperation))
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getOneOrNullResult()
            ;
    }

    public function getAndLock(int $id): ?Payment
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }
}
