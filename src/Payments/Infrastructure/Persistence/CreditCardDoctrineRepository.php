<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Payments\Domain\CreditCard;
use App\Payments\Domain\CreditCardReadStorage;
use App\Payments\Domain\CreditCardWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\QueryException;

final class CreditCardDoctrineRepository extends EntityRepository implements CreditCardReadStorage,
                                                                             CreditCardWriteStorage
{
    public function get(int $id): ?CreditCard
    {
        return $this->find($id);
    }

    /**
     * @return CreditCard[]
     * @throws QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.cardholderName.firstName',
                            't.cardholderName.lastName',
                            't.city',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(Limitation::limitation($query->getLimitation()))
                ->getQuery()
                ->getResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(
                    FuzzySearchBuilder::search(
                        $query->getFuzzy(),
                        [
                            't.cardholderName.firstName',
                            't.cardholderName.lastName',
                            't.city',
                        ]
                    )
                )
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function countByAccountId(int $accountId): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->where(Criteria::expr()->eq('accountId', $accountId))
                )
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(CreditCard $creditCard): void
    {
        $this->getEntityManager()->persist($creditCard);
        $this->getEntityManager()->flush();
    }

    public function getAndLock(int $id): ?CreditCard
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }

    /**
     * @throws NonUniqueResultException
     * @throws QueryException
     */
    public function getByCardParams(
        int $accountId,
        string $last4digits,
        string $expiryMonth,
        string $expiryYear
    ): ?CreditCard {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('accountId', $accountId))
                        ->andWhere(Criteria::expr()->contains('cardNumber', $last4digits))
                        ->andWhere(Criteria::expr()->eq('expiryMonth', $expiryMonth))
                        ->andWhere(Criteria::expr()->eq('expiryYear', $expiryYear))
                )
                ->getQuery()
                ->getOneOrNullResult()
            ;
    }
}
