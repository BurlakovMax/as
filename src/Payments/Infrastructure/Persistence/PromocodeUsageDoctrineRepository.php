<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Payments\Domain\PromocodeUsage;
use App\Payments\Domain\PromocodeUsageReadStorage;
use App\Payments\Domain\PromocodeUsageWriteStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

final class PromocodeUsageDoctrineRepository extends EntityRepository implements PromocodeUsageReadStorage, PromocodeUsageWriteStorage
{
    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByCodeIdAndAccountId(int $codeId, int $accountId): ?PromocodeUsage
    {
        $this->createQueryBuilder('t')
            ->addCriteria(
                Criteria::create()
                    ->where(Criteria::expr()
                        ->eq('accountId', $accountId))
                    ->andWhere(Criteria::expr()
                        ->eq('codeId', $codeId)
                    )
            )
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(PromocodeUsage $promocodeUsage): void
    {
        $this->getEntityManager()->persist($promocodeUsage);
    }
}
