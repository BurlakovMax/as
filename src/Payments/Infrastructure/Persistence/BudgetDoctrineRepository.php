<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Payments\Domain\Budget;
use App\Payments\Domain\BudgetReadStorage;
use App\Payments\Domain\BudgetWriteStorage;
use Doctrine\ORM\EntityRepository;

final class BudgetDoctrineRepository extends EntityRepository implements BudgetReadStorage, BudgetWriteStorage
{
    public function getByAccountId(int $accountId): ?Budget
    {
        return $this->find($accountId);
    }

    public function getBySearchQuery(SearchQuery $searchQuery): array
    {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->addCriteria(CriteriaBuilder::createByFilters($searchQuery->getFilters()))
                ->addCriteria(Limitation::limitation($searchQuery->getLimitation()))
                ->getQuery()
                ->getResult();
    }

    public function countBySearchQuery(SearchQuery $searchQuery): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.accountId) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($searchQuery->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(Budget $budget): void
    {
        $this->getEntityManager()->persist($budget);
    }
}
