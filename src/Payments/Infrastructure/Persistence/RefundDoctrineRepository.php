<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Payments\Domain\Refund;
use App\Payments\Domain\RefundWriteStorage;
use Doctrine\ORM\EntityRepository;

final class RefundDoctrineRepository extends EntityRepository implements RefundWriteStorage
{
    public function add(Refund $refund): void
    {
        $this->getEntityManager()->persist($refund);
    }
}