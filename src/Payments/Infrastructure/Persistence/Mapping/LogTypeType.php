<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\Logs\LogType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class LogTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return LogType::class;
    }

    public function getName(): string
    {
        return 'log_type';
    }
}