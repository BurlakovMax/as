<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\RefundType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class RefundTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return RefundType::class;
    }

    public function getName(): string
    {
        return 'refund_type';
    }
}