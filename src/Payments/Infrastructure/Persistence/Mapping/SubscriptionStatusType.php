<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\SubscriptionStatus;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class SubscriptionStatusType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return SubscriptionStatus::class;
    }

    public function getName(): string
    {
        return 'subscription_status';
    }
}