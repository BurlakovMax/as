<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\EscortPaymentStatus;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class EscortPaymentStatusType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return EscortPaymentStatus::class;
    }

    public function getName(): string
    {
        return 'escort_payment_status';
    }
}