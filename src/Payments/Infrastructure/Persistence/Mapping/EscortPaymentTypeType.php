<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\EscortPaymentType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class EscortPaymentTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return EscortPaymentType::class;
    }

    public function getName(): string
    {
        return 'escort_payment_type';
    }
}
