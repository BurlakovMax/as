<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\CreditCardType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class CreditCardTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return CreditCardType::class;
    }

    public function getName()
    {
        return 'credit_card_type';
    }
}
