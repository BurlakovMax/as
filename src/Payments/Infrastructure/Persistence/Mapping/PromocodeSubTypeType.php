<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\PromocodeSubType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class PromocodeSubTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return PromocodeSubType::class;
    }

    public function getName(): string
    {
        return 'promocode_subtype';
    }
}
