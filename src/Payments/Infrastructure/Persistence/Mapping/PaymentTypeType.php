<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\TransactionType;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class PaymentTypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return TransactionType::class;
    }

    public function getName(): string
    {
        return 'payment_type';
    }
}
