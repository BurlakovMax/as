<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\PromocodeCategory;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class PromocodeCategoryType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return PromocodeCategory::class;
    }

    public function getName(): string
    {
        return 'promocode_category';
    }
}
