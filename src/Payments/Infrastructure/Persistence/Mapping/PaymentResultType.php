<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence\Mapping;

use App\Payments\Domain\PaymentResult;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class PaymentResultType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return PaymentResult::class;
    }

    public function getName(): string
    {
        return 'payment_result';
    }
}