<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Payments\Domain\EscortPaymentType;
use App\Payments\Domain\PaymentItem;
use App\Payments\Domain\PaymentItemReadStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\QueryException;

final class PaymentItemDoctrineRepository extends EntityRepository implements PaymentItemReadStorage
{
    /**
     * @return PaymentItem[]
     * @throws QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');
        $builder
            ->addCriteria(
                Criteria::create()
                ->andWhere(
                    Criteria::expr()->orX(
                        Criteria::expr()->eq('t.subjectType', EscortPaymentType::escort()),
                        Criteria::expr()->eq('t.subjectType', EscortPaymentType::topUps())
                    )
                )
            )
            ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()));

        Ordering::add($builder, $query->getOrderQueries());

        return
            $builder
                ->getQuery()
                ->getResult();
    }
}