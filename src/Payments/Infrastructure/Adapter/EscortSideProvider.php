<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Adapter;

use App\Escorts\Application\EscortSideCommandProcessor;
use App\Payments\Domain\EscortSideStorage;

final class EscortSideProvider implements EscortSideStorage
{
    private EscortSideCommandProcessor $escortSideCommandProcessor;

    public function __construct(EscortSideCommandProcessor $escortSideCommandProcessor)
    {
        $this->escortSideCommandProcessor = $escortSideCommandProcessor;
    }

    /**
     * @throws \App\Escorts\Application\EscortNotFound
     * @throws \App\Escorts\Domain\EscortDoesNotContainsCity
     * @throws \Throwable
     */
    public function activateAfterPaid(int $escortId, int $locationId): void
    {
        $this->escortSideCommandProcessor->activateAfterPaid($escortId, $locationId);
    }

    public function getRecurringPeriod(): int
    {
        return $this->escortSideCommandProcessor->getRecurringPeriod();
    }
}