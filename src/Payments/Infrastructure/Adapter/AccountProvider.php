<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Adapter;

use App\Payments\Domain\AccountStorage;
use App\Security\Domain\AccountService;

final class AccountProvider implements AccountStorage
{
    private AccountService $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @throws \App\Security\Domain\AccountNotFound
     */
    public function addTopUps(int $accountId, int $topUps): void
    {
        $this->accountService->addTopUps($accountId, $topUps);
    }
}