<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Adapter;

use App\Payments\Domain\PlaceStorage;
use App\Places\Application\PlaceCommandProcessor;

final class PlaceProvider implements PlaceStorage
{
    private PlaceCommandProcessor $placeCommandProcessor;

    public function __construct(PlaceCommandProcessor $placeCommandProcessor)
    {
        $this->placeCommandProcessor = $placeCommandProcessor;
    }

    public function setBusinessOwner(int $placeId, int $businessOwnerId, int $recurringPeriod): void
    {
        $this->placeCommandProcessor->assignOwner($placeId, $businessOwnerId, $recurringPeriod);
    }
}