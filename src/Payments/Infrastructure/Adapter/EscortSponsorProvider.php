<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Adapter;

use App\Escorts\Application\EscortSponsorCommandProcessor;
use App\Payments\Domain\EscortSponsorStorage;

final class EscortSponsorProvider implements EscortSponsorStorage
{
    private EscortSponsorCommandProcessor $escortSponsorCommandProcessor;

    public function __construct(EscortSponsorCommandProcessor $escortSponsorCommandProcessor)
    {
        $this->escortSponsorCommandProcessor = $escortSponsorCommandProcessor;
    }

    /**
     * @throws \App\Escorts\Application\EscortNotFound
     * @throws \App\Escorts\Domain\EscortDoesNotContainsCity
     * @throws \Throwable
     */
    public function activateAfterPaid(int $escortId, int $locationId): void
    {
        $this->escortSponsorCommandProcessor->activateAfterPaid($escortId, $locationId);
    }

    public function getRecurringPeriod(): int
    {
        return $this->escortSponsorCommandProcessor->getRecurringPeriod();
    }
}