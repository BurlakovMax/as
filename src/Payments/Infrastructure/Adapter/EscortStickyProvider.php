<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Adapter;

use App\Escorts\Application\EscortStickyCommandProcessor;
use App\Escorts\Application\EscortStickyQueryProcessor;
use App\Payments\Domain\EscortStickyStorage;

final class EscortStickyProvider implements EscortStickyStorage
{
    private EscortStickyCommandProcessor $escortStickyCommandProcessor;

    private EscortStickyQueryProcessor $escortStickyQueryProcessor;

    public function __construct(
        EscortStickyCommandProcessor $escortStickyCommandProcessor,
        EscortStickyQueryProcessor $escortStickyQueryProcessor
    ) {
        $this->escortStickyCommandProcessor = $escortStickyCommandProcessor;
        $this->escortStickyQueryProcessor = $escortStickyQueryProcessor;
    }

    /**
     * @throws \App\Escorts\Application\EscortNotFound
     * @throws \App\Escorts\Domain\EscortDoesNotContainsCity
     * @throws \Throwable
     */
    public function activateAfterPaid(int $escortId, int $locationId, int $days): void
    {
        $this->escortStickyCommandProcessor->activateAfterPaid($escortId, $locationId, $days);
    }

    public function getRecurringPeriod(): int
    {
        return $this->escortStickyQueryProcessor->getRecurringPeriod();
    }
}