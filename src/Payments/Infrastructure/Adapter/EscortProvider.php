<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure\Adapter;

use App\Escorts\Domain\EscortService;
use App\Payments\Domain\EscortStorage;

final class EscortProvider implements EscortStorage
{
    private EscortService $escortService;

    public function __construct(EscortService $escortService)
    {
        $this->escortService = $escortService;
    }

    /**
     * @throws \App\Escorts\Application\EscortNotFound
     */
    public function activate(int $escortId): void
    {
        $this->escortService->activate($escortId);
    }

    public function getRecurringPeriod(): int
    {
        return $this->escortService->getRecurringPeriod();
    }
}