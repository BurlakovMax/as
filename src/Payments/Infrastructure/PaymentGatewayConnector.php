<?php

declare(strict_types=1);

namespace App\Payments\Infrastructure;

use App\Payments\Application\CreateTransactionCommand;
use App\Payments\Application\CreditCardData;
use App\Payments\Application\PaymentWasNotCreated;
use App\Payments\Domain\Payment;
use App\Payments\Domain\PaymentOperations;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class PaymentGatewayConnector implements PaymentOperations
{
    private LoggerInterface $logger;

    private string $paymentURL;

    private HttpClientInterface $client;

    public function __construct(LoggerInterface $logger, string $paymentURL, string $paymentToken)
    {
        $this->logger = $logger;
        $this->paymentURL = $paymentURL;
        $this->client = HttpClient::create(
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => "Bearer {$paymentToken}",
                ]
            ]
        );
    }

    public function pay(Payment $payment, CreditCardData $creditCard): CreateTransactionCommand
    {
        $json = [
            'id' => 1,
            'jsonrpc' => '2.0',
            'method' => 'com.payment-gateway.operation.pay-sync',
            'params' => [
                'totalCost' => [
                    'amount' => (int)$payment->getAmount()->getAmount(),
                    'currency' => $payment->getAmount()->getCurrency()->getCode()
                ],
                'email' => $payment->getEmail()->getValue(),
                'expMonth' => (int)$creditCard->getExpiryMonth(),
                'expYear' => (int)$creditCard->getExpiryYear(),
                'cvc' => (int)$creditCard->getCvc2(),
                'cardNumber' => $creditCard->getCardNumber(),
                'cardholderName' => $creditCard->getCardholderName(),
                'description' => (string)$payment->getId(),
            ],
        ];

        $response = $this->client->request(
            'POST',
            $this->paymentURL,
            [
                'json' => $json,
            ]
        );

        return $this->checkResponse($response->toArray());
    }

    public function refund(Payment $payment, int $amount): CreateTransactionCommand
    {
        $json = [
            'id' => 1,
            'jsonrpc' => '2.0',
            'method' => 'com.payment-gateway.operation.refund-sync',
            'params' => [
                'operationId' => $payment->getLastOperation(),
                'amount' => $amount,
                'description' => (string)$payment->getId(),
            ],
        ];

        $response = $this->client->request(
            'POST',
            $this->paymentURL,
            [
                'json' => $json,
            ]
        );

        return $this->checkResponse($response->toArray());
    }

    private function checkResponse(array $content): CreateTransactionCommand
    {
        if (array_key_exists('result', $content)) {
            return new CreateTransactionCommand(
                $content['result']['id'],
                $content['result']['merchantId'],
                $content['result']['providerId'],
                $content['result']['customerId'],
                $content['result']['cardId'],
                $content['result']['createdAt'],
                $content['result']['status'],
                $content['result']['changedAt'],
                $content['result']['description'],
                $content['result']['amount'],
                $content['result']['currency'],
                $content['result']['action'],
                $content['result']['externalId'],
                $content['result']['response'],
                $content['result']['errorReason'],
            );
        }

        $this->logger->error('Subscription pay error', ['content' => $content]);

        if (array_key_exists('error', $content)) {
            throw new PaymentWasNotCreated($content['error']['message']);
        }

        throw new PaymentWasNotCreated('PaymentGateway is not answer');
    }

    public function payWithCustomerData(Payment $payment, CreditCardData $creditCard): CreateTransactionCommand
    {
        $json = [
            'id' => 1,
            'jsonrpc' => '2.0',
            'method' => 'com.payment-gateway.operation.pay-with-customer-data-sync',
            'params' => [
                'totalCost' => [
                    'amount' => (int)$payment->getAmount()->getAmount(),
                    'currency' => $payment->getAmount()->getCurrency()->getCode()
                ],
                'email' => $payment->getEmail()->getValue(),
                'customerId' => $creditCard->getCustomerId(),
                'cardId' => $creditCard->getCardId(),
                'description' => (string)$payment->getId(),
            ],
        ];

        $response = $this->client->request(
            'POST',
            $this->paymentURL,
            [
                'json' => $json,
            ]
        );

        return $this->checkResponse($response->toArray());
    }
}
