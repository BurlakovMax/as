<?php

declare(strict_types=1);

namespace App\Security\Infrastructure;

use App\Security\Domain\ConfirmationCodeGenerator;

final class RandomConfirmationCodeGenerator implements ConfirmationCodeGenerator
{
    /** @var string */
    private $alphabet;

    /** @var int */
    private $length;

    public function __construct(string $alphabet, int $length)
    {
        $this->alphabet = $alphabet;
        $this->length = $length;
    }

    /**
     * @throws \Exception
     */
    public function generate(): string
    {
        $result = [];
        $size = strlen($this->alphabet);
        for ($i = 0; $i < $this->length; $i++) {
            $result[] = $this->alphabet[random_int(0, $size - 1)];
        }
        return implode('', $result);
    }
}
