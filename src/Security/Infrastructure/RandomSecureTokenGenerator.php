<?php

declare(strict_types=1);

namespace App\Security\Infrastructure;

use App\Security\Domain\SecureTokenGenerator;

final class RandomSecureTokenGenerator implements SecureTokenGenerator
{
    /** @var int */
    private $length;

    public function __construct(int $length)
    {
        $this->length = $length;
    }

    /**
     * Generate URI-safe secure token
     *
     * @throws \Exception
     */
    public function generate(): string
    {
        // to get base64 string of length n we only need 6/8*n bytes
        $byteCount = (int)ceil($this->length * .75);
        $bytes = random_bytes($byteCount);
        return rtrim(strtr(base64_encode($bytes), '+/', '-_'), '=');
    }
}
