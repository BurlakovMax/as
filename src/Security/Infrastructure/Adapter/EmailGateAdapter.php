<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Adapter;

use App\EmailGate\Domain\MessageSender;
use App\Security\Domain\EmailProvider;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\UuidInterface;

final class EmailGateAdapter implements EmailProvider
{
    /** @var MessageSender */
    private $messageSender;

    public function __construct(MessageSender $messageSender)
    {
        $this->messageSender = $messageSender;
    }

    public function sendSyncByRawValues(
        UuidInterface $messageId,
        Email $emailSender,
        Email $emailTo,
        string $body,
        string $subject,
        ?Email $replyTo = null
    ): void {
        $this->messageSender->sendSyncByRawValues(
            $messageId,
            $emailSender,
            $emailTo,
            $body,
            $subject,
            $replyTo
        );
    }
}