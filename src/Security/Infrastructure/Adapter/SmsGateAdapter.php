<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Adapter;

use LazyLemurs\Structures\PhoneNumber;
use App\Security\Domain\SmsProvider;
use App\SmsGate\Domain\MessageService;
use Ramsey\Uuid\Uuid;

final class SmsGateAdapter implements SmsProvider
{
    /** @var MessageService */
    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function send(PhoneNumber $number, string $text): void
    {
        $this->messageService->sendSync(Uuid::uuid4(), $number, $text);
    }
}