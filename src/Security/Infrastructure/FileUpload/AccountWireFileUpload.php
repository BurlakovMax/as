<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\FileUpload;

final class AccountWireFileUpload extends FileUpload
{
    /**
     * @param string[] $supportMimeTypes
     */
    public function __construct(string $rootDir, string $publicDir, string $dir, array $supportMimeTypes)
    {
        parent::__construct($rootDir, $publicDir, $dir, $supportMimeTypes);
    }
}
