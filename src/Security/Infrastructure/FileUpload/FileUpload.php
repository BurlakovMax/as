<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\FileUpload;

use App\Core\Infrastructure\ImageManager;
use App\Security\Domain\FileUpload as FileUploadInterface;
use LazyLemurs\FileUploader\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class FileUpload implements FileUploadInterface
{
    private string $rootDir;

    private string $publicDir;

    private string $dir;

    /**
     * @var string[]
     */
    private array $supportMimeTypes;

    /**
     * @param string[] $supportMimeTypes
     */
    public function __construct(string $rootDir, string $publicDir, string $dir, array $supportMimeTypes)
    {
        $this->rootDir = $rootDir;
        $this->publicDir = $publicDir;
        $this->dir = $dir;
        $this->supportMimeTypes = $supportMimeTypes;
    }

    /**
     * @throws \LazyLemurs\FileUploader\MimeTypeIsNotSupported
     */
    public function save(string $name, UploadedFile $file): string
    {
        $image = new File(
            $name,
            $this->rootDir,
            $this->publicDir,
            $this->dir,
            $this->supportMimeTypes,
            $file
        );

        $image->save();

        $url = ImageManager::resize($this->rootDir, $this->publicDir, $image->getDirWithFile(), 1200);
        ImageManager::remove($this->rootDir . $this->publicDir . $image->getDirWithFile());

        return $url;
    }
}
