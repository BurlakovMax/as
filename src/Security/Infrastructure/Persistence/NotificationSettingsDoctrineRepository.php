<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence;

use App\Security\Domain\NotificationSettings;
use App\Security\Domain\NotificationSettingsStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;

final class NotificationSettingsDoctrineRepository extends EntityRepository implements NotificationSettingsStorage
{
    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(NotificationSettings $notificationSettings): void
    {
        $this->getEntityManager()->persist($notificationSettings);
    }

    /**
     * @return NotificationSettings[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function findByAccountIdAndLock(int $accountId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('accountId', $accountId))
                )
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getResult()
            ;
    }

    /**
     * @return NotificationSettings[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function findByAccountId(int $accountId): array
    {
        return
            $this->createQueryBuilder('t')
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('accountId', $accountId))
                )
                ->getQuery()
                ->getResult()
            ;
    }
}