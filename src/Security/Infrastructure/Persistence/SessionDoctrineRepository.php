<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence;

use App\Security\Domain\Session;
use App\Security\Domain\SessionStorage;
use Doctrine\ORM\EntityRepository;

final class SessionDoctrineRepository extends EntityRepository implements SessionStorage
{
    public function findByToken(string $token): ?Session
    {
        return $this->find($token);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Session $session): void
    {
        $this->getEntityManager()->persist($session);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function destroy(Session $session): void
    {
        $this->getEntityManager()->remove($session);
    }

    public function forceLogout(int $accountId): void
    {
        $query = $this->getEntityManager()->createQuery('DELETE FROM App\Security\Domain\Session s WHERE s.accountId = :accountId');
        $query->setParameter('accountId', $accountId);
        $query->execute();
    }
}
