<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Security\Domain\Account;
use App\Security\Domain\AccountStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\QueryException;
use LazyLemurs\Structures\Email;

final class AccountDoctrineRepository extends EntityRepository implements AccountStorage
{
    public function findById(int $id): ?Account
    {
        return $this->find($id);
    }

    /**
     * @return Account[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.username', 't.email']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->addCriteria(
                    Limitation::limitation($query->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $query->getOrderQueries());

        return
            $queryBuilder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $query): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.username', 't.email']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    public function findByEmail(Email $email): ?Account
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function findByUsername(string $username): ?Account
    {
        return $this->findOneBy(['username' => $username]);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Account $account): void
    {
        $this->getEntityManager()->persist($account);
        $this->getEntityManager()->flush();
    }

    /**
     * @return Account[]
     * @throws QueryException
     */
    public function getAllByIds(array $accountIds): array
    {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->addCriteria(Criteria::create()->andWhere(
                    Criteria::expr()->in('id', $accountIds)
                ))
                ->getQuery()
                ->getResult();
    }

    public function getAndLock(int $id): ?Account
    {
        return $this->find($id, LockMode::PESSIMISTIC_WRITE);
    }
}
