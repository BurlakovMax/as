<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence\Mapping;

use App\Security\Domain\ConfirmationAction;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class ConfirmationActionType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return ConfirmationAction::class;
    }

    public function getName(): string
    {
        return 'confirmation_action';
    }
}
