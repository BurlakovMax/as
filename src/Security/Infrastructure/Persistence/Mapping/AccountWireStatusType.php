<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence\Mapping;

use App\Security\Domain\AccountWireStatus;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class AccountWireStatusType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return AccountWireStatus::class;
    }

    public function getName()
    {
        return 'account_wire_status';
    }
}
