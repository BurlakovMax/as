<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence;

use App\Security\Domain\Confirmation;
use App\Security\Domain\ConfirmationStorage;
use Doctrine\ORM\EntityRepository;

final class ConfirmationDoctrineRepository extends EntityRepository implements ConfirmationStorage
{
    public function get(string $id): ?Confirmation
    {
        return $this->find($id);
    }

    public function utilizeAttempt(string $id): void
    {
        $dql = <<<dql
UPDATE Security:Confirmation t
SET t.attemptsLeft = t.attemptsLeft - 1
WHERE t.id = :id
dql;
        $params = ['id' => $id];
        $this->getEntityManager()->createQuery($dql)->execute($params);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Confirmation $confirmation): void
    {
        $this->getEntityManager()->persist($confirmation);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(Confirmation $confirmation): void
    {
        $this->getEntityManager()->remove($confirmation);
    }
}
