<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Security\Domain\Spammer;
use App\Security\Domain\SpammerStorage;
use Doctrine\ORM\EntityRepository;

final class SpammerDoctrineRepository extends EntityRepository implements SpammerStorage
{
    /**
     * @return Spammer[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBySearchQuery(SearchQuery $searchQuery): array
    {
        $queryBuilder =
            $this
                ->createQueryBuilder('t')
                ->addCriteria(FuzzySearchBuilder::search($searchQuery->getFuzzy(), ['t.email', 't.ipAddress']))
                ->addCriteria(CriteriaBuilder::createByFilters($searchQuery->getFilters()))
                ->addCriteria(
                    Limitation::limitation($searchQuery->getLimitation())
                )
        ;

        Ordering::add($queryBuilder, $searchQuery->getOrderQueries());

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countBySearchQuery(SearchQuery $searchQuery): int
    {
        $count =
            $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->addCriteria(CriteriaBuilder::createByFilters($searchQuery->getFilters()))
                ->getQuery()
                ->getOneOrNullResult();

        return (int)$count['count'];
    }

    public function get(int $id): ?Spammer
    {
        return $this->find($id);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Spammer $spammer): void
    {
        $this->getEntityManager()->persist($spammer);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(Spammer $spammer): void
    {
        $this->getEntityManager()->remove($spammer);
    }
}
