<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Security\Domain\AccountWire;
use App\Security\Domain\AccountWireStorage;
use Doctrine\ORM\EntityRepository;

final class AccountWireDoctrineRepository extends EntityRepository implements AccountWireStorage
{
    /**
     * @return AccountWire[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        // TODO: Implement getBySearchQuery() method.
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        // TODO: Implement countBySearchQuery() method.
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(AccountWire $accountWire): void
    {
        $this->getEntityManager()->persist($accountWire);
    }
}
