<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\EventSubscribers;

use App\Security\Domain\EmailProvider;
use App\Security\Domain\PasswordResetRequested;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class PasswordResetRequestedSubscriber implements MessageHandlerInterface
{
    private EmailProvider $emailProvider;

    private string $fromEmail;

    public function __construct(EmailProvider $emailProvider, string $fromEmail)
    {
        $this->emailProvider = $emailProvider;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @throws \Throwable
     */
    public function __invoke(PasswordResetRequested $event): void
    {
        $this->emailProvider->sendSyncByRawValues(
            Uuid::uuid4(),
            new Email($this->fromEmail),
            $event->getEmail(),
            'Confirmation code: ' . $event->getConfirmationCode(),
            'Adultsearch confirmation code'
        );
    }
}
