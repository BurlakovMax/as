<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\EventSubscribers;

use App\Security\Domain\EmailProvider;
use App\Security\Domain\RegistrationRequested;
use App\Security\Domain\SmsProvider;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SymfonyRegistrationRequestedSubscriber implements MessageHandlerInterface
{
    private EmailProvider $emailProvider;

    private SmsProvider $smsProvider;

    private string $fromEmail;

    private string $appUrl;

    public function __construct(EmailProvider $emailProvider, SmsProvider $smsProvider, string $fromEmail, string $appUrl)
    {
        $this->emailProvider = $emailProvider;
        $this->smsProvider = $smsProvider;
        $this->fromEmail = $fromEmail;
        $this->appUrl = $appUrl;
    }

    /**
     * @throws \Throwable
     */
    public function __invoke(RegistrationRequested $event): void
    {
        $this->emailProvider->sendSyncByRawValues(
            Uuid::uuid4(),
            new Email($this->fromEmail),
            $event->getEmail(),
            $this->appUrl .
                '/api/v1/sign_up_confirm?confirmationId=' . $event->getConfirmationId() .
                '&confirmationCode=' . $event->getConfirmationCode(),
            'Adultsearch confirmation code'
        );
    }
}
