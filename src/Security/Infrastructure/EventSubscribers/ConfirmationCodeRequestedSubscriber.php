<?php

declare(strict_types=1);

namespace App\Security\Infrastructure\EventSubscribers;

use App\Security\Domain\ConfirmationCodeRequested;
use App\Security\Domain\EmailProvider;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ConfirmationCodeRequestedSubscriber implements MessageHandlerInterface
{
    private EmailProvider $emailProvider;

    private string $fromEmail;

    public function __construct(EmailProvider $emailProvider, string $fromEmail)
    {
        $this->emailProvider = $emailProvider;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @throws \Throwable
     */
    public function __invoke(ConfirmationCodeRequested $event): void
    {
        $this->emailProvider->sendSyncByRawValues(
            Uuid::uuid4(),
            new Email($this->fromEmail),
            $event->getEmail(),
            'Confirmation code: ' . $event->getConfirmationCode(),
            'Adultsearch confirmation code'
        );
    }
}
