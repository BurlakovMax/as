<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Core\DomainSupport\Enumerable;

final class AccountStatus extends Enumerable
{
    public static function active(): self
    {
        return self::createEnum('active');
    }

    public static function deleted(): self
    {
        return self::createEnum('deleted');
    }

    public static function banned(): self
    {
        return self::createEnum('banned');
    }
}