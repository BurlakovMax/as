<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\RegistrationRequested;

final class RegistrationResponse
{
    private int $accountId;

    private string $confirmationId;

    public function __construct(RegistrationRequested $registrationRequested)
    {
        $this->accountId = $registrationRequested->getId();
        $this->confirmationId = $registrationRequested->getConfirmationId();
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getConfirmationId(): string
    {
        return $this->confirmationId;
    }
}
