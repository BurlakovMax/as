<?php

declare(strict_types=1);

namespace App\Security\Application;

class CreateNotificationSettingsCommand
{
    private string $notificationName;

    private bool $isEnable;

    public function __construct(string $notificationName, bool $isEnable)
    {
        $this->notificationName = $notificationName;
        $this->isEnable = $isEnable;
    }
    public function getNotificationName(): string
    {
        return $this->notificationName;
    }

    public function isEnable(): bool
    {
        return $this->isEnable;
    }
}