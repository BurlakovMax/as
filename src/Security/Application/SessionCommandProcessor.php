<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\AccountStorage;
use App\Security\Domain\SecureTokenGenerator;
use App\Security\Domain\Session;
use App\Security\Domain\SessionStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class SessionCommandProcessor
{
    private SessionStorage $sessionStorage;

    private AccountStorage $accountStorage;

    private TransactionManager $transactionManager;

    private SecureTokenGenerator $tokenGenerator;

    public function __construct(
        SessionStorage $sessionStorage,
        TransactionManager $transactionManager,
        AccountStorage $accountStorage,
        SecureTokenGenerator $secureTokenGenerator
    ) {
        $this->sessionStorage = $sessionStorage;
        $this->transactionManager = $transactionManager;
        $this->accountStorage = $accountStorage;
        $this->tokenGenerator = $secureTokenGenerator;
    }

    /**
     * @throws \Throwable
     */
    public function add(int $accountId): string
    {
        $token = $this->tokenGenerator->generate();

        $this->transactionManager->transactional(function () use ($accountId, $token): void {
            $account = $this->accountStorage->findById($accountId);

            $this->sessionStorage->add(
                new Session($token, $account, new \DateTimeImmutable('+1 day'))
            );
        });

        return $token;
    }
}
