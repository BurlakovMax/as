<?php

declare(strict_types=1);

namespace App\Security\Application;

use Money\Money;

final class AccountWireCreateCommand
{
    private int $accountId;

    private string $name;

    private string $amount;

    private string $bankName;

    private string $image;

    public function __construct(int $accountId, string $name, string $amount, string $bankName, string $image)
    {
        $this->accountId = $accountId;
        $this->name = $name;
        $this->amount = $amount;
        $this->bankName = $bankName;
        $this->image = $image;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getBankName(): string
    {
        return $this->bankName;
    }

    public function getImage(): string
    {
        return $this->image;
    }
}
