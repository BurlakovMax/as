<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\Account;
use LazyLemurs\Structures\Email;
use Symfony\Component\Security\Core\User\UserInterface;
use Swagger\Annotations as SWG;

final class AccountData implements UserInterface
{
    /**
     * @var string[]
     *
     * @SWG\Property(
     *     property="roles",
     *     type="array",
     *     @SWG\Items(type="string"))
     * )
     */
    private array $roles;

    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private Email $email;

    /**
     * @SWG\Property()
     */
    private bool $emailConfirmed;

    /**
     * @SWG\Property()
     */
    private string $username;

    /**
     * @SWG\Property()
     */
    private ?string $avatar;

    /**
     * @SWG\Property()
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @SWG\Property()
     */
    private ?\DateTimeImmutable $deletedOn;

    /**
     * @SWG\Property()
     */
    private bool $whitelisted;

    /**
     * @SWG\Property()
     */
    private bool $isBanned;

    /**
     * @SWG\Property()
     */
    private ?string $banReason;

    /**
     * @SWG\Property()
     */
    private bool $agency;

    /**
     * @SWG\Property()
     */
    private bool $worker;

    /**
     * @SWG\Property()
     */
    private int $topUps;

    /**
     * @SWG\Property()
     */
    private int $forumPostsCount;

    /**
     * @SWG\Property()
     */
    private ?int $accountLevel;

    /**
     * @SWG\Property()
     */
    private ?bool $advertiser;

    /**
     * @SWG\Property()
     */
    private ?string $notes;

    /**
     * @SWG\Property()
     */
    private int $maxEscortsNumber;

    public function __construct(Account $account)
    {
        $this->id = $account->getId();
        $this->roles = $account->getRoles();
        $this->email = $account->getEmail();
        $this->emailConfirmed = $account->isEmailConfirmed();
        $this->username = $account->getUsername();
        $this->whitelisted = $account->isWhitelisted();
        $this->avatar = $account->getAvatar();
        $this->createdAt = $account->getCreatedAt();
        $this->deletedOn = $account->getDeletedOn();
        $this->topUps = $account->getTopUps() ?? 0;
        $this->forumPostsCount = $account->forumPostsCount();
        $this->accountLevel = $account->getAccountLevel();
        $this->agency = $account->isAgency();
        $this->worker = $account->isWorker();
        $this->advertiser = $account->isAdvertiser();
        $this->notes = $account->getNotes();
        $this->maxEscortsNumber = $account->escortsMaxNumber() ?? 0;
        $this->isBanned = $account->isBanned();
        $this->banReason = $account->getBanReason();
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getPassword(): ?string
    {
        return null;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->email->getValue();
    }

    public function eraseCredentials(): void
    {
        /*_*/
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->username;
    }

    public function isWhitelisted(): ?bool
    {
        return $this->whitelisted;
    }

    public function isEmailConfirmed(): ?bool
    {
        return $this->emailConfirmed;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getDeletedOn(): ?\DateTimeImmutable
    {
        return $this->deletedOn;
    }

    public function getTopUps(): ?int
    {
        return $this->topUps;
    }

    public function getForumPostCount(): int
    {
        return $this->forumPostsCount;
    }

    public function getAccountLevel(): ?int
    {
        return $this->accountLevel;
    }

    public function isAgency(): bool
    {
        return $this->agency;
    }

    public function isWorker(): bool
    {
        return $this->worker;
    }

    public function isAdvertiser(): bool
    {
        return $this->advertiser;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function getMaxEscortsNumber(): int
    {
        return $this->maxEscortsNumber;
    }

    public function isBanned(): bool
    {
        return $this->isBanned;
    }

    public function getBanReason(): ?string
    {
        return $this->banReason;
    }
}
