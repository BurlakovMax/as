<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\NotificationSettings;
use App\Security\Domain\NotificationSettingsStorage;

final class NotificationSettingsQueryProcessor
{
    private NotificationSettingsStorage $notificationSettingsStorage;

    public function __construct(NotificationSettingsStorage $notificationSettingsStorage)
    {
        $this->notificationSettingsStorage = $notificationSettingsStorage;
    }

    /**
     * @return mixed[]
     */
    public function findByAccountId(int $accountId): array
    {
        $notifications = array_map(
            [ $this, 'mapToData' ],
            $this->notificationSettingsStorage->findByAccountId($accountId)
        );

        $newComment = false;
        $newCommentMe = false;
        foreach ($notifications as $notification) {
            if ($notification->getNotificationName() === 'newcomment') {
                $newComment = $notification->isEnable();
            }

            if ($notification->getNotificationName() === 'newcommentme') {
                $newCommentMe = $notification->isEnable();
            }
        }

        return [
            'newcomment' => $newComment,
            'newcommentme' => $newCommentMe
        ];
    }

    private function mapToData(NotificationSettings $notificationSettings): NotificationSettingsData
    {
        return new NotificationSettingsData($notificationSettings);
    }
}