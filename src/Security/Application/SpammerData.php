<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\Spammer;

final class SpammerData
{
    private int $id;

    private ?string $email;

    private ?string $ipAddress;

    private bool $isStrict;

    private ?int $createdBy;

    private ?\DateTimeImmutable $createdAt;

    public function __construct(Spammer $spammer)
    {
        $this->id = $spammer->getId();
        $this->email = $spammer->getEmail();
        $this->ipAddress = $spammer->getIpAddress();
        $this->isStrict = $spammer->isStrict();
        $this->createdBy = $spammer->getCreatedBy();
        $this->createdAt = $spammer->getCreatedAt();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function getIsStrict(): bool
    {
        return $this->isStrict;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }
}
