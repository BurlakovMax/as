<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\AuthenticationException;
use App\Security\Domain\Authenticator;
use LazyLemurs\Structures\Email;
use LazyLemurs\TransactionManager\TransactionManager;
use Psr\Log\LoggerInterface;

final class AuthenticationService
{
    private TransactionManager $transactionManager;

    private Authenticator $authenticator;

    private LoggerInterface $logger;

    public function __construct(
        Authenticator $authenticator,
        TransactionManager $transactionManager,
        LoggerInterface $logger
    ) {
        $this->authenticator = $authenticator;
        $this->transactionManager = $transactionManager;
        $this->logger = $logger;
    }

    /**
     * @throws \Throwable
     */
    public function authenticateAccount(Email $email, string $password): AccountData
    {
        try {
            return new AccountData($this->authenticator->authenticateAccount($email, $password));
        } catch (AuthenticationException $exception) {
            throw $exception;
        } catch (\Throwable $exception) {
            $this->logger->error(
                sprintf(
                    'Unable to authenticate user. Email: %s, error message: %s',
                    $email,
                    $exception->getMessage()
                )
            );
            throw $exception;
        }
    }

    /**
     * @throws \Throwable
     */
    public function authenticateAccountWithToken(Email $email, string $password): string
    {
        return $this->transactionManager->transactional(
            function () use ($email, $password) {
                try {
                    return $this->authenticator->authenticateAccountWithToken($email, $password);
                } catch (AuthenticationException $exception) {
                    throw $exception;
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to authenticate user. Email: %s, error message: %s',
                            $email,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }

    /**
     * @throws \Throwable
     */
    public function invalidateToken(string $token): void
    {
        $this->transactionManager->transactional(
            function () use ($token) {
                try {
                    $this->authenticator->invalidateToken($token);
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to invalidate token. Token: %s, error message: %s',
                            $token,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }
}
