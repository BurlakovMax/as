<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Core\DomainSupport\Enumerable;

final class AccountRoles extends Enumerable
{
    public static function user(): self
    {
        return self::createEnum('ROLE_USER');
    }

    public static function advertiser(): self
    {
        return self::createEnum('ROLE_ADVERTISER');
    }

    public static function agency(): self
    {
        return self::createEnum('ROLE_AGENCY');
    }

    public static function worker(): self
    {
        return self::createEnum('ROLE_WORKER');
    }
}