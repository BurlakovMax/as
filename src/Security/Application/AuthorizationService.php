<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\Authorizer;

final class AuthorizationService
{
    private Authorizer $authorizer;

    public function __construct(Authorizer $authorizer)
    {
        $this->authorizer = $authorizer;
    }

    /**
     * @throws \Exception
     */
    public function authorize(string $token): ?SessionData
    {
        $session = $this->authorizer->authorize($token);

        if ($session === null) {
            return null;
        }

        return new SessionData(
            $session->getAccountId(),
            $session->getToken(),
            $session->getExpiresAt(),
            $session->getAccountRoles(),
            $session->getAccountEmail(),
            $session->isAgency()
        );
    }
}
