<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\BadPassword;
use App\Security\Domain\EmailTaken;
use App\Security\Domain\InvalidConfirmation;
use App\Security\Domain\InvalidConfirmationCode;
use App\Security\Domain\UsernameTaken;
use App\Security\Domain\AccountNotFound;
use App\Security\Domain\AccountService;
use LazyLemurs\Structures\Email;
use LazyLemurs\TransactionManager\TransactionManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class AccountCommandService
{
    private AccountService $accountService;

    private TransactionManager $transactionManager;

    private LoggerInterface $logger;

    private MessageBusInterface $bus;

    public function __construct(
        AccountService $accountService,
        TransactionManager $transactionManager,
        LoggerInterface $logger,
        MessageBusInterface $bus
    ) {
        $this->accountService = $accountService;
        $this->transactionManager = $transactionManager;
        $this->logger = $logger;
        $this->bus = $bus;
    }

    /**
     * @throws \Throwable
     */
    public function register(Email $email, string $username, string $password, string $ipAddress): RegistrationResponse
    {
        return $this->transactionManager->transactional(
            function () use ($email, $username, $password, $ipAddress) {
                try {
                    $registrationRequested = $this->accountService->register($email, $username, $password, $ipAddress);
                    $this->bus->dispatch($registrationRequested);

                    return new RegistrationResponse($registrationRequested);
                } catch (UsernameTaken | EmailTaken | BadPassword $exception) {
                    throw $exception;
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to register user. Email: %s, error message: %s',
                            $email,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }

    /**
     * @throws \Throwable
     */
    public function update(AccountUpdateCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
            $this->accountService->update($command);
        });
    }

    /**
     * @throws \Throwable
     */
    public function confirmRegistration(string $confirmationId, string $confirmationCode): void
    {
        $this->transactionManager->transactional(
            function () use ($confirmationId, $confirmationCode) {
                try {
                    $this->accountService->confirmRegistration($confirmationId, $confirmationCode);
                } catch (InvalidConfirmationCode $exception) {
                    $this->accountService->utilizeConfirmationAttempt($confirmationId);
                    throw $exception;
                } catch (InvalidConfirmation $exception) {
                    throw $exception;
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to confirm registration. Confirmation: %s, code: %d, error message: %s',
                            $confirmationId,
                            $confirmationCode,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }

    /**
     * @throws \Throwable
     */
    public function requestPasswordReset(Email $email): string
    {
        return $this->transactionManager->transactional(
            function () use ($email) {
                try {
                    $event = $this->accountService->requestPasswordReset($email);
                    $this->bus->dispatch($event);
                    return $event->getConfirmationId();
                } catch (AccountNotFound $exception) {
                    throw $exception;
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to reset password. Email: %s, error message: %s',
                            $email,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }

    /**
     * @throws \Throwable
     * @throws InvalidConfirmation
     * @throws InvalidConfirmationCode
     * @throws BadPassword
     */
    public function confirmPasswordReset(string $confirmationId, string $confirmationCode, string $password): void
    {
        try {
            $this->transactionManager->transactional(
                function () use ($confirmationId, $confirmationCode, $password) {
                    try {
                        $this->accountService->confirmPasswordReset($confirmationId, $confirmationCode, $password);
                    } catch (InvalidConfirmation | InvalidConfirmationCode | BadPassword $exception) {
                        throw $exception;
                    } catch (\Throwable $exception) {
                        $this->logger->error(
                            sprintf(
                                'Unable to confirm reset password. Confirmation: %s, code: %d, error message: %s',
                                $confirmationId,
                                $confirmationCode,
                                $exception->getMessage()
                            )
                        );
                        throw $exception;
                    }
                }
            );
        } catch (InvalidConfirmationCode $exception) {
            $this->accountService->utilizeConfirmationAttempt($confirmationId);
            throw $exception;
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @throws \Throwable
     */
    public function updateUsername(int $userId, ?string $username): void
    {
        $this->transactionManager->transactional(
            function () use ($userId, $username) {
                try {
                    $this->accountService->updateUsername($userId, $username);
                } catch (AccountNotFound | UsernameTaken $exception) {
                    throw $exception;
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to update user. AccountId: %s, error message: %s',
                            $userId,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }

    /**
     * @throws \Throwable
     */
    public function changeEmail(int $userId, Email $email): string
    {
        return
            $this->transactionManager->transactional(
                function () use ($userId, $email): string {
                    try {
                        $event = $this->accountService->changeEmail($userId, $email);
                        $this->bus->dispatch($event);
                        return $event->getConfirmationId();
                    } catch (AccountNotFound | EmailTaken $exception) {
                        throw $exception;
                    } catch (\Throwable $exception) {
                        $this->logger->error(
                            sprintf(
                                'Unable to update user. AccountId: %s, error message: %s',
                                $userId,
                                $exception->getMessage()
                            )
                        );
                        throw $exception;
                    }
                }
            )
        ;
    }

    public function requestConfirmationCode(Email $email, int $accountId): string
    {
        return
            $this->transactionManager->transactional(
                function () use ($accountId, $email): string {
                    try {
                        $event = $this->accountService->requestConfirmationCode($email, $accountId);
                        $this->bus->dispatch($event);
                        return $event->getConfirmationId();
                    } catch (AccountNotFound $exception) {
                        throw $exception;
                    } catch (\Throwable $exception) {
                        $this->logger->error(
                            sprintf(
                                'Unable to send new confirmation code to user. AccountId: %s, error message: %s',
                                $accountId,
                                $exception->getMessage()
                            )
                        );
                        throw $exception;
                    }
                }
            );
    }

    public function setEmailConfirmed(int $accountId, string $confirmationId, string $confirmationCode): void
    {
        $this->transactionManager->transactional(
            function () use ($accountId, $confirmationId, $confirmationCode): void {
                try {
                    $this->accountService->setEmailConfirmed($accountId, $confirmationId, $confirmationCode);
                } catch (InvalidConfirmation | InvalidConfirmationCode | AccountNotFound $exception) {
                    throw $exception;
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to confirm change e-mail. Confirmation: %s, code: %d, error message: %s',
                            $confirmationId,
                            $confirmationCode,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }

    /**
     * @throws \Throwable
     */
    public function updatePassword(string $password, int $userId, string $oldPassword): void
    {
        $this->transactionManager->transactional(function () use ($password, $userId, $oldPassword): void {
                try {
                    $this->accountService->updatePassword($password, $userId, $oldPassword);
                } catch (AccountNotFound $exception) {
                    throw $exception;
                } catch (\Throwable $exception) {
                    $this->logger->error(
                        sprintf(
                            'Unable to update user. AccountId: %s, error message: %s',
                            $userId,
                            $exception->getMessage()
                        )
                    );
                    throw $exception;
                }
            }
        );
    }

    public function setAvatar(int $accountId, string $image): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $image): void {
            $this->accountService->setAvatar($accountId, $image);
        });
    }

    public function ban(int $accountId, bool $isBanned, ?string $banReason): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $isBanned, $banReason): void {
            $this->accountService->ban($accountId, $isBanned, $banReason);
        });
    }

    public function whiteList(int $accountId, bool $isWhitelisted): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $isWhitelisted): void {
            $this->accountService->whitelist($accountId, $isWhitelisted);
        });
    }

    public function delete(int $accountId, ?string $deleteReason): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $deleteReason): void {
            $this->accountService->delete($accountId, $deleteReason);
        });
    }

    public function forceLogout(int $accountId): void
    {
        $this->transactionManager->transactional(function () use ($accountId): void {
           $this->accountService->forceLogout($accountId);
        });
    }
}
