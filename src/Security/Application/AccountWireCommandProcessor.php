<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\AccountWire;
use App\Security\Domain\AccountWireStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class AccountWireCommandProcessor
{
    private AccountWireStorage $storage;

    private TransactionManager $transactionManager;

    public function __construct(AccountWireStorage $storage, TransactionManager $transactionManager)
    {
        $this->storage = $storage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function add(AccountWireCreateCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($command): void {
           $this->storage->add(new AccountWire($command));
        });
    }
}
