<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\Spammer;
use App\Security\Domain\SpammerNotFound;
use App\Security\Domain\SpammerStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class SpammerCommandProcessor
{
    private SpammerStorage $spammerStorage;

    private TransactionManager $transactionManager;

    public function __construct(SpammerStorage $spammerStorage, TransactionManager $transactionManager)
    {
        $this->spammerStorage = $spammerStorage;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws \Throwable
     */
    public function add(SpammerCommand $spammerCommand): void
    {
        $this->transactionManager->transactional(function () use ($spammerCommand): void {
           $this->spammerStorage->add(new Spammer($spammerCommand));
        });
    }

    /**
     * @throws \Throwable
     */
    public function remove(int $id): void
    {
        $this->transactionManager->transactional(function () use ($id): void {
            $spammer = $this->spammerStorage->get($id);

            if (null === $spammer) {
                throw new SpammerNotFound();
            }

            $this->spammerStorage->remove($spammer);
        });
    }
}
