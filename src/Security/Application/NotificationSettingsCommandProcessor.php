<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\NotificationSettings;
use App\Security\Domain\NotificationSettingsStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class NotificationSettingsCommandProcessor
{
    private TransactionManager $transactionManager;

    private NotificationSettingsStorage $notificationSettingsStorage;

    public function __construct(
        TransactionManager $transactionManager,
        NotificationSettingsStorage $notificationSettingsStorage
    ) {
        $this->transactionManager = $transactionManager;
        $this->notificationSettingsStorage = $notificationSettingsStorage;
    }

    /**
     * @throws \Throwable
     */
    public function add(int $accountId, CreateNotificationSettingsCommand $command): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $command): void {
            $this->notificationSettingsStorage->add(
                new NotificationSettings(
                    $accountId,
                    $command->getNotificationName(),
                    $command->isEnable()
                )
            );
        });
    }

    /**
     * @param mixed[] $notifications
     * @param mixed[] $settings
     * @throws \Throwable
     */
    public function changeSettings(int $accountId, array $notifications, array $settings): void
    {
        foreach (['newcomment', 'newcommentme'] as $setting) {
            if (array_key_exists($setting, $notifications)) {
                $command = new UpdateNotificationSettingsCommand($setting, $settings[$setting]);
                $this->update($accountId, [$command]);
            } else {
                $command = new CreateNotificationSettingsCommand($setting, $settings[$setting]);
                $this->add($accountId, $command);
            }
        }
    }

    /**
     * @param UpdateNotificationSettingsCommand[] $commands
     * @throws \Throwable
     */
    public function update(int $accountId, array $commands): void
    {
        $this->transactionManager->transactional(function () use ($accountId, $commands): void {
            $notifications = $this->notificationSettingsStorage->findByAccountIdAndLock($accountId);

            foreach ($notifications as $notification) {
                foreach ($commands as $command) {
                    $notification->update($command->isEnable(), $command->getNotificationName());
                }
            }
        });
    }
}