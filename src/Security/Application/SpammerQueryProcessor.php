<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Core\Application\Search\SearchQuery;
use App\Security\Domain\Spammer;
use App\Security\Domain\SpammerStorage;

final class SpammerQueryProcessor
{
    private SpammerStorage $spammerStorage;

    public function __construct(SpammerStorage $spammerStorage)
    {
        $this->spammerStorage = $spammerStorage;
    }

    /**
     * @return SpammerData[]
     */
    public function getBySearchQuery(SearchQuery $searchQuery): array
    {
        return $this->mapListToData($this->spammerStorage->getBySearchQuery($searchQuery));
    }

    public function countBySearchQuery(SearchQuery $searchQuery): int
    {
        return $this->spammerStorage->countBySearchQuery($searchQuery);
    }

    /**
     * @param Spammer[] $spammers
     * @return SpammerData[]
     */
    private function mapListToData(array $spammers): array
    {
        return array_map(fn($spammer): SpammerData => new SpammerData($spammer), $spammers);
    }
}
