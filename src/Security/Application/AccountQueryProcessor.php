<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Core\Application\Search\SearchQuery;
use App\Security\Domain\Account;
use App\Security\Domain\AccountNotFound;
use App\Security\Domain\AccountStorage;
use LazyLemurs\Structures\Email;

final class AccountQueryProcessor
{
    private AccountStorage $accountStorage;

    public function __construct(AccountStorage $accountStorage)
    {
        $this->accountStorage = $accountStorage;
    }

    /**
     * @return AccountData[]
     */
    public function getBySearchQuery(SearchQuery $query): array
    {
        $list = $this->accountStorage->getBySearchQuery($query);

        return $this->mapListToData($list);
    }

    public function countBySearchQuery(SearchQuery $query): int
    {
        return $this->accountStorage->countBySearchQuery($query);
    }

    /**
     * @param Account[] $list
     * @return AccountData[]
     */
    private function mapListToData(array $list): array
    {
        return array_map(
            function (Account $account): AccountData {
                return $this->mapToData($account);
            },
            $list
        );
    }

    /**
     * @throws AccountNotFound
     */
    public function getByEmail(Email $email): AccountData
    {
        $account = $this->accountStorage->findByEmail($email);

        if (!$account) {
            throw new AccountNotFound();
        }

        return $this->mapToData($account);
    }

    /**
     * @throws AccountNotFound
     */
    public function getAccount(int $accountId): AccountData
    {
        return $this->mapToData(
            $this->get($accountId)
        );
    }

    public function isAgency(?int $accountId): bool
    {
        if (null !== $accountId) {
            $account = $this->get($accountId);
            return $account->isAgency();
        }

        return false;
    }

    /**
     * @throws AccountNotFound
     */
    private function get(int $accountId): Account
    {
        $account = $this->accountStorage->findById($accountId);

        if (!$account) {
            throw new AccountNotFound();
        }

        return $account;
    }

    private function mapToData(Account $account): AccountData
    {
        return new AccountData($account);
    }

    /**
     * @return AccountData[]
     */
    public function getAccountsByIds(array $accountIds): array
    {
        return array_map([ $this, 'mapToData' ], $this->accountStorage->getAllByIds($accountIds));
    }
}