<?php

declare(strict_types=1);

namespace App\Security\Application;

use LazyLemurs\Structures\Email;

final class SessionData
{
    private int $accountId;

    private string $token;

    private \DateTimeImmutable $expiresAt;

    /** @var string[] */
    private array $roles;

    private Email $email;

    private bool $isAgency;

    public function __construct(
        int $accountId,
        string $token,
        \DateTimeImmutable $expiresAt,
        array $accountRoles,
        Email $accountEmail,
        bool $isAgency
    ) {
        $this->accountId = $accountId;
        $this->token = $token;
        $this->expiresAt = $expiresAt;
        $this->roles = $accountRoles;
        $this->email = $accountEmail;
        $this->isAgency = $isAgency;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getExpiresAt(): \DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function isAgency(): bool
    {
        return $this->isAgency;
    }
}
