<?php

declare(strict_types=1);

namespace App\Security\Application;

use LazyLemurs\Commander\Property;

final class AccountUpdateCommand
{
    private int $id;

    /**
     * @Property(type="?string")
     */
    private ?string $password;

    /**
     * @Property(type="?string")
     */
    private ?string $notes;

    /**
     * @Property(type="?bool")
     */
    private ?bool $isWorker;

    /**
     * @Property(type="?bool")
     */
    private ?bool $isAgency;

    /**
     * @Property(type="?bool")
     */
    private ?bool $isAdvertiser;

    /**
     * @Property(type="int")
     */
    private int $maxEscortsNumber;

    /**
     * @Property(type="int")
     */
    private int $topUps;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function isWorker(): bool
    {
        return null !== $this->isWorker;
    }

    public function isAgency(): bool
    {
        return null !== $this->isAgency;
    }

    public function isAdvertiser(): bool
    {
        return null !== $this->isAdvertiser;
    }

    public function getMaxEscortsNumber(): int
    {
        return $this->maxEscortsNumber;
    }

    public function getTopUps(): int
    {
        return $this->topUps;
    }
}
