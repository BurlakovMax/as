<?php

declare(strict_types=1);

namespace App\Security\Application;

final class SpammerCommand
{
    private ?string $email;

    private ?string $ipAddress;

    private bool $isStrict;

    private int $createdBy;

    private \DateTimeImmutable $createdAt;

    public function __construct(?string $email, ?string $ipAddress, int $createdBy, bool $isStrict)
    {
        $this->email = $email;
        $this->ipAddress = $ipAddress;
        $this->isStrict = $isStrict;
        $this->createdBy = $createdBy;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function getIsStrict(): bool
    {
        return $this->isStrict;
    }

    public function getCreatedBy(): int
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
