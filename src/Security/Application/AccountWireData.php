<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\AccountWire;
use App\Security\Domain\AccountWireStatus;
use Money\Money;

final class AccountWireData
{
    private int $id;

    private int $accountId;

    private string $name;

    private string $amount;

    private string $bankName;

    private string $image;

    private AccountWireStatus $status;

    private \DateTimeImmutable $createdAt;

    private ?\DateTimeImmutable $processedAt;

    private ?int $processedBy;

    public function __construct(
        AccountWire $accountWire
    ) {
        $this->id = $accountWire->getId();
        $this->accountId = $accountWire->getAccountId();
        $this->name = $accountWire->getName();
        $this->amount = $accountWire->getAmount();
        $this->bankName = $accountWire->getBankName();
        $this->image = $accountWire->getImage();
        $this->status = $accountWire->getStatus();
        $this->createdAt = $accountWire->getCreatedAt();
        $this->processedAt = $accountWire->getProcessedAt();
        $this->processedBy = $accountWire->getProcessedBy();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getBankName(): string
    {
        return $this->bankName;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getStatus(): AccountWireStatus
    {
        return $this->status;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getProcessedAt(): ?\DateTimeImmutable
    {
        return $this->processedAt;
    }

    public function getProcessedBy(): ?int
    {
        return $this->processedBy;
    }
}
