<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\FileUpload;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class AvatarUploadProcessor
{
    private FileUpload $fileUpload;

    public function __construct(FileUpload $fileUpload)
    {
        $this->fileUpload = $fileUpload;
    }

    public function save(string $name, UploadedFile $uploadedFile): string
    {
        return $this->fileUpload->save($name, $uploadedFile);
    }
}