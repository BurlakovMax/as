<?php

declare(strict_types=1);

namespace App\Security\Application;

use App\Security\Domain\NotificationSettings;

final class NotificationSettingsData
{
    private NotificationSettings $notificationSettings;

    public function __construct(NotificationSettings $notificationSettings)
    {
        $this->notificationSettings = $notificationSettings;
    }

    public function getId(): int
    {
        return $this->getNotificationSettings()->getId();
    }

    public function getAccountId(): int
    {
        return $this->getNotificationSettings()->getAccountId();
    }

    public function getNotificationName(): string
    {
        return $this->getNotificationSettings()->getNotificationName();
    }

    public function isEnable(): bool
    {
        return $this->getNotificationSettings()->isEnable();
    }

    private function getNotificationSettings(): NotificationSettings
    {
        return $this->notificationSettings;
    }
}