<?php

declare(strict_types=1);

namespace App\Security\Presentation\EventSubscribers;

use App\Security\Domain\EmailProvider;
use App\Security\Domain\PasswordResetRequested;
use App\Security\Domain\PhoneConfirmationRequested;
use App\Security\Domain\RegistrationRequested;
use App\Security\Domain\SmsProvider;
use LazyLemurs\DomainEvents\DomainEvent;
use LazyLemurs\DomainEvents\DomainEventSubscriber;
use LazyLemurs\Structures\Email;
use Ramsey\Uuid\Uuid;

final class NotificationSubscriber implements DomainEventSubscriber
{
    /** @var EmailProvider */
    private $emailProvider;

    /** @var SmsProvider */
    private $smsProvider;

    /** @var string */
    private $fromEmail;

    public function __construct(EmailProvider $emailProvider, SmsProvider $smsProvider, string $fromEmail)
    {
        $this->emailProvider = $emailProvider;
        $this->smsProvider = $smsProvider;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return string[] Names of event's classes.
     */
    public function getListenedEventClasses(): array
    {
        return [
            RegistrationRequested::class,
            PasswordResetRequested::class,
            PhoneConfirmationRequested::class,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function handleEvent(DomainEvent $event): void
    {
        switch (true) {
            case $event instanceof RegistrationRequested:
                $this->emailProvider->sendSyncByRawValues(
                    Uuid::uuid4(),
                    new Email($this->fromEmail),
                    $event->getEmail(),
                    'Confirmation code:' . $event->getConfirmationCode(),
                    'Adultsearch confirmation code'
                );
                break;
            case $event instanceof PasswordResetRequested:
                $this->emailProvider->sendSyncByRawValues(
                    Uuid::uuid4(),
                    new Email($this->fromEmail),
                    $event->getEmail(),
                    'Confirmation code:' . $event->getConfirmationCode(),
                    'Adultsearch confirmation code'
                );
                break;
            case $event instanceof PhoneConfirmationRequested:
                $this->smsProvider->send(
                    $event->getPhone(),
                    sprintf('AdultSearch confirmation code: %s', $event->getConfirmationCode())
                );
                break;
            default:
                throw new \RuntimeException("Event not supported. " . get_class($event));
        }
    }
}
