<?php

declare(strict_types=1);

namespace App\Security\Domain;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\Email;

/**
 * @ORM\Entity(repositoryClass="App\Security\Infrastructure\Persistence\SessionDoctrineRepository")
 * @ORM\Table(name="account_session")
 */
class Session
{
    /**
     * @ORM\Column(name="token", type="string", length=32)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $token;

    /**
     * @ORM\Column(name="expires_at", type="timestamp", length=10)
     */
    private DateTimeImmutable $expiresAt;

    /**
     * @ORM\Column(name="account_id", type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\OneToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="account_id")
     */
    private Account $account;

    public function __construct(
        string $token,
        Account $account,
        DateTimeImmutable $expiresAt
    )
    {
        $this->token = $token;
        $this->accountId = $account->getId();
        $this->account = $account;
        $this->expiresAt = $expiresAt;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @throws \Exception
     */
    public function getExpiresAt(): DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    /**
     * @return string[]
     */
    public function getAccountRoles(): array
    {
        return $this->getAccount()->getRoles();
    }

    public function getAccountEmail(): Email
    {
        return $this->getAccount()->getEmail();
    }

    public function isAgency(): bool
    {
        return $this->getAccount()->isAgency();
    }

    private function getAccount(): Account
    {
        return $this->account;
    }
}
