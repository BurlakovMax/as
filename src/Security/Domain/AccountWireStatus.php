<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Core\DomainSupport\Enumerable;

final class AccountWireStatus extends Enumerable
{
    public static function unProcessed(): self
    {
        return self::createEnum(0);
    }

    public static function processed(): self
    {
        return self::createEnum(1);
    }
}
