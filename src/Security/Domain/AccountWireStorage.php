<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Core\Application\Search\SearchQuery;

interface AccountWireStorage
{
    /**
     * @return AccountWire[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function add(AccountWire $accountWire): void;
}
