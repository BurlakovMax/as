<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Core\DomainSupport\Enumerable;

final class ConfirmationAction extends Enumerable
{
    public static function emailRegistration(): self
    {
        return self::createEnum('emailRegistration');
    }

    public static function emailChange(): self
    {
        return self::createEnum('emailChange');
    }

    public static function passwordReset(): self
    {
        return self::createEnum('passwordReset');
    }

    public static function phoneNumber(): self
    {
        return self::createEnum('phoneNumber');
    }

    public static function codeRequested(): self
    {
        return self::createEnum('codeRequested');
    }
}
