<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Security\Application\AccountWireCreateCommand;
use Money\Money;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Security\Infrastructure\Persistence\AccountWireDoctrineRepository")
 * @ORM\Table(name="account_wire")
 */
class AccountWire
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $bankName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $image;

    /**
     * @ORM\Column(type="account_wire_status")
     */
    private AccountWireStatus $status;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp", length=11)
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="processed_stamp", type="timestamp", length=11, nullable=true)
     */
    private ?\DateTimeImmutable $processedAt;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $processedBy;

    public function __construct(
        AccountWireCreateCommand $command
    ) {
        $this->id = 0;
        $this->accountId = $command->getAccountId();
        $this->name = $command->getName();
        $this->amount = $command->getAmount();
        $this->bankName = $command->getBankName();
        $this->image = $command->getImage();
        $this->status = AccountWireStatus::unProcessed();
        $this->createdAt = new \DateTimeImmutable();
        $this->processedAt = null;
        $this->processedBy = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getBankName(): string
    {
        return $this->bankName;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getStatus(): AccountWireStatus
    {
        return $this->status;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getProcessedAt(): ?\DateTimeImmutable
    {
        return $this->processedAt;
    }

    public function getProcessedBy(): ?int
    {
        return $this->processedBy;
    }
}
