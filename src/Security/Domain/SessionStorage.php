<?php

declare(strict_types=1);

namespace App\Security\Domain;

interface SessionStorage
{
    public function findByToken(string $token): ?Session;

    public function add(Session $session): void;

    public function destroy(Session $session): void;

    public function forceLogout(int $accountId): void;
}
