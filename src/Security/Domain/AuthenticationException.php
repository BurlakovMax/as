<?php

declare(strict_types=1);

namespace App\Security\Domain;

use LazyLemurs\Exceptions\Exception;

abstract class AuthenticationException extends Exception
{
}
