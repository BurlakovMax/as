<?php

declare(strict_types=1);

namespace App\Security\Domain;

final class Authorizer
{
    /** @var SessionStorage */
    private $sessionStorage;

    public function __construct(SessionStorage $sessionStorage)
    {
        $this->sessionStorage = $sessionStorage;
    }

    public function authorize(string $token): ?Session
    {
        $session = $this->sessionStorage->findByToken($token);

        if ($session === null || $session->getExpiresAt() < new \DateTime()) {
            return null;
        }

        return $session;
    }
}
