<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Core\Application\Search\SearchQuery;

interface SpammerStorage
{
    /**
     * @return Spammer[]
     */
    public function getBySearchQuery(SearchQuery $searchQuery): array;

    public function countBySearchQuery(SearchQuery $searchQuery): int;

    public function get(int $id): ?Spammer;

    public function add(Spammer $spammer): void;

    public function remove(Spammer $spammer): void;
}
