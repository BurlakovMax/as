<?php

declare(strict_types=1);

namespace App\Security\Domain;

final class ConfirmationGenerator
{
    /** @var SecureTokenGenerator */
    private $idGenerator;

    /** @var ConfirmationCodeGenerator */
    private $codeGenerator;

    /** @var string */
    private $ttl;

    /** @var int */
    private $attempts;

    public function __construct(
        SecureTokenGenerator $idGenerator,
        ConfirmationCodeGenerator $codeGenerator,
        string $ttl,
        int $attempts
    ) {
        $this->idGenerator = $idGenerator;
        $this->codeGenerator = $codeGenerator;
        $this->ttl = $ttl;
        $this->attempts = $attempts;
    }

    /**
     * @throws \Exception
     */
    public function generate(ConfirmationAction $action, ?int $accountId, ?string $value = null): Confirmation
    {
        return new Confirmation(
            $this->idGenerator->generate(),
            $this->codeGenerator->generate(),
            $action,
            $accountId,
            $value,
            new \DateTimeImmutable($this->ttl),
            $this->attempts
        );
    }
}
