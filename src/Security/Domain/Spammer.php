<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Security\Application\SpammerCommand;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Security\Infrastructure\Persistence\SpammerDoctrineRepository")
 * @ORM\Table(name="admin_spammer")
 */
class Spammer
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="email", type="string", length=128, unique=true)
     */
    private ?string $email;

    /**
     * @ORM\Column(name="ip_address", type="string", length=50)
     */
    private ?string $ipAddress;

    /**
     * @ORM\Column(name="strict", type="boolean", nullable=true)
     */
    private ?bool $isStrict;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp")
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private ?int $createdBy;

    public function __construct(SpammerCommand $command)
    {
        $this->id = 0;

        $this->email = $command->getEmail() ?? $command->getEmailDomain();
        $this->ipAddress = $command->getIpAddress();
        $this->isStrict = $command->getIsStrict();
        $this->createdAt = $command->getCreatedAt() ?? null;
        $this->createdBy = $command->getCreatedBy() ?? null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function isStrict(): bool
    {
        return $this->isStrict !== null;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }
}
