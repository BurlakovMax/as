<?php

declare(strict_types=1);

namespace App\Security\Domain;

use LazyLemurs\Structures\Email;
use Ramsey\Uuid\UuidInterface;

interface EmailProvider
{
    /**
     * @throws \Throwable
     */
    public function sendSyncByRawValues(
        UuidInterface $messageId,
        Email $emailSender,
        Email $emailTo,
        string $body,
        string $subject,
        ?Email $replyTo = null
    ): void;
}