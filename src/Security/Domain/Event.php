<?php

declare(strict_types=1);

namespace App\Security\Domain;

use LazyLemurs\DomainEvents\DomainEvent;
use LazyLemurs\Structures\Email;

abstract class Event extends DomainEvent
{
    /** @var int */
    private $id;

    /** @var Email */
    private $email;

    /** @var string */
    private $confirmationCode;

    public function __construct(int $id, Email $email, string $confirmationCode)
    {
        parent::__construct();
        $this->id = $id;
        $this->email = $email;
        $this->confirmationCode = $confirmationCode;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getConfirmationCode(): string
    {
        return $this->confirmationCode;
    }
}