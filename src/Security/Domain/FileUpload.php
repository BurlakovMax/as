<?php

declare(strict_types=1);

namespace App\Security\Domain;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileUpload
{
    public function save(string $name, UploadedFile $file): string;
}