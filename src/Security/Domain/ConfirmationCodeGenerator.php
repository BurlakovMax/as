<?php

declare(strict_types=1);

namespace App\Security\Domain;

interface ConfirmationCodeGenerator
{
    /**
     * Generate confirmation code
     */
    public function generate(): string;
}
