<?php

declare(strict_types=1);

namespace App\Security\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Security\Infrastructure\Persistence\NotificationSettingsDoctrineRepository")
 * @ORM\Table(name="account_opt")
 */
class NotificationSettings
{
    /**
     * @ORM\Column(name="opt_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(name="x", type="string", length=20)
     */
    private string $notificationName;

    /**
     * @ORM\Column(name="opt", type="boolean")
     */
    private bool $isEnable;

    public function __construct(int $accountId, string $notificationName, bool $isEnable)
    {
        $this->id = 0;
        $this->accountId = $accountId;
        $this->notificationName = $notificationName;
        $this->isEnable = $isEnable;
    }

    public function update(bool $isEnable, string $notificationName): void
    {
        if ($this->getNotificationName() !== $notificationName) {
            return;
        }

        $this->isEnable = $isEnable;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getNotificationName(): string
    {
        return $this->notificationName;
    }

    public function isEnable(): bool
    {
        return $this->isEnable;
    }
}