<?php

declare(strict_types=1);

namespace App\Security\Domain;

interface SecureTokenGenerator
{
    /**
     * Generate secure token
     */
    public function generate(): string;
}
