<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Security\Application\AccountUpdateCommand;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\Email;

/**
 * @ORM\Entity(repositoryClass="App\Security\Infrastructure\Persistence\AccountDoctrineRepository")
 * @ORM\Table(name="account")
 */
class Account
{
    private const ROLE_USER = 'ROLE_USER';
    private const ROLE_ADVERTISER = 'ROLE_ADVERTISER';
    private const ROLE_AGENCY = 'ROLE_AGENCY';
    private const ROLE_WORKER = 'ROLE_WORKER';

    /**
     * @ORM\Column(name="account_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="email", type="email", length=128, unique=true)
     */
    private Email $email;

    /**
     * @ORM\Column(name="username", type="string", length=32, unique=true)
     */
    private string $username;

    /**
     * @var PasswordHash|null
     * @ORM\Embedded(class="App\Security\Domain\PasswordHash", columnPrefix=false)
     */
    private $passwordHash;

    /**
     * @ORM\Column(name="whitelisted", type="boolean", nullable=true)
     */
    private ?bool $isWhitelisted;

    /**
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    private ?bool $isApproved;

    /**
     * @ORM\Column(name="banned", type="boolean", nullable=true)
     */
    private ?bool $isBanned;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $banReason;

    /**
     * @ORM\Column(name="email_confirmed", type="boolean", nullable=true)
     */
    private ?bool $isEmailConfirmed;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $avatar;

    /**
     * @ORM\Column(name="account_level", type="integer", nullable=true)
     */
    private ?int $accountLevel;

    /**
     * @ORM\Column(name="worker", type="boolean", nullable=true)
     */
    private ?bool $worker;

    /**
     * @ORM\Column(name="advertiser", type="boolean", nullable=true)
     */
    private ?bool $advertiser;

    /**
     * @ORM\Column(name="agency", type="boolean", nullable=true)
     */
    private ?bool $agency;

    /**
     * @ORM\Column(name="ip_address", type="string", length=50)
     */
    private string $ipAddress;

    /**
     * @ORM\Column(name="created_stamp", type="timestamp")
     */
    private ?\DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(name="last_login_stamp", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $lastVisitedAt;

    /**
     * @ORM\Column(name="deleted", type="timestamp", nullable=true)
     */
    private ?\DateTimeImmutable $deletedOn;

    /**
     * @ORM\Column(name="repost", type="integer", nullable=true)
     */
    private ?int $topUps;

    /**
     * @ORM\Column(name="forum_post", type="integer")
     */
    private ?int $forumPostsCount;

    /**
     * @ORM\Column(name="notes", type="string", nullable=true)
     */
    private ?string $notes;

    /**
     * @ORM\Column(name="ads_limit", type="integer", nullable=true)
     */
    private ?int $escortsMaxNumber;

    /**
     * @throws \Exception
     */
    public function __construct(Email $email, string $username, PasswordHash $passwordHash, string $ipAddress)
    {
        $this->id = 0;

        $this->email = $email;
        $this->username = $username;
        $this->passwordHash = $passwordHash;
        $this->ipAddress = $ipAddress;

        $this->isWhitelisted = false;
        $this->isApproved = true;
        $this->isBanned = false;
        $this->banReason = null;
        $this->isEmailConfirmed = false;
        $this->accountLevel = 0;
        $this->avatar = null;
        $this->worker = false;
        $this->advertiser = false;
        $this->agency = false;
        $this->forumPostsCount = 0;

        $this->createdAt = new \DateTimeImmutable();
        $this->deletedOn = null;
        $this->lastVisitedAt = null;
        $this->topUps = null;
        $this->notes = null;
        $this->escortsMaxNumber = 2;
    }

    public function update(AccountUpdateCommand $command): void
    {
        if ('' !== $command->getPassword()) {
            $this->passwordHash = new PasswordHash($command->getPassword());
        }

        $this->notes = $command->getNotes();
        $this->worker = $command->isWorker();
        $this->agency = $command->isAgency();
        $this->advertiser = $command->isAdvertiser();
        $this->escortsMaxNumber = $command->getMaxEscortsNumber();
        $this->topUps =  $command->getTopUps();
    }

    public function ban(bool $isBanned, ?string $banReason): void
    {
        $this->isBanned = $isBanned;
        $this->banReason = $banReason;
    }

    public function whiteList(bool $isWhitelisted): void
    {
        $this->isWhitelisted = $isWhitelisted;
    }

    public function delete(?string $deleteReason): void
    {
        $this->deletedOn = new \DateTimeImmutable();
        $this->notes = $deleteReason;
        $this->isApproved = false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function hasPassword(): bool
    {
        return $this->passwordHash !== null;
    }

    public function getPasswordHash(): ?PasswordHash
    {
        return $this->passwordHash;
    }

    public function isWhitelisted(): ?bool
    {
        return $this->isWhitelisted;
    }

    public function isBanned(): bool
    {
        return $this->isBanned;
    }

    public function getBanReason(): ?string
    {
        return $this->banReason;
    }

    public function isEmailConfirmed(): bool
    {
        return $this->isEmailConfirmed;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getLastVisitedAt(): ?\DateTimeImmutable
    {
        return $this->lastVisitedAt;
    }

    public function getDeletedOn(): ?\DateTimeImmutable
    {
        return $this->deletedOn;
    }

    public function setEmailConfirmed(bool $value): void
    {
        $this->isEmailConfirmed = $value;
    }

    public function setPasswordHash(PasswordHash $value): void
    {
        $this->passwordHash = $value;
    }

    public function updateEmail(Email $email): void
    {
        $this->email = $email;
    }

    public function updateUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getTopUps(): ?int
    {
        return $this->topUps;
    }

    public function getAccountLevel(): ?int
    {
        return $this->accountLevel;
    }

    public function addTopUps(int $topUps): void
    {
        if (null === $this->topUps) {
            $this->topUps = $topUps;
            return;
        }

        $this->topUps += $topUps;
    }

    /**
     * @throws NotEnoughTopUps
     */
    public function removeTopUps(int $topUps): void
    {
        if ($this->topUps < $topUps) {
            throw new NotEnoughTopUps();
        }

        $this->topUps -= $topUps;
    }

    public function isAgency(): bool
    {
        return in_array(self::ROLE_AGENCY, $this->getRoles());
    }

    public function isWorker(): bool
    {
        return in_array(self::ROLE_WORKER, $this->getRoles());
    }

    public function isAdvertiser(): bool
    {
        return in_array(self::ROLE_ADVERTISER, $this->getRoles());
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        $roles = [
            self::ROLE_USER,
        ];

        if ($this->advertiser) {
            $roles[] = self::ROLE_ADVERTISER;
        }

        if ($this->agency) {
            $roles[] = self::ROLE_AGENCY;
        }

        if ($this->worker) {
            $roles[] = self::ROLE_WORKER;
        }

        return $roles;
    }

    public function forumPostsCount(): int
    {
        return $this->forumPostsCount;
    }

    public function setAvatar(string $image): void
    {
        $this->avatar = $image;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function escortsMaxNumber(): ?int
    {
        return $this->escortsMaxNumber;
    }
}
