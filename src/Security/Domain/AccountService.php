<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Security\Application\AccountUpdateCommand;
use LazyLemurs\Structures\Email;

final class AccountService
{
    private AccountStorage $accountStorage;

    private SessionStorage $sessionStorage;

    private ConfirmationStorage $confirmationStorage;

    private ConfirmationGenerator $confirmationGenerator;

    public function __construct(
        AccountStorage $accountStorage,
        ConfirmationStorage $confirmationStorage,
        ConfirmationGenerator $confirmationGenerator,
        SessionStorage $sessionStorage
    ) {
        $this->accountStorage = $accountStorage;
        $this->confirmationStorage = $confirmationStorage;
        $this->confirmationGenerator = $confirmationGenerator;
        $this->sessionStorage = $sessionStorage;
    }

    /**
     * @throws UsernameTaken
     * @throws EmailTaken
     * @throws BadPassword
     * @throws \Exception
     */
    public function register(Email $email, string $username, string $password, string $ipAddress): RegistrationRequested
    {
        if ($this->accountStorage->findByUsername($username)) {
            throw new UsernameTaken();
        }

        if ($this->accountStorage->findByEmail($email)) {
            throw new EmailTaken();
        }

        $account = new Account(
            $email,
            $username,
            new PasswordHash($password),
            $ipAddress
        );
        $this->accountStorage->add($account);

        $confirmation = $this->confirmationGenerator->generate(
            ConfirmationAction::emailRegistration(),
            $account->getId()
        );
        $this->confirmationStorage->add($confirmation);

        return new RegistrationRequested(
            $account->getId(),
            $account->getEmail(),
            $confirmation->getCode(),
            $confirmation->getId()
        );
    }

    public function update(AccountUpdateCommand $command): void
    {
        $account = $this->accountStorage->getAndLock($command->getId());

        $account->update($command);
    }

    /**
     * @throws InvalidConfirmation
     * @throws InvalidConfirmationCode
     * @throws \Exception
     */
    public function confirmRegistration(string $confirmationId, string $confirmationCode): void
    {
        $confirmation = $this->getConfirmation(
            $confirmationId,
            $confirmationCode,
            ConfirmationAction::emailRegistration()
        );

        $account = $this->accountStorage->getAndLock($confirmation->getAccountId());

        if (!$account || $account->isBanned() || $account->isEmailConfirmed()) {
            throw new \Exception('Invalid user for confirmation');
        }

        $account->setEmailConfirmed(true);

        $this->confirmationStorage->delete($confirmation);
    }

    /**
     * @throws AccountNotFound
     * @throws \Exception
     */
    public function requestPasswordReset(Email $email): PasswordResetRequested
    {
        $account = $this->accountStorage->findByEmail($email);

        if (!$account) {
            throw new AccountNotFound();
        }

        $confirmation = $this->confirmationGenerator->generate(
            ConfirmationAction::passwordReset(),
            $account->getId()
        );
        $this->confirmationStorage->add($confirmation);

        return new PasswordResetRequested(
            $account->getId(),
            $account->getEmail(),
            $confirmation->getCode(),
            $confirmation->getId()
        );
    }

    /**
     * @throws InvalidConfirmation
     * @throws InvalidConfirmationCode
     * @throws \Exception
     */
    public function confirmPasswordReset(string $confirmationId, string $confirmationCode, string $password): void
    {
        $confirmation = $this->getConfirmation(
            $confirmationId,
            $confirmationCode,
            ConfirmationAction::passwordReset()
        );

        $account = $this->accountStorage->getAndLock($confirmation->getAccountId());
        $this->confirmationStorage->delete($confirmation);

        if (!$account || $account->isBanned()) {
            throw new \Exception('Invalid user for confirmation');
        }

        $account->setPasswordHash(new PasswordHash($password));
    }

    public function utilizeConfirmationAttempt(string $confirmationId): void
    {
        $this->confirmationStorage->utilizeAttempt($confirmationId);
    }

    /**
     * @throws AccountNotFound
     * @throws UsernameTaken
     */
    public function updateUsername(int $accountId, ?string $username): void
    {
        $account = $this->getAndLock($accountId);

        $usernameTaken = $this->accountStorage->findByUsername($username);

        if (null !== $usernameTaken) {
            throw new UsernameTaken();
        }

        $account->updateUsername($username);
    }

    /**
     * @throws AccountNotFound
     * @throws EmailTaken
     */
    public function changeEmail(int $accountId, Email $email): ChangeEmailRequested
    {
        $account = $this->getAndLock($accountId);

        if ($this->accountStorage->findByEmail($email)) {
            throw new EmailTaken();
        }

        $account->updateEmail($email);
        $account->setEmailConfirmed(false);

        $confirmation = $this->confirmationGenerator->generate(
            ConfirmationAction::emailChange(),
            $account->getId()
        );

        $this->confirmationStorage->add($confirmation);

        return new ChangeEmailRequested(
            $account->getId(),
            $account->getEmail(),
            $confirmation->getCode(),
            $confirmation->getId()
        );
    }

    /**
     * @throws AccountNotFound
     */
    public function requestConfirmationCode(Email $email, int $accountId): ConfirmationCodeRequested
    {
        $account = $this->get($accountId);

        $confirmation = $this->confirmationGenerator->generate(
            ConfirmationAction::emailChange(),
            $account->getId()
        );

        $this->confirmationStorage->add($confirmation);

        return new ConfirmationCodeRequested(
            $account->getId(),
            $email,
            $confirmation->getCode(),
            $confirmation->getId()
        );
    }

    /**
     * @throws AccountNotFound
     * @throws InvalidConfirmation
     * @throws InvalidConfirmationCode
     */
    public function setEmailConfirmed(int $accountId, string $confirmationId, string $confirmationCode): void
    {
        $confirmation = $this->getConfirmation(
            $confirmationId,
            $confirmationCode,
            ConfirmationAction::emailChange()
        );
        $this->confirmationStorage->delete($confirmation);

        $this->getAndLock($accountId)->setEmailConfirmed(true);
    }

    /**
     * @throws AccountNotFound
     * @throws BadPassword
     * @throws InvalidCredentials
     */
    public function updatePassword(string $password, int $userId, string $oldPassword): void
    {
        $account = $this->getAndLock($userId);

        if (!$account->hasPassword() || !$account->getPasswordHash()->isValid($oldPassword)) {
            throw new InvalidCredentials();
        }

        $account->setPasswordHash(new PasswordHash($password));
    }

    /**
     * @throws AccountNotFound
     */
    public function addTopUps(int $accountId, int $topUps): void
    {
        $this->getAndLock($accountId)->addTopUps($topUps);
    }

    /**
     * @throws AccountNotFound
     * @throws NotEnoughTopUps
     */
    public function removeTopUps(int $accountId, int $topUps): void
    {
        $this->getAndLock($accountId)->removeTopUps($topUps);
    }

    /**
     * @throws AccountNotFound
     */
    public function setAvatar(int $accountId, string $image): void
    {
        $this->getAndLock($accountId)->setAvatar($image);
    }

    /**
     * @throws AccountNotFound
     */
    public function ban(int $accountId, bool $isBanned, ?string $banReason): void
    {
        $this->getAndLock($accountId)->ban($isBanned, $banReason);
    }

    public function whitelist(int $accountId, bool $isWhitelisted): void
    {
        $this->getAndLock($accountId)->whiteList($isWhitelisted);
    }

    public function delete(int $accountId, ?string $deleteReason): void
    {
        $this->getAndLock($accountId)->delete($deleteReason);
    }

    public function forceLogout(int $accountId): void
    {
        $this->sessionStorage->forceLogout($accountId);
    }

    /**
     * @throws AccountNotFound
     */
    private function get(int $userId): Account
    {
        $account = $this->accountStorage->findById($userId);

        if (!$account) {
            throw new AccountNotFound();
        }

        return $account;
    }

    /**
     * @throws AccountNotFound
     */
    private function getAndLock(int $accountId): Account
    {
        $account = $this->accountStorage->getAndLock($accountId);

        if (!$account) {
            throw new AccountNotFound();
        }

        return $account;
    }

    /**
     * @throws InvalidConfirmation
     * @throws InvalidConfirmationCode
     * @throws \Exception
     */
    private function getConfirmation(
        string $confirmationId,
        string $confirmationCode,
        ConfirmationAction $action
    ): Confirmation
    {
        $confirmation = $this->confirmationStorage->get($confirmationId);

        if (
            $confirmation === null
            || $confirmation->isExpired()
            || $confirmation->getAttemptsLeft() < 1
            || $confirmation->getAction() !== $action
        ) {
            throw new InvalidConfirmation();
        }

        if ($confirmation->getCode() !== $confirmationCode) {
            throw new InvalidConfirmationCode();
        }

        return $confirmation;
    }
}
