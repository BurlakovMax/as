<?php

declare(strict_types=1);

namespace App\Security\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Security\Infrastructure\Persistence\ConfirmationDoctrineRepository")
 * @ORM\Table(name="account_confirmation")
 */
class Confirmation
{
    /**
     * @ORM\Column(name="id", type="string", length=32)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private string $code;

    /**
     * @ORM\Column(type="confirmation_action")
     */
    private ConfirmationAction $action;

    /**
     * @ORM\Column(type="timestamp", length=10)
     */
    private \DateTimeImmutable $expiresAt;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $attemptsLeft;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     */
    private ?int $accountId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $value;

    public function __construct(
        string $id,
        string $code,
        ConfirmationAction $action,
        ?int $accountId,
        ?string $value,
        \DateTimeImmutable $expiresAt,
        int $attemptsLeft
    ) {
        $this->id = $id;
        $this->code = $code;
        $this->action = $action;
        $this->accountId = $accountId;
        $this->value = $value;
        $this->expiresAt = $expiresAt;
        $this->attemptsLeft = $attemptsLeft;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getAction(): ConfirmationAction
    {
        return $this->action;
    }

    /**
     * @throws \Exception
     */
    public function getExpiresAt(): \DateTimeImmutable
    {
        return $this->expiresAt;
    }

    /**
     * @throws \Exception
     */
    public function isExpired(): bool
    {
        return $this->getExpiresAt() < new \DateTimeImmutable();
    }

    public function getAttemptsLeft(): int
    {
        return $this->attemptsLeft;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }
}
