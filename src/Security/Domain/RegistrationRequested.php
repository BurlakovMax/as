<?php

declare(strict_types=1);

namespace App\Security\Domain;

use LazyLemurs\Structures\Email;

final class RegistrationRequested extends Event
{
    private string $confirmationId;

    public function __construct(int $id, Email $email, string $confirmationCode, string $confirmationId)
    {
        parent::__construct($id, $email, $confirmationCode);

        $this->confirmationId = $confirmationId;
    }

    public function getConfirmationId(): string
    {
        return $this->confirmationId;
    }
}
