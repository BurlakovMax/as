<?php

declare(strict_types=1);

namespace App\Security\Domain;

use LazyLemurs\Exceptions\Exception;

final class BadPassword extends Exception
{
}
