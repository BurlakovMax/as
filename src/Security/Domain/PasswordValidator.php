<?php

declare(strict_types=1);

namespace App\Security\Domain;

//use App\Security\Application\PasswordRestrictionsData;

final class PasswordValidator
{
    private const
        NOT_ALLOWED = -1,
        ALLOWED = 0,

        TOTAL = 6,
        TOTAL_MAX = 4096,

        CYRILLIC = self::NOT_ALLOWED,
        LATIN = 1,
        DIGIT = 1,
        SPECIAL = self::ALLOWED,
        UPPER_CASE = self::ALLOWED,
        LOWER_CASE = self::ALLOWED,

        DIGIT_LIST_EXP = '0-9',
        SPECIAL_LIST_EXP = "\'\/~`\!\@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:\"\<\>,\.\?",
        LATIN_UPPER_LIST_EXP = 'A-Z',
        CYRILLIC_UPPER_LIST_EXP = 'А-ЯЁ',
        LATIN_LOWER_LIST_EXP = 'a-z',
        CYRILLIC_LOWER_LIST_EXP = 'а-яё';


//    public static function getRestrictionsData(): PasswordRestrictionsData
//    {
//        return new PasswordRestrictionsData(
//            self::TOTAL,
//            self::CYRILLIC,
//            self::LATIN,
//            self::UPPER_CASE,
//            self::LOWER_CASE,
//            self::DIGIT,
//            self::SPECIAL
//        );
//    }

    /**
     * @throws BadPassword
     */
    public static function validatePassword(string $password): void
    {
        self::validateTotal($password);
        self::validate($password, 'cyrillic', self::CYRILLIC, self::getCyrillicList());
        self::validate($password, 'latin', self::LATIN, self::getLatinList());
        self::validate($password, 'digit', self::DIGIT, self::DIGIT_LIST_EXP);
        self::validate($password, 'special', self::SPECIAL,self::SPECIAL_LIST_EXP);
        self::validate($password, 'upper', self::UPPER_CASE, self::getUpperCaseList());
        self::validate($password, 'lower', self::LOWER_CASE, self::getLowerCaseList());
        self::validateOnlyAllowedList($password, self::getAllAllowedList());
    }

    /**
     * @throws BadPassword
     */
    private static function validateOnlyAllowedList($password, $allowedList): void
    {
        if (!preg_match("/^[$allowedList]+$/", $password)) {
            throw new BadPassword('Password contains not allowed symbols');
        }
    }

    /**
     * @throws BadPassword
     */
    private static function validateTotal(string $password): void
    {
        $passwordLength = strlen($password);
        if ($passwordLength < self::TOTAL || $passwordLength > self::TOTAL_MAX) {
            throw new BadPassword(
                sprintf('Password length must be at interval [%d, %d].',
                    self::TOTAL, self::TOTAL_MAX)
            );
        }
    }

    /**
     * @throws BadPassword
     */
    private static function validate(string $password, string $type, int $count, string $list): void
    {
        switch ($count) {
            case self::NOT_ALLOWED:
                if (preg_match("@[$list]@", $password)) {
                    throw new BadPassword(sprintf('Password must not contains %s characters', $type));
                }
                return;
            case self::ALLOWED:
                return;
            default:
                if (!preg_match_all("@[$list]@", $password, $matches) || count($matches[0]) < $count) {
                    throw new BadPassword(sprintf('Password must contains at least %s %s characters, now it contains %s', $count, $type, count($matches[0])));
                }
        }
    }

    private static function getAllAllowedList(): string
    {
        $list = '';
        $list .= self::getLowerCaseList();
        $list .= self::getUpperCaseList();
        if (self::DIGIT !== self::NOT_ALLOWED) {
            $list .= self::DIGIT_LIST_EXP;
        }
        if (self::SPECIAL !== self::NOT_ALLOWED) {
            $list .= self::SPECIAL_LIST_EXP;
        }

        return $list;
    }

    private static function getUpperCaseList(): string
    {
        $list = '';
        if (self::CYRILLIC > 0) {
            $list .= self::CYRILLIC_UPPER_LIST_EXP;
        }
        if (self::LATIN > 0) {
            $list .= self::LATIN_UPPER_LIST_EXP;
        }

        return $list;
    }

    private static function getLowerCaseList(): string
    {
        $list = '';
        if (self::CYRILLIC > 0) {
            $list .= self::CYRILLIC_LOWER_LIST_EXP;
        }
        if (self::LATIN > 0) {
            $list .= self::LATIN_LOWER_LIST_EXP;
        }

        return $list;
    }

    private static function getLatinList(): string
    {
        $list = '';
        if (self::UPPER_CASE !== self::NOT_ALLOWED) {
            $list .= self::LATIN_UPPER_LIST_EXP;
        }
        if (self::LOWER_CASE !== self::NOT_ALLOWED) {
            $list .= self::LATIN_LOWER_LIST_EXP;
        }

        return $list;
    }

    private static function getCyrillicList(): string
    {
        $list = '';
        if (self::UPPER_CASE !== self::NOT_ALLOWED) {
            $list .= self::CYRILLIC_UPPER_LIST_EXP;
        }
        if (self::LOWER_CASE !== self::NOT_ALLOWED) {
            $list .= self::CYRILLIC_LOWER_LIST_EXP;
        }

        return $list;
    }
}
