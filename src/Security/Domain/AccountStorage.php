<?php

declare(strict_types=1);

namespace App\Security\Domain;

use App\Core\Application\Search\SearchQuery;
use LazyLemurs\Structures\Email;

interface AccountStorage
{
    public function findById(int $id): ?Account;

    /**
     * @return Account[]
     */
    public function getBySearchQuery(SearchQuery $query): array;

    public function countBySearchQuery(SearchQuery $query): int;

    public function findByEmail(Email $email): ?Account;

    public function findByUsername(string $username): ?Account;

    public function add(Account $account): void;

    /**
     * @return Account[]
     */
    public function getAllByIds(array $accountIds): array;

    public function getAndLock(int $accountId): ?Account;
}
