<?php

declare(strict_types=1);

namespace App\Security\Domain;

use LazyLemurs\Structures\Email;

final class Authenticator
{
    private AccountStorage $accountStorage;

    private SessionStorage $sessionStorage;

    private SecureTokenGenerator $tokenGenerator;

    private string $tokenTtl;

    public function __construct(
        AccountStorage $accountStorage,
        SessionStorage $sessionStorage,
        SecureTokenGenerator $tokenGenerator,
        string $tokenTtl
    )
    {
        $this->accountStorage = $accountStorage;
        $this->sessionStorage = $sessionStorage;
        $this->tokenGenerator = $tokenGenerator;
        $this->tokenTtl = $tokenTtl;
    }

    /**
     * @throws AccountSuspended
     * @throws InvalidCredentials
     */
    public function authenticateAccount(Email $email, string $password): Account
    {
        $account = $this->accountStorage->findByEmail($email);

        if (!$account) {
            throw new InvalidCredentials();
        }

        if (!$account->hasPassword() || !$account->getPasswordHash()->isValid($password)) {
            throw new InvalidCredentials();
        }

        if ($account->isBanned()) {
            throw new AccountSuspended();
        }

        return $account;
    }

    /**
     * @throws AccountSuspended
     * @throws InvalidCredentials
     * @throws \Exception
     */
    public function authenticateAccountWithToken(Email $email, string $password): string
    {
        $session = $this->createSession($this->authenticateAccount($email, $password));

        $this->sessionStorage->add($session);

        return $session->getToken();
    }

    public function invalidateToken(string $token): void
    {
        $session = $this->sessionStorage->findByToken($token);

        if ($session === null) {
            return;
        }

        $this->sessionStorage->destroy($session);
    }

    /**
     * @throws \Exception
     */
    private function createSession(Account $account): Session
    {
        $token = $this->tokenGenerator->generate();
        $expiresAt = new \DateTimeImmutable($this->tokenTtl);
        return new Session($token, $account, $expiresAt);
    }
}
