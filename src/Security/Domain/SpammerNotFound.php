<?php

declare(strict_types=1);

namespace App\Security\Domain;

use LazyLemurs\Exceptions\NotFoundException;

final class SpammerNotFound extends NotFoundException
{
}
