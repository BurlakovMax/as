<?php

declare(strict_types=1);

namespace App\Security\Domain;

interface NotificationSettingsStorage
{
    public function add(NotificationSettings $notificationSettings): void;

    /**
     * @return NotificationSettings[]
     */
    public function findByAccountIdAndLock(int $accountId): array;

    /**
     * @return NotificationSettings[]
     */
    public function findByAccountId(int $accountId): array;
}