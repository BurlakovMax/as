<?php

declare(strict_types=1);

namespace App\Security\Domain;

interface ConfirmationStorage
{
    public function get(string $id): ?Confirmation;

    public function utilizeAttempt(string $id): void;

    public function add(Confirmation $confirmation): void;

    public function delete(Confirmation $confirmation): void;
}
