<?php

declare(strict_types=1);

namespace App\Security\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class PasswordHash
{
    /** @var int Hashing algorithm */
    private const ALGORITHM = \PASSWORD_ARGON2I;

    /** @var <string, int> Hashing options */
    private const OPTIONS = [
        'memory_cost' => 1024,
        'time_cost' => 2,
        'threads' => 2,
    ];

    /**
     * @ORM\Column(name="password", type="string", length=100, nullable=true)
     */
    private string $passwordHash;

    /**
     * @throws BadPassword
     */
    public function __construct(string $password)
    {
        PasswordValidator::validatePassword($password);
        $this->passwordHash = $this->encode($password);
    }

    public function isValid(string $password): bool
    {
        return password_verify($password, $this->passwordHash);
    }

    private function encode(string $password): string
    {
        return password_hash($password, self::ALGORITHM, self::OPTIONS);
    }
}
