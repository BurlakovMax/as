<?php

declare(strict_types=1);

namespace App\Security\Domain;

final class Unauthorized extends AuthenticationException
{
    public function __construct($message = '', $code = 401, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
