<?php

declare(strict_types=1);

namespace App\Security\Domain;

final class InvalidCredentials extends AuthenticationException
{
}
