<?php

declare(strict_types=1);

namespace App\Attributes\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="attribute")
 */
class Attribute
{
    /**
     * @var int
     *
     * @ORM\Column(name="attribute_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=60)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="datatype", type="string", length=10)
     */
    private $dataType;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $options;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", length=4, nullable=true)
     */
    private $facet;

    public function isTextType(): bool
    {
        return $this->dataType === 'text' || $this->dataType === 'cover';
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDataType(): string
    {
        return $this->dataType;
    }

    public function getOptions(): ?string
    {
        return $this->options;
    }

    public function getFacet(): ?int
    {
        return $this->facet;
    }
}