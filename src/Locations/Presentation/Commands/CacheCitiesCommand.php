<?php

declare(strict_types=1);

namespace App\Locations\Presentation\Commands;

use App\Core\Application\CacheProcessor;
use App\Locations\Application\CityQueryProcessor;
use LazyLemurs\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CacheCitiesCommand extends Command
{
    private CityQueryProcessor $queryProcessor;
    private CacheProcessor $cacheProcessor;

    public function __construct(CityQueryProcessor $queryProcessor, CacheProcessor $cacheProcessor)
    {
        parent::__construct('cache:cities');

        $this->queryProcessor = $queryProcessor;
        $this->cacheProcessor = $cacheProcessor;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Add all cities to cache')
            ->setHelp('This command add all cities to Cache')
        ;
    }

    protected function doExecute(InputInterface $input, OutputInterface $output): void
    {
        $this->cacheProcessor->writeToCache('locations.cities', $this->queryProcessor->getAllCities());
    }
}
