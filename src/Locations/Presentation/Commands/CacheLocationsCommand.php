<?php

declare(strict_types=1);

namespace App\Locations\Presentation\Commands;

use App\Core\Application\CacheProcessor;
use App\Locations\Application\LocationTreeBuilder;
use LazyLemurs\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CacheLocationsCommand extends Command
{
    private LocationTreeBuilder $service;
    private CacheProcessor $cacheProcessor;

    public function __construct(
        LocationTreeBuilder $service,
        CacheProcessor $cacheProcessor
    ) {
        parent::__construct('cache:locations');

        $this->service = $service;
        $this->cacheProcessor = $cacheProcessor;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Add locations hierarchy to cache')
            ->setHelp('This command add all locations to Cache')
        ;
    }

    protected function doExecute(InputInterface $input, OutputInterface $output): void
    {
        $this->cacheProcessor->writeToCache('locations.tree', $this->service->getLocationTree());
    }
}
