<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\City;

final class TreeCityData
{
    private int $id;

    private string $name;

    private string $url;

    private ?bool $isPopular;

    public function __construct(City $city)
    {
        $this->id = $city->getId();
        $this->name = $city->getName();
        $this->url = $city->getUrl();
        $this->isPopular = $city->getIsPopular();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getIsPopular(): ?bool
    {
        return $this->isPopular;
    }
}