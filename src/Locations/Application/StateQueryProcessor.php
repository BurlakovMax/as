<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Core\Application\Search\SearchQuery;
use App\Locations\Domain\State;
use App\Locations\Domain\StateReadStorage;

final class StateQueryProcessor
{
    private StateReadStorage $stateReadStorage;

    public function __construct(StateReadStorage $stateReadStorage)
    {
        $this->stateReadStorage = $stateReadStorage;
    }

    /**
     * @throws StateNotFound
     */
    public function getById(int $id): StateData
    {
        return $this->mapToData(
            $this->get($id)
        );
    }

    /**
     * @throws StateNotFound
     */
    public function getByUrl(string $url): StateData
    {
        $state = $this->stateReadStorage->getByUrl($url);

        if (!$state) {
            throw new StateNotFound();
        }

        return $this->mapToData($state);
    }

    /**
     * @return StateData[]
     * @throws StateBadRequest
     */
    public function getListByFilterQuery(SearchQuery $query): array
    {
        foreach ($query->getOrderQueries() as $orderQuery) {
            if (!in_array($orderQuery->getProperty(), $this->getAllowedOrderProperties(), true)) {
                throw new StateBadRequest(sprintf('Invalid ordering property "%s"', $orderQuery->getProperty()));
            }
        }

        return array_map(
            [
                $this,
                'mapToData'
            ],
            $this->stateReadStorage->getListByFilterQuery($query)
        );
    }

    public function countByFilterQuery(SearchQuery $query): int
    {
        return $this->stateReadStorage->countByFilterQuery($query);
    }

    /**
     * @return TreeStateData[]
     */
    public function getTreeListByCountryId(int $countryId): array
    {
        return array_map(
            [
                $this,
                'mapToTreeData'
            ],
            $this->stateReadStorage->getListByCountryId($countryId)
        );
    }

    /**
     * @throws StateNotFound
     */
    private function get(int $id): State
    {
        $state = $this->stateReadStorage->get($id);

        if (!$state) {
            throw new StateNotFound();
        }

        return $state;
    }

    private function mapToData(State $state): StateData
    {
        return new StateData($state);
    }

    private function mapToTreeData(State $state): TreeStateData
    {
        return new TreeStateData($state);
    }

    /**
     * @return string[]
     */
    private function getAllowedOrderProperties(): array
    {
        return [
            't.id',
            't.name',
        ];
    }
}
