<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\TopCityReadStorage;

final class TopCityQueryProcessor
{
    private TopCityReadStorage $topCityReadStorage;

    public function __construct(TopCityReadStorage $topCityReadStorage)
    {
        $this->topCityReadStorage = $topCityReadStorage;
    }

    public function isBigCity(int $id): bool
    {
        $topCity = $this->topCityReadStorage->get($id);

        if (null === $topCity) {
            return false;
        }

        return $topCity->isBigCity();
    }
}