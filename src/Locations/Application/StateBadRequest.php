<?php

declare(strict_types=1);

namespace App\Locations\Application;

use LazyLemurs\Exceptions\Exception;

final class StateBadRequest extends Exception
{

}
