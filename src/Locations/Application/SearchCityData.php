<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\City;

final class SearchCityData
{
    private int $id;

    private string $name;

    private string $url;

    private ?string $stateName;

    private string $countryName;

    public function __construct(City $city)
    {
        $this->id = $city->getId();
        $this->name = $city->getName();
        $this->url = $city->getUrl();
        $this->stateName = $city->getStateName();
        $this->countryName = $city->getCountryName();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getStateName(): ?string
    {
        return $this->stateName;
    }

    public function getCountryName(): string
    {
        return $this->countryName;
    }
}