<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Core\Application\CacheProcessor;

final class LocationTreeBuilder
{
    private CountryQueryProcessor $countryQueryProcessor;

    private StateQueryProcessor $stateQueryProcessor;

    private CityQueryProcessor $cityQueryProcessor;

    private CacheProcessor $cache;

    public function __construct(
        CountryQueryProcessor $countryQueryProcessor,
        StateQueryProcessor $stateQueryProcessor,
        CityQueryProcessor $cityQueryProcessor,
        CacheProcessor $cache
    ) {
        $this->countryQueryProcessor = $countryQueryProcessor;
        $this->stateQueryProcessor = $stateQueryProcessor;
        $this->cityQueryProcessor = $cityQueryProcessor;
        $this->cache = $cache;
    }

    /**
     * @return TreeCountryData[]
     * @param bool $useCache
     */
    public function getLocationTree(bool $useCache = false): array
    {
        if ($useCache) {
          $locations = $this->cache->readFromCache('locations.tree');
          if (null !== $locations) {
            return $locations;
          }
        }

        $worldParts = $this->countryQueryProcessor->getTreeWorldParts();
        $worldParts = $this->getSubCountries($worldParts);
        return $this->getStates($worldParts);
    }

    /**
     * @param TreeCountryData[] $countries
     * @return TreeCountryData[]
     */
    private function getSubCountries(array $countries): array
    {
        $result = [];
        foreach ($countries as $country) {
            $country->setSubCountries($this->getCountryCities($this->countryQueryProcessor->getTreeListByParentId($country->getId())));
            $result[] = $country;
        }
        return $result;
    }

    /**
     * @param TreeCountryData[] $countries
     * @return TreeCountryData[]
     */
    private function getStates(array $countries): array
    {
        $result = [];
        foreach ($countries as $country) {
            if ($country->getHasStates()) {
                $country->setStates($this->getStateCities($this->stateQueryProcessor->getTreeListByCountryId($country->getId())));
            }
            $result[] = $country;
        }
        return $result;
    }

    /**
     * @param TreeCountryData[] $countries
     * @return TreeCountryData[]
     */
    private function getCountryCities(array $countries): array
    {
        $result = [];
        foreach ($countries as $country) {
            $country->setCities($this->cityQueryProcessor->getTreeListbyCountryId($country->getId()));
            $result[] = $country;
        }
        return $result;
    }

    /**
     * @param TreeStateData[] $states
     * @return TreeStateData[]
     */
    private function getStateCities(array $states): array
    {
        $result = [];
        foreach ($states as $state) {
            $state->setCities($this->cityQueryProcessor->getTreeListbyStateId($state->getId()));
            $result[] = $state;
        }
        return $result;
    }
}
