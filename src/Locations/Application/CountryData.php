<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\Country;
use Swagger\Annotations as SWG;

final class CountryData
{
    private const TYPE = 'country';
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private ?string $subdomain;

    /**
     * @SWG\Property()
     */
    private ?bool $active;

    /**
     * @SWG\Property()
     */
    private bool $hasStates;

    /**
     * @SWG\Property()
     */
    private int $type;

    /**
     * @SWG\Property()
     */
    private string $code;

    public function __construct(Country $country)
    {
        $this->id = $country->getId();
        $this->name = $country->getName();
        $this->subdomain = $country->getSubdomain();
        $this->active = $country->getActive();
        $this->hasStates = $country->hasStates();
        $this->type = (int) $country->getType()->getRawValue();
        $this->code = $country->getCode();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSubdomain(): ?string
    {
        return $this->subdomain;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function isHasStates(): bool
    {
        return $this->hasStates;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTypeString(): string
    {
        return self::TYPE;
    }
}
