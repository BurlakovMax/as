<?php

declare(strict_types=1);

namespace App\Locations\Application;

use LazyLemurs\Exceptions\NotFoundException;

final class StateNotFound extends NotFoundException
{

}
