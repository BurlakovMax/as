<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Core\Application\Search\SearchQuery;
use App\Locations\Domain\Country;
use App\Locations\Domain\CountryReadStorage;

final class CountryQueryProcessor
{
    private CountryReadStorage $countryReadStorage;

    public function __construct(CountryReadStorage $countryReadStorage)
    {
        $this->countryReadStorage = $countryReadStorage;
    }

    /**
     * @throws CountryNotFound
     */
    public function getById(int $id): CountryData
    {
        return $this->mapToData(
            $this->get($id)
        );
    }

    /**
     * @return CountryData[]
     * @throws CountryBadRequest
     */
    public function getListByFilterQuery(SearchQuery $query): array
    {
        foreach ($query->getOrderQueries() as $orderQuery) {
            if (!in_array($orderQuery->getProperty(), $this->getAllowedOrderProperties(), true)) {
                throw new CountryBadRequest(sprintf('Invalid ordering property "%s"', $orderQuery->getProperty()));
            }
        }

        return array_map(
            [
                $this,
                'mapToData'
            ],
            $this->countryReadStorage->getListByFilterQuery($query)
        );
    }

    public function countByFilterQuery(SearchQuery $query): int
    {
        return $this->countryReadStorage->countByFilterQuery($query);
    }

    /**
     * @return TreeCountryData[]
     */
    public function getTreeWorldParts(): array
    {
        return array_map(
            [
                $this,
                'mapToTreeData'
            ],
            $this->countryReadStorage->getListByLevel(0)
        );
    }

    /**
     * @throws CountryNotFound
     */
    private function get(int $id): Country
    {
        $country = $this->countryReadStorage->get($id);

        if (!$country) {
            throw new CountryNotFound();
        }

        return $country;
    }

    private function mapToData(Country $country): CountryData
    {
        return new CountryData($country);
    }

    private function mapToTreeData(Country $country): TreeCountryData
    {
        return new TreeCountryData($country);
    }

    /**
     * @return string[]
     */
    private function getAllowedOrderProperties(): array
    {
        return [
            't.id',
            't.name',
        ];
    }

    /**
     * @return TreeCountryData[]
     */
    public function getTreeListByParentId(int $parentId): array
    {
        return array_map(
            [
                $this,
                'mapToTreeData'
            ],
            $this->countryReadStorage->getListByParentId($parentId)
        );
    }
}
