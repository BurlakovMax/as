<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\State;
use Swagger\Annotations as SWG;

final class StateData
{
    private const TYPE = 'state';
    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private string $url;

    /**
     * @SWG\Property()
     */
    private int $countryId;

    /**
     * @SWG\Property()
     */
    private ?bool $active;

    /**
     * @SWG\Property()
     */
    private string $code;

    public function __construct(State $state)
    {
        $this->id = $state->getId();
        $this->name = $state->getName();
        $this->url = $state->getUrl();
        $this->countryId = $state->getCountryId();
        $this->active = false;
        $this->code = $state->getCode();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTypeString(): string
    {
        return self::TYPE;
    }
}
