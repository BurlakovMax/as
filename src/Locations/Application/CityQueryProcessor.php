<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Core\Application\Search\SearchQuery;
use App\Locations\Domain\City;
use App\Locations\Domain\CityReadStorage;

final class CityQueryProcessor
{
    /** @var CityReadStorage */
    private $cityReadStorage;

    public function __construct(CityReadStorage $cityReadStorage)
    {
        $this->cityReadStorage = $cityReadStorage;
    }

    /**
     * @throws CityNotFound
     */
    public function getById(int $id): CityData
    {
        return $this->mapToData(
            $this->get($id)
        );
    }

    /**
     * @throws CityNotFound
     */
    public function getByUrl(string $url): CityData
    {
        $city = $this->cityReadStorage->getByUrl($url);

        if (!$city) {
            throw new CityNotFound();
        }

        return $this->mapToData($city);
    }

    /**
     * @param int ...$ids
     * @return CityData[]
     */
    public function getByIds(int ...$ids): array
    {
        return array_map([$this, 'mapToData'], $this->cityReadStorage->getByIds(...$ids));
    }

    /**
     * @return CityData[]
     * @throws CityBadRequest
     */
    public function getListByFilterQuery(SearchQuery $query): array
    {
        foreach ($query->getOrderQueries() as $orderQuery) {
            if (!in_array($orderQuery->getProperty(), $this->getAllowedOrderProperties(), true)) {
                throw new CityBadRequest(sprintf('Invalid ordering property "%s"', $orderQuery->getProperty()));
            }
        }

        $cities = $this->cityReadStorage->getListByFilterQuery($query);

        return array_map(
            [
                $this,
                'mapToData'
            ],
            $cities
        );
    }

    /**
     * @return TreeCityData[]
     */
    public function getTreeListbyStateId(int $stateId): array
    {
        return array_map(
            [
                $this,
                'mapToTreeData'
            ],
            $this->cityReadStorage->getListbyStateId($stateId)
        );
    }

    /**
     * @return TreeCityData[]
     */
    public function getTreeListbyCountryId(int $countryId): array
    {
        return array_map(
            [
                $this,
                'mapToTreeData'
            ],
            $this->cityReadStorage->getListbyCountryId($countryId)
        );
    }

    public function countByFilterQuery(SearchQuery $query): int
    {
        return $this->cityReadStorage->countByFilterQuery($query);
    }

    /**
     * @return CityData[]
     */
    public function getTopCities(): array
    {
        return array_map(
            [
                $this,
                'mapToData'
            ],
            $this->cityReadStorage->getTopCities()
        );
    }

    /**
     * @return SearchCityData[]
     */
    public function getAllCities(): array
    {
        return array_map(
            [
                $this,
                'mapToSearchData'
            ],
            $this->cityReadStorage->getAllCities()
        );
    }

    /**
     * @return CityData[]
     */
    public function getPopularCitiesListByParentId(int $stateId): array
    {
        return array_map(
            [
                $this,
                'mapToData'
            ],
            $this->cityReadStorage->getPopularCitiesListByParentId($stateId)
        );
    }

    /**
     * @throws CityNotFound
     */
    private function get(int $id): City
    {
        $city = $this->cityReadStorage->get($id);

        if (!$city) {
            throw new CityNotFound();
        }

        return $city;
    }

    private function mapToData(City $city): CityData
    {
        return new CityData($city);
    }

    private function mapToSearchData(City $city): SearchCityData
    {
        return new SearchCityData($city);
    }

    private function mapToTreeData(City $city): TreeCityData
    {
        return new TreeCityData($city);
    }

    /**
     * @return string[]
     */
    private function getAllowedOrderProperties(): array
    {
        return [
            't.id',
            't.name',
        ];
    }
}
