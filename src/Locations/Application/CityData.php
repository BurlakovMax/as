<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\City;
use Swagger\Annotations as SWG;

final class CityData
{
    private const TYPE = 'city';

    /**
     * @SWG\Property()
     */
    private int $id;

    /**
     * @SWG\Property()
     */
    private string $name;

    /**
     * @SWG\Property()
     */
    private string $url;

    /**
     * @SWG\Property()
     */
    private ?string $description;

    /**
     * @SWG\Property(type="array", @SWG\Items(type="string"))
     */
    private array $images;

    /**
     * @SWG\Property()
     */
    private ?string $video;

    /**
     * @SWG\Property()
     */
    private bool $active;

    /**
     * @SWG\Property()
     */
    private ?bool $isSignificant;

    /**
     * @SWG\Property()
     */
    private ?int $countryId;

    /**
     * @SWG\Property()
     */
    private ?int $stateId;

    private ?string $stateName;

    private ?string $countryName;

    private ?bool $isPopular;

    private string $code;

    public function __construct(City $city)
    {
        $this->id = $city->getId();
        $this->name = $city->getName();
        $this->url = $city->getUrl();
        $this->description = $city->getDescription();
        $this->images = $city->getImages();
        $this->video = $city->getVideo();
        $this->active = $city->getActive();
        $this->isSignificant = $city->getIsSignificant();
        $this->countryId = $city->getCountryId();
        $this->stateId = $city->getStateId();
        $this->stateName = $city->getStateName();
        $this->countryName = $city->getCountryName();
        $this->isPopular = $city->getIsPopular();
        $this->code = $city->getCode();
    }

    public function getStateName(): ?string
    {
        return $this->stateName;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function getIsSignificant(): ?bool
    {
        return $this->isSignificant;
    }

    public function getCountryId(): ?int
    {
        return $this->countryId;
    }

    public function getStateId(): ?int
    {
        return $this->stateId;
    }

    public function getIsPopular(): ?bool
    {
        return $this->isPopular;
    }

    public function getTypeString(): string
    {
        return self::TYPE;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
