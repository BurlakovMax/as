<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\Country;

final class TreeCountryData
{
    private int $id;

    private string $name;

    private string $code;

    private bool $hasStates;

    /**
     * @var TreeCountryData[]
     */
    private array $subCountries;

    /**
     * @var TreeStateData[]
     */
    private array $states;

    /**
     * @var TreeCityData[]
     */
    private array $cities;

    public function __construct(Country $country)
    {
        $this->id = $country->getId();
        $this->name = $country->getName();
        $this->code = $country->getCode();
        $this->hasStates = $country->hasStates();
        $this->subCountries = [];
        $this->states = [];
        $this->cities = [];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getHasStates(): bool
    {
        return $this->hasStates;
    }

    /**
     * @return TreeCountryData[]
     */
    public function getSubCountries(): array
    {
        return $this->subCountries;
    }

    /**
     * @return TreeStateData[]
     */
    public function getStates(): array
    {
        return $this->states;
    }

    /**
     * @return TreeCityData[]
     */
    public function getCities(): array
    {
        return $this->cities;
    }

    /**
     * @param TreeCountryData[] $subCountries
     */
    public function setSubCountries(array $subCountries): void
    {
        $this->subCountries = $subCountries;
    }

    /**
     * @param TreeStateData[] $states
     */
    public function setStates(array $states): void
    {
        $this->states = $states;
    }

    /**
     * @param TreeCityData[] $cities
     */
    public function setCities(array $cities): void
    {
        $this->cities = $cities;
    }
}