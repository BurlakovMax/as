<?php

declare(strict_types=1);

namespace App\Locations\Application;

use App\Locations\Domain\State;

final class TreeStateData
{
    private int $id;

    private string $name;

    private string $url;

    /**
     * @var TreeCityData[]
     */
    private array $cities;

    public function __construct(State $state)
    {
        $this->id = $state->getId();
        $this->name = $state->getName();
        $this->url = $state->getUrl();
        $this->cities = [];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return TreeCityData[]
     */
    public function getCities(): array
    {
        return $this->cities;
    }

    /**
     * @param TreeCityData[] $cities
     */
    public function setCities(array $cities): void
    {
        $this->cities = $cities;
    }
}