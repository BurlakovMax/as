<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use App\Core\DomainSupport\Enumerable;

final class Type extends Enumerable
{
    public static function worldPart(): self
    {
        return self::createEnum(0);
    }

    public static function country(): self
    {
        return self::createEnum(1);
    }

    public static function state(): self
    {
        return self::createEnum(2);
    }

    public static function city(): self
    {
        return self::createEnum(3);
    }

    public static function neighborhood(): self
    {
        return self::createEnum(4);
    }
}
