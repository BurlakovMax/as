<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use App\Core\Application\Search\SearchQuery;

interface CityReadStorage
{
    public function get(int $id): ?City;

    public function getByUrl(string $url): ?City;

    /**
     * @param int ...$ids
     * @return City[]
     */
    public function getByIds(int ...$ids): array;

    public function countByFilterQuery(SearchQuery $query): int;

    /**
     * @return City[]
     */
    public function getListByFilterQuery(SearchQuery $query): array;

    /**
     * @return City[]
     */
    public function getTopCities(): array;

    /**
     * @return City[]
     */
    public function getAllCities(): array;

    /**
     * @return City[]
     */
    public function getListbyStateId(int $stateId): array;

    /**
     * @return City[]
     */
    public function getListbyCountryId(int $countryId): array;

    /**
     * @return City[]
     */
    public function getPopularCitiesListByParentId(int $Id): array;
}
