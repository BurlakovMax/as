<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Locations\Infrastructure\Persistence\StateDoctrineRepository")
 * @ORM\Table(name="location_location")
 */
class State
{
    /**
     * @ORM\Column(name="loc_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var Type
     *
     * @ORM\Column(name="loc_type", type="type")
     */
    private $type;

    /**
     * @ORM\Column(name="country_id", type="integer", length=11)
     */
    private int $countryId;

    /**
     * @var Country
     *
     * @ORM\OneToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="loc_id")
     */
    private $country;

    /**
     * @ORM\Column(name="loc_name", type="string", length=100)
     */
    private string $name;

    /**
     * @ORM\Column(name="loc_url", type="string", length=150)
     */
    private string $url;

    /**
     * @ORM\Column(name="has_place_or_ad", type="boolean")
     */
    private bool $active;

    /**
     * @ORM\Column(name="s", type="string", length=4)
     */
    private string $code;

    public function getActive(): bool
    {
        return $this->active;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getCountryName(): string
    {
        return $this->getCountry()->getName();
    }

    public function getCountrySubdomain(): string
    {
        return $this->getCountry()->getSubdomain();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    private function getCountry(): Country
    {
        return $this->country;
    }
}
