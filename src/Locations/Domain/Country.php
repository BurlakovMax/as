<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Locations\Infrastructure\Persistence\CountryDoctrineRepository")
 * @ORM\Table(name="location_location")
 */
class Country
{
    /**
     * @ORM\Column(name="loc_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var Type
     *
     * @ORM\Column(name="loc_type", type="type")
     */
    private $type;

    /**
     * @ORM\Column(name="loc_name", type="string", length=100)
     */
    private string $name;

    /**
     * @ORM\Column(name="country_sub", type="string", length=20, nullable=true)
     */
    private ?string $subdomain;

    /**
     * @ORM\Column(name="has_states", type="boolean")
     */
    private bool $hasStates;

    /**
     * @ORM\Column(name="has_place_or_ad", type="boolean", nullable=true)
     */
    private ?bool $active;

    /**
     * @ORM\Column(name="s", type="string", length=4)
     */
    private string $code;

    /**
     * @ORM\Column(name="level", type="integer")
     */
    private int $level;

    /**
     * @ORM\Column(name="weight", type="integer")
     */
    private int $weight;

    /**
     * @ORM\Column(name="loc_parent", type="integer")
     */
    private int $parentLocation;

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSubdomain(): ?string
    {
        return $this->subdomain;
    }

    public function hasStates(): bool
    {
        return $this->hasStates;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
