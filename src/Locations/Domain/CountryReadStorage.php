<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use App\Core\Application\Search\SearchQuery;

interface CountryReadStorage
{
    public function get(int $id): ?Country;

    public function countByFilterQuery(SearchQuery $query): int;

    public function getByName(string $name): ?Country;

    /**
     * @return Country[]
     */
    public function getListByFilterQuery(SearchQuery $query): array;

    /**
     * @return Country[]
     */
    public function getListByLevel(int $level): array;

    /**
     * @return Country[]
     */
    public function getListByParentId(int $parentId): array;
}
