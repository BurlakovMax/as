<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use App\Core\Application\Search\SearchQuery;

interface StateReadStorage
{
    public function get(int $id): ?State;

    public function getByUrl(string $url): ?State;

    public function countByFilterQuery(SearchQuery $query): int;

    /**
     * @return State[]
     */
    public function getListByFilterQuery(SearchQuery $query): array;

    /**
     * @return State[]
     */
    public function getListByCountryId(int $countryId): array;
}
