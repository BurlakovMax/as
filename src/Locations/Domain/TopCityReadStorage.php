<?php

declare(strict_types=1);

namespace App\Locations\Domain;

interface TopCityReadStorage
{
    public function get(int $id): ?TopCity;
}