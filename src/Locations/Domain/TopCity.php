<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Locations\Infrastructure\Persistence\TopCityDoctrineRepository")
 * @ORM\Table(name="location_topcities")
 */
class TopCity
{
    /**
     * @ORM\Column(name="loc_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $stateId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $countryId;

    /**
     * @ORM\Column(name="loc_url", type="string", length=100)
     */
    private string $url;

    /**
     * @ORM\Column(name="loc_name", type="string", length=50)
     */
    private string $name;

    /**
     * @ORM\Column(name="big", type="boolean")
     */
    private bool $isBigCity;

    public function getId(): int
    {
        return $this->id;
    }

    public function getStateId(): int
    {
        return $this->stateId;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isBigCity(): bool
    {
        return $this->isBigCity;
    }
}