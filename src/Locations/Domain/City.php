<?php

declare(strict_types=1);

namespace App\Locations\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Locations\Infrastructure\Persistence\CityDoctrineRepository")
 * @ORM\Table(name="location_location")
 */
class City
{
    /**
     * @ORM\Column(name="loc_id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var Type
     *
     * @ORM\Column(name="loc_type", type="type")
     */
    private $type;

    /**
     * @ORM\Column(name="country_id", type="integer", length=11)
     */
    private ?int $countryId;

    /**
     * @var Country
     *
     * @ORM\OneToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="loc_id")
     */
    private $country;

    /**
     * @ORM\Column(name="loc_parent", type="integer", length=11, nullable=true)
     */
    private ?int $stateId;

    /**
     * @var State|null
     *
     * @ORM\OneToOne(targetEntity="State")
     * @ORM\JoinColumn(name="loc_parent", referencedColumnName="loc_id", nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(name="loc_name", type="string", length=100)
     */
    private string $name;

    /**
     * @ORM\Column(name="full_url", type="string", length=150)
     */
    private string $url;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private ?string $description;

    /**
     * @ORM\Column(name="image_filenames", type="string", nullable=true)
     */
    private ?string $images;

    /**
     * @ORM\Column(name="video_html", type="string", nullable=true)
     */
    private ?string $video;

    /**
     * @ORM\Column(name="has_place_or_ad", type="boolean")
     */
    private bool $active;

    /**
     * @ORM\Column(name="significant", type="boolean", nullable=true)
     */
    private ?bool $isSignificant;

    /**
     * @ORM\Column(name="bp", type="boolean", nullable=true)
     */
    private ?bool $isPopular;

    /**
     * @ORM\Column(name="s", type="string", length=4)
     */
    private string $code;

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getCountryId(): int
    {
        return $this->countryId;
    }

    public function getCountryName(): string
    {
        return $this->getCountry()->getName();
    }

    public function getCountrySubdomain(): string
    {
        return $this->getCountry()->getSubdomain();
    }

    public function getStateId(): ?int
    {
        return $this->hasState() ? $this->stateId : null;
    }

    public function getStateName(): ?string
    {
        return $this->hasState() ? $this->getState()->getName() : null;
    }

    public function getStateUrl(): ?string
    {
        return $this->hasState() ? $this->getState()->getUrl() : null;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images ? explode('|', $this->images) : [];
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    private function getCountry(): Country
    {
        return $this->country;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function getIsSignificant(): ?bool
    {
        return $this->isSignificant;
    }

    public function getIsPopular(): ?bool
    {
        return $this->isPopular;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    private function hasState(): bool
    {
        return $this->stateId !== null && $this->stateId !== $this->countryId;
    }

    private function getState(): ?State
    {
        return $this->state;
    }
}
