<?php

declare(strict_types=1);

namespace App\Locations\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Locations\Domain\City;
use App\Locations\Domain\CityReadStorage;
use App\Locations\Domain\Type;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\QueryException;

final class CityDoctrineRepository extends EntityRepository implements CityReadStorage
{
    public function get(int $id): ?City
    {
        return $this->find($id);
    }

    public function getByUrl(string $url): ?City
    {
        return $this->findOneBy(['url' => $url]);
    }

    /**
     * @param int ...$ids
     * @return City[]
     * @throws QueryException
     */
    public function getByIds(int ...$ids): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addCriteria($this->createCriteria(null))
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->in('id', $ids)
            ))
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->eq('active', true)
            ));

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws QueryException
     */
    public function countByFilterQuery(SearchQuery $query): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->addCriteria($this->createCriteria(null))
                ->innerJoin('t.state', 'state')
                ->innerJoin('t.country', 'country')
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.name']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    public function getListbyStateId(int $stateId): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addCriteria($this->createCriteria(null))
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->eq('stateId', $stateId)
            ))
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->eq('active', true)
            ));

        return
            $builder
                ->orderBy('t.name')
                ->getQuery()
                ->getResult()
            ;
    }

    public function getListbyCountryId(int $countryId): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addCriteria($this->createCriteria(null))
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->eq('countryId', $countryId)
            ))
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->eq('active', true)
            ));

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @return City[]
     * @throws QueryException
     */
    public function getListByFilterQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->innerJoin('t.state', 'state')
            ->innerJoin('t.country', 'country')
            ->addCriteria($this->createCriteria(null))
            ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.name']))
            ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
            ->addCriteria(
                Limitation::limitation($query->getLimitation())
            )
        ;

        Ordering::add($builder, $query->getOrderQueries());

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @return City[]
     * @throws QueryException
     */
    public function getAllCities(): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->innerJoin('t.state', 'state')
            ->innerJoin('t.country', 'country')
            ->addCriteria(Criteria::create()->where(Criteria::expr()->eq('t.active', true)))
            ->orderBy('t.name');

        return $builder->getQuery()->getResult();
    }

    /**
     * @return City[]
     * @throws QueryException
     */
    public function getTopCities(): array
    {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->innerJoin('t.state', 'state')
                ->innerJoin('t.country', 'country')
                ->addCriteria($this->createCriteria(null))
                ->addCriteria(
                    Criteria::create()
                        ->andWhere(Criteria::expr()->eq('t.active', true))
                        ->andWhere(Criteria::expr()->orX(
                            Criteria::expr()->eq('country.hasStates', false),
                            Criteria::expr()->eq('t.isSignificant', true)
                        ))
                )
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @return City[]
     * @throws QueryException
     */
    public function getPopularCitiesListByParentId(int $id): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addCriteria($this->createCriteria(null))
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->eq('stateId', $id),
            ));

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    private function createCriteria(?string $query): Criteria
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->eq('t.type', (int)Type::city()->getRawValue()));
        if ($query !== null) {
            $criteria->andWhere(Criteria::expr()->contains('t.name', $query));
        }
        return $criteria;
    }
}
