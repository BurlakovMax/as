<?php

declare(strict_types=1);

namespace App\Locations\Infrastructure\Persistence;

use App\Locations\Domain\TopCity;
use App\Locations\Domain\TopCityReadStorage;
use Doctrine\ORM\EntityRepository;

final class TopCityDoctrineRepository extends EntityRepository implements TopCityReadStorage
{
    public function get(int $id): ?TopCity
    {
        return $this->find($id);
    }
}