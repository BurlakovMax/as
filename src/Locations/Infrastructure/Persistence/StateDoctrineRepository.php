<?php

declare(strict_types=1);

namespace App\Locations\Infrastructure\Persistence;

use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Locations\Domain\State;
use App\Locations\Domain\StateReadStorage;
use App\Locations\Domain\Type;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

final class StateDoctrineRepository extends EntityRepository implements StateReadStorage
{
    public function get(int $id): ?State
    {
        return $this->find($id);
    }

    public function getByUrl(string $url): ?State
    {
        return $this->findOneBy(['url' => $url]);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countByFilterQuery(SearchQuery $query): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->innerJoin('t.country', 'country')
                ->addCriteria(Criteria::create()->where(
                    Criteria::expr()->eq('type', Type::state()->getRawValue())
                ))
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.name']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @return State[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getListByFilterQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->innerJoin('t.country', 'country')
            ->addCriteria(Criteria::create()->where(
                Criteria::expr()->eq('type', Type::state()->getRawValue())
            ))
            ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.name']))
            ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
            ->addCriteria(
                Limitation::limitation($query->getLimitation())
            )
        ;

        Ordering::add($builder, $query->getOrderQueries());

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @return State[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getListByCountryId(int $countryId): array
    {
        $builder = $this->createQueryBuilder('t');

        return
            $builder
                ->addCriteria($this->createCriteria($countryId, null))
                ->getQuery()
                ->getResult()
            ;
    }

    private function createCriteria(int $countryId, ?string $query): Criteria
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->eq('t.type', Type::state()->getRawValue()));
        if ($countryId !== 0) {
            $criteria->andWhere(Criteria::expr()->eq('t.countryId', $countryId));
        }
        if ($query !== null) {
            $criteria->andWhere(Criteria::expr()->contains('t.name', $query));
        }
        return $criteria;
    }
}
