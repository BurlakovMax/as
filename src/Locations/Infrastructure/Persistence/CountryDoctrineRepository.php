<?php

declare(strict_types=1);

namespace App\Locations\Infrastructure\Persistence;

use App\Core\Application\Search\LimitationQueryImmutable;
use App\Core\Application\Search\OrderQuery;
use App\Core\Application\Search\SearchQuery;
use App\Core\Infrastructure\Search\Doctrine\Expression\CriteriaBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\FuzzySearchBuilder;
use App\Core\Infrastructure\Search\Doctrine\Expression\Limitation;
use App\Core\Infrastructure\Search\Doctrine\Expression\Ordering;
use App\Locations\Application\CountryBadRequest;
use App\Locations\Domain\Country;
use App\Locations\Domain\CountryReadStorage;
use App\Locations\Domain\Type;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

final class CountryDoctrineRepository extends EntityRepository implements CountryReadStorage
{
    public function get(int $id): ?Country
    {
        return $this->find($id);
    }

    public function getByName(string $name): ?Country
    {
        return $this->findOneBy(
            [
                'loc_parent' => 0,
                'loc_name' => $name,
            ]
        );
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countByFilterQuery(SearchQuery $query): int
    {
        $count =
            $this->createQueryBuilder('t')
                ->addCriteria($this->createCriteria(null))
                ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.name']))
                ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
                ->select('COUNT(t.id) as count')
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return (int)$count['count'];
    }

    /**
     * @return Country[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getListByFilterQuery(SearchQuery $query): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addCriteria($this->createCriteria(null))
            ->addCriteria(FuzzySearchBuilder::search($query->getFuzzy(), ['t.name']))
            ->addCriteria(CriteriaBuilder::createByFilters($query->getFilters()))
            ->addCriteria(
                Limitation::limitation($query->getLimitation())
            )
        ;

        Ordering::add($builder, $query->getOrderQueries());

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    /**
     * @return Country[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getListByLevel(int $level = 0): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addCriteria(Criteria::create()->andWhere(
                Criteria::expr()->eq('level', $level)
            ))->addOrderBy('t.weight', 'ASC');

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    public function getListByParentId(int $parentId): array
    {
        $builder = $this->createQueryBuilder('t');

        $builder
            ->addCriteria($this->createCriteria(null))
            ->addCriteria(Criteria::create()->andWhere(
                Criteria::expr()->eq('parentLocation', $parentId)
            ))->addOrderBy('t.weight', 'ASC');

        return
            $builder
                ->getQuery()
                ->getResult()
            ;
    }

    private function createCriteria(?string $query): Criteria
    {
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->eq('t.type', (int)Type::country()->getRawValue()));
        if ($query !== null) {
            $criteria->andWhere(Criteria::expr()->contains('t.name', $query));
        }
        return $criteria;
    }
}
