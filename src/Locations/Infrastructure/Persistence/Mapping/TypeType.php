<?php

declare(strict_types=1);

namespace App\Locations\Infrastructure\Persistence\Mapping;

use App\Locations\Domain\Type;
use LazyLemurs\DoctrineEnumerableType\EnumerableType;

final class TypeType extends EnumerableType
{
    protected function getEnumClass(): string
    {
        return Type::class;
    }

    public function getName()
    {
        return 'type';
    }
}
