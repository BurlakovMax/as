<?php

declare(strict_types=1);

namespace App\Support\Domain;

use App\Support\Application\CreateContactFormCommand;
use Doctrine\ORM\Mapping as ORM;
use LazyLemurs\Structures\Email;
use DateTimeImmutable;
use LazyLemurs\Structures\Ip;
use LazyLemurs\Structures\Url;
use LazyLemurs\Structures\PhoneNumber;

/**
 * @ORM\Entity(repositoryClass="App\Support\Infrastructure\Persistence\ContactFormDoctrineRepository")
 * @ORM\Table(name="contact")
 */
class ContactForm
{
    /**
     * @ORM\Column(name="contact_id", type="integer", length=11)
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $accountId;

    /**
     * @ORM\Column(type="email", length=100)
     */
    private Email $email;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private Ip $ipAddress;

    /**
     * @ORM\Column(name="stamp", type="timestamp", length=11)
     */
    private DateTimeImmutable $submitOn;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(name="text", type="text")
     */
    private string $message;

    /**
     * @ORM\Column(name="url", type="string", length=180)
     */
    private Url $referenceUrl;

    /**
     * @ORM\Column(name="agent", type="string", length=255)
     */
    private string $userAgent;

    /**
     * @ORM\Column(name="`read`", type="integer", length=3)
     */
    private int $wasRead;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $assigned;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private int $readBy;

    /**
     * @ORM\Column(name="respond", type="text")
     */
    private string $respondText;

    public function __construct(CreateContactFormCommand $createContactFormCommand)
    {
        $this->id = 0;
        $this->accountId = $createContactFormCommand->getAccountId() ?? 0;
        $this->email = $createContactFormCommand->getEmail();
        $this->ipAddress = $createContactFormCommand->getIpAddress();
        $this->submitOn = new DateTimeImmutable();
        $this->phone = $this->phoneNumberToString($createContactFormCommand->getPhone());
        $this->name = $createContactFormCommand->getName() ?? '';
        $this->message = $createContactFormCommand->getMessage() ?? '';
        $this->referenceUrl = $createContactFormCommand->getReferenceUrl();
        $this->userAgent = $createContactFormCommand->getUserAgent();
        $this->wasRead = 0;
        $this->assigned = 0;
        $this->readBy = 0;
        $this->respondText = $createContactFormCommand->getRespondText() ?? '';
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountId(): int
    {
        return $this->accountId;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getIpAddress(): Ip
    {
        return $this->ipAddress;
    }

    public function getSubmitOn(): DateTimeImmutable
    {
        return $this->submitOn;
    }

    public function getPhone(): ?PhoneNumber
    {
        return $this->stringToPhoneNumber($this->phone);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getReferenceUrl(): Url
    {
        return $this->referenceUrl;
    }

    public function getAgent(): string
    {
        return $this->userAgent;
    }

    public function wasRead(): int
    {
        return $this->wasRead;
    }

    public function getAssigned(): int
    {
        return $this->assigned;
    }

    public function getReadBy(): int
    {
        return $this->readBy;
    }

    public function respondText(): string
    {
        return $this->respondText;
    }

    private function phoneNumberToString(?PhoneNumber $phoneNumber): string
    {
        if ($phoneNumber !== null) {
            return $phoneNumber->getValue();
        }

        return '';
    }

    private function stringToPhoneNumber(string $phone): PhoneNumber
    {
        return new PhoneNumber($phone);
    }
}