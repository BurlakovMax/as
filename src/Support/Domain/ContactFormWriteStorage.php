<?php

declare(strict_types=1);

namespace App\Support\Domain;

interface ContactFormWriteStorage
{
    public function create(ContactForm $contactForm): void;
}