<?php

declare(strict_types=1);

namespace App\Support\Application;

use LazyLemurs\Structures\Email;
use LazyLemurs\Commander\Property;
use LazyLemurs\Structures\Ip;
use LazyLemurs\Structures\PhoneNumber;
use LazyLemurs\Structures\Url;

final class CreateContactFormCommand
{
    /**
     * @Property()
     */
    private ?int $accountId;

    /**
     * @Property()
     */
    private Email $email;

    private Ip $ipAddress;

    private string $userAgent;

    /**
     * @Property()
     */
    private Url $referenceUrl;

    /**
     * @Property()
     */
    private ?PhoneNumber $phone;

    /**
     * @Property()
     */
    private ?string $name;

    /**
     * @Property()
     */
    private ?string $message;

    /**
     * @Property()
     */
    private ?bool $wasRead;

    /**
     * @Property()
     */
    private ?int $assigned;

    /**
     * @Property()
     */
    private ?int $readBy;

    /**
     * @Property()
     */
    private ?string $respondText;

    public function __construct(Ip $ipAddress, string $userAgent)
    {
        $this->ipAddress = $ipAddress;
        $this->userAgent = $userAgent;
    }

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getIpAddress(): Ip
    {
        return $this->ipAddress;
    }

    public function getPhone(): ?PhoneNumber
    {
       return $this->phone;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getReferenceUrl(): ?Url
    {
        return $this->referenceUrl;
    }

    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    public function wasRead(): bool
    {
        return $this->wasRead;
    }

    public function getAssigned(): ?int
    {
        return $this->assigned;
    }

    public function getReadBy(): ?int
    {
        return $this->readBy;
    }

    public function getRespondText(): ?string
    {
        return $this->respondText;
    }
}