<?php

declare(strict_types=1);

namespace App\Support\Application;

use App\Support\Domain\ContactForm;
use App\Support\Domain\ContactFormWriteStorage;
use LazyLemurs\TransactionManager\TransactionManager;

final class ContactFormCommandProcessor
{
    private TransactionManager $transactionManager;

    private ContactFormWriteStorage $contactFormWriteStorage;

    public function __construct(
        TransactionManager $transactionManager,
        ContactFormWriteStorage $contactFormWriteStorage
    ) {
        $this->transactionManager = $transactionManager;
        $this->contactFormWriteStorage = $contactFormWriteStorage;
    }

    /**
     * @throws \Throwable
     */
    public function create(CreateContactFormCommand $contactFormCommand): int
    {
        $contactForm = new ContactForm($contactFormCommand);

        $this->transactionManager->transactional(function () use ($contactForm) {
            $this->contactFormWriteStorage->create($contactForm);
        });

        return $contactForm->getId();
    }
}