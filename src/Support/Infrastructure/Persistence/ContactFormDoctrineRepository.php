<?php

declare(strict_types=1);

namespace App\Support\Infrastructure\Persistence;

use App\Support\Domain\ContactForm;
use App\Support\Domain\ContactFormWriteStorage;
use Doctrine\ORM\EntityRepository;

final class ContactFormDoctrineRepository extends EntityRepository implements ContactFormWriteStorage
{
    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(ContactForm $contactForm): void
    {
        $this->getEntityManager()->persist($contactForm);
    }
}