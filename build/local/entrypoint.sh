#!/usr/bin/env bash

composer install --no-interaction --no-ansi --optimize-autoloader --apcu-autoloader
rm -rf ~/.composer/cache/*
bin/console cache:locations
bin/console cache:cities
php-fpm
