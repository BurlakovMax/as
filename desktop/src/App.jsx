import React from 'react';
import { Route } from 'react-router-dom';
import './App.scss';
import Locations from "./Pages/Open/Index/Locations";

class App extends React.Component {
    render() {
        return (
            <>
                <nav className="navbar navbar-expand-md navbar-light fixed-top bg-light">
                    <a className="navbar-brand" href="/">Adult Search</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarCollapse">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Link</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link disabled" href="#" tabIndex="-1"
                                   aria-disabled="true">Disabled</a>
                            </li>
                        </ul>
                    </div>
                </nav>

                <main role="main" className="container">
                    <Route path={'/'} component={Locations} />
                </main>
            </>
        );
    }
}

export default App;
