import * as Sentry from '@sentry/browser';
import {setLocation} from "../store/common";
import axios from "../services/api_init";

export default {

    getImageServer: function () {
        let url = '';
        let host = window.location.hostname;

        if(host.includes('127.0.0.1')) {
            url = '';
        }

        return url;
    },

    /**
     * Get image base url
     * @param string (avatar|escort|place)
     * @returns {string}
     */
    getImagePath: function (type) {
        let url = this.getImageServer();

        switch (type) {
            case 'avatar':
                url += '/images/avatar'
                break;
            case 'escort':
                url += '/images/escorts'
                break;
            case 'place':
                url += '/images/places'
                break;
            default:
                url = ''
        }

        return url;
    },

    getVideoPath: function (type) {
        let url = this.getImageServer();

        switch (type) {
            case 'escort':
                url += '/videos/escorts'
                break;
            case 'place':
                url += '/videos/places'
                break;
            default:
                url = ''
        }

        return url;
    },

    getServer: function () {
        let url = 'prod';
        let host = window.location.hostname;

        if(host.includes('127.0.0.1')) {
            url = 'local';
        } else if (host.includes('pwa.adultsearch.com')) {
            url = 'dev';
        }

        return url;
    },

    isProd: function () {
        return this.getServer() === 'prod';
    },

    isLocal: function () {
        return this.getServer() === 'local';
    },

    isDev: function () {
        return this.getServer() === 'dev';
    },

    debug: function (title, data = false) {
        if (!this.isProd()) {
            if (arguments.length === 2) {
                console.log(title, data);
            } else {
                console.log(title);
            }
        }
    },

    catch: function (error) {
        if (this.isProd()) {
            Sentry.captureException(error);
        } else {
            console.error(error);
            if (error.response && error.response.data) {
                console.error(error.response.data.message);
            }
        }
    },

    version: function () {
        if (this.isProd()) {
            axios.get('/version.txt').then(version => {
                Sentry.init({
                    dsn: 'https://8929f2d6cb194ac7bbc7aba8d896e88e@sentry.io/2704256',
                    release: version.data,
                });
            });
        }
    },

    getEscortStatus: function (escort, format = false) {
        let status;
        switch (escort.status) {
            case -2:
                status = 'Deleted'
                break;
            case -1:
                status = 'In Creation'
                break;
            case 0:
                status = 'Expired'
                break;
            case 1:
                status = 'Active'
                break;
            case 2:
                status = 'Hidden'
                break;
            case 3:
                status = 'In Verification'
                break;
            default:
                status = 'Unset';
        }

        if (format) {
            return status.toLowerCase().replace(' ', '_');
        } else {
            return status;
        }
    },

    /**
     * Get escort upgrade type(s) as used in payment form url
     * @param type (sponsor|side|stickyWeek|stickyMonth)
     */
    getEscortUpgradeType: function (type = false) {
        let types = [
            'escortSponsorLocationIds',
            'escortSideLocationIds',
            'escortStickyWeekLocationIds',
            'escortStickyMonthLocationIds'
        ];

        if (type) {
            switch (type) {
                case 'sponsor':
                    return types[0]
                    break;
                case 'side':
                    return types[1]
                    break;
                case 'stickyWeek':
                    return types[2]
                    break;
                case 'stickyMonth':
                    return types[3]
                    break;
            }
        }

        return types;
    },

    getPaymentUrl: function (params) {
        let queryUrl = Object.keys(params).map(key =>
          key + '=' + params[key]
        ).join('&');

        return `/payment/pay?${queryUrl}`;
    },

    /**
     * Get global title for each component
     * based on list of components in App.jsx
     */
    getTitle: function (component) {
        let titles = {
            'Account': 'My AS',
            'ChangeEmail': 'Change Email',
            'ConfirmEmail': 'Confirm Email',
            'ChangePassword': 'Change Password',
            'ChangeSettings': 'Settings',
            'ChangeUsername': 'Edit Username',
            'SignupConfirm': 'Signup Confirmed',
            'Payments': 'Payments',
            'ResetPassword': '',
            'Signin': '',
            'Signup': '',
            'Step1': 'Post Ad',
            'Step2': 'Post Ad',
            'Confirm': 'Phone Confirmation',
            'BuyTopups': 'Buy Topups',
            'MyAds': 'My Ads',
            'DuplicateAd': 'Duplicate Ad',
            'ChangeLocation': 'Change Ad Location',
            'EditAdThumbnail': 'Edit Ad Thumbnails',
            'ContactUs': 'Contact Us',
            'PaymentForm': 'Payment Details',
            'PaymentRecharge': 'Recharge Balance',
            'PrivacyPolicy': 'Privacy Policy',
            'TermsOfService': 'Terms Of Service',
            'City': '',
            'EscortsCategory': '',
            'PlacesCategory': '',
            'Place': '',
            'BusinessownerPick': 'Choose Advertising Package',
            'ForumTopicNew': 'New Forum Topic',
            'Wire': 'Wire Sent Form',
            'PlaceAdd': 'Add your business',
        };

        return (titles[component.constructor.name])? titles[component.constructor.name] : false;
    },

    getScreenWidth: function () {
        return Math.max(window.screen.width, window.innerWidth);
    },

    getHoursFrom: function (key = false, value = false) {
        let hours = [
            {value: '0', name: '12:00AM'},
            {value: '30', name: '12:30AM'},
            {value: '60', name: '01:00AM'},
            {value: '90', name: '01:30AM'},
            {value: '120', name: '02:00AM'},
            {value: '150', name: '02:30AM'},
            {value: '180', name: '03:00AM'},
            {value: '210', name: '03:30AM'},
            {value: '240', name: '04:00AM'},
            {value: '270', name: '04:30AM'},
            {value: '300', name: '05:00AM'},
            {value: '330', name: '05:30AM'},
            {value: '360', name: '06:00AM'},
            {value: '390', name: '06:30AM'},
            {value: '420', name: '07:00AM'},
            {value: '450', name: '07:30AM'},
            {value: '480', name: '08:00AM'},
            {value: '510', name: '08:30AM'},
            {value: '540', name: '09:00AM'},
            {value: '570', name: '09:30AM'},
            {value: '600', name: '10:00AM'},
            {value: '630', name: '10:30AM'},
            {value: '660', name: '11:00AM'},
            {value: '690', name: '11:30AM'},
            {value: '720', name: '12:00PM'},
            {value: '750', name: '12:30PM'},
            {value: '780', name: '01:00PM'},
            {value: '810', name: '01:30PM'},
            {value: '840', name: '02:00PM'},
            {value: '870', name: '02:30PM'},
            {value: '900', name: '03:00PM'},
            {value: '930', name: '03:30PM'},
            {value: '960', name: '04:00PM'},
            {value: '990', name: '04:30PM'},
            {value: '1020', name: '05:00PM'},
            {value: '1050', name: '05:30PM'},
            {value: '1080', name: '06:00PM'},
            {value: '1110', name: '06:30PM'},
            {value: '1140', name: '07:00PM'},
            {value: '1170', name: '07:30PM'},
            {value: '1200', name: '08:00PM'},
            {value: '1230', name: '08:30PM'},
            {value: '1260', name: '09:00PM'},
            {value: '1290', name: '09:30PM'},
            {value: '1320', name: '10:00PM'},
            {value: '1350', name: '10:30PM'},
            {value: '1380', name: '11:00PM'},
            {value: '1410', name: '11:30PM'},
        ];

        if (key) {
            let result = hours.filter(hour => {
                return parseInt(hour.value) === parseInt(key)
            });
            return (result.length)? result[0].name : false;
        } else if (value) {
            let result = hours.filter(hour => {
                return hour.name === value
            });
            return (result.length)? result[0].value : false;
        } else {
            return hours;
        }
    },

    getHoursTo: function (key = false, value = false) {
        let hours = [
            {value: '1470', name: '12:30AM'},
            {value: '1500', name: '01:00AM'},
            {value: '1530', name: '01:30AM'},
            {value: '1560', name: '02:00AM'},
            {value: '1590', name: '02:30AM'},
            {value: '1620', name: '03:00AM'},
            {value: '1650', name: '03:30AM'},
            {value: '1680', name: '04:00AM'},
            {value: '1710', name: '04:30AM'},
            {value: '1740', name: '05:00AM'},
            {value: '1770', name: '05:30AM'},
            {value: '1800', name: '06:00AM'},
            {value: '1830', name: '06:30AM'},
            {value: '1860', name: '07:00AM'},
            {value: '1890', name: '07:30AM'},
            {value: '1920', name: '08:00AM'},
            {value: '1950', name: '08:30AM'},
            {value: '1980', name: '09:00AM'},
            {value: '2010', name: '09:30AM'},
            {value: '2040', name: '10:00AM'},
            {value: '2070', name: '10:30AM'},
            {value: '2100', name: '11:00AM'},
            {value: '2130', name: '11:30AM'},
            {value: '720', name: '12:00PM'},
            {value: '750', name: '12:30PM'},
            {value: '780', name: '01:00PM'},
            {value: '810', name: '01:30PM'},
            {value: '840', name: '02:00PM'},
            {value: '870', name: '02:30PM'},
            {value: '900', name: '03:00PM'},
            {value: '930', name: '03:30PM'},
            {value: '960', name: '04:00PM'},
            {value: '990', name: '04:30PM'},
            {value: '1020', name: '05:00PM'},
            {value: '1050', name: '05:30PM'},
            {value: '1080', name: '06:00PM'},
            {value: '1110', name: '06:30PM'},
            {value: '1140', name: '07:00PM'},
            {value: '1170', name: '07:30PM'},
            {value: '1200', name: '08:00PM'},
            {value: '1230', name: '08:30PM'},
            {value: '1260', name: '09:00PM'},
            {value: '1290', name: '09:30PM'},
            {value: '1320', name: '10:00PM'},
            {value: '1350', name: '10:30PM'},
            {value: '1380', name: '11:00PM'},
            {value: '1410', name: '11:30PM'},
            {value: '1440', name: '12:00AM'},
        ];

        if (key) {
            let result = hours.filter(hour => {
                return parseInt(hour.value) === parseInt(key)
            });
            return (result.length)? result[0].name : false;
        } else if (value) {
            let result = hours.filter(hour => {
                return hour.name === value
            });
            return (result.length)? result[0].value : false;
        } else {
            return hours;
        }
    },

    /**
     * Use or not email with escort
     * @returns {boolean}
     */
    useEscortEmail: function () {
        return false;
    },

    /**
     * Save current city for header to LS + global state
     * @param location
     */
    setCity: function (location) {
        let city = {
            name: location.name,
            url: location.url,
            id: location.id,
        }
        localStorage.setItem('city', JSON.stringify(city));
        setLocation(city);
    },

    /**
     * Get current city for header from LS
     */
    getCity: function () {
        let location = localStorage.getItem('city');

        if (location) {
            try {
                let city = JSON.parse(location);
                if (city && typeof city === 'object') {
                    return city;
                }
            } catch (e) {
                return false;
            }
        }

        return false;
    },

    /**
     * Social network names in mysql
     * @returns {string[]}
     */
    getSocials: function () {
        return [
            'facebook',
            'twitter',
            'telegram',
            'wechat',
            'instagram',
            'whatsapp',
        ];
    },

    cents2dollars: function (value) {
        let dollars = value / 100;
        return new Intl.NumberFormat('de-DE').format(dollars);
    },

    getUploadError: function (type, code = false) {
        let error = `Error uploading ${type}`;

        switch (code) {
            case 400:
                error += ': type not supported or file may be broken';
                break;
            case 413:
                error += ': file exceeds size limit';
                break;
            default:
        }

        return error;
    },

    getBankAccountId: function () {
        return 1000002;
    },

    ucFirst: function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    },

};
