import axios from "axios";
import {history} from "../index";
import config from "./config";

export default {
    clearToken: async function (token) {
        if (!token) {
            token = this.getToken();
        }

        if (!token) {
            return 'Error: Token is empty';
        }

        try {
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
            await axios.post('/api/v1/logout');
            localStorage.removeItem('auth');
            return false;
        } catch (error) {
            config.catch(error);

            if (error.response && error.response.data) {
                if (error.response.data.code === 400) {
                    localStorage.removeItem('auth');
                    return false;
                } else {
                    return error.response.data.message
                }
            }
        }
    },

    saveToken: function (token) {
        localStorage.setItem('auth', token);
    },

    getToken: function () {
        return localStorage.getItem('auth');
    },


    gotoAccount: function () {
        history.push(this.getLink('account'));
    },

    gotoHome: function () {
        history.push(this.getLink('home'));
    },

    gotoSignin: function () {
        history.push(this.getLink('signin'));
    },

    gotoSignup: function () {
        history.push(this.getLink('signup'));
    },

    isPage: function (page) {
        let uri = window.location.pathname;

        if (page === 'home' && (uri === '/' || !uri)) {
            return true;
        } else if (page === 'myposts' && uri === this.getLink('myposts')) {
            return true;
        } else if (page === 'adbuild' && uri.includes('adbuild')) {
            return true;
        } else if (page === 'adbuild1' && uri === '/adbuild') {
            return true;
        } else if (page === 'messages' && uri.includes('pm')) {
            return true;
        } else if (page === 'signin' && uri === '/account/signin') {
            return true;
        } else if (page === 'signup' && uri === '/account/signup') {
            return true;
        } else if (page === 'reset' && uri === '/account/reset') {
            return true;
        } else if (page === 'account' && uri.includes('account')) {
            return true;
        } else if (page === 'topups' && uri === '/classifieds/topups') {
            return true;
        }

        return false;
    },

    getLink: function (page, params = false) {
        let link = false;
        switch (page) {
            case 'home':
                link = '/'
                break;
            case 'account':
                link = '/account'
                break;
            case 'myposts':
                link = '/classifieds/myposts'
                break;
            case 'topups':
                link = '/classifieds/topups'
                break;
            case 'adbuild':
                link = '/adbuild'
                break;
            case 'adbuild1':
                link = '/adbuild'
                break;
            case 'adbuild2':
                link = '/adbuild/step2'
                break;
            case 'adbuild3':
                link = '/adbuild/step3'
                break;
            case 'adbuild-confirm':
                link = '/adbuild/confirm'
                break;
            case 'contact':
                link = '/contact_us'
                break;
            case 'terms':
                link = '/terms_of_service'
                break;
            case 'privacy':
                link = '/privacy_policy'
                break;
            case 'signin':
                link = '/account/signin'
                break;
            case 'signup':
                link = '/account/signup'
                break;
            case 'reset':
                link = '/account/reset'
                break;
            case 'account-confirm':
                link = '/account/confirm'
                break;
            case 'change-email':
                link = '/account/change-email'
                break;
            case 'confirm-email':
                link = '/account/confirm-email'
                break;
            case 'chat':
                link = 'https://pwa.adultsearch.com/chat'
                break;
            case 'recharge':
                link = '/payment/recharge'
                break;
            default:
                link = '#'
        }

        if (params !== false && link !== '#') {
            let queryUrl = Object.keys(params).map(key =>
                key + '=' + params[key]
            ).join('&');

            return `${link}?${queryUrl}`
        } else {
            return link;
        }
    },

    getAccount: async function (noRedirect) {
        let token = this.getToken();

        if (!token) {
            if (!noRedirect) {
                this.gotoSignin();
            }
            return false;
        }

        try {
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
            const res = await axios.get('/api/v1/pc/account');
            return res.data;
        } catch (error) {
            config.catch(error);

            return false;
        }
    }

};
