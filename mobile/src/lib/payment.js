import axios from "../services/api_init";
import config from "./config";

export default {

    getPromocode: function () {
        let input = document.getElementById('promocode');
        if (input) {
            return input.value.trim();
        } else {
            return '';
        }
    },

    getParams: function (accountId) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const promocode = this.getPromocode();

        return {
            placeId: urlParams.get('placeId'),
            escortId: urlParams.get('escortId'),
            paymentType: urlParams.get('paymentType'),
            accountId: accountId,
            promocode: promocode,
            topUps: urlParams.get('topUps'),
            recurringPeriod: (urlParams.get('recurringPeriod')) ? parseInt(urlParams.get('recurringPeriod')) : null,
            ownerPackage: urlParams.get('ownerPackage'),
            locationIds: urlParams.has('locationIds') ? urlParams.get('locationIds').split(',') : [],
            [config.getEscortUpgradeType('stickyWeek')]: urlParams.has(config.getEscortUpgradeType('stickyWeek')) ? urlParams.get(config.getEscortUpgradeType('stickyWeek')).split(',') : [],
            [config.getEscortUpgradeType('stickyMonth')]: urlParams.has(config.getEscortUpgradeType('stickyMonth')) ? urlParams.get(config.getEscortUpgradeType('stickyMonth')).split(',') : [],
            [config.getEscortUpgradeType('sponsor')]: urlParams.has(config.getEscortUpgradeType('sponsor')) ? urlParams.get(config.getEscortUpgradeType('sponsor')).split(',') : [],
            [config.getEscortUpgradeType('side')]: urlParams.has(config.getEscortUpgradeType('side')) ? urlParams.get(config.getEscortUpgradeType('side')).split(',') : [],
        };
    },

    getUrlParam: function (key) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        return urlParams.get(key);
    },

    getBudget: async function () {
        try {
            const res = await axios.get('/api/v1/pc/budget');

            return parseFloat(res.data);
        } catch (error) {
            config.catch(error);

            return false;
        }
    }

};
