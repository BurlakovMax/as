import axios from "../services/api_init";
import db from "../istore";
import config from "./config";

export default {

    getCountries: async function (offset = 0, limit = 100) {
        try {
            const res = await axios.get(`/api/v1/countries?offset=${offset}&limit=${limit}`);

            return res.data.list;
        } catch (error) {
            config.catch(error);

            return false;
        }
    },

    getCountriesWithState: async function (forceUpdate = false) {
        let list = [];
        const locations = await db.locations.toArray();

        if (!forceUpdate && locations.length > 0) {
            list = locations;
        } else {
            config.debug('locations update');

            await db.locations.clear();
            await db.locations_cities.clear();

            try {
                const res = await axios.get('/api/v1/countries_with_state');
                list = res.data;

                // save countries for index accordeon
                await db.locations.bulkPut(list);

                // get cities list for autocomplete search
                let cities = [];
                list.map(country =>
                    this.getCountryStates(country).map(state =>
                        {
                            state.cities.map(city =>
                                {
                                    city.country_code = country.code.toLowerCase();
                                    city.state = state.name;
                                    cities.push(city);
                                    return null;
                                }
                            )
                            return null;
                        }
                    )
                )

                // save cities for autocomplete search
                await db.locations_cities.bulkPut(cities);

                // save date for updates
                await db.locations_update.put({
                    id: 'locations',
                    date: new Date()
                });
            } catch (error) {
                config.catch(error);

                return false;
            }

            config.debug('locations update');
        }

        return list;
    },

    getCountryStates: function (country) {
        return (country.hasStates)? country.states : country.subCountries;
    },

    getUpdates: async function () {
        let date = await db.locations_update
            .where('id').equals('locations')
            .first()

        if (date) {
            let date1 = new Date(date.date);
            let date2 = new Date();
            let hours = Math.abs(date1 - date2) / 36e5;
            config.debug('locations updated', parseInt(hours));

            if (parseInt(hours) > 24) {
                await this.getCountriesWithState(true);
            }
        } else {
            config.debug('locations updated', 'empty');

            await this.getCountriesWithState(true);
        }
    }

};
