import Cookies from 'universal-cookie';

export default {

    /**
     * Save unfinished business payment
     */
    saveCookieFinish: function (params) {
        let cookies = new Cookies();

        cookies.set(
          'businessFinish',
          params,
          {path: '/', expires: new Date(Date.now()+3600), maxAge: (30 * 86400)} // 1 month
        );
    },

    /**
     * Clear unfinished business payment
     */
    clearCookieFinish: function () {
        let cookies = new Cookies();

        cookies.set('businessFinish',[],{path: '/', maxAge: 0})
    },

    getCookieFinish: function () {
        let cookies = new Cookies();
        return cookies.get('businessFinish');
    },
};
