import config from "./config";
import db from "../istore";
import axios from "../services/api_init";

export default {

    // parse event to values
    getEvent: function (event) {
        let filterName, filterValue, filterTitle, filterId;

        // get values from checkbox in modal
        if (event.currentTarget.nodeName === 'INPUT') {
            filterName = event.currentTarget.name; //Ethnicity
            filterValue = event.currentTarget.value; //1
            filterTitle = event.currentTarget.dataset.title; //Asian
            filterId = event.currentTarget.id; //language-5
        }

        // get values from div in options list
        if (event.currentTarget.nodeName === 'DIV') {
            filterName = event.currentTarget.dataset.name; //Ethnicity
            filterValue = event.currentTarget.dataset.value; //1
            filterTitle = event.currentTarget.dataset.title; //Asian
            filterId = event.currentTarget.dataset.id; //language-5
        }

        console.log(event.currentTarget.nodeName, [filterName, filterValue, filterTitle, filterId]);

        return [filterName, filterValue, filterTitle, filterId];
    },

    // highlight selected parent
    heighLight: function (event) {
        let [filterName, filterValue, filterTitle, filterId] = this.getEvent(event);

        let parent = document.getElementById(`${filterId}-parent`);
        if (parent) {
            parent.classList.add('active');
        }
    },

    // ADD FILTER
    add: function (event, filtersSelected) {
        let [filterName, filterValue, filterTitle, filterId] = this.getEvent(event);

        config.debug('FILTER ADD', `${filterName} [${filterTitle}]`);

        let list = [];
        let values = [];
        let ids = [];

        // add filter - list of objects
        if (filtersSelected[filterName] !== undefined) {
            list = filtersSelected[filterName];
            values = filtersSelected[filterName+'Values'];
            ids = filtersSelected[filterName+'Ids'];
        }
        list.push({
            'filter': filterName.charAt(0).toLowerCase() + filterName.slice(1), // first char to lowercase
            'option' : filterTitle,
            'value': (['Payments','Services'].includes(filterName))? filterValue : parseInt(filterValue), // check if value is digit or string
        })
        // add filter - list of values
        values.push(filterTitle);

        // add filter - list of ids (can be strings or digits)
        (['Payments','Services'].includes(filterName))? ids.push(filterValue) : ids.push(parseInt(filterValue));

        filtersSelected[filterName] = list;
        filtersSelected[filterName+'Values'] = values;
        filtersSelected[filterName+'Ids'] = ids;

        return filtersSelected;
    },

    // REMOVE FILTER
    delete: function (event, filtersSelected) {
        let [filterName, filterValue, filterTitle, filterId] = this.getEvent(event);

        config.debug('FILTER REMOVE', `${filterName} [${filterTitle}]`);

        let newFilterList = filtersSelected[filterName].filter(
            filter => {
                return filter.value.toString() !== filterValue;
            }
        )
        let newFilterIds = filtersSelected[filterName + 'Ids'].filter(
            filter => {
                return filter.toString() !== filterValue;
            }
        )
        let newFilterValues = filtersSelected[filterName + 'Values'].filter(
            filter => {
                return filter !== filterTitle;
            }
        )

        if (newFilterList.length > 0) {
            filtersSelected[filterName] = newFilterList;
            filtersSelected[filterName + 'Ids'] = newFilterIds;
            filtersSelected[filterName + 'Values'] = newFilterValues;
        } else {
            delete filtersSelected[filterName];
            delete filtersSelected[filterName + 'Ids'];
            delete filtersSelected[filterName + 'Values'];
        }

        return filtersSelected;
    },

    getOptions: function (filtersSelected) {
        let filters = [];

        Object.keys(filtersSelected).map((key) =>
            {
                let filter = filtersSelected[key];
                filter.map(option => {
                    if (typeof option === 'object') {
                        filters.push(option);
                    }
                    return null;
                })
                return null;
            }
        )

        return filters;
    },

    count: function (filtersSelected, countOptions = false) {
        let count = 0;

        if (countOptions) {
            // count filter items
            let options = this.getOptions(filtersSelected);
            count = options.length;

        } else {
            // count filter groups
            for (let key in filtersSelected) {
                if (key.includes('Ids')) {
                    count++
                }
            }
        }

        return count;
    },

    exist: function (event, filtersSelected) {
        let exist = false;
        let [filterName, filterValue, filterTitle, filterId] = this.getEvent(event);

        if (filtersSelected[filterName]) {
            exist = filtersSelected[filterName].some(
                filter => {
                    return filter.value.toString() === filterValue;
                }
            )
        }

        return exist;
    },

    search: function (escorts, filtersSelected) {
        let escortsFilter = escorts.filter(escort => {
            let foundEthnicity, foundEyeColor, foundHairColor, foundLanguage, foundPayments, foundServices;

            if (filtersSelected['Ethnicity']) {
                if (filtersSelected['EthnicityValues'].includes(escort['ethnicity'])) {
                    foundEthnicity = true;
                }
            } else {
                foundEthnicity = true;
            }

            if (filtersSelected['EyeColor']) {
                if (filtersSelected['EyeColorValues'].includes(escort['eyeColor'])) {
                    foundEyeColor = true;
                }
            } else {
                foundEyeColor = true;
            }

            if (filtersSelected['HairColor']) {
                if (filtersSelected['HairColorValues'].includes(escort['hairColor'])) {
                    foundHairColor = true;
                }
            } else {
                foundHairColor = true;
            }

            if (filtersSelected['Language']) {
                let intersection = filtersSelected['LanguageIds'].filter(x => escort['languageIds'].includes(x));
                foundLanguage = intersection.length;
            } else {
                foundLanguage = true;
            }

            if (filtersSelected['Payments']) {
                let escortPayments = [];
                if (escort.isAmexAccepted) escortPayments.push('isAmexAccepted');
                if (escort.isVisaAccepted) escortPayments.push('isVisaAccepted');
                if (escort.isDiscoverAccepted) escortPayments.push('isDiscoverAccepted');
                let intersection = filtersSelected['PaymentsIds'].filter(x => escortPayments.includes(x));
                foundPayments = intersection.length;
            } else {
                foundPayments = true;
            }

            let foundServicesAll = 0;
            if (filtersSelected['Services']) {
                filtersSelected['ServicesIds'].map(
                    filter => {
                        if (escort[filter]) {
                            foundServicesAll++
                        }
                        return null;
                    }
                )
                foundServices = !!(foundServicesAll);
            } else {
                foundServices = true;
            }

            return (foundEthnicity && foundEyeColor && foundHairColor && foundLanguage && foundPayments && foundServices);
        });

        return escortsFilter;
    },

    // clear old filters
    dbClearSelectedFilters: async function (categoryId) {
        await db.escort_filters_selected.delete(categoryId);
    },

    // save new filters
    dbSaveSelectedFilters: async function (categoryId, filters) {
        let filtersDb = {
            id: categoryId,
            data: filters
        };
        await db.escort_filters_selected.put(filtersDb);
    },

    dbGetSelectedFilters: async function (categoryId) {
        let filtersDb = await db.escort_filters_selected
            .where('id').equals(categoryId)
            .first()

        return (filtersDb)? filtersDb.data : {};
    },

    dbGetFilters: async function () {
        let filters = await db.escort_filters
            .toCollection()
            .toArray();

        if (filters.length === 0) {
            try {
                const url = '/api/v1/escort_filters';
                const res = await axios.get(url);
                const keys = Object.keys(res.data);
                keys.forEach(key => {
                    filters.push({name: key, values: res.data[key]});
                });

                await db.escort_filters.bulkPut(filters);
            } catch (error) {
                config.catch(error);
            }
        }

        return filters;
    }

}
