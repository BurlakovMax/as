import Cookies from 'universal-cookie';
import axios from "../services/api_init";
import auth from "./auth";
import db from "../istore";
import {history} from "../index";
import config from "./config";

export default {

    /**
     * Validate available to
     */
    validateAvailable: function () {
        let error = true;

        let parent = document.getElementById('available-to');
        let inputs = parent.getElementsByTagName('input');

        for (let item of inputs) {
            if (item.checked || item.dataset.checked) {
                error = false;
            }
        }

        if (error) {
            const y = (parent.getBoundingClientRect().top + window.pageYOffset) - 100;
            window.scrollTo({top: y, behavior: 'smooth'});
            return 'You have to pick at least 1 option';
        } else {
            return false;
        }
    },

    /**
     * Validate incall/outcall
     */
    validateCall: function () {
        let error = true;

        let incall = document.getElementById('incall');
        let outcall = document.getElementById('outcall');

        if ((incall.checked || incall.dataset.checked) || (outcall.checked || outcall.dataset.checked)) {
            error = false;
        }

        if (error) {
            const y = (document.getElementById('callRates').getBoundingClientRect().top + window.pageYOffset) - 170;
            window.scrollTo({top: y, behavior: 'smooth'});
            return 'You must select either outcall and/or incall';
        } else {
            return false;
        }
    },

    clearCookie: function (clearFinish) {
        let cookies = new Cookies();

        let keys = ['adbuildLocations','adbuildType','adbuildId','adbuildPhone','adbuildAccount'];

        if (clearFinish) {
            keys.push('adbuildFinish')
        }

        keys.map(key => {
            cookies.set(key,[],{path: '/', maxAge: 0});
            return null;
        })
    },

    loadCookie: function () {
        let cookies = new Cookies();
        let data = {
            'token' : false,
            'account' : false,
            'phone' : false,
            'escortId' : false,
            'locations' : [],
            'type' : {},
            'total' : 0,
        };

        // load token
        let token = cookies.get('auth');
        if (token !== undefined) {
            data.token = token
        }

        // load phone
        let phone = cookies.get('adbuildPhone');
        if (phone !== undefined) {
            data.phone = phone
        }

        // load accountId
        let account = cookies.get('adbuildAccount');
        if (account !== undefined) {
            data.account = account
        }

        // load escort id
        let escortId = cookies.get('adbuildId');
        if (escortId !== undefined) {
            data.escortId = escortId
        }

        // load locations
        let locations = cookies.get('adbuildLocations');
        if (typeof locations === 'object') {
            data.locations = locations
        }

        // load type id => name
        let type = cookies.get('adbuildType');
        if (typeof type === 'object') {
            data.type = type;
        }

        // count total for locations prices
        for (let key in data.locations) {
            let loc = data.locations[key];
            if (loc && loc.price) {
                data.total += parseFloat(loc.price)
            }
        }

        return data;
    },

    /**
     * save locations data to cookies
     * @param summary array
     * @param price mixed, if empty - we save only summary
     */
    saveCookieLocations: function (summary, price) {
        let locations = [];

        if (price) {
            for (let key in summary) {
                let item = summary[key];
                let loc = {
                    'id' : item.id,
                    'name' : item.name,
                    'state' : item.stateName,
                    'price' : price,
                    'upgrade' : false,
                };
                locations.push(loc);
            }
        } else {
            locations = summary;
        }

        let cookies = new Cookies();
        cookies.set(
          'adbuildLocations',
          locations,
          {path: '/', expires: new Date(Date.now()+3600), maxAge: 3600} // 1 hour
        );
    },

    /**
     * Save escort id data for step 4
     * @param escortId
     */
    saveCookieEscortId : function (escortId) {
        let cookies = new Cookies();
        cookies.set(
          'adbuildId',
          escortId,
          {path: '/', expires: new Date(Date.now()+3600), maxAge: 3600} // 1 hour
        );
    },

    /**
     * Save escort pe id data for step 3
     * @param type
     */
    saveCookieType : function (type) {
        let cookies = new Cookies();

        if (type && typeof type === 'object') {
            // save
            cookies.set(
                'adbuildType',
                type,
                {path: '/', expires: new Date(Date.now()+3600), maxAge: 3600} // 1 hour
            );
        } else {
            // clear
            cookies.set('adbuildType',[],{path: '/', maxAge: 0});
        }
    },

    /**
     * Save unfinished payment
     * @param escortId
     */
    saveCookieFinish : function (escortId, accountId) {
        let cookies = new Cookies();

        if (!accountId) {
            accountId = 0;
        }

        cookies.set(
          'adbuildFinish',
          {'escortId': escortId, 'accountId': accountId},
          {path: '/', expires: new Date(Date.now()+3600), maxAge: (30 * 86400)} // 1 month
        );
    },

    /**
     * Check if user has unfinished escort payment
     * @param accountId
     */
    checkCookieFinish: function (accountId) {
        let cookies = new Cookies();
        let finish = cookies.get('adbuildFinish');

        if (accountId && finish !== undefined) {
            this.saveCookieFinish(finish.escortId, accountId)
        }

        return finish;
    },

    /**
     * Save phone data for step 4
     * @param phone
     */
    saveCookiePhone : function (phone) {
        let cookies = new Cookies();
        cookies.set(
          'adbuildPhone',
          phone,
          {path: '/', expires: new Date(Date.now()+3600), maxAge: 3600} // 1 hour
        );
    },

    /**
     * check for unfinished escort payment
     * @param accountId
     * @returns {Promise<void>}
     */
    checkFinish: async function (accountId) {
        let finish = this.checkCookieFinish(accountId);

        if (finish && finish.escortId) {
            try {
                await axios.post('/api/v1/link_escort_to_account', {
                    'escortId': finish.escortId,
                    'accountId': accountId,
                });
            } catch (error) {
                config.catch(error);

                // clear adbuild cookie if we fail to assign ad to user
                this.clearCookie(true);
            }
        }
    },

    saveAccount : function (account) {
        let cookies = new Cookies();
        cookies.set(
          'adbuildAccount',
          account,
          {path: '/', expires: new Date(Date.now()+3600), maxAge: 3600} // 1 hour
        );
    },

    /**
     * Create url for payment form
     * @param escortId
     * @param src
     * @returns {string} /payment/pay?paymentType=combo&escortId=1896991&src=escort&locationIds=1279
     * &escortSponsorLocationIds=1279
     * &escortSideLocationIds=1279
     * &escortStickyWeekLocationIds=1279 || &escortStickyMonthLocationIds=1279
     */
    getPaymentUrl: function (escortId = false, src = 'escort') {
        let data = this.loadCookie();

        // escort has upgrades
        let hasUpgrades = false;

        // create ids for locations & upgrades
        let locations = {
            'locationIds' : []
        };

        for (let key in data.locations) {
            let item = data.locations[key];
            if (item && typeof item === 'object') {
                if (!item.upgrade) {
                    // base cities
                    locations['locationIds'].push(item.id)
                } else {
                    // upgrades
                    if(locations.hasOwnProperty(item.upgrade) === false) {
                        locations[item.upgrade] = [];
                        hasUpgrades = true;
                    }
                    locations[item.upgrade].push(item.id)
                }
            }
        }

        let locationsUrl = '';
        for (let type in locations) {
            locationsUrl += '&' + type + '=' + locations[type].join(',');
        }

        let params = {
            'paymentType': (hasUpgrades) ? 'combo' : 'escort',
            'escortId': (escortId)? escortId : data.escortId,
        };

        if (src) {
            params['src'] = src;
        }

        let queryUrl = Object.keys(params).map(key =>
          key + '=' + params[key]
        ).join('&');

        return `/payment/pay?${queryUrl}${locationsUrl}`;
    },

    /**
     * Check how many locations user can select on step 2
     * @param account Account object
     * @returns {{limit: boolean, error: boolean}}
     */
    getCitiesLimit: function (account) {
        let limit = false;
        let error = false;
        let user = (account && Object.keys(account).length > 0)? account : false;

        if (!user) {
            limit = 1;
            error = 'You are allowed to post one ad only in 1 location, unless you are proven advertiser. Please log in to your account or contact us at support@adultsearch.com';
        } else if (user && !user.whitelisted) {
            limit = 1;
            error = 'You are allowed to post one ad only in 1 location, unless you are proven advertiser. If you need to post in more locations, please contact us at support@adultsearch.com';
        } else if (user && user.whitelisted) {
            limit = false;
        }

        return {
            'limit' : limit,
            'error' : error,
        }
    },



    getListAge: function () {
        let list = [];

        for (let i = 21; i < 70; i++) {
            list[i] = i;
        }

        return list;
    },

    getListEthnicity: async function () {
        let list = [];
        let key = 'ethnicity';

        let local = await db.escort_sprav
          .where('name').equals(key)
          .toArray();

        if (local.length > 0) {
            list = local[0].data;
        } else {
            try {
                const res = await axios.get(`/api/v1/options/ethnicity`);
                list = res.data;

                await db.escort_sprav.bulkPut([{'name': key, 'data': list}]);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getListEyeColor: async function () {
        let list = [];
        let key = 'eye_color';

        let local = await db.escort_sprav
          .where('name').equals(key)
          .toArray();

        if (local.length > 0) {
            list = local[0].data;
        } else {
            try {
                const res = await axios.get(`/api/v1/options/eye_color`);
                list = res.data;

                await db.escort_sprav.bulkPut([{'name': key, 'data': list}]);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getListKitty: async function () {
        let list = [];
        let key = 'kitty';

        let local = await db.escort_sprav
          .where('name').equals(key)
          .toArray();

        if (local.length > 0) {
            list = local[0].data;
        } else {
            try {
                const res = await axios.get(`/api/v1/options/kitty`);
                list = res.data;

                await db.escort_sprav.bulkPut([{'name': key, 'data': list}]);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getListPenisSize: function () {
        let list = [
            {'value': 3, 'text': `3'`},
            {'value': 4, 'text': `4'`},
            {'value': 5, 'text': `5'`},
            {'value': 6, 'text': `6'`},
            {'value': 7, 'text': `7'`},
            {'value': 8, 'text': `8'`},
        ];

        return list;
    },

    getListReply: function () {
        let list = [
            {'value': 1, 'text': 'Yes, make my email anonymous and  forward  inquiries to me.', 'selected': true},
            {'value': 3, 'text': 'Yes, include the email address above.'},
            {'value': 2, 'text': 'No, I don`t want to receive any email inquiries.'},
        ];

        return list;
    },

    getListCupSize: async function () {
        let list = [];
        let key = 'cupsize';

        let local = await db.escort_sprav
          .where('name').equals(key)
          .toArray();

        if (local.length > 0) {
            list = local[0].data;
        } else {
            try {
                const res = await axios.get(`/api/v1/options/cupsize`);
                list = res.data;

                await db.escort_sprav.bulkPut([{'name': key, 'data': list}]);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },


    getListHairColor: async function () {
        let list = [];
        let key = 'hair_color';

        let local = await db.escort_sprav
          .where('name').equals(key)
          .toArray();

        if (local.length > 0) {
            list = local[0].data;
        } else {
            try {
                const res = await axios.get(`/api/v1/options/hair_color`);
                list = res.data;

                await db.escort_sprav.bulkPut([{'name': key, 'data': list}]);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },


    getListBuild: async function () {
        let list = [];
        let key = 'build';

        let local = await db.escort_sprav
          .where('name').equals(key)
          .toArray();

        if (local.length > 0) {
            list = local[0].data;
        } else {
            try {
                const res = await axios.get(`/api/v1/options/build`);
                list = res.data;

                await db.escort_sprav.bulkPut([{'name': key, 'data': list}]);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },


    getListHeightFeet: function () {
        return [2, 3, 4, 5, 6, 7];
    },


    getListHeightInches: function () {
        return [
            {value: 411, name: `< 5' (< 152 cm)`},
            {value: 500, name: `5' (152 cm)`},
            {value: 501, name: `5'1" (154 cm)`},
            {value: 502, name: `5'2" (157 cm)`},
            {value: 503, name: `5'3" (160 cm)`},
            {value: 504, name: `5'4" (162 cm)`},
            {value: 505, name: `5'5" (165 cm)`},
            {value: 506, name: `5'6" (167 cm)`},
            {value: 507, name: `5'7" (170 cm)`},
            {value: 508, name: `5'8" (172 cm)`},
            {value: 509, name: `5'9" (175 cm)`},
            {value: 510, name: `5'10" (177 cm)`},
            {value: 511, name: `5'11" (180 cm)`},
            {value: 600, name: `6' (182 cm)`},
            {value: 601, name: `6'1" (185 cm)`},
            {value: 602, name: `6'2" (187 cm)`},
            {value: 603, name: `6'3" (190 cm)`},
            {value: 604, name: `6'4" (193 cm)`},
            {value: 605, name: `6'5" (195 cm)`},
            {value: 606, name: `6'6" (198 cm)`},
            {value: 607, name: `> 6'6" (> 200 cm)`},
        ];
    },

    getListLanguage: async function () {
        let list = [];
        let key = 'languages';

        let local = await db.escort_sprav
          .where('name').equals(key)
          .toArray();

        if (local.length > 0) {
            list = local[0].data;
        } else {
            try {
                const res = await axios.get(`/api/v1/options/languages`);
                list = res.data;

                await db.escort_sprav.bulkPut([{'name': key, 'data': list}]);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    /**
     * Go to payment page
     * clear cookies must be at the end !!!
     */
    gotoPayment: function (escortId, accountId, src = false) {
        let url = '/';

        if (accountId) {
            url = this.getPaymentUrl(escortId, src);

            // clear all adbuild steps cookies
            this.clearCookie();
        } else {
            // save unfinished escort
            this.saveCookieFinish(escortId);

            auth.gotoSignin();
            return;
        }

        history.push(url);
    },

    /**
     * Remove city in summary (by close icon)
     * @param event
     */
    removeSummaryCity: function (summary, event) {
        event.preventDefault();

        let id = event.currentTarget.dataset.id;
        let upgrade = event.currentTarget.dataset.upgrade;

        // check last basic city
        let count = 0;
        for (let key in summary) {
            let item = summary[key];
            if (!item.upgrade || item.upgrade === false) {
                count++
            }
        }

        if (!upgrade && count === 1) {
            alert('Cannot remove last city');
            return summary;
        }

        if (upgrade) {
            // delete upgrade
            for (let key in summary) {
                let item = summary[key];
                if (item.id === parseInt(id) && item.upgrade === upgrade) {
                    delete summary[key];
                }
            }

            // try to uncheck top upgrade list
            let checkbox = document.getElementById(`${upgrade}_${id}`);
            if (checkbox) {
                checkbox.checked = false;
            }

            // uncheck sticky radio
            if (upgrade.includes('Sticky')) {
                let labelId = `sticky_location_${id}`;
                let parent = document.getElementById(labelId);

                let inputs = parent.getElementsByTagName('input');
                for (let item of inputs) {
                    item.checked = false;
                }

                let labels = parent.getElementsByTagName('label');
                for (let item of labels) {
                    item.classList.remove('active');
                }
            }

        } else {
            // delete city
            for (let key in summary) {
                let item = summary[key];
                if (item.id === parseInt(id) && item.upgrade === false) {
                    delete summary[key];
                }
            }
        }

        return summary;
    }

};
