import axios from "../services/api_init";
import config from "./config";

export default {

    getPhoneInputValue: function () {
        let phoneDiv = document.getElementById('phone');
        let phoneStr = phoneDiv.getElementsByTagName('input')[0].value;

        let phone = phoneStr.replace(/[\+\s-\(\)]/g, '');

        return phone;
    },

    setPhoneInputError: function (error) {
        let phoneDiv = document.getElementById('phone');

        if (error) {
            phoneDiv.classList.add('error');
            const y = (phoneDiv.getBoundingClientRect().top + window.pageYOffset) - 60;
            window.scrollTo({top: y, behavior: 'smooth'});
        } else {
            phoneDiv.classList.remove('error');
        }
    },

    phoneCheck: async function (phoneNumber) {
        try {
            const res = await axios.get(`/api/v1/is_phone_verified?phone=${phoneNumber}`);

            return {
                valid: res.data,
                error: false,
            };
        } catch (error) {
            config.catch(error);

            return {
                valid: false,
                error: error,
            };
        }
    },

};
