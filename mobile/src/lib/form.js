import config from "./config";
import phone from "./phone";

export default {

    /**
     * Validate common inputs & selects
     * @returns {}
     */
    validateErrors: function (errors) {
        let count = 0;

        for (let id in errors) {
            let input = document.getElementById(id);

            if (input.value) {
                if (input.dataset.pattern) {
                    // check by data-pattern
                    let regexp = new RegExp(`^${input.dataset.pattern}$`);
                    config.debug(`${id} [${input.value}]`, regexp);

                    if (regexp.test(input.value)) {
                        errors[id] = false
                    } else {
                        count++
                    }
                } else {
                    // simple check by string value
                    errors[id] = false
                }
            } else {
                count++
            }
        }

        // scroll up to error input
        for (let id in errors) {
            if (errors[id]) {
                const y = (document.getElementById(id).getBoundingClientRect().top + window.pageYOffset) - 170;
                window.scrollTo({top: y, behavior: 'smooth'});
                break;
            }
        }

        return [errors, count];
    },

    validatePictures: function (picturesCount, minCount = 1) {
        let error = false;

        if (!picturesCount) {
            error = `Please upload minimum ${minCount} `;
            error += (minCount === 1)? 'photo' : 'photos';

            const y = (document.getElementById('photos').getBoundingClientRect().top + window.pageYOffset) - 60;
            window.scrollTo({top: y, behavior: 'smooth'});
        }

        return error;
    },

    /**
     * url: https://www.w3resource.com/javascript/form/email-validation.php
     * @param email
     * @returns {boolean}
     */
    validateEmail: function (email, focus = true) {
        let error = false;

        if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,6})+$/.test(email)) {
            return error;
        } else {
            if (focus) {
                let emailInput = document.getElementById('email');
                if (emailInput) {
                    emailInput.focus();
                }
            }

            error = 'Please use valid email address';
            return error;
        }
    },

    validatePhone: function (minLength = 8) {
        let error = false;
        phone.setPhoneInputError(false);

        let phoneNumber = phone.getPhoneInputValue();
        if (!phoneNumber || phoneNumber.length < minLength) {
            phone.setPhoneInputError(true);
            error = 'You must enter valid phone number';
        }

        return error;
    }

};
