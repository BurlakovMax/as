import axios from "../services/api_init";
import config from "./config";

export default {

    /**
     * Delete photo in state & api
     * @param event
     * @param pictures
     * @param limit
     * @param parentId
     * @returns {Promise<{limit: boolean, list: *, error: boolean}>}
     */
    deletePhoto: async function (event, pictures, limit = 0, parentId = 0) {
        let result = {
            'list': pictures,
            'limit': false,
            'error': false,
        };

        let ok = false;
        let type = event.currentTarget.dataset.type;
        let index = event.currentTarget.dataset.index;
        let id = event.currentTarget.dataset.id;
        let name = event.currentTarget.dataset.name;

        // type is required for processing
        if (!type) {
            result.error = 'Parent [type] not defined';
            return result;
        }

        if (type === 'escort') {
            // deleting existing images (by name) - escort edit
            if (id === 'local') {
                try {
                    await axios.delete(`/api/v1/pc/escorts/${parentId}/images`, {
                        data: {
                            images: [name]
                        }
                    });
                    ok = true;
                } catch (error) {
                    config.catch(error);

                    result.error = 'Error deleting photo';
                }
            } else {
                // deleting temporary images (by id) - escort step3
                try {
                    await axios.delete(`/api/v1/escorts/image_delete`, {
                        data: {
                            id: id
                        }
                    });
                    ok = true;
                } catch (error) {
                    config.catch(error);

                    result.error = 'Error deleting photo';
                }
            }
        }

        // TODO: delete place photo - need api
        if (type === 'place') {
            console.log('cannot delete image', type);
        }

        if (ok) {
            // updating pictures list & return
            result.list.splice(parseInt(index), 1);
        }

        // check limit flag
        if (parseInt(limit) > 0 && result.list.length >= parseInt(limit)) {
            result.limit = true;
            result.error = 'Maximum photo count reached.';
        }

        // clear input file value
        let input = document.getElementById('photo');
        if (input) {
            input.value = '';
        }

        return result;
    },

    uploadPhoto: async function (event, pictures, limit = 0) {
        let type = event.currentTarget.dataset.type;
        let result = {
            'list': pictures,
            'limit': false,
            'error': false,
        };

        // check limit
        if (parseInt(limit) > 0 && result.list.length >= parseInt(limit)) {
            result.limit = true;
            result.error = 'Maximum photo count reached.';
            return result;
        }

        // type is required for processing
        if (!type) {
            result.error = 'Parent [type] not defined';
            return result;
        }

        // returned res.data === object
        if (type === 'escort') {
            try {
                const formData = new FormData();
                formData.append('image', event.target.files[0])

                let config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                };
                let res = await axios.post('/api/v1/escorts/image_upload', formData, config);

                if (res.data.id) {
                    // updating pictures list & return
                    result.list.push(res.data)
                } else {
                    result.error = config.getUploadError('image');
                }
            } catch (error) {
                config.catch(error);

                result.error = config.getUploadError('image');
                if (error.response.status === 400 || error.response.status === 413) {
                    result.error = config.getUploadError('image', error.response.status);
                }
            }
        }

        // returned res.data === string (img url)
        if (type === 'wire') {
            try {
                const formData = new FormData();
                formData.append('image', event.target.files[0])

                let config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                };
                let res = await axios.post('/api/v1/pc/account/wire_upload', formData, config);

                if (res.data) {
                    let imageUrl = res.data;
                    let data = imageUrl.split('/');
                    let imageName = data[data.length - 1];

                    // updating pictures list & return
                    let image = {
                        id: imageName.split('.').slice(0, -1).join('.'),
                        name: imageName,
                        url: imageUrl,
                        readonly: true, // cannot delete
                    };
                    result.list.push(image)
                } else {
                    result.error = config.getUploadError('image');
                }
            } catch (error) {
                config.catch(error);

                result.error = config.getUploadError('image');
                if (error.response.status === 400 || error.response.status === 413) {
                    result.error = config.getUploadError('image', error.response.status);
                }
            }
        }

        // returned res.data.image === string (img url)
        if (type === 'place') {
            try {
                const formData = new FormData();
                formData.append('image', event.target.files[0])

                let config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                };
                let res = await axios.post('/api/v1/placeImageUpload', formData, config);

                if (res.data) {
                    let imageUrl = res.data.image;
                    let data = imageUrl.split('/');
                    let imageName = data[data.length - 1];

                    // updating pictures list & return
                    let image = {
                        id: imageName.split('.').slice(0, -1).join('.'),
                        name: imageName,
                        url: imageUrl,
                        readonly: true, // cannot delete
                    };
                    result.list.push(image)
                } else {
                    result.error = config.getUploadError('image');
                }
            } catch (error) {
                config.catch(error);

                result.error = config.getUploadError('image');
                if (error.response.status === 400 || error.response.status === 413) {
                    result.error = config.getUploadError('image', error.response.status);
                }
            }
        }

        // check limit flag
        if (parseInt(limit) > 0 && result.list.length >= parseInt(limit)) {
            result.limit = true;
            result.error = 'Maximum photo count reached.';
        }

        // clear input file value
        let input = document.getElementById('photo');
        if (input) {
            input.value = '';
        }

        return result;
    },

};
