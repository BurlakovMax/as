import db from "../istore";

export default {

    saveScroll: async function (offset = false, limit = false) {
/*        if (window.pageYOffset === 0) {
            return;
        }*/

        let data = {
            'id': 'scroll',
            'top': window.pageYOffset,
            'offset': offset,
            'limit': limit,
        };

        await db.position.put(data);
    },

    getScrollLocal: async function () {
        let data = await db.position
          .where({'id': 'scroll'})
          .toArray();
        return (data.length)? data[0] : false;
    },

    getScroll: async function () {
        let page = await this.getPage();
        if (page === 'escort') {
            await this.savePage('list');

            let data = await db.position
              .where({'id': 'scroll'})
              .toArray();
            if (data.length) {
                return data[0];
            }
        }
        return null;
    },

    clear: async function (id) {
        await db.position.delete(id);
    },

    savePage: async function (page) {
        let data = {
            'id': 'page',
            'page': page,
        };

        await db.position.put(data);
    },

    getPage: async function () {
        let data = await db.position
          .where({'id': 'page'})
          .toArray();
        return (data.length)? data[0].page : false;
    },
};
