if (typeof importScripts === 'function') {
    importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js');

    const CACHE_NAME = 'offline-html';
    const FALLBACK_HTML_URL = '/offline.html';
    self.addEventListener('install', async (event) => {
        event.waitUntil(
            caches.open(CACHE_NAME)
                .then((cache) => cache.add(FALLBACK_HTML_URL))
        );
    });

    workbox.setConfig({ debug: false });
    workbox.core.skipWaiting();
    workbox.core.clientsClaim();

    workbox.core.setCacheNameDetails({
        prefix: 'asng-app',
        suffix: 'v1'
    });

    const showNotification = () => {
        self.registration.showNotification('Post Sent', {
            body: 'You are back online and your post was successfully sent!',
            icon: 'logo512.png',
            badge: 'favicon-32x32.png'
        });
    };

    const bgSyncPlugin = new workbox.backgroundSync.BackgroundSyncPlugin('asQueue', {
        maxRetentionTime: 24 * 60, // Retry for max of 24 Hours
        callbacks: {
            queueDidReplay: showNotification
        }
    });

    workbox.routing.setDefaultHandler(new workbox.strategies.StaleWhileRevalidate());

    workbox.routing.setCatchHandler(({event}) => {
        // https://medium.com/dev-channel/service-worker-caching-strategies-based-on-request-types-57411dd7652c
        console.log(event.request.destination);
        switch (event.request.destination) {
            case 'document':
                return caches.open(CACHE_NAME).then(cache => cache.match(FALLBACK_HTML_URL));
            case 'image':
                return Response.error();
            case 'font':
                return Response.error();
            default:
                return Response.error();
        }
    });

    workbox.routing.registerRoute(
        new RegExp('.*offline.*'),
        new workbox.strategies.StaleWhileRevalidate()
    );

    workbox.routing.registerRoute(
        new RegExp('.*build.*'),
        new workbox.strategies.StaleWhileRevalidate()
    );

    workbox.routing.registerRoute(
        /.*(?:png|gif|jpg|jpeg|svg)$/,
        new workbox.strategies.CacheFirst({
            cacheName: 'asng-images',
            plugins: [
                new workbox.expiration.ExpirationPlugin({
                    maxEntries: 60,
                    maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
                }),
            ],
        }),
    );

    workbox.routing.registerRoute(
        /.*(?:js|css)$/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'asng-static-resources',
        })
    );

    workbox.routing.registerRoute(
        new RegExp('.*payment.*'),
        new workbox.strategies.NetworkOnly()
    );

    workbox.routing.registerRoute(new RegExp('.*account.*'), new workbox.strategies.NetworkOnly());
    workbox.routing.registerRoute(new RegExp('account/signup$'), new workbox.strategies.NetworkOnly());
    workbox.routing.registerRoute(
        new RegExp('account/signup$'),
        new workbox.strategies.NetworkOnly({
            plugins: [bgSyncPlugin]
        })
    );
    workbox.routing.registerRoute(new RegExp('account/signin$'), new workbox.strategies.NetworkOnly());
    workbox.routing.registerRoute(
        new RegExp('account/signin$'),
        new workbox.strategies.NetworkOnly({
            plugins: [bgSyncPlugin]
        })
    );
    workbox.routing.registerRoute(new RegExp('account/logout$'), new workbox.strategies.NetworkOnly());

    workbox.routing.registerRoute(
        new RegExp('.*adbuild.*'),
        new workbox.strategies.NetworkFirst()
    );

    workbox.routing.registerRoute(/.*myposts.*/, ({url, event, params}) => {
        return fetch(event.request).then(response => {
            return response;
        }).catch(err => {
            console.log(err);
        });
    });

    workbox.routing.registerRoute(
        /\/api\/.*\/*.json/,
        new workbox.strategies. NetworkOnly({
            plugins: [bgSyncPlugin]
        })
    );

    workbox.routing.registerRoute(new RegExp('.*api.*'), new workbox.strategies.NetworkOnly());
    workbox.routing.registerRoute(new RegExp('.*mng.*'), new workbox.strategies.NetworkOnly());

    workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);
}