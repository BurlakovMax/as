import { createStore, createEvent } from 'effector';

export const changeTitle = createEvent('Change title');
export const title = createStore(null).on(changeTitle, (state, payload) => payload);

export const setAccount = createEvent('Set global account');
export const account = createStore({}).on(setAccount, (state, payload) => payload);

export const setLocation = createEvent('Set global location');
export const location = createStore({}).on(setLocation, (state, payload) => payload);

export const setLanguage = createEvent('Set global language');
export const language = createStore({}).on(setLanguage, (state, payload) => payload);

export const setOffline = createEvent('Set offline mode');
export const offline = createStore(false).on(setOffline, (state, payload) => payload);
