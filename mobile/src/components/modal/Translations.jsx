import React, {Component} from 'react';
import {Link} from "react-router-dom";
import InputSearch from "../../components/form/input-search";
import $ from "jquery";
import Modal from "../../components/common/modal";
import Cookies from 'universal-cookie';
import {setLanguage} from "../../store/common";
import geo from "../../lib/geo";

export default class Translations extends Component {
    constructor(props) {
        super(props);

        this.state = {
            languages: [],
            languagesList: geo.getLanguagesList(),
            countriesList: geo.getCountriesList(),

            languageActive: '',
            languageDefault: 'en',

            cookieName: 'googtrans',

            searchText: '',
            searchResults: [],
            searchEmpty: true,
        };
    }

    setLanguage = (active) => {
        this.setState({
            languageActive: active
        });

        // set global language for header
        setLanguage({language: active.toUpperCase()});
    }

    setCookies = (active) => {
        let cookies = new Cookies();
        let hostElements = window.location.hostname.split('.').reverse();
        let rootDomain = hostElements.length > 2 ? `${hostElements[1]}.${hostElements[0]}` : window.location.hostname;

        // remove lang cookies
        cookies.remove(this.state.cookieName,{
            path: '/',
            //sameSite: 'none',
            domain: rootDomain,
        });
        cookies.remove(this.state.cookieName,{
            path: '/',
            //sameSite: 'none',
        });

        // add lang cookies
        if (active !== this.state.languageDefault) {
            cookies.set(
              this.state.cookieName,
              `/${this.state.languageDefault}/${active}`,
              {
                  path: '/',
                  //sameSite: 'none',
                  //expires: new Date(Date.now()+3600), maxAge: 3600 // no expire, just browser session
              }
            );
        }
    }

    getLanguage = () => {
        let cookies = new Cookies();
        let active = this.state.languageDefault;

        let language = cookies.get(this.state.cookieName);
        if (language !== undefined) {
            active = language.split('/').pop();
        }

        this.setLanguage(active);
    }

    getLanguages = () => {
        const languages = [];
        const languagesCodesArr = Object.keys(this.state.languagesList);

        // get all langs
        for (let i = 0; i < languagesCodesArr.length; i += 1) {
            const code = languagesCodesArr[i];
            const {name, country} = this.state.languagesList[code];

            if (name) {
                const {emoji} = this.state.countriesList[country];

                languages.push({
                    id: code,
                    name: name,
                    emoji,
                    active: code === this.state.languageActive
                });
            }
        }

        this.setState({
            languages: languages,
        });

        this.getLanguagesDefault(languages);
    }

    // copy fav langs to top
    getLanguagesDefault = (languages) => {
        let list = [...languages];

        list.unshift({
            id: 'es',
            name: this.state.languagesList['es'].name,
            emoji: this.state.countriesList['ES'].emoji,
            active: this.state.languageActive === 'es'
        })
        list.unshift({
            id: 'en',
            name: this.state.languagesList['en'].name,
            emoji: this.state.countriesList['GB'].emoji,
            active: this.state.languageActive === 'en'
        })

        this.setState({
            searchResults: list,
            searchEmpty: false,
        });
    }

    searchDb = (searchText) => {
        if (searchText) {
            let results = this.state.languages.filter(lang => {
                return lang.name.toLowerCase().startsWith(searchText)
            });
            this.setState({
                searchResults: results,
                searchEmpty: results.length === 0,
            });
        } else {
            this.getLanguagesDefault(this.state.languages);
        }
    }

    searchHandle = (event) => {
        let searchText = event.currentTarget.value;

        this.setState({
            searchText: searchText
        });

        this.searchDb(searchText);
    }

    searchClose = (event) => {
        event.preventDefault();

        document.getElementById('language-search-input').value = '';

        this.setState({
            searchEmpty: true,
            searchText: false,
        });

        this.getLanguagesDefault(this.state.languages);
    }

    /**
     * Set global location & go to location url
     */
    searchClick = (event) => {
        event.preventDefault();

        let active = event.currentTarget.id;

        this.setLanguage(active);
        this.setCookies(active);

        $('.modal').modal('hide');

        setTimeout(async () => {
            window.location.reload();
        }, 200)
    }

    async componentDidMount() {
        await this.getLanguage();
        await this.getLanguages();
    }

    render() {
        let noResults = (this.state.searchText && this.state.searchEmpty)? true : false;

        return (
          <>
              <Modal
                id={'language-modal'}
                title={'Choose language'}
                fullScreen={true}
                headerHide={true}
                content={
                  <>
                      <div className="language-search-head">
                          <div>
                              <div className="d-flex flex-nowrap align-items-center">
                                  <div className="w-100">

                                      <InputSearch
                                        classes={'transparent-input-bg border-none p-b-0'}
                                        icon_left={true}
                                        id={'language-search-input'}
                                        placeholder={'Search language'}
                                        name={'language'}
                                        icon_not_interactive={true}
                                        handleChange={this.searchHandle}
                                      />

                                  </div>

                                  <button onClick={this.searchClose} type="button" className="close color-tertiary f-s-base weight-400 p-10" data-dismiss="modal">
                                      <svg className="icon_close_modal">
                                          <use xlinkHref="/images/icons.svg#icon_close_modal" />
                                      </svg>
                                  </button>
                              </div>
                          </div>
                      </div>

                      <div className={`language-search-container`}>
                          <div className="language-search-container-inner" id="language-search-container-inner">

                              <div className="container">
                                  {noResults &&
                                  <p className="information color-tertiary p-t-15">
                                      Sorry, nothing found for your query. Please try again.
                                  </p>
                                  }
                              </div>

                              <ul className={`language-search-results ${this.state.searchText? 'results' : ''}`}>
                                  {this.state.searchResults.map((lang, index) =>
                                    <li key={index}>
                                        <Link
                                          className={`language-search-result`}
                                          onClick={this.searchClick}
                                          id={lang.id}
                                          to={'/#'}>
                                            <span className={'m-r-10'}>{lang.emoji}</span> {lang.name}
                                            {lang.active &&
                                                <svg className="icon_check_round">
                                                    <use xlinkHref="/images/icons.svg#icon_check_round" />
                                                </svg>
                                            }
                                        </Link>
                                    </li>
                                  )}
                              </ul>

                          </div>
                      </div>
                  </>
                }
              />
          </>
        )
    }
}
