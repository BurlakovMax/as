import React, {Component} from 'react';
import {Link} from "react-router-dom";
import InputSearch from "../../components/form/input-search";
import db from "../../istore";
import $ from "jquery";
import Modal from "../../components/common/modal";

export default class LocationsSearch extends Component {
    constructor(props) {
        super(props);

        this.timeout = null;

        this.state = {
            searchText: '',
            searchResults: [],
            searchEmpty: true,
        };
    }

    searchDb = async () => {
        let results = await db.locations_cities
          .where('name')
          .startsWithIgnoreCase(this.state.searchText)
          .toArray();

        this.setState({
            searchResults: results,
            searchEmpty: results.length === 0,
        });
    }

    searchHandle = (event) => {
        clearTimeout(this.timeout);

        // minimum 2 chars to start searching
        if (event.currentTarget.value.length < 2) {
            this.setState({
                searchResults: [],
                //searchEmpty: true,
                searchText: false,
            });
            return false;
        }

        this.timeout = setTimeout(this.searchDb, 400);

        this.setState({
            searchText: event.currentTarget.value
        });
    }

    searchClose = (event) => {
        event.preventDefault();

        this.setState({
            searchResults: [],
            searchEmpty: true,
            searchText: false,
        });
    }

    /**
     * Set global location & go to location url
     */
    searchClick = (event) => {
        event.preventDefault();

        $('.modal').modal('hide');

        this.props.history.push(event.currentTarget.getAttribute('href'));
    }

    componentDidMount() {
        let modalWindow = $('#location-search-modal');
        let modalInput = $('#location-search-input');

        modalWindow.on('shown.bs.modal', function () {
            modalInput.trigger('focus');
        });
        modalWindow.on('hidden.bs.modal', function () {
            modalInput.val('');
        });
    }

    render() {
        let noResults = (this.state.searchText && this.state.searchEmpty)? true : false;

        return (
          <>
          {this.props.showInput &&
              <div className="location-search-block bg-primary p-15 m-b-20">
                  <p className="text-white text-center weight-600">Type city or choose from below:</p>

                  <InputSearch
                    classes={'p-0'}
                    icon_right={true}
                    id={'open-location-modal-input'}
                    placeholder={'Type city name'}
                    name={'city'}
                    readonly={true}
                    data={{'data-toggle': 'modal', 'data-target': '#location-search-modal'}}
                    //handleClick={this.searchShow}
                  />
              </div>
          }

              <Modal
                id={'location-search-modal'}
                title={'Search for your city'}
                fullScreen={true}
                headerArrow={true}
                handleClickArrow={this.searchClose}
                content={
                  <>
                      <div className="location-search-head">
                          <div>
                              <div className="d-flex flex-nowrap align-items-center">
                                  <div className="w-100">

                                      <InputSearch
                                        classes={'gray-input-bg border-none p-b-0'}
                                        icon_right={true}
                                        id={'location-search-input'}
                                        placeholder={'Type city name here'}
                                        name={'city'}
                                        icon_not_interactive={true}
                                        handleChange={this.searchHandle}
                                        autoFocus={true}
                                      />

                                  </div>
                              </div>
                          </div>
                      </div>

                      <div className={`location-search-container ${noResults? 'no-results' : ''}`}>
                          <div className="location-search-container-inner" id="location-search-container-inner">

                              <div className="container">
                                  {noResults &&
                                      <p className="information color-tertiary p-t-15">
                                          Sorry, nothing found for your query. Please try again.
                                      </p>
                                  }
                              </div>

                              {(this.state.searchText && !this.state.searchEmpty) &&
                              <ul className="location-search-results">
                                  {this.state.searchResults.map((city, index) =>
                                    <li key={index}>
                                        <Link
                                          className="location-search-result"
                                          data-city={city.name}
                                          data-state={city.state}
                                          data-country_code={city.country_code}
                                          onClick={this.searchClick}
                                          to={city.url}>
                                            {`${city.name}, ${city.state}`}
                                        </Link>
                                    </li>
                                  )}
                              </ul>
                              }

                          </div>
                      </div>
                  </>
                }
              />

          </>
        )
    }
}
