import React, {Component} from 'react';
import auth from "../../lib/auth";
import axios from '../../services/api_init';
import {Link} from "react-router-dom";
import {isMobileSafari} from 'react-device-detect';

class SidebarMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: false,
            token: null,
            version: 0
        };
    }

    signOut = event => {
        event.preventDefault();

        auth.clearToken()
          .then(error => {
              if (!error) {
                  auth.gotoSignin()
              }
          })
    }

    getClasses = () => {
        let classes = ['modal', 'hamburger-menu'];

        // remove fade for ios safari
        if (!isMobileSafari) {
            classes.push('fade')
        }

        return classes.join(' ');
    }

    async componentDidMount() {
        const version = await axios.get('/version.txt');
        await this.setState({
            token: auth.getToken(),
            version: version.data
        });
    }

    render() {
        return (
          <div className={this.getClasses()} id="hamburger-menu" tabIndex="-1" role="dialog" aria-hidden="true">
            <div className={'modal-dialog'} role="document">

                    <div className="modal-content">
                        <div className="hamburger-menu-head p-b-20">
                            <div className="d-flex justify-content-end">
                                <button className="close-modal color-secondary btn-link d-flex align-items-center justify-content-center p-x-15 p-t-20 p-b-10" data-dismiss="modal">
                                    <svg className="icon_close_modal">
                                        <use xlinkHref="/images/icons.svg#icon_close_modal" />
                                    </svg>
                                </button>
                            </div>

                            <div className="container">
                                <div className="list-with-icons ">
                                    <ul className="button-list">
                                        <li>
                                            <Link to={'/#'} className="link p-y-15" onClick={this.signOut}>
                                                <div className="icon-with-title m-r-10">
                                                    <div
                                                        className="icon m-r-15 d-flex justify-content-center align-items-center gray icon-logout">
                                                        <svg className="icon_logout">
                                                            <use xlinkHref="/images/icons.svg#icon_logout" />
                                                        </svg>
                                                    </div>
                                                    <span className="color-quaternary">Logout</span>
                                                </div>
                                                <div className="arrow-right">
                                                    <svg className="arrow-right-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right" />
                                                    </svg>
                                                </div>
                                            </Link>
                                        </li>

                                        <li>
                                            <Link className="link p-y-15" to={auth.getLink('home')}>
                                                <div className="icon-with-title m-r-10">
                                                    <div
                                                      className="icon m-r-15 d-flex justify-content-center align-items-center purple icon-location">
                                                        <svg className="icon_change_location">
                                                            <use xlinkHref="/images/icons.svg#icon_change_location" />
                                                        </svg>
                                                    </div>
                                                    <span className="color-quaternary">Change Location</span>
                                                </div>
                                                <div className="arrow-right">
                                                    <svg className="arrow-right-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right" />
                                                    </svg>
                                                </div>
                                            </Link>
                                        </li>

                                        <li>
                                            <Link className="link p-y-15" to={auth.getLink('adbuild')}>
                                                <div className="icon-with-title m-r-10">
                                                    <div
                                                      className="icon m-r-15 d-flex justify-content-center align-items-center dark-blue icon-post_an_ad">
                                                        <svg className="icon_post_ad">
                                                            <use xlinkHref="/images/icons.svg#icon_post_ad" />
                                                        </svg>
                                                    </div>
                                                    <span className="color-quaternary">Post An Ad</span>
                                                </div>
                                                <div className="arrow-right">
                                                    <svg className="arrow-right-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right" />
                                                    </svg>
                                                </div>
                                            </Link>
                                        </li>

{/*                                        <li>
                                            <Link className="link p-y-15" to={auth.getLink('home')}>
                                                <div className="icon-with-title m-r-10">
                                                    <div
                                                      className="icon m-r-15 d-flex justify-content-center align-items-center green icon-desktop">
                                                        <svg className="icon_fullsite">
                                                            <use xlinkHref="/images/icons.svg#icon_fullsite" />
                                                        </svg>
                                                    </div>
                                                    <span className="color-quaternary">Full Site Version</span>
                                                </div>
                                                <div className="arrow-right">
                                                    <svg className="arrow-right-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right" />
                                                    </svg>
                                                </div>
                                            </Link>
                                        </li>*/}

                                        <li>
                                            <Link className="link p-y-15" to={auth.getLink('privacy')}>
                                                <div className="icon-with-title m-r-10">
                                                    <div
                                                      className="icon m-r-15 d-flex justify-content-center align-items-center color-secondary icon-privacy_policy">
                                                        <svg className="icon_privacy_policy">
                                                            <use xlinkHref="/images/icons.svg#icon_privacy_policy" />
                                                        </svg>
                                                    </div>
                                                    <span className="color-quaternary">Privacy Policy</span>
                                                </div>
                                                <div className="arrow-right">
                                                    <svg className="arrow-right-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right" />
                                                    </svg>
                                                </div>
                                            </Link>
                                        </li>

                                        <li>
                                            <Link className="link p-y-15" to={auth.getLink('terms')}>
                                                <div className="icon-with-title m-r-10">
                                                    <div
                                                      className="icon m-r-15 d-flex justify-content-center align-items-center color-secondary icon-tos">
                                                        <svg className="icon_tos">
                                                            <use xlinkHref="/images/icons.svg#icon_tos" />
                                                        </svg>
                                                    </div>
                                                    <span className="color-quaternary">Terms Of Service</span>
                                                </div>
                                                <div className="arrow-right">
                                                    <svg className="arrow-right-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right" />
                                                    </svg>
                                                </div>
                                            </Link>
                                        </li>

                                        <li>
                                            <Link className="link p-y-15" to={auth.getLink('contact')}>
                                                <div className="icon-with-title m-r-10">
                                                    <div
                                                      className="icon m-r-15 d-flex justify-content-center align-items-center pink-strong icon-envelope_v2">
                                                        <svg className="icon_contact_us">
                                                            <use xlinkHref="/images/icons.svg#icon_contact_us" />
                                                        </svg>
                                                    </div>
                                                    <span className="color-quaternary">Contact Us</span>
                                                </div>
                                                <div className="arrow-right">
                                                    <svg className="arrow-right-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right" />
                                                    </svg>
                                                </div>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className={'version'}>Version {this.state.version}</div>
                    </div>
                </div>
          </div>
        )
    }
}

export default SidebarMenu;
