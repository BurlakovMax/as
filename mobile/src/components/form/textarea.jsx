import React, {Component} from 'react';

export default class Textarea extends Component {
    constructor(props) {
        super(props);
    }

    getClasses = () => {
        let classes = ['primary-textarea'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        if (this.props.error) {
            classes.push('error');
        }

        return classes.join(' ');
    }

    render() {
        let id = this.props.id || false;
        let label = this.props.label || false;
        let textareaClasses = this.props.textareaClasses || '';
        let type = this.props.type || 'text';
        let name = this.props.name || false;
        let value = this.props.value || '';
        let placeholder = this.props.placeholder || '';
        let dataAttributes = this.props.data || [];
        let attributes = this.props.attributes || [];
        let required = this.props.required || false;
        let readonly = this.props.readonly || false;
        let disabled = this.props.disabled || false;
        let maxCharacters = this.props.maxCharacters || 1000;

        return (
          <div className={this.getClasses()}>
              {label &&
                <label htmlFor={id}>{label}</label>
              }

              <div className="textarea-container position-relative">
                <textarea
                  type={type}
                  name={name}
                  id={id}
                  className={textareaClasses}
                  placeholder={placeholder}
                  required={required}
                  readOnly={readonly}
                  disabled={disabled}
                  defaultValue={value}
                  maxLength={maxCharacters}
                  onChange={this.props.handleChange}
                  {...dataAttributes}
                  {...attributes}
                />
              </div>
              {this.props.error &&
                  <span className="error-message">
                    {this.props.error}
                  </span>
              }
          </div>
        )
    }
}
