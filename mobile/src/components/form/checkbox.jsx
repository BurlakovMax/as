import React, {Component} from 'react';

export default class Checkbox extends Component {

    getClasses = () => {
        let classes = ['primary-checkbox', 'position-relative'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        return classes.join(' ');
    }

    getLabelClasses = () => {
        let classes = [];

        if (!this.props.textWrap) {
            classes.push('text-nowrap');
        }

        if (this.props.required_icon) {
            classes.push('required');
        }

        return classes.join(' ');
    }

    render() {
      let id = this.props.id || '';
      let parentId = this.props.parentId || '';
      let label = this.props.label || false;
      let required = this.props.required || false;
      let disabled = this.props.disabled || false;
      let dataAttributes = this.props.dataAttrs || [];
      let attributes = this.props.attributes || [];

      let htmlFor = {'htmlFor': this.props.id};
      if (!this.props.htmlFor) {
        htmlFor = {};
      }

      if (this.props.checked) {
        attributes['checked'] = 'checked';
      }

        return (
          <div id={parentId} className={this.getClasses()}>
              <label
                className={this.getLabelClasses()}
                {...htmlFor}
              >
                  {label}
                  <input className={this.props.checkboxClasses}
                         type='checkbox'
                         id={id}
                         name={this.props.name}
                         defaultValue={this.props.value}
                         required={required}
                         //defaultChecked={checked}  // dot not use !!!
                         disabled={disabled}
                         onChange={this.props.handleChange}
                         {...dataAttributes}
                         {...attributes}
                  />
                      <span></span>
              </label>
          </div>
        )
    }
}
