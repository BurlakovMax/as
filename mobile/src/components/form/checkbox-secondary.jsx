import React, {Component} from 'react';

export default class CheckboxSecondary extends Component {
    getClasses = () => {
        let classes = ['btn-group', 'btn-group-toggle', 'wrapper-secondary-checkbox-radio-btn'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        return classes.join(' ');
    }

    getLabelClasses = (item) => {
        let classes = ['btn', 'primary-checkbox-radio-btn'];

        if (item.checked) {
          classes.push('active');
        }

        return classes.join(' ');
    }

    render() {
      let data = this.props.data || [];
      let type = this.props.type || 'checkbox';
      let id = this.props.id || '';

        return (
          <>
            <div className={this.getClasses()} id={id} data-toggle="buttons">

              { data.map((item, index) =>
                {
                  let dataAttributes = item.dataAttrs || {};
                  let attributes = item.attributes || {};

                  if (item.checked) {
                    attributes['checked'] = 'checked';
                  }

                  return (
                    <label
                      htmlFor={type === 'checkbox' ? item.id : item.id+'_'+index}
                      className={this.getLabelClasses(item)}
                      key={index}
                      onClick={item.handleClick}
                      id={`label_${item.id}`}
                      disabled={this.props.disabled}
                    >
                      <input
                        type={type}
                        id={item.id}
                        name={item.name}
                        title={item.title}
                        autoComplete="off"
                        defaultValue={item.value || ''}
                        //checked={item.checked || false} // dot not use !!!
                        required={item.required || false}
                        {...dataAttributes}
                        {...attributes}
                      />
                      {item.title}
                    </label>
                  )
                }
              ) }
            </div>

            {this.props.error &&
              <div className="primary-text-input error">
                <span className="error-message">
                    {this.props.error}
                </span>
              </div>
            }
          </>
        )
    }
}
