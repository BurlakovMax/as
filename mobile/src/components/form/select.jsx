import React, {Component} from 'react';

export default class Select extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }

        this.handleInsideChange = this.handleInsideChange.bind(this);
    }

    getClasses = () => {
        let classes = ['primary-select'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        if (this.props.error || this.props.error_message) {
            classes.push('error');
        }

        return classes.join(' ');
    }

    handleInsideChange(event) {
        this.setState({value: event.target.value});
    }

    render() {
        let data = this.props.data || [];
        let label = this.props.label || false;
        let id = this.props.id || false;
        let name = this.props.name || false;
        let required = this.props.required || false;
        let required_icon = this.props.required_icon || '';
        let value = this.state.value || this.props.value;
        let attr = this.props.dataAttrs || [];
        let tabindex = this.props.tabindex || '';

        return (
          <div className={this.getClasses()}>

              {label && <label className={required_icon && 'required'} htmlFor={id}>
                  {this.props.required_icon &&<span className="color-error">*</span>} {label}
              </label>}

              <div className="select-container">
                  <select name={name}
                          id={id}
                          required={required}
                          onChange={this.props.handleChange || this.handleInsideChange}
                          //defaultValue={value}
                          value={value}
                          disabled={this.props.disabled || false}
                          tabIndex={tabindex}
                          {...attr}
                  >
                    <option value={''} className="disabled-option">
                        {this.props.disabledOption}
                    </option>

                    {data.map((item, key) =>
                      typeof item === 'object' ? (
                        <option value={(item.id)? item.id : item.value} className={item.class} disabled={item.disabled} key={key}>{(item.name)? item.name : item.text}</option>
                      ) : (
                        <option value={key} key={key}>{item}</option>
                      )
                    ) }
                  </select>
              </div>

              {this.props.error_message &&
                  <span className="error-message">
                    {this.props.error_message}
                  </span>
              }
          </div>
        )
    }
}
