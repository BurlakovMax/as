import React, {Component} from 'react';
import ButtonSubmit from "./button-submit";
import auth from "../../lib/auth";

export default class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            passwordHide: true
        };
    }

    getClasses = () => {
        let classes = ['primary-text-input'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        if (this.props.error) {
            classes.push('error');
        }

        if (this.props.success) {
            classes.push('success');
        }

        if (this.props.maxCharacters && !this.props.counter_disabled) {
            classes.push('max-characters');
        }

        return classes.join(' ');
    }

    getInputClasses = () => {
        let classes = ['input-container', 'position-relative'];

        if (this.props.icon_left || this.props.icon_user || this.props.icon_password) {
            classes.push('input-container_icon-left');
        }

        if (this.props.icon_right || this.props.icon_copy) {
            classes.push('input-container_icon-right');
        }

        if (this.props.icon_card) {
            classes.push('input-container_icon-card');
        }

        if (this.props.button) {
            classes.push('input-container_button');
        }

        return classes.join(' ');
    }

    passwordToggle = (event) => {
        event.preventDefault();

        this.setState({
            passwordHide: !this.state.passwordHide
        });
    }

    copyToClipboard = (event) => {
        event.preventDefault();

        let input = document.getElementById(this.props.id);
        input.focus();
        input.select();
        input.setSelectionRange(0, 99999);

        try {
            let ok = document.execCommand('copy');
            if (ok) {
                input.classList.add('copy-success');
            } else {
                input.classList.add('copy-error');
            }
        } catch (err) {
            input.classList.add('copy-error');
        }

        input.blur();
        setTimeout(async () => {
            input.classList.remove('copy-success');
            input.classList.remove('copy-success');
        }, 600)
    }

    render() {
        let id = this.props.id || false;
        let label = this.props.label || false;
        let maxCharacters = this.props.maxCharacters || 1000;
        let minCharacters = this.props.minCharacters || 0;
        let fieldClasses = this.props.field_classes || '';
        let name = this.props.name || '';
        let value = this.props.value || '';
        let placeholder = this.props.placeholder || '';
        let dataAttributes = this.props.data || [];
        let attributes = this.props.attributes || [];
        let required = this.props.required || false;
        let readonly = this.props.readonly || false;
        let disabled = this.props.disabled || false;
        let autocomplete = this.props.autocomplete || '';
        let title = this.props.title || '';
        let tabindex = this.props.tabindex || '';

        let type = this.props.type || 'text';
        if (this.props.icon_password) {
            type = this.state.passwordHide ? 'password' : 'text';
        }

        return (
          <div className={this.getClasses()}>

              <div className="top-form-fields">
                  {label &&
                    <label className={this.props.required_icon && 'required'} htmlFor={id}>
                        {this.props.required_icon &&<span className="color-error">*</span>} {label}
                    </label>
                  }
                  {maxCharacters > 0 &&
                    <span className="max-characters-number"></span>
                  }
              </div>

              <div className={this.getInputClasses()}>
                  <input className={fieldClasses}
                         type={type}
                         {...dataAttributes}
                         {...attributes}
                         name={name}
                         id={id}
                         title={title}
                         defaultValue={value}
                         placeholder={placeholder}
                         required={required}
                         readOnly={readonly}
                         disabled={disabled}
                         maxLength={maxCharacters}
                         minLength={minCharacters}
                         onClick={this.props.handleClick}
                         onBlur={this.props.handleBlur}
                         onChange={this.props.handleChange}
                         onKeyDown={this.props.handleKeyDown}
                         autoComplete={autocomplete}
                         tabIndex={tabindex}
                  />

                  {this.props.button &&
                    <ButtonSubmit classes={this.props.buttonClasses} type={'text'} text={this.props.buttonText || 'Submit'} handleClick={this.props.buttonHandleClick}/>
                  }

                  {this.props.icon_user &&
                      <div className="field-icon field-icon_login field-icon_not-interactive">
                          <svg className="icon_user">
                              <use xlinkHref="/images/icons.svg#icon_user"/>
                          </svg>
                      </div>
                  }

                  {this.props.icon_card &&
                      <div className={`credit_card field-icon field-icon_not-interactive ${this.props.icon_card}`}></div>
                  }

                  {this.props.icon_password &&
                    <>
                        {!this.props.error &&
                          <a
                              className="field-icon field-icon_password-hide field-icon_align-right"
                              id="password_link"
                              onClick={this.passwordToggle}
                              href="/#"
                          >
                              {this.state.passwordHide ? 'show' : 'hide'}
                          </a>
                        }
                      <div className="field-icon field-icon_password">
                          <svg className="icon_password">
                              <use xlinkHref="/images/icons.svg#icon_password"/>
                          </svg>
                      </div>
                    </>
                  }

                  {this.props.icon_copy &&
                      <div onClick={this.copyToClipboard} className="field-icon field-icon_copy field-icon_align-right">
                          <svg className="icon_copy">
                              <use xlinkHref="/images/icons.svg#icon_copy"/>
                          </svg>
                      </div>
                  }
              </div>

              {this.props.error &&
                  <span className="error-message">
                    {this.props.error}
                  </span>
              }
              {this.props.success &&
                <span className="success-message">
                    {this.props.success}
                </span>
              }
          </div>
        )
    }
}
