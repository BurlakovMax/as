import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class ButtonBordered extends Component {

    getClasses = () => {
        let classes = ['btn', 'primary-bordered-btn'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        return classes.join(' ');
    }

    render() {
        let id = this.props.id || '';
        let type = this.props.type || 'submit';
        let text = this.props.text || 'Submit';
        let disabled = this.props.disabled || false;
        let title = this.props.title || '';
        let data = this.props.data || false;

        return (
          <>
          {this.props.link &&
              <Link
                className={this.getClasses()}
                id={id}
                type={'text'}
                title={title}
                disabled={disabled}
                onClick={this.props.handleClick}
                to={this.props.link}
                {...data}
              >
                  <span>{text}</span>
              </Link>
          }
          {!this.props.link &&
              <button
                className={this.getClasses()}
                id={id}
                type={type}
                title={title}
                disabled={disabled}
                onClick={this.props.handleClick}
                {...data}
              >
                  <span>{text}</span>
              </button>
          }
        </>
        )
    }
}
