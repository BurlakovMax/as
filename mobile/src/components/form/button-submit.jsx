import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class ButtonSubmit extends Component {

    getClasses = () => {
        let classes = ['btn', 'primary-submit-btn'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        return classes.join(' ');
    }

    render() {
        let id = this.props.id || '';
        let loading = this.props.loading || false;
        let text = this.props.text || 'Submit';
        let type = this.props.type || 'submit';
        let data = this.props.data || false;
        let disabled = this.props.disabled || false;

        return (
        <>
          {this.props.link &&
                <Link
                  id={id}
                  className={this.getClasses()}
                  disabled={loading || disabled ? true : false}
                  type={'text'}
                  onClick={this.props.handleClick}
                  to={this.props.link}
                  {...data}
                >
                    <span>{loading ? 'Please wait ...' : text}</span>
                </Link>
          }
          {!this.props.link &&
                <button
                  id={id}
                  className={this.getClasses()}
                  disabled={loading || disabled ? true : false}
                  type={type}
                  onClick={this.props.handleClick}
                  {...data}
                >
                    <span>{loading ? 'Please wait ...' : text}</span>
                </button>
          }
        </>
        )
    }
}
