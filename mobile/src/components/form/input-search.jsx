import React, {Component} from 'react';

export default class InputSearch extends Component {

  getClasses = () => {
    let classes = ['primary-text-input'];

    if (this.props.classes) {
      classes.push(this.props.classes);
    }

    if (this.props.error) {
      classes.push('error');
    }

    return classes.join(' ');
  }

  getInputClasses = () => {
    let classes = ['input-container', 'position-relative'];

    if (this.props.icon_left) {
      classes.push('input-container_icon-left');
    }

    if (this.props.icon_right) {
      classes.push('input-container_icon-right');
    }

    if (this.props.icon_clear) {
      classes.push('input-container_icon-clear');
    }

    return classes.join(' ');
  }

  getButtonClasses = (clear = false) => {
    let classes = ['field-icon', 'field-icon_search'];

    if (this.props.icon_right) {
      classes.push('field-icon_align-right');
    }

    if (this.props.icon_clear && clear) {
      classes.push('field-icon_clear');
    }

    if (this.props.icon_not_interactive) {
      classes.push('field-icon_not-interactive');
    }

    return classes.join(' ');
  }

  render() {
    let dataAttributes = this.props.data || [];

    return (
      <div className={this.getClasses()}>
        <div className="top-form-fields"></div>

        <div className={this.getInputClasses()}>
          <input
            type="text"
            id={this.props.id}
            name={this.props.name}
            defaultValue={this.props.value}
            placeholder={this.props.placeholder}
            readOnly={this.props.readonly}
            autoComplete="off"
            onClick={this.props.handleClick}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            {...dataAttributes}
          />

          {this.props.icon_clear &&
          <button onClick={this.props.handleClear} type="button" className={this.getButtonClasses('clear')}>
            <svg className="icon_bordered_cross">
              <use xlinkHref="/images/icons.svg#icon_cross"></use>
            </svg>
          </button>
          }

          <button type="submit" className={this.getButtonClasses()}>
            <svg className="icon_search">
              <use xlinkHref="/images/icons.svg#icon_search"></use>
            </svg>
          </button>
        </div>

          <span className="error-message">
            {this.props.error}
          </span>
      </div>
    )
  }
}
