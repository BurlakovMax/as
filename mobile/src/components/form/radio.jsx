import React, {Component} from 'react';

export default class Radio extends Component {

    getClasses = () => {
        let classes = ['btn-group', 'btn-group-toggle', 'wrapper-primary-radio-btn'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        return classes.join(' ');
    }

    render() {
        let data = this.props.data || false;

        return (
          <div className={this.getClasses()} data-toggle="buttons">
              { data.map((item, index) =>
                <label className="btn primary-radio-btn" key={index}>
                    <input type="radio"
                           name={item.name}
                           id={`${item.id}_${index}`}
                           defaultValue={item.value}
                           autoComplete="off"
                           defaultChecked={item.checked ?? false}
                           required={item.required ?? false}
                           onClick={this.props.handleClick}
                           {...item.dataAttrs}
                    />
                    {item.title}
                </label>
              ) }
          </div>
        )
    }
}
