import React, {Component} from 'react';
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/main.css';

export default class InputTel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: this.props.error,
        };
    }

    getClasses = () => {
        let classes = ['primary-text-input', 'primary-input-tel'];

        if (this.state.error) {
            classes.push('error');
        }

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        return classes.join(' ');
    }

    /**
     * DOCS
     * https://patw0929.github.io/react-intl-tel-input/?selectedKind=Documentation&selectedStory=Getting%20Started&full=0&addons=0&stories=1&panelRight=0&addonPanel=storybooks%2Fstorybook-addon-knobs
     */
    render() {
        let id = this.props.id || false;
        let label = this.props.label || false;
        let name = this.props.name || false;
        let value = this.props.value || '';
        let required = this.props.required || false;
        let readonly = this.props.readonly || false;
        let disabled = this.props.disabled || false;

        return (
          <div className={this.getClasses()}>
              {label &&
                <label className={this.props.required_icon && 'required'} htmlFor={id}>{label}</label>
              }

              <div className="input-container position-relative">
                  <IntlTelInput
                    //onPhoneNumberChange={onChange()}
                    onPhoneNumberBlur={this.props.handleBlur}
                    separateDialCode={true}
                    preferredCountries={['us','gb','ca']}
                    defaultCountry={'us'}
                    nationalMode={false}
                    fieldId={id}
                    fieldName={name}
                    defaultValue={value}
                    required={required}
                    readOnly={readonly}
                    disabled={disabled}
                  />
              </div>
              <span className="error-message">
                {this.state.error}
              </span>
          </div>
        )
    }
}
