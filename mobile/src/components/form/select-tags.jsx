import React, {Component} from 'react';

export default class SelectTags extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: [], // list for selected tags
        }
    }

    getClasses = () => {
        let classes = ['primary-select', 'primary-select-language-spoken', 'position-relative'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        if (this.props.error) {
            classes.push('error');
        }

        return classes.join(' ');
    }

    addTag = (event) => {
        let tags = this.state.value;
        tags.push(event.currentTarget.value);

        this.setState({
            value: tags
        });
    }

    removeTag = (event) => {
        let id = event.currentTarget.dataset.id;

        let tags = this.state.value;
        tags.map((item, index) =>
          {
              if (parseInt(id) === parseInt(item)) {
                  tags.splice(index, 1);
              }
              return null;
          }
        )

        this.setState({
            value: tags
        });
    }

    async componentDidUpdate(prevProps, prevState) {
        if (this.props.value && prevProps.value !== this.props.value) {
            this.setState({
                value: this.props.value
            });
        }
    }

    render() {
        let data = this.props.data || [];
        let label = this.props.label || false;
        let id = this.props.id || false;
        let name = this.props.name || false;
        let required_icon = this.props.required_icon || false;

        // create list with id => name
        let options = {};
        data.map(item =>
          options[item.value] = (item.name)? item.name : item.text
        )

        return (
          <div className={this.getClasses()}>
              {label &&
                <label className={required_icon && 'required'} htmlFor={id}>{label}</label>
              }

              <div className="select-container position-relative">
                  <select id={id} onChange={this.addTag}>
                      <option value="" className="disabled-option">
                          {this.props.disabledOption}
                      </option>

                      {data.map((item, key) =>
                        typeof item === 'object' ? (
                          <option value={item.value} className={item.class} disabled={item.disabled} key={key}>{(item.name)? item.name : item.text}</option>
                        ) : (
                          <option value={key} key={key}>{item}</option>
                        )
                      ) }
                  </select>
              </div>

              {this.props.error_message &&
                <span className="error-message">
                    {this.props.error_message}
                </span>
              }

              <div className="wrapper-tags">
                  {this.state.value.map((item, key) =>
                    <div className="tag" key={key}>
                        <div onClick={this.removeTag} className="delete-tag" data-id={item}></div>{options[item]}
                        <input type="hidden" name={name} value={item}/>
                    </div>
                  ) }
              </div>
          </div>
        )
    }
}
