import React, {Component} from 'react';
import Input from "../form/input";
import Select from "../form/select";

class FormCard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cardType: this.props.cardType || 'unknown',
        }
    }

    getClasses = () => {
        let classes = ['wrapper-ad-creation-container', 'bg-white', 'p-y-15'];

        if (this.props.classes) {
            classes.push(this.props.classes)
        }

        return classes.join(' ');
    }

    getListYears = (deltaYear = 10) => {
        let list = [];
        let year = new Date().getFullYear();

        for (let i = parseInt(year); i < (parseInt(year) + deltaYear); i++) {
            list[i] = i;
        }

        return list;
    }

    getListMonths = () => {
        let list = [];

        for (let i = 1; i <= 12; i++) {
            list[i] = i;
        }

        return list;
    }

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value.trim();

        if (name === 'ccCc') {
            this.setState({
                cardType: this.getCardType(value),
            });
            this.getCardSpaces(value)
        }

        if (typeof this.props.handleChange !== 'undefined') {
            this.props.handleChange(event);
        }
    }

    getCardSpaces = (number) => {
        const data = number
            .replace(/[^\d]/g, '')
            .replace(/(.{4})/g, '$1 ')
            .trim()

        document.getElementById('ccCc').value = data;
    }

    getCardType = (cardNumber) => {
        //JCB
        let jcb_regex = new RegExp('^(?:2131|1800|35)[0-9]{0,}$');
        // American Express
        let amex_regex = new RegExp('^3[47][0-9]{0,}$');
        // Diners Club
        let diners_regex = new RegExp('^3(?:0[0-59]{1}|[689])[0-9]{0,}$');
        // Visa
        let visa_regex = new RegExp('^4[0-9]{0,}$');
        // MasterCard
        let mastercard_regex = new RegExp('^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$');
        // Maestro
        let maestro_regex = new RegExp('^(5[06789]|6)[0-9]{0,}$');
        //Discover
        let discover_regex = new RegExp('^(6011|65|64[4-9]|62212[6-9]|6221[3-9]|622[2-8]|6229[01]|62292[0-5])[0-9]{0,}$');

        // get rid of anything but numbers
        let num = cardNumber.replace(/\D/g, '');

        // checks per each, as their could be multiple hits
        //fix: ordering matter in detection, otherwise can give false results in rare cases
        var type = 'unknown';
        if (num.match(jcb_regex)) {
            type = 'jcb';
        } else if (num.match(amex_regex)) {
            type = 'amex';
        } else if (num.match(diners_regex)) {
            type = 'diners';
        } else if (num.match(visa_regex)) {
            type = 'visa';
        } else if (num.match(mastercard_regex)) {
            type = 'mastercard';
        } else if (num.match(discover_regex)) {
            type = 'discover';
        } else if (num.match(maestro_regex)) {
            if (parseInt(num[0]) === 5) { //started 5 must be mastercard
                type = 'mastercard';
            } else {
                type = 'maestro'; //maestro is all 60-69 which is not something else, thats why this condition in the end
            }
        }

        return type;
    }

    render() {
        let title = this.props.title || 'Payment Details';
        let alert = this.props.alert || '';
        let errors = this.props.errors || {}; //ccCc,ccMonth,ccYear,ccCvc2
        let tabIndex = this.props.tabIndex || [1,2,3,4];
        let hideCVV = this.props.hideCVV || false;

        return (
            <div id={'credit-card'} className={this.getClasses()}>
                <h2 className="weight-700 color-primary p-b-10">{title}</h2>

                <Input
                    classes={'p-b-5'}
                    label={'Credit Card Number'}
                    type={'tel'}
                    id={'ccCc'}
                    name={'ccCc'}
                    placeholder={'Enter credit card number'}
                    error={errors.ccCc}
                    //required={true}
                    required_icon={true}
                    minCharacters={13}
                    maxCharacters={19}
                    handleChange={this.handleChange}
                    data={{'data-pattern': '[\\d\\s]{13,19}'}}
                    icon_card={this.state.cardType}
                    //value={this.state.payment.ccCc}
                    tabindex={tabIndex[0]}
                />

                {alert &&
                    <p className="p-b-15">
                        {alert}
                    </p>
                }

                <div className="d-flex align-items-center justify-content-between p-b-15">

                    <Select
                        classes={'m-b-auto flex-fill'}
                        label={'Month'}
                        disabledOption={'Month'}
                        id={'ccMonth'}
                        name={'ccMonth'}
                        data={this.getListMonths()}
                        //required={true}
                        required_icon={true}
                        handleChange={this.handleChange}
                        error_message={errors.ccMonth}
                        tabindex={tabIndex[1]}
                        //value={this.state.escort.emailVisibility || 1}
                    />

                    <Select
                        classes={'m-b-auto m-x-10 flex-fill'}
                        label={'Year'}
                        disabledOption={'Year'}
                        id={'ccYear'}
                        name={'ccYear'}
                        data={this.getListYears()}
                        //required={true}
                        required_icon={true}
                        handleChange={this.handleChange}
                        error_message={errors.ccYear}
                        tabindex={tabIndex[2]}
                        //value={this.state.escort.emailVisibility || 1}
                    />

                    {!hideCVV &&
                        <Input
                            classes={'p-b-0 m-b-auto text-center flex-fill'}
                            label={'CVV/CVC'}
                            type={'tel'}
                            id={'ccCvc2'}
                            name={'ccCvc2'}
                            placeholder={'123'}
                            error={errors.ccCvc2}
                            //required={true}
                            required_icon={true}
                            minCharacters={3}
                            maxCharacters={4}
                            handleChange={this.handleChange}
                            data={{'data-pattern': '[\\d]{3,4}'}}
                            tabindex={tabIndex[3]}
                        />
                    }

                </div>
            </div>
        )
    }
}

export default FormCard;
