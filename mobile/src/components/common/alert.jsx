import React, {Component} from 'react';

export default class Alert extends Component {
    getClasses = () => {
        let classes = ['alert-container', 'container'];

        if (!this.props.transparent) {
            classes.push('bg-white')
        }

        if (this.props.classes) {
            classes.push(this.props.classes)
        }

        if (!this.isClosable()) {
            classes.push('p-0')
        }

        return classes.join(' ');
    }

    isClosable() {
        return (this.props.closable === false)? false : true;
    }

    componentDidMount() {
        // go top only for closable alert
        if (this.isClosable()) {
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
    }

    render() {
        /**
         * Type of alert: error, success, warning
         * @type {string}
         */
        let type = this.props.type || 'error';

        return (
          <>
              {this.props.message &&
                  <div className={this.getClasses()}>
                      <div className={`alert-${type} alert fade show ${!this.isClosable()? 'not-closable' : ''}`} role="alert">
                          {this.props.message}

                          {this.isClosable() &&
                              <button type="button" className="alert_close" onClick={this.props.close}>
                                  <svg className="icon_alert_close">
                                      <use xlinkHref="/images/icons.svg#icon_alert_close"/>
                                  </svg>
                              </button>
                          }
                      </div>
                  </div>
              }
          </>
        )
    }
}
