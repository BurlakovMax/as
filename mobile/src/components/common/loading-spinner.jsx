import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class LoadingSpinner extends Component {
    getClasses = () => {
      let classes = ['loader'];

      document.body.classList.add('load');
      if (!this.props.loading) {
        classes.push('hide-loader')
        document.body.classList.remove('load');
      }

      if (this.props.payment) {
        classes.push('with-payment')
      }

      return classes.join(' ');
    }

    render() {
        return (
            <div className={this.getClasses()}>
                {this.props.payment &&
                  <span className="loader-payment">Processing payment<br />Please <span className="color-error text-uppercase">do not</span> close or refresh page</span>
                }
                <span className="loader-progress">load</span>
                <svg className="icon_loader loader-icon">
                    <use xlinkHref="/images/icons.svg#icon_loader"/>
                </svg>
            </div>
        );
    }
}

LoadingSpinner.propTypes = {
    loading: PropTypes.bool.isRequired,
};

