import React from 'react';
import {
    Route,
    Redirect,
} from "react-router-dom";
import auth from "../../lib/auth";

export default function PrivateRoute({ children, ...rest }) {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                auth.getToken() ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/account/signin",
                            state: { from: location }
                        }}
                    />
                )
            }
        />
    );
}