// | DOCS https://github.com/fredolss/rater-js
// --- EXAMPLE / DEMO https://fredolss.github.io/rater-js/example/
import $ from "jquery";
import rater from "rater-js";

let ratingStars = () => {
  const $ratingBlock = $('.primary-rating');

  $ratingBlock.each((index, element) => {

    // Initialize uninitialized rating-blocks
    if ( !$(element).hasClass('star-rating') ) {
      const starSize = element.dataset.large !== undefined ? 41 : 21;
      let rating = element.dataset.rating !== undefined ? parseFloat(element.dataset.rating) : 0;
      let readonly = element.dataset.readonly !== undefined ? Boolean(element.dataset.readonly) : false;

      let myRater = rater({
        element: element,
        showToolTip: false,
        starSize: starSize,
        rating: rating,
        readOnly: readonly,
        rateCallback (ratingValue, done) {
          if(!ratingValue || ratingValue === undefined) {
            return;
          } else if (ratingValue < 0) {
            ratingValue = 0;
          } else if (ratingValue > 5) {
            ratingValue = 5;
          }

          this.setRating(ratingValue);
          $('#star').val(ratingValue);

          done();
        },
      });

      if($(element).hasClass('disabled')) {
        myRater.dispose();
      }
    }
  });
};

export default ratingStars
