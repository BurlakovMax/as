import $ from "jquery";

let placeHours = () => {

  $(function() {
    $(document)
      .on('change', '.daytoggle input', function () {
        var day = $(this).data('day');
        $('.collapse-' + day).collapse($(this).prop('checked') ? 'show' : 'hide');
      });
  });
};

export default placeHours
