import React, {Component} from 'react';

class FormPhoto extends Component {
    render() {
        let pictures = this.props.pictures || [];
        let picturesMax = this.props.picturesMax || 10;
        let picturesLimit = this.props.picturesLimit || false;
        let picturesError = this.props.picturesError || false;
        let picturesLoading = this.props.picturesLoading || false;

        return (
          <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10" id={'photos'}>
              <div className="container">
                  <div className="ad-creation-container">

                      {this.props.type === 'escort' &&
                        <>
                          <h2 className="weight-700 color-primary p-b-10">
                              <span className="color-error">*</span> Photos <span className={'weight-400 f-s-16'}>(min 1 image required)</span>
                          </h2>

                          <p className="p-b-15">
                            Your first image will also be your thumbnail. You can edit it after submitting your ad. Click the button below to browse & upload
                            <span className={'weight-600'}> up to {picturesMax} images</span>.
                          </p>
                      </>
                      }

                      {this.props.type === 'wire' &&
                         <h2 className="weight-700 color-primary p-b-10">
                             <span className="color-error">*</span> Wire Receipt Image
                          </h2>
                      }

                      {this.props.type === 'place' &&
                         <h2 className="weight-700 color-primary p-b-10">
                             <span className="color-error">*</span> Main Photo
                          </h2>
                      }

                      <div className="file_uploader __photo">
                          {picturesError &&
                            <div className="file_uploader__error m-b-10">{picturesError}</div>
                          }

                          <div className="file_uploader__container">
                              <div className="file_uploader__preview">
                                  {!picturesLimit &&
                                      <label type="button" className={`file_uploader__button file_uploader__picture`}>
                                          {picturesLoading ? (
                                              <div className="loader d-flex">
                                                  <span className="loader-progress">load</span>
                                                  <svg className="loader-icon">
                                                      <use xlinkHref="/images/icons.svg#icon_loader"/>
                                                  </svg>
                                              </div>
                                          ) : (
                                              <>
                                                  <svg className="icon_photo_upload">
                                                      <use xlinkHref="/images/icons.svg#icon_photo_upload"/>
                                                  </svg>
                                                  <input
                                                      onChange={this.props.uploadHandle}
                                                      data-type={this.props.type}
                                                      type="file"
                                                      id="photo"
                                                      name="photo"
                                                      accept="image/*"
                                                  />
                                              </>
                                          )}
                                      </label>
                                  }

                                  {pictures.map((item, index) =>
                                      <div key={index} className="file_uploader__picture">
                                          {!item.readonly &&
                                              <div
                                                onClick={this.props.deleteHandle}
                                                data-type={this.props.type}
                                                data-index={index}
                                                data-id={item.id}
                                                data-name={item.name}
                                                className="file_uploader__delete"
                                              />
                                          }
                                          <div style={{backgroundImage: `url(${item.url})`}} className="file_uploader__img"/>
                                      </div>
                                  )}
                              </div>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
        )
    }
}

export default FormPhoto;
