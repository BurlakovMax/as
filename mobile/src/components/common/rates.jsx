import React, {Component} from 'react';

export default class Rates extends Component {
    render() {
        return (
            <div className="rates-tabs">
                <div className="rates-tabs-nav-container p-b-20">
                    <ul className="nav nav-tabs" id="primary-tab" role="tablist">
                        <li className="nav-item d-flex align-items-center">
                            <a className="nav-link color-tertiary weight-600 p-y-5 p-x-30 d-flex align-items-center f-s-16 active"
                               id="incall-tab" data-toggle="tab" href="#incall" role="tab"
                               aria-controls="incall" aria-selected="true">Incall</a>
                        </li>

                        <li className="nav-item d-flex align-items-center">
                            <a className="nav-link color-tertiary weight-600 p-y-5 p-x-30 d-flex align-items-center f-s-16"
                               id="outcall-tab" data-toggle="tab" href="#outcall" role="tab"
                               aria-controls="outcall" aria-selected="true">Outcall</a>
                        </li>
                    </ul>
                </div>

                <div className="tab-content rates-tab-content">
                    <div className="tab-pane fade show active" id="incall" role="tabpanel" aria-labelledby="incall-tab">
                        {this.props.isIncallEnabled ?
                            (
                                !this.props.incallRate ?
                                    (
                                        <>
                                            <div className="data-list-item weight-600 d-flex align-items-center justify-content-between p-b-5">
                                                <span className="left">Duration</span>
                                                <span className="right">Price</span>
                                            </div>

                                            {this.props.incallRates.map((rate, i) => {
                                                if (rate[1]) {
                                                    return (
                                                      <div className="data-list-item d-flex align-items-center justify-content-between p-b-15 m-t-15" key={i}>
                                                          <span className="left">{rate[0]}</span>
                                                          <span className="right">$ {rate[1]}</span>
                                                      </div>
                                                    )
                                                }
                                                return null;
                                            })}
                                        </>
                                    ) :
                                    (
                                        'Yes'
                                    )

                            ) :
                            (
                                'No'
                            )
                        }
                    </div>

                    <div className="tab-pane fade" id="outcall" role="tabpanel" aria-labelledby="outcall-tab">
                        {this.props.isOutcallEnabled ?
                            (
                                !this.props.outcallRate ?
                                    (
                                        <>
                                            <div className="data-list-item weight-600 d-flex align-items-center justify-content-between p-b-5">
                                                <span className="left">Duration</span>
                                                <span className="right">Price</span>
                                            </div>

                                            {this.props.outcallRates.map((rate, i) => {
                                                if (rate[1]) {
                                                    return (
                                                      <div className="data-list-item d-flex align-items-center justify-content-between p-b-15 m-t-15" key={i}>
                                                          <span className="left">{rate[0]}</span>
                                                          <span className="right">$ {rate[1]}</span>
                                                      </div>
                                                    )
                                                }
                                                return null;
                                            })}
                                        </>
                                    ) :
                                    (
                                        'Yes'
                                    )

                            ) :
                            (
                                'No'
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}
