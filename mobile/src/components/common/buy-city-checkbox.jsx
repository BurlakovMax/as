import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class BuyCityCheckbox extends Component {
    state = {
        isChecked: false,
    }

    toggleCheckboxChange = () => {
        const { handleCheckboxChange, id } = this.props;

        this.setState(({ isChecked }) => (
            {
                isChecked: !isChecked,
            }
        ));

        handleCheckboxChange(id);
    }

    render() {
        const { label, id, price } = this.props;
        const { isChecked } = this.state;
        return (
          <div className={`ad-creation-upgrade_controls m-b-20 ${isChecked? 'active' : ''}`}>
              <label className="d-block m-0" htmlFor={id}>
                <div className="ad-creation-upgrade_title d-flex align-items-center justify-content-between">
                    <div className="primary-checkbox position-relative p-0">
                        <label>
                            {label}
                            <input
                                type="checkbox"
                                checked={isChecked}
                                onChange={this.toggleCheckboxChange}
                                id={id}
                                value={id} />
                        </label>
                    </div>
                    <div className="price p-x-15 p-y-10 m-l-10 weight-700 lh-normal color-white">
                        ${price}/mo
                    </div>
                </div>
              </label>
          </div>
        );
    }
}

BuyCityCheckbox.propTypes = {
    label: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    handleCheckboxChange: PropTypes.func.isRequired,
};
