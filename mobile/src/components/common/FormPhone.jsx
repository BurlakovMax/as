import React, {Component} from 'react';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/bootstrap.css';

class FormPhone extends Component {
    getClasses = () => {
        let classes = ['wrapper-ad-creation-container', 'bg-white', 'p-y-15'];

        if (this.props.classes) {
            classes.push(this.props.classes)
        }

        return classes.join(' ');
    }

    getPhoneClasses = () => {
        let classes = ['primary-text-input', 'primary-input-tel', 'p-b-0'];

        if (this.props.phoneClasses) {
            classes.push(this.props.phoneClasses)
        }

        return classes.join(' ');
    }

    render() {
        let value = this.props.value || '';
        let phoneValid = this.props.phoneValid || false;
        let phoneResult = this.props.phoneResult || false;
        let phoneError = this.props.phoneError || false;
        let required = this.props.required || false;
        let header = this.props.header || '';
        let label = this.props.label || '';
        let labelRequired = this.props.labelRequired || '';

        let defaultCountry = this.props.defaultCountry || 'us';
        let defaultCountries = this.props.defaultCountries || ['us','ca','gb'];

        return (
            <div id={'phone'} className={this.getClasses()}>
                <div className="container">
                    <div className="ad-creation-container">

                        {header &&
                            <h2 className="weight-700 color-primary p-b-10">
                                {required && <span className="color-error">*</span>} {header}
                            </h2>
                        }

                        {label &&
                            <p className="phone-label p-b-15">
                                {labelRequired && <span className="color-error">*</span>} {label}
                            </p>
                        }

                        <PhoneInput
                            containerclassName={this.getPhoneClasses()}
                            enableSearch={true}
                            onBlur={this.props.handleBlur}
                            country={defaultCountry}
                            preferredCountries={defaultCountries}
                            value={value}
                            //autoFormat={false}
                            //onChange={phone => this.setState({ phone })}
                        />

                        {(phoneValid && phoneResult && !phoneError) && <div id="phone_verify_result" className={'m-t-10 weight-600 color-success'}>Phone verified.</div>}
                        {(!phoneValid && phoneResult && !phoneError) && <div id="phone_verify_result" className={'m-t-10 weight-600 color-error'}>Phone not verified.</div>}
                        {phoneError && <div className={'m-t-10 color-error'}>{phoneError}</div>}
                    </div>
                </div>
            </div>
        )
    }
}

export default FormPhone;
