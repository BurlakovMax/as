import React, {Component} from 'react';
import {Link} from "react-router-dom";
import auth from "../../lib/auth";

export default class Footer extends Component {
  render() {
        return (
          <footer className="footer">

            <div className={`footer__item ${this.props.active.home? 'footer__item_active' : ''}`}>
              <Link to={auth.getLink('home')} className="footer__item-link footer__item_home">
                <span className="footer__item-icon">
                  <svg className="icon_footer_location">
                      <use xlinkHref="/images/icons.svg#icon_footer_location"/>
                  </svg>
                </span>
                <span className="footer__item-title">Search</span>
              </Link>
            </div>

            <div className={`footer__item ${this.props.active.myposts? 'footer__item_active' : ''}`}>
                  <Link to={auth.getLink('myposts')} className="footer__item-link footer__item_my-ads">
                      <span className="footer__item-icon">
                            <svg className="icon_footer_myads">
                                <use xlinkHref="/images/icons.svg#icon_footer_myads"/>
                            </svg>
                      </span>
                      <span className="footer__item-title">My Ads</span>
                  </Link>
              </div>

            <div className={`footer__item ${this.props.active.adbuild? 'footer__item_active' : ''}`}>
                  <Link to={auth.getLink('adbuild')} className="footer__item-link footer__item_post-ad">
                      <span className="footer__item-icon">
                          <svg className="icon_footer_postad">
                              <use xlinkHref="/images/icons.svg#icon_footer_postad"/>
                          </svg>
                      </span>
                      <span className="footer__item-title">Post Ad</span>
                  </Link>
              </div>

            <div className={`footer__item ${this.props.active.messages? 'footer__item_active' : ''}`}>
                  <Link to="/messenger" className="footer__item-link footer__item_messages">
                      <span className="footer__item-icon">
                        <span className="footer__messages">
                            <svg className="icon_footer_messages">
                                <use xlinkHref="/images/icons.svg#icon_footer_messages"/>
                            </svg>
                            {this.props.messagesCount > 0 &&
                                <span className="footer__messages-count">{this.props.messagesCount}</span>
                            }
                        </span>
                      </span>
                      <span className="footer__item-title">Messages</span>
                  </Link>
              </div>

            {auth.getToken() ? (
              <div className={`footer__item ${this.props.active.account? 'footer__item_active' : ''}`}>
                <Link to={auth.getLink('account')} className="footer__item-link footer__item_my-as">
                  <span className="footer__item-icon">
                    <svg className="icon_footer_myas">
                        <use xlinkHref="/images/icons.svg#icon_footer_myas"/>
                    </svg>
                  </span>
                  <span className="footer__item-title">My AS</span>
                </Link>
              </div>
            ) : (
              <div className={`footer__item ${this.props.active.signin? 'footer__item_active' : ''}`}>
                <Link to={auth.getLink('signin')} className="footer__item-link footer__item_my-as">
                  <span className="footer__item-icon">
                    <svg className="icon_footer_myas">
                        <use xlinkHref="/images/icons.svg#icon_footer_myas"/>
                    </svg>
                  </span>
                  <span className="footer__item-title">Sign In</span>
                </Link>
              </div>
            )}

          </footer>
        );
    }
}
