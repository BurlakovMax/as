import React, {Component} from 'react';
import Flickity from 'react-flickity-component';

export default class ProfileCarousel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      totalSlides: 0,
      currentSlide: 0,
      imagesPopupVisible: false,
      imagesPopupSelectedSlide: 0,
    };
  }

  countTotalSlides = () => {
    let totalSlides = 0;

    if (this.props.slides.videos) {
      totalSlides += this.props.slides.videos.length;
    }

    if (this.props.slides.images) {
      totalSlides += this.props.slides.images.length;
    }

    this.setState({
      totalSlides: totalSlides
    });
  }

  changeStatusCurrentSlide = () => {
    this.setState({
      currentSlide: this.flkty.selectedIndex + 1
    });
  }

  showImagesPopup = (i) => {
    this.setState({
      imagesPopupVisible: true,
      imagesPopupSelectedSlide: i,
    });
    this.imagesPopupFlkty.select(i, false, true);
  }

  hideImagesPopup = () => {
    this.setState({
      imagesPopupVisible: false
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.slides !== this.props.slides) {
      this.countTotalSlides()
      this.changeStatusCurrentSlide()
    }
  }

  componentDidMount = () => {
    this.countTotalSlides()
    this.changeStatusCurrentSlide()

    this.flkty.on('change', () => {
      this.changeStatusCurrentSlide()
    })
  }

  getClasses = () => {
    let classes = ['carousel','profile-carousel'];

    if (this.props.classes) {
      classes.push(this.props.classes);
    }

    return classes.join(' ');
  }

  render() {
    let imagesPopupVisibleClass = !this.state.imagesPopupVisible ? 'invisible' : '';

    return (
        <>
          <div className={this.getClasses()}>

            {this.state.totalSlides > 1 &&
              <div className="profile-carousel__counter">{this.state.currentSlide} of {this.state.totalSlides}</div>
            }

            <Flickity
                className={'carousel__inner'}
                options={{
                  cellAlign: 'center',
                  draggable: true,
                  contain: true,
                  pageDots: false,
                  prevNextButtons: false,
                  freeScrollFriction: 0.03,
                }}
                flickityRef={c => this.flkty = c}
            >
              {this.props.slides.videos && this.props.slides.videos.map((video, i) => {
                return <div className="carousel__item carousel__item-video" key={i}>
                  <video controls>
                    <source src={`${this.props.srcPrefixVideos}/${video}`} type="video/mp4" />
                  </video>
                </div>
              })}

              {this.props.slides.images && this.props.slides.images.map((image, i) => {
                return <div
                    className="carousel__item"
                    onClick={() => this.showImagesPopup(i)}
                    key={i}
                >
                  <img src={`${this.props.srcPrefixImages}/${image}`} alt="Carousel" />
                </div>
              })}
            </Flickity>
          </div>

          {this.props.slides.images &&
            <div className={`images-popup ${this.props.className} ${imagesPopupVisibleClass}`}>
              <div className="popup-close" onClick={this.hideImagesPopup} />

              <div className="carousel">
                <Flickity
                    className={'carousel__inner'}
                    options={{
                      cellAlign: 'center',
                      draggable: true,
                      contain: true,
                      groupCells: true,
                      prevNextButtons: false,
                      freeScrollFriction: 0.03,
                    }}
                    flickityRef={c => this.imagesPopupFlkty = c}
                >
                  {this.props.slides.images.map((image, i) => {
                    return <div className="carousel__item image-wrapper" key={i}>
                      <img src={`${this.props.srcPrefixImages}/${image}`} alt="Carousel" className="image" />
                    </div>
                  })}
                </Flickity>
              </div>
            </div>
          }
        </>
    )
  }
}
