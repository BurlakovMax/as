import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class Breadcrumbs extends Component {

    render() {
        const links = this.props.links || [];
        return (
            <div className="primary-breadcrumbs">
                <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb bg-transparent">
                            { links.map(link =>
                                <li key={link.name} className="breadcrumb-item weight-300 f-s-10 d-flex align-items-center">
                                    <Link className="link" to={ link.url }>{ link.name }</Link>
                                </li>
                            ) }
                        </ol>
                    </nav>
                </div>
            </div>
        )
    }
}
