import React from 'react';
import {Link} from "react-router-dom";
import {useStore} from 'effector-react';
import auth from "../../lib/auth";
import {history} from "../../index";
import {
    title as titleState,
    location as locationState, setLocation,
    language as languageState,
} from '../../store/common';
import config from "../../lib/config";

const Header = () => {
    let fixed = false;
    let transparent = false;
    let showLanguage = true;
    let showLocation = true;
    let showAuth = true;

    let orientation = false;
    if (window.matchMedia) {
        orientation = (window.matchMedia('(orientation: landscape)')).matches ? 'landscape' : 'portrait';
    }

    // global: current page title
/*    let title = useStore(titleState);
    let titleMax = 50;
    if (title && title.length > titleMax && orientation === 'portrait') {
        title = title.substr(0,titleMax - 1) + (title.length > titleMax ? ' ...' : '')
    }*/
    let title = false; /// DISABLE TITLE !!!

    // global: current city object (from global or LS)
    let location = useStore(locationState);
    if (!location || !Object.keys(location).length) {
        location = config.getCity();
        config.setCity(location);
    }

    // global: current language object
    const language = useStore(languageState);

    // local header vars
    let showBackLink = (auth.isPage('home') || auth.isPage('signin') || auth.isPage('signup') || auth.isPage('reset'))? false : true;
    let showLogo = (auth.isPage('home') || auth.isPage('signin') || auth.isPage('signup') || auth.isPage('reset'))? true : false;
    let showTitle = (!showLogo && title)? true : false;

    if (!history.length) {
        showBackLink = false;
    }

    // show small text logo
    let logoText = (showLogo && orientation === 'portrait' && location.name)? true : false;

    // spacial cases
    let topups = auth.isPage('topups');
    if (topups) {
        fixed = true;
        transparent = true;
        showLogo = false;
        showTitle = false;
        showLanguage = false;
        showLocation = false;
        showAuth = false;
    }

    return (
      <header className={`${transparent ? 'bg-transparent' : ''} ${fixed ? 'fixed' : ''}`}>
          <div className={`header-container ${showBackLink ? 'justify-content-end' : 'justify-content-center'}`}>

              {/*LINK BACK*/}
              {showBackLink &&
                  <div className={`header-back-btn-container ${title? '' : 'm-r-auto'}`}>
                      <button onClick={history.goBack} className={'header-back-btn btn-link color-white'}>
                          <svg className="icon_header_back">
                              <use xlinkHref="/images/icons.svg#icon_header_back"></use>
                          </svg>
                      </button>
                  </div>
              }

              {/*LOGO*/}
              {showLogo &&
                  <Link className="brand m-r-auto" to={auth.getLink('home')}>
                      {logoText ?
                        (<span className="brand__text">AS</span>)
                        :
                        (<img src="/images/brand2.svg" alt="AdultSearch" className="brand__image"/>)
                      }
                  </Link>
              }

              {/*TEXT TITLE*/}
              {showTitle &&
                  <div className="header-title m-l-10">
                      {title}
                  </div>
              }

              {/*LANGUAGES*/}
              {showLanguage &&
                  <div className="language-dropdown header-language__dropdown m-l-5">
                      <a href="/#" className="language-dropdown__select arrow-dropdown__small" data-toggle="modal"
                         data-target="#language-modal">
                          <span className={'m-r-5'}>{language.language}</span>
                      </a>
                  </div>
              }

              {/*LOCATION NAME WITH ICON*/}
              {(showLocation && location.name) &&
                  <Link to={location.url} className="header-location arrow-dropdown__small p-l-20">
                      <span className="header-location_label m-r-5">{location.name}</span>
                  </Link>
              }

              {/* NO LOCATION - ONLY ICON*/}
              {(showLocation && !location.name) &&
                <a href={'/#'} className="header-location arrow-dropdown__small p-l-20" data-toggle="modal" data-target="#location-search-modal"></a>
              }

              {/*NOT LOGGED - LOGIN BTN*/}
              {(showAuth && !auth.getToken()) &&
                  <Link to={auth.getLink('signup')} className={`header-signin m-l-20`}>
                      Sign Up
                  </Link>
              }

              {/*LOGGED - HUMBURGER MENU*/}
              {(showAuth && auth.getToken()) &&
                  <a href="/#" className="burger-menu-toggle m-l-15" data-toggle="modal" data-target="#hamburger-menu">
                      <svg className="icon_burger">
                          <use xlinkHref="/images/icons.svg#icon_burger"/>
                      </svg>
                  </a>
              }

          </div>
      </header>
    );
};

export default Header;
