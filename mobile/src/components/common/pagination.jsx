import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Pagination extends Component {
    constructor(props) {
        super(props);

        this.state = {
            limits: [10, 20, 30],
            limit: props.limit || 10,
            offset: props.offset || 0,
        }
    }

    perPage = (event) => {
        event.preventDefault();

        let limit = parseInt(event.currentTarget.value);

        if (this.state.limit !== limit) {
            this.setState({
                offset: 0,
                limit: limit,
            });
            this.props.getData(0, limit); // restart from 1 page
        }
    }

    prevPage = async event => {
        event.preventDefault();
        let offset = this.state.offset - this.state.limit;
        if (offset < 0) { offset = 0; }
        this.setState({
            offset: offset
        });

        this.props.getData(offset, this.state.limit);
    }

    nextPage = event => {
        event.preventDefault();
        let offset = this.state.offset + this.state.limit;
        this.setState({
            offset: offset
        });

        this.props.getData(offset, this.state.limit);
    }

    getClasses = () => {
        let classes = ['primary-pagination', 'd-flex', 'align-items-center', 'flex-nowrap w-100'];

        if (this.props.classes) {
            classes.push(this.props.classes);
        }

        return classes.join(' ');
    }

    render() {
        const next = this.state.offset + this.state.limit;
        const prevClasses = 'prev flex-grow-1 d-flex align-items-center justify-content-center bg-white color-secondary p-y-10 m-r-10';
        const nextClasses = 'next flex-grow-1 d-flex align-items-center justify-content-center bg-white color-secondary p-y-10 m-l-10';

        let prevDisabled = (this.state.offset === 0)? true : false;
        let nextDisabled = (next >= this.props.total)? true : false;

        let showControls = (!prevDisabled || !nextDisabled)? true : false;
        let showPerPage = (this.props.total > this.state.limits[0])? true : false;

        if (!showControls && !showPerPage) {
            return null;
        }

        return (
          <>
          {showControls &&
              <div className="primary-pagination d-flex align-items-center flex-nowrap w-100 p-y-20">
                  <a href="/#" className={prevDisabled ? `${prevClasses} disabled` : prevClasses} onClick={this.prevPage}>
                      <svg className="icon_pagination_prev">
                          <use xlinkHref="/images/icons.svg#icon_pagination_prev"/>
                      </svg>
                      <span className="m-l-10">Prev Page</span>
                  </a>

                  <a href="/#" className={nextDisabled ? `${nextClasses} disabled` : nextClasses} onClick={this.nextPage}>
                      <span className="m-r-10">Next Page</span>
                      <svg className="icon_pagination_next">
                          <use xlinkHref="/images/icons.svg#icon_pagination_next"/>
                      </svg>
                  </a>
              </div>
          }

          {showPerPage &&
              <div className={`per-page-btn-group w-100 d-flex align-items-center flex-nowrap justify-content-center ${showControls ? '' : 'm-t-10'}`}>
                  <span className="text f-s-14">Show me</span>
                  <div className="btn-group btn-group-toggle p-x-10" data-toggle="buttons">
                      {this.state.limits.map(limit =>
                        <label key={limit} id={`ipp-${limit}`}
                               className={`per-page-label link btn btn-secondary f-s-14 bg-transparent color-tertiary ${limit === this.state.limit ? 'active' : ''}`}>
                            <input onClick={this.perPage} type="radio" name="ipp" id={`ipp${limit}`} value={limit} autoComplete="off" defaultChecked={limit === this.state.limit}/>
                            {limit}
                        </label>
                      )}
                  </div>
                  <span className="text f-s-14">per page</span>
              </div>
          }
          </>
        );
    }
}

Pagination.propTypes = {
    total: PropTypes.number.isRequired,
    limit: PropTypes.number,
    offset: PropTypes.number,
    classes: PropTypes.string,
    getData: PropTypes.func.isRequired,
};
