import React, {Component} from 'react';
import Flickity from 'react-flickity-component';
import {Link} from "react-router-dom";

export default class NavigationCarousel extends Component {

  selectCurrentSlide = () => {
    this.props.slides.some((slide, i) => {
      if (window.location.href.indexOf(this.props.urlPrefix + slide.url) > -1 && this.flkty.slides.length > 0) {
        let currentSlideIndex = this.flkty.selectedIndex;
        let newSlideIndex = Math.ceil((i + 1) / this.flkty.slides[0].cells.length) - 1;

        if (currentSlideIndex !== newSlideIndex) {
          this.flkty.select(newSlideIndex);
        }

        return true;
      }
      return null;
    });
  }

    componentDidUpdate = () => {
    this.selectCurrentSlide()
  }

  componentDidMount = () => {
    this.selectCurrentSlide()
  }

  render() {
    return (
        <div className={`carousel navigation-carousel ${this.props.className}`}>
          <Flickity
              className={'carousel__inner'}
              options={{
                cellAlign: 'center',
                draggable: true,
                contain: true,
                groupCells: true,
                pageDots: false,
                prevNextButtons: false,
                freeScrollFriction: 0.03,
              }}
              flickityRef={c => this.flkty = c}
          >
            {this.props.slides.map((slide) => {
              if (slide.name) {
                if (window.location.href.indexOf(this.props.urlPrefix + slide.url) > -1) {
                  return <div className="carousel__item thumb-icon carousel__item_active" key={slide.name}>
                    <div className={`icon icon_${slide.url} thumb-icon__icon`} />
                    <div className="thumb-icon__title">{slide.name}</div>
                  </div>
                } else {
                  if (slide.name === 'M4M Escorts') {
                    return <a className="carousel__item thumb-icon" href={slide.url} target="_blank" key={slide.name} rel="noopener noreferrer">
                      <div className="icon icon_m4m thumb-icon__icon"/>
                      <div className="thumb-icon__title">{slide.name}</div>
                    </a>
                  } else {
                    return <Link className="carousel__item thumb-icon" to={`${this.props.urlPrefix}${slide.url}`} key={slide.name}>
                      <div className={`icon icon_${slide.url} thumb-icon__icon`}/>
                      <div className="thumb-icon__title">{slide.name}</div>
                    </Link>
                  }
                }
              }
              return null;
            })}
          </Flickity>
        </div>
    )
  }
}
