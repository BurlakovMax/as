import React, {Component} from 'react';
import Input from "../form/input";
import config from "../../lib/config";

class FormSocial extends Component {
    render() {
        let values = this.props.values || {};

        return (
          <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
              <div className="container">
                  <div className="ad-creation-container">

                      <h2 className="weight-700 color-primary p-b-10">
                          Social Media
                      </h2>

                      <div className="wrapper-social-fields">

                          {config.getSocials().map((social, index) => {
                              let title = config.ucFirst(social);
                              return (
                                  <div key={index} className="social-row m-b-20 d-flex align-items-center">
                                      <div className="icon m-r-20">
                                          <label htmlFor={social} className="m-b-0">
                                              {social === 'instagram' ? (
                                                  <svg className={`icon_${social}_big`} width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                      <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="45" height="45">
                                                          <path fillRule="evenodd" clipRule="evenodd" d="M0 22.5C0 10.0736 10.0736 0 22.5 0C34.9264 0 45 10.0736 45 22.5C45 34.9264 34.9264 45 22.5 45C10.0736 45 0 34.9264 0 22.5Z" fill="white"/>
                                                      </mask>
                                                      <g mask="url(#mask0)">
                                                          <path fillRule="evenodd" clipRule="evenodd" d="M45.0003 35.7579C45.0003 40.8609 40.8611 45 35.7571 45H9.24321C4.1382 45 0 40.8609 0 35.7579V9.2456C0 4.14069 4.1382 -0.000427246 9.24321 -0.000427246H35.7571C40.8611 -0.000427246 45.0003 4.14069 45.0003 9.2456V35.7579Z" fill="url(#paint0_linear)"/>
                                                      </g>
                                                      <path fillRule="evenodd" clipRule="evenodd" d="M33.5862 27.1831C33.5862 31.1192 30.3925 34.3075 26.4563 34.3075H17.573C13.6343 34.3075 10.4414 31.1192 10.4414 27.1831V18.3077C10.4414 14.375 13.6343 11.1833 17.573 11.1833H26.4563C30.3925 11.1833 33.5862 14.375 33.5862 18.3077V27.1831ZM27.5111 8.4375H16.5184C11.6439 8.4375 7.69336 12.3854 7.69336 17.2556V28.2377C7.69336 33.1062 11.6439 37.0532 16.5184 37.0532H27.5111C32.3848 37.0532 36.3353 33.1062 36.3353 28.2377V17.2556C36.3353 12.3854 32.3848 8.4375 27.5111 8.4375Z" fill="white"/>
                                                      <path fillRule="evenodd" clipRule="evenodd" d="M22.3985 27.3299C19.89 27.3299 17.8582 25.2991 17.8582 22.7953C17.8582 20.289 19.89 18.2607 22.3985 18.2607C24.9045 18.2607 26.9372 20.289 26.9372 22.7953C26.9372 25.2991 24.9045 27.3299 22.3985 27.3299ZM22.3986 15.2587C18.2311 15.2587 14.8535 18.6316 14.8535 22.7953C14.8535 26.9565 18.2311 30.3328 22.3986 30.3328C26.5651 30.3328 29.9427 26.9565 29.9427 22.7953C29.9427 18.6316 26.5651 15.2587 22.3986 15.2587Z" fill="white"/>
                                                      <path fillRule="evenodd" clipRule="evenodd" d="M30.9375 15.0008C30.9375 16.0364 30.0963 16.875 29.0621 16.875C28.0262 16.875 27.1875 16.0364 27.1875 15.0008C27.1875 13.966 28.0262 13.125 29.0621 13.125C30.0963 13.125 30.9375 13.966 30.9375 15.0008Z" fill="white"/>
                                                      <defs>
                                                          <linearGradient id="paint0_linear" x1="22.7307" y1="-13.7202" x2="-13.5553" y2="23.8303" gradientUnits="userSpaceOnUse">
                                                              <stop stopColor="#6968DF"/>
                                                              <stop offset="0.385505" stopColor="#D22A9C"/>
                                                              <stop offset="1" stopColor="#FFD879"/>
                                                          </linearGradient>
                                                      </defs>
                                                  </svg>
                                              ) : (
                                                  <svg className={`icon_${social}_big`}>
                                                      <use xlinkHref={`/images/icons.svg#icon_${social}_big`}/>
                                                  </svg>
                                              )}
                                          </label>
                                      </div>
                                      <div className="field">
                                          <Input
                                              classes={'p-b-0'}
                                              id={social}
                                              name={social}
                                              type={'text'}
                                              value={values[social]}
                                              maxCharacters={255}
                                              placeholder={`Enter ${title} profile link`}
                                              handleChange={this.props.handleChange}
                                          />
                                      </div>
                                  </div>
                              )
                          })}

                      </div>
                  </div>
              </div>
          </div>
        )
    }
}

export default FormSocial;
