import React, {Component} from 'react';
import {isMobileSafari} from 'react-device-detect';

export default class Modal extends Component {
    getClasses = () => {
        let classes = ['modal'];

        // remove fade for ios safari
        if (!isMobileSafari) {
            classes.push('fade')
        }

        if (this.props.fullScreen) {
            classes.push('modal-full-screen')
        }

        if (this.props.classes) {
            classes.push(this.props.classes)
        }

        return classes.join(' ');
    }

    render() {
        let headerHide = this.props.headerHide || false;

        return (
            <div className={this.getClasses()} tabIndex="-1" role="dialog" id={this.props.id} data-backdrop="static">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">

                        {!headerHide &&
                            <div className={`modal-header ${this.props.headerArrow? 'modal-header__arrow' : ''}`}>

                                {/*header with back arrow*/}
                                {this.props.headerArrow &&
                                    <button onClick={this.props.handleClickArrow} className={'close-modal btn-link color-white'} data-dismiss="modal">
                                        <svg className="icon_header_back">
                                            <use xlinkHref="/images/icons.svg#icon_header_back"></use>
                                        </svg>
                                    </button>
                                }

                                <h2 className="weight-700 d-block w-100 text-center">
                                    {this.props.title}
                                </h2>

                                {/* default header with close button*/}
                                {!this.props.headerArrow &&
                                    <button href="/#" onClick={this.props.handleClickClose} className="close-modal btn-link color-secondary" data-dismiss="modal">
                                        {this.props.fullScreen ? (
                                          <svg className="icon_close_modal_white">
                                              <use xlinkHref="/images/icons.svg#icon_close_modal_white"/>
                                          </svg>
                                        ) : (
                                          <svg className="icon_alert_close">
                                              <use xlinkHref="/images/icons.svg#icon_alert_close"/>
                                          </svg>
                                        )}
                                    </button>
                                }
                            </div>
                        }

                        <div className="modal-body">
                            {this.props.content}
                        </div>

                        {this.props.footer &&
                            <div className="modal-footer">
                                {this.props.footer}
                            </div>
                        }

                    </div>
                </div>
            </div>
        )
    }
}
