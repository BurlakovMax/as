import React, {Component} from 'react';
import {Link} from "react-router-dom";
import auth from "../../lib/auth";

export default class Help extends Component {

    getClasses = () => {
        let classes = ['contact-for-help-container', 'bg-white', 'p-b-10'];

        if (this.props.classes) {
            classes.push(this.props.classes)
        }

        return classes.join(' ');
    }

    render() {
        return (
          <div className={this.getClasses()}>
              <div className="container">
                  <div className="contact-for-help-container-inner p-y-10">
                      <h2 className="color-primary weight-700 p-b-10">For help please contact</h2>
                      <span className="d-block color-tertiary">U.S. & Canada:</span>
                      <p><a href="tel:1-702-935-1688" className="color-tertiary weight-600">1-702-935-1688</a> (9am to 5pm EST Mon - Fri)</p>
                      <a href="mailto:support@adultsearch.com" className="color-tertiary weight-600 d-block">support@adultsearch.com</a>
                      <div className={'m-t-15'}>
                        <Link to={auth.getLink('contact')}>Contact Form</Link>
                      </div>
                  </div>
              </div>
          </div>
        )
    }
}
