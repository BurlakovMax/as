import React, {Component} from 'react';

export default class Or extends Component {
    getClasses = () => {
        let classes = ['separation-block'];

        if (this.props.classes) {
            classes.push(this.props.classes)
        }

        return classes.join(' ');
    }

    render() {
        return (
            <div className={this.getClasses()}>
                <div className="line"></div>
                <span className="word text-uppercase color-quaternary f-s-14 p-x-10">or</span>
                <div className="line"></div>
            </div>
        )
    }
}
