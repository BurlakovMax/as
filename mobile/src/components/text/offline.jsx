import React, {Component} from 'react';

export default class Offline extends Component {

    render() {
        return (
            <div className="fcontent">
                <div className="fcontainer">
                    <img className="icon_no_inet" src="/images/no-inet-icon.svg" alt="No Internet"/>
                    <div className="first-text">
                        <strong className="first-text">Sorry,</strong>
                        <br/>
                        <span className="second-text">this page is unavailable, if there's no Internet connection. To open it, please connect to Internet.</span>
                    </div>
                </div>
            </div>
        )
    }
}
