import Dexie from 'dexie';

const name = 'asng';
const version = 4;

const db = new Dexie(name);

db.version(version).stores({
    advertising: `id, typeSlug`,
    sync_cities: `id, updated_at`,
    breadcrumbs: `id, object_id, type`,
    cities: `id, name, url`,
    escort_sprav: `name`,
    escort_filters: `name`,
    escort_filters_selected: `id`,
    escort_types: `id`,
    escorts: `id, *locationIds, *languageIds, typeSlug, ethnicity, eyeColor, hairColor, postedAt`,
    forum_links: `id, locationId`,
    locations: `id`,
    locations_cities: `id, name`,
    locations_update: `id`,
    m4mlinks: `id`,
    place_attributes: `id, placeId`,
    place_information: `id, placeId`,
    place_reviews: `id, placeId`,
    place_rates: `id, placeId`,
    place_types: `id`,
    place_working_hours: `id`,
    places: `id, [typeId+locationId]`,
    sponsor_links: `id, *locationIds`,
    sponsors: `id, typeSlug`,
    position: `id`,
});

export default db;
