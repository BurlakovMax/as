import {Component} from 'react';
import {withRouter} from 'react-router-dom';
import auth from "./lib/auth";

class ScrollToTop extends Component {

    /**
     * Auto scroll to top when page changed
     * @param prevProps
     */
    componentDidUpdate(prevProps) {
        let goTop = true;

        // do not auto scroll when return to my ads
        if (this.props.location.pathname === auth.getLink('myposts')) {
            goTop = false;
        }

        if (goTop && this.props.location !== prevProps.location) {
            window.scrollTo(0, 0)
        }
    }

    render() {
        return this.props.children
    }
}

export default withRouter(ScrollToTop);
