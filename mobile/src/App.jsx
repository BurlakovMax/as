import React, {Component} from 'react';
import { Route, withRouter, Switch} from 'react-router-dom';
import {createComponent} from 'effector-react';
import './App.scss';
import $ from "jquery";
import offlineService from './services/offline';
import {offline} from './store/common';
import Alert from "./components/common/alert";
import ChangeEmail from "./pages/account/ChangeEmail";
import ConfirmEmail from "./pages/account/ConfirmEmail";
import ChangePassword from "./pages/account/ChangePassword";
import ChangeSettings from "./pages/account/ChangeSettings";
import ChangeUsername from "./pages/account/ChangeUsername";
import City from "./pages/locations/City";
import Confirm from "./pages/adbuild/confirm";
import ContactUs from "./pages/common_pages/contact_us_form";
import Escort from "./pages/escorts/Escort";
import EscortsCategory from "./pages/escorts/EscortsCategory";
import Footer from "./components/common/Footer";
import Header from "./components/common/Header";
import MainPage from "./pages/index/MainPage";
import MyAds from "./pages/account/MyAds";
import Account from "./pages/account/Account";
import PaymentForm from "./pages/payments/PaymentForm";
import PaymentRecharge from "./pages/payments/PaymentRecharge";
import Payments from "./pages/account/Payments";
import Place from "./pages/places/Place";
import PlacesCategory from "./pages/places/PlacesCategory";
import PlaceReview from "./pages/places/PlaceReview";
import PlaceAdd from "./pages/places/PlaceAdd";
import PrivacyPolicy from "./pages/common_pages/privacy_policy";
import Signin from "./pages/auth/Signin";
import Signup from "./pages/auth/Signup";
import SignupConfirm from "./pages/auth/SignupConfirm";
import Step1 from "./pages/adbuild/step1";
import Step2 from "./pages/adbuild/step2";
import auth from './lib/auth';
import locations from "./lib/locations";
import ResetPassword from "./pages/auth/ResetPassword";
import TermsOfService from "./pages/common_pages/terms_of_service";
import BuyTopups from "./pages/account/BuyTopups";
import BuyUpgrades from "./pages/account/BuyUpgrades";
import EditAd from "./pages/account/EditAd";
import ChangeLocation from "./pages/account/ChangeLocation";
import PrivateRoute from "./components/common/PrivateRoute";
import DuplicateAd from "./pages/account/DuplicateAd";
import BusinessownerPick from "./pages/businessowner/pick";
import EditAdThumbnail from "./pages/account/EditAdThumbnail";
import ForumIndex from "./pages/forum/ForumIndex";
import ForumTopicNew from "./pages/forum/ForumTopicNew";
import ForumTopic from "./pages/forum/ForumTopic";
import SidebarMenu from "./components/modal/SidebarMenu";
import Translations from "./components/modal/Translations";
import LocationsSearch from "./components/modal/LocationsSearch";
import Wire from "./pages/account/Wire";
import {isMobileSafari} from 'react-device-detect';

offlineService.checkOffline();

const OfflineAlert = createComponent(offline, (props, state) => (
    <div>
        { state ? (
            <Alert message="No Internet connection..." close={() => {}} />
        ) : '' }
    </div>
))

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            active: {},
            messagesCount: 0,
        }
    }

    getFooterActive = () => {
        let active = {};

        if (auth.isPage('home')) {
            active['home'] = true;
        } else if (auth.isPage('myposts')) {
            active['myposts'] = true;
        } else if (auth.isPage('adbuild')) {
            active['adbuild'] = true;
        } else if (auth.isPage('messages')) {
            active['messages'] = true;
        } else if (auth.isPage('signin')) {
            active['signin'] = true;
        } else if (auth.isPage('account')) {
            active['account'] = true;
        }

        this.setState({
            active: active
        });
    }

    async componentDidMount() {
        this.getFooterActive();

        if (auth.getToken()) {
            document.body.classList.add('auth');
        }

        if (isMobileSafari) {
            document.body.classList.add('mobile-safari');
        }

        await locations.getUpdates();
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.getFooterActive();
            this.onRouteChanged();
        }
    }

    onRouteChanged() {
        $('.modal').modal('hide');
    }

    render() {
        return (
            <>
                <OfflineAlert />

                <Header />

                {/*modals - all pages*/}
                <Translations />
                <LocationsSearch history={this.props.history} />
                <SidebarMenu />

                {/*AFTER ADD COMPONENT - ADD TITLE IN lib/config.js !!*/}
                <Switch>
                    <Route exact path="/" component={MainPage} />

                    <Route path="/businessowner/pick/:placeId" component={BusinessownerPick} />

                    {/*ESCORT MAIN FORM*/}
                    <Route path="/classifieds/myposts/:escortId/edit" render={(props) => <EditAd {...props} action={'edit'} />} />
                    <Route path="/classifieds/myposts/:escortId/renew" render={(props) => <EditAd {...props} action={'renew'} />} />
                    <Route path="/adbuild/step3" render={(props) => <EditAd {...props} action={'step3'} />} />

                    <PrivateRoute exact path="/account" component={Account} />
                    <PrivateRoute exact path="/account/change-email" component={ChangeEmail} />
                    <PrivateRoute exact path="/account/confirm-email" component={ConfirmEmail} />
                    <PrivateRoute exact path="/account/change-password" component={ChangePassword} />
                    <PrivateRoute exact path="/account/change-settings" component={ChangeSettings} />
                    <PrivateRoute exact path="/account/change-username" component={ChangeUsername} />
                    <PrivateRoute exact path="/account/payments" component={Payments} />
                    <PrivateRoute exact path="/account/wire" component={Wire} />
                    <Route exact path="/account/confirm" component={SignupConfirm} />
                    <Route exact path="/account/reset" component={ResetPassword} />
                    <Route exact path="/account/signin" component={Signin} />
                    <Route exact path="/account/signup" component={Signup} />

                    <Route exact path="/adbuild" component={Step1} />
                    <Route exact path="/adbuild/step2" component={Step2} />
                    <Route exact path="/adbuild/confirm" component={Confirm} />

                    <PrivateRoute exact path="/classifieds/myposts" component={MyAds} />
                    <PrivateRoute path="/classifieds/myposts/:escortId/change_location" component={ChangeLocation} />
                    <PrivateRoute path="/classifieds/myposts/:escortId/edit_thumbnail" component={EditAdThumbnail} />
                    <PrivateRoute path="/classifieds/myposts/:escortId/duplicate" component={DuplicateAd} />
                    <PrivateRoute exact path="/classifieds/topups" component={BuyTopups} />
                    <PrivateRoute path="/classifieds/myposts/:escortId/:escortAction" component={BuyUpgrades} />

                    <PrivateRoute path="/payment/pay" component={PaymentForm} />
                    <PrivateRoute path="/payment/recharge" component={PaymentRecharge} />

                    <Route exact path="/contact_us" component={ContactUs} />
                    <Route exact path="/privacy_policy" component={PrivacyPolicy} />
                    <Route exact path="/terms_of_service" component={TermsOfService} />
                    <PrivateRoute path='/messenger' component={() => {
                        if (auth.getToken()) {
                            window.location.href = auth.getLink('chat');
                        } else {
                            this.props.history.push(auth.getLink('signin'));
                        }
                        return null;
                    }}/>

                    <Route exact path="/:stateName/:cityName" component={City} />

                    {/*ESCORT*/}
                    <Route path={`/:stateName/:cityName/female-escorts/:escortId`} component={Escort} />
                    <Route path={`/:stateName/:cityName/tstv-shemale-escorts/:escortId`} component={Escort} />
                    <Route path={`/:stateName/:cityName/body-rubs/:escortId`} component={Escort} />

                    {/*ESCORT - CATEGORY*/}
                    <Route exact path={`/:stateName/:cityName/female-escorts`} component={EscortsCategory} />
                    <Route exact path={`/:stateName/:cityName/tstv-shemale-escorts`} component={EscortsCategory} />
                    <Route exact path={`/:stateName/:cityName/body-rubs`} component={EscortsCategory} />

                    {/*PLACE - SUBMIT*/}
                    <Route path={`/:stateName/:cityName/add`} component={PlaceAdd} />

                    {/*PLACE - REVIEW*/}
                    <Route path={`/:stateName/:cityName/erotic-massage-parlor/:placeId/add_review`} component={PlaceReview} />
                    <Route path={`/:stateName/:cityName/gay-bath-house/:placeId/add_review`} component={PlaceReview} />
                    <Route path={`/:stateName/:cityName/lingerie-modeling-1on1/:placeId/add_review`} component={PlaceReview} />
                    <Route path={`/:stateName/:cityName/sex-shop/:placeId/add_review`} component={PlaceReview} />
                    <Route path={`/:stateName/:cityName/strip-club/:placeId/add_review`} component={PlaceReview} />

                    {/*PLACE*/}
                    <Route path={`/:stateName/:cityName/erotic-massage-parlor/:placeId`} component={Place} />
                    <Route path={`/:stateName/:cityName/gay-bath-house/:placeId`} component={Place} />
                    <Route path={`/:stateName/:cityName/lingerie-modeling-1on1/:placeId`} component={Place} />
                    <Route path={`/:stateName/:cityName/sex-shop/:placeId`} component={Place} />
                    <Route path={`/:stateName/:cityName/strip-club/:placeId`} component={Place} />

                    {/*PLACE - CATEGORY*/}
                    <Route exact path={`/:stateName/:cityName/erotic-massage-parlor`} component={PlacesCategory} />
                    <Route exact path={`/:stateName/:cityName/gay-bath-house`} component={PlacesCategory} />
                    <Route exact path={`/:stateName/:cityName/lingerie-modeling-1on1`} component={PlacesCategory} />
                    <Route exact path={`/:stateName/:cityName/sex-shop`} component={PlacesCategory} />
                    <Route exact path={`/:stateName/:cityName/strip-club`} component={PlacesCategory} />

                    {/*FORUMS*/}
                    <Route path={`/:stateName/:cityName/sex-forum/:forumSlug/new-topic`} component={ForumTopicNew} />
                    <Route path={`/:stateName/:cityName/sex-forum/:forumSlug/topic/:topicId`} component={ForumTopic} />
                    <Route path={`/:stateName/:cityName/sex-forum/:forumSlug`} component={ForumIndex} />

                    <Route exact path="*" component={MainPage} />
                </Switch>

                <Footer active={this.state.active} messagesCount={this.state.messagesCount} />
            </>
        );
    }
}

export default withRouter(App);
