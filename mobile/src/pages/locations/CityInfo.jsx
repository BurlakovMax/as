import React, {Component} from 'react';
import {Link} from "react-router-dom";
import LoadingSpinner from "../../components/common/loading-spinner";
import escorts from "../../services/escorts";
import places from "../../services/places";
import sponsors from "../../services/sponsors";
import forums from "../../services/forums";
import m4mlinks from "../../services/m4m-links";
import cities from "../../services/cities";
import {changeTitle} from "../../store/common";
import config from "../../lib/config";
import ButtonSubmit from "../../components/form/button-submit";
import auth from "../../lib/auth";
import background from "../../services/background";

class CityInfo extends Component {
    constructor(props) {
        super(props);

        changeTitle('');

        this.state = {
            loading: false,
            account: {},

            escortTypes: [],
            placeTypes: [],
            sponsors: [],
            forums: [],
            city: {},
            m4mLink: {},
        };
    }

    getData = async () => {
        const escortTypesList = escorts.getTypes(this.state.city.id, 'city').catch(error => config.catch(error));
        const placeTypesList = places.getTypes(this.state.city.id, 'city').catch(error => config.catch(error));
        const sponsorsList = sponsors.getPlace(this.state.city.id).catch(error => config.catch(error));
        const forumsList = forums.getForums(this.state.city.id, 1).catch(error => config.catch(error));
        const m4mLink = m4mlinks.getLink(this.state.city.id).catch(error => config.catch(error));

        let states = await Promise.all([escortTypesList, placeTypesList, sponsorsList, forumsList, m4mLink]);
        config.debug('data', states);

        await this.setState({
            escortTypes: Array.isArray(states[0]) ? states[0] : [],
            placeTypes: Array.isArray(states[1]) ? states[1] : [],
            sponsors: Array.isArray(states[2]) ? states[2] : [],
            forums: Array.isArray(states[3]) ? states[3] : [],
            m4mLink: states[4],
        });
    }

    getCity = async () => {
        let city = await cities.getByUrl(
          this.props.match.params.stateName,
          this.props.match.params.cityName
        );
        config.setCity(city);

        this.setState({
            city: city
        });
    }

    getEscortUrl = (escortType) => {
        let url = {
            external: false,
            url: '/#'
        };

        if(escortType.url === undefined) {
            return url;
        }

        if (escortType.url.toLowerCase().startsWith('http')) {
            url = {
                external: true,
                url: escortType.url
            }
        } else {
            url = {
                external: false,
                url: `${this.state.city.url}${escortType.url}`
            }
        }

        return url;
    }

    // initial city background sync
    syncCity = async () => {
        setTimeout(async () => {
            localStorage.setItem('sync', this.state.city.id);
            await background.syncCity(this.state.city.id)
                .finally(() => {
                    localStorage.removeItem('sync')
                })
        }, 10000);
    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount(true)
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getCity();
        await this.getData();
        await this.syncCity();

        this.setState({loading: false});
    }

    render() {
        const escortTypes = [...this.state.escortTypes, ...[this.state.m4mLink]];

        return (
            <div>

                <div className="m-y-10 bg-white">
                    <div className="container">

                        {/*EROTIC SERVICES*/}
                        <h2 className="weight-700 p-t-15 color-primary">Erotic Services in { this.state.city.name }</h2>
                        { escortTypes.map((escortType, i) =>
                            {
                                let url = this.getEscortUrl(escortType);
                                return (
                                  url.external ? (
                                    <div key={i} className="with-counter-btn">
                                        <a href={url.url} target={'_blank'} rel="noopener noreferrer" className="with-counter-btn_link">
                                            <span className="text">{ escortType.name }</span>
                                            <span className="count d-flex align-items-center justify-content-center position-relative m-l-20 m-r-30">
                                                { escortType.count || 0 }
                                            </span>
                                        </a>
                                    </div>
                                  ) : (
                                    <div key={i} className="with-counter-btn">
                                        <Link to={url.url} className="with-counter-btn_link">
                                            <span className="text">{ escortType.name }</span>
                                            <span className="count d-flex align-items-center justify-content-center position-relative m-l-20 m-r-30">
                                                { escortType.count || 0 }
                                            </span>
                                        </Link>
                                    </div>
                                  )
                                )
                            }
                        )}

                        {/*EROTIC SERVICES - SPONSORS*/}
                        { this.state.sponsors.length > 0 &&
                        <div className="city-advertisement-container p-b-10">
                            { this.state.sponsors.map(sponsor =>
                                <div key={sponsor.id} data-id={sponsor.id} className="city-advertisement bg-white m-t-5">
                                    <Link to={`${this.state.city.url}${sponsor.typeSlug}/${sponsor.id}`} className="content-item bg-white d-flex justify-content-between flex-nowrap">
                                        <div className="left d-flex flex-nowrap">
                                            <img className="m-r-10" src={`${config.getImagePath('escort')}/${ sponsor.thumbnail }`} alt="sponsor" />
                                            <div className="information m-t-5">
                                                <div className="weight-600 color-tertiary">{ sponsor.type }</div>
                                                <span className="d-block">{ sponsor.ethnicity }</span>
                                            </div>
                                        </div>

                                        <div className="right d-flex flex-nowrap m-t-5 m-r-5 color-primary">
                                            { sponsor.isIncallEnabled && <div className="incall m-r-5">Incall</div>}
                                            { (sponsor.isIncallEnabled && sponsor.isOutcallEnabled) && <span>/</span>}
                                            { sponsor.isOutcallEnabled && <div className="outcall m-l-5">Outcall</div>}
                                        </div>
                                    </Link>
                                </div>
                            )}
                        </div>
                        }
                    </div>
                </div>

                {/*ADULT PLACES*/}
                {this.state.placeTypes.length > 0 &&
                <div className="m-y-5 bg-white">
                    <div className="container">
                        <h2 className="weight-700 p-t-15 color-primary">Adult Places in {this.state.city.name}</h2>
                        {this.state.placeTypes.map(placeType =>
                          <div key={placeType.url} className="with-counter-btn">
                              <Link to={`${this.state.city.url}${placeType.url}`} className="with-counter-btn_link">
                                  <span className="text">{placeType.name}</span>
                                  <span className="count d-flex align-items-center justify-content-center position-relative m-l-20 m-r-30">
                                    {placeType.count}
                                  </span>
                              </Link>
                          </div>
                        )}
                    </div>
                </div>
                }

                {/*FORUMS*/}
                {this.state.forums.length > 0 &&
                <div className="m-y-10 bg-white">
                    <div className="container">
                        <h2 className="weight-700 p-t-15 color-primary">Sex Forums</h2>
                        {this.state.forums.map(forum =>
                          <div key={forum.url} className="with-counter-btn">
                              <Link to={`${this.state.city.url}sex-forum/${forum.url}`} className="with-counter-btn_link">
                                  <span className="text">{forum.name}</span>
                                  <span className="count d-flex align-items-center justify-content-center position-relative m-l-20 m-r-30">
                                    {forum.count}
                                  </span>
                              </Link>
                          </div>
                        )}
                    </div>
                </div>
                }

                {this.state.account.id &&
                    <div className="bg-white m-y-10">
                        <div className="container">
                            <div className="page-actions p-y-15 d-flex flex-row justify-content-between">
                                <ButtonSubmit link={`${this.state.city.url}add`} text={'Add New Place'}/>
                            </div>
                        </div>
                    </div>
                }

                <LoadingSpinner loading={this.state.loading} />
            </div>
        );
    }
}

export default CityInfo;
