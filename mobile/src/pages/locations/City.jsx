import React, {Component} from 'react';
import {Switch, Route, withRouter} from "react-router-dom";
import breadcrumbs from "../../services/breadcrumbs.js";
import escorts from "../../services/escorts";
import places from "../../services/places";
import m4mlinks from "../../services/m4m-links.js";
import cities from "../../services/cities.js";
import NavigationCarousel from "../../components/common/navigation-carousel";
import Breadcrumbs from "../../components/common/breadcrumbs";
import CityInfo from "./CityInfo";
import Offline from "../../components/text/offline";
import config from "../../lib/config";

class City extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,

            links: [],
            escortTypes: [],
            placeTypes: [],
            city: {},
            m4mLink: {},
        };
    }

    getData = async () => {
        await this.setState({
            links: await breadcrumbs.getBreadcrumbs(this.state.city.id, 'city'),
            escortTypes: await escorts.getTypes(this.state.city.id, 'city'),
            placeTypes: await places.getTypes(this.state.city.id, 'city'),
            m4mLink: await m4mlinks.getLink(this.state.city.id),
        });
    }

    async componentDidMount() {
        await this.setState({loading: true});

        try {
            let city = await cities.getByUrl(
                this.props.match.params.stateName,
                this.props.match.params.cityName,
            );
            config.setCity(city);

            await this.setState({
                city: city
            })

            await this.getData();
        } catch (error) {
            config.catch(error);
        } finally {
            await this.setState({loading: false});
        }
    }

    render() {
        const slides = [...this.state.escortTypes, ...[this.state.m4mLink], ...this.state.placeTypes];
        const {path} = this.props.match;

        return (
          <div className="content city" id="city-page">

              { this.state.city.id === null ? (
                <Offline/>
              ) : (
                  <>
                      <Breadcrumbs links={this.state.links} />

                      {this.state.city.url && <NavigationCarousel className={'bg-white'} urlPrefix={this.state.city.url} slides={slides} />}

                      <Switch>
                          <Route path={path} component={CityInfo} />
                      </Switch>
                  </>
              ) }

            </div>
        )
    }
}

export default withRouter(City);
