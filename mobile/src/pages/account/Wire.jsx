import React, {Component} from 'react';
import axios from '../../services/api_init';
import auth from "../../lib/auth";
import LoadingSpinner from "../../components/common/loading-spinner";
import Alert from "../../components/common/alert";
import ButtonSubmit from "../../components/form/button-submit";
import Input from "../../components/form/input";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import ButtonBordered from "../../components/form/button-bordered";
import uploader from "../../lib/uploader";
import FormPhoto from "../../components/common/FormPhoto";
import form from "../../lib/form";

export default class Wire extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            errorMessage: false,
            successMessage: false,
            loading: false,
            errors: {},

            pictures: [],
            picturesMax: 1,
            picturesLimit: false,
            picturesError: false,

            data: {
                name: null,
                amount: null,
                bank_name: null
            }
        };
    }

    fillForm = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            data: Object.assign(this.state.data, res)
        });
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;

        let errors = {
            'name': 'Please type valid name',
            'amount': 'Please type amount digit',
            'bank_name': 'Please type valid bank name',
        };

        for (let id in errors) {
            if (document.getElementById(id).value) {
                errors[id] = false
            } else {
                ok = false;
            }
        }

        this.setState({
            errors: errors
        });

        // check image uploaded
        let picturesError = form.validatePictures(this.state.pictures.length)
        if (picturesError) {
            this.setState({
                picturesError: picturesError
            });
            ok = false;
        }

        return ok;
    }

    submit = async event => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false,
            successMessage: false,
            picturesError: false,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        this.setState({
            loading: true,
        });

        try {
            await axios.post('/api/v1/pc/account/wire', {
                name: this.state.data.name,
                bankName: this.state.data.bank_name,
                amount: this.state.data.amount,
                image: this.state.pictures[0].name
            });

            this.setState({
                loading: false,
                successMessage: 'Your wire info has been sent',
            });
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    deletePhoto = async event => {
        event.preventDefault();

        let result = await uploader.deletePhoto(
          event,
          this.state.pictures,
          this.state.picturesMax,
        );

        this.setState({
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })
    }

    uploadPhoto = async event => {
        event.preventDefault();

        let result = await uploader.uploadPhoto(
          event,
          this.state.pictures,
          this.state.picturesMax
        );

        this.setState({
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })
    }


    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await auth.getAccount()
          .then(account => {
              if (!account || !account.agency) {
                  auth.gotoAccount();
              }
          })

        this.setState({ loading: false });
    }

    render() {
        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

          {this.state.successMessage &&
          <>
            <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>
              <div className="content wire">
                  <div className="bg-white p-y-20">
                      <div className="container">
                          <ButtonSubmit text={'Return to AS account'} link={auth.getLink('account')}/>
                      </div>
                  </div>
              </div>
          </>
          }

          {!this.state.successMessage &&
            <div className="content wire">
              <form action="#" method="POST" onSubmit={this.submit}>
                  <div className="bg-white p-y-20 m-b-10">
                      <div className="container">

                          <h2 className="weight-700 color-primary m-b-20">Let us know you sent us wire</h2>

                          <p>
                              After you wire us money, you need to submit form below. Do not send us emails anymore.
                              If you don't fill out form below, it might take days or weeks to add your money to the budget.
                          </p>

                          <p className="m-b-20">
                              All fields are mandatory.
                          </p>

                          <Input
                            label={'Senders Name'}
                            type={'text'}
                            required={true}
                            required_icon={true}
                            handleChange={this.fillForm}
                            name={'name'}
                            id={'name'}
                            placeholder={'Enter Senders Name'}
                            error={this.state.errors.name}
                          />

                          <Input
                            label={'Account ID Number'}
                            type={'text'}
                            disabled={true}
                            name={'account_id'}
                            id={'account_id'}
                            value={config.getBankAccountId()}
                            error={this.state.errors.account_id}
                            required_icon={true}
                          />

                          <Input
                            label={'Wire Amount'}
                            type={'number'}
                            required={true}
                            required_icon={true}
                            handleChange={this.fillForm}
                            name={'amount'}
                            id={'amount'}
                            placeholder={'Enter Wire Amount'}
                            error={this.state.errors.amount}
                            attributes={{'min': 0, 'inputMode': 'numeric', 'pattern': '\d*'}}
                          />

                          <Input
                            label={'Bank Sent From'}
                            type={'text'}
                            required={true}
                            required_icon={true}
                            handleChange={this.fillForm}
                            name={'bank_name'}
                            id={'bank_name'}
                            placeholder={'Enter Bank Sent From'}
                            error={this.state.errors.bank_name}
                          />

                      </div>
                  </div>


                  <FormPhoto
                    type={'wire'}
                    pictures={this.state.pictures}
                    picturesMax={this.state.picturesMax}
                    picturesLimit={this.state.picturesLimit}
                    picturesError={this.state.picturesError}
                    uploadHandle={this.uploadPhoto}
                    deleteHandle={this.deletePhoto}
                  />


                  <div className="bg-white p-y-20">
                      <div className="container">
                          <ButtonSubmit classes={'m-b-20'}/>
                          <ButtonBordered text={'Cancel'} link={auth.getLink('account')}/>
                      </div>
                  </div>

              </form>
          </div>
          }

              <LoadingSpinner loading={this.state.loading} />
          </>
        );
    }
}
