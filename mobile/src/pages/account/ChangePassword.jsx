import React, {Component} from 'react';
import axios from '../../services/api_init';
import auth from "../../lib/auth";
import LoadingSpinner from "../../components/common/loading-spinner";
import Alert from "../../components/common/alert";
import ButtonSubmit from "../../components/form/button-submit";
import Input from "../../components/form/input";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            errorMessage: false,
            loading: false,
            errors: {},

            data: {
                current_password: null,
                password: null,
                password_confirm: null
            }
        };
    }

    fillForm = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            data: Object.assign(this.state.data, res)
        });
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;

        let errors = {
            'current_password': 'Please type current password',
            'password': 'Please type new password',
            'password_confirm': 'Please confirm new password',
        };

        for (let id in errors) {
            if (document.getElementById(id).value) {
                errors[id] = false
            } else {
                ok = false;
            }
        }

        if (this.state.data.password !== this.state.data.password_confirm) {
            errors['password_confirm'] = 'New password and confirm password do not match';
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    submit = async event => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        try {
            await axios.put('/api/v1/pc/account/change_password', {
                password: this.state.data.password,
                oldPassword: this.state.data.current_password,
            });

            auth.gotoAccount();
        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: error.response.data.message,
            });
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    async componentDidMount() {
        this.setState({ loading: true });
        await auth.getAccount();
        this.setState({ loading: false });
    }

    render() {
        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

          <div className="content settings" id="change-password-page">
            <div className="change-password p-y-15 bg-white">
                <div className="container">
                    <form action="#" method="POST" onSubmit={this.submit}>
                        <Input
                          label={'Current password'}
                          type={'password'}
                          required={true}
                          handleChange={this.fillForm}
                          name={'current_password'}
                          id={'current_password'}
                          placeholder={'Enter current password here'}
                          error={this.state.errors.current_password}
                        />

                        <Input
                          label={'New password'}
                          type={'password'}
                          required={true}
                          handleChange={this.fillForm}
                          name={'password'}
                          id={'password'}
                          placeholder={'Enter new password here'}
                          minLength={6}
                          maxLength={20}
                          error={this.state.errors.password}
                        />

                        <Input
                          label={'Confirm new password'}
                          type={'password'}
                          required={true}
                          handleChange={this.fillForm}
                          name={'password_confirm'}
                          id={'password_confirm'}
                          placeholder={'Confirm new password'}
                          minLength={6}
                          maxLength={20}
                          error={this.state.errors.password_confirm}
                        />

                        <ButtonSubmit text={'Save'}/>
                    </form>
                </div>

                <LoadingSpinner loading={this.state.loading} />
            </div>
          </div>
          </>
        );
    }
}
