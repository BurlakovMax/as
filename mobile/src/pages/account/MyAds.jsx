import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from '../../services/api_init';
import Moment from "react-moment";
import LoadingSpinner from "../../components/common/loading-spinner";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import auth from "../../lib/auth";
import adbuild from "../../lib/adbuild";
import {offline} from '../../store/common';
import Offline from "../../components/text/offline";
import Alert from "../../components/common/alert";
import {history} from "../../index";
import escorts from "../../services/escorts";
import ratingStars from "../../components/common/rating-stars";
import ButtonSubmit from "../../components/form/button-submit";
import payment from "../../lib/payment";

export default class MyAds extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            errorMessage: false,
            successMessage: false,
            account: {},

            budget: 0,
            counters: {},
            places: [],
            placesTypes: {},
            escorts: [],
            escortsUpgrades: {},

            showMenu: {},
        };
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    showMenu = id => event => {
        event.preventDefault();

        let showMenu = this.state.showMenu;
        showMenu[id] = !showMenu[id];

        this.setState({
            showMenu: showMenu
        });
    }

    deleteEscort = async event => {
        event.preventDefault();

        const escortId = parseInt(event.currentTarget.id);

        if (window.confirm('Are you sure to delete this ad?')) {
            try {
                await axios.delete(`/api/v1/pc/escorts/${escortId}`);

                await this.getEscorts();
            } catch (error) {
                config.catch(error);
            }
        }
    }

    getEscorts = async () => {
        try {
            const res = await axios.get('/api/v1/pc/escorts');

            this.setState({
                escorts: res.data.list,
            });

            config.debug('ESCORTS', this.state.escorts);

            // clear finish cookies if user has no ads
            if (res.data.total === 0) {
                adbuild.clearCookie(true);
            }
        } catch (error) {
            config.catch(error);
        }
    }

    getEscortTopup = escortId => async event => {
        event.preventDefault();

        this.setState({
            loading: true,
            errorMessage: null,
            successMessage: null,
        });

        try {
            await axios.post(`/api/v1/pc/escorts/${escortId}/move_to_top`);

            await auth.getAccount()
                .then(account => {
                    if (account) {
                        this.setState({
                            account: account,
                        });
                    }
                })

            // update escort postedAt in local db
            await escorts.getById(escortId, true);

            this.setState({
                loading: false,
                successMessage: 'Your ad successfully moved to top',
            });
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    getEscortCounters = async () => {
        try {
            const res = await axios.get('/api/v1/pc/escorts/counters');
            this.setState({
                counters: res.data
            });
        } catch (error) {
            config.catch(error);
        }
    }

    getEscortsUpgrades = async () => {
        await Promise.all(
            this.state.escorts.map(
                escort => this.getEscortUpgrade(escort)
            )
        )
    }

    getEscortUpgrade = async escort => {
        let escortsUpgrades = this.state.escortsUpgrades

        try {
            const res = await axios.get(`/api/v1/pc/escorts/${escort.id}/sponsorship`);

            escortsUpgrades[escort.id] = res.data;
            this.setState({
                escortsUpgrades: escortsUpgrades
            });
        } catch (error) {
            config.catch(error);
        }
    }

    canBuyUpgrade = (escort, type) => {
        let canBuy = true;
        let escortsUpgrades = this.state.escortsUpgrades;

        if (escortsUpgrades[escort.id]) {
            if (type === 'sponsor') {
                if (escortsUpgrades[escort.id]['escortSponsorLocationIds'].length) {
                    canBuy = false;
                }
            } else if (type === 'side') {
                if (escortsUpgrades[escort.id]['escortSideLocationIds'].length) {
                    canBuy = false;
                }
            } else if (type === 'sticky') {
                if (escortsUpgrades[escort.id]['escortStickyMonthLocationIds'].length || escortsUpgrades[escort.id].escortStickyWeekLocationIds.length) {
                    canBuy = false;
                }
            }
        }

        return canBuy;
    }

    getPlaces = async () => {
        try {
            const res = await axios.get('/api/v1/pc/places?status=active&limit=10&offset=0');

            this.setState({
                places: res.data.list,
            });

            config.debug('PLACES', this.state.places);
        } catch (error) {
            config.catch(error);
        }
    }

    getPlacesTypes = async () => {
        try {
            const res = await axios.get('/api/v1/placeTypes');

            // parse to object with keys
            let list = {};
            res.data.list.map(type => {
                list[type.id] = type;
                return null;
            })

            this.setState({
                placesTypes: list
            });
        } catch (error) {
            config.catch(error);
        }
    }


    /**
     * Create payment link for unpayed escort (with upgrades)
     * @returns {string} /payment/pay?paymentType=combo&escortId=1896991&src=escort&locationIds=1279
     * &escortSponsorLocationIds=1279
     * &escortSideLocationIds=1279
     * &escortStickyWeekLocationIds=1279 || &escortStickyMonthLocationIds=1279
     */
    gotoPayment = escort => async event => {
        event.preventDefault();

        let data = [];
        let hasUpgrades = false;
        let upgradesUrl = '';
        let types = config.getEscortUpgradeType();

        try {
            const res = await axios.get(`/api/v1/pc/escorts/${escort.id}/sponsorship`);
            data = res.data;

        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: 'Error creating payment link',
            });

            return null;
        }

        types.map(type => {
            if (data[type] && data[type].length) {
                hasUpgrades = true;

                let upgradesIds = [];
                data[type].map(sponsor => {
                    upgradesIds.push(sponsor.locationId);
                    return null;
                })
                upgradesUrl += '&' + type + '=' + upgradesIds.join(',');
            }
            return null;
        })

        let params = {
            paymentType: (hasUpgrades) ? 'combo' : 'escort',
            escortId: escort.id,
            locationIds: escort.locationIds,
            src: 'escortPay',
        };

        let link = config.getPaymentUrl(params) + upgradesUrl;

        history.push(link);
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await auth.getAccount()
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getEscortCounters();
        await this.getEscorts();
        await this.getEscortsUpgrades();

        await this.getPlacesTypes();
        await this.getPlaces();

        this.setState({
            budget: await payment.getBudget()
        });

        this.setState({ loading: false });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.places !== this.state.places) {
            // update rating
            ratingStars();
        }
    }

    renderEscorts() {
        return (
            <div className="tab-pane fade show active" id="profiles" role="tabpanel" aria-labelledby="profiles-tab">

                {/*LIST*/}
                {this.state.escorts.map(escort =>
                    <div key={escort.id} id={escort.id} className={`wrapper-my-ads-container bg-white p-t-10 m-b-10 all-escorts ${escort.status === 1 ? 'active' : 'expired'}`}>
                        <div className="container">
                            <div className="my-ads-container">
                                <div className="ad-id p-b-10">
                                    <div className="weight-600 f-s-16">
                                        {escort.FirstName} {escort.title}
                                        <span className={`escort-status m-l-5 ${config.getEscortStatus(escort, true)}`}>{config.getEscortStatus(escort)}</span>
                                    </div>
                                </div>

                                {/*AD INFO*/}
                                <div className="info-container d-flex align-items-start m-b-10">
                                    <div className="my-ads-photo m-r-5">
                                        {escort.images.length > 0 ?
                                            (<img src={`${config.getImagePath('escort')}/${escort.thumbnail}`} alt="My Ad" className="photo"/>)
                                            :
                                            (<img src="/images/icons/User_1.jpg" alt="myAdsPhoto" className="photo"/>)
                                        }
                                    </div>
                                    <div className="info">
                                        {escort.locations.map(location =>
                                            <p key={location.id} className="lh-normal m-b-5"><span className="color-label f-s-16 m-r-5">City:</span> {location.cityName}, {location.stateName}</p>
                                        )}
                                        <p className="lh-normal m-b-5">
                                            <span className="color-label f-s-16 m-r-5">Category:</span> {escort.type}
                                        </p>
                                        <p className="lh-normal m-b-5">
                                            <span className="color-label f-s-16 m-r-5">Post Date:</span> <Moment format="YYYY/MM/DD">{escort.postedAt}</Moment>
                                        </p>
                                        {escort.expireStamp &&
                                        <p className="lh-normal m-b-5">
                                            <span className="color-label f-s-16 m-r-5">Expires in:</span> <Moment format="YYYY/MM/DD">{escort.expireStamp}</Moment>
                                        </p>
                                        }
                                    </div>
                                </div>

                                {/*AD CONTROLS*/}
                                <div className="secondary-info-link-group d-flex align-items-center justify-content-between flex-nowrap color-secondary" id={escort.id}>
                                    <Link to={`${escort.locations[0].url}${escort.typeSlug}/${escort.id}`} className="link-container justify-content-start">
                                        <div className="icon">
                                            <svg className="icon_view_ad">
                                                <use xlinkHref="/images/icons.svg#icon_view_ad"/>
                                            </svg>
                                        </div>
                                        <span className="title">View Ad</span>
                                    </Link>
                                    <div className="link-container justify-content-end manage-ad" onClick={this.showMenu(escort.id)}>
                                        <div className="icon">
                                            <svg className="icon_gear">
                                                <use xlinkHref="/images/icons.svg#icon_gear"/>
                                            </svg>
                                        </div>
                                        <span className="title">Manage Ad</span>
                                    </div>
                                    {(escort.status > 0 && this.state.account.topUps > 0) &&
                                    <Link to={'/#'} onClick={this.getEscortTopup(escort.id)} className="link-container justify-content-end">
                                        <div className="icon">
                                            <svg className="icon_upload">
                                                <use xlinkHref="/images/icons.svg#icon_upload"/>
                                            </svg>
                                        </div>
                                        <span className="title">Top Up Ad</span>
                                    </Link>
                                    }
                                    {escort.status < 0 &&
                                    <Link to={'/#'} onClick={this.gotoPayment(escort)} className="link-container link-container__pay_url justify-content-end">
                                        <div className="icon">
                                            <svg className="payments">
                                                <use xlinkHref="/images/icons.svg#payments"/>
                                            </svg>
                                        </div>
                                        <span className="title weight-600">Pay Ad</span>
                                    </Link>
                                    }
                                </div>

                                {/*AD MENU*/}
                                <div className={`collapse ${this.state.showMenu[escort.id]? 'show' : ''}`} aria-labelledby="ad-heading-2" data-parent="#ads-accordion">
                                    <div className="card-body bg-white p-x-0 p-y-0 m-t-10">
                                        <div className="container">
                                            <div className="list-with-icons">
                                                <ul className="button-list">
                                                    {(escort.status > 0 && escort.status !== 3) &&
                                                    <>
                                                        <li>
                                                            {this.canBuyUpgrade(escort, 'sponsor') ? (
                                                                <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/escortSponsor`}>
                                                                    <div className="icon-with-title m-r-10">
                                                                        <div className="icon m-r-10 dark-blue">
                                                                            <svg className="icon_grid">
                                                                                <use xlinkHref="/images/icons.svg#icon_grid"/>
                                                                            </svg>
                                                                        </div>
                                                                        <span className="color-quaternary">Buy City Thumbnail</span>
                                                                    </div>
                                                                    <div className="arrow-right">
                                                                        <svg className="arrow-right-icon">
                                                                            <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                        </svg>
                                                                    </div>
                                                                </Link>
                                                            ) : (
                                                                <div className="link p-y-15">
                                                                    <div className="icon-with-title m-r-10">
                                                                        <div className="icon m-r-10 dark-blue">
                                                                            <svg className="icon_grid">
                                                                                <use xlinkHref="/images/icons.svg#icon_grid"/>
                                                                            </svg>
                                                                        </div>
                                                                        <span className="color-quaternary">Buy City Thumbnail</span>
                                                                    </div>
                                                                    <div className="arrow-right">
                                                                        <svg className="icon_check">
                                                                            <use xlinkHref="/images/icons.svg#icon_check"/>
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            )}
                                                        </li>

                                                        <li>
                                                            {this.canBuyUpgrade(escort, 'side')? (
                                                                <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/escortSide`}>
                                                                    <div className="icon-with-title m-r-10">
                                                                        <div className="icon m-r-10 pink-pale">
                                                                            <svg className="icon_monitor">
                                                                                <use xlinkHref="/images/icons.svg#icon_monitor"/>
                                                                            </svg>
                                                                        </div>
                                                                        <span className="color-quaternary">Buy Side Sponsor</span>
                                                                    </div>
                                                                    <div className="arrow-right">
                                                                        <svg className="arrow-right-icon">
                                                                            <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                        </svg>
                                                                    </div>
                                                                </Link>
                                                            ) : (
                                                                <div className="link p-y-15">
                                                                    <div className="icon-with-title m-r-10">
                                                                        <div className="icon m-r-10 pink-pale">
                                                                            <svg className="icon_monitor">
                                                                                <use xlinkHref="/images/icons.svg#icon_monitor"/>
                                                                            </svg>
                                                                        </div>
                                                                        <span className="color-quaternary">Buy Side Sponsor</span>
                                                                    </div>
                                                                    <div className="arrow-right">
                                                                        <svg className="icon_check">
                                                                            <use xlinkHref="/images/icons.svg#icon_check"/>
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            )}
                                                        </li>

                                                        {((parseInt(escort.rawType) === 2 || this.state.account.agency) && this.canBuyUpgrade(escort, 'sticky')) ? (
                                                            <li>
                                                                <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/escortSticky`}>
                                                                    <div className="icon-with-title m-r-10">
                                                                        <div className="icon m-r-10 yellow">
                                                                            <svg className="icon_list_with_clip">
                                                                                <use xlinkHref="/images/icons.svg#icon_list_with_clip"/>
                                                                            </svg>
                                                                        </div>
                                                                        <span className="color-quaternary">Buy Sticky Upgrade</span>
                                                                    </div>
                                                                    <div className="arrow-right">
                                                                        <svg className="arrow-right-icon">
                                                                            <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                        </svg>
                                                                    </div>
                                                                </Link>
                                                            </li>
                                                        ) : (
                                                            <li>
                                                                <div className="link p-y-15">
                                                                    <div className="icon-with-title m-r-10">
                                                                        <div className="icon m-r-10 yellow">
                                                                            <svg className="icon_list_with_clip">
                                                                                <use xlinkHref="/images/icons.svg#icon_list_with_clip"/>
                                                                            </svg>
                                                                        </div>
                                                                        <span className="color-quaternary">Buy Sticky Upgrade</span>
                                                                    </div>
                                                                    <div className="arrow-right">
                                                                        <svg className="icon_check">
                                                                            <use xlinkHref="/images/icons.svg#icon_check"/>
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        )}

                                                    </>
                                                    }

                                                    <li>
                                                        <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/edit`}>
                                                            <div className="icon-with-title m-r-10">
                                                                <div className="icon m-r-10 light-blue">
                                                                    <svg className="icon_edit_username_bordered">
                                                                        <use xlinkHref="/images/icons.svg#icon_edit_username_bordered"/>
                                                                    </svg>
                                                                </div>
                                                                <span className="color-quaternary">Edit Ad</span>
                                                            </div>
                                                            <div className="arrow-right">
                                                                <svg className="arrow-right-icon">
                                                                    <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                </svg>
                                                            </div>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/edit_thumbnail`}>
                                                            <div className="icon-with-title m-r-10">
                                                                <div className="icon m-r-10 brown">
                                                                    <svg className="icon_gear">
                                                                        <use xlinkHref="/images/icons.svg#icon_gear"/>
                                                                    </svg>
                                                                </div>
                                                                <span className="color-quaternary">Edit Thumbnail</span>
                                                            </div>
                                                            <div className="arrow-right">
                                                                <svg className="arrow-right-icon">
                                                                    <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                </svg>
                                                            </div>
                                                        </Link>
                                                    </li>

                                                    {escort.status > 0 &&
                                                    <li>
                                                        <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/duplicate`}>
                                                            <div className="icon-with-title m-r-10">
                                                                <div className="icon m-r-10 light-blue">
                                                                    <svg className="icon_duplicate">
                                                                        <use xlinkHref="/images/icons.svg#icon_duplicate"/>
                                                                    </svg>
                                                                </div>
                                                                <span className="color-quaternary">Duplicate Ad</span>
                                                            </div>
                                                            <div className="arrow-right">
                                                                <svg className="arrow-right-icon">
                                                                    <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                </svg>
                                                            </div>
                                                        </Link>
                                                    </li>
                                                    }

                                                    <li>
                                                        <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/change_location`}>
                                                            <div className="icon-with-title m-r-10">
                                                                <div className="icon m-r-10 purple">
                                                                    <svg className="icon_location">
                                                                        <use xlinkHref="/images/icons.svg#icon_location"/>
                                                                    </svg>
                                                                </div>
                                                                <span className="color-quaternary">Change Location</span>
                                                            </div>
                                                            <div className="arrow-right">
                                                                <svg className="arrow-right-icon">
                                                                    <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                </svg>
                                                            </div>
                                                        </Link>
                                                    </li>

                                                    {/*RENEW*/}
                                                    { escort.status === 0 &&
                                                    <li>
                                                        <Link className="link p-y-15" to={`/classifieds/myposts/${escort.id}/renew`}>
                                                            <div className="icon-with-title m-r-10">
                                                                <div className="icon m-r-10 green">
                                                                    <svg className="icon_payments">
                                                                        <use xlinkHref="/images/icons.svg#icon_payments"/>
                                                                    </svg>
                                                                </div>
                                                                <span className="color-quaternary">Renew Ad</span>
                                                            </div>
                                                            <div className="arrow-right">
                                                                <svg className="arrow-right-icon">
                                                                    <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                </svg>
                                                            </div>
                                                        </Link>
                                                    </li>
                                                    }

                                                    <li>
                                                        <a className="link p-y-15" href={'/#'} id={escort.id} onClick={this.deleteEscort}>
                                                            <div className="icon-with-title m-r-10">
                                                                <div className="icon m-r-10 lighter-light-blue">
                                                                    <svg className="icon_trash">
                                                                        <use xlinkHref="/images/icons.svg#icon_trash"/>
                                                                    </svg>
                                                                </div>
                                                                <span className="color-quaternary">Delete Ad</span>
                                                            </div>
                                                            <div className="arrow-right">
                                                                <svg className="arrow-right-icon">
                                                                    <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                                </svg>
                                                            </div>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                )}

                {/*EMPTY*/}
                {this.state.escorts.length === 0 &&
                    <div className="wrapper-my-ads-container bg-white m-b-10">
                    <div className="container">

                        <div className="d-flex w-100 align-items-center bg-white flex-column p-t-20 p-b-40">
                            <svg className="icon_no_ads">
                                <use xlinkHref="/images/icons.svg#icon_no_ads"/>
                            </svg>
                            <div className="color-quaternary text-center p-y-20 f-s-base">
                                <p className="f-s-20 weight-600 p-b-10">You don’t have any ads</p>
                                <p className="f-s-16">
                                    Post an Ad and start letting know users how  beautiful you are!
                                </p>
                            </div>

                            <ButtonSubmit link={auth.getLink('adbuild')} text={'Post Ad'}/>
                        </div>

                    </div>
                </div>
                }
            </div>
        )
    }

    renderPlaces() {
        return (
            <div className="tab-pane fade" id="places" role="tabpanel" aria-labelledby="places-tab">

                {/*LIST*/}
                {this.state.places.map(item =>
                    {
                        let place = item.place;
                        let city = item.city;
                        let typeName = this.state.placesTypes[place.typeId].name;
                        let typeUrl = this.state.placesTypes[place.typeId].url;
                        return (
                            <div key={place.id} id={place.id} className={`wrapper-my-ads-container bg-white p-t-10 m-b-10 all-escorts`}>
                                <div className="container">
                                    <div className="my-ads-container">
                                        <div className="ad-id p-b-10">
                                            <div className="weight-600 f-s-16">
                                                {place.name}
                                            </div>
                                        </div>

                                        {/*PLACE INFO*/}
                                        <div className="info-container d-flex align-items-start m-b-10">
                                            <div className="my-ads-photo m-r-5">
                                                <img src={`${config.getImagePath('place')}/${place.thumbnail}`} alt="My Place" className="photo"/>
                                            </div>
                                            <div className="info">
                                                <p className="lh-normal m-b-5">
                                                    <span className="color-label f-s-16 m-r-5">City:</span> {city.name}, {city.stateName}
                                                </p>
                                                <p className="lh-normal m-b-5">
                                                    <span className="color-label f-s-16 m-r-5">Category:</span> {typeName}
                                                </p>
                                                <p className="lh-normal m-b-5">
                                                    <span className="color-label f-s-16 m-r-5">Create Date:</span> <Moment format="YYYY/MM/DD">{place.createdAt}</Moment>
                                                </p>
                                                <div className="lh-normal m-b-5 d-flex align-items-center flex-wrap">
                                                    <span className="color-label f-s-16 m-r-5">Rating:</span>
                                                    <div className="rating">
                                                        <div className="primary-rating" data-readonly={true} data-rating={ place.overall }/>
                                                    </div>
                                                    <span className={'m-l-5 f-s-14'}>({place.review} reviews)</span>
                                                </div>
                                            </div>
                                        </div>

                                        {/*PLACE CONTROLS*/}
                                        <div className="secondary-info-link-group d-flex align-items-center justify-content-between flex-nowrap color-secondary">
                                            <Link to={`${city.url}${typeUrl}/${place.id}`} className="link-container justify-content-start">
                                                <div className="icon">
                                                    <svg className="icon_view_ad">
                                                        <use xlinkHref="/images/icons.svg#icon_view_ad"/>
                                                    </svg>
                                                </div>
                                                <span className="title">View Place</span>
                                            </Link>
                                            <Link to={`${city.url}add?id=${place.id}`} className="link-container justify-content-end">
                                                <div className="icon">
                                                    <svg className="icon_gear">
                                                        <use xlinkHref="/images/icons.svg#icon_gear"/>
                                                    </svg>
                                                </div>
                                                <span className="title">Edit Place</span>
                                            </Link>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        )
                    }
                )}

                {/*EMPTY*/}
                {this.state.places.length === 0 &&
                    <div className="wrapper-my-ads-container bg-white m-b-10">
                    <div className="container">

                        <div className="d-flex w-100 align-items-center bg-white flex-column p-t-20 p-b-40">
                            <svg className="icon_no_ads">
                                <use xlinkHref="/images/icons.svg#icon_no_ads"/>
                            </svg>
                            <div className="color-quaternary text-center p-y-20 f-s-base">
                                <p className="f-s-20 weight-600 p-b-10">You don’t have any places</p>
                                <p className="f-s-16">
                                    <Link className={'weight-700'} to={auth.getLink('home')}>Please go to a city page and add new place at the bottom</Link>.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                }

            </div>
        )
    }

    render() {
        return (
            <>
                <div className="content my-ads" id="my-ads-page">

                    {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}
                    {this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

                    { offline.getState() ? (
                        <Offline/>
                    ) : (
                        <>
                            {/*ADS & PLACES TABS*/}
                            <div className="primary-tabs-nav-container position-relative bg-white m-b-10 bs-primary">
                                <ul className="nav nav-tabs scrolling-tabs d-flex" id="primary-tab" role="tablist">
                                    <li className="nav-item d-flex align-items-center width-50">
                                        <a className="nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1 active"
                                           id="profiles-tab" data-toggle="tab" href="#profiles" role="tab" aria-controls="profiles" aria-selected="true">
                                            Profiles
                                        </a>
                                    </li>
                                    <li className="nav-item d-flex align-items-center width-50">
                                        <a className="nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1"
                                           id="places-tab" data-toggle="tab" href="#places" role="tab" aria-controls="places" aria-selected="false">
                                            Places
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            {/*TOPUPS & BUDGET*/}
                            <div className="wrapper-my-ads-container bg-primary p-y-20 m-b-10">
                                <div className="container">
                                    <div className="my-ads-container text-center">

                                        <h2 className="color-white weight-700 p-b-15">
                                            Account ID# {this.state.account.id}
                                        </h2>

                                        <p className="color-white p-b-5">
                                            Budget balance: <span className={'weight-600'}>${this.state.budget || 0}</span>
                                        </p>

                                        <Link to={auth.getLink('recharge')} className="success-filled-btn m-b-10">
                                            <span>Recharge Budget</span>
                                        </Link>


                                        <p className="color-white p-t-10 p-b-5">
                                            <span className="color-success weight-700">{this.state.account.topUps || 0}</span> Top-Ups Remaining
                                        </p>

                                        <Link to={auth.getLink('topups')} className="success-filled-btn">
                                            <span>Buy Ads Top Ups</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>

                            {/*COUNTERS*/}
                            <div className="wrapper-my-ads-container bg-white m-b-10">
                                <div className="container p-x-5">
                                    <div className="my-ads-container" id="account-escort-status">
                                        <div className="primary-info-link-group d-flex justify-content-between flex-nowrap">
                                            <div className="link-container d-flex flex-column align-items-center p-5">
                                                <span className="count color-primary weight-600">{this.state.escorts.length}</span>
                                                <span className="name f-s-16 color-tertiary">Total Ads</span>
                                            </div>
                                            <div className="link-container d-flex flex-column align-items-center p-5">
                                                <span className="count color-primary weight-600">{ this.state.counters.active }</span>
                                                <span className="name f-s-16 color-tertiary">Live Ads</span>
                                            </div>
                                            <div className="link-container d-flex flex-column align-items-center p-5">
                                                <span className="count color-primary weight-600">{ this.state.counters.expired }</span>
                                                <span className="name f-s-16 color-tertiary">Expired Ads</span>
                                            </div>
                                            <div className="link-container d-flex flex-column align-items-center p-5">
                                                <span className="count color-primary weight-600">{ this.state.places.length }</span>
                                                <span className="name f-s-16 color-tertiary">Places</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="tab-content primary-tab-content">

                                {this.renderEscorts()}

                                {this.renderPlaces()}

                            </div>
                        </>
                    ) }

                </div>

                <LoadingSpinner loading={this.state.loading} />
            </>
        )
    }
}
