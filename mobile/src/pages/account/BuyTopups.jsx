import React, {Component} from 'react';
import axios from '../../services/api_init';
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import {history} from "../../index";

export default class BuyTopups extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            currentPrice: 0,
            selectedTopUps: 1,
            topUps: []
        };
    }

    setPrice = event => {
        event.preventDefault();
        this.setState({
            selectedTopUps: event.target.value
        });
    }

    payToUps = event => {
        event.preventDefault();

        let params = {
            'paymentType': 'topUps',
            'topUps': this.state.selectedTopUps,
            'src': 'topups',
        };

        history.push(config.getPaymentUrl(params));
    }

    getPriceList = async () => {
        try {
            const res = await axios.get('/api/v1/pc/escorts/top_ups_price');
            this.setState({
                currentPrice: res.data.price,
                topUps: res.data.topUps,
            });
        } catch (error) {
            config.catch(error);
        }
    }

    async componentDidMount() {
        await this.getPriceList();
    }

    render() {
        return (
          <div className="content buy-popups bg-seventh-gray" id="buy-topups-page">
            <div className="buy-top-ups-modal-head">
                <div className="bg-primary color-white p-y-20">
                    <div className="container">
                        <div className="banner m-b-30 text-center p-t-15">
                            <svg className="icon_modal_buy_top_ups">
                                <use xlinkHref="/images/icons.svg#icon_modal_buy_top_ups"/>
                            </svg>
                        </div>

                        <h2 className="weight-700 lh-25 m-b-25 p-x-25 text-center">Put your ad to the top of the list... anytime you want</h2>

                        <div className="paragraphs m-b-20 text-center">
                            <p>
                                Buy top-up credits for just { this.state.currentPrice } per one top up and then you can use this top-up
                                credit to
                                put any of your ads to the top of the list. Anytime you want, even multiple times a day!
                            </p>
                            <p>
                                Don't miss your opportunity to be the first one the clients will see in the list.
                            </p>
                        </div>
                    </div>
                </div>

                <div className="p-y-20 flex-grow-1">
                    <div className="container">

                        <form action="#" method="POST" className="w-100 h-100" onSubmit={this.payToUps}>
                            <div className="primary-select m-b-30">
                                <label className="required">Top Up Credit</label>
                                <div className="select-container">
                                    <select name="topUps" id="number" onChange={this.setPrice}>
                                        { this.state.topUps.map(topUp =>
                                            <option key={topUp} value={ topUp }>{ `${topUp} top-up credits - ${(topUp * this.state.currentPrice).toFixed(2)}` }</option>
                                        ) }
                                    </select>
                                </div>
                            </div>

                            <button disabled={!window.navigator.onLine} className="primary-filled-btn success-submit-btn m-b-30" type="submit">
                                <span>Buy Top Ups</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        )
    }
}
