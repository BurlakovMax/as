import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from '../../services/api_init';
import auth from "../../lib/auth";
import {history} from "../../index";
import config from "../../lib/config";
import {changeTitle} from "../../store/common";
import BuyCityCheckbox from "../../components/common/buy-city-checkbox";
import LoadingSpinner from "../../components/common/loading-spinner";
import CheckboxSecondary from "../../components/form/checkbox-secondary";
import ButtonSubmit from "../../components/form/button-submit";
import ButtonBordered from "../../components/form/button-bordered";
import escorts from "../../services/escorts";
import Alert from "../../components/common/alert";

export default class BuyUpgrades extends Component {
    constructor(props) {
        super(props);

        this.config = {
          'escortSponsor'  : {
                'title' : 'Be A Cover Star',
                'class' : 'be-a-cover-star',
                'api' : 'escort_sponsor_price',
            },
          'escortSide'  : {
                'title' : 'Appear On Every Page',
                'class' : 'appear-on-every-page',
                'api' : 'escort_side_price',
            },
          'escortSticky'  : {
                'title' : 'Appear always on the top',
                'class' : 'appear-always-on-top',
                'api' : '',
            },
        };

        this.state = {
            loading: false,
            account: {},

            action: this.props.match.params.escortAction,

            escort: {},

            sponsor: {
                allowLocations: {},
                price: 0,
            },
            locationsCount: 0,

            stickyPrice: {},
            stickyAvailable: [],
            stickyNotAvailable: [],
            availableSponsorLocations: [],
            stickySubscribed: {},
            stickySelected: 0,
        };
    }

    getSponsor = async () => {
        try {
            const res = await axios.get(`/api/v1/pc/escorts/${this.state.escort.id}/${this.config[this.state.action].api}`);
            this.setState({
                sponsor: res.data,
                locationsCount: 0,
            });
        } catch (error) {
            config.catch(error);
        }
    }

    getStickyPrice = async () => {
        try {
            const res = await axios.get(`/api/v1/escorts/escort_sticky_price`,{
                params: {
                    accountId: this.state.account.id,
                    locationIds: this.state.escort.locationIds,
                    type: this.state.escort.rawType,
                }
            });

            // parse api response
            let prices = {};
            for (let price of res.data.list) {
                prices[price.locationId] = price;
            }
            this.setState({
                stickyPrice: prices
            });
        } catch (error) {
            config.catch(error);
        }
    }

    checkByEscortSponsors = async () => {
        try {
            const res = await axios.get('/api/v1/escorts/can_buy_escort_sponsor',
                {
                    params: {
                        locationIds: this.state.escort.locationIds,
                        type: this.state.escort.rawType,
                    }
                });
            this.setState({availableSponsorLocations: res.data});
        } catch (error) {
            config.catch(error);
        }
    }

    getStickyAvailable = async () => {
        try {
            const res = await axios.get(`/api/v1/escorts/can_buy_escort_sticky`,
              {
                  params: {
                      locationIds: this.state.escort.locationIds,
                      type: this.state.escort.rawType,
                  }
              }
            );

            // parse api response
            let data = {};
            res.data.map(item =>
              data[item.id] = item.available
            )

            // create two lists: av & not av locations
            let available = [];
            let notAvailable = [];
            this.state.escort.locations.map(item =>
              {
                  let loc = {
                      'cityId' : item.locationId,
                      'cityName' : item.cityName,
                      'stateName' : item.stateName,
                  };
                  if (data[item.locationId]) {
                      available.push(loc)
                  } else {
                      notAvailable.push(loc)
                  }
                  return null;
              }
            )

            this.setState({
                stickyAvailable: available,
                stickyNotAvailable: notAvailable,
            });
        } catch (error) {
            config.catch(error);
        }
    }

    subscribeToSticky = async (event) => {
        event.preventDefault();

        let locationId = event.currentTarget.dataset.id;

        try {
            await axios.post(`/api/v1/pc/escorts/${this.state.escort.id}/add_to_waiting_list`,
                {
                    'locationId': locationId,
                    'type': this.state.escort.rawType,
                }
            );

            let stickySubscribed = this.state.stickySubscribed;
            stickySubscribed[locationId] = locationId;
            this.setState({
              stickySubscribed: stickySubscribed
            });
        } catch (error) {
            config.catch(error);
        }
    }

    isSubscribedSticky = async (locationId) => {
      let stickySubscribed = this.state.stickySubscribed;
      try {
          const res = await axios.get(`/api/v1/pc/escorts/${this.state.escort.id}/has_in_waiting_list?locationId=${locationId}&type=${this.state.escort.rawType}`);

          if (res.data.has) {
              stickySubscribed[locationId] = locationId;
          }
          this.setState({
              stickySubscribed: stickySubscribed
          });
      } catch (error) {
          config.catch(error);
      }
    }

    checkSubscribedSticky = async () => {
        await Promise.all(
          this.state.stickyNotAvailable.map(
            location => this.isSubscribedSticky(location.cityId)
          )
      )
    }

    checkLocations = id => {
        if (this.locationIds.has(id)) {
            this.locationIds.delete(id);
            this.setState({
                locationsCount: this.state.locationsCount - 1
            });
        } else {
            this.locationIds.add(id);
            this.setState({
                locationsCount: this.state.locationsCount + 1
            });
        }
    }

    checkLocationsSticky = (event) => {
        event.preventDefault();

        let form = document.getElementById('escortUpgrades');
        let inputs = form.getElementsByTagName('input');
        let selected = 0;

        this.locationIds.clear();

        for (let item of inputs) {
            if (item.checked) {
                let price = {};
                price[parseInt(item.id)] = item.value;
                this.locationIds.add(price);
                selected = item.id;
            }
        }

        this.setState({
            locationsCount: this.locationIds.size,
            stickySelected: selected
        });
    }

    clearLocationsSticky = (event) => {
        event.preventDefault();

        let id = `sticky_location_${event.currentTarget.dataset.id}`;
        let parent = document.getElementById(id);

        let inputs = parent.getElementsByTagName('input');
        for (let item of inputs) {
            item.checked = false;
        }

        let labels = parent.getElementsByTagName('label');
        for (let item of labels) {
            item.classList.remove('active');
        }

        this.setState({
            locationsCount: 0,
            stickySelected: 0,
        });

        this.locationIds.clear();
    }

    isStickyDisabled = (locationId) => {
        if (this.state.locationsCount === 0 || parseInt(this.state.stickySelected) === parseInt(locationId)) {
            return false;
        }

        return true;
    }

    submit = event => {
        event.preventDefault();

        let params = {};
        if (this.state.action === 'escortSticky') {

            let locationIds = [];
            let locationIdsWeek = [];
            let locationIdsMonth = [];
            this.locationIds.forEach(location => {
                let id = parseInt(Object.keys(location)[0]);
                locationIds.push(id);
                if (location[id] === 'week') {
                    locationIdsWeek.push(id)
                }
                if (location[id] === 'month') {
                    locationIdsMonth.push(id);
                }
            })

            params = {
                'paymentType': this.state.action,
                'escortId': this.state.escort.id,
            };
            if (locationIdsWeek.length) {
                params[config.getEscortUpgradeType('stickyWeek')] = Array.from(locationIdsWeek).join(',')
            }
            if (locationIdsMonth.length) {
                params[config.getEscortUpgradeType('stickyMonth')] = Array.from(locationIdsMonth).join(',')
            }
        } else {
            params = {
                'paymentType': this.state.action,
                'escortId': this.state.escort.id,
                'locationIds': Array.from(this.locationIds).join(','),
            };
        }
        let url = config.getPaymentUrl(params);

        history.push(url);
    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount()
          .then(account => {
              if (account) {
                  this.setState({
                      account: account
                  });
              }
          })

        this.setState({
            escort: await escorts.getById(this.props.match.params.escortId)
        });

        if (this.state.action === 'escortSticky') {
            await this.getStickyPrice();
            await this.getStickyAvailable();
            await this.checkSubscribedSticky();
        } else {
            await this.getSponsor();
            await this.checkByEscortSponsors();
        }

        this.locationIds = new Set();

        changeTitle(this.config[this.state.action].title);

        this.setState({loading: false});
    }

    render() {
        const locations = Object.values(this.state.sponsor.allowLocations);

        if (typeof this.config[this.state.action] !== 'object') {
            history.push(auth.getLink('myposts'));
            return;
        }

        return (
          <div className={`content bg-white m-b-20 ${this.config[this.state.action].class}`} id={this.state.action}>
            <div className="container">
                <div className="primary-modal-head">

                    <form id={'escortUpgrades'} action="#" method="POST" className="w-100 text-center" onSubmit={this.submit}>

                        <div className="banner"/>

                        {/*TEXT INFO*/}
                        {this.state.action === 'escortSponsor' &&
                        <>
                            <h1 className="weight-700 m-b-5 p-x-30">Be A Cover Star for ${this.state.sponsor.price} a month</h1>
                            <div className="paragraphs m-b-10">
                                <p>
                                    Be the viewer's first fruit! Post your photo on your city's cover for ${this.state.sponsor.price} a month.
                                </p>
                                <p>
                                    Post your photo and link on your city's cover page
                                </p>
                            </div>
                        </>
                        }

                        {this.state.action === 'escortSide' &&
                        <>
                            <h1 className="weight-700 m-b-10 p-x-30">Appear on every page for ${this.state.sponsor.price} a month</h1>
                            <div className="paragraphs m-b-20">
                                <p>
                                    Show up in sponsored ads section on the right of every page! Buy a sponsor ad upgrade for just ${this.state.sponsor.price} a month.
                                </p>
                            </div>

                            <Alert
                                type={'success'}
                                classes={'f-s-16 m-b-10'}
                                transparent={true}
                                closable={false}
                                message={'Disclaimer: This upgrade is only visible on desktop and tablet version of our website (which is about 30% of our visitors - the rest 70% are users on cell phones)'}
                            />
                        </>
                        }

                        {/*CHECKBOXES*/}
                        {this.state.action !== 'escortSticky' &&
                        <div className="checkboxes ad-creation-upgrade">
                            {locations.map(location =>
                                this.state.availableSponsorLocations.findIndex(current => current.id === location.cityId) >= 0 ? (
                                        <BuyCityCheckbox
                                            id={location.cityId}
                                            key={location.cityId}
                                            label={`${location.cityName}, ${location.stateName}`}
                                            price={this.state.sponsor.price}
                                            handleCheckboxChange={this.checkLocations}/>
                                    ) : ''
                            )}
                        </div>
                        }


                        {/*CHECKBOXES + RADIO*/}
                        {this.state.action === 'escortSticky' &&
                        <>
                            <h1 className="weight-700 m-b-5 p-x-15">Appear always on the top of the list ... from ${this.state.sponsor.price}a month</h1>
                            <div className="paragraphs m-b-10">
                                <p>
                                    Show up at the very top of the list, and be ahead of your competition.
                                </p>
                                <p>
                                    Ads on top of the list have 500% more views than average ad. Your ad will be always at the beginning of the first page and will not be moved to second or subsequent pages.
                                </p>
                            </div>

                            {/*NOT AVAILABLE LOCATIONS*/}
                            {(parseInt(this.state.escort.rawType) === 2 || this.state.account.agency) &&
                            <div className={'ad-creation-upgrade waiting'}>
                                <div className="ad-creation-upgrade_options d-block">
                                    {this.state.stickyNotAvailable.map(location =>
                                        <label key={location.cityId} className={`flow selection ad-creation-upgrade_options-item icon-clear p-x-10`}>
                                            <div>
                                                {location.cityName}, {location.stateName} - <span className="weight-600 color-error">not available</span>
                                                <div className="f-s-14 weight-400 color-label">Sticky upgrade sold out, but you can join the waiting list in this location</div>
                                            </div>
                                            {this.state.stickySubscribed[location.cityId] ?
                                              (<ButtonBordered handleClick={(e) => {e.preventDefault()}} text={'Already Subscribed'}/>)
                                              :
                                              (<ButtonSubmit handleClick={this.subscribeToSticky} data={{'data-id': location.cityId}} text={'Subscribe to waiting list'}/>)
                                            }
                                        </label>
                                    )}
                                </div>
                            </div>
                            }

                          {/*AVAILABLE LOCATIONS*/}
                          <div className={'ad-creation-upgrade'}>
                            <div className="ad-creation-upgrade_options d-block">
                                {this.state.stickyAvailable.map(location =>
                                  <label key={location.cityId}
                                         onClick={(e) => {e.preventDefault()}}
                                         id={`sticky_location_${location.cityId}`}
                                         className={`selection ad-creation-upgrade_options-item icon-clear p-x-10 ${this.isStickyDisabled(location.cityId)? 'disabled' : ''}`}>
                                      <span>{location.cityName}, {location.stateName}</span>
                                      <CheckboxSecondary
                                        classes={'m-l-10'}
                                        type={'radio'}
                                        data={[
                                            {
                                                'id': location.cityId,
                                                'name': `sticky_${location.cityId}`,
                                                'title': `$${this.state.stickyPrice[location.cityId].weeklyPrice}/week`,
                                                'value': 'week',
                                                'handleClick': this.checkLocationsSticky,
                                            },
                                            {
                                                'id': location.cityId,
                                                'name': `sticky_${location.cityId}`,
                                                'title': `$${this.state.stickyPrice[location.cityId].monthlyPrice}/mo`,
                                                'value': 'month',
                                                'handleClick': this.checkLocationsSticky,
                                            },
                                        ]}
                                      />
                                      <div data-id={location.cityId} onClick={this.clearLocationsSticky} className={'icon-clear-btn'}>
                                          <svg className="icon_close">
                                              <use xlinkHref="/images/icons.svg#icon_close"/>
                                          </svg>
                                      </div>
                                  </label>
                                )}
                            </div>
                          </div>
                        </>
                        }

                        <button disabled={this.state.locationsCount === 0 || !window.navigator.onLine} className="primary-submit-btn location-sponsor-escort m-b-20" type="submit">
                            <span>Continue</span>
                        </button>

                        <Link className="primary-bordered-btn" to={auth.getLink('myposts')}>
                            No thanks
                        </Link>
                    </form>

                </div>
            </div>

          <LoadingSpinner loading={this.state.loading} />
          </div>
        );
    }
}
