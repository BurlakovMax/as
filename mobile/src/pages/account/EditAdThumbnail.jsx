import React, {Component} from 'react';
import axios from '../../services/api_init';
import LoadingSpinner from "../../components/common/loading-spinner";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css'
import ButtonSubmit from "../../components/form/button-submit";
import uploader from "../../lib/uploader";
import ButtonBordered from "../../components/form/button-bordered";
import auth from "../../lib/auth";
import escorts from "../../services/escorts";

export default class EditAdThumbnail extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            errorMessage: false,

            escort: {},

            pictures: [], // uploaded images
            picturesMax: 8,
            picturesLimit: false,
            picturesError: false,

            // selected image src for crop
            selectedThumb: '',

            src: null,

            //@url https://www.npmjs.com/package/react-image-crop#usage
            crop: {
                width: 125,
                height: 125,
                aspect: 1/1,
            },

            // crop config
            cropMinWidth: 125,
            cropMinHeight: 125,
            cropErr: false,

            // cropped image data
            croppedImage: false, // blob url
            croppedImageBlob: false, // blob

            // cropped uploading results
            croppedResultOk: false,
            croppedResultErr: false,

            // uploaded cropped image
            croppedUploaded: false,
        };
    }

    deletePhoto = async event => {
        event.preventDefault();

        let result = await uploader.deletePhoto(
          event,
          this.state.pictures,
          this.state.picturesMax
        );

        this.setState({
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })

        // clear cropped data
        this.setState({
            selectedThumb: '',
            croppedImage: '',
            croppedImageBlob: '',
        });
    }

    uploadPhoto = async event => {
        event.preventDefault();

        let result = await uploader.uploadPhoto(
          event,
          this.state.pictures,
          this.state.picturesMax
        );

        this.setState({
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })
    }


    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.cropImage(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({ crop });
    };

    async cropImage(crop) {
        this.setState({
            cropErr: false
        });
        if (this.imageRef && crop.width && crop.height) {
            const [croppedImage, croppedImageBlob] = await this.cropImageCreate(
              this.imageRef,
              crop,
              'newFile.jpg'
            );
            this.setState({
                croppedImage: croppedImage,
                croppedImageBlob: croppedImageBlob,
            });
        } else {
            this.setState({
                cropErr: 'Please select thumbnail area'
            });
        }
    }

    cropImageCreate(image, crop, fileName) {
        // check for min sizes
        if (crop.width < this.state.cropMinWidth || crop.height < this.state.cropMinHeight) {
            this.setState({
                cropErr: 'Please select larger thumbnail area'
            });
        }

        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
          image,
          crop.x * scaleX,
          crop.y * scaleY,
          crop.width * scaleX,
          crop.height * scaleY,
          0,
          0,
          crop.width,
          crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve([this.fileUrl, blob]);
            }, 'image/jpeg');
        });
    }

    cropImageUpload = async () => {
        this.setState({
            croppedResultOk: false,
            croppedResultErr: false,
        });

        try {
            const formData = new FormData();
            formData.append('image', this.state.croppedImageBlob, 'thumb.jpg');

            let config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            };
            let res = await axios.post('/api/v1/escorts/image_upload', formData, config);

            if (res.data.id) {
                this.setState({
                    croppedUploaded: res.data,
                })
            } else {
                this.setState({
                    croppedResultErr: 'Crop uploading error',
                    loading: false,
                });
            }
        } catch (error) {
            config.catch(error);

            this.setState({
                croppedResultErr: 'Crop uploading error',
                loading: false,
            });
        }
    }

    cropImageSave = async () => {
        this.setState({
            croppedResultOk: false,
            croppedResultErr: false,
        });

        try {
            await axios.put(`/api/v1/pc/escorts/${this.state.escort.id}/thumbnail`, {
                thumbnail: this.state.croppedUploaded.name,
            });

            this.setState({
                croppedResultOk: true,
                loading: false,
            });
        } catch (error) {
            config.catch(error);

            this.setState({
                croppedResultErr: 'Thumb saving error',
                loading: false,
            });
        }
    }

    submit = async event => {
        event.preventDefault();

        this.setState({
            errorMessage: false,
        });

        // crop area not selected or too small
        if (this.state.cropErr) {
            return;
        }

        this.setState({loading: true});

        // 1. uploading cropped blob as usual img
        await this.cropImageUpload();

        if (this.state.croppedResultErr) {
            return;
        }

        // 2. save this image as thumbnail
        await this.cropImageSave();

        // 3. update escort in local db
        await escorts.getById(this.state.escort.id, true);

        this.setState({loading: false});
    }


    selectThumb = event => {
        // toggle active class
        let selections = document.getElementsByClassName('thumb');
        Array.from(selections).forEach(
          function(element, index, array) {
              element.classList.remove('selected')
          }
        );
        event.currentTarget.classList.add('selected');

        // apply selected src to preview img
        this.setState({
            selectedThumb: event.currentTarget.dataset.src
        });
    }

    async componentDidMount() {
        this.setState({ loading: true });

        this.setState({
            escort: await escorts.getById(this.props.match.params.escortId)
        });

        this.setState({ loading: false });
    }

    render() {
        if (!Object.keys(this.state.escort).length) {
            return <div />;
        }

        return (
          <div className="content ad-creation">

              <div className="wrapper-ad-creation-container bg-white p-y-20">
                  <div className="container">
                      <div className="ad-creation-container">
                          <h2 className="weight-700 color-primary">
                              Change thumbnail for classified ad #{this.state.escort.id}, "{this.state.escort.firstName}"
                          </h2>

                          {this.state.escort.thumbnail &&
                              <div className="thumbnail-current p-y-20 border-gray-b">
                                  <h3 className="weight-700 color-primary p-b-10">
                                      Current thumbnail:
                                  </h3>
                                  <div className="thumbnail-current-image image-wrapper flex-shrink-0">
                                      <img id="current" className="image" alt={this.state.escort.firstName} src={`${config.getImagePath('escort')}/${this.state.escort.thumbnail}`}/>
                                  </div>
                              </div>
                          }

                          <div className="thumbnail-select p-y-20">
                              <h3 className="weight-600 color-primary p-b-10">
                                  1. Click on image, from which you want to create thumbnail or upload a new one:
                              </h3>
                              <div className={'thumb_image_select file_uploader'}>

                                  {/*UPLOAD NEW PHOTOS BTN*/}
                                  {(this.state.pictures.length !== this.state.picturesLimit) &&
                                      <div className={'thumb'}>
                                          <label type="button" className="file_uploader__button">
                                              Upload new image
                                              <input
                                                onChange={this.uploadPhoto}
                                                data-type={'escort'}
                                                type="file"
                                                id="photo"
                                                name="photo"
                                                accept="image/*"
                                                className={'d-none'}
                                              />
                                          </label>
                                      </div>
                                  }

                                  {/*EXISTING IMAGES*/}
                                  {this.state.escort.images.map((image, index) => {
                                    let src = `${config.getImagePath('escort')}/${image}`;
                                    return (
                                      <div key={index} data-src={src} className={'thumb'} onClick={this.selectThumb}>
                                          <div className="thumb_image image-wrapper flex-shrink-0">
                                              <img alt={this.state.escort.firstName} src={src}/>
                                          </div>
                                      </div>
                                    )
                                  })}

                                  {/*NEW UPLOADED IMAGES*/}
                                  {this.state.pictures.map((item, index) => {
                                        let src = item.url;
                                        return(
                                          <div key={index} data-src={src} className="thumb file_uploader__picture" onClick={this.selectThumb}>
                                              <div
                                                onClick={this.deletePhoto}
                                                data-type={'escort'}
                                                data-index={index}
                                                data-id={item.id}
                                                data-name={item.name}
                                                className="file_uploader__delete"
                                              />
                                              <div style={{backgroundImage: `url(${src})`}} className="file_uploader__img"/>
                                          </div>
                                        )
                                      }
                                  )}

                              </div>
                          </div>

                            <div id="detail_2" className={`thumbnail-detail-2 p-y-10 overflow-hidden ${this.state.selectedThumb? 'd-block' : 'd-none'}`}>
                                <form action="#" method="POST" id="thumbForm" onSubmit={this.submit}>
                                    <h3 className="color-primary weight-600 p-b-10">
                                        2. Move and/or expand the highlighted box to create your preferred thumbnail:
                                    </h3>
                                    <div className="thumbnail-crop">
                                        <div className={'photo_preview'}>
                                          <ReactCrop
                                            src={this.state.selectedThumb}
                                            crop={this.state.crop}
                                            ruleOfThirds
                                            onImageLoaded={this.onImageLoaded}
                                            onComplete={this.onCropComplete}
                                            onChange={this.onCropChange}
                                          />
                                            {this.state.cropErr &&
                                            <div className={'selection_result color-error weight-600'}>
                                                {this.state.cropErr}
                                            </div>
                                            }
                                        </div>
                                        <div className={'selection_preview m-y-10'}>
                                            Selection preview:
                                            <div className={'thumbnail-crop-preview'}>
                                                {this.state.croppedImage && (
                                                  <img alt="Crop" style={{ maxWidth: '100%' }} src={this.state.croppedImage} />
                                                )}
                                            </div>
                                        </div>
                                        {this.state.croppedResultOk &&
                                            <div className={'selection_result color-success'}>
                                                Thumbnail has been successfully updated. If you don't like it, you can submit the form again.
                                                <div className="weight-600">Please allow some time to reflect the change on our pages.</div>
                                            </div>
                                        }
                                        {this.state.croppedResultErr &&
                                            <div className={'selection_result color-error weight-600'}>
                                                There was an error updating your thumbnail. Please try again in a few minutes.
                                            </div>
                                        }
                                        <ButtonSubmit classes={'m-t-15'} text={'Update'}/>

                                        {this.state.croppedResultOk &&
                                            <ButtonBordered classes={'m-t-15'} link={auth.getLink('myposts')} text={'Go back to My Ads'}/>
                                        }
                                    </div>
                                </form>
                            </div>

                      </div>
                  </div>
              </div>

            <LoadingSpinner loading={this.state.loading} />
          </div>
        );
    }
}
