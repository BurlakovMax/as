import React, {Component} from 'react';
import axios from '../../services/api_init';
import auth from "../../lib/auth";
import Alert from "../../components/common/alert";
import LoadingSpinner from "../../components/common/loading-spinner";
import ButtonSubmit from "../../components/form/button-submit";
import Input from "../../components/form/input";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";

export default class ChangeUsername extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            errorMessage: false,
            loading: false,
            errors: {},

            currentName: false,
            account: {
                name: null
            }
        };
    }

    fillForm = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            account: Object.assign(this.state.account, res)
        });
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;
        let errors = {};

        if (this.state.account.name === this.state.currentName) {
            errors['username'] = 'Please use another username to change'
            ok = false;
        }

        if (!document.getElementById('username').value) {
            errors['username'] = 'Please type new username'
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    submit = async event => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        try {
            await axios.put('/api/v1/pc/account/change_username', {username: this.state.account.name});

            auth.gotoAccount();
        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: error.response.data.message,
            });
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    async componentDidMount() {
        this.setState({ loading: true });
        await auth.getAccount()
        .then(account => {
            if (account) {
                this.setState({
                    account: account,
                    currentName: account.name,
                });
            }
        })
        this.setState({ loading: false });
    }

    render() {
        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

          <div className="edit-username p-y-15 bg-white" id="change-username-page">
            <div className="container">
                <p>
                    Your current username is <span className="color-tertiary weight-600">{ this.state.currentName }</span>
                </p>

                <form action="#" method="POST" onSubmit={this.submit}>

                    <Input
                      classes={'m-y-15'}
                      label={'Username'}
                      handleChange={this.fillForm}
                      required={true}
                      name={'name'}
                      id={'username'}
                      placeholder={'Enter username here'}
                      minLength={3}
                      maxLength={32}
                      error={this.state.errors.username}
                    />

                    <ButtonSubmit text={'Save'} loading={this.state.loading}/>
                </form>

                <LoadingSpinner loading={this.state.loading} />
            </div>
          </div>
          </>
        )
    }
}

