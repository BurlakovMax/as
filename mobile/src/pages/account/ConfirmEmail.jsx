import React, {Component} from 'react';
import axios from '../../services/api_init';
import LoadingSpinner from "../../components/common/loading-spinner";
import Alert from "../../components/common/alert";
import auth from "../../lib/auth";
import ButtonSubmit from "../../components/form/button-submit";
import Input from "../../components/form/input";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import form from "../../lib/form";
import Or from "../../components/text/or";
import ButtonBordered from "../../components/form/button-bordered";

export default class ConfirmEmail extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            successMessage: false,
            errorMessage: false,
            loading: false,

            account: {},

            confirmationId: null,
            confirmationCode: null,
        };
    }

    fillConfirm = event => {
        const target = event.target;

        // convert to capitals
        target.value = target.value.toUpperCase();

        this.setState({
            confirmationCode: target.value
        });
    }

    // send confirm code again - auto mode
    requestCode = async event => {
        event.preventDefault();

        this.setState({
            errorMessage: false,
            successMessage: false,
            loading: true,
        });

        try {
            let res = await axios.post('/api/v1/pc/account/request_email_confirmation',
              {
                  'email': this.state.account.email,
              }
            );

            if (res.data.confirmationId) {
                this.setState({
                    loading: false,
                    errorMessage: false,
                    successMessage: 'Confirmation code sent.',
                    confirmationId: res.data.confirmationId,
                });
            }
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }


    }

    submitCode = async event => {
        event.preventDefault();

        this.setState({
            errorMessage: false,
            successMessage: false,
            loading: true,
        });

        try {
            await axios.post('/api/v1/pc/account/set_email_confirmed', {
                confirmationId: this.state.confirmationId,
                confirmationCode: this.state.confirmationCode
            });

            this.setState({
                loading: false,
            });

            auth.gotoAccount();
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await auth.getAccount()
          .then(account => {
              this.setState({
                  account: account
              });
          })

        if (this.state.account.emailConfirmed) {
            auth.gotoAccount();
        }

        this.setState({ loading: false });
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    render() {
        return (
        <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}
          {this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

            <div className="change-email p-y-15 bg-white" id="change-email-page">

                { this.state.confirmationId ? (
                    <>
                    <div className="container">
                        <div className="bg-white">
                            <h2 className="weight-700 p-y-10 color-primary">Please check your email inbox and enter confirmation code</h2>
                        </div>

                        <form action="#" id="confirmForm" method="POST" onSubmit={this.submitCode}>
                            <Input
                              classes={'m-y-15 p-b-5'}
                              label={'Confirmation code'}
                              handleChange={this.fillConfirm}
                              required={true}
                              name={'confirmation_code'}
                              id={'confirmation_code'}
                              placeholder={'Enter confirmation code from email'}
                              maxLength={6}
                              minLength={6}
                              //error={this.state.errors.confirmation_code}
                            />

                            <ButtonSubmit text={'Confirm'} loading={this.state.loading}/>
                        </form>

                        <Or classes={'m-t-20 m-b-5'}/>

                        <div className="p-b-15 bg-white">
                            <div className="container">
                                <div className="bg-white">
                                    <p className="p-y-10 color-primary">If you didn't receive confirmation code - you can request it again.</p>
                                </div>
                                <ButtonBordered text={'Resend code'} classes={'m-t-15'} handleClick={this.requestCode}/>
                            </div>
                        </div>

                    </div>
                    </>
                ) : (
                    <div className="p-b-15 bg-white">
                        <div className="container">
                            <div className="bg-white">
                                <p className="p-y-10">Click to send confirmation code to your email: <span className={'weight-600 color-primary'}>{this.state.account.email}</span></p>
                            </div>
                            <ButtonSubmit text={'Request code'} classes={'m-t-15'} handleClick={this.requestCode}/>
                        </div>
                    </div>
                ) }
            </div>

            <LoadingSpinner loading={this.state.loading} />
        </>
        )
    }
}
