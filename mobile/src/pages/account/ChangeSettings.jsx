import React, {Component} from 'react';
import axios from '../../services/api_init';
import Alert from "../../components/common/alert";
import ButtonSubmit from "../../components/form/button-submit";
import LoadingSpinner from "../../components/common/loading-spinner";
import auth from "../../lib/auth";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";

export default class ChangeSettings extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            errorMessage: false,
            loading: false,

            settings: {
                newcomment: false,
                newcommentme: false
            }
        };
    }

    getSettings = async () => {
        try {
            const res = await axios.get('/api/v1/pc/account/settings');
            this.setState({
                settings: res.data
            });
        } catch (error) {
            config.catch(error);
        }
    }

    fillSettings = event => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        let res = {[name]: value};
        this.setState({
            settings: Object.assign(this.state.settings, res)
        });
    }

    saveSettings = async event => {
        event.preventDefault();

        this.setState({
            loading: false
        });

        try {
            await axios.put('/api/v1/pc/account/change_settings', this.state.settings);

            this.setState({
                loading: false
            });

            auth.gotoAccount();
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    async componentDidMount() {
        this.setState({ loading: true });
        await this.getSettings();
        this.setState({ loading: false });
    }

    render() {
        return (
        <>
        {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

        <div className="content settings" id="change-settings-page">
            <div className="account-settings p-y-15 bg-white">
            <div className="container">
                <h2 className="weight-700 p-b-15 color-primary">
                    Email Notifications
                </h2>
                <form action="#" method="POST" onSubmit={this.saveSettings}>
                    <div
                        className="settings-item d-flex justify-content-between align-items-center flex-nowrap p-b-10">
                        <span className="checkbox-title">When someone commented on your review</span>
                        <div className="secondary-checkbox-container m-l-10">
                            <input
                                type='checkbox'
                                className='secondary-checkbox d-none'
                                id='first'
                                name='newcomment'
                                value='true'
                                onChange={this.fillSettings}
                                checked={this.state.settings.newcomment} />
                                <label htmlFor='first'> </label>
                        </div>
                    </div>

                    <div
                        className="settings-item d-flex justify-content-between align-items-center flex-nowrap p-y-10">
                        <span className="checkbox-title">When someone comments on a review you commented</span>
                        <div className="secondary-checkbox-container m-l-10">
                            <input
                                type='checkbox'
                                className='secondary-checkbox d-none'
                                id='second'
                                name='newcommentme'
                                value='true'
                                onChange={this.fillSettings}
                                checked={this.state.settings.newcommentme} />
                            <label htmlFor='second'> </label>
                        </div>
                    </div>

                    <ButtonSubmit text={'Save'} loading={this.state.loading} classes={'m-t-20'} />
                </form>
            </div>

            <LoadingSpinner loading={this.state.loading} />
        </div>
        </div>
        </>
        )
    }
}
