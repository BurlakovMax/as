import React, {Component} from 'react';
import axios from '../../services/api_init';
import LoadingSpinner from "../../components/common/loading-spinner";
import auth from "../../lib/auth";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import {history} from "../../index";

export default class DuplicateAd extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            account: {},
            escortId: props.match.params.escortId,
            selectedType: null,
            escortTypes: [],
        }
    }

    getEscortsTypes = async () => {
        try {
            const res = await axios.get('/api/v1/escorts/types');

            let list = [];

            // parse list from api
            res.data.map((item, index) =>
              {
                  let id = Object.keys(item)[0];
                  list.push({
                      'name': item[id].name,
                      'value': id,
                      'slug': item[id].slug,
                  })
                  return null;
              }
            )

            this.setState({
                escortTypes: list
            });
        } catch (error) {
            config.catch(error);
        }
    }

    selectType = event => {
        this.setState({
            selectedType: event.target.value
        });
    }

    duplicateEscort = async event => {
        event.preventDefault();

        try {
            await axios.post(`/api/v1/pc/escorts/${this.state.escortId}/duplicate`, {
                type: parseInt(this.state.selectedType)
            });

            history.push(auth.getLink('myposts'));
        } catch (error) {
            config.catch(error);
        }
    }

    async componentDidMount() {
        this.setState({loading:true});

        await auth.getAccount()
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getEscortsTypes();

        this.setState({loading:false});
    }

    render() {
        return (
          <div className="content ad-creation" id="duplicate-escort-page">
            <div className="wrapper-ad-creation-container bg-white p-y-20">
                <div className="container">
                    <div className="ad-creation-container">
                        <h2 className="weight-700 color-primary p-b-20">
                            Please select the category that you want to duplicate your ad into:
                        </h2>

                        <form action="#" method="POST" onSubmit={this.duplicateEscort}>

                            <div className="btn-group btn-group-toggle wrapper-primary-radio-btn m-b-25"
                                 data-toggle="buttons">
                                { this.state.escortTypes.map(escortType =>
                                    <label key={escortType.slug} className="btn primary-radio-btn">
                                        <input
                                            type="radio"
                                            name="type"
                                            id="section-for-ad"
                                            onClick={this.selectType}
                                            autoComplete="off"
                                            value={escortType.value}
                                        />{ escortType.name }
                                    </label>
                                ) }

                            </div>

                            <button
                                className="primary-submit-btn"
                                type="submit"
                                disabled={this.state.selectedType === null || !window.navigator.onLine}
                            >
                                <span>Duplicate Into This Category</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

              <LoadingSpinner loading={this.state.loading} />
          </div>
        );
    }
}
