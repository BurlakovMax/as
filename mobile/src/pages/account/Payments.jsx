import React, {Component} from 'react';
import axios from '../../services/api_init';
import Moment from "react-moment";
import $ from "jquery";
import ButtonBordered from "../../components/form/button-bordered";
import Help from "../../components/text/help";
import Pagination from "../../components/common/pagination";
import LoadingSpinner from "../../components/common/loading-spinner";
import {changeTitle, setAccount} from '../../store/common';
import config from "../../lib/config";
import auth from "../../lib/auth";
import Modal from "../../components/common/modal";
import ButtonSubmit from "../../components/form/button-submit";
import FormCard from "../../components/common/FormCard";
import form from "../../lib/form";
import Alert from "../../components/common/alert";

export default class Payments extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            errorMessage: false,
            successMessage: false,
            account: {},

            subscriptions: [],
            totalSubscriptions: 0,
            selectedSubscription: null,

            transactions: [],
            totalTransactions: 0,

            showCards: new URLSearchParams(window.location.search).get('showCards') || false, // use cards tab or not

            cards: [],
            totalCards: 0,
            selectedCard: null,

            card: {},
            cardErrors: {},
        };
    }

    getSubscriptions = async (offset = 0, limit = 10) => {
        this.setState({loading: true});

        try {
            const res = await axios.get(`/api/v1/pc/payments?offset=${offset}&limit=${limit}&property=createdAt&ordering=desc`);

            this.setState({
                subscriptions: res.data.list,
                totalSubscriptions: res.data.total
            });
        } catch (error) {
            config.catch(error);
        }

        this.setState({loading: false});
    }

    getCards = async (offset = 0, limit = 10) => {
        this.setState({loading: true});

        try {
            const res = await axios.get(`/api/v1/pc/creditCards?offset=${offset}&limit=${limit}&ordering=desc`);

            this.setState({
                cards: res.data.list,
                totalCards: res.data.total
            });
        } catch (error) {
            config.catch(error);
        }

        this.setState({loading: false});
    }

    getTransactions = async (offset = 0, limit = 10) => {
        this.setState({loading: true});

        try {
            const res = await axios.get(`/api/v1/pc/transactions?offset=${offset}&limit=${limit}&property=createdAt&ordering=desc`);

            this.setState({
                transactions: res.data.list,
                totalTransactions: res.data.total
            });
        } catch (error) {
            config.catch(error);
        }

        this.setState({loading: false});
    }


    showSubscriptionCancel = e => {
        const id = e.currentTarget.dataset.payment_id;
        this.setState({
            selectedSubscription: id
        });

        $('#cancel-subscription-modal').modal('show');
    }

    cancelSubscription = async (event) => {
        event.preventDefault();

        $('.modal').modal('hide');

        try {
            await axios.put(`/api/v1/pc/subscriptions/${this.state.selectedSubscription}/cancel`);
            await this.getSubscriptions();

            // close all infos
            Array.from(document.getElementsByClassName('dropdown')).forEach(
                function(element, index, array) {
                    element.classList.remove('dropdown_active')
                }
            );
            Array.from(document.getElementsByClassName('dropdown__list')).forEach(
                function(element, index, array) {
                    element.style.display = 'none';
                }
            );

            this.setState({
                selectedSubscription: null,
            });
        } catch (error) {
            config.catch(error);
        }
    }


    toggleSubscription = e => {
        let id = e.currentTarget.dataset.id;
        let parent = document.getElementById('subscription-'+id);
        this.toggleInfo(parent);
    }

    toggleTransaction = e => {
        let id = e.currentTarget.dataset.id;
        let parent = document.getElementById('transaction-'+id);
        this.toggleInfo(parent);
    }

    toggleInfo = (parent) => {
        let info = parent.querySelector('.dropdown__list');
        if (info.style.display === 'block') {
            info.style.display = 'none';
            parent.classList.remove('dropdown_active');
        } else {
            info.style.display = 'block';
            parent.classList.add('dropdown_active');
        }
    }


    fillCard = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value.trim();
        let res = {[name]: value};

        if (name === 'ccCc') {
            // remove visual spaces from cc number
            res = {[name]: value.replace(/\s/g,'')};
        }

        this.setState({
            card: Object.assign(this.state.card, res),
            cardErrors: Object.assign(this.state.cardErrors, {[name]: false})
        });
    }

    validateCard = () => {
        let ok = true;

        // validate common inputs & selects
        let validate = {
            'ccCc': 'You must enter valid card number',
            'ccMonth': 'Invalid card month',
            'ccYear': 'Invalid card year',
        };

        let [errors, errorsCount] = form.validateErrors(validate);

        this.setState({
            cardErrors: errors
        });
        if (errorsCount) {
            ok = false;
        }

        return ok;
    }

    /**
     * Clear card modal values & errors on close
     * @param event
     * @returns {Promise<void>}
     */
    closeCard = (event) => {
        event.preventDefault();

        $('.modal').modal('hide');

        this.setState({
            card: {},
            cardErrors: {},
        });

        this.clearCard();
    }

    // clear card form values
    clearCard = () => {
        ['ccCc','ccMonth','ccYear'].map(id => {
            let input = document.getElementById(id);
            if (input.type === 'tel') {
                input.value = '';
            } else {
                input.selectedIndex = null;
            }
        })
    }

    submitCard = async (event) => {
        event.preventDefault();

        this.setState({
            errorMessage: null,
            successMessage: null,
        });

        // validate form inputs & selects
        if (!this.validateCard()) {
            return false;
        }

        $('.modal').modal('hide');

        this.setState({
            loading: true,
        });

        // TODO: save card to db, need api !!!
        console.log('submitCard', this.state.card);

        this.clearCard();
        await this.getCards();
        this.setState({
            card: {},
            cardErrors: {},
            successMessage: 'Your card successfully saved',
        });


        this.setState({
            loading: false
        });
    }

    showCardDelete = e => {
        const id = e.currentTarget.dataset.card_id;
        this.setState({
            selectedCard: id
        });

        $('#card-delete-modal').modal('show');
    }

    removeCard = async (event) => {
        event.preventDefault();

        $('.modal').modal('hide');

        // TODO: delete card in db, need api !!!
        console.log('removeCard', this.state.selectedCard);

    }


    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount()
            .then(account => {
                if (account) {
                    this.setState({
                        account: account
                    });
                }
            })

        await this.getSubscriptions();
        await this.getTransactions();
        await this.getCards();

        this.setState({loading: false});
    }

    renderCards = () => {
        return (
            <div className={`tab-pane fade ${this.state.showCards? 'show active' : ''}`} id="cards" role="tabpanel" aria-labelledby="cards-tab">
                <div className="bg-white m-b-10">

                    <div className="container p-y-10">
                        <h2 className="color-primary weight-700 f-s-18 d-flex justify-content-between">Your Saved Cards <span>{this.state.totalCards}</span></h2>
                    </div>

                    <Modal
                        id={'card-delete-modal'}
                        title={'Remove Card'}
                        content={
                            <form action="#" method="post" className="w-100 h-100">
                                <h2 className="title color-primary weight-700 p-b-15">
                                    Are you sure you want to remove your card?
                                </h2>

                                <div className="paragraphs m-b-25">
                                    This action cannot be undone.
                                </div>

                                <div className="action-buttons d-flex">
                                    <ButtonBordered classes={'flex-fill m-r-10'} type={'button'} text={'Close'} data={{'data-dismiss': 'modal'}}/>
                                    <ButtonSubmit classes={'flex-fill'} text={'Continue'} handleClick={this.removeCard} />
                                </div>
                            </form>
                        }
                    />

                    <div className="container">
                        {this.state.cards.map((card, index) => {
                            return (
                                <div key={index} id={card.id} className="payment-item m-b-10">
                                    <div className={'payment-item__icon'}>
                                        {card.type ?
                                            (<img src={`/images/cc_pay/${card.type}.svg`} alt={card.type}/>)
                                            :
                                            (<div className="credit_card small unknown"></div>)
                                        }
                                    </div>
                                    <div className={'payment-item__text'}>
                                        <div className={'color-black f-s-16'}>{card.cardNumber}</div>
                                        <div className={'color-label f-s-14'}>Expires {card.expiryMonth}/{card.expiryYear}</div>
                                    </div>
                                    <div data-card_id={card.id} onClick={this.showCardDelete} className={'payment-item__action'}>
                                        Remove
                                    </div>
                                </div>
                            )
                        })}
                    </div>

                    {this.state.totalCards === 0 &&
                        <div className="container">
                            <div className="bg-seventh-gray m-b-10 p-20">
                                <div className="color-tertiary">You have no cards</div>
                            </div>
                        </div>
                    }

                    {/*ADD CARD MODAL*/}
                    <div className="container p-b-5">
                        <div className="payment-item m-b-10 f-s-30 weight-700 color-success" data-toggle="modal" data-target="#card-modal">
                            <div className={'payment-item__icon'}>
                                +
                            </div>
                            <div className={'payment-item__text f-s-16'}>
                                Add Card
                            </div>
                        </div>
                        <Modal
                            id={'card-modal'}
                            title={'Add credit card'}
                            handleClickClose={this.closeCard}
                            content={
                                <form method="post" action="#" onSubmit={this.submit} id="card_save_form">
                                    <FormCard
                                        title={'Card Details'}
                                        handleChange={this.fillCard}
                                        errors={this.state.cardErrors}
                                        hideCVV={true}
                                    />
                                    <div className={'m-b-15 d-flex'}>
                                        <ButtonBordered classes={'flex-fill m-r-10'} type={'button'} text={'Close'} handleClick={this.closeCard}/>
                                        <ButtonSubmit classes={'flex-fill'} type={'button'} text={'Continue'} handleClick={this.submitCard}/>
                                    </div>
                                </form>
                            }
                        />
                    </div>

                    {this.state.totalCards > 0 &&
                        <div className={'container bg-seventh-gray p-b-15 p-t-5'}>
                            <Pagination total={this.state.totalCards} getData={this.getCards} />
                        </div>
                    }

                </div>
            </div>
        )
    }

    renderSubscriptions = () => {
        return (
            <div className={`tab-pane fade ${!this.state.showCards? 'show active' : ''}`} id="subscriptions" role="tabpanel" aria-labelledby="subscriptions-tab">
              <div className="bg-seventh-gray m-b-10 p-b-15">

                  <div className="container p-y-10">
                      <h2 className="color-primary weight-700 f-s-18 d-flex justify-content-between">Active Subscriptions <span>{this.state.totalSubscriptions}</span></h2>
                  </div>

                  <Modal
                      id={'cancel-subscription-modal'}
                      title={'Cancel Subscription'}
                      content={
                          <form action="#" method="post" className="w-100 h-100">
                              <h2 className="title color-primary weight-700 p-b-15">
                                  Are you sure you want to cancel the recurring charges of this subscription?
                              </h2>

                              <div className="paragraphs m-b-25">
                                  This action cannot be undone.
                              </div>

                              <div className="action-buttons d-flex">
                                  <ButtonBordered classes={'flex-fill m-r-10'} type={'button'} text={'Close'} data={{'data-dismiss': 'modal'}}/>
                                  <ButtonSubmit classes={'flex-fill'} text={'Continue'} handleClick={this.cancelSubscription} />
                              </div>
                          </form>
                      }
                  />

                  { this.state.subscriptions.map((subscription, index) =>
                    <div key={index} className="dropdown m-b-1" id={`subscription-${subscription.payment.id}`}>

                        <div className="dropdown__title bg-white" data-id={subscription.payment.id} onClick={this.toggleSubscription}>
                            <div className="container d-flex justify-content-between align-items-center p-y-5">
                                <div>
                                    <div className="weight-700 text-capitalize">
                                        ${config.cents2dollars(subscription.payment.amount)}
                                    </div>
                                    <div className="f-s-14 color-nitro-gray">
                                        <Moment format="YYYY/MM/DD HH:mm:ss">{subscription.payment.createdAt}</Moment>
                                    </div>
                                </div>

                                <div className="dropdown__caret">
                                    <svg className="icon_caret_down" style={{width: '19px', height: '15px'}}>
                                        <use xlinkHref="/images/icons.svg#icon_caret_down"/>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div className="dropdown__list" style={{display: 'none'}}>
                            <div className="container">
                                <div className="d-flex justify-content-between align-items-center border-b-white p-y-15 f-s-16">
                                    <div className="p-r-40 color-nitro-gray text-nowrap">Subject</div>
                                    <div className="content-break lh-normal text-right">
                                        {subscription.payment.items.map(item => {
                                            if (item.topUps > 0) {
                                                return <div key={item.id}>{item.serviceTypes[0]} #{item.topUps}</div>
                                            } else {
                                                return <div key={item.id}>{item.serviceTypes[0]} #{item.escortId}</div>
                                            }
                                        })}
                                    </div>
                                </div>
                            </div>

                            <div className="container">
                                <div className="d-flex justify-content-between align-items-center border-b-white p-y-15 f-s-16">
                                    <div className="p-r-40 color-nitro-gray text-nowrap">Credit Card</div>
                                    <div className="content-break lh-normal text-right">#{subscription.card ? subscription.card.cardNumber : '-' }</div>
                                </div>
                            </div>

                            <div className="container">
                                <div className="d-flex justify-content-between align-items-center border-b-white p-y-15 f-s-16">
                                    <div className="p-r-40 color-nitro-gray text-nowrap">Result</div>
                                    <div className="content-break lh-normal text-right">{subscription.payment.declineCode ? subscription.payment.declineReason : 'Success'}</div>
                                </div>
                            </div>

                            <div className="container p-y-15">
                                {<ButtonBordered id='filters-modal-reset'
                                                 classes='primary-bordered-btn_empty'
                                                 type='button'
                                                 text='Cancel Subscription'
                                                 handleClick={this.showSubscriptionCancel}
                                                 data={{
                                                     'data-payment_id': subscription.payment.id,
                                                 }} />}
                            </div>
                        </div>

                    </div>
                  ) }

                  {this.state.totalSubscriptions > 0 &&
                    <div className={'container'}>
                        <Pagination total={this.state.totalSubscriptions} getData={this.getSubscriptions} />
                    </div>
                  }

                  {this.state.totalSubscriptions === 0 &&
                      <div className="bg-white p-20">
                          <div className="color-tertiary">You have no subscriptions</div>
                      </div>
                  }
              </div>
            </div>
        )
    }

    renderTransactions = () => {
        return (
            <div className="tab-pane fade" id="transactions" role="tabpanel" aria-labelledby="transactions-tab">
              <div className="bg-seventh-gray m-b-10 p-b-15">

                  <div className="container p-y-10">
                      <h2 className="color-primary weight-700 f-s-18  d-flex justify-content-between">Transactions <span>{this.state.totalTransactions}</span></h2>
                  </div>

                  { this.state.transactions.map((transaction, index) =>
                    <div key={index} className="dropdown m-b-1" id={`transaction-${transaction.transaction.id}`}>

                        <div className="dropdown__title bg-white" data-id={transaction.transaction.id} onClick={this.toggleTransaction.bind()}>
                            <div className="container d-flex justify-content-between align-items-center p-y-5">
                                <div>
                                    <div className="weight-700 text-capitalize">
                                        ${config.cents2dollars(transaction.transaction.amount)}
                                    </div>
                                    <div className="f-s-14 color-nitro-gray">
                                        <span className="p-r-15">
                                            <Moment format="YYYY/MM/DD HH:mm:ss">{transaction.transaction.createdAt}</Moment>
                                        </span>
                                    </div>
                                </div>

                                <div className="dropdown__caret">
                                    <svg className="icon_caret_down" style={{width: '19px', height: '15px'}}>
                                        <use xlinkHref="/images/icons.svg#icon_caret_down"/>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div className="dropdown__list" style={{display: 'none'}}>

                            <div className="container">
                                <div className="d-flex justify-content-between align-items-center border-b-white p-y-15 f-s-16">
                                    <div className="p-r-40 color-nitro-gray text-nowrap">Credit Card</div>
                                    <div className="content-break lh-normal text-right">
                                        {transaction.card ? transaction.card.cardNumber : '-'}
                                    </div>
                                </div>
                            </div>

                            <div className="container">
                                <div className="d-flex justify-content-between align-items-center border-b-white p-y-15 f-s-16">
                                    <div className="p-r-40 color-nitro-gray text-nowrap">Transaction ID</div>
                                    <div className="content-break lh-normal text-right">{transaction.transaction.transId}</div>
                                </div>
                            </div>

                            <div className="container">
                                <div className="d-flex justify-content-between align-items-center border-b-white p-y-15 f-s-16">
                                    <div className="p-r-40 color-nitro-gray text-nowrap">Subject</div>
                                    <div className="content-break lh-normal text-right">
                                        {transaction.transaction.items.map(item => {
                                            if (item.topUps > 0) {
                                                return <div key={item.id}>{item.serviceTypes[0]} #{item.topUps}</div>
                                            } else {
                                                return <div key={item.id}>{item.serviceTypes[0]} #{item.escortId}</div>
                                            }
                                        })}
                                    </div>
                                </div>
                            </div>

                            <div className="container">
                                <div className="d-flex justify-content-between align-items-center border-b-white p-y-15 f-s-16">
                                    <div className="p-r-40 color-nitro-gray text-nowrap">Type</div>
                                    <div className="content-break lh-normal text-right">{transaction.transaction.type === 'N' ? 'Normal' : 'Wire'}</div>
                                </div>
                            </div>

                        </div>

                    </div>
                  ) }

                  {this.state.totalTransactions > 0 &&
                    <div className={'container'}>
                        <Pagination total={this.state.totalTransactions} getData={this.getTransactions} />
                    </div>
                  }

                  {this.state.totalTransactions === 0 &&
                      <div className="bg-white p-20">
                          <div className="color-tertiary">You have no transactions</div>
                      </div>
                  }
              </div>
            </div>
        )
    }

    render() {
        return (
        <>
            {this.state.errorMessage && <Alert closable={true} message={this.state.errorMessage} close={this.closeAlert}/>}
            {this.state.successMessage && <Alert closable={true} message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

            {/*TABS*/}
            <div className="primary-tabs-nav-container position-relative bg-white m-b-10 bs-primary">
                <ul className="nav nav-tabs scrolling-tabs d-flex" id="primary-tab" role="tablist">
                    {this.state.showCards &&
                        <li className="nav-item d-flex align-items-center flex-fill">
                            <a className={`nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1 ${this.state.showCards? 'active' : ''}`}
                               id="cards-tab" data-toggle="tab" href="#cards" role="tab" aria-controls="cards"
                               aria-selected="true">
                                Cards
                            </a>
                        </li>
                    }
                    <li className="nav-item d-flex align-items-center flex-fill">
                        <a className={`nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1 ${!this.state.showCards? 'active' : ''}`}
                           id="subscriptions-tab" data-toggle="tab" href="#subscriptions" role="tab" aria-controls="subscriptions" aria-selected="false">
                            Subscriptions
                        </a>
                    </li>
                    <li className="nav-item d-flex align-items-center flex-fill">
                        <a className="nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1"
                           id="transactions-tab" data-toggle="tab" href="#transactions" role="tab" aria-controls="transactions" aria-selected="false">
                            Transactions
                        </a>
                    </li>
                </ul>
            </div>

            <div className="content payment" id="payments-list">

                {/*TABS CONTENT*/}
                <div className="tab-content primary-tab-content">
                    {this.state.showCards && this.renderCards()}

                    {this.renderSubscriptions()}

                    {this.renderTransactions()}
                </div>

                <Help/>
            </div>

            <LoadingSpinner loading={this.state.loading} />
        </>
        )
    }
}
