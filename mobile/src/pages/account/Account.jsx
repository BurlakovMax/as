import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from '../../services/api_init';
import { changeTitle, setAccount, offline} from '../../store/common';
import Alert from "../../components/common/alert";
import LoadingSpinner from "../../components/common/loading-spinner";
import auth from "../../lib/auth";
import adbuild from "../../lib/adbuild";
import Modal from "../../components/common/modal";
import ButtonSubmit from "../../components/form/button-submit";
import $ from "jquery";
import config from "../../lib/config";
import business from "../../lib/business";
import {history} from "../../index";
import Offline from "../../components/text/offline";

export default class Account extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            errorMessage: false,
            loading: false,
            account: {},

            countPosts: 0,
            countReviews: 0,

            finish: false, // unfinished flag
            finishEscort: false,
            finishBusiness: false,
        };
    }

    signOut = event => {
        event.preventDefault();

        auth.clearToken()
          .then(error => {
              if (!error) {
                  auth.gotoSignin()
              }
          })
    }

    saveAvatar = async event => {
        const formData = new FormData();
        formData.append('image', event.target.files[0])

        const params = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        try {
            await axios.post('/api/v1/pc/account/avatar_upload', formData, params);
            await auth.getAccount()
              .then(account => {
                  if (account) {
                      this.setState({
                          account: account
                      });
                      setAccount(account);
                  }
              })
        } catch (error) {
            config.catch(error);

            let errorText = config.getUploadError('image');
            if (error.response.status === 400 || error.response.status === 413) {
                errorText = config.getUploadError('image', error.response.status);
            }

            this.setState({
                errorMessage: errorText
            });
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    getPostCount = async () => {
        try {
            const res = await axios.get('/api/v1/pc/account/posts_count');
            this.setState({countPosts: res.data});
        } catch (error) {
            config.catch(error);
        }
    }

    getReviewCount = async () => {
        try {
            const res = await axios.get('/api/v1/pc/account/reviews_count');
            this.setState({countReviews: res.data});
        } catch (error) {
            config.catch(error);
        }
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await this.getPostCount();
        await this.getReviewCount();

        await auth.getAccount()
          .then(account => {
              if (account) {
                  this.setState({
                      account: account
                  });
                  setAccount(account);
              }
          })

        this.checkFinish();
        await this.checkEmailConfirmed();

        this.setState({ loading: false });
    }

    checkFinish = () => {
        // 1. check unfinished business
        let finishBusiness = business.getCookieFinish();
        if (finishBusiness) {
            this.setState({
                finish: true,
                finishBusiness: finishBusiness,
            });
            $('#finish').modal('show');
            return;
        }

        // 2. check unfinished escort
        let finishEscort = adbuild.checkCookieFinish();
        if (finishEscort && this.state.account) {
            if (parseInt(finishEscort.accountId) === this.state.account.id) {
                this.setState({
                    finish: true,
                    finishEscort: finishEscort,
                });
                $('#finish').modal('show');
            }
        }
    }

    checkEmailConfirmed = () => {
        if (this.state.account.emailConfirmed === false) {
            $('#confirm').modal('show');
        }
    }

    renderAgencyText = () => {
        return (
          <div className="content agency-long-text p-b-0 m-b-10">
              <div className="agency-long-text-container bg-white p-t-25 p-b-10">
                  <div className="container">
                      <h2 className="color-primary weight-700 text-center p-b-20">
                          Agency Guidelines
                      </h2>

                      <p className="m-t-10 p-b-25">
                          Please wire transfer <span className={'color-error'}>2,700 EUR</span> or more for your account credit to the following Account number. We will no longer be taking credit cards from Agencies. We are a European company not Americain. Therefore we do not bank in the USA.
                      </p>

                      <div className="orange_hl m-b-15">
                          <ul className="list-unstyled">
                              <li><span className={'weight-600'}>Bank name:</span> OCBC</li>
                              <li><span className={'weight-600'}>Bank address:</span> 65 Chulia St, #01-00, Singapore, 0495132</li>
                              <li><span className={'weight-600'}>Account Number:</span> 713494789001</li>
                              <li><span className={'weight-600'}>Swift code:</span> OCBCSGSG (or OCBCSGSGXXX)</li>
                          </ul>
                          <ul className="list-unstyled">
                              <li><span className={'weight-600'}>Company:</span> Music Sound Better PTE LTD</li>
                              <li><span className={'weight-600'}>Company address:</span> 20 Collyer Quay, suite 09-01, Singapore</li>
                          </ul>

                          <div className={'color-error weight-600'}>
                              <p className={'m-b-15'}>Do not send ANY INFO on the wire reference (wire notes) at your bank except your account number which is #{config.getBankAccountId()}.</p>
                              <p>IF YOU PUT ANYTHING OTHER IN THE REFERENCE THAN YOUR ACCOUNT NUMBER, YOUR ACCOUNT WILL BE BANNED AND YOUR MONEY WILL BE SEIZED.</p>
                          </div>
                      </div>

                      <p className="p-b-10">
                          We will no longer be taking wire info via email. You will upload the info along with the image of your wire from your account page.
                      </p>
                      <div className="m-b-35">
                          <ButtonSubmit link={'/account/wire'} text={'Upload Wire Info'} />
                      </div>

                      <p className="color-error text-uppercase weight-600 m-t-15 m-b-5">
                          Banks are slow, not fast
                      </p>
                      <div className={'m-b-20'}>
                          If you use Bofa, Chase, Citi to send your wire it will take bout 5 working days before the money is in our account. All other banks can take up to 3 weeks. Other banks use whats called
                          &nbsp;<a href="https://blog.revolut.com/swift-sepa-how-international-money-transfers-actually-work/" target="_blank" rel="noopener noreferrer" className="color-secondary weight-600">Intermediary Banks</a>.
                          This means your bank sends your money to another bank &amp; then that bank sends it to our bank which can take up to 3 weeks.
                          <p className={'m-t-15'}>BANKS WORK SLOW NOT FAST.</p>
                          When we see the funds in our account (We check the account twice daily, Hong Kong time) we will credit your account.
                      </div>

                      <p>
                          The <span className={'color-error'}>2,700 EUR</span> or any amount you send above that will be credited to your account &amp;
                          used anyway you want when the funds are in your account. Your account will be allowed 10 individual ads &amp; you may buy as many reposts as you like, star covers, side sponsor ads, sticky sponsor ads, or any other type of ads for your 10 individual ads.
                      </p>

                      <p className="color-error m-y-15">
                          Banks are closed every Saturday, Sunday, the following holidays, &amp; so are we.
                      </p>

                      <p className={'weight-600'}>
                          Chinese Holidays:
                      </p>
                      <table>
                          <tbody>
                          <tr><td style={{width: '70px'}}>1 May</td><td style={{width: '50px'}}>Wed</td><td>Labour Day</td></tr>
                          <tr><td>12 May</td><td>Sun</td><td>Birthday of Buddha</td></tr>
                          <tr><td>13 May</td><td>Mon</td><td>Birthday of Buddha Holiday</td></tr>
                          <tr><td>7 Jun</td><td>Fri</td><td>Tuen Ng Festival</td></tr>
                          <tr><td>1 Jul</td><td>Mon</td><td>HKSAR Establishment Day</td></tr>
                          <tr><td>14 Sep</td><td>Sat</td><td>The Day Following Mid-Autumn Festival</td></tr>
                          <tr><td>1 Oct</td><td>Tue</td><td>National Day</td></tr>
                          <tr><td>7 Oct</td><td>Mon</td><td>Chung Yeung Festival</td></tr>
                          <tr><td>25 Dec</td><td>Wed</td><td>Christmas Day</td></tr>
                          <tr><td>26 Dec</td><td>Thu</td><td>The First Weekday After Christmas Day</td></tr>
                          </tbody>
                      </table>

                  </div>
              </div>
          </div>
        )
    }

    /**
     * Go to payment page - for business or escort
     */
    gotoPayment = (event) => {
        event.preventDefault();

        $('.modal').modal('hide');

        if (this.state.finishBusiness) {
            let params = business.getCookieFinish();
            history.push(config.getPaymentUrl(params));
            return;
        }

        if (this.state.finishEscort) {
            history.push(adbuild.getPaymentUrl(this.state.finishEscort.escortId));
        }
    }

    gotoConfirm = async (event) => {
        event.preventDefault();

        $('.modal').modal('hide');

        history.push(auth.getLink('confirm-email'));
    }

    render() {
        return (
            <>
                { offline.getState() ? (
                    <Offline/>
                ) : (
                    <>
                        {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

                        {this.state.account.agency && this.renderAgencyText()}

                        <div className="content settings" id="account-page">
                            <div className="edit-image-container p-y-15 bg-white">
                                <div className="container">
                                    <div className="edit-image-container-inner">
                                        <form id="change-avatar-form" className="edit-image-form d-flex justify-content-center"
                                              action="#" method="POST" encType="multipart/form-data">
                                            <label className="position-relative m-0" htmlFor="change-avatar">
                                                {this.state.account.avatar ?
                                                    (<img src={`${config.getImageServer()}${this.state.account.avatar}`} alt="avatar"/>)
                                                    :
                                                    (<div className="empty-avatar" style={{'backgroundImage': 'url(/images/icons/User_1.jpg)'}}/>)
                                                }
                                                <svg width="30" height="30" viewBox="0 0 16 16" fill="currentColor"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" clipRule="evenodd"
                                                          d="M13.8968 3.91901L13.5312 4.28463L11.4548 2.20825L11.8215 1.84263C12.2237 1.44035 12.8837 1.43825 13.2871 1.84263L13.8968 2.45235C14.3001 2.85673 14.3001 3.51463 13.8968 3.91901ZM12.4259 5.38882L5.21098 12.6048L3.13564 10.5295L10.3495 3.31349L12.4259 5.38882ZM3.7286 13.3308L1.90364 13.8368L2.4086 12.0119L3.7286 13.3308ZM15.002 1.34711L14.3923 0.737393C13.4086 -0.246321 11.6957 -0.245274 10.7162 0.737393L1.44688 10.0067C1.3526 10.1021 1.2845 10.2204 1.24679 10.3503L0.0284053 14.7493C-0.0470233 15.0206 0.0305005 15.3119 0.228501 15.5099C0.37831 15.6597 0.577358 15.7393 0.781643 15.7393C0.851834 15.7393 0.920977 15.7299 0.99012 15.7121L5.39012 14.4926C5.51898 14.457 5.63841 14.3879 5.73374 14.2915L15.002 5.02425C16.014 4.01015 16.014 2.3612 15.002 1.34711Z"/>
                                                </svg>
                                            </label>
                                            <input
                                                className="d-none"
                                                id="change-avatar"
                                                onChange={this.saveAvatar}
                                                name="avatar"
                                                accept="image/*"
                                                type="file"/>
                                        </form>
                                    </div>

                                    <h1 className="weight-700 text-center p-t-20 color-primary primary-ls">{this.state.account.name}</h1>
                                </div>
                            </div>

                            <div className="forums-reviews bg-white m-y-10">
                                <div className="container">
                                    <div className="primary-info-link-group d-flex align-items-center justify-content-between flex-nowrap">
                                        <div className="link-container d-flex flex-column align-items-center p-y-5">
                                            <span className="count color-primary weight-600">{this.state.countPosts}</span>
                                            <span className="name f-s-16 color-tertiary">Forum Posts</span>
                                        </div>
                                        <div className="link-container d-flex flex-column align-items-center p-y-5">
                                            <span className="count color-primary weight-600">{this.state.countPosts}</span>
                                            <span className="name f-s-16 color-tertiary">Reviews</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="bg-white m-b-10">
                                <div className="container">
                                    <div className="list-with-icons">
                                        <ul className="button-list">
                                            <li>
                                                <Link className="link p-y-15" to="/account/payments">
                                                    <div className="icon-with-title m-r-10">
                                                        <div className="icon m-r-10 green">
                                                            <svg className="payments">
                                                                <use xlinkHref="/images/icons.svg#payments"/>
                                                            </svg>
                                                        </div>
                                                        <span className="color-quaternary weight-600">Payments</span>
                                                    </div>
                                                    <div className="arrow-right">
                                                        <svg className="arrow-right-icon">
                                                            <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                        </svg>
                                                    </div>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link className="link p-y-15" to="/account/change-username">
                                                    <div className="icon-with-title m-r-10">
                                                        <div className="icon m-r-10 light-blue">
                                                            <svg className="edit-username-icon">
                                                                <use xlinkHref="/images/icons.svg#edit-username" />
                                                            </svg>
                                                        </div>
                                                        <span className="color-quaternary">Change username</span>
                                                    </div>
                                                    <div className="arrow-right">
                                                        <svg className="arrow-right-icon">
                                                            <use xlinkHref="/images/icons.svg#arrow-right" />
                                                        </svg>
                                                    </div>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link className="link p-y-15" to="/account/change-password">
                                                    <div className="icon-with-title m-r-10">
                                                        <div className="icon m-r-10 pink">
                                                            <svg className="lock-question">
                                                                <use xlinkHref="/images/icons.svg#lock-question" />
                                                            </svg>
                                                        </div>
                                                        <span className="color-quaternary">Change Password</span>
                                                    </div>
                                                    <div className="arrow-right">
                                                        <svg className="arrow-right-icon">
                                                            <use xlinkHref="/images/icons.svg#arrow-right" />
                                                        </svg>
                                                    </div>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link className="link p-y-15" to={auth.getLink('change-email')}>
                                                    <div className="icon-with-title m-r-10">
                                                        <div className="icon m-r-10 yellow">
                                                            <svg className="envelope">
                                                                <use xlinkHref="/images/icons.svg#envelope" />
                                                            </svg>
                                                        </div>
                                                        <span className="color-quaternary">Change Email</span>
                                                    </div>
                                                    <div className="arrow-right">
                                                        <svg className="arrow-right-icon">
                                                            <use xlinkHref="/images/icons.svg#arrow-right" />
                                                        </svg>
                                                    </div>
                                                </Link>
                                            </li>

                                            <li>
                                                <Link className="link p-y-15" to="/account/change-settings">
                                                    <div className="icon-with-title m-r-10">
                                                        <div className="icon m-r-10 brown">
                                                            <svg className="gear">
                                                                <use xlinkHref="/images/icons.svg#gear"/>
                                                            </svg>
                                                        </div>
                                                        <span className="color-quaternary">Settings</span>
                                                    </div>
                                                    <div className="arrow-right">
                                                        <svg className="arrow-right-icon">
                                                            <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                        </svg>
                                                    </div>
                                                </Link>
                                            </li>

                                            <li>
                                                <a className="link p-y-15" href="/#" onClick={this.signOut}>
                                                    <div className="icon-with-title m-r-10">
                                                        <div className="icon m-r-10 gray">
                                                            <svg className="logout">
                                                                <use xlinkHref="/images/icons.svg#logout"/>
                                                            </svg>
                                                        </div>
                                                        <span className="color-quaternary">Logout</span>
                                                    </div>
                                                    <div className="arrow-right">
                                                        <svg className="arrow-right-icon">
                                                            <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                        </svg>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div className="bg-white m-b-10">
                                <div className="container">
                                    <div className="list-with-icons">
                                        <ul className="button-list">
                                            <li>
                                                <a className="link p-y-15" rel="noopener noreferrer" target={'_blank'} href="https://www.escorts.biz/login">
                                                    <div className="icon-with-title m-r-10">
                                                        <div className="icon m-r-10 blue-sea">
                                                            <img src="/images/icons/body_person.svg" alt="Login to Escorts.biz"/>
                                                        </div>
                                                        <span className="color-quaternary">Login to Escorts.biz</span>
                                                    </div>
                                                    <div className="arrow-right">
                                                        <svg className="arrow-right-icon">
                                                            <use xlinkHref="/images/icons.svg#arrow-right"/>
                                                        </svg>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div className="bg-white account-footer">
                                <div className="container p-x-5">
                                    <div className="secondary-info-link-group d-flex color-secondary border-0 p-y-5">
                                        {/*switch to desktop - disabled*/}
                                        {/*                                    <Link to={auth.getLink('home')} className="link-container justify-content-start">
                                        <div className="icon">
                                            <img src="/images/icons/monitor.svg" alt="Full Site"/>
                                        </div>
                                        <span className="title">Full Site</span>
                                    </Link>*/}
                                        <Link to={auth.getLink('terms')} className="link-container justify-content-start">
                                            <div className="icon">
                                                <svg className="icon_tos">
                                                    <use xlinkHref="/images/icons.svg#icon_tos" />
                                                </svg>
                                            </div>
                                            <span className="title">Terms Of Service</span>
                                        </Link>
                                        <Link to={auth.getLink('privacy')} className="link-container justify-content-start">
                                            <div className="icon">
                                                <img src="/images/icons/privacy_policy.svg" alt="Privacy Policy"/>
                                            </div>
                                            <span className="title">Privacy Policy</span>
                                        </Link>
                                        <Link to={auth.getLink('contact')} className="link-container justify-content-start">
                                            <div className="icon">
                                                <img src="/images/icons/messages.svg" alt="Contact"/>
                                            </div>
                                            <span className="title">Contact Us</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {this.state.finish &&
                        <Modal
                            id={'finish'}
                            title={'Unfinished payment'}
                            content={
                                <div className="wrapper-ad-creation-container bg-white color-secondary p-t-15 p-b-25 m-b-10">
                                    <div className="container">
                                        <div className="ad-creation-container">

                                            <h2 className="color-primary text-center p-b-20">
                                                You have unpaid {this.state.finishBusiness? 'advertising package' : 'escort ad'} waiting.
                                            </h2>

                                            <ButtonSubmit handleClick={this.gotoPayment} text={'Continue to payment'}/>

                                        </div>
                                    </div>
                                </div>
                            }
                        />
                        }

                        {this.state.account.emailConfirmed === false &&
                        <Modal
                          id={'confirm'}
                          title={'Email is not confirmed'}
                          content={
                              <div className="wrapper-ad-creation-container bg-white color-secondary p-t-15 p-b-25 m-b-10">
                                  <div className="container">
                                      <div className="ad-creation-container">

                                          <h2 className="color-primary text-center p-b-20">
                                              You haven't confirmed your e-mail. To have a full access to the site, please, confirm it.
                                          </h2>

                                          <ButtonSubmit handleClick={this.gotoConfirm} text={'Confirm'}/>

                                      </div>
                                  </div>
                              </div>
                          }
                        />
                        }
                    </>
                ) }

                <LoadingSpinner loading={this.state.loading} />
            </>
        )
    }
}
