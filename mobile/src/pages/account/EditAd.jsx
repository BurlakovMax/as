import React, {Component} from 'react';
import {history} from "../../index";
import { changeTitle } from '../../store/common';
import axios from '../../services/api_init';
import Alert from "../../components/common/alert";
import ButtonSubmit from "../../components/form/button-submit";
import CheckboxSecondary from "../../components/form/checkbox-secondary";
import Input from "../../components/form/input";
import LoadingSpinner from "../../components/common/loading-spinner";
import Modal from "../../components/common/modal";
import Select from "../../components/form/select";
import SelectTags from "../../components/form/select-tags";
import Textarea from "../../components/form/textarea";
import FormPhoto from "../../components/common/FormPhoto";
import FormSocial from "../../components/common/FormSocial";
import FormPhone from "../../components/common/FormPhone";
import adbuild from "../../lib/adbuild";
import form from "../../lib/form";
import auth from "../../lib/auth";
import config from "../../lib/config";
import uploader from "../../lib/uploader";
import phone from "../../lib/phone";

import Visiting from "../adbuild/visiting";
import Summary from "../adbuild/summary";
import Steps from "../adbuild/steps";
import Upgrades from "../adbuild/upgrades";
import escorts from "../../services/escorts";

export default class EditAd extends Component {
    constructor(props) {
        super(props);

        this.interval = false;

        let titles = {
            'edit': 'Edit Ad',
            'renew': 'Renew Ad',
            'step3': 'Post Ad',
        };
        changeTitle(titles[this.props.action]);

        this.state = {
            account: {},
            errorMessage: false,
            loading: false,

            escortId: props.match.params.escortId,
            escort: {},
            type: {},
            summary: {},

            total: 0,
            price: 0,

            errors: {},
            errorAvailable: '',
            errorCall: '',
            errorEmail: '',

            listAge: [],
            listEthnicity: [],
            listLanguage: [],
            listHeightFeet: [],
            listHeightInches: [],
            listEyeColor: [],
            listHairColor: [],
            listBuild: [],
            listCupSize: [],
            listKitty: [],
            listReply: [],
            listPenisSize: [],

            incallShow: false,
            outcallShow: false,

            visitingFrom: '',
            visitingTo: '',

            phone: '',
            phoneOld: '',
            phoneValid: false,
            phoneResult: false,
            phoneError: false,

            pictures: [],
            picturesMax: 8,
            picturesLimit: false,
            picturesError: false,
            picturesLoading: false,

            videos: [],
            videosMax: 3,
            videosLimit: false,
            videosError: false,
            videoProgress: 0,
        };
    }

    getEscort = async () => {
        try {
            const res = await axios.get(`/api/v1/escorts/${this.state.escortId}`);
            config.debug('ESCORT', res.data);

            // check renew status
            if (this.props.action === 'renew' && res.data.status !== 0) {
                history.push(auth.getLink('myposts'));
                return;
            }

            // parse images list
            let pictures = [];
            res.data.images.map(image =>
              {
                  let item = {
                      id: 'local', // to delete only in state
                      name: image,
                      url: `${config.getImagePath('escort')}/${image}`,
                  }
                  pictures.push(item);
                  return null;
              }
            )

            // parse videos list
            let videos = [];
            res.data.videos.map(video =>
              {
                  let id = video.split('.').slice(0, -1).join('.');
                  let item = {
                      id: 'local',  // to delete only in state
                      videoName: video,
                      image: `${config.getVideoPath('escort')}/${id}.jpg`,
                      url: `${config.getVideoPath('escort')}/${video}`,
                  }
                  videos.push(item);
                  return null;
              }
            );

            let escort = res.data;
            escort.heightInches = escort.heightFeet * 100 + escort.heightInches;

            this.setState({
                escort: escort,
                locations: res.data.locations,
                pictures: pictures,
                videos: videos,
                phone: res.data.phone,
                phoneOld: res.data.phone,
            });
        } catch (error) {
            config.catch(error);
        }
    }

    /**
     * Fill in visiting dates
     */
    getVisiting = () => {
        let escort = this.state.escort;
        if (escort.visitingFrom && escort.visitingTo) {
            let from = new Date(escort.visitingFrom).toLocaleDateString('en-US');
            let to = new Date(escort.visitingTo).toLocaleDateString('en-US');

            document.getElementById('visiting_button').innerHTML = from + ' - ' + to;
            document.getElementById('visitingFrom').value = from;
            document.getElementById('visitingTo').value = to;
            document.getElementById('isVisiting').value = 1;
        }
    }

    /**
     * Fill in summary for renew
     */
    getSummary = () => {
        let summary = {};

        this.state.escort.locations.map(
          loc => {
              let city = {};
              city.id = loc.locationId;
              city.name = loc.cityName;
              city.price = this.state.price;
              city.state = loc.stateName;
              city.upgrade = '';
              summary[loc.locationId] = city;
              return null;
          }
        );

        this.setState({
            summary: summary,
        });

        config.debug('SUMMARY', summary);
    }

    getType = async () => {
        this.setState({
            type: await escorts.getTypeById(this.state.escort.rawType),
        });
    }

    getPrice = async () => {
        try {
            const res = await axios.get('/api/v1/escorts/price');

            this.setState({
                price: res.data
            });
        } catch (error) {
            config.catch(error);
        }
    }


    deletePhoto = async event => {
        event.preventDefault();

        let result = await uploader.deletePhoto(
          event,
          this.state.pictures,
          this.state.picturesMax,
          this.state.escort.id,
        );

        this.setState({
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })
    }

    uploadPhoto = async event => {
        event.preventDefault();

        this.setState({
            picturesLoading: true
        });

        let result = await uploader.uploadPhoto(
          event,
          this.state.pictures,
          this.state.picturesMax
        );

        this.setState({
            picturesLoading: false,
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })
    }


    deleteVideo = async event => {
        event.preventDefault();

        let ok = false;
        let index = event.currentTarget.dataset.index;
        let id = event.currentTarget.dataset.id;
        let name = event.currentTarget.dataset.name;

        // deleting existing videos & images (by name) - escort edit
        if (id === 'local') {
            let img = name.split('.').slice(0, -1).join('.');
            try {
                await axios.delete(`/api/v1/pc/escorts/${this.state.escort.id}/videos`, {
                    data: {
                        videos: [name],
                        images: [`${img}.jpg`]
                    }
                });
                ok = true;
            } catch (error) {
                config.catch(error);

                this.setState({
                    videosError: 'Error deleting video',
                });
            }
        } else {
            // deleting temporary videos & images (by id) - escort step3
            try {
                await axios.delete(`/api/v1/escorts/video_delete`, {
                    data: {
                        id: id
                    }
                });
                ok = true;
            } catch (error) {
                config.catch(error);

                this.setState({
                    videosError: 'Error deleting video',
                });
            }
        }

        if (ok) {
            // updating list
            let list = this.state.videos;
            list.splice(index, 1);
            this.setState({
                videos: list
            })

            if (list.length < this.state.videosMax) {
                this.setState({
                    videosLimit: false,
                    videosError: false,
                });
            }
        }
    }

    uploadVideo = async event => {
        this.setState({
            videosError: '',
        });

        // check limit
        if (this.state.videos.length >= this.state.videosMax) {
            this.setState({
                videosLimit: true,
                videosError: 'Maximum video count reached.',
            });
            return false;
        }

        try {
            const formData = new FormData();
            formData.append('video', event.target.files[0])

            // start uploading & show progress video
            let config = {
                onUploadProgress: (progressEvent) => {
                    this.setState({
                        videosProgress: Math.round( (progressEvent.loaded * 100) / progressEvent.total )
                    });
                },
                headers: {
                    'content-type': 'multipart/form-data'
                }
            };
            const res = await axios.post('/api/v1/escorts/video_upload', formData, config);

            // updating list
            let list = this.state.videos;
            let data = res.data;
            data['processing'] = true;
            list.push(data)
            this.setState({
                videos: list
            })
            this.interval = setInterval(this.uploadVideoProcess, 3000);

            this.setState({
                videosProgress: 0,
            });
        } catch (error) {
            config.catch(error);

            let videosError = config.getUploadError('video');
            if (error.response.status === 400 || error.response.status === 413) {
                videosError = config.getUploadError('video', error.response.status);
            }

            this.setState({
                videosProgress: 0,
                videosError: videosError
            });
        }
    }

    uploadVideoProcess = async () => {
        let videos = this.state.videos;

        videos.map(video => {
            if (video.processing) {
                fetch(config.getImageServer() + video.image, { method: 'GET' })
                  .then(res => {
                      if (res.ok) {
                          video.processing = false;
                      }
                  }).catch(error => {
                    config.catch(error);
                });
            }
        })

        let finished = videos.map(video => {
            return video.processing === false
        })

        this.setState({
            videos: videos
        });

        if (finished) {
            this.interval = false
        }
    }


    /**
     * Add/Remove city (by checkbox)
     * @param event
     * @returns {Promise<void>}
     */
    toggleUpgrade = (event) => {
        //event.preventDefault();

        let checkbox = event.currentTarget;
        let id = parseInt(checkbox.value);
        let action = (checkbox.checked)? 'add' : 'remove';
        let summary = this.state.summary;

        // create city object from upgrade checkbox data
        let city = {
            id: id,
            name: checkbox.dataset.name,
            state: checkbox.dataset.state,
            price: parseFloat(checkbox.dataset.price),
            upgrade: checkbox.dataset.upgrade,
        };

        if (action === 'add') {
            summary.push(city);
        } else {
            for (let key in summary) {
                let item = summary[key];
                if (item.id === city.id && item.upgrade === city.upgrade) {
                    delete summary[key];
                }
            }
        }

        this.setState({
            summary: summary
        });
    }

    /**
     * Add/Remove STICKY city (by radio)
     * @param event
     */
    toggleUpgradeRadio = (event) => {
        event.preventDefault();

        let parent = event.currentTarget;
        let inputs = parent.getElementsByTagName('input');
        let id, upgrade, price, name, state;
        let summary = this.state.summary;

        for (let radio of inputs) {
            if (radio.checked) {
                id = parseInt(radio.value);
                upgrade = radio.dataset.upgrade;
                price = radio.dataset.price;
                name = radio.dataset.name;
                state = radio.dataset.state;
            }
        }

        // delete old radio
        for (let key in summary) {
            let item = summary[key];
            if (item.id === id && (item.upgrade !== false && item.upgrade.indexOf('Sticky') !== -1)) {
                delete summary[key];
            }
        }

        // add new radio
        let city = {
            id: id,
            name: name,
            price: price,
            upgrade: upgrade,
            state: state,
        };
        summary.push(city);

        this.setState({
            summary: summary
        });
    }

    /**
     * Remove city in summary (by close icon)
     * @param event
     */
    removeSummaryCity = (event) => {
        event.preventDefault();

        let summary = adbuild.removeSummaryCity(this.state.summary, event);

        this.setState({
            summary: summary,
        });
    }


    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    /**
     * Toggle incall & outcall rates inputs
     */
    toggleRate = (event) => {
        let id = event.currentTarget.getAttribute('for');

        if (id === 'incall') {
            this.setState({
                incallShow: !this.state.incallShow
            });
        }

        if (id === 'outcall') {
            this.setState({
                outcallShow: !this.state.outcallShow
            });
        }
    }


    /**
     * Verify phone number on input blur
     */
    phoneVerify = async () => {
        let phoneNumber = phone.getPhoneInputValue();

        phone.setPhoneInputError(false);
        this.setState({
            phoneError: false
        });

        // do not verify phone when edit - if not changed
        if (this.props.action !== 'step3' && phoneNumber === this.state.phoneOld) {
            this.setState({
                phoneValid: true,
                phoneResult: true,
            });
            return;
        }

        let phoneError = form.validatePhone();
        if (phoneError) {
            this.setState({
                phoneError: phoneError
            });
            return false;
        }

        this.setState({
            phone: phoneNumber,
        });

        await this.phoneCheck(phoneNumber);
    }

    phoneCheck = async (phoneNumber) => {
        this.setState({
            phoneValid: false,
            phoneError: false,
        });

        let result = await phone.phoneCheck(phoneNumber);
        if (!result.error) {
            this.setState({
                phoneValid: result.valid,
                phoneResult: true,
            });
        }
    }


    validate = () => {
        let ok = true;

        // validate available & call rates
        if (this.state.type.id !== 14) {
            let errorAvailable = adbuild.validateAvailable();
            this.setState({
                errorAvailable: errorAvailable
            });

            let errorCall = adbuild.validateCall();
            this.setState({
                errorCall: errorCall
            });

            if (errorAvailable || errorCall) {
                ok = false;
            }
        }

        // validate common inputs & selects
        let validate = {
            'age': 'You must specify your age. Must be over 18.',
            'name': 'You must include your working name',
            'title': 'You must include an introduction',
            'content': 'You must include some ad text',
        };
        let [errors, errorsCount] = form.validateErrors(validate);
        this.setState({
            errors: errors
        });
        if (errorsCount) {
            ok = false;
        }

        // validate photos
        let picturesError = form.validatePictures(this.state.pictures.length)
        if (picturesError) {
            this.setState({
                picturesError: picturesError
            });
            ok = false;
        }

        // validate phone
        let phoneError = form.validatePhone();
        if (phoneError) {
            this.setState({
                phoneError: phoneError,
                phoneResult: false,
            });
            ok = false;
        }

        if (config.useEscortEmail()) {
            // validate email
            this.setState({
                errorEmail: false
            });
            let emailInput = document.getElementById('email');
            let emailError = form.validateEmail(emailInput.value);
            if (emailError) {
                this.setState({
                    errorEmail: emailError
                });
                ok = false;
                const y = (emailInput.getBoundingClientRect().top + window.pageYOffset) - 170;
                window.scrollTo({top: y, behavior: 'smooth'});
            }
        }

        return ok;
    }

    submit = async (event) => {
        event.preventDefault();

        // save locations for pay link
        adbuild.saveCookieLocations(this.state.summary, false);

        let errorText = '';
        if (this.props.action === 'step3') {
            errorText = 'Error creating your ad. Please try again.';
        } else {
            errorText = 'Error updating your ad. Please try again.';
        }

        this.setState({
            errorMessage: false,
            errors: {},
        });

        // validate form inputs & selects
        if (!this.validate()) {
            return false;
        }

        // clear upload inputs
        ['photo','video'].map(id => {
            let input = document.getElementById(id);
            if (input) {
                input.value = '';
            }
            return null;
        })

        try {
            const form = event.currentTarget;
            const formData = new FormData(form);

            const heightFeet = Math.round(parseInt(formData.get('heightInches')) / 100);
            formData.set('heightFeet', heightFeet);
            const heightInches = Math.round(parseInt(formData.get('heightInches') % 100));
            formData.set('heightInches', heightInches);

            // save full phone
            formData.set('phone', this.state.phone);

            // append photos
            this.state.pictures.map(item => {
                  formData.append('images[]', item.name);
                  return null;
            })

            // append videos
            this.state.videos.map(item => {
                  formData.append('videos[]', item.videoName)
                  return null;
            })

            if (this.state.account) {
                formData.append('accountId', this.state.account.id)
            }

            if (this.props.action === 'step3') {
                /*CREATE ESCORT*/
                const res = await axios.post('/api/v1/escorts', formData, config);

                if (res.data.id) {
                    if (!this.state.phoneValid) {
                        adbuild.saveCookiePhone(this.state.phone);
                        adbuild.saveCookieEscortId(res.data.id);
                        window.location.href = auth.getLink('adbuild-confirm');
                    } else {
                        adbuild.gotoPayment(res.data.id, this.state.account.id, 'escort');
                    }
                } else {
                    this.setState({
                        errorMessage: errorText
                    });
                }
            } else {
                /*EDIT OR RENEW ESCORT*/
                formData.append('_method', 'PUT');
                const res = await axios.post(`/api/v1/pc/escorts/${this.state.escortId}`, formData);

                if (res.status === 204) {
                    let phoneValid = this.state.phoneValid;
                    // phone didn't changed
                    if (this.state.phone === this.state.phoneOld) {
                        phoneValid = true
                    }
                    if (!phoneValid) {
                        adbuild.saveCookiePhone(this.state.phone);
                        adbuild.saveCookieEscortId(this.state.escortId);
                        window.location.href = auth.getLink('adbuild-confirm', {'src': 'edit'});
                    } else {
                        // update escort in local db
                        await escorts.getById(this.state.escortId, true);

                        if (this.props.action === 'edit') {
                            window.location.href = auth.getLink('myposts');
                        }
                        if (this.props.action === 'renew') {
                            adbuild.gotoPayment(this.state.escortId, this.state.account.id, false);
                            adbuild.saveCookieType(false);
                        }
                        return;
                    }
                } else {
                    this.setState({
                        errorMessage: errorText
                    });
                }
            }

        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: errorText
            });
        }
    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount(true)
          .then(account => {
              if (account) {
                  adbuild.saveAccount(account.id)
                  this.setState({
                      account: account,
                  });
              }
          })

        if (this.props.action === 'step3') {
            let data = adbuild.loadCookie();
            this.setState({
                type: await escorts.getTypeById(data.type.typeId),
                summary: data.locations
            });
        } else {
            await this.getEscort();
            await this.getType();
            await this.getPrice();
            await this.phoneCheck(this.state.escort.phone);
            setTimeout(this.getVisiting, 1000)
            this.getSummary();

            // save type for summary
            if (this.props.action === 'renew') {
                let type = {
                    typeId: this.state.type.id,
                    typeName: this.state.type.name,
                    typeSlug: this.state.type.slug
                };
                adbuild.saveCookieType(type);
            }

            // check auth and ownership
            if (!this.state.account.id) {
                auth.gotoSignin();
                return;
            }
            if (this.state.account.id !== this.state.escort.accountId) {
                window.location.href = auth.getLink('myposts');
                return;
            }
        }

        this.setState({
            listLanguage: await adbuild.getListLanguage(),
            listEthnicity: await adbuild.getListEthnicity(),
            listEyeColor: await adbuild.getListEyeColor(),
            listKitty: await adbuild.getListKitty(),
            listPenisSize: await adbuild.getListPenisSize(),
            listCupSize: await adbuild.getListCupSize(),
            listHairColor: await adbuild.getListHairColor(),
            listBuild: await adbuild.getListBuild(),
            listHeightFeet: adbuild.getListHeightFeet(),
            listHeightInches: adbuild.getListHeightInches(),
            listReply: adbuild.getListReply(),
            listAge: adbuild.getListAge(),
        });

        this.setState({loading: false});
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.type !== this.state.type) {
            this.forceUpdate();
        }
    }

    render() {

        if (this.state.errorMessage) {
            window.scrollTo({top: 0, behavior: 'smooth'});
        }

        return (
            <>
            {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

            <div className={`content ad-creation ad-${this.props.action}`} id={`escort-${this.props.action}-page`}>
                <form method="post" action="#" onSubmit={this.submit}>

                    {this.props.action === 'step3' &&
                        <div className="bg-white m-b-10 p-y-20 p-b-10">
                            <Steps
                              steps={['Category','Your Location','About Ad','Checkout']}
                              active={3}
                            />
                        </div>
                    }

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <p>
                                    <span className="color-error">*</span> Fields are required. All optional info will help you be found faster. We allow your client
                                    to search by hair color, services, rates, etc. If you want to show up more often in client
                                    results, fill in the optional fields.
                                </p>
                            </div>
                        </div>
                    </div>

                    {this.state.type.id !== 14 &&
                    <>
                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        <span className="color-error">*</span> Available to
                                    </h2>
                                    <CheckboxSecondary
                                        error={this.state.errorAvailable}
                                        id={'available-to'}
                                        data={[
                                            {
                                                'id': 'available-to-men',
                                                'name': 'isAvailableForMen',
                                                'title': 'Men',
                                                'value': 1,
                                                'checked': this.state.escort.isAvailableForMen,
                                            },
                                            {
                                                'id': 'available-to-women',
                                                'name': 'isAvailableForWomen',
                                                'title': 'Women',
                                                'value': 1,
                                                'checked': this.state.escort.isAvailableForWomen,
                                            },
                                            {
                                                'id': 'available-to-couples',
                                                'name': 'isAvailableForCouple',
                                                'title': 'Couples',
                                                'value': 1,
                                                'checked': this.state.escort.isAvailableForCouple,
                                            },
                                        ]} />
                                </div>
                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        <span className="color-error">*</span> Incall / Outcall
                                    </h2>
                                    <p className="p-b-15">
                                        You must indicate incall, outcall or both. Rates are optionally encouraged; our web users
                                        can search by rates.
                                    </p>
                                    <CheckboxSecondary
                                        error={this.state.errorCall}
                                        id={'callRates'}
                                        data={[
                                            {
                                                'id': 'incall',
                                                'name': 'isIncallEnabled',
                                                'title': 'Incall',
                                                'value': 1,
                                                'checked': this.state.escort.isIncallEnabled,
                                                //'handleClick': this.toggleRate, // always show input rates
                                            },
                                            {
                                                'id': 'outcall',
                                                'name': 'isOutcallEnabled',
                                                'title': 'Outcall',
                                                'value': 2,
                                                'checked': this.state.escort.isOutcallEnabled,
                                                //'handleClick': this.toggleRate, // always show input rates
                                            },
                                        ]} />
                                </div>
                            </div>
                        </div>

                        <div id={'incall_rate'} className="incall_rate wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Incall Rates, $
                                    </h2>
                                    <div className="d-flex">
                                        <Input
                                          classes={'p-b-0'}
                                          label={'Hour:'}
                                          type={'number'}
                                          id={'incall_rate_h'}
                                          name={'incallRateOneHour'}
                                          value={this.state.escort.incallRateOneHour}
                                          placeholder={'Price'}
                                          attributes={{'min': 0, 'inputMode': 'numeric', 'pattern': '\d*'}}
                                        />
                                        <Input
                                          classes={'p-b-0 m-x-10'}
                                          label={'2 Hours:'}
                                          type={'number'}
                                          id={'incall_rate_2h'}
                                          name={'incallRateTwoHours'}
                                          value={this.state.escort.incallRateTwoHours}
                                          placeholder={'Price'}
                                          attributes={{'min': 0, 'inputMode': 'numeric', 'pattern': '\d*'}}
                                        />
                                        <Input
                                          classes={'p-b-0'}
                                          label={'Overnight:'}
                                          type={'number'}
                                          id={'incall_rate_over'}
                                          name={'incallRateOvernight'}
                                          value={this.state.escort.incallRateOvernight}
                                          placeholder={'Price'}
                                          attributes={{'min': 0, 'inputMode': 'numeric', 'pattern': '\d*'}}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id={'outcall_rate'} className="outcall_rate wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Outcall Rates, $
                                    </h2>
                                    <div className="d-flex">
                                        <Input
                                          classes={'p-b-0'}
                                          label={'Hour:'}
                                          type={'number'}
                                          id={'outcall_rate_h'}
                                          name={'outcallRateOneHour'}
                                          value={this.state.escort.outcallRateOneHour}
                                          placeholder={'Price'}
                                          attributes={{'min': 0, 'inputMode': 'numeric', 'pattern': '\d*'}}
                                        />
                                        <Input
                                          classes={'p-b-0 m-x-10'}
                                          label={'2 Hours:'}
                                          type={'number'}
                                          id={'outcall_rate_2h'}
                                          name={'outcallRateTwoHours'}
                                          value={this.state.escort.outcallRateTwoHours}
                                          placeholder={'Price'}
                                          attributes={{'min': 0, 'inputMode': 'numeric', 'pattern': '\d*'}}
                                        />
                                        <Input
                                          classes={'p-b-0'}
                                          label={'Overnight:'}
                                          type={'number'}
                                          id={'outcall_rate_over'}
                                          name={'outcallRateOvernight'}
                                          value={this.state.escort.outcallRateOvernight}
                                          placeholder={'Price'}
                                          attributes={{'min': 0, 'inputMode': 'numeric', 'pattern': '\d*'}}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Visiting?
                                    </h2>
                                    <p className="p-b-15">
                                        Dates are optional. If you have specific dates per location, include them in your ad text.
                                    </p>
                                    <button
                                        id={'visiting_button'}
                                        type="button"
                                        className="primary-submit-btn"
                                        data-toggle="modal"
                                        data-target="#visiting_modal"
                                        data-ajax="false"
                                    >Select dates</button>
                                </div>
                            </div>
                        </div>

                        <Modal
                          id={'visiting_modal'}
                          title={'Visiting dates'}
                          classes={'visiting'}
                          content={<Visiting/>}
                        />

                    </>
                    }

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    City
                                </h2>
                                <Input
                                    classes={'p-b-0'}
                                    label={'e.g. Downtown'}
                                    type={'text'}
                                    id={'location'}
                                    name={'placeDescription'}
                                    value={this.state.escort.placeDescription}
                                    placeholder={'Enter location'}
                                    error={this.state.errors.location}
                                    maxCharacters={70}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    Girlfriend Experience
                                </h2>
                                <CheckboxSecondary
                                    data={[
                                        {
                                            'id': 'gfe',
                                            'name': 'gfe',
                                            'title': 'Yes',
                                            'value': 1,
                                            'checked': this.state.escort.gfe,
                                        },
                                        {
                                            'id': 'gfe',
                                            'name': 'gfeLimited',
                                            'title': 'No (Limited)',
                                            'value': 1,
                                            'checked': this.state.escort.gfeLimited,
                                        },
                                    ]}
                                />
                            </div>
                        </div>
                    </div>

                    {this.state.type.id === 6 &&
                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    Do you provide tantra massage?
                                </h2>
                                <CheckboxSecondary
                                    type={'radio'}
                                    data={[
                                        {
                                            'id': 'tantra',
                                            'name': 'tantra',
                                            'title': 'Yes',
                                            'value': '1',
                                            'checked': (this.state.escort)? this.state.escort.tantra : false,
                                        },
                                        {
                                            'id': 'tantra',
                                            'name': 'tantra',
                                            'title': 'No',
                                            'value': '0',
                                            'checked': (this.state.escort)? !this.state.escort.tantra : false,
                                        },
                                    ]}
                                />
                            </div>
                        </div>
                    </div>
                    }

                    {this.state.type.id !== 14 &&
                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    Fetish Session Activities
                                </h2>
                                <p className="p-b-15">
                                    Optional, select all that apply
                                </p>
                                <CheckboxSecondary
                                    classes={'m-b-15'}
                                    data={[
                                        {
                                            'id': 'fetish_dominant',
                                            'name': 'fetishDominant',
                                            'title': 'Dominant',
                                            'value': 1,
                                            'checked': this.state.escort.fetishDominant,
                                        },
                                        {
                                            'id': 'fetish_submissive',
                                            'name': 'fetishSubmissive',
                                            'title': 'Submissive',
                                            'value': 1,
                                            'checked': this.state.escort.fetishSubmissive,
                                        },
                                        {
                                            'id': 'fetish_swith',
                                            'name': 'fetishSwitch',
                                            'title': 'Switch',
                                            'value': 1,
                                            'checked': this.state.escort.fetishSwitch,
                                        },
                                    ]}
                                />
                            </div>
                        </div>
                    </div>
                    }

                    {[5, 14].indexOf(parseInt(this.state.type.id)) === -1 &&
                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    <span className="color-error">*</span> Age
                                </h2>
                                <Select
                                    classes={'m-b-0'}
                                    disabledOption={"Select your age"}
                                    id={'age'}
                                    name={'age'}
                                    data={this.state.listAge}
                                    value={this.state.escort.age}
                                    //required={true}
                                    error_message={this.state.errors.age}
                                />
                            </div>
                        </div>
                    </div>
                    }

                    {[5, 12, 14].indexOf(parseInt(this.state.type.id)) === -1 &&
                    <>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Ethnicity
                                    </h2>
                                    <Select
                                        classes={'m-b-0'}
                                        disabledOption={'Choose ethnicity'}
                                        id={'ethnicity'}
                                        name={'ethnicity'}
                                        data={this.state.listEthnicity}
                                        value={this.state.escort.ethnicityRawValue}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Language Spoken
                                    </h2>
                                    <SelectTags
                                        classes={'m-b-0'}
                                        disabledOption={'Choose language'}
                                        id={'language'}
                                        name={'languageIds[]'}
                                        data={this.state.listLanguage}
                                        value={this.state.escort.languageIds}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container d-flex">

                                <div className="ad-creation-container width-50 m-r-5">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Height
                                    </h2>
                                    <Select
                                      classes={'m-b-0'}
                                      disabledOption={'Choose'}
                                      id={'height_inches'}
                                      name={'heightInches'}
                                      data={this.state.listHeightInches}
                                      value={this.state.escort.heightInches}
                                    />
                                </div>

                                <div className="ad-creation-container width-50 m-l-5">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Weight (LBS)
                                    </h2>
                                    <Input
                                      type={'number'}
                                      classes={'p-b-0'}
                                      id={'weight'}
                                      name={'weight'}
                                      placeholder={'Enter weight'}
                                      value={this.state.escort.weight}
                                    />
                                </div>

                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container d-flex">

                                <div className="ad-creation-container width-50 m-r-5">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Eye Color
                                    </h2>
                                    <Select
                                        classes={'m-b-0'}
                                        disabledOption={'Choose'}
                                        id={'eyecolor'}
                                        name={'eyeColor'}
                                        data={this.state.listEyeColor}
                                        value={this.state.escort.rawEyeColor}
                                    />
                                </div>

                                <div className="ad-creation-container width-50 m-l-5">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Hair Color
                                    </h2>
                                    <Select
                                      classes={'m-b-0'}
                                      disabledOption={'Choose'}
                                      id={'haircolor'}
                                      name={'hairColor'}
                                      data={this.state.listHairColor}
                                      value={this.state.escort.rawHairColor}
                                    />
                                </div>

                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Build
                                    </h2>
                                    <Select
                                        classes={'m-b-0'}
                                        disabledOption={'Choose build'}
                                        id={'build'}
                                        name={'bodyType'}
                                        data={this.state.listBuild}
                                        value={this.state.escort.bodyTypeRawValue}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Measurements (Inches)
                                    </h2>
                                    <div className="d-flex">
                                        <Input
                                            label={'Bust'}
                                            classes={'p-b-0'}
                                            id={'measure_1'}
                                            name={'bust'}
                                            placeholder={'Bust Size'}
                                            type={'number'}
                                            value={this.state.escort.bust}
                                            field_classes={'p-l-5 p-r-5'}
                                            attributes={{'min': 0,'inputMode': 'numeric','pattern': '\d*'}}
                                        />
                                        <Input
                                            label={'Waist'}
                                            classes={'p-b-0 m-l-5'}
                                            id={'measure_2'}
                                            name={'waist'}
                                            placeholder={'Waist Size'}
                                            type={'number'}
                                            value={this.state.escort.waist}
                                            field_classes={'p-l-5 p-r-5'}
                                            attributes={{'min': 0,'inputMode': 'numeric','pattern': '\d*'}}
                                        />
                                        <Input
                                            label={'Hips'}
                                            classes={'p-b-0 m-l-5'}
                                            id={'measure_3'}
                                            name={'hip'}
                                            placeholder={'Hips Size'}
                                            type={'number'}
                                            value={this.state.escort.hip}
                                            field_classes={'p-l-5 p-r-5'}
                                            attributes={{'min': 0,'inputMode': 'numeric','pattern': '\d*'}}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Cup Size
                                    </h2>
                                    <Select
                                        classes={'m-b-0'}
                                        disabledOption={'Choose cup size'}
                                        id={'cupsize'}
                                        name={'cupSize'}
                                        data={this.state.listCupSize}
                                        value={this.state.escort.cupSizeRawValue}
                                    />
                                </div>
                            </div>
                        </div>

                        {[1, 6].indexOf(parseInt(this.state.type.id)) === 0 && // only in these categories
                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Kitty
                                    </h2>
                                    <Select
                                        classes={'m-b-0'}
                                        disabledOption={'Choose your kitty'}
                                        id={'kitty'}
                                        name={'kitty'}
                                        data={this.state.listKitty}
                                        value={this.state.escort.kittyRawValue}
                                    />
                                </div>
                            </div>
                        </div>
                        }

                        {[2, 3, 4, 7].indexOf(parseInt(this.state.type.id)) === 0 && // only in these categories
                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Penis size (inches)
                                    </h2>
                                    <Select
                                        classes={'m-b-0'}
                                        disabledOption={'Choose penis size'}
                                        id={'penis_size'}
                                        name={'penisSize'}
                                        data={this.state.listPenisSize}
                                        value={this.state.escort.penisSize}
                                    />
                                </div>
                            </div>
                        </div>
                        }

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Are you a pornstar?
                                    </h2>
                                    <CheckboxSecondary
                                        type={'radio'}
                                        data={[
                                            {
                                                'id': 'pornstar',
                                                'name': 'isPornstar',
                                                'title': 'Yes',
                                                'value': '1',
                                                'checked': (this.state.escort)? this.state.escort.isPornstar : false,
                                            },
                                            {
                                                'id': 'pornstar',
                                                'name': 'isPornstar',
                                                'title': 'No',
                                                'value': '0',
                                                'checked': (this.state.escort)? !this.state.escort.isPornstar : false,
                                            },
                                        ]}
                                    />
                                </div>
                            </div>
                        </div>

                        {[1, 6].indexOf(parseInt(this.state.type.id)) === 0 && // only in these categories
                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">
                                    <h2 className="weight-700 color-primary p-b-10">
                                        Current Pregnant?
                                    </h2>
                                    <CheckboxSecondary
                                        type={'radio'}
                                        data={[
                                            {
                                                'id': 'pregnant',
                                                'name': 'isPregnant',
                                                'title': 'Yes',
                                                'value': '1',
                                                'checked': (this.state.escort) ? this.state.escort.isPregnant : false,
                                            },
                                            {
                                                'id': 'pregnant',
                                                'name': 'isPregnant',
                                                'title': 'No',
                                                'value': '0',
                                                'checked': (this.state.escort) ? !this.state.escort.isPregnant : false,
                                            },
                                        ]}
                                    />
                                </div>
                            </div>
                        </div>
                        }

                    </>
                    }

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    <span className="color-error">*</span> Name on Ad
                                </h2>
                                <Input
                                    classes={'p-b-0'}
                                    label={'Working name'}
                                    type={'text'}
                                    id={'name'}
                                    name={'firstName'}
                                    value={this.state.escort.firstName}
                                    placeholder={'Enter name on ad'}
                                    //required={true}
                                    error={this.state.errors.name}
                                    maxCharacters={30}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    <span className="color-error">*</span> Introduction
                                </h2>
                                <Input
                                    classes={'p-b-0'}
                                    label={'Your "Tagline"'}
                                    type={'text'}
                                    id={'title'}
                                    name={'title'}
                                    value={this.state.escort.title}
                                    placeholder={'Enter tagline here'}
                                    //required={true}
                                    error={this.state.errors.title}
                                    maxCharacters={70}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    <span className="color-error">*</span> Ad Text
                                </h2>
                                <p className="p-b-15">
                                    You may not post ads stating an exchange of sexual favors for money or use code words such
                                    as 'greek', 'bbbj', 'blow', 'trips to greece', etc. You may not post content which
                                    advertises an illegal service.
                                </p>
                                <Textarea
                                    classes={'p-b-0 ad-text'}
                                    textareaClasses={'tinymce'}
                                    label={'Type something about yourself'}
                                    id={'content'}
                                    name={'content'}
                                    value={this.state.escort.content}
                                    //required={true}
                                    error={this.state.errors.content}
                                />
                            </div>
                        </div>
                    </div>

                    {/*EMAILS OPTIONS*/}
                    {config.useEscortEmail() &&
                        <>
                            <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                                <div className="container">
                                    <div className="ad-creation-container">
                                        <h2 className="weight-700 color-primary p-b-10">
                                            <span className="color-error">*</span> Email address on your Ad
                                        </h2>
                                        <Input
                                            classes={'p-b-0'}
                                            label={'Your Email'}
                                            type={'email'}
                                            id={'email'}
                                            name={'email'}
                                            value={this.state.escort.email}
                                            placeholder={'Enter email here'}
                                            //required={true}
                                            error={this.state.errorEmail}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                                <div className="container">
                                    <div className="ad-creation-container">
                                        <h2 className="weight-700 color-primary p-b-10">
                                            <span className="color-error">*</span> Receive email?
                                        </h2>
                                        <p className="p-b-15">
                                            We can make your email anonymous and forward you viewer responses or post your email
                                            address. If you do not want to receive email, be sure to include a phone number in your ad.
                                        </p>
                                        <Select
                                            classes={'m-b-0'}
                                            disabledOption={'Choose an option'}
                                            id={'emailVisibility'}
                                            name={'emailVisibility'}
                                            data={this.state.listReply}
                                            required={true}
                                            value={this.state.escort.emailVisibility || 1}
                                        />
                                    </div>
                                </div>
                            </div>
                        </>
                    }

                    {/*PHONE*/}
                    <FormPhone
                        required={true}
                        classes={'m-b-10'}
                        value={this.state.escort.phone}
                        header={'Phone Number'}
                        label={'Customer contact number.'}
                        phoneValid={this.state.phoneValid}
                        phoneResult={this.state.phoneResult}
                        phoneError={this.state.phoneError}
                        handleBlur={this.phoneVerify}
                    />

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    Your Website
                                </h2>
                                <p className="p-b-15">
                                    e.g. MyWebsite.com. You do not need to include http:// or www
                                </p>
                                <Input
                                    classes={'p-b-0'}
                                    type={'text'}
                                    id={'website'}
                                    name={'website'}
                                    value={this.state.escort.website}
                                    placeholder={'Enter website here'}
                                    required={false}
                                    error={false}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">Credit Cards you accept</h2>
                                <CheckboxSecondary
                                    data={[
                                        {
                                            'id': 'payment_visa',
                                            'name': 'isVisaAccepted',
                                            'title': 'Visa/MC',
                                            'value': 1,
                                            'checked': this.state.escort.isVisaAccepted,
                                        },
                                        {
                                            'id': 'payment_amex',
                                            'name': 'isAmexAccepted',
                                            'title': 'AMEX',
                                            'value': 1,
                                            'checked': this.state.escort.isAmexAccepted,
                                        },
                                        {
                                            'id': 'payment_dis',
                                            'name': 'isDiscoverAccepted',
                                            'title': 'Discover',
                                            'value': 1,
                                            'checked': this.state.escort.isDiscoverAccepted,
                                        },
                                    ]}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">
                                <h2 className="weight-700 color-primary p-b-10">
                                    TER ID
                                </h2>
                                <p className="p-b-15">
                                    If you have been reviewed on TheEroticReview.com, include your ID and we'll link you.
                                </p>
                                <Input
                                    classes={'p-b-0'}
                                    type={'text'}
                                    id={'ter'}
                                    name={'terId'}
                                    value={this.state.escort.terId}
                                    placeholder={'Enter TER ID here'}
                                    required={false}
                                    error={false}
                                />
                            </div>
                        </div>
                    </div>

                    {/*SOCIALS*/}
                    <FormSocial values={this.state.escort}/>

                    {/*PHOTOS*/}
                    <FormPhoto
                        type={'escort'}
                        pictures={this.state.pictures}
                        picturesMax={this.state.picturesMax}
                        picturesLimit={this.state.picturesLimit}
                        picturesError={this.state.picturesError}
                        picturesLoading={this.state.picturesLoading}
                        uploadHandle={this.uploadPhoto}
                        deleteHandle={this.deletePhoto}
                    />

                    {/*VIDEOS*/}
                    <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                        <div className="container">
                            <div className="ad-creation-container">

                                <h2 className="weight-700 color-primary p-b-10">
                                    Video
                                </h2>

                                <p className="p-b-15 color-primary weight-600">
                                    Naked videos & genitalia are NOT ALLOWED. This includes topless video.
                                </p>

                                <p className="p-b-15">
                                    Max. video length is 20 sec or 10MB. This only uploads video files.
                                </p>

                                <div className="file_uploader __video">
                                    {this.state.videosError &&
                                        <div className="file_uploader__error m-b-10">{this.state.videosError}</div>
                                    }
                                    <div className="file_uploader__container">
                                        <div className="file_uploader__preview">
                                            {!this.state.videosLimit &&
                                                <label type="button" className="file_uploader__button file_uploader__picture">
                                                    {this.state.videosProgress ?
                                                      (
                                                        <div className="loader d-flex">
                                                            <span className="loader-progress">{this.state.videosProgress}%</span>
                                                            <svg className="loader-icon">
                                                                <use xlinkHref="/images/icons.svg#icon_loader"/>
                                                            </svg>
                                                        </div>
                                                      ) : (
                                                        <>
                                                            <svg className="icon_video_upload">
                                                                <use xlinkHref="/images/icons.svg#icon_video_upload"/>
                                                            </svg>
                                                            <input type="file" onChange={this.uploadVideo} name="video" id="video" accept="video/*"/>
                                                        </>
                                                      )
                                                    }
                                                </label>
                                            }

                                            {this.state.videos.map((item, index) =>
                                              <div key={index} className={`file_uploader__picture ${item.processing? 'processing' : ''}`}>
                                                    {!item.processing && <div onClick={this.deleteVideo} data-index={index} data-id={item.id} data-name={item.name} className="file_uploader__delete"/>}
                                                    <div style={{backgroundImage: item.processing ? '' : `url(${item.image})`}} className="file_uploader__img"/>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    {/*UPGRADES*/}
                    {(['renew','step3'].includes(this.props.action) && this.state.type.id > 0) &&
                        <Upgrades
                          handleChange={this.toggleUpgrade}
                          handleChangeSticky={this.toggleUpgradeRadio}
                          account={this.state.account}
                          summary={this.state.summary}
                          typeId={this.state.type.id}
                          action={this.props.action}
                        />
                    }


                    {/*SUMMARY*/}
                    <div className="wrapper-ad-creation-container bg-white p-y-15">
                        <div className="container">
                            <div className="ad-creation-container">
                                <div id="calculator" className="m-b-15">

                                {this.props.action === 'renew' && <Summary step={'renew'} locations={this.state.summary} readonly={true}/>}
                                {this.props.action === 'step3' && <Summary step={3} locations={this.state.summary} handleClick={this.removeSummaryCity}/>}

                                </div>
                                <p className="p-b-15">
                                    <ButtonSubmit name={'continue'} text={'Save & Continue'}/>
                                </p>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <LoadingSpinner loading={this.state.loading} />
            </>
        )
    }
}
