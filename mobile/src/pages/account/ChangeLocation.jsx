import React, {Component} from 'react';
import axios from '../../services/api_init';
import LoadingSpinner from "../../components/common/loading-spinner";
import auth from "../../lib/auth";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import {history} from "../../index";
import escorts from "../../services/escorts";

export default class ChangeLocation extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            account: {},
            escort: {
                locations: []
            },
            escortId: props.match.params.escortId,
            states: [],
            cities: [],
            fromCity: null,
            toCity: null,
            currentCity: null,
        };
    }

    getEscort = async () => {
        try {
            const res = await axios.get(`/api/v1/escorts/${this.state.escortId}`);
            this.setState({
                escort: res.data
            });
            if (res.data.locations.length === 1) {
                this.setState({
                    fromCity: res.data.locations[0].locationId,
                    currentCity: `${res.data.locations[0].cityName}, ${res.data.locations[0].stateName}`
                });
            }
        } catch (error) {
            config.catch(error);
        }
    }

    setFromCity = event => {
        this.setState({
            fromCity: event.target.value
        });
    }

    setToCity = event => {
        this.setState({
            toCity: event.target.value
        });
    }

    getStates = async () => {
        try {
            const res = await axios.get(`/api/v1/states?offset=0&limit=1000`);
            this.setState({
                states: res.data.list
            });
        } catch (error) {
            config.catch(error);
        }
    }

    getCities = async event => {
        const stateId = event.target.value;
        this.setState({ loading: true });
        try {
            const res = await axios.get(`/api/v1/popularCities?stateId=${stateId}`);
            this.setState({
                cities: Object.values(res.data.list),
                toCity: null,
            });
        } catch (error) {
            config.catch(error);
        } finally {
            this.setState({ loading: false });
        }
    }

    submit = async event => {
        event.preventDefault();

        this.setState({ loading: true });

        try {
            await axios.put(`/api/v1/pc/escorts/${this.state.escortId}/change_location`, {
                fromCityId: this.state.fromCity,
                toCityId: this.state.toCity
            });

            // update escort in local db
            await escorts.getById(this.state.escortId, true);

            history.push(auth.getLink('myposts'));
        } catch (error) {
            config.catch(error);
        } finally {
            this.setState({ loading: false });
        }
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await auth.getAccount()
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getEscort();
        await this.getStates();

        this.setState({ loading: false });
    }

    render() {
        return (
          <div className="content my-avs" id="change-location-page">
            <div className="wrapper-my-avs-container bg-white p-t-25 p-b-15 m-b-10">
                <div className="container">
                    <div className="my-avs-container">

                        <form id="change_location_form" action="#" method="POST" onSubmit={this.submit}>
                            { this.state.escort.locations.length > 1 ? (
                                <>
                                    <p className="p-b-15">
                                        Your ad is posted in more than one location. First pick the location that you want to
                                        move your ad from:
                                    </p>

                                    <div className="primary-select">
                                        <label className="required" htmlFor="from">Ad cities</label>
                                        <div className="select-container">
                                            <select name="from" id="from" onChange={this.setFromCity}>
                                                <option value="" defaultValue className="disabled-option">
                                                    Please choose
                                                </option>
                                                { this.state.escort.locations.map(location =>
                                                    <option key={location.id} value={location.locationId}>{location.cityName}</option>
                                                ) }
                                            </select>
                                        </div>
                                    </div>
                                </>
                            ) : ( this.state.escort.locations.length > 0 ? (
                                <p className="p-b-15">
                                    Your ad is currently located in <span className="weight-600">{this.state.currentCity}</span>
                                </p>
                            ): '' ) }

                            <p className="p-b-15">
                                Pick state and city that you want to move your ad to:
                            </p>

                            <div className="primary-select">
                                <label className="required" htmlFor="state">State</label>
                                <div className="select-container">
                                    <select name="state" id="state" onChange={this.getCities}>
                                        <option value="" defaultValue className="disabled-option">Please choose
                                        </option>
                                        { this.state.states.map(state =>
                                            <option key={state.id} value={state.id}>{state.name}</option>
                                        ) }
                                    </select>
                                </div>
                            </div>

                            <div className="primary-select">
                                <label className="required" htmlFor="city">City</label>
                                <div className="select-container">
                                    <select name="city" id="city" onChange={this.setToCity}>
                                        <option value="" defaultValue className="disabled-option">Please choose
                                        </option>
                                        { this.state.cities.map(city =>
                                            <option key={city.id} value={city.id}>{city.name}</option>
                                        ) }
                                    </select>
                                </div>
                            </div>

                            <button
                                className="primary-submit-btn"
                                id="move_btn"
                                type="submit"
                                disabled={this.state.fromCity === null || this.state.toCity === null || !window.navigator.onLine}>
                                <span>Move My Ad</span>
                            </button>
                        </form>
                    </div>
                </div>

            <LoadingSpinner loading={this.state.loading} />
            </div>
          </div>
        );
    }
}
