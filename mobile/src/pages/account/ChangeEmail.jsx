import React, {Component} from 'react';
import axios from '../../services/api_init';
import LoadingSpinner from "../../components/common/loading-spinner";
import Alert from "../../components/common/alert";
import auth from "../../lib/auth";
import ButtonSubmit from "../../components/form/button-submit";
import Input from "../../components/form/input";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import form from "../../lib/form";

export default class ChangeEmail extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            successMessage: false,
            errorMessage: false,
            loading: false,
            errors: {},
            account: {},

            currentEmail: false,
            confirmationId: null,
            confirmationCode: null,
        };
    }

    fillForm = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            account: Object.assign(this.state.account, res)
        });
    }

    fillConfirm = event => {
        const target = event.target;

        // convert to capitals
        target.value = target.value.toUpperCase();

        this.setState({
            confirmationCode: target.value
        });
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;
        let errors = {};

        if (this.state.account.email === this.state.currentEmail) {
            errors['email'] = 'Please use another email to change'
            ok = false;
        }

        let emailError = form.validateEmail(document.getElementById('email').value);
        if (emailError) {
            errors['email'] = emailError;
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    submit = async event => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false,
            successMessage: false,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        this.setState({
            loading: true
        });

        try {
            const res = await axios.put('/api/v1/pc/account/change_email', {
                'email': this.state.account.email
            });

            if (res.data && res.data.confirmationId) {
                this.setState({
                    loading: false,
                    errorMessage: false,
                    successMessage: 'Email updated successfully. Please confirm new email.',
                    confirmationId: res.data.confirmationId,
                });
                document.getElementById('confirmation_code').value = '';
            } else {
                this.setState({
                    loading: false,
                    errorMessage: 'Error updating email. Please try again later.',
                });
            }
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    submitCode = async event => {
        event.preventDefault();

        this.setState({
            errorMessage: false,
            successMessage: false,
            loading: true,
        });

        try {
            await axios.post('/api/v1/pc/account/set_email_confirmed', {
                confirmationId: this.state.confirmationId,
                confirmationCode: this.state.confirmationCode
            });

            this.setState({
                loading: false,
            });

            auth.gotoAccount();
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await auth.getAccount()
          .then(account => {
              this.setState({
                  account: account,
                  currentEmail: account.email,
              });
          })

        this.setState({ loading: false });
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    render() {
        return (
        <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}
          {this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

            <div className="change-email p-y-15 bg-white" id="change-email-page">

                { this.state.confirmationId ? (
                    <>
                    <div className="container">
                        <div className="bg-white">
                            <h2 className="weight-700 p-y-10 color-primary">Please check your new email inbox and enter confirmation code</h2>
                        </div>

                        <form action="#" id="confirmForm" method="POST" onSubmit={this.submitCode}>
                            <Input
                              classes={'m-y-15 p-b-5'}
                              label={'Confirmation code'}
                              handleChange={this.fillConfirm}
                              required={true}
                              name={'confirmation_code'}
                              id={'confirmation_code'}
                              placeholder={'Enter confirmation code from email'}
                              maxLength={6}
                              minLength={6}
                              error={this.state.errors.confirmation_code}
                            />

                            <ButtonSubmit text={'Confirm'} loading={this.state.loading}/>
                        </form>
                    </div>
                    </>
                ) : (
                    <>
                    <div className="container">
                        <p>
                            Your current email is <span className="color-primary weight-600">{ this.state.currentEmail }</span>
                        </p>
                        <form action="#" method="POST" id="emailForm" onSubmit={this.submit}>
                            <Input
                              classes={'m-y-15 p-b-5'}
                              label={'New Email'}
                              type={'email'}
                              handleChange={this.fillForm}
                              required={true}
                              name={'email'}
                              id={'email'}
                              minLength={6}
                              maxLength={100}
                              placeholder={'Enter email here'}
                              error={this.state.errors.email}
                            />

                            <ButtonSubmit text={'Save'} loading={this.state.loading}/>
                        </form>
                    </div>

                    </>
                ) }
            </div>

            <LoadingSpinner loading={this.state.loading} />
        </>
        )
    }
}
