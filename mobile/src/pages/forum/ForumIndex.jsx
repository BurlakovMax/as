import React, {Component} from 'react';
import {Link} from "react-router-dom";
import cities from "../../services/cities.js";
import axios from '../../services/api_init';
import {changeTitle} from "../../store/common";
import InputSearch from "../../components/form/input-search";
import Pagination from "../../components/common/pagination";
import LoadingSpinner from "../../components/common/loading-spinner";
import config from "../../lib/config";

export default class ForumIndex extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,

			city: {},

			forumSlug: this.props.match.params.forumSlug,
			forums: [],
			forum: {},

			topicsList: [],
			topicsTotal: 0,

			searchText: '',
			searchClear: '',

			// pagination params
			offset: 0,
			limit: 10,
		};
	}

	searchTopics = async (event) => {
		event.preventDefault();

		let searchText = document.getElementById('search').value;

		// if empty input - reset search
		if (searchText.length === 0) {
			await this.searchClear();
			return null;
		}

		// minimum 3 chars to start searching
		if (searchText.length < 3) {
			this.setState({
				searchText: false,
				searchClear: false,
			});
			return false;
		}

		this.setState({
			searchText: searchText,
			searchClear: true,
		});

		await this.getTopics(0, 10, searchText);
	}

	searchClear = async (event) => {
		if (event) {
			event.preventDefault();
		}

		document.getElementById('search').value = '';

		this.setState({
			searchText: false,
			searchClear: false,
		});

		await this.getTopics();
	}

	searchHandle = (event) => {
		let searchText = event.currentTarget.value;

		if (searchText.length) {
			this.setState({
				searchClear: true
			});
		} else {
			this.setState({
				searchClear: false
			});
		}
	}

	getForums = async () => {
		try {
			let params = {
				locationId: this.state.city.id,
				offset: 0,
				limit: 500,
			};
			const res = await axios.get('/api/v1/getForumsByLocationId', {params});

			this.setState({
				forums: res.data.list
			});
		} catch (error) {
			config.catch(error);
		}
	}

	getTopicNewUrl = () => {
		return `${this.state.city.url}sex-forum/${this.state.forum.url}/new-topic/`;
	}

	getTopicUrl = (topic) => {
		return `${this.state.city.url}sex-forum/${this.state.forum.url}/topic/${topic.id}`;
	}

	getTopics = async (offset = 0, limit = 10, searchText = false) => {
		this.setState({loading: true});

		let list = [];
		let total = 0;

		try {
			let params = {
				locationId: this.state.city.id,
				forumId: this.state.forum.id,
				offset: offset,
				limit: limit,
			};

			if (searchText) {
				params['query'] = searchText
			}

			const res = await axios.get('/api/v1/forumTopics', {params});
			total = res.data.total;
			list = res.data.list;

		} catch (error) {
			config.catch(error);
		}

		this.setState({
			topicsList: list,
			topicsTotal: total,
		});

		this.setState({loading: false});
		window.scrollTo({top: 0, behavior: 'smooth'});
	}

	getData = async (offset = 0, limit = 10) => {
		await this.getTopics(offset, limit);
	}

	async componentDidMount() {
		this.setState({loading: true});

		const {stateName, cityName, forumSlug} = this.props.match.params;

		let city = await cities.getByUrl(stateName, cityName);
		config.setCity(city);

		this.setState({
			city: city,
			forumSlug: forumSlug
		});

		// get forums list
		await this.getForums();

		// get current forum
		const forum = this.state.forums.find(forum => forum.url === forumSlug) || {};
		changeTitle(forum.name);
		this.setState({
			forum: forum
		});

		// get current forum topics
		await this.getTopics();

		this.setState({loading: false});
	}

	render() {

		return (
			<div className="content forum">

				{/*SEARCH INPUT*/}
				<div className="bg-primary m-b-10 p-y-25 wrapper-forum-container">
					<div className="container">
						<div className="forum-container">

							<div className="search-forum">
								<h3 className="color-white weight-600 text-center p-b-20">Search in this forum</h3>

								<form onSubmit={this.searchTopics} action="forum/forum_search" method="get">

									<InputSearch
										classes={'p-b-0'}
										type={'search'}
										id={'search'}
										name={'search'}
										placeholder={'Search Forum'}
										required_icon={false}
										icon_right={true}
										icon_clear={this.state.searchClear}
										handleChange={this.searchHandle}
										handleClear={this.searchClear}
									/>
								</form>

							</div>

						</div>
					</div>
				</div>

{/*				{% if states %}
				<div className="bg-white m-b-5 p-y-15 wrapper-forum-container">
					<div className="container">
						<div className="forum-container">
							<h2 className="color-primary weight-700 m-b-15">Select a Sub Category</h2>

							<ul className="p-l-15">
								{% for forum in states %}
								{% for forum2 in forum %}
								<li className="m-l-0 m-b-10" style="list-style-type: none;">
									<a className="color-tertiary" href="/forum/viewforum?forum_id={{forum2.forum_id}}" title="" data-ajax="false">{{forum2.forum_name}} ({{forum2.forum_posts}}) {{forum2.paging}}
										{% if forum2.new %} <span className="color-error f-s-10 weight-600">*new post*</span>{% endif %}
									</a>
								</li>
								{% endfor %}
								{% endfor %}
							</ul>

						</div>
					</div>
				</div>
				{% else %}*/}

				<div className="bg-white text-center p-y-10 m-t-10">
					<div className="container">

						<Link className="third-filled" to={this.getTopicNewUrl()}>
							Start A New Topic
						</Link>

					</div>
				</div>

				<div className="bg-white m-b-10 p-y-15 wrapper-forum-container">
					<div className="container">
						<div className="forum-container">
							<h2 className="color-primary weight-700 d-flex justify-content-between">Forum Topics <span className="weight-400">{this.state.topicsTotal > 0 && this.state.topicsTotal}</span></h2>

							{this.state.topicsTotal > 0 &&

								<div className="forum-topics">

									{this.state.topicsList.map((topic, index) =>
										{
											return (
												<div key={index} className="topic d-flex align-items-center p-y-15">
													<Link to={this.getTopicUrl(topic)} className="information p-x-10 flex-grow-1">
														<div className="top p-b-5">
															<span className="message weight-600 f-s-16 color-tertiary">
																	{topic.title}
															</span>
														</div>

														<div className="bottom d-flex align-items-center justify-content-between">
															<div className="user-name d-flex align-items-center color-primary">
																<svg className="icon_contacts">
																	<use xlinkHref="/images/icons.svg#icon_contacts"></use>
																</svg>
																<span className="name p-x-5 lh-normal break-all">{topic.accountName}</span>
															</div>
															<div className="replies d-flex align-items-center color-tertiary">
																<svg className="icon_messages">
																	<use xlinkHref="/images/icons.svg#icon_messages"></use>
																</svg>
																<span className="amount p-l-10 lh-normal">{topic.countReplies}</span>
															</div>
														</div>
													</Link>
												</div>
											)
										}
									)}

									{this.state.topicsTotal > 0 && <Pagination total={this.state.topicsTotal} getData={this.getData}/>}

								</div>

							}

							{!this.state.topicsTotal &&
								<div className="d-flex w-100 align-items-center flex-column p-y-20 forum-no-result">
									<div className="forum-no-result__icon">
										<svg className="icon_no_results">
											<use xlinkHref="/images/icons.svg#icon_no_results"></use>
										</svg>
									</div>

									<div className="color-quaternary text-center p-t-20 f-s-base">
										<p className="f-s-16">
											Oh no! Unfortunately, no results where found
											<br/>
											Try searching for different topics or use other keywords
										</p>
									</div>
								</div>
							}

						</div>
					</div>
				</div>

				<LoadingSpinner loading={this.state.loading} />
			</div>
		);
	}
}
