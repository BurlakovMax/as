import React, {Component} from 'react';
import cities from "../../services/cities.js";
import axios from '../../services/api_init';
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import LoadingSpinner from "../../components/common/loading-spinner";
import ButtonSubmit from "../../components/form/button-submit";
import Checkbox from "../../components/form/checkbox";
import Textarea from "../../components/form/textarea";
import Input from "../../components/form/input";
import auth from "../../lib/auth";
import adbuild from "../../lib/adbuild";
import Alert from "../../components/common/alert";
import ButtonBordered from "../../components/form/button-bordered";
import forums from "../../services/forums";

export default class ForumTopicNew extends Component {
	constructor(props) {
		super(props);

		changeTitle(config.getTitle(this));

		this.state = {
			loading: false,
			errorMessage: false,
			successMessage: false,
			account: {},

			city: {},

			topic: {},

			forumSlug: this.props.match.params.forumSlug,
			forums: [],
			forum: {},
		};
	}

	getForums = async () => {
		try {
			let params = {
				locationId: this.state.city.id,
				offset: 0,
				limit: 500,
			};
			const res = await axios.get('/api/v1/getForumsByLocationId', {params});

			this.setState({
				forums: res.data.list
			});
		} catch (error) {
			config.catch(error);
		}
	}

	fillForm = event => {
		const target = event.target;
		const name = target.name;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		let res = {[name]: value};

		this.setState({
			topic: Object.assign(this.state.topic, res)
		});
	}

	subscribe = async (topicId) => {
		if (this.state.topic.watch) {
			try {
				await axios.post('/api/v1/pc/subscribers', {
					topicId: topicId
				});
			} catch (error) {
				config.catch(error);
			}
		}
	}

	submit = async event => {
		event.preventDefault();

		this.setState({
			loading: true,
			errorMessage: false,
			successMessage: false,
		});

		let params = {
			'forumId': this.state.forum.id,
			'locationId': this.state.city.id,
			'title': this.state.topic.title,
			'text': this.state.topic.text,
		}

		try {
			let res = await axios.post('/api/v1/pc/forumTopics', params);

			if (res.data.id) {
				document.getElementById('title').value = '';
				document.getElementById('text').value = '';

				this.setState({
					loading: false,
					successMessage: 'Topic successfully created',
					topic: Object.assign(this.state.topic, {'id': res.data.id})
				});

				await this.subscribe(res.data.id);

				// update forum topic & counts
				await forums.getForums(this.state.city.id, 1, true);
			} else {
				this.setState({
					loading: false,
					errorMessage: 'Error creating new topic. Please try again later.'
				});
			}

		} catch (error) {
			config.catch(error);

			this.setState({
				loading: false,
				errorMessage: error.response.data.message,
			});
		}

	}

	getTopicUrl = () => {
		return `${this.state.city.url}sex-forum/${this.state.forum.url}/topic/${this.state.topic.id}`;
	}

	getForumUrl = () => {
		return `${this.state.city.url}sex-forum/${this.state.forum.url}`;
	}

	closeAlert = () => {
		this.setState({
			errorMessage: null,
			successMessage: null,
		});
	}

	async componentDidMount() {
		this.setState({loading: true});

		await auth.getAccount()
			.then(account => {
				if (account) {
					adbuild.saveAccount(account.id)
					this.setState({
						account: account,
					});
				}
			})

		const {stateName, cityName, forumSlug} = this.props.match.params;

		let city = await cities.getByUrl(stateName, cityName);
		config.setCity(city);

		this.setState({
			city: city,
			forumSlug: forumSlug
		});

		// get forums list
		await this.getForums();

		// get current forum
		const forum = this.state.forums.find(forum => forum.url === forumSlug) || {};
		this.setState({
			forum: forum
		});

		this.setState({loading: false});
	}

	render() {

		return (
			<>
			<div className="content">

					<form name="posting" onSubmit={this.submit} method="post" data-ajax="false">

					<input type="hidden" name="forum_id" value="1"/>

					<div className="bg-seventh-gray">
						<div className="container">
							<h1 className="color-primary weight-700 p-y-10 f-s-18">
								{this.state.forum.name}
							</h1>
						</div>
					</div>

					{this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}
					{this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

					<div className="bg-white p-t-10 p-b-15">
						<div className="container">

							<Input
								classes={'p-b-15'}
								label={'Title'}
								type={'text'}
								id={'title'}
								name={'title'}
								value={''}
								placeholder={'Enter the title of your topic here'}
								required={true}
								maxCharacters={100}
								handleChange={this.fillForm}
							/>

							<Textarea
								classes={'p-b-20'}
								label={'Comments'}
								type={'text'}
								id={'text'}
								name={'text'}
								value={''}
								placeholder={'Enter your comment here'}
								required={true}
								handleChange={this.fillForm}
							/>

							<Checkbox
								classes={'m-b-15'}
								label={'Follow with Email Notifications'}
								name={'watch'}
								id={'watch'}
								value={1}
								handleChange={this.fillForm}
							/>

							{!this.state.successMessage &&
								<ButtonSubmit/>
							}

							{this.state.successMessage &&
								<div className="d-flex justify-content-between">
									<ButtonBordered link={this.getForumUrl()} classes={'m-r-10'} text={`Back to forum`}/>
									<ButtonSubmit link={this.getTopicUrl()} classes={'m-l-10'} text={`Go to new topic`}/>
								</div>
							}
						</div>
					</div>

				</form>

				<LoadingSpinner loading={this.state.loading} />
			</div>
			</>
		);
	}
}
