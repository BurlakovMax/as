import React, {Component} from 'react';
import cities from "../../services/cities.js";
import axios from '../../services/api_init';
import Moment from "react-moment";
import { changeTitle } from '../../store/common';
import LoadingSpinner from "../../components/common/loading-spinner";
import auth from "../../lib/auth";
import Pagination from "../../components/common/pagination";
import {Link} from "react-router-dom";
import config from "../../lib/config";

export default class ForumTopic extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			errorMessage: false,
			successMessage: false,
			account: {},

			city: {},

			topicId: this.props.match.params.topicId,
			topics: [],
			topic: {},
			topicSubscribed: false,

			postsList: [],
			postsFirst: [],
			postsTotal: 0,

			replyError: false,
			replySuccess: false,

			// pagination params
			offset: 0,
			limit: 10,
		};
	}

	// 1
	getCity = async () => {
		const {stateName, cityName} = this.props.match.params;

		let city = await cities.getByUrl(stateName, cityName);
		config.setCity(city);

		this.setState({
			city: city,
		});
	}

	// 2
	getTopic = async () => {
		try {
			const res = await axios.get(`/api/v1/forumTopics/${this.state.topicId}`);

			this.setState({
				topic: res.data.topic,
				postsFirst: res.data.post,
			});
			changeTitle(res.data.title);

		} catch (error) {
			config.catch(error);
		}
	}

	// 3
	getPosts = async (offset = 0, limit = 10, scrollTop = true) => {
		this.setState({loading: true});

		let list = [];
		let total = 0;

		try {
			let params = {
				locationId: this.state.city.id,
				topicId: this.state.topicId,
				offset: offset,
				limit: limit,
			};
			const res = await axios.get('/api/v1/forumPosts', {params});
			total = res.data.total;
			list = res.data.list;

		} catch (error) {
			config.catch(error);
		}

		this.setState({
			offset: offset,
			limit: limit,
			postsList: list,
			postsTotal: total,
		});

		this.setState({loading: false});

		if (scrollTop) {
			window.scrollTo({top: 0, behavior: 'smooth'});
		}
	}

	getData = async (offset = 0, limit = 10, scrollTop = true) => {
		this.setState({loading: true});
		await this.getPosts(offset, limit, scrollTop);
		this.setState({loading: false});
	}

	subscribeToggle = async (event) => {
		event.preventDefault();

		this.closeAlert();

		if (!this.state.account.id) {
			this.setState({
				errorMessage: 'Please login or register'
			});
		}

		if (this.state.topicSubscribed) {
			try {
				await axios.delete('/api/v1/pc/subscribers', {
					data: {
						topicId: this.state.topicId
					}
				});

				this.setState({
					topicSubscribed: false
				});
			} catch (error) {
				config.catch(error);
			}
		} else {
			try {
				let res = await axios.post('/api/v1/pc/subscribers', {
					topicId: this.state.topicId
				});

				if (res.data.id) {
					this.setState({
						topicSubscribed: true
					});
				}
			} catch (error) {
				config.catch(error);
			}
		}
	}

	subscribeCheck = async () => {
		if (this.state.account.id) {
			try {
				await axios.get(`/api/v1/pc/subscribers?topicId=${this.state.topicId}`);

				this.setState({
					topicSubscribed: true
				});
			} catch (error) {
				config.catch(error);
			}
		}
	}

	addReply = async (event) => {
		event.preventDefault();

		let textInput = document.getElementById('reply');
		let text = textInput.value;
		if (!text) {
			this.setState({
				replyError: 'Please write reply text'
			});
			return false;
		}

		try {
			let res = await axios.post('/api/v1/pc/forumPosts', {
				topicId: this.state.topicId,
				text: text
			});

			if (res.data.id) {
				textInput.value = '';
				this.setState({
					replySuccess: 'Your reply successfully added'
				});
				await this.getData(this.state.offset, this.state.limit, false);
			}
		} catch (error) {
			config.catch(error);

			this.setState({
				replyError: 'Error adding your reply. Please try again.'
			});
		}

		setTimeout(() => {
			this.setState({
				replySuccess: false
			});
		}, 3000)
	}

	closeAlert = () => {
		this.setState({
			errorMessage: null,
			successMessage: null,
		});
	}

	async componentDidMount() {
		this.setState({loading: true});

		await auth.getAccount(true)
			.then(account => {
				if (account) {
					this.setState({
						account: account,
					});
				}
			})

		await this.getCity();

		// get current topic info
		await this.getTopic();

		// get topic posts
		await this.getPosts();

		// get subcribed status
		await this.subscribeCheck();

		this.setState({loading: false});
	}

	render() {
		let countPages = (this.state.topic && this.state.topic.countReplies > this.state.limit)? Math.ceil(this.state.topic.countReplies / this.state.limit) : 1;

		return (
			<>
					<div className="content forum-reply">

					{/*TOPIC COUNTS*/}
						<div className="bg-white wrapper-forum-reply-container">
							<div className="forum-reply-container ">

								<div className="primary-info-link-group d-flex align-items-center justify-content-between flex-nowrap border-bottom">
									<a className="link-container d-flex flex-column align-items-center p-y-5" href="/#">
										<span className="count color-primary weight-600">{this.state.topic.countReplies}</span>
										<span className="name f-s-16 color-tertiary">Total Posts</span>
									</a>
									<a className="link-container d-flex flex-column align-items-center p-y-5" href="/#">
										<span className="count color-primary weight-600">{countPages}</span>
										<span className="name f-s-16 color-tertiary">Topic Pages</span>
									</a>
								</div>
							</div>
						</div>


						{/*TOPIC SUBSCRIBE*/}
						{this.state.account.id &&
							<div className="bg-white text-center p-y-10 m-b-10">
								<div className="container">
									<a className="third-filled" onClick={this.subscribeToggle} href="/#">
									{this.state.topicSubscribed ? 'Unsubscribe From This Topic' : 'Subscribe To This Topic'}
									</a>
								</div>
							</div>
						}

					{/*TOPIC TITLE*/}
					<div className="bg-fifth-gray m-b-10 p-y-15 wrapper-forum-reply-container">
						<div className="container">
							<div className="forum-reply-container ">
								<h2 className="color-primary weight-700 p-b-15">{this.state.topic.title}</h2>

								<div className="row-reply">
									<div className="header-reply d-flex align-items-center justify-content-between m-b-10">
										<div className="name d-flex align-items-center">
											<svg className="icon_contacts">
												<use xlinkHref="/images/icons.svg#icon_contacts"></use>
											</svg>
											<h3 className="weight-600 p-x-10 lh-normal">{this.state.topic.accountName}</h3>
											<div className="reply-icon">
												<svg className="icon_messages">
													<use xlinkHref="/images/icons.svg#icon_messages"></use>
												</svg>
											</div>
										</div>
										<div className="date p-l-5 f-s-16">
											<Moment format="MM/DD/YYYY HH:mm">
												{this.state.topic.createdAt}
											</Moment>
										</div>
									</div>
									<div className="body-reply">
										<p className="message f-s-16">
											{this.state.postsFirst.text}
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					{/*TOPIC REPLIES*/}
					<div className="bg-fifth-gray m-b-10 p-y-15 wrapper-forum-reply-container">
						<div className="container">
							<div className="forum-reply-container ">
								<h2 className="color-primary weight-700 p-b-15">Replies</h2>

								{/*TOPIC REPLIES - LIST*/}
								{this.state.postsList.map((post, index) =>
									{
										return (
											<div key={index} className="row-reply p-b-20 m-b-20">
												<div className="header-reply d-flex align-items-center justify-content-between m-b-10">
													<div className="name d-flex align-items-center">
														<div className="contact-icon">
															<svg className="icon_contacts">
																<use xlinkHref="/images/icons.svg#icon_contacts"></use>
															</svg>
														</div>
														<h3 className="weight-600 p-x-10 lh-normal">{post.accountName}</h3>
														<div className="reply-icon">
															<svg className="icon_messages">
																<use xlinkHref="/images/icons.svg#icon_messages"></use>
															</svg>
														</div>
													</div>
													<div className="date p-l-5 f-s-16">
														<Moment format="MM/DD/YYYY HH:mm">
															{post.createdAt}
														</Moment>
													</div>
												</div>
												<div className="body-reply">
													<p className="message f-s-16">
														{post.text}
													</p>
												</div>
											</div>
										)
									}
								)}

								{/*TOPIC REPLIES - ADD FORM*/}
								<div className="answer">
									{this.state.account.id ? (
										<>
											{this.state.replyError &&
												<div className={'answer-error m-t-5'}>
													{this.state.replyError}
												</div>
											}
											{this.state.replySuccess &&
												<div className={'answer-success m-t-5'}>
													{this.state.replySuccess}
												</div>
											}
											<form onSubmit={this.addReply} className="form review-reply-form" method="POST" action="#" data-ajax="false">
												<div className="answer">
													<div className="d-flex align-items-center m-t-10">
														<textarea className="answer-textarea p-x-10 p-y-10 f-s-16" id={'reply'} name="reply" maxLength={1000} placeholder="Write your reply" required/>
														<button
															className="submit-btn bg-transparent p-r-0 color-secondary"
															disabled={!window.navigator.onLine}
															type="submit">
															<svg className="icon_send_reply">
																<use xlinkHref="/images/icons.svg#icon_send_reply"/>
															</svg>
														</button>
													</div>
												</div>
											</form>
										</>
									) : (
										<Link to={auth.getLink('signin')} className={'d-block text-center'}>
											To post a reply on this topic, register free on Adultsearch.com
										</Link>
									)}
								</div>

								{this.state.postsTotal > 0 && <Pagination total={this.state.postsTotal} getData={this.getData}/>}

							</div>
						</div>
					</div>

					<LoadingSpinner loading={this.state.loading} />
				</div>
			</>
		);
	}
}
