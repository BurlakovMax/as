import React, {Component} from 'react';
import {Link} from "react-router-dom";
import Moment from 'react-moment';
import places from "../../services/places.js";
import Breadcrumbs from "../../components/common/breadcrumbs";
import PlaceMap from "./PlaceMap";
import ProfileCarousel from "../../components/common/profile-carousel";
import breadcrumbs from "../../services/breadcrumbs.js";
import LoadingSpinner from "../../components/common/loading-spinner";
import axios from '../../services/api_init';
import db from "../../istore";
import auth from "../../lib/auth";
import {changeTitle} from '../../store/common';
import config from "../../lib/config";
import ratingStars from "../../components/common/rating-stars";
import cities from "../../services/cities";
import Socials from "../../components/common/socials";

export default class Place extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,

            placeId: props.match.params.placeId,
            place: {},
            rates: [],
            reviews: [],
            workingHours: [],
            attributes: [],
            information: [],
            account: {},
            links: [],
            city: {},
            category: {},
            placeTypes: [],
            isOwner: false,

            showCommentForm: {},
            showDropdownMenu: {},
            showCommentError: {},
            showCommentSuccess: {},
        };
    }

    getCity = async () => {
        const {stateName, cityName} = this.props.match.params;
        let city = await cities.getByUrl(stateName, cityName);
        config.setCity(city);

        this.setState({
            city: city,
        });
    }

    getCategory = async () => {
        // get category slug from url
        const categorySlug = this.props.match.url.split('/')[this.props.match.url.split('/').length - 2];

        // get category object
        let types = await places.getTypes(this.state.city.id, 'city');
        const category = types.find(type => type.url === categorySlug) || {};

        this.setState({
            placeTypes: types,
            category: category,
        });
    }

    getData = async () => {
        await this.setState({
            information: await places.getInformation(this.state.placeId),
            place: await places.getById(this.state.placeId),
            links: await breadcrumbs.getPlaceBreadcrumbs(this.state.placeId),
            attributes: await places.getAttributes(this.state.placeId),
            workingHours: await places.getWorkingHours(this.state.placeId),
            reviews: await places.getReviews(this.state.placeId),
            rates: await places.getRates(this.state.placeId),
        });
        changeTitle(this.state.place.name);

        config.debug('attributes', this.state.attributes);
    }

    showCommentForm = reviewId => event => {
        event.preventDefault();

        let showCommentForm = this.state.showCommentForm;
        showCommentForm[reviewId] = !showCommentForm[reviewId];

        this.setState({
            showCommentForm: showCommentForm
        });
    }

    showDropdownMenu = reviewId => event => {
        event.preventDefault();

        let showDropdownMenu = this.state.showDropdownMenu;
        showDropdownMenu[reviewId] = !showDropdownMenu[reviewId];

        this.setState({
            showDropdownMenu: showDropdownMenu
        });
    }

    closeDropdownMenu = async (event) => {
        this.setState({
            showDropdownMenu: {}
        });
    }

    addComment = async (event) => {
        event.preventDefault();

        if (!this.state.account.id) {
            window.location.href = auth.getLink('signin', {'origin': this.state.originUrl});
            return;
        } else {
            let reviewId = event.currentTarget.dataset.review_id;
            let reviewText = document.getElementById(`add_review_${reviewId}`).value;

            this.clearCommentStates(reviewId);

            try {
                let postData = {
                    'comment' : reviewText
                };
                const res = await axios.post(`/api/v1/pc/places/${this.state.placeId}/reviews/${reviewId}/comments`, postData);

                if(res.data.id) {
                    // update reviews in indexdb
                    this.setState({
                        reviews: await places.getReviews(this.state.placeId, true)
                    });

                    this.updateCommentStates(reviewId, true)
                } else {
                    this.updateCommentStates(reviewId, false)
                }

            } catch (error) {
                config.catch(error);

                this.updateCommentStates(reviewId, false)
            }
        }
    }

    deleteComment = async (event) => {
        event.preventDefault();

        let commentId = event.currentTarget.dataset.comment_id;
        let reviewId = event.currentTarget.dataset.review_id;

        this.clearCommentStates(reviewId);

        if (window.confirm('Are you sure to delete this comment?')) {
            try {
                await axios.delete(`/api/v1/pc/places/${this.state.placeId}/reviews/${reviewId}/comments/${commentId}`);
            } catch (error) {
                config.catch(error);
            }

            // clear reviews in indexdb
            await db.place_reviews.clear();

            // update reviews in indexdb
            this.setState({
                reviews: await places.getReviews(this.state.placeId, true)
            });
        }
    }

    deleteReview = async (event) => {
        event.preventDefault();

        let id = event.currentTarget.dataset.id;

        if (window.confirm('Are you sure to delete this review?')) {
            try {
                await axios.delete(`/api/v1/pc/placeReviews/${id}`);
            } catch (error) {
                config.catch(error);
            }

            // clear reviews in indexdb
            await db.place_reviews.clear();

            // update reviews in indexdb
            this.setState({
                reviews: await places.getReviews(this.state.placeId, true)
            });
        } else {
            this.closeDropdownMenu();
        }
    }

    clearCommentStates = (reviewId) => {

        let showCommentSuccess = this.state.showCommentSuccess;
        showCommentSuccess[reviewId] = false;
        this.setState({
            showCommentSuccess: showCommentSuccess
        });

        let showCommentError = this.state.showCommentError;
        showCommentError[reviewId] = false;
        this.setState({
            showCommentError: showCommentError
        });

    }

    updateCommentStates = (reviewId, success) => {

        if (success) {

            let showCommentSuccess = this.state.showCommentSuccess;
            showCommentSuccess[reviewId] = true;
            this.setState({
                showCommentSuccess: showCommentSuccess
            });

            let showCommentForm = this.state.showCommentForm;
            showCommentForm[reviewId] = false;
            this.setState({
                showCommentForm: showCommentForm
            });

        } else {

            let showCommentError = this.state.showCommentError;
            showCommentError[reviewId] = true;
            this.setState({
                showCommentError: showCommentError
            });

        }
    }

    /**
     * Check if add review button is available
     */
    showReviewButton = () => {
        let show = true;
        this.state.reviews.forEach(review =>
          {
              if (review.review.accountId === this.state.account.id) {
                  show = false;
              }
          }
        )

        return show;
    }

    getReviewLink = (reviewId) => {
        let url = '#';

        if (!this.state.account.id) {
            url = auth.getLink('signin', {'origin': this.state.originUrl})
        } else {
            if (reviewId) {
                url = `${this.getPlaceUrl()}/add_review?reviewId=${reviewId}`
            } else {
                url = `${this.getPlaceUrl()}/add_review`
            }
        }

        return url;
    }

    getPlaceOwner = () => {
        if (this.state.account.id && this.state.account.id === this.state.place.ownerId) {
            this.setState({
                isOwner: true
            });
        }
    }

    getPlaceUrl = () => {
        return `${this.state.city.url}${this.state.category.url}/${this.state.place.id}`;
    }

    getPlaceEditUrl = () => {
        return `${this.state.city.url}add?id=${this.state.place.id}`;
    }

    getPlaceBuyUrl = () => {
        return `/businessowner/pick/${this.state.place.id}`;
    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount(true)
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getCity();
        await this.getCategory();
        await this.getData();
        await this.getPlaceOwner();

        config.debug('PLACE', this.state.place);

        this.setState({
            originUrl: encodeURIComponent(window.location.href.replace(window.location.origin, '')),
            loading: false
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.reviews !== this.state.reviews) {
            // update rating
            ratingStars();
        }
    }

    renderInformation(info) {
        if (parseInt(info.value) === 1) {
            return (
                <div key={info.id} className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                    <span className="left">{info.title}</span>
                    <span className="right m-l-5">Yes</span>
                </div>
            )
        }
    }

    renderAttribute(attribute) {
        let value = false;

        let options = {};
        let optionsRaw = attribute.options.split('|');
        optionsRaw.map(option => {
            let arr = option.split('@');
            options[arr[0]] = arr[1];
        })

        if (attribute.value && attribute.value !== 'NULL') {
            if (options[attribute.value]) {
                value = options[attribute.value];
            }
        }

        if (attribute.value_text && attribute.value_text !== 'NULL') {
            if (value) {
                value += ', ' + attribute.value_text;
            } else {
                value = attribute.value_text;
            }
        }

        if (value) {
            return (
                <div key={attribute.id} className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                    <span className="left">{attribute.title}</span>
                    <span className="right">{value}</span>
                </div>
            )
        }
    }

    render() {
        return (
        <div className="content profile place">

            <Breadcrumbs links={this.state.links}/>

            {/*SLIDER*/}
            <div className="profile-carousel-container position-relative bg-white">
                {
                    this.state.place.image
                        ? (
                            <ProfileCarousel
                              slides={{
                                  // TODO: add video to slider ???
                                  images: [this.state.place.image]
                              }}
                              srcPrefixImages={config.getImagePath('place')}
                            />
                        ) : (
                            <div className="bg-white p-b-10">
                                <div className="no-photo">
                                    <img src="/images/icons/no_photo.svg" alt="Avatar"/>
                                </div>
                            </div>
                        )}
            </div>

            {/*PLACE INFO*/}
            <div className="establishment-information p-y-10 bg-white">
                <div className="container">
                    <h1 className="weight-700">{this.state.place.name}</h1>

                    <div className="rating d-flex p-y-10">
                        <div className="primary-rating" data-readonly={true} data-rating={this.state.place.overall}/>

                        <span className="count m-l-5 f-s-16">{this.state.reviews.length}</span>
                    </div>

                    <div className="contact weight-600 f-s-18 color-primary d-flex align-items-center">
                        <span className="icon m-r-5">
                          <svg className="icon_phone">
                            <use xlinkHref="/images/icons.svg#phone" />
                          </svg>
                        </span>
                        <a href={`tel:${this.state.place.phone1}`}>{this.state.place.phone1}</a>
                        {this.state.place.phone2 &&
                          <>
                            , &nbsp; <a href={`tel:${this.state.place.phone2}`}>{this.state.place.phone2}</a>
                          </>
                        }
                    </div>

                    <div className="location p-t-15 d-flex align-items-center f-s-16">
                        <span className="icon m-r-5">
                            <svg className="icon_place_location">
                                <use xlinkHref="/images/icons.svg#icon_place_location" />
                            </svg>
                        </span>
                        <span>
                            {this.state.place.address1}
                            {this.state.place.address2 &&
                            <>
                                , &nbsp;{this.state.place.address2}
                            </>
                            }
                        </span>
                    </div>
                </div>
            </div>

            {/*PLACE TABS*/}
            <div className="primary-tabs" onTouchMove={this.closeDropdownMenu}>

                <div className="primary-tabs-nav-container position-relative bg-white m-t-10 bs-primary">
                    <ul className="nav nav-tabs scrolling-tabs d-flex flex-nowrap justify-content-between" id="primary-tab" role="tablist">
                        <li className="nav-item d-flex align-items-center">
                            <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                Info
                            </a>
                        </li>
                        {this.state.rates.length > 0 &&
                            <li className="nav-item d-flex align-items-center">
                                <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center" id="rates-tab" data-toggle="tab" href="#rates" role="tab" aria-controls="rates" aria-selected="false">
                                    Rates
                                </a>
                            </li>
                        }
                        <li className="nav-item d-flex align-items-center">
                            <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center" id="hours-tab" data-toggle="tab" href="#hours" role="tab" aria-controls="hours" aria-selected="false">
                                Hours
                            </a>
                        </li>
                        <li className="nav-item d-flex align-items-center">
                            <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">
                                Reviews
                            </a>
                        </li>
                        <li className="nav-item d-flex align-items-center">
                            <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center" id="directions-tab" data-toggle="tab" href="#directions" role="tab" aria-controls="directions" aria-selected="false">
                                Directions
                            </a>
                        </li>
                    </ul>
                </div>

                <div className="tab-content primary-tab-content">
                    <div className="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
                        <div className="tab-content-container bg-white">
                            <div className="container">
                                <div className="tab-content-container-inner info p-t-20 p-b-5">
                                    {this.state.attributes.map(attribute => {
                                        return this.renderAttribute(attribute)
                                        }
                                    )}
                                    {this.state.information.map(info => {
                                        return this.renderInformation(info)
                                        }
                                    )}

                                    <Socials values={this.state.place}/>

                                    {this.state.place.webSite &&
                                        <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                                            <span className="left">Website</span>
                                            <span className="right m-l-5">
                                                <a className={'link'} target="_blank" href={this.state.place.webSite}>{this.state.place.webSite}</a>
                                            </span>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="profile-error m-t-5 bg-white f-s-16">
                            <div className="container">
                                <div className="profile-error-inner d-flex justify-content-between p-y-10">
                                    {!this.state.place.ownerId &&
                                        <Link className="d-flex flex-grow-1 justify-content-center align-items-center color-secondary" to={this.getPlaceBuyUrl()}>
                                            <svg className="icon_house">
                                                <use xlinkHref="/images/icons.svg#icon_house"></use>
                                            </svg>
                                            <span className="m-l-5">Your Business?</span>
                                        </Link>
                                    }
                                    {this.state.isOwner &&
                                        <Link className="d-flex flex-grow-1 justify-content-center align-items-center color-secondary" to={this.getPlaceEditUrl()}>
                                            <svg className="icon_house">
                                                <use xlinkHref="/images/icons.svg#icon_house"></use>
                                            </svg>
                                            <span className="m-l-5">Manage My Place</span>
                                        </Link>
                                    }
                                    <Link className="d-flex flex-grow-1 justify-content-center align-items-center color-secondary" to={auth.getLink('contact')}>
                                        <svg className="icon_bordered_cross">
                                            <use xlinkHref="/images/icons.svg#icon_bordered_cross_blue"></use>
                                        </svg>
                                        <span className="m-l-5">Report an Error</span>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>

                    {this.state.rates.length > 0 &&
                        <div className="tab-pane fade" id="rates" role="tabpanel" aria-labelledby="rates-tab">
                            <div className="tab-content-container bg-white">
                                <div className="container">
                                    <div className="tab-content-container-inner rates p-t-20 p-b-5">
                                        <div className="data-list-item weight-600 d-flex align-items-center justify-content-between p-b-5 m-b-10">
                                            <span className="left">Duration</span>
                                            <span className="right">Price</span>
                                        </div>
                                        {this.state.rates.map(rate =>
                                            <div key={rate.title}
                                                 className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                                                <span className="left">{rate.title}</span>
                                                <span className="right">${parseFloat(rate.value)}</span>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    }

                    <div className="tab-pane fade" id="hours" role="tabpanel" aria-labelledby="hours-tab">
                        <div className="tab-content-container bg-white">
                            <div className="container">
                                <div className="tab-content-container-inner hours p-t-20 p-b-5">
                                    {this.state.workingHours.map(workingHour =>
                                        <div key={workingHour.dayOfWeek} className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                                            <span className="left">{workingHour.dayOfWeek}</span>
                                            <span className="right">{workingHour.from} - {workingHour.to}</span>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                        <div className="tab-content-container bg-white">
                            <div className="container">
                                <div className="tab-content-container-inner reviews p-t-20 p-b-20">
                                    {this.state.reviews.map(review =>
                                        <div key={review.review.id} id={`review_${review.review.id}`} className="single-review p-b-10 m-b-10">

                                                <div className="d-flex justify-content-between">
                                                    {/*user info + avatar*/}
                                                    <div className="user d-flex flex-nowrap align-items-center">
                                                        {review.author.avatar ?
                                                            (<img className="user-image" src={`${config.getImageServer()}${review.author.avatar}`} alt="avatar"/>)
                                                            :
                                                            (<img className="user-image" src="/images/icons/User_1.jpg" alt="avatar"/>)
                                                        }
                                                        <div className="info p-l-10">
                                                            <div className="name weight-600 d-block color-tertiary">{review.author.name}</div>
                                                            <div className="primary-rating d-block" data-readonly={true} data-rating={review.review.star}/>
                                                            <span className="date m-t-5 d-block f-s-14">
                                                            <Moment format="YYYY/MM/DD HH:mm">{review.review.reviewDate}</Moment>
                                                        </span>
                                                        </div>
                                                    </div>

                                                    {/*dropdown menu*/}
                                                    {review.review.accountId === this.state.account.id &&
                                                        <div key={review.review.id} className="drop-actions" data-review_id={review.review.id}>
                                                            <button onClick={this.showDropdownMenu(review.review.id)} type="button" className="btn-menu drop-actions__toggler">
                                                                <svg className="icon_menu">
                                                                    <use xlinkHref="/images/icons.svg#icon_menu"/>
                                                                </svg>
                                                            </button>

                                                            <div className={`popup-menu drop-actions__list ${this.state.showDropdownMenu[review.review.id]? 'drop-actions__list_visible' : ''}`}>
                                                                {/*  {% if ismember %}
                                                                <Link to="/pm/compose?recipient={{ data.account_id }}" className="popup-menu__item">Send PM</Link>
                                                                {% endif %}*/}

                                                                <Link to={this.getReviewLink(review.review.id)} className="popup-menu__item">Edit</Link>
                                                                <a href="/#" data-id={review.review.id} onClick={this.deleteReview} className="popup-menu__item">Remove</a>

                                                                {/* <a href="{{ data.link_move }}" className="popup-menu__item" data-ajax="false">Move</a>
                                                                {% if hasPermission('edit_all_places') %}
                                                                <a href="{{ data.link_makeforum }}" className="popup-menu__item" data-ajax="false">Move to Forum</a>
                                                                {% endif %}
                                                                <a href="{{ id }}/review/{{ data.review_id }}/uploadVideo" className="popup-menu__item" data-ajax="false">Upload video</a>
                                                                {% endif %}*/}
                                                            </div>
                                                        </div>
                                                    }
                                                </div>

                                                <div className="message">
                                                    <p className="p-t-10">
                                                        {review.review.comment}
                                                    </p>
                                                </div>

                                                {(review.comments.length > 0) &&
                                                    <div className="review-comments">
                                                        <span className="p-t-10 p-b-20 weight-600 f-s-20 color-primary d-block">Member Comments</span>

                                                        {review.comments.map((comment, index) =>
                                                          <div id={`comment_${comment.comment.id}`} key={index} className={`single-review p-b-10 m-b-10 ${index > 3? 'd-none' : ''}`}>
                                                              <div className="user d-flex flex-nowrap align-items-center">
                                                                  {comment.author.avatar ?
                                                                    (<img className="user-image" src={`${config.getImageServer()}${comment.author.avatar}`} alt={comment.author.name}/>)
                                                                    :
                                                                    (<img className="user-image" src="/images/icons/User_1.jpg" alt={comment.author.name}/>)
                                                                  }
                                                                  <div className="info p-x-10">
                                                                      <div className="d-flex align-items-center flex-wrap">
                                                                          <span className="name weight-600 d-block color-tertiary m-r-10">{comment.author.name}</span>
                                                                          {comment.comment.accountId === this.state.account.id &&
                                                                            <a className="delete-place-review-comment color-primary"
                                                                               data-review_id={review.review.id}
                                                                               data-comment_id={comment.comment.id}
                                                                               onClick={this.deleteComment}
                                                                               href="/#">
                                                                                Delete
                                                                            </a>
                                                                          }
                                                                      </div>
                                                                      <span className="date m-t-5 d-block f-s-14">
                                                                        <Moment format="YYYY/MM/DD HH:mm">{comment.comment.createdAt}</Moment>
                                                                      </span>
                                                                  </div>
                                                              </div>
                                                              <div className="message">
                                                                  <p className="p-t-10">
                                                                      {comment.comment.comment}
                                                                  </p>
                                                              </div>
                                                          </div>
                                                        )}

                                                    </div>
                                                }

                                                <div className="answer">
                                                    {this.state.account.id ? (
                                                      <>
                                                      {this.state.showCommentError[review.review.id] &&
                                                          <div className={'answer-error m-t-5'}>
                                                              Error adding your comment. Please try again.
                                                          </div>
                                                      }
                                                      {this.state.showCommentSuccess[review.review.id] &&
                                                          <div className={'answer-success m-t-5'}>
                                                              Your comment successfully added.
                                                          </div>
                                                      }
                                                      {(!this.state.showCommentForm[review.review.id] && !this.state.showCommentSuccess[review.review.id] && !this.state.showCommentError[review.review.id]) &&
                                                          <a href="/#" onClick={this.showCommentForm(review.review.id)} className="open-review-reply d-block m-t-10 color-fifth">
                                                              Add a comment or question...
                                                          </a>
                                                      }
                                                      {this.state.showCommentForm[review.review.id] &&
                                                          <form id={`form_${review.review.id}`} onSubmit={this.addComment} data-review_id={review.review.id} className="form review-reply-form" method="POST" action="#" data-ajax="false">
                                                              <div className="answer">
                                                                  <div className="d-flex align-items-center m-t-10">
                                                                      <textarea id={`add_review_${review.review.id}`} className="answer-textarea p-x-10 p-y-10 f-s-16" name="comment" placeholder="Add a comment or question..." required/>
                                                                      <button className="submit-btn bg-transparent p-r-0 color-secondary" disabled={!window.navigator.onLine} type="submit">
                                                                          <svg className="icon_send_reply">
                                                                              <use xlinkHref="/images/icons.svg#icon_send_reply"/>
                                                                          </svg>
                                                                      </button>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                      }
                                                      </>
                                                    ) : (
                                                      <Link to={auth.getLink('signin', {'origin': this.state.originUrl})} className="login-message m-t-10 d-block f-s-14">
                                                          Please login to add comments to this review
                                                      </Link>
                                                    )}
                                                </div>

                                            </div>
                                    )}
                                    {this.state.reviews.length === 0 &&
                                        <div className={'p-y-20 text-center color-label'}>No reviews so far. You can add one.</div>
                                    }
                                    {this.showReviewButton() &&
                                        <Link className="primary-filled-btn" to={this.getReviewLink(false)}>
                                            Write a review
                                        </Link>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="tab-pane fade" id="directions" role="tabpanel" aria-labelledby="directions-tab">
                        <div className="tab-content-container bg-white">
                            <div className="container">
                                <div className="tab-content-container-inner directions p-t-20 p-b-5">
                                    <div className="directions-map m-b-20">
                                        {this.state.place.latitude ? (
                                            <PlaceMap
                                                lat={parseFloat(this.state.place.latitude)}
                                                lng={parseFloat(this.state.place.longitude)}
                                                title={this.state.place.name}
                                            />
                                        ) : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <LoadingSpinner loading={this.state.loading} />
        </div>
        )
    }
}
