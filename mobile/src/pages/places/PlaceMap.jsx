import React, {Component} from 'react';
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';

class PlaceMap extends Component {
    render() {
        return (
            <Map
                google={this.props.google}
                zoom={14}
                style={{width: '100%', height: '100%', position: 'relative'}}
                containerStyle={{width: '100%', height: '400px', position: 'relative'}}
                initialCenter={{
                    lat: this.props.lat,
                    lng: this.props.lng
                }}
            >
                <Marker
                    name={this.props.title}
                />
            </Map>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyBGu1jSkG2g5AyKtXLgc1cYpMOqpmLAvlg'
})(PlaceMap);
