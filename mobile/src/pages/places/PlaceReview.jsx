import React, {Component} from 'react';
import {Link} from "react-router-dom";
import places from "../../services/places.js";
import LoadingSpinner from "../../components/common/loading-spinner";
import axios from '../../services/api_init';
import ButtonSubmit from "../../components/form/button-submit";
import Alert from "../../components/common/alert";
import Textarea from "../../components/form/textarea";
import auth from "../../lib/auth";
import ratingStars from "../../components/common/rating-stars";
import {history} from "../../index";
import config from "../../lib/config";

export default class PlaceReview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorMessage: null,
            successMessage: null,
            loading: false,
            blocked: false,
            placeId: props.match.params.placeId,
            place: {},
            reviewId: new URLSearchParams(window.location.search).get('reviewId') || false,
            account: {},
            providerId: 0, // ???
            review: {},
        };
    }

    getPlace = async () => {
        this.setState({
            place: await places.getById(this.state.placeId),
        });
    }

    getAccount = async () => {
        try {
            const res = await axios.get('/api/v1/pc/account');

            this.setState({
                account: res.data,
            });
        } catch (error) {
            config.catch(error);
        }
    }

    getReview = async () => {
        try {
            const res = await axios.get(`/api/v1/places/${this.state.placeId}/reviews/${this.state.reviewId}`);

            if (res.data.author.accountId === this.state.account.id) {
                this.setState({
                    review: res.data.review
                });
            } else {
                this.setState({
                    errorMessage: 'Sorry, you are not allowed to edit this review',
                    reviewId: false,
                });
            }
        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: 'Sorry, review not found',
                reviewId: false,
            });
        }
    }

    /**
     * Add/edit review
     * @param event
     * @returns {Promise<boolean>}
     */
    submit = async event => {
        event.preventDefault();

        let star = document.getElementById('star').value;
        let comment = document.getElementById('comment').value;
        if (!comment) {
            alert('Please write your comment');
            return false;
        }

        this.setState({
            loading: true,
            errorMessage: false,
        });

        if (this.state.reviewId) {
            await this.editReview(comment, star);
        } else {
            await this.addReview(comment, star);
        }
    }

    addReview = async (comment, star) => {
        try {
            const res = await axios.post('/api/v1/pc/placeReviews', {
                providerId: this.state.providerId,
                placeId: this.state.place.id,
                comment: comment,
                star: star,
            });

            if (res.data.id) {
                await places.getReviews(this.state.place.id, true);

                this.setState({
                    loading: false,
                    successMessage: 'Review successfully added'
                });
            } else {
                this.setState({
                    loading: false,
                    errorMessage: 'Error adding review, please try again later'
                });
            }
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: 'Error adding review, please try again later'
            });
        }
    }

    editReview = async (comment, star) => {
        try {
            await axios.put(`/api/v1/pc/placeReviews/${this.state.reviewId}`, {
                providerId: this.state.providerId,
                comment: comment,
                star: star,
            });

            // update reviews in indexdb
            await places.getReviews(this.state.place.id, true);

            this.setState({
                loading: false,
                successMessage: 'Review successfully updated'
            });

        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: 'Error updating review, please try again later'
            });
        }
    }

    async componentDidMount() {
        this.setState({loading: true});

        await this.getPlace();
        await this.getAccount();

        // get review info for edit
        if (this.state.reviewId) {
            await this.getReview();
        }

        this.setState({loading: false});
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.review !== this.state.review || !this.state.reviewId) {
            // update rating
            ratingStars();
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    render() {

        return (
        <div className="content feedback place-preview-add" id="place-review-page">
          <div className="feedback-container bg-white f-s-16 p-b-20">
              <div className="container">

                  {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

                  <div className="feedback-container-inner p-y-10">
                      <p className="title weight-600">
                          You are about to {this.state.reviewId? 'edit' : 'leave'} a review for <span className="color-primary weight-600">{this.state.place.name}</span>
                      </p>

                      {this.state.successMessage &&
                      <>
                        <div className={'m-y-20 color-success'}>{this.state.successMessage}</div>
                        <ButtonSubmit handleClick={history.goBack} text={`Return to ${this.state.place.name}`}/>
                      </>
                      }

                      {!this.state.successMessage &&
                          <form action="#" onSubmit={this.submit} method="POST">
                              <input type="hidden" name="star" defaultValue={this.state.reviewId? this.state.review.star : 0} id="star"/>

                              <p className="recommend m-t-10">
                                  Would you recommend this {this.state.place.name} place?
                              </p>

                              <div className="primary-rating large m-y-10" data-rating={this.state.reviewId? this.state.review.star : 0} data-large={true}></div>

                              <Textarea
                                classes={'m-t-10'}
                                name={'comment'}
                                id={'comment'}
                                label={`Comments about ${this.state.place.name}`}
                                required={true}
                                placeholder={'Enter comment here'}
                                attributes={this.state.blocked ? ['disabled'] : false}
                                value={this.state.reviewId? this.state.review.comment : ''}
                              />

                              {this.state.blocked ?
                                (<p>You may not leave review for this place. If you think there is a mistake, please <Link to={auth.getLink('contact')}>contact us</Link> right away.</p>)
                                :
                                (<ButtonSubmit/>)
                              }
                          </form>
                      }
                  </div>
              </div>
          </div>

          <LoadingSpinner loading={this.state.loading} />
        </div>
        )
    }
}
