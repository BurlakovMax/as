import React, {Component} from "react";
import {Link} from "react-router-dom";
import Breadcrumbs from "../../components/common/breadcrumbs";
import breadcrumbs from "../../services/breadcrumbs.js";
import NavigationCarousel from "../../components/common/navigation-carousel";
import cities from "../../services/cities.js";
import escorts from "../../services/escorts";
import places from "../../services/places";
import m4mlinks from "../../services/m4m-links.js";
import LoadingSpinner from "../../components/common/loading-spinner";
import Pagination from "../../components/common/pagination";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import db from "../../istore";
import axios from "../../services/api_init";
import moment from "moment";
import ratingStars from "../../components/common/rating-stars";
import ButtonSubmit from "../../components/form/button-submit";
import auth from "../../lib/auth";

export default class PlacesCategory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            account: {},

            links: [],
            category: {},
            city: {},
            escortTypes: [],
            placeTypes: [],
            places: [],
            m4mLink: {},
            placesList: [],
            placesTotal: 0,

            // pagination params
            offset: 0,
            limit: 10,
        }
    }

    getPlaceUrl = (placeId) => {
        return `${this.state.city.url}${this.state.category.url}/${placeId}`;
    }

    // 1
    getCity = async () => {
        const {stateName, cityName} = this.props.match.params;
        let city = await cities.getByUrl(stateName, cityName);
        config.setCity(city);

        this.setState({
            city: city,
        });
    }

    // 2
    getCategory = async () => {
        // get category slug from url
        const categorySlug = this.props.match.url.split('/')[this.props.match.url.split('/').length - 1];

        // get category object
        let types = await places.getTypes(this.state.city.id, 'city');
        const category = types.find(type => type.url === categorySlug) || {};

        await this.setState({
            placeTypes: types,
            category: category,
        });
    }

    // 3
    getStates = async () => {
        const links = breadcrumbs.getCategoryBreadcrumbs(this.state.city.id, this.state.category.url);
        const city = cities.getById(this.state.city.id);
        const escortTypesList = escorts.getTypes(this.state.city.id, 'city');
        const m4mLink = m4mlinks.getLink(this.state.city.id);

        let states = await Promise.all([links, city, escortTypesList, m4mLink]);

        await this.setState({
            links: Array.isArray(states[0]) ? states[0] : [],
            city: states[1],
            escortTypes: Array.isArray(states[2]) ? states[2] : [],
            m4mLink: states[3],
        });
    }

    // 4
    getPlaces = async (offset = 0, limit = 10) => {
        let list = [];
        let show = [];
        let total = 0;

        const places = await this.getPlacesLocal();

        if (places.length > 0) {

            list = places;
            total = list.length;
            config.debug('places LOCAL', list);

        } else {

            // initially get ALL records w/o limit
            let params = {
                locationId: this.state.city.id,
                typeId: this.state.category.id,
                offset: 0,
                limit: 30,
            };

            try {
                const res = await axios.get('/api/v1/places', {params});
                total = res.data.total;
                list = res.data.list;
                config.debug('places SERVER', list);

                await db.places.bulkPut(list);

                await db.sync_cities.put({
                    id: this.state.city.id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }

        }

        if (list.length) {
            show = list.slice(offset, offset+limit);
        }

        this.setState({
            placesList: show,
            placesTotal: total,
        });

        window.scrollTo({top: 0, behavior: 'smooth'});
    }

    getPlacesLocal = async () => {
        return await db.places.where({
            typeId: this.state.category.id,
            locationId: this.state.city.id,
        }).toArray();
    }

    getData = async (offset = 0, limit = 10) => {
        await this.getPlaces(offset, limit);
    }

    async componentDidUpdate(prevProps, prevState, snapshot) {
        const category = this.props.match.url.split('/')[this.props.match.url.split('/').length - 1];
        const prevCategory = prevProps.match.url.split('/')[this.props.match.url.split('/').length - 1];

        if (category !== prevCategory) {
            this.setState({loading: true});

            await this.getCity();
            await this.getCategory();

            changeTitle(this.state.category.name);

            await this.getStates();

            // get list from api/db
            await this.getPlaces();

            this.setState({loading: false});
        }

        if (prevState.placesList !== this.state.placesList) {
            // init ratings
            ratingStars();
        }
    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount(true)
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getCity();
        await this.getCategory();

        changeTitle(this.state.category.name);

        await this.getStates();

        // get list from api/db
        await this.getData();

        this.setState({loading: false});
    }

    render() {
        const slides = [...this.state.escortTypes, ...[this.state.m4mLink], ...this.state.placeTypes];

        return (
            <>
            <div className="content escort-list">

                <Breadcrumbs links={this.state.links} />

                {this.state.city.url && <NavigationCarousel className={'bg-white'} urlPrefix={this.state.city.url} slides={slides} />}

                <div className="bg-white m-b-10">
                    <div className="container">
                        <div className="page-actions p-y-15 d-flex flex-row justify-content-between">
                            <div className="location-info-container d-flex align-items-center">
                                <p className="location-info f-s-20 weight-700">
                                    { this.state.placesTotal } {this.state.category.name} in <span className="location-name color-primary text-nowrap">{ this.state.city.name }</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="bg-white m-b-10 p-y-10">
                    <div className="container">
                        <div className="location-container">

                            <div className="items-container d-flex flex-row flex-wrap list-view">
                                { this.state.placesList.map(place =>
                                    <Link key={place.id} to={this.getPlaceUrl(place.id)} className="card m-b-15 erotic-massage">
                                        <div className="image-container">
                                            {place.thumbnail ? (
                                              <img className="image image-cover" src={`${config.getImagePath('place')}/${place.thumbnail}`} alt={ place.name } />
                                            ) : (
                                              <img className="image image-empty" alt={ place.name } />
                                            )}
                                        </div>
                                        <div className="info-container d-flex justify-content-between flex-column flex-grow-1 p-t-5">
                                            <div className="d-flex flex-grow-1 flex-column justify-content-between">
                                                <div className="p-x-10">
                                                    <div className="title-container">
                                                        <span title={place.name} className="title d-block weight-600 m-b-5">{ place.name }</span>
                                                    </div>
                                                    <div className="reviews-container m-b-5 d-flex flex-wrap f-s-14">
                                                        <div className="rating">
                                                            <div className="primary-rating" data-readonly={true} data-rating={ place.overall }/>
                                                        </div>
                                                        {place.review > 0 &&
                                                            <div className="reviews-count color-tertiary">
                                                                {place.review} Reviews
                                                            </div>
                                                        }
                                                    </div>
                                                    <div className="address-container m-b-5 f-s-14">
                                                        <p className="address color-tertiary">{place.address1}</p>
                                                    </div>
                                                </div>
                                                <div data-phone={place.phone1} className="phone-container call-phone d-flex align-items-center p-y-10 p-x-10">
                                                    <div className="icon m-r-5">
                                                        <svg className="icon_phone">
                                                            <use xlinkHref="/images/icons.svg#icon_phone"/>
                                                        </svg>
                                                    </div>
                                                    <span className="phone weight-600">{place.phone1}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                ) }
                            </div>

                            {this.state.placesTotal > 0 && <Pagination total={this.state.placesTotal} getData={this.getData}/>}
                        </div>
                    </div>
                </div>

                <div className="bg-white m-b-10">
                    <div className="container">
                        <div className="page-actions p-y-15 d-flex flex-row justify-content-between">
                            <ButtonSubmit link={`${this.state.city.url}add`} text={'Add New Place'}/>
                        </div>
                    </div>
                </div>

                <LoadingSpinner loading={this.state.loading} />
            </div>
            </>
        );
    }
}
