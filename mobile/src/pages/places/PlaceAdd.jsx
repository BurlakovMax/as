import React, {Component} from 'react';
import {changeTitle} from "../../store/common";
import LoadingSpinner from "../../components/common/loading-spinner";
import axios from '../../services/api_init';
import ButtonSubmit from "../../components/form/button-submit";
import auth from "../../lib/auth";
import cities from "../../services/cities";
import config from "../../lib/config";
import Input from "../../components/form/input";
import Select from "../../components/form/select";
import locations from "../../services/locations";
import FormSocial from "../../components/common/FormSocial";
import FormPhoto from "../../components/common/FormPhoto";
import uploader from "../../lib/uploader";
import Checkbox from "../../components/form/checkbox";
import placeHours from "../../components/common/place-hours";
import form from "../../lib/form";
import Alert from "../../components/common/alert";
import FormPhone from "../../components/common/FormPhone";
import phone from "../../lib/phone";
import places from "../../services/places";
import db from "../../istore";

/**
 * https://www.figma.com/file/yzONljbqkEumCJy5WmUYbf/AS-PWA?node-id=2080%3A12210
 */
export default class PlaceAdd extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            errorMessage: null,
            successMessage: null,
            loading: false,
            account: {},

            errors: {},
            errorEmail: false,

            place: {},
            hours: {},
            city: {},
            countries: [],

            listType: [],
            listCountry: [],
            listState: [],
            listCity: [],
            listHoursFrom: [],
            listHoursTo: [],
            listHours: {}, // all hours with keys
            listDays: ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'],

            pictures: [],
            picturesMax: 1,
            picturesLimit: false,
            picturesError: false,

            allday_start: 0, // 12:00 AM in to keys
            allday_end: 1440, // 12:00 AM in from keys

            phone: '',
            phoneValid: false,
            phoneResult: false,
            phoneError: false,

            attributes: {},
            attributesKeys: { // ids for attributes from "attribute" table
                always: 1,
                monday_from: 2,
                monday_to: 3,
                tuesday_from: 4,
                tuesday_to: 5,
                wednesday_from: 6,
                wednesday_to: 7,
                thursday_from: 8,
                thursday_to: 9,
                friday_from: 10,
                friday_to: 11,
                saturday_from: 12,
                saturday_to: 13,
                sunday_from: 14,
                sunday_to: 15,
            },

            // place to edit by owner
            placeOwner: false,
            placeId: new URLSearchParams(window.location.search).get('id'),
        };
    }

    /**
     * Set all day hours to day
     * @param event
     */
    toggleDayAll = event => {
        if (event.currentTarget.checked) {
            let dayName = event.currentTarget.dataset.day;
            let dayFrom = `${dayName}_from`;
            let dayTo = `${dayName}_to`;

            // update selects values
            document.getElementById(dayFrom).value = this.state.allday_start;
            document.getElementById(dayTo).value = this.state.allday_end;

            // update state for 24/7
            this.fillHoursAllday(dayName);

            // insert text to labels
            this.setLabel(dayName, {
                [dayFrom]: this.state.allday_start,
                [dayTo]: this.state.allday_end,
            })
        }
    }

    /**
     * Show/close day hours accordeon
     * @param event
     */
    toggleDay = event => {
        let dayName = event.currentTarget.dataset.day;
        let dayFrom = `${dayName}_from`;
        let dayTo = `${dayName}_to`;

        if (event.currentTarget.checked) {
            // check allday input
            document.getElementById(`${dayName}_allday`).checked = true;

            // set 24 hours open - as default
            this.toggleDayAll(event);
        } else {
            // if day unchecked - remove day from attributes
            let attributes = this.state.attributes;

            delete(attributes[`${dayName}_from`]);
            delete(attributes[`${dayName}_to`]);

            this.setState({
                attributes: attributes
            });

            // clear labels texts
            this.setLabel(dayName, {
                [dayFrom]: false,
                [dayTo]: false,
            })
        }
    }

    /**
     * Insert time1 & time2 to day label || clear texts
     * @param dayName
     * @param data {}
     */
    setLabel = (dayName, data) => {
        let showLabel = false;

        for (let key in data) {
            if (data[key] !== false) {
                showLabel = true;
                document.getElementById(`${key}_label`).innerHTML = this.state.listHours[data[key]];
            } else {
                document.getElementById(`${key}_label`).innerHTML = '';
            }
        }

        if (showLabel) {
            document.getElementById(`${dayName}_label`).classList.add('show');
        } else {
            document.getElementById(`${dayName}_label`).classList.remove('show');
        }
    }


    deletePhoto = async event => {
        event.preventDefault();

        let result = await uploader.deletePhoto(
          event,
          this.state.pictures,
          this.state.picturesMax,
          this.state.place.id,
        );

        this.setState({
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })
    }

    uploadPhoto = async event => {
        event.preventDefault();

        let result = await uploader.uploadPhoto(
          event,
          this.state.pictures,
          this.state.picturesMax
        );

        this.setState({
            pictures: result.list,
            picturesLimit: result.limit,
            picturesError: result.error,
        })
    }


    getCity = async () => {
        const {stateName, cityName} = this.props.match.params;
        let city = await cities.getByUrl(stateName, cityName);
        config.setCity(city);

        this.setState({
            city: city,
        });
    }

    getListType = async () => {
        try {
            const res = await axios.get('/api/v1/placeTypes');

            return res.data.list;
        } catch (error) {
            config.catch(error);
        }
    }

    getListCountry = async () => {
        return [
            {value: '16046', name: 'United States', 'selected': true},
            {value: '16047', name: 'Canada'},
            {value: '41973', name: 'United Kingdom'},
        ];
    }

    getListHoursFrom = async () => {
        let list = config.getHoursFrom();

        // create full list
        this.getListHoursAll(list);

        return list;
    }

    getListHoursTo = async () => {
        let list = config.getHoursTo();

        // create full list
        this.getListHoursAll(list);

        return list;
    }

    getListHoursAll = (hoursList) => {
        let hours = this.state.listHours;

        hoursList.map(
          item => {
              hours[item.value] = item.name;
              return null;
        })

        this.setState({
            listHours: hours,
        });
    }


    getStatesByDefault = async () => {
        let usId = this.state.listCountry[0].value;
        document.getElementById('countryId').value = usId;

        await this.getStates(usId);
    }

    /**
     * OnSelect for country
     * @param event
     * @returns {Promise<void>}
     */
    getStates = async (event) => {
        let countryId = event;
        if (typeof event === 'object') {
            countryId = event.currentTarget.value;
        }

        // assign new selected value to place edit
        if (this.state.place.id) {
            this.setState({
                place: Object.assign(this.state.place, {
                    'countryId': countryId,
                    'stateId': false,
                    'locationId': false,
                })
            });
        }

        let states = [];

        // clear child lists
        this.setState({
            listState: [],
            listCity: [],
        });
        document.getElementById('stateId').value = '';
        document.getElementById('locationId').value = '';

        let country = this.state.countries.filter(
          country => {
              return parseInt(country.id) === parseInt(countryId);
          }
        )

        if (country.length) {
            country[0].states.map(state => {
              states.push({
                  value: state.id,
                  name: state.name,
              })
              return null;
            })
        } else {
            let errors = this.state.errors;
            errors['stateId'] = 'States list is empty';
            this.setState({
                errors: errors
            });
        }

        this.setState({
            listState: states,
        });
    }

    /**
     * OnSelect for state
     * @param event
     * @returns {Promise<void>}
     */
    getCities = async (event) => {
        this.setState({loading: true});

        let stateId = (typeof event === 'object')? event.currentTarget.value : event;
        let cities = await locations.getCitiesByStateId(stateId);
        let errors = this.state.errors;

        // assign new selected value to place edit
        if (this.state.place.id) {
            this.setState({
                place: Object.assign(this.state.place, {
                    'stateId': stateId,
                    'locationId': false,
                })
            });
        }

        if (cities.length) {
            errors['stateId'] = false;
            this.setState({
                listCity: cities,
                errors: errors,
            });
        } else {
            errors['locationId'] = 'City list is empty';
            this.setState({
                errors: errors
            });
        }

        document.getElementById('locationId').value = '';

        this.setState({loading: false});
    }

    fillForm = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            place: Object.assign(this.state.place, res)
        });
    }

    fillHours = event => {
        let target = event.target;
        let dayName = target.dataset.day;
        let dayFrom = `${dayName}_from`;
        let dayTo = `${dayName}_to`;

        // uncheck allday checkbox
        document.getElementById(`${dayName}_allday`).checked = false;

        // update label text
        this.setLabel(dayName, {
            [target.name]: target.value
        })

        // add current hours to attrubutes
        let attributes = this.state.attributes;
        attributes[target.name] = target.value;

        this.setState({
            attributes: attributes
        });
    }

    fillHoursAllday = (dayName) => {
        let attributes = this.state.attributes;

        attributes[`${dayName}_from`] = this.state.allday_start;
        attributes[`${dayName}_to`] = this.state.allday_end;

        this.setState({
            attributes: attributes
        });
    }

    /**
     * Verify phone number on input blur
     */
    phoneVerify = () => {
        let phoneNumber = phone.getPhoneInputValue();

        phone.setPhoneInputError(false);
        this.setState({
            phoneError: false
        });

        let phoneError = form.validatePhone();
        if (phoneError) {
            this.setState({
                phoneError: phoneError
            });
            return false;
        }

        this.setState({
            phone: phoneNumber,
        });
    }

    validate = () => {
        let ok = true;

        // validate common inputs & selects
        let validate = {
            'typeId': 'You must choose business type',
            'name': 'You must write business name',
            'stateId': 'You must choose a state/province',
            'locationId': 'You must choose a location',
            'address1': 'You must write an address',
            'zipCode': 'You must include your zipcode',
        };
        let [errors, errorsCount] = form.validateErrors(validate);
        this.setState({
            errors: errors
        });
        if (errorsCount) {
            ok = false;
        }

        // validate photos
        let picturesError = form.validatePictures(this.state.pictures.length)
        if (picturesError) {
            this.setState({
                picturesError: picturesError
            });
            ok = false;
        }

        // validate phone
        let phoneError = form.validatePhone();
        if (phoneError) {
            this.setState({
                phoneError: phoneError,
                phoneResult: false,
            });
            ok = false;
        }

        let emailError = form.validateEmail(this.state.place.email);
        if (emailError) {
            this.setState({
                errorEmail: emailError
            });
            ok = false;
        }

        return ok;
    }

    /**
     * Submit form
     * @param event
     * @returns {Promise<boolean>}
     * Frontend/Open/Places/Presentation/Api/Rest/Version1/PlacesController.php
     * src/Places/Application/PlaceCommand.php
     */
    submit = async event => {
        event.preventDefault();

        this.setState({
            errors: {},
            picturesError: false,
            errorEmail: false,
        });

        // validate form inputs & selects
        if (!this.validate()) {
            return false;
        }

        this.setState({
            loading: true,
            errorMessage: false,
            successMessage: false,
        });

        let postData = {
            // set general data
            'locationId': this.state.place['locationId'],
            'name': this.state.place['name'],
            'typeId': this.state.place['typeId'],
            'webSite': this.state.place['webSite'],

            // set complex data
            'address': {
                address1: this.state.place['address1'],
                address2: this.state.place['address2'],
                zipCode: this.state.place['zipCode'],
            },
            'contactDetails': {
                phone1: this.state.phone,
                email: this.state.place['email'],
            },
            'socialNetworks': {
                facebook: this.state.place['facebook'],
                twitter: this.state.place['twitter'],
                instagram: this.state.place['instagram'],
                wechat: this.state.place['wechat'],
                telegram: this.state.place['telegram'],
                whatsapp: this.state.place['whatsapp'],
            },

            // append image
            'image': this.state.pictures[0].name,
        };

        // set attributes with keys from db
        if (Object.keys(this.state.attributes).length) {
            let attributes = [];
            for (let key in this.state.attributes) {
                let attr = {
                    'attribute_id': this.state.attributesKeys[key],
                    'value': parseInt(this.state.attributes[key]),
                }
                attributes.push(attr);
            }
            postData['placeAttributes'] = attributes;
        }

        config.debug('postData', postData);

        let errorText = 'Error adding place, please try again later';
        let successText = 'Thank you for submitting the place. It will be live soon.';
        if (this.state.placeOwner) {
            errorText = 'Error updating place, please try again later';
            successText = 'Your place successfully updated. Please allow some time for site updates.';
        }

        try {
            if (this.state.placeOwner) {
                // UPDATE PLACE
                await axios.put(`/api/v1/pc/places/${this.state.place.id}`, postData);

                // delete old info from local
                await db.places.where('id').equals(this.state.place.id).delete();
                await db.place_working_hours.where('id').equals(this.state.place.id).delete();

                this.setState({
                    loading: false,
                    successMessage: successText
                });
            } else {
                // ADD PLACE
                const res = await axios.post('/api/v1/places', postData);

                if (res.data.id) {
                    this.setState({
                        loading: false,
                        successMessage: successText
                    });
                } else {
                    this.setState({
                        loading: false,
                        errorMessage: errorText
                    });
                }
            }
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: errorText
            });
        }
    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount()
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getCity();

        this.setState({
            countries: await locations.getCountriesWithState(), // all countries + states
            listType: await this.getListType(),
            listCountry: await this.getListCountry(),
            listHoursFrom: await this.getListHoursFrom(),
            listHoursTo: await this.getListHoursTo(),
        });

        await this.getPlace();

        // auto select us by default
        if (!this.state.place.id) {
            await this.getStatesByDefault();
        }

        // bind accordeon actions
        placeHours();

        this.setState({loading: false});
    }

    /**
     * Get place for edit by owner
     * */
    getPlace = async () => {
        if (this.state.placeId && this.state.account.id) {
            let place = await places.getById(this.state.placeId);

            if (this.state.account.id === place.ownerId) {
                let city = await cities.getById(place.locationId);

                // append locatiosn info to place
                place['countryId'] = city.countryId;
                place['stateId'] = city.stateId;

                // get cities list
                await this.getStates(city.countryId);
                await this.getCities(city.stateId);

                // parse images list
                let pictures = [];
                [place.image].map(image =>
                    {
                        let item = {
                            id: 'local', // to delete only in state
                            name: image,
                            url: `${config.getImagePath('place')}/${image}`,
                        }
                        pictures.push(item);
                        return null;
                    }
                )

                let hours = await places.getWorkingHours(place.id);

                // assign hours to attributes & show hours selects
                let attributes = [];
                hours.map(hour => {
                    let dayName = hour.dayOfWeek.toLowerCase();
                    let dayFrom = `${dayName}_from`;
                    let dayTo = `${dayName}_to`;

                    let fromValue = (hour.from.length === 6)? `0${hour.from}` : hour.from;
                    let toValue = (hour.to.length === 6)? `0${hour.to}` : hour.to;

                    let from = config.getHoursFrom(false, fromValue);
                    let to = config.getHoursTo(false, toValue);

                    // save for attributes state
                    attributes[dayFrom] = from;
                    attributes[dayTo] = to;

                    // insert text to labels
                    this.setLabel(dayName, {
                        [dayFrom]: from,
                        [dayTo]: to,
                    })

                    // show day hours
                    document.getElementById(`${dayName}_show`).checked = true;
                    document.getElementById(`collapse-${dayName}`).classList.add('show');
                })

                config.debug('place', place);
                config.debug('city', city);
                config.debug('hours', hours);
                config.debug('attributes', attributes);

                this.setState({
                    placeOwner: true,
                    place: place,
                    phone: place.phone1,
                    pictures: pictures,
                    picturesLimit: true,
                    attributes: attributes,
                });
            }
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    render() {
        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}
          {this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

          <div className="content add-business">

              {!this.state.successMessage &&
                <form method="post" action="#" onSubmit={this.submit}>

                  {/*GENERAL INFO*/}
                  <div className="wrapper-container bg-white m-b-5 p-y-20 wrapper-add-business-container">
                      <div className="container">
                          <div className="add-business-container">
                              <h2 className="color-primary m-b-15 weight-700">Business Details</h2>

                              <Select
                                classes={'m-b-15'}
                                label={'Business Type'}
                                disabledOption={"Please choose"}
                                id={'typeId'}
                                name={'typeId'}
                                data={this.state.listType}
                                //required={true}
                                required_icon={true}
                                error_message={this.state.errors.typeId}
                                handleChange={this.fillForm}
                                value={this.state.place.typeId}
                              />

                              <Input
                                classes={'p-b-0'}
                                label={'Business Name'}
                                type={'text'}
                                id={'name'}
                                name={'name'}
                                placeholder={'Enter business name here'}
                                error={this.state.errors.name}
                                //required={true}
                                required_icon={true}
                                maxCharacters={100}
                                handleChange={this.fillForm}
                                value={this.state.place.name}
                              />
                          </div>
                      </div>
                  </div>

                  {/*LOCATION*/}
                  <div className="wrapper-container bg-white m-b-5 p-y-20 wrapper-add-business-container">
                      <div className="container">
                          <div className="add-business-container">
                              <h2 className="color-primary m-b-15 weight-700">Location</h2>

                              <Select
                                classes={'m-b-15'}
                                label={'Country'}
                                disabledOption={"Please choose"}
                                id={'countryId'}
                                name={'Country'}
                                data={this.state.listCountry}
                                //required={true}
                                required_icon={true}
                                error_message={this.state.errors.countryId}
                                handleChange={this.getStates}
                                value={this.state.place.countryId}
                              />

                              <Select
                                classes={'m-b-15'}
                                label={'State'}
                                disabledOption={"Please choose"}
                                id={'stateId'}
                                name={'state'}
                                data={this.state.listState}
                                disabled={this.state.listState.length === 0}
                                //required={true}
                                required_icon={true}
                                error_message={this.state.errors.stateId}
                                handleChange={this.getCities}
                                value={this.state.place.stateId}
                              />

                              <Select
                                classes={'m-b-15'}
                                label={'City'}
                                disabledOption={"Please choose"}
                                id={'locationId'}
                                name={'locationId'}
                                data={this.state.listCity}
                                disabled={this.state.listCity.length === 0}
                                //required={true}
                                required_icon={true}
                                error_message={this.state.errors.locationId}
                                handleChange={this.fillForm}
                                value={this.state.place.locationId}
                              />

                              <Input
                                classes={'p-b-15'}
                                label={'Address 1'}
                                type={'text'}
                                id={'address1'}
                                name={'address1'}
                                placeholder={'Enter business address here'}
                                error={this.state.errors.address1}
                                //required={true}
                                required_icon={true}
                                maxCharacters={100}
                                handleChange={this.fillForm}
                                value={this.state.place.address1}
                              />

                              <Input
                                classes={'p-b-15'}
                                label={'Address 2'}
                                type={'text'}
                                id={'address2'}
                                name={'address2'}
                                placeholder={'Enter additional address here'}
                                error={this.state.errors.address2}
                                maxCharacters={100}
                                handleChange={this.fillForm}
                                value={this.state.place.address2}
                              />

                              <Input
                                classes={'p-b-0'}
                                label={'Zipcode'}
                                type={'text'}
                                id={'zipCode'}
                                name={'zipCode'}
                                placeholder={'Enter zipcode here'}
                                error={this.state.errors.zipCode}
                                //required={true}
                                required_icon={true}
                                handleChange={this.fillForm}
                                value={this.state.place.zipCode}
                                //attributes={{'min': 4,'max': 6,'inputMode': 'numeric','pattern': '\d*'}}
                                //maxCharacters={6}
                                //minCharacters={4}
                              />

                          </div>
                      </div>
                  </div>

                  {/*CONTACT INFO*/}
                  <div className="wrapper-container bg-white m-b-5 p-y-20 wrapper-add-business-container">
                      <div className="container">
                          <div className="add-business-container">
                              <h2 className="color-primary weight-700">Contact Details</h2>

                              <FormPhone
                                  required={true}
                                  classes={'place-phone'}
                                  value={this.state.phone}
                                  label={'Phone number'}
                                  labelRequired={true}
                                  phoneValid={this.state.phoneValid}
                                  phoneResult={this.state.phoneResult}
                                  phoneError={this.state.phoneError}
                                  handleBlur={this.phoneVerify}
                              />

                              <Input
                                classes={'p-b-15'}
                                label={'Email'}
                                type={'email'}
                                id={'email'}
                                name={'email'}
                                placeholder={'Enter business email here'}
                                error={this.state.errorEmail}
                                //required={true}
                                required_icon={true}
                                maxCharacters={100}
                                handleChange={this.fillForm}
                                value={this.state.place.email}
                              />

                              <Input
                                classes={'p-b-0'}
                                label={'Website'}
                                type={'url'}
                                id={'webSite'}
                                name={'webSite'}
                                placeholder={'Enter business website here'}
                                error={this.state.errors.webSite}
                                maxCharacters={100}
                                handleChange={this.fillForm}
                                value={this.state.place.webSite}
                              />
                          </div>
                      </div>
                  </div>

                  {/*SOCIALS*/}
                  <FormSocial handleChange={this.fillForm} values={this.state.place}/>

                  {/*PHOTOS*/}
                  <FormPhoto
                    type={'place'}
                    pictures={this.state.pictures}
                    picturesMax={this.state.picturesMax}
                    picturesLimit={this.state.picturesLimit}
                    picturesError={this.state.picturesError}
                    uploadHandle={this.uploadPhoto}
                    deleteHandle={this.deletePhoto}
                  />

                  {/*HOURS*/}
                  <div className="wrapper-container bg-white p-t-20 p-b-0 wrapper-add-business-container">
                      <div className="container">
                          <div className="add-business-container">
                              <h2 className="color-primary m-b-15 weight-700">Business Hours</h2>

                              <div className="wrapper-accordion-business-hours">

                                  {this.state.listDays.map((day, index) => {
                                        let key = index + 1; // day index, 1-7
                                        let dayName = day.toLowerCase();
                                        return (
                                          <div key={index} className={`accordion-business-hours-row ${index !== 6 ? 'm-b-15' : ''}`}>

                                              <div className="accordion-header p-b-15 d-flex align-items-center justify-content-between">
                                                  <div className="left-side">
                                                      <Checkbox
                                                        classes={'daytoggle'}
                                                        label={day}
                                                        id={`${dayName}_show`}
                                                        name={`${dayName}_show`}
                                                        value={''}
                                                        dataAttrs={{'data-day': dayName}}
                                                        handleChange={this.toggleDay}
                                                        //checked={}
                                                      />
                                                  </div>
                                                  <div className="right-side">
                                                      <h3 id={`${dayName}_label`} className={`text-uppercase day_label`} data-toggle="collapse" data-target={`#collapse${day}`} aria-expanded="true">
                                                          <span id={`${dayName}_from_label`}></span> - <span id={`${dayName}_to_label`}></span>
                                                      </h3>
                                                  </div>
                                              </div>

                                              <div className={`accordion-body collapse collapse-${dayName}`} id={`collapse-${dayName}`} data-parent=".wrapper-accordion-business-hours">
                                                  <div className="selects-row p-b-15 d-flex align-items-center justify-content-between">
                                                      <div className="left-side m-r-5">
                                                          {/*DATE FROM*/}
                                                          <Select
                                                            classes={'m-b-0 dayoc'}
                                                            label={'From'}
                                                            disabledOption={"--don't know--"}
                                                            id={`${dayName}_from`}
                                                            name={`${dayName}_from`}
                                                            data={this.state.listHoursFrom}
                                                            dataAttrs={{'data-day': dayName}}
                                                            handleChange={this.fillHours}
                                                            value={this.state.attributes[`${dayName}_from`]}
                                                          />
                                                      </div>
                                                      <div className="right-side m-l-5">
                                                          {/*DATE TO*/}
                                                          <Select
                                                            classes={'m-b-0 dayoc'}
                                                            label={'To'}
                                                            disabledOption={"--don't know--"}
                                                            id={`${dayName}_to`}
                                                            name={`${dayName}_to`}
                                                            data={this.state.listHoursTo}
                                                            dataAttrs={{'data-day': dayName}}
                                                            handleChange={this.fillHours}
                                                            value={this.state.attributes[`${dayName}_to`]}
                                                          />
                                                      </div>
                                                  </div>
                                                  <div className="all-day p-b-15 d-flex align-items-center justify-content-end">
                                                      <Checkbox
                                                        checkboxClasses={'allday'}
                                                        label={'24 hours?'}
                                                        id={`${dayName}_allday`}
                                                        name={`${dayName}_allday`}
                                                        dataAttrs={{'data-day': dayName}}
                                                        handleChange={this.toggleDayAll}
                                                      />
                                                  </div>
                                              </div>

                                          </div>
                                        )
                                    }
                                  )}

                              </div>

                          </div>
                      </div>
                  </div>


                  <div className="wrapper-ad-creation-container bg-white p-y-15">
                      <div className="container">
                          <div className="ad-creation-container">
                              <ButtonSubmit text={'Save'}/>
                          </div>
                      </div>
                  </div>

              </form>
              }

              {this.state.successMessage &&
                  <div className="container bg-white p-y-20">
                    <ButtonSubmit text={`Return to ${this.state.city.name}`} link={this.state.city.url}/>
                  </div>
              }

              <LoadingSpinner loading={this.state.loading} />
          </div>
          </>
        )
    }
}
