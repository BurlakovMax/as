import React, {Component} from 'react';
import Steps from "./steps";
import ButtonSubmit from "../../components/form/button-submit";
import Select from "../../components/form/select";
import axios from '../../services/api_init';
import Summary from "./summary";
import Checkbox from "../../components/form/checkbox";
import Alert from "../../components/common/alert";
import LoadingSpinner from "../../components/common/loading-spinner";
import adbuild from "../../lib/adbuild";
import auth from "../../lib/auth";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import {history} from "../../index";
import locations from "../../services/locations";

export default class Step2 extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            account: {},
            token: false,
            loading: false,

            errorMessage: false,
            errorLocation: false,

            countries: [],
            locations: [],
            price: 0,
            summary: {},
            typeId: 0,
            limit: false,
            limitError: false,
        };
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    getPrice = async () => {
        try {
            const res = await axios.get('/api/v1/escorts/price');

            this.setState({
                price: res.data
            });
        } catch (error) {
            config.catch(error);
        }
    }

    /**
     * Get list of cities from api by selected state
     */
    getCitiesBySelect = async (event) => {
        event.preventDefault();

        let stateId = event.target.value;
        await this.getCities(stateId);
    }

    /**
     * Get list of cities from default selected option
     */
    getCitiesById = async (stateId) => {
        await this.getCities(stateId);
    }

    getCities = async (stateId) => {
        this.setState({loading: true});

        let cities = await locations.getCitiesByStateId(stateId);

        this.setState({
            locations: cities,
        });

        this.setState({loading: false});

        // print cities html in summary
        this.printCities();
    }

    /**
     * Print list of cities with checkboxes
     * @returns {*[]}
     */
    printCities = () => {

        if (this.state.locations.length > 0) {
            return (
              this.state.locations.map((item, key) =>
                {
                    return (
                      <div id={`_city${item.id}`} className="cities" key={key}>
                          <label htmlFor={`loc_${item.id}`}>
                          <div className={'row-checkbox-price d-flex align-items-center justify-content-between'}>
                              <div className="column">
                                  <Checkbox
                                    classes={'pickloc'}
                                    label={item.name}
                                    id={`loc_${item.id}`}
                                    value={item.id}
                                    name={'loc_id'}
                                    handleChange={this.toggleCity}
                                    htmlFor={false}
                                    checked={this.state.summary[item.id] !== undefined}
                                  />
                              </div>
                              <div className="column">
                                <span className="weight-600">
                                  {this.state.price} USD
                                </span>
                              </div>
                          </div>
                          </label>
                      </div>
                    )
                }
              )
            );
        }
    }

    // update summary locations list
    updateSummary = (id, action) => {
        let summary = this.state.summary;

        if (action === 'add') {
            let city = this.state.locations[id];
            city.price = this.state.price;
            city.state = city.stateName;
            city.upgrade = '';
            summary[id] = city;
        } else {
            delete summary[id];
        }

        this.setState({
            summary: summary,
        });

        config.debug('SUMMARY', summary);
    }

    /**
     * Add/Remove city (by checkbox)
     * @param event
     * @returns {Promise<void>}
     */
    toggleCity = (event) => {
        let locId = event.currentTarget.id;
        let id = locId.replace('loc_', '');
        let action = (event.currentTarget.checked)? 'add' : 'remove';

        this.setState({
            errorLocation: false
        });

        // check limit
        if (this.state.limit && action === 'add') {
            if (Object.keys(this.state.summary).length >= this.state.limit) {
                this.setState({
                    errorLocation: this.state.limitError
                });
                event.currentTarget.checked = false;
                return false;
            }
        }

        this.updateSummary(id, action);
    }

    /**
     * Remove city in summary (by close icon)
     * @param event
     * @returns {Promise<void>}
     */
    removeCity = (event) => {
        event.preventDefault();

        let id = event.currentTarget.dataset.id;

        // try to uncheck top states list
        let checkbox = document.getElementById('loc_'+id);
        if (checkbox) {
            checkbox.checked = false;
        }

        this.updateSummary(id, 'remove');
    }

    /**
     * Submit form & go to next step
     * @param event
     * @returns {Promise<boolean>}
     */
    submit = async (event) => {
        event.preventDefault();

        // check for any city selected
        let ids = [];
        for (let key in this.state.summary) {
            let item = this.state.summary[key];
            ids.push(item.id)
        }

        if(!ids.length) {
            this.setState({
                errorLocation: 'A location is required.'
            });
            return false;
        } else {
            adbuild.saveCookieLocations(this.state.summary, this.state.price);
            history.push(auth.getLink('adbuild3'));
        }
    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount(true)
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        this.setState({
            countries: await locations.getCountriesWithState('step2')
        });

        await this.getPrice();

        // print cities for default selected option
        let nav = document.getElementById('navigation');
        if (nav.value && nav.value > 0) {
            await this.getCitiesById(nav.value);
        }

        let limit = adbuild.getCitiesLimit(this.state.account);
        this.setState({
            limit: limit.limit,
            limitError: limit.error,
        });

        this.setState({loading: false});
    }

    render() {

        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

          <div className="content ad-creation location" id="step-2-page">
            <form method="get" onSubmit={this.submit} action="#" id="select-location-for-ad__">

              <div className="bg-white m-b-10 p-y-20 p-b-10">
                  <Steps
                    steps={['Category','Your Location','About Ad','Checkout']}
                    active={2}
                  />
              </div>

              <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                  <div className="container">
                      <div className="ad-creation-container">
                          <h2 className="weight-700 color-primary text-center p-b-15">Select Location(s) For Your Ad</h2>

                          <ul className="ul-list p-0 list-position-inside custom-style-bullet m-b-20">
                              <li>
                                  All prices are for a 30-day period. Payments are recurring, so will charge you every 30
                                  days for a new 30 days period. You can cancel this subscription anytime (for example
                                  right after posting an ad - so your payment will effectively be non-recurring).
                              </li>
                              {this.state.limit &&
                                  <li>
                                      You can choose <span className={'weight-600'}>maximum {this.state.limit} location{this.state.limit > 1 && 's'}</span>.
                                  </li>
                              }
                              {!this.state.limit &&
                                  <li>
                                      You can choose multiple locations in states/countries from the dropdown.
                                  </li>
                              }
                              <li>
                                  You can add visiting dates for chosen locations in the next section.
                              </li>
                          </ul>

                          <Select
                            data={this.state.countries}
                            label={'Location'}
                            classes={'select-location'}
                            disabledOption={'Please choose'}
                            id={'navigation'}
                            name={'navigation'}
                            required={true}
                            required_icon={true}
                            handleChange={this.getCitiesBySelect}
                          />

                          {this.state.errorLocation &&
                            <div className="color-error m-b-10">{this.state.errorLocation}</div>
                          }

                          <div className="checkbox-price">
                              {this.printCities()}
                          </div>

                      </div>
                  </div>
              </div>

              <div className="wrapper-ad-creation-container bg-white p-y-15">
                  <div className="container">
                      <div className="ad-creation-container">

                          <div id="calculator" className="m-b-15">
                              <Summary locations={this.state.summary} handleClick={this.removeCity}/>
                          </div>

                          <ButtonSubmit text={'Continue'}/>

                      </div>
                  </div>
              </div>

            </form>
          </div>

          <LoadingSpinner loading={this.state.loading} />
          </>
        )
    }
}
