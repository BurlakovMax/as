import React from 'react';
import DayPicker, {DateUtils} from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import ButtonBordered from "../../components/form/button-bordered";
import ButtonSubmit from "../../components/form/button-submit";

export default class Visiting extends React.Component {
  static defaultProps = {
    numberOfMonths: 2,
  };

  constructor(props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      from: undefined,
      to: undefined,
    };
  }

  handleDayClick(day) {
    const range = DateUtils.addDayToRange(day, this.state);
    this.setState(range);

/*    let from = new Date(range.from).toLocaleDateString('en-US');
    let to = new Date(range.to).toLocaleDateString('en-US');*/
  }

  handleResetClick() {
    this.setState(this.getInitialState());
  }

  render() {
    const {from, to} = this.state;
    const modifiers = {start: from, end: to};

    // hidden inputs value
    let fromSave = (from)? new Date(from).toLocaleDateString('en-US') : '';
    let toSave = (to)? new Date(to).toLocaleDateString('en-US') : '';
    let visitingValue = (fromSave && toSave)? 1 : 0;
    let visitingButton = document.getElementById('visiting_button');

    // change button text
    if (visitingButton) {
      if (visitingValue) {
        visitingButton.innerHTML = fromSave + ' - ' + toSave;
      } else {
        visitingButton.innerHTML = 'Select dates';
      }
    }

    return (
      <div className="date_picker">
        <DayPicker
          className="Selectable"
          numberOfMonths={this.props.numberOfMonths}
          selectedDays={[from, {from, to}]}
          modifiers={modifiers}
          onDayClick={this.handleDayClick}
        />

        <div className={'row'}>
          <div className={'col-6'}>
            <ButtonBordered type={'button'} text={'Reset'} handleClick={this.handleResetClick}/>
          </div>
          <div className={'col-6'}>
            <ButtonSubmit type={'button'} text={'Save'} data={{'data-dismiss' : 'modal'}}/>
          </div>
        </div>

        <input type="hidden" id="visitingFrom" name="visitingFrom" value={fromSave}/>
        <input type="hidden" id="visitingTo" name="visitingTo" value={toSave}/>
        <input type="hidden" id="isVisiting" name="isVisiting" value={visitingValue}/>
      </div>
    );
  }
}
