import React, {Component} from 'react';
import adbuild from "../../lib/adbuild";

export default class Summary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      typeId: 0,
      typeName: '',
    };
  }

  async componentDidMount() {
    let data = adbuild.loadCookie();
    this.setState({
      typeId: data.type.typeId,
      typeName: data.type.typeName,
    });

    // redirect to step1 if no valid type
    if (this.props.step === 3 && (!data.type.typeId || !data.type.typeName)) {
      window.location = '/adbuild'
    }
  }

  parseLocations = () => {
    let total = 0;
    let data = {
      total: 0,
      locations: []
    };

    for (let key in this.props.locations) {
      data.locations.push(this.props.locations[key]);
      total += parseFloat(this.props.locations[key].price);
    }

    data.total = total.toFixed(2);

    return data;
  }

  render() {
    let readonly = this.props.readonly || false;
    let data = this.parseLocations();

    return (
      <div className="ad-creation-summary" id="absa">

        <h2 className="weight-700 color-primary m-b-25">Summary</h2>
        {this.state.typeName ?
            (<p className="p-b-10">{this.state.typeName}, 30 Days</p>)
            :
            (<p className="p-b-10 color-error">Type not found</p>)
        }

        <input type={'hidden'} name={'type'} value={this.state.typeId}/>

        {data.locations.map((item, index) =>
          <div className="row-summary d-flex align-items-center justify-content-between p-y-10" key={index}>

            {!item.upgrade && <input type={'hidden'} name={'locationIds[]'} value={item.id}/>}

            <div className="column">
              <span>
                  {item.name}, {item.state}
              </span>
            </div>
            <div className="column d-flex align-items-center">
                <span className={`row-summary__price weight-600 lh-normal ${readonly? '' : 'm-r-20'}`}>
                  $ {item.price}
                </span>
                {!readonly &&
                  <div className="remove-row">
                      <button
                          type="button"
                          className="remove-button p-0 m-0 cart"
                          onClick={this.props.handleClick}
                          data-upgrade={(item.upgrade)? item.upgrade : ''}
                          data-id={item.id}
                          rel={item.id}
                      >
                        <span className="icon color-primary">
                          <svg className="icon_close">
                              <use xlinkHref="/images/icons.svg#icon_close"></use>
                          </svg>
                        </span>
                      </button>
                  </div>
                }
            </div>
          </div>
        )}

        {data.total > 0 &&
          <p className="m-t-20 text-right">
            Total <span className="weight-600">$ {data.total}</span>
          </p>
        }
      </div>

    )
  }
}

