import React, {Component} from 'react';
import Steps from "./steps";
import Radio from "../../components/form/radio";
import ButtonSubmit from "../../components/form/button-submit";
import Alert from "../../components/common/alert";
import axios from '../../services/api_init';
import LoadingSpinner from "../../components/common/loading-spinner";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import {history} from "../../index";
import auth from "../../lib/auth";
import adbuild from "../../lib/adbuild";
import {offline} from '../../store/common';
import Offline from "../../components/text/offline";

export default class Step1 extends Component {
  constructor(props) {
    super(props);

    changeTitle(config.getTitle(this));

    this.state = {
      errorMessage: false,
      loading: false,

      typeId: 0,
      typeName: '',
      typeSlug: '',

      types: [],
    };
  }

  closeAlert = () => {
    this.setState({
      errorMessage: null
    });
  }

  getTypes = async () => {
    this.setState({
      loading: true
    });

    try {
      const res = await axios.get('/api/v1/escorts/types');

      let list = [];

      // parse list from api
      res.data.map((item, index) =>
        {
          let id = Object.keys(item)[0];
          list.push({
            'id': 'section-for-ad',
            'name': 'type',
            'title': item[id].name,
            'dataAttrs': {'data-name':item[id].name, 'data-slug':item[id].slug},
            'value': id,
            'required': true,
          })
          return null;
        }
      )

      this.setState({
        types: list,
        loading: false,
      });
    } catch (error) {
      config.catch(error);
    }
  }

  checkType = (event) => {
    this.setState({
      typeId: event.currentTarget.value,
      typeName: event.currentTarget.dataset.name,
      typeSlug: event.currentTarget.dataset.slug,
    });
  }

  /**
   * Submit form & go to next step
   * @param event
   * @returns {Promise<boolean>}
   */
  submit = async (event) => {
    event.preventDefault();

    if (!this.state.typeId) {
      this.setState({
        errorMessage: 'Please select one of types'
      });

      return false;
    }

    let type = {
      typeId: this.state.typeId,
      typeName: this.state.typeName,
      typeSlug: this.state.typeSlug
    };
    adbuild.saveCookieType(type);

    history.push(auth.getLink('adbuild2'));
  }

  async componentDidMount() {
    await this.getTypes();
  }

  render() {

    return (
      <>
        {offline.getState() ? (
          <Offline/>
        ) : (
          <>
            {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

            <div className="content ad-creation agreement" id="step-1-page">
              <form method="get" onSubmit={this.submit} action="#">

                <div className="bg-white m-b-10 p-y-20 p-b-10">
                  <Steps
                      steps={['Type','Your Location','About Ad','Checkout']}
                      active={1}
                  />
                </div>

                <div className="wrapper-ad-creation-container bg-white color-secondary p-t-15 p-b-25 m-b-10">
                  <div className="container">
                    <div className="ad-creation-container">

                      <h2 className="weight-700 color-primary text-center p-b-20">Select a section for your ad</h2>

                      <Radio
                          classes={'m-b-25'}
                          data={this.state.types}
                          handleClick={this.checkType}
                      />

                      <ButtonSubmit text={'Continue'}/>

                    </div>
                  </div>
                </div>
              </form>
            </div>
          </>
        )
        }
        <LoadingSpinner loading={this.state.loading} />
      </>
    )
  }
}
