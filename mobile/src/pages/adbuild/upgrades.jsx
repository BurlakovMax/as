import React, {Component} from 'react';
import Checkbox from "../../components/form/checkbox";
import axios from "axios";
import CheckboxSecondary from "../../components/form/checkbox-secondary";
import config from "../../lib/config";
import Alert from "../../components/common/alert";

export default class Upgrades extends Component {
  constructor(props) {
    super(props);

    this.state = {
      summary: {}, // from props

      typeId: 0,
      locations: [],
      locationsIds: [],

      openHome: true,
      homePrice: 0,
      homeData: [],

      openSide: true,
      sidePrice: 0,
      sideData: [],

      openSticky: true,
      stickyPrice: 0,
      stickyPriceAgency: {},
      stickyData: [],
    };
  }

  toggleHome = (event) => {
    this.setState({
      openHome: !this.state.openHome
    });
  }

  toggleSide = (event) => {
    this.setState({
      openSide: !this.state.openSide
    });
  }

  toggleSticky = (event) => {
    this.setState({
      openSticky: !this.state.openSticky
    });
  }


  getHomePrice = async () => {
    try {
      const res = await axios.get(`/api/v1/escorts/escort_sponsor_price`);

      this.setState({
        homePrice: res.data
      });
    } catch (error) {
      config.catch(error);
    }
  }

  getHomeAvailable = async () => {
    try {
      const res = await axios.get(`/api/v1/escorts/can_buy_escort_sponsor`,
        {
          params: {
            locationIds: this.state.locationsIds,
            type: this.state.typeId,
          }
        }
      );

      // parse api response
      let available = {};
      res.data.map(item =>
        available[item.id] = item.available
      )

      let data = [];
      this.state.locations.map(item =>
        {
          data.push({
            'loc_id' : item.id,
            'loc_name' : item.name,
            'loc_state' : item.state,
            'available' : available[item.id],
          })
          return null;
        }
      )
      this.setState({
        homeData: data
      });

    } catch (error) {
      config.catch(error);
    }
  }

  getStickyPrice = async () => {
    try {
      const res = await axios.get(`/api/v1/escorts/escort_sticky_price`,{
        params: {
          accountId: (typeof this.props.account === 'object')? this.props.account.id : '',
          locationIds: this.state.locationsIds,
          type: this.state.typeId,
        }
      });

      // parse api response
      let prices = {};
      for (let price of res.data.list) {
        prices[price.locationId] = price;
      }
      this.setState({
        stickyPriceAgency: prices
      });
    } catch (error) {
      config.catch(error);
    }
  }

  getStickyAvailable = async () => {
    try {
      const res = await axios.get(`/api/v1/escorts/can_buy_escort_sticky`,
        {
          params: {
            locationIds: this.state.locationsIds,
            type: this.state.typeId,
          }
        }
      );

      // parse api response
      let available = {};
      res.data.map(item =>
        available[item.id] = item.available
      )

      let data = [];
      this.state.locations.map(item =>
        {
          data.push({
            'loc_id' : item.id,
            'loc_name' : item.name,
            'loc_state' : item.state,
            'available' : available[item.id],
          })
          return null;
        }
      )
      this.setState({
        stickyData: data
      });

    } catch (error) {
      config.catch(error);
    }
  }

  getSidePrice = async () => {
    try {
      const res = await axios.get(`/api/v1/escorts/escort_side_price`);

      this.setState({
        sidePrice: res.data
      });
    } catch (error) {
      config.catch(error);
    }
  }

  getSideAvailable = async () => {
    try {
      const res = await axios.get(`/api/v1/escorts/can_buy_escort_side`,
        {
          params: {
            locationIds: this.state.locationsIds,
            type: this.state.typeId,
          }
        }
      );

      // parse api response
      let available = {};
      res.data.map(item =>
        available[item.id] = item.available
      )

      let data = [];
      this.state.locations.map(item =>
        {
          data.push({
            'loc_id' : item.id,
            'loc_name' : item.name,
            'loc_state' : item.state,
            'available' : available[item.id],
          })
          return null;
        }
      )
      this.setState({
        sideData: data
      });

    } catch (error) {
      config.catch(error);
    }
  }

  /**
   locations: {
    id: 1279
    name: "Mobile"
    price: "9.99"
    state: "Alabama"
    upgrade: false
   }
   */
  async getData() {
    this.setState({loading: true});

    // take only cities, w/o upgrades
    let locations = [];
    let locationsIds = [];

    for (let key in this.state.summary) {
      let item = this.state.summary[key];
      if (!item.upgrade) {
        locations.push(item);
        locationsIds.push(item.id);
      }
    }

    this.setState({
      locations: locations,
      locationsIds: locationsIds,
    });

    await this.getSidePrice();
    await this.getSideAvailable();

    await this.getHomePrice();
    await this.getHomeAvailable();

    // only for existing escort
    await this.getStickyPrice();
    await this.getStickyAvailable();

    this.setState({loading: false});
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let update = {};

    if (nextProps.summary !== []) {
      update.summary = nextProps.summary;
    }

    if (nextProps.typeId) {
      update.typeId = nextProps.typeId;
    }

    return update;
  }

  async componentDidUpdate(prevProps, prevState) {
    if(prevProps.summary !== this.props.summary) {
      this.setState({
        summary: this.props.summary
      });
      await this.getData();
    }
  }

  async componentDidMount() {
    if (this.props.action === 'step3') {
      await this.getData();
    }
  }

  render() {
    let showHome = (this.state.homePrice && this.state.homeData.length)? true : false;
    let showSide = (this.state.sidePrice && this.state.sideData.length)? true : false;
    let showSticky = (this.props.account.agency || parseInt(this.state.typeId) === 2)? true : false;

    if (!showHome && !showSide && !showSticky) {
      return '';
    }

    if (this.state.loading) {
      return (
        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10 text-center color-primary">
          Loading upgrades ...
        </div>
      );
    }

    return (
      <div className="wrapper-ad-creation-container bg-white p-t-15 m-b-10">

        <div className="container">
          <div className="ad-creation-container">
            <h2 className="weight-700 color-primary p-b-10">
              Upgrades
            </h2>
          </div>
        </div>


        {showHome &&
          <div className={`ad-creation-upgrade ${this.state.openHome? 'ad-creation-upgrade_opened' : ''}`}>
          <div className="container">

            <div className="ad-creation-upgrade_controls">
              <div onClick={this.toggleHome} className="ad-creation-upgrade_title d-flex align-items-center justify-content-between">
                Be A Cover Star
                <span className="price p-x-15 p-y-10 m-l-10 weight-700 lh-normal color-white">
                    ${this.state.homePrice}/mo
                </span>
              </div>

              <div className="ad-creation-upgrade_options">
                {this.state.homeData.map((home, index) =>
                  home.available ?
                    (
                      <label key={index} className="ad-creation-upgrade_options-item d-flex align-items-center justify-content-between p-y-15 p-x-10 m-y-0">
                        <Checkbox
                          label={`${home.loc_name} $${this.state.homePrice}/month`}
                          classes={'min-width-0 lh-normal'}
                          id={`${config.getEscortUpgradeType('sponsor')}_${home.loc_id}`}
                          name={`homesp${home.loc_id}`}
                          value={home.loc_id}
                          checked={home.checked}
                          required={false}
                          textWrap={1}
                          dataAttrs={{
                            'data-price': this.state.homePrice,
                            'data-name': 'City thumbnail in '+home.loc_name,
                            'data-state': home.loc_state,
                            'data-upgrade': config.getEscortUpgradeType('sponsor'), // this param goes to url for payment
                          }}
                          handleChange={this.props.handleChange}
                        />
                      </label>
                    ) : (
                      <label key={index} className="ad-creation-upgrade_options-item d-flex align-items-center justify-content-between p-y-15 p-x-10 m-y-0">
                        <Checkbox
                        label={`City thumbnail in ${home.loc_name} not available - sold out`}
                        classes={'min-width-0 lh-normal color-error'}
                        disabled={true}
                        textWrap={1}
                        />
                      </label>
                    )
                )}
              </div>
            </div>

            <div className="ad-creation-upgrade_description">
              <div className="ad-creation-upgrade_image">
                <img src="/images/adbuild/m_sp_home.png" alt=""/>
                <div className="ad-creation-upgrade_your-ad-here __top30">
                  Your AD
                  <br/>
                  Here!
                </div>
              </div>
              <p className="weight-700">
                Be A Cover Star for ${this.state.homePrice} a month
              </p>
              <p>
                Be the viewer's first fruit! Post your photo on your city's cover for ${this.state.homePrice} a month.
              </p>
              <p>
                Post your photo and link on your city's cover page
              </p>
            </div>
          </div>
        </div>
        }


        {showSide &&
        <div className={`ad-creation-upgrade ${this.state.openSide? 'ad-creation-upgrade_opened' : ''}`}>
          <div className="container">
            <div className="ad-creation-upgrade_controls">
              <div onClick={this.toggleSide} className="ad-creation-upgrade_title d-flex align-items-center justify-content-between">
                Appear on every page
                <span className="price p-x-15 p-y-10 m-l-10 weight-700 lh-normal color-white">
                    ${this.state.sidePrice}/mo
                </span>
              </div>

              <div className="ad-creation-upgrade_options">
                {this.state.sideData.map((side, index) =>
                  side.available ?
                    (
                      <label key={index} className="ad-creation-upgrade_options-item d-flex align-items-center justify-content-between p-y-15 p-x-10 m-y-0">
                        <Checkbox
                          label={`${side.loc_name} $${this.state.sidePrice}/month`}
                          classes={'min-width-0 lh-normal'}
                          id={`${config.getEscortUpgradeType('side')}_${side.loc_id}`}
                          name={`side_sponsor_${side.loc_id}`}
                          value={side.loc_id}
                          checked={side.checked}
                          textWrap={1}
                          dataAttrs={{
                            'data-price': this.state.sidePrice,
                            'data-name': 'Side sponsor in ' + side.loc_name,
                            'data-state': side.loc_state,
                            'data-upgrade': config.getEscortUpgradeType('side'), // this param goes to url for payment
                          }}
                          handleChange={this.props.handleChange}
                        />
                      </label>
                    ) : (
                      <label key={index} className="ad-creation-upgrade_options-item d-flex align-items-center justify-content-between p-y-15 p-x-10 m-y-0">
                        <Checkbox
                        label={`Side sponsor in ${side.loc_name} not available - sold out`}
                        classes={'min-width-0 lh-normal color-error'}
                        disabled={true}
                        textWrap={1}
                        />
                      </label>
                    )
                )}
              </div>
            </div>

            <div className="ad-creation-upgrade_description">
              <div className="ad-creation-upgrade_image">
                <img src="/images/adbuild/m_sp_side.png" alt=""/>
                <div className="ad-creation-upgrade_your-ad-here __top50">
                  Your AD
                  <br/>
                  Here!
                </div>
              </div>
              <p className="weight-700">
                Appear on every page for ${this.state.sidePrice} a month
              </p>
              <p>
                Show up in sponsored ads section on the right of every page! Buy a sponsor ad upgrade for just ${this.state.sidePrice} a month.
              </p>

              <Alert
                  type={'success'}
                  classes={'f-s-16 m-t-15'}
                  transparent={true}
                  closable={false}
                  message={'Disclaimer: This upgrade is only visible on desktop and tablet version of our website (which is about 30% of our visitors - the rest 70% are users on cell phones)'}
              />

            </div>
          </div>
        </div>
        }


        {showSticky &&
        <div className={`ad-creation-upgrade ${this.state.openSticky? 'ad-creation-upgrade_opened' : ''}`}>
          <div className="container">
            <div className="ad-creation-upgrade_controls">
              <div onClick={this.toggleSticky} className="ad-creation-upgrade_title d-flex align-items-center justify-content-between">
                Appear always on the top of the list
{/*                <span className="price p-x-15 p-y-10 m-l-10 weight-700 lh-normal color-white">
                    ${this.state.stickyPrice}/mo
                </span>*/}
              </div>

              <div className="ad-creation-upgrade_options">
                { this.state.stickyData.map((sticky, index) =>
                  sticky.available ?
                    (
                      <label key={index} id={`sticky_location_${sticky.loc_id}`} className="ad-creation-upgrade_options-item d-flex align-items-center justify-content-between p-y-15 p-x-10 m-y-0">
                        {sticky.loc_name}
                        <CheckboxSecondary
                          type={'radio'}
                          data={[
                            {
                              'id': `escortStickyLocationIds_${sticky.loc_id}`,
                              'name': `sticky_sponsor_week_${sticky.loc_id}`,
                              'title': `$${this.state.stickyPriceAgency[sticky.loc_id].weeklyPrice}/week`,
                              'value': `${sticky.loc_id}`,
                              'attributes': {
                                'data-price': this.state.stickyPriceAgency[sticky.loc_id].weeklyPrice,
                                'data-upgrade': config.getEscortUpgradeType('stickyWeek'),
                                'data-name': 'Sticky Ad '+sticky.loc_name,
                                'data-state': sticky.loc_state,
                              },
                              'handleClick': this.props.handleChangeSticky,
                            },
                            {
                              'id': `escortStickyLocationIds_${sticky.loc_id}`,
                              'name': `sticky_sponsor_month_${sticky.loc_id}`,
                              'title': `$${this.state.stickyPriceAgency[sticky.loc_id].monthlyPrice}/mo`,
                              'value': `${sticky.loc_id}`,
                              'attributes': {
                                'data-price': this.state.stickyPriceAgency[sticky.loc_id].monthlyPrice,
                                'data-upgrade': config.getEscortUpgradeType('stickyMonth'),
                                'data-name': 'Sticky Ad '+sticky.loc_name,
                                'data-state': sticky.loc_state,
                              },
                              'handleClick': this.props.handleChangeSticky,
                            },
                          ]}
                        />
                      </label>
                    ) : (
                      <label key={index} className="ad-creation-upgrade_options-item d-flex align-items-center justify-content-between p-y-15 p-x-10 m-y-0">
                        <Checkbox
                        label={`Sticky upgrade in ${sticky.loc_name} not available - sold out`}
                        classes={'min-width-0 lh-normal color-error'}
                        disabled={true}
                        textWrap={1}
                        />
                      </label>
                    )
                ) }
              </div>
            </div>

            <div className="ad-creation-upgrade_description">
              <div className="ad-creation-upgrade_image">
                <img src="/images/adbuild/m_sp_sticky.png" alt=""/>
                <div className="ad-creation-upgrade_your-ad-here __top65">
                  Your AD
                  <br/>
                  Here!
                </div>
              </div>
              <p className="weight-700">
                Appear always on the top of the list ... from ${this.state.stickyPrice} a month
              </p>
              <p>
                Show up at the very top of the list, and be ahead of your competition.
              </p>
              <p>
                Ads on top of the list have 500% more views than average ad. Your ad will be always at the beginning of the first page and will not be moved to second or subsequent pages.
              </p>
            </div>
          </div>
        </div>
        }

      </div>
    )
  }
}

