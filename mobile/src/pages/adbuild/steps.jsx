import React, {Component} from 'react';

export default class Steps extends Component {
    constructor(props) {
        super(props);

        this.state = {
            steps: this.props.steps,
            length: this.props.steps.length,
            active: this.props.active,
        };
    }

    getStepClass = (i) => {
        let classes = ['steps__index'];

        if (i === this.state.active) {
            classes.push('steps__index_current');
        }

        if (i === this.state.active && i === this.state.length) {
            classes.push('steps__index_last');
        }

        return classes.join(' ');
    }

    /**
     * If steps is array with texts, ex. ['First', 'Second', ...] - we print text under step circle
     * If no steps array - print only numbers, with last step = length
     */
    componentDidMount() {
        if (!this.state.steps && this.state.length > 0) {
            let steps = [];
            for (var i = 1; i <= this.state.length; i++) {
                steps.push(i);
            }
            this.setState({
                steps: steps
            });
        }
    }

    render() {
        return (
          <div className={`steps steps_${this.state.length}`}>
            { this.state.steps.map((step, index) =>
              {
                  let i = index + 1
                  return (
                    <div className="steps__item" key={index}>
                        <div className={this.getStepClass(i)}>{i}</div>
                        <div className="steps__title">{step}</div>
                    </div>
                  )
            }
            ) }
          </div>
        )
    }
}
