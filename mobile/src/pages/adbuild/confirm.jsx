import React, {Component} from 'react';
import ButtonSubmit from "../../components/form/button-submit";
import Alert from "../../components/common/alert";
import Input from "../../components/form/input";
import axios from '../../services/api_init';
import adbuild from "../../lib/adbuild";
import auth from "../../lib/auth";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import ButtonBordered from "../../components/form/button-bordered";
import Or from "../../components/text/or";

export default class Confirm extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.timeout = null;

        this.state = {
            errorMessage: false,

            escortId: '',
            account: {},

            src: new URLSearchParams(window.location.search).get('src'),

            phone: '',
            phoneValid: false,
            phoneSent: false,
            phoneResult: false,
            phoneError: false,

            codeError: false,
            codeCount: 0,
            codeCountMax: 60,
        };

    }

    async componentDidMount() {
        this.setState({loading: true});

        await auth.getAccount(true)
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        let data = adbuild.loadCookie();
        this.setState({
            phone: data.phone,
            escortId: data.escortId,
        });

        if (!data.phone || !data.escortId) {
            window.location.href = auth.getLink('adbuild');
        }

        this.setState({loading: false});
    }


    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }


    /**
     * Verify phone number - action send
     */
    phoneVerifySend = async () => {
        // reset interval for resending code
        clearTimeout(this.timeout);
        this.setState({
            codeCount: this.state.codeCountMax
        });

        try {
            let postData = {
                'id' : this.state.escortId,
                'phone' : this.state.phone,
            };
            let url = (this.state.account.id)? '/api/v1/pc/escorts/request_phone_confirmation' : '/api/v1/escorts/request_phone_confirmation';
            await axios.post(url, postData);

            //countdown to show resend button
            this.timeout = setInterval(() => {
                this.setState(prevState => {
                    if (this.state.codeCount === 0) {
                        clearTimeout(this.timeout);
                    } else {
                        return {codeCount: prevState.codeCount - 1}
                    }
                })
            }, 1000)

            this.setState({
                phoneSent: true,
                errorMessage: '',
            });
        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: 'Error making confirmation request',
            });
        }
    }

    /**
     * Verify phone number - action check by code
     */
    phoneVerifyCheck = async (event) => {
        event.preventDefault();

        this.setState({
            codeError: false
        });

        window.scrollTo({top: 0, behavior: 'smooth'});

        // get 4 digit code
        let codeStr = '';
        let code = 0;
        [1,2,3,4].map((id, key) => {
            codeStr += document.getElementById(`verify${id}`).value;
            return null;
        })
        code = parseInt(codeStr);

        if (!code || codeStr.length !== 4) {
            this.setState({
                codeError: 'Please enter valid 4 digit code'
            });
            [1,2,3,4].map((id, key) => {
                let input = document.getElementById(`verify${id}`);
                if (!input.value) {
                    input.classList.add('error');
                }
                return null;
            })
            return false;
        }

        this.setState({
            phoneResult: true,
            phoneValid: false,
            errorMessage: '',
        });

        try {
            let postData = {
                'id' : this.state.escortId,
                'phone' : this.state.phone,
                'accountId': this.state.account.id,
                'code': code
            };
            await axios.post('/api/v1/escorts/confirm_phone', postData);

            clearTimeout(this.timeout);
            this.setState({
                phoneValid: true,
            });
        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: 'Invalid confirmation',
            });
        }
    }

    phoneVerifyBlur = async (event) => {
        let input = event.currentTarget;
        input.classList.remove('error');
    }

    phoneVerifyChange = async (event) => {
        let input = event.currentTarget;
        let number = input.value;
        let id = parseInt(input.dataset.id);

        this.setState({
            codeError: false
        });

        // validate single char
        if (number.length === 0) {
            return;
        } else if (number.length > 1) {
            input.value = number.charAt(0);
            return;
        } else if (number.length === 1) {
            // validate single 0-9 number
            if (/^[0-9]{1}$/.test(number)) {
                input.classList.remove('error');
                if (id === 4) {
                    document.getElementById('verify-phone-btn').focus();
                } else {
                    // focus next input
                    document.getElementById(`verify${id+1}`).focus();
                }
            } else {
                input.value = '';
                input.classList.add('error');
                return null;
            }
        }
    }

    /**
     * Go to payment page
     * clear cookies must be at the end !!!
     */
    submit = async (event) => {
        event.preventDefault();

        let url = '/';

        if (this.state.account.id) {
            if (this.state.src === 'edit') {
                url = auth.getLink('myposts');
            } else {
                url = adbuild.getPaymentUrl();

                // clear all adbuild steps cookies
                adbuild.clearCookie();
            }
        } else {
            // save unfinished escort
            adbuild.saveCookieFinish(this.state.escortId);

            auth.gotoSignin();
            return;
        }

        window.location.href = url;
    }

    render() {
        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

          <div className="content ad-creation" id="step-confirm-page">
            <div className="content ad-creation">
              <form method="post" action="#" onSubmit={this.submit}>

                  <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                      <div className="container">
                          <div className="ad-creation-container">
                              <h2 className="weight-700 color-primary p-b-10">
                                  Verify your phone
                              </h2>

                              {!this.state.phoneSent &&
                                  <div id="phone_verify_stage_1">
                                      <p className={'p-y-10'}>
                                          Phone number <span className="phone_verification_phone weight-600">{this.state.phone}</span> is not verified
                                      </p>
                                      <p className="m-t-10 m-b-20">
                                          <ButtonSubmit type={'button'} text={'Send verification code'} handleClick={this.phoneVerifySend}/>
                                      </p>
                                  </div>
                              }

                              {this.state.phoneSent &&
                                  <div id="phone_verify_stage_2">
                                      <p>
                                          A 4-digit code has just been sent to your phone <span className="weight-600 phone_verification_phone">{this.state.phone}</span>.
                                      </p>
                                      <p className="p-y-20">
                                          It might take up to 1 minute for the code to arrive.
                                      </p>

                                      {!this.state.phoneValid &&
                                      <>
                                        <div className="d-flex justify-content-between">
                                            {[1,2,3,4].map((id, key) => {
                                                return(
                                                    <Input
                                                        key={key}
                                                        classes={'phone-verify p-b-10'}
                                                        id={`verify${id}`}
                                                        type={'tel'}
                                                        maxCharacters={1}
                                                        counter_disabled={true}
                                                        required={true}
                                                        data={{'data-id': id}}
                                                        handleChange={this.phoneVerifyChange}
                                                        handleBlur={this.phoneVerifyBlur}
                                                    />
                                                )
                                            })}
                                        </div>
                                        <div className={'color-error'}>
                                            {this.state.codeError}
                                        </div>
                                        <ButtonSubmit id={'verify-phone-btn'} classes={'m-y-15'} handleClick={this.phoneVerifyCheck} text={'Verify phone number'}/>

                                          <Or classes={'m-t-20 m-b-5'}/>

                                          <div>
                                              <p className={'p-y-5'}>
                                                  If you didn't receive confirmation code - you can request it again.
                                              </p>

                                              {(this.state.codeCount === 0) ? (
                                                  <ButtonBordered classes={'m-y-10'} type={'button'} text={'Send code again'} handleClick={this.phoneVerifySend}/>
                                              ) : (
                                                  <p className="m-y-15 text-center color-label f-s-16">
                                                    Please wait {this.state.codeCount} seconds to resend
                                                  </p>
                                              )}
                                          </div>
                                      </>
                                      }

                                      {this.state.phoneValid &&
                                        <>
                                            <div className={'m-b-20 weight-600 color-success'}>Phone successfully verified.</div>
                                            <ButtonSubmit text={this.state.account.id ? 'Continue to payment' : 'Continue to account'}/>
                                        </>
                                      }
                                  </div>
                              }

                          </div>
                      </div>
                  </div>


              </form>
          </div>
          </div>
          </>
        )
    }
}
