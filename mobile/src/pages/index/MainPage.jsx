import React, {Component} from 'react';
import LocationsSearch from "../../components/modal/LocationsSearch"
import IndexLocations from "./IndexLocations";

class MainPage extends Component {

    render() {
        return (
            <div className="content city-all bg-white">
                <LocationsSearch showInput={true} history={this.props.history} />
                <IndexLocations />
            </div>
        );
    }
}

export default MainPage;
