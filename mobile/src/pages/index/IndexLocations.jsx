import React, {Component} from 'react';
import ReactLazyTree from 'react-lazy-tree';
import Alert from "../../components/common/alert";
import LoadingSpinner from "../../components/common/loading-spinner";
import {Link} from "react-router-dom";
import locations from "../../lib/locations";

export default class IndexLocations extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorMessage: false,
            loading: false,
            countries: [],
            locations: [],
        };
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    async componentDidMount() {
        this.setState({ loading: true });

        let countries = await locations.getCountriesWithState();
        this.setState({
            countries: countries
        });

        await locations.getUpdates();

        this.setState({ loading: false });
    }

    render() {
        const nodes = this.state.countries.map(country => {
            let node = {
                name: country.name,
                children: [],
            };

            node.children = country.states.map(state => {
                let node = {
                    name: state.name,
                    children: [],
                };
                node.children = state.cities.map(city => {
                    return {
                        name: city.name,
                        url: city.url
                    };
                });
                return node;
            });

            const subCountries = country.subCountries.map(state => {
                let node = {
                    name: state.name,
                    children: [],
                };
                node.children = state.cities.map(city => {
                    return {
                        name: city.name,
                        url: city.url
                    };
                });
                return node;
            });

            node.children = [...node.children, ...subCountries];

            return node;
        });
        return (
          <>
            {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

              <div className="city-accordion-container">
                  <ReactLazyTree {...{
                      data: nodes,

                      mapListClassName: () => 'hamburger',

                      mapListItemClassName: ({ depth, isLeafNode, isOnActivePath }) => {
                          const icon = isLeafNode
                              ? 'hamburger__node--leaf'
                              : `hamburger__node--${isOnActivePath ? 'expanded' : 'contracted'}`;
                          const leaf = `${isOnActivePath && isLeafNode ? 'hamburger__node--active' : ''}`;
                          const nodeDepth = `hamburger__node--depth-${depth}`;

                          return `hamburger__node ${nodeDepth} ${leaf} ${icon}`;
                      },

                      mapNodeContent: ({ depth, node }) => {
                          const base = 'hamburger__label';
                          const modifier = `hamburger__label--depth-${depth}`;
                          const deselect = 'hamburger__label--deselect';
                          const className = `${base} ${modifier} ${deselect}`;

                          if (node.url) {
                              return (
                                  <span className={className}>
                                      <Link to={node.url}>{node.name}</Link>
                                  </span>
                              );
                          } else {
                              return (
                                  <span className={className}>{node.name}</span>
                              );
                          }

                      }
                  }}/>
              </div>

              <LoadingSpinner loading={this.state.loading}/>
          </>
        )
    }
}
