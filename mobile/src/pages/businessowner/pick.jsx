import React, {Component} from 'react';
import axios from "axios";
import ButtonSubmit from "../../components/form/button-submit";
import places from "../../services/places";
import Checkbox from "../../components/form/checkbox";
import auth from "../../lib/auth";
import {changeTitle} from "../../store/common";
import config from "../../lib/config";
import Alert from "../../components/common/alert";
import business from "../../lib/business";
import {history} from "../../index";
import Help from "../../components/text/help";

export default class BusinessownerPick extends Component {
  constructor(props) {
    super(props);

    changeTitle(config.getTitle(this));

    this.state = {
      loading: false,
      account: false,
      errorMessage: false,

      prices: {},
      showPrice: {},
      selectedPrice: false,
      errorPrice: false,

      place: {},
      placeId: props.match.params.placeId,
    };
  }

  togglePrice = (event) => {
    //event.preventDefault();

    let id = event.currentTarget.id;

    // save selected price
    this.setState({
      selectedPrice: id
    });

    // show text
    let showPrice = {};
    showPrice[id] = true;
    this.setState({
      showPrice: showPrice
    });

    // clear other checkboxes
    let form = document.getElementById('businessowner-form');
    let inputs = form.getElementsByTagName('input');
    for (let item of inputs) {
      if (item.id !== id) {
        item.checked = false;
      }
    }
  }

  gotoPayment = () => {
    let params = {
      'src': 'place',
      'paymentType': 'placeowner',
      'placeId': this.state.place.id,
      'recurringPeriod': parseInt(this.state.prices[this.state.selectedPrice].recurringPeriod),
      'ownerPackage': this.state.selectedPrice
    };

    if (this.state.account) {
      history.push(config.getPaymentUrl(params));
    } else {
      business.saveCookieFinish(params);
      auth.gotoSignin();
    }
  }

  submit = (event) => {
    event.preventDefault();

    this.setState({
      errorMessage: false,
      errorPrice: false,
    });

    // validate
    if(!this.state.selectedPrice) {
      this.setState({
        errorPrice: 'Please select an option:'
      });
      return;
    }

    this.gotoPayment();
  }

  getPrices = async () => {
    try {
      const res = await axios.get(`/api/v1/places/owner_prices`);

      this.setState({
        prices: res.data
      });
    } catch (error) {
      let res = error.response.data;
      this.setState({
        loading: false,
        errorMessage: res.message,
      });
    }
  }

  closeAlert = () => {
    this.setState({
      errorMessage: null
    });
  }

  async componentDidMount() {
    this.setState({loading: true});

    await this.getPrices();

    await auth.getAccount(true)
      .then(account => {
        if (account) {
          this.setState({
            account: account,
          });
        }
      })

    this.setState({
      place: await places.getById(this.state.placeId),
    });

    this.setState({loading: false});
  }

  render() {
    let texts = {
      'premiumOwnerPackageMonth' : {
        'label': 'Premium Owner Package',
        'description': 'Premium owner package. You will be listed first in the hierarchy of listings. You will be able to edit your listing and add special promotions/events, pictures, videos, coupons all your correct business information.',
      },
      'premiumOwnerPackageYear' : {
        'label': 'Premium Owner Package',
        'description': 'Premium owner package for year.',
      },
      'regularOwnerPackageYear' : {
        'label': 'Regular Owner Package',
        'description': 'Regular owner package. You will be able to edit your listing and add special promotions/events, pictures, videos, coupons & all your correct business information.',
      },
    }

    return (
      <div className="content choose-advertising-package">

        {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert}/>}

        <div className="bg-white m-b-10">
          <div className="container">
            <div className="choose-advertising-package-container p-y-10">
              <h2 className="weight-700 color-primary p-b-10">{this.state.place.name} {this.state.place.zipCode}</h2>
              <div className="text-center">
                <img src={`${config.getImagePath('place')}/${this.state.place.thumbnail}`} alt={this.state.place.name} className="advertising-package-image"/>
              </div>
            </div>
          </div>
        </div>

        {(Object.keys(this.state.prices).length > 0 && !this.state.place.ownerId) &&
          <div className="bg-white">
            <div className="container">
            <div className="choose-advertising-package-container p-y-15">

              <form id={'businessowner-form'} method="POST" onSubmit={this.submit} data-ajax="false">

                {this.state.errorPrice &&
                  <div className="color-error m-b-10">{this.state.errorPrice}</div>
                }

                <div className="p-b-15">
                  {Object.keys(this.state.prices).map((id, index) =>
                    {
                      let period = (this.state.prices[id].recurringPeriod === 30)? 'mo' : 'yr';
                      return(
                        <div key={index}>
                          <label id={id} htmlFor={id} className={`package-item selection d-flex align-items-center justify-content-between p-y-10 p-x-10 m-b-15 ${this.state.showPrice[id] && 'selection_active'}`}>
                            <Checkbox
                              label={texts[id].label}
                              name={'p'}
                              id={id}
                              classes={'min-width-0 lh-normal'}
                              value={this.state.prices[id].amount}
                              textWrap={1}
                              handleChange={this.togglePrice}
                              htmlFor={false}
                            />
                            <span className="price p-x-10 p-y-10 m-l-10 weight-700 lh-normal color-white">
                              {this.state.prices[id].amount}/{period}
                            </span>
                          </label>
                          {this.state.showPrice[id] &&
                            <p className="package-description m-b-15 p-x-10">
                              {texts[id].description}
                            </p>
                          }
                        </div>
                        )
                    }
                  )}
                </div>

                <ButtonSubmit text={this.state.account ? 'Continue to payment page' : 'Continue to account'}/>

              </form>

            </div>
          </div>
          </div>
        }

        {this.state.place.ownerId &&
          <>
            <div className="bg-white m-y-10">
              <div className="container">
                <h2 className="weight-700 color-primary p-y-30">Sorry, this place already has owner</h2>
              </div>
            </div>

            <Help/>
          </>
        }

      </div>
    )
  }
}

