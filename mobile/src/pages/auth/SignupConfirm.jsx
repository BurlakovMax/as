import React, {Component} from 'react';
import axios from '../../services/api_init';
import ButtonSubmit from "../../components/form/button-submit";
import Alert from "../../components/common/alert";
import Cookies from 'universal-cookie';
import auth from "../../lib/auth";
import Input from "../../components/form/input";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";

export default class SignupConfirm extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            errorMessage: false,
            errors: {},

            // use confirm input & validate code or just redirect to singin
            useConfirm: false,

            // user data from signup page
            email: '',
            username: '',

            confirmationId: false, // id from api
            confirmationCode: false, // code from user
        };
    }

    fillForm = event => {
        const target = event.target;
        const value = target.value;

        this.setState({
            confirmationCode: value
        });
    }

    validate = () => {
        let ok = true;
        let errors = {};

        if (!this.state.confirmationCode) {
            errors['code'] = 'Please enter confirmation code';
            ok = false;
        }

        if (!this.state.confirmationId) {
            this.setState({
                errorMessage: 'Confirmation ID not found',
            });
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    submit = async (event) => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        this.setState({
            loading: true,
        });

        try {
            let postData = {
                'confirmationId' : this.state.confirmationId,
                'confirmationCode' : this.state.confirmationCode,
            };
            await axios.get('/api/v1/sign_up_confirm', postData);

            this.gotoSignup();
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    gotoSignup = () => {

        // clear signup info
        let cookies = new Cookies();
        cookies.set('signup',[],{path: '/', maxAge: 0})

        auth.gotoSignin();
    }

    componentDidMount() {
        let cookies = new Cookies();
        let data = cookies.get('signup');

        if (data !== undefined) {
            this.setState({
                email: data.email,
                username: data.username,
                confirmationId: data.confirmationId,
            });
        } else {
            this.gotoSignup();
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    render() {

        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

          <div id="sign-up-confirm-page" className="content registration">
            <div className="wrapper-sent-message-container bg-white p-y-15">
              <div className="container">
                  <div className="sent-message-container">
                      <div className="icon-block d-flex justify-content-center align-items-center p-y-30">
                          <svg className="icon_send_message">
                              <use xlinkHref="/images/icons.svg#icon_send_message"/>
                          </svg>
                      </div>

                      {!this.state.useConfirm &&
                          <div className="text-block">
                              <p className="m-b-20">
                                  Thank you <span className="weight-600">{this.state.username}</span>. We have sent an email message to the address <span className="weight-600">{this.state.email}</span>.
                                  Please access your email and click the verification link provided in order to activate your AdultSearch account.
                              </p>
                              <p className="m-b-20">
                                  If you do not receive this email, please check your <span className="weight-600">Spam</span> or <span className="weight-600">Junk</span> email
                                  folder.
                              </p>

                              <p className="m-y-25">
                                  {<ButtonSubmit text={'Proceed to Login'} handleClick={this.gotoSignup}/>}
                              </p>
                          </div>
                      }

                      {this.state.useConfirm &&
                          <form action="#" onSubmit={this.submit} method="POST" data-ajax="false" id="confirm-form">
                              <div className="text-block">
                                  <p className="m-b-20">
                                      Thank you <span className="weight-600">{this.state.username}</span>. We have sent an email message to the address <span className="weight-600">{this.state.email}</span>.
                                      Please access your email and click the verification link provided in order to activate your AdultSearch account.
                                      If you do not receive this email, please check your <span className="weight-600">Spam</span> or <span className="weight-600">Junk</span> email
                                      folder.
                                  </p>

                                  <Input
                                    label={'Confirmation code'}
                                    name={'code'}
                                    id={'code'}
                                    placeholder={'Enter confirmation code here'}
                                    minLength={6}
                                    maxLength={6}
                                    required={true}
                                    handleChange={this.fillForm}
                                    error={this.state.errors.code}
                                  />

                                  <p className="m-b-20">
                                      {<ButtonSubmit loading={this.state.loading}/>}
                                  </p>
                              </div>
                          </form>
                      }

                  </div>
              </div>
          </div>
          </div>
          </>
        )
    }
}
