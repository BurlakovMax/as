import React, {Component} from 'react';
import axios from '../../services/api_init';
import Alert from "../../components/common/alert";
import auth from "../../lib/auth";
import Input from "../../components/form/input";
import ButtonSubmit from "../../components/form/button-submit";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import form from "../../lib/form";

export default class ResetPassword extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            emailSent: false,
            errorMessage: null,
            successMessage: null,

            errorEmail: false,
            errors: {},

            passwordLink: {
                email: null,
            },
            confirmPassword: {
                confirmationId: null,
                password: null,
                confirmationCode: null
            }
        }
    };

    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    fillPasswordLink = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            passwordLink: Object.assign(this.state.passwordLink, res)
        });
    }

    fillConfirmPassword = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            confirmPassword: Object.assign(this.state.confirmPassword, res)
        });
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;

        let errors = {
            'password': 'Please type new password',
            'confirmationCode': 'Please enter confirmation code',
        };

        for (let id in errors) {
            if (document.getElementById(id).value) {
                errors[id] = false
            } else {
                ok = false;
            }
        }

        if (this.state.confirmPassword.password !== this.state.confirmPassword.confirm_password) {
            errors['confirm_password'] = 'New password and confirm password do not match';
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    confirmPassword = async event => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false,
            successMessage: false,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        try {
            await axios.post('/api/v1/confirm_password_reset', this.state.confirmPassword);

            this.setState({
                successMessage: 'Success! Your password was updated.'
            });

            auth.gotoSignin();
            return;
        } catch (error) {
            config.catch(error);

            if (error.response && error.response.data) {
                this.setState({
                    errorMessage: error.response.data.message
                });
            }
        }
    }

    sendLink = async event => {
        event.preventDefault();

        this.setState({
            errorMessage: false,
            successMessage: false,
            errorEmail: false,
        });

        let emailError = form.validateEmail(this.state.passwordLink.email);
        if (emailError) {
            this.setState({
                errorEmail: emailError
            });
            return false;
        }

        try {
            const res = await axios.post('/api/v1/password_reset', this.state.passwordLink);
            this.setState({
                emailSent: true,
                confirmPassword: Object.assign(this.state.confirmPassword, {confirmationId: res.data.confirmationId})
            });
            this.setState({
                successMessage: 'Success! Confirmation code was sent to your email.'
            });
        } catch (error) {
            config.catch(error);

            if (error.response && error.response.data) {
                this.setState({
                    errorMessage: error.response.data.message
                });
            }
        }
    }

    componentDidMount() {
        if (auth.getToken()) {
            auth.gotoAccount()
        }
    }

    render() {
        return (
            <>
            {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}
            {this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

            <div className="content reset" id="reset-password-form">
                {!auth.getToken() &&
                    <div className="reset-container">
                        {this.state.emailSent ? (
                          <>
                              <div className="bg-white">
                                  <div className="container">
                                      <h2 className="weight-700 p-y-10 color-primary">Please check your email inbox</h2>
                                  </div>
                              </div>

                              <form action="#" method="post" data-ajax="false" onSubmit={this.confirmPassword}>
                                  <div className="bg-white p-y-10">
                                      <div className="container">
                                          <p>Set a new password for your account</p>

                                          <Input
                                            label={'Password'}
                                            type={'password'}
                                            name={'password'}
                                            id={'password'}
                                            placeholder={'Enter password here'}
                                            handleChange={this.fillConfirmPassword}
                                            error={this.state.errors.password}
                                            required={true}
                                          />

                                          <Input
                                            label={'Password again'}
                                            type={'password'}
                                            name={'confirm_password'}
                                            id={'confirm_password'}
                                            placeholder={'Enter password here'}
                                            handleChange={this.fillConfirmPassword}
                                            error={this.state.errors.confirm_password}
                                            minLength={6}
                                            maxLength={20}
                                            required={true}
                                          />

                                          <Input
                                            label={'Confirmation code'}
                                            type={'text'}
                                            name={'confirmationCode'}
                                            id={'confirmationCode'}
                                            placeholder={'Enter confirmation code from email'}
                                            handleChange={this.fillConfirmPassword}
                                            error={this.state.errors.confirmationCode}
                                            minLength={6}
                                            maxLength={20}
                                            required={true}
                                          />

                                          <ButtonSubmit text={'Set Password'}/>

                                      </div>
                                  </div>
                              </form>
                          </>
                        ) : ''}

                        {!this.state.emailSent ? (
                          <form action="#" method="post" data-ajax="false" onSubmit={this.sendLink}>
                              <div className="bg-white p-t-25">
                                  <div className="container">
                                      <p className={'m-b-10'}>Have you forgotten your password?</p>

                                      <Input
                                        label={'Your Email'}
                                        type={'email'}
                                        name={'email'}
                                        id={'email'}
                                        required={true}
                                        placeholder={'Enter email here'}
                                        handleChange={this.fillPasswordLink}
                                        error={this.state.errorEmail}
                                      />

                                  </div>
                              </div>

                              <div className="bg-white p-y-20 m-t-10">
                                  <div className="container">
                                      <button
                                          className="primary-submit-btn m-b-10 h-45"
                                          type="submit"
                                          disabled={!window.navigator.onLine}
                                      >
                                          <span>Reset Password</span>
                                      </button>
                                  </div>
                              </div>
                          </form>
                        ) : ''}
                    </div>
                }
            </div>
            </>
        )
    }
}
