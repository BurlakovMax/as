import React, {Component} from 'react';
import axios from '../../services/api_init';
import ButtonSubmit from "../../components/form/button-submit";
import Alert from "../../components/common/alert";
import Cookies from 'universal-cookie';
import auth from "../../lib/auth";
import adbuild from "../../lib/adbuild";
import Input from "../../components/form/input";
import {Link} from "react-router-dom";
import {changeTitle} from "../../store/common";
import config from "../../lib/config";
import form from "../../lib/form";

export default class Signup extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            errorMessage: false,
            errors: {},

            // config vars
            show_terms: true,

            // posted data to api in signup
            user: {
                email: null,
                username: null,
                password: null,
                password2: null,
            },
        };
    }

    fillForm = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            user: Object.assign(this.state.user, res)
        });
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;

        let errors = {
            'username': 'Please type your username',
            'password': 'Please type your password',
        };

        for (let id in errors) {
            if (document.getElementById(id).value) {
                errors[id] = false
            } else {
                ok = false;
            }
        }

        if (this.state.user.password && this.state.user.password !== this.state.user.password2) {
            errors['password2'] = 'Password and confirm password do not match';
            ok = false;
        }

        let emailError = form.validateEmail(this.state.user.email);
        if (emailError) {
            errors['email'] = emailError;
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    submit = async (event) => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        this.setState({
            loading: true,
        });

        try {
            const res = await axios.post('/api/v1/sign_up', this.state.user);

            if (res.data && res.data.confirmationId) {
                // save email & name to cookies for confirmation page
                let cookies = new Cookies();
                cookies.set(
                  'signup',
                  {'email': this.state.user.email, 'username': this.state.user.username, 'confirmationId': res.data.confirmationId},
                  {path: '/', expires: new Date(Date.now()+3600), maxAge: 3600} // 1 hour
                );

                // check for unfinished escort payment
                await adbuild.checkFinish(res.data.accountId);

                window.location.href = auth.getLink('account-confirm');
            } else {
                this.setState({
                    loading: false,
                    errorMessage: 'Error signing up. Please use valid email.',
                });
            }

        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    printButton = () => {
        let text = '';

        if (this.state.loading) {
            text = 'Please wait ...';
        } else {
            text = this.state.show_terms ? 'I accept. Create my account' : 'Create my account';
        }

        return <ButtonSubmit loading={this.state.loading} text={text} />;
    }

    componentDidMount() {
        if (auth.getToken()) {
            auth.gotoAccount()
        }
    }

    render() {

        return (
          <>
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}

          <div id="sign-up-page" className="content registration">
          {!auth.getToken() &&
              <form action="#" onSubmit={this.submit} method="POST" data-ajax="false" id="signup-form">

                  <div className="registration-container bg-white m-b-10 p-t-10">
                      <div className="container">

                          <Input
                            label={'Your Email'}
                            type={'email'}
                            name={'email'}
                            id={'email'}
                            placeholder={'Enter email here'}
                            minLength={6}
                            maxLength={100}
                            required={true}
                            handleChange={this.fillForm}
                            error={this.state.errors.email}
                          />

                          <Input
                            label={'Username'}
                            type={'text'}
                            name={'username'}
                            id={'username'}
                            placeholder={'Enter username here'}
                            minLength={3}
                            maxLength={32}
                            required={true}
                            handleChange={this.fillForm}
                            error={this.state.errors.username}
                          />

                          <Input
                            label={'Password'}
                            type={'password'}
                            name={'password'}
                            id={'password'}
                            placeholder={'Enter password here'}
                            minLength={6}
                            maxLength={20}
                            required={true}
                            handleChange={this.fillForm}
                            error={this.state.errors.password}
                          />

                          <Input
                            label={'Confirm Password'}
                            type={'password'}
                            name={'password2'}
                            id={'password2'}
                            placeholder={'Retype password here'}
                            minLength={6}
                            maxLength={20}
                            required={true}
                            handleChange={this.fillForm}
                            error={this.state.errors.password2}
                          />

                      </div>
                  </div>

                  <div className="registration-container bg-white p-y-10">
                      <div className="container">

                          {this.state.show_terms &&
                              <div className="info-container p-t-5 p-b-10">
                                  <h3 className="required">Terms of Services</h3>
                                  <p className="terms-of-service p-t-10 f-s-16">
                                      Please check the Adult Search Account information you've entered above (feel free to change
                                      anything you like), and review the <Link className="weight-600" to={auth.getLink('terms')} target="_blank">Terms of Service</Link>.
                                  </p>
                                  <p className="privacy-policy p-t-10 f-s-16">
                                      By clicking on ‘I accept’ below you are agreeing to the <Link className="weight-600" to={auth.getLink('privacy')} target="_blank">Privacy Policy</Link>
                                  </p>
                              </div>
                          }

                          {this.printButton()}

                      </div>
                  </div>

              </form>
          }
          </div>
          </>
        )
    }
}
