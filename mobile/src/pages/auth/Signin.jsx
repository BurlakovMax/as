import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from '../../services/api_init';
import Alert from "../../components/common/alert";
import auth from "../../lib/auth";
import form from "../../lib/form";
import Input from "../../components/form/input";
import adbuild from "../../lib/adbuild";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import Or from "../../components/text/or";

class Signin extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            errorMessage: false,
            account: {},
            errors: {},
            token: null,
            user: { // posted data to api
                email: null,
                password: null,
            }
        };
    }

    fillForm = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let res = {[name]: value};

        this.setState({
            user: Object.assign(this.state.user, res)
        });
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;

        let errors = {
            'password': 'Please type your password',
        };

        for (let id in errors) {
            if (document.getElementById(id).value) {
                errors[id] = false
            } else {
                ok = false;
            }
        }

        let emailError = form.validateEmail(this.state.user.email);
        if (emailError) {
            errors['email'] = emailError;
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    submit = async (event) => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: false,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        this.setState({
            loading: true,
        });

        try {
            const res = await axios.post('/api/v1/login', this.state.user);

            if (res.data) {
                auth.saveToken(res.data)

                await auth.getAccount()
                  .then(account => {
                      if (account) {
                          this.setState({
                              account: account
                          });
                      }
                  })

                // check for unfinished escort payment
                await adbuild.checkFinish(this.state.account.id);

                let origin = new URLSearchParams(window.location.search).get('origin');
                if (origin) {
                    window.location.href = origin;
                } else {
                    auth.gotoAccount();
                }
            } else {
                this.setState({
                    loading: false,
                    errorMessage: 'Error signing in. Please use valid email and password.',
                });
            }

        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                errorMessage: error.response.data.message,
            });
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null
        });
    }

    componentDidMount() {
        document.body.classList.add('account-signin');
    }

    render() {

        // hide remember me checkbox, not used now
        let showRememberMe = false;

        return (
        <div className="content login p-t-15">
          {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} transparent={true}/>}

            <div id="sign-in-page" className="content login p-t-15">
                <div className="container">
                    <div className="login-container">
                        <div className="login-form">

                            <form action='#' onSubmit={this.submit} method="POST" id="signin-form">

                                <div className="double-input m-b-25 ">
                                    <Input
                                        icon_user={true}
                                        type={'email'}
                                        name={'email'}
                                        id={'email'}
                                        placeholder={'Your email'}
                                        required={true}
                                        handleChange={this.fillForm}
                                        minLength={6}
                                        maxLength={100}
                                        error={this.state.errors.email}
                                    />

                                    <Input
                                        icon_password={true}
                                        name="password"
                                        id="password"
                                        placeholder="Password"
                                        required={true}
                                        handleChange={this.fillForm}
                                        minLength={6}
                                        maxLength={20}
                                        error={this.state.errors.password}
                                    />
                                </div>

                                {showRememberMe &&
                                    <div className="primary-checkbox position-relative p-b-10">
                                        <label className="text-nowrap " htmlFor="keep-logged-in">
                                            Remember Me On This Device
                                            <input type="checkbox" defaultChecked={true} name="saveme" id="keep-logged-in" value="1"/>
                                            <span></span>
                                        </label>
                                    </div>
                                }

                                <button className="primary-submit-btn" disabled={!!this.state.loading || !window.navigator.onLine} type="submit">
                                    <span>{this.state.loading ? 'Please wait ...' : 'Login'}</span>
                                </button>
                            </form>

                            <Or classes={'m-y-20'}/>

                            <Link className="success-filled-btn m-b-20" to={auth.getLink('signup')}>
                                <span>Create New Account</span>
                            </Link>

                            <Link to={auth.getLink('reset')} className="secondary-btn large weight-600 d-table mx-auto m-b-50 color-quaternary">
                                Forgot your password?
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default Signin;
