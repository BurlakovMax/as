import React, {Component} from 'react';
import ButtonSubmit from "../../components/form/button-submit";
import ButtonBordered from "../../components/form/button-bordered";
import Checkbox from "../../components/form/checkbox";
import Alert from "../../components/common/alert";
import Modal from "../../components/common/modal";

export default class EscortsFilter extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filters: [],
			showFilters: {},
			successMessage: 'Expand a filter group and select desired filters, then click "Apply filters"',
		};
	}

	toggleFilter = event => {
		//event.preventDefault();

		let id = event.currentTarget.dataset.id;

		let showFilters = this.state.showFilters;
		showFilters[id] = !showFilters[id];

		this.setState({
			showFilters: showFilters
		});
	}

	printFilterOptions = (options) => {
		return (typeof options === 'object')? options : []
	}

	isActive = (filterName, value) => {
		let active = false;

		if (this.props.filtersSelected[filterName]) {
			if (this.props.filtersSelected[filterName+'Ids'].includes(value)) {
				active = true;
			}
		}

		return active;
	}

	closeAlert = () => {
		this.setState({
			successMessage: null
		});
	}

	render() {
		let count = (this.props.filtersExist)? this.props.filtersCount : this.props.escortsTotal;

		return (
			<Modal
				id={'filters-modal'}
				title={'Filters'}
				fullScreen={true}
				content={
					<div className="filters">
						{this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

						{ this.props.filters.map((filter, index) =>
							<div id={filter.name.toLowerCase()} key={index} className="filters__filter">

								<div className={`filter__header p-x-15 f-s-20 ${this.state.showFilters[filter.name.toLowerCase()]? 'active' : ''}`} onClick={this.toggleFilter} data-id={filter.name.toLowerCase()}>
									<h2 className="weight-700 m-r-15">{filter.name}</h2>

									{/*selected  options tags*/}
									<div className={'filter__selected_list'}>
										{this.printFilterOptions(this.props.filtersSelected[filter.name]).map((selected, index) =>
											<div key={index} className="filter__selected f-s-16" data-id={selected.value}>{selected.option}</div>
										)}
									</div>

									<div className="filter__arrow m-l-auto">
										<svg className="icon_caret_down">
											<use xlinkHref="/images/icons.svg#icon_caret_down" />
										</svg>
									</div>
								</div>

								{this.state.showFilters[filter.name.toLowerCase()] &&
								<div className="filter__content">
									{filter.values.map((option, index) =>
										<Checkbox
											key={index}
											classes={`filter__option f-s-20 ${this.isActive(filter.name, option.value) && 'active'}`}
											label={option.name}
											id={`${filter.name.toLowerCase()}-${option.value}`}
											parentId={`${filter.name.toLowerCase()}-${option.value}-parent`}
											name={filter.name}
											value={option.value}
											handleChange={this.props.toggleFilter}
											dataAttrs={{'data-title':option.name}}
										/>
									)}
								</div>
								}

							</div>
						) }
					</div>
				}
				footer={
					<div className="filters__buttons bg-white w-100">
						<div className="text-center f-s-16 m-b-6 color-primary">Found results: <span className={`weight-600 ${count? '' : 'color-error'}`}>{count}</span></div>
						<div className="row">
							<div className="col-4 p-r-0">
								<ButtonBordered disabled={!this.props.filtersExist} handleClick={this.props.resetFilter} id={'filters-modal-reset'} classes={'color-primary color-primary-border'} type={'button'} text={'Reset'}/>
							</div>
							<div className="col-8">
								<ButtonSubmit handleClick={this.props.applyFilter} data={{'data-dismiss': 'modal'}} id={'filters-modal-submit'} type={'button'} text={`Apply Filters`}/>
							</div>
						</div>
					</div>
				}
			/>
		);
	}
}

