import React, {Component} from 'react';
import {Link} from "react-router-dom";
import breadcrumbs from "../../services/breadcrumbs.js";
import places from "../../services/places";
import m4mlinks from "../../services/m4m-links.js";
import cities from "../../services/cities.js";
import NavigationCarousel from "../../components/common/navigation-carousel";
import Breadcrumbs from "../../components/common/breadcrumbs";
import escorts from "../../services/escorts.js";
import LoadingSpinner from "../../components/common/loading-spinner";
import sponsors from "../../services/sponsors.js";
import Pagination from "../../components/common/pagination";
import EscortsFilter from './EscortsFilter';
import db from "../../istore";
import axios from '../../services/api_init';
import moment from 'moment';
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import position from "../../lib/position";
import filter from "../../lib/filter";

export default class EscortsCategory extends Component {
	constructor(props) {
		super(props);

		this.timeout = false;
		this.interval = false;

		this.state = {
			loading: false,
			city: {},
			categoryUrl: null,
			category: {},

			links: [],

			filters: [],
			filtersSelected: {},
			filtersCount: 0,

			advertising: [],
			sponsors: [],
			escortTypes: [],
			placeTypes: [],
			m4mLink: {},

			escortsList: [],
			escortsTotal: 0,
			escortsTotalLocal: 0,

			// pagination params
			offset: 0,
			limit: 10,
			firstPage: true,
		};
	}

	handleScroll = async (event) => {
		clearTimeout(this.timeout);

		this.timeout = setTimeout(async () => {
			await position.saveScroll(this.state.offset, this.state.limit);
		}, 300)
	}

	toggleFilter = async (event) => {
		//event.preventDefault();

		let filtersSelected, count;

		// highlight selected parent
		filter.heighLight(event);

		// process filter add or delete
		if (filter.exist(event, this.state.filtersSelected)) {
			filtersSelected = filter.delete(event, this.state.filtersSelected);
		} else {
			filtersSelected = filter.add(event, this.state.filtersSelected);
		}
		this.setState({
			filtersSelected: filtersSelected
		});

		// re calculate data & get total count
		if (filter.count(this.state.filtersSelected)) {
			count = await this.searchFilter(true);
			await filter.dbClearSelectedFilters(this.state.category.id);
			await filter.dbSaveSelectedFilters(this.state.category.id, filtersSelected);
		} else {
			count = await this.getEscorts(0 ,10,true);
			await filter.dbClearSelectedFilters(this.state.category.id);
		}

		this.setState({
			filtersCount: count,
			escortsTotal: count,
		});
	}

	resetFilter = async event => {
		event.preventDefault();

		let count = await this.getEscorts(0,10,true);

		config.debug('FILTER RESET', count);

		this.setState({
			filtersSelected: {},
			filtersCount: count,
			escortsTotal: count,
		});
	}

	applyFilter = async event => {
		event.preventDefault();

		let count = filter.count(this.state.filtersSelected);
		config.debug('FILTER APPLY', count);

		// if filter is reset or empty
		if (count === 0) {
			await this.getEscorts(0, 10);
			await filter.dbClearSelectedFilters(this.state.category.id);
		} else {
			this.setState({
				offset: 0,
				limit: 10,
			});
			await this.searchFilter();
			await filter.dbSaveSelectedFilters(this.state.category.id, this.state.filtersSelected);
		}
	}

	searchFilter = async (count = false) => {
		let show = [];
		let escorts = await this.getEscortsLocal();

		// search escorts by filters
		let escortsFilter = filter.search(escorts, this.state.filtersSelected);
		let total = escortsFilter.length;

		config.debug('FILTER SEARCH', total);

		// we need only count
		if (count) {
			return total;
		}

		let offset = this.state.offset;
		let limit = this.state.limit;

		// restore list limits
		let scroll = await position.getScroll();
		if (scroll) {
			offset = scroll.offset;
			limit = scroll.limit;
		}

		if (total) {
			show = escortsFilter.slice(offset, offset + limit);
		}

		this.setState({
			escortsList: show,
			escortsTotal: total,
			firstPage: offset === 0,
			offset: offset,
			limit: limit,
		});

		// restore client position
		let top = 0
		if (scroll) {
			top = scroll.top
		}

		window.scrollTo({top: top, behavior: 'smooth'});
	}

	// load selected filters from local db
	getFiltersLocal = async () => {
		this.setState({
			filtersSelected: await filter.dbGetSelectedFilters(this.state.category.id)
		});
	}

	getEscortsLocal = async () => {
		let escorts = await db.escorts
			.where('locationIds').equals(this.state.city.id)
			.and(item => item.typeSlug === this.state.categoryUrl)
			.reverse()
			.sortBy('postedAt');

		this.setState({
			escortsTotalLocal: escorts.length
		});

		config.debug(`local total: ${escorts.length}`);

		return escorts;
	}

	getEscortsLocalCount = async () => {
		let countLocal = await db.escorts
			.where('locationIds').equals(this.state.city.id)
			.and(item => item.typeSlug === this.state.categoryUrl)
			.count();

		this.setState({
			escortsTotalLocal: countLocal
		});

		// clear checking local count
		if (!this.isLoadingEscorts()) {
			clearInterval(this.interval);
		}
	}

	getEscorts = async (offset = 0, limit = 10, count = false) => {
		let list = [];
		let show = [];
		let total = 0;

		// restore list limits
		let scroll = await position.getScroll();
		if (scroll) {
			offset = scroll.offset;
			limit = scroll.limit;
		}

		const escorts = await this.getEscortsLocal();

		if (escorts.length > 0) {

			list = escorts;
			total = list.length;
			config.debug(`local total: ${total}`);

		} else {

			// initially get ALL records w/o limit
			let params = {
				locationId: this.state.city.id,
				type: this.state.categoryUrl,
				offset: 0,
				limit: 100,
			};

			try {
				config.debug('category escorts');

				const res = await axios.get('/api/v1/escorts', {params});
				total = res.data.total;
				list = res.data.list;

				config.debug(`server total: ${total}`);
				if (!config.isProd()) {
					console.timeEnd('category escorts');
				}

				await db.escorts.bulkPut(list);

				await db.sync_cities.put({
					id: this.state.city.id,
					updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
				});
				await this.getEscortsLocalCount();

			} catch (error) {
				config.catch(error);
			}

		}

		// we need only count
		if (count) {
			return total;
		}

		if (total) {
			show = list.slice(offset, offset+limit);
		}

		this.setState({
			escortsList: show,
			escortsTotal: total,
			firstPage: offset === 0,
			offset: offset,
			limit: limit,
		});

		// restore client position
		let top = 0
		if (scroll) {
			top = scroll.top
		}

		window.scrollTo({top: top, behavior: 'smooth'});
	}

	getData = async (offset = 0, limit = 10) => {
		if (filter.count(this.state.filtersSelected)) {
			this.setState({
				offset: offset,
				limit: limit,
			});
			await this.searchFilter();
		} else {
			await this.getEscorts(offset, limit);
		}
	}

	getCityCategory = async () => {
		const categoryUrl = this.props.match.url.split('/')[this.props.match.url.split('/').length - 1];

		const {stateName, cityName} = this.props.match.params;
		let city = await cities.getByUrl(stateName, cityName);
		config.setCity(city);

		this.setState({
			city: city,
			categoryUrl: categoryUrl,
		});
	}

	getDataStates = async () => {
		const links = breadcrumbs.getCategoryBreadcrumbs(this.state.city.id, this.state.categoryUrl).catch(error => { config.catch(error); });
		const filters = filter.dbGetFilters().catch(error => { config.catch(error); });
		const advertising = escorts.getAdvertising(this.state.city.id, this.state.categoryUrl).catch(error => { config.catch(error); });
		const sponsorsList = sponsors.getEscort(this.state.city.id, this.state.categoryUrl).catch(error => { config.catch(error); });
		const escortTypesList = escorts.getTypes(this.state.city.id, 'city').catch(error => { config.catch(error); });
		const placeTypesList = places.getTypes(this.state.city.id, 'city').catch(error => { config.catch(error); });
		const m4mLinkList = m4mlinks.getLink(this.state.city.id).catch(error => { config.catch(error); });
		let states = await Promise.all([links, filters, advertising, sponsorsList, escortTypesList, placeTypesList, m4mLinkList]);

		await this.setState({
			links: Array.isArray(states[0]) ? states[0] : [],
			filters: Array.isArray(states[1]) ? states[1] : [],
			advertising: Array.isArray(states[2]) ? states[2] : [],
			sponsors: Array.isArray(states[3]) ? states[3] : [],
			escortTypes: Array.isArray(states[4]) ? states[4] : [],
			placeTypes: Array.isArray(states[5]) ? states[5] : [],
			m4mLink: states[6],
		});

		const category = this.state.escortTypes.find(escortType => escortType.slug === this.state.categoryUrl) || {};
		changeTitle(category.name);
		this.setState({category: category});
	}

	async componentDidUpdate(prevProps, prevState, snapshot) {
		const category = this.props.match.url.split('/')[this.props.match.url.split('/').length - 1];
		const prevCategory = prevProps.match.url.split('/')[this.props.match.url.split('/').length - 1];

		if (category !== prevCategory) {
			this.setState({loading: true});

			await this.getCityCategory();
			await this.getDataStates();
			await this.getFiltersLocal();
			await this.getEscortsLocal();

			// get list from filter or api/db
			await this.getData();

			// start checking local count
			this.interval = setInterval(async () => {
				await this.getEscortsLocalCount();
			}, 5000)

			this.setState({loading: false});
		}
	}

	async componentDidMount() {
		window.addEventListener('scroll', this.handleScroll, false);

		this.setState({loading: true});

		await this.getCityCategory();
		await this.getDataStates();
		await this.getFiltersLocal();
		await this.getEscortsLocal();

		// get list from filter or api/db
		await this.getData();

		// start checking local count
		this.interval = setInterval(async () => {
			await this.getEscortsLocalCount();
		}, 5000)

		this.setState({loading: false});
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll, false);
	}

	isLoadingEscorts = () => {
		if (this.state.category.count !== 'undefined') {
			let serverCount = (this.state.category.count > 5)? (this.state.category.count - 5) : this.state.category.count;
			return (this.state.escortsTotalLocal >= serverCount)? false : true;
		} else {
			return true;
		}
	}

	showFilterButton = () => {
		let show = true;

		if (this.isLoadingEscorts()) {
			show = false;
		}

		if (filter.count(this.state.filtersSelected) === 0 && this.state.escortsTotal === 0) {
			show = false;
		}

		return show;
	}

	render() {
		const slides = [...this.state.escortTypes, ...[this.state.m4mLink], ...this.state.placeTypes];
		let filterOptions = filter.getOptions(this.state.filtersSelected);

		return (
			<div className="content escort-list">

				<Breadcrumbs links={this.state.links} />

				{this.state.city.url && <NavigationCarousel className={'bg-white'} urlPrefix={this.state.city.url} slides={slides} />}

				<div className="bg-white m-b-10 p-b-10">
					<div className="container">

						<div className="page-actions p-y-15 d-flex flex-row align-items-center justify-content-between">

							<div className="location-info-container d-flex align-items-center">
								<div className="location-info weight-700">
									{ filter.count(this.state.filtersSelected) ? this.state.escortsTotal : this.state.escortsTotalLocal } {this.state.category.name} in <span className="location-name color-primary text-nowrap">{ this.state.city.name }</span>
									{ this.isLoadingEscorts() && <div className="f-s-14 weight-400 color-label">More results are loading ...</div>}
								</div>
							</div>

							{/*filters open button*/}
							{this.showFilterButton() &&
								<div id="filters-modal-open" data-toggle="modal" data-target="#filters-modal" className="filters-container filter-open p-y-5 p-x-10 d-flex flex-row align-items-center m-l-5">
									<div className="selected-options">
										{filter.count(this.state.filtersSelected) === 0 &&
											<div className="p-r-10 f-s-16">Filters</div>
										}
										{filter.count(this.state.filtersSelected) > 0 &&
											<div className="primary-counter-container d-flex align-items-center justify-content-center m-r-5 text-white">
												<span className="primary-counter">{filter.count(this.state.filtersSelected, true)}</span>
											</div>
										}
									</div>
									<div className="filters">
										<svg className="icon_filters">
											<use xlinkHref="/images/icons.svg#icon_filters"/>
										</svg>
									</div>
								</div>
							}

							{/*filters modal*/}
							<EscortsFilter
								toggleFilter={this.toggleFilter}
								resetFilter={this.resetFilter}
								applyFilter={this.applyFilter}
								filters={this.state.filters}
								filtersSelected={this.state.filtersSelected}
								filtersExist={filter.count(this.state.filtersSelected)}
								filtersCount={this.state.filtersCount}
								escortsTotal={this.state.escortsTotal}
							/>

						</div>

						{/*filters selected tags*/}
						{filterOptions.map((option, index) =>
							<div key={index}
								 onClick={this.toggleFilter}
								 data-name={option.filter.charAt(0).toUpperCase() + option.filter.slice(1)}
								 data-title={option.option}
								 data-value={option.value}
								 data-id={`${option.filter.toLowerCase()}-${option.value}`}
								 className={`with-cross-icon m-b-5 m-r-5`}>
								<svg className="icon_close">
									<use xlinkHref="/images/icons.svg#icon_close"/>
								</svg>
								<span className="text">
									<span className={'color-secondary'}>{option.filter}</span>: {option.option}
								</span>
							</div>
						)}

					</div>
				</div>

				<div className="bg-white m-b-10 p-y-10">
					<div className="container">
						<div className="escort-list-container">

							<div className="items-container d-flex flex-row flex-wrap list-view">

								{/*ADVERTISING*/}
								{this.state.firstPage && this.state.advertising.map(escort =>
									<a key={escort.id} href={ escort.directLink } target={'_blank'} className="card m-b-15 escort-list">
										<div className="image-container">
											<img className="image image-cover" src={`${config.getImagePath('escort')}/${escort.thumbnail}` } alt={ escort.firstName } />
										</div>
										<div className="info-container d-flex justify-content-between flex-column flex-grow-1 p-t-5">
											<div className="d-flex flex-grow-1 flex-column justify-content-between">
												<div className="p-x-10">
													<div className="title-container">
														<div className="m-b-5 escort-label">Advertising</div>
														<span className="title d-block weight-600 m-b-5">{ escort.firstName }</span>
													</div>
													<div className="description-container f-s-16">
														<p className="description color-tertiary">{ escort.title }</p>
													</div>
												</div>
												{ escort.phone &&
													<div data-phone={ escort.phone } className="phone-container call-phone d-flex align-items-center p-y-10 p-x-10">
														<div className="icon m-r-5">
															<svg className="icon_phone">
																<use xlinkHref="/images/icons.svg#phone" />
															</svg>
														</div>
														<span className="phone weight-600">{ escort.phone }</span>
													</div>
												}
											</div>
										</div>
									</a>
								) }

								{/*SPONSORS*/}
								{this.state.firstPage && this.state.sponsors.map(sponsor =>
									<Link key={sponsor.id} to={ `${this.state.city.url}${this.state.categoryUrl}/${sponsor.id}?locationId=${this.state.city.id}` } className="card m-b-15 escort-list">
										<div className="image-container">
											<img className="image image-cover" src={`${config.getImagePath('escort')}/${sponsor.thumbnail}` } alt={ sponsor.firstName } />
										</div>
										<div className="info-container d-flex justify-content-between flex-column flex-grow-1 p-t-5">
											<div className="d-flex flex-grow-1 flex-column justify-content-between">
												<div className="p-x-10">
													<div className="title-container">
														<div className="m-b-5 escort-label">Sponsor</div>
														<span className="title d-block weight-600 m-b-5">{ sponsor.firstName }</span>
													</div>
													<div className="description-container f-s-16">
														<p className="description color-tertiary">{ sponsor.title }</p>
													</div>
												</div>
												{ sponsor.phone &&
													<div data-phone={ sponsor.phone } className="phone-container call-phone d-flex align-items-center p-y-10 p-x-10">
														<div className="icon m-r-5">
															<svg className="icon_phone">
																<use xlinkHref="/images/icons.svg#phone" />
															</svg>
														</div>
														<span className="phone weight-600">{ sponsor.phone }</span>
													</div>
												}
											</div>
										</div>
									</Link>
								) }

								{/*ESCORTS*/}
								{ this.state.escortsList.map(escort =>
									<Link key={escort.id} to={ `${this.state.city.url}${this.state.categoryUrl}/${escort.id}?locationId=${this.state.city.id}` } className="card m-b-15 escort-list">
										<div className="image-container">
											<img className="image image-cover" src={`${config.getImagePath('escort')}/${escort.thumbnail}` } alt={ escort.firstName } />
										</div>
										<div className="info-container d-flex justify-content-between flex-column flex-grow-1 p-t-5">
											<div className="d-flex flex-grow-1 flex-column justify-content-between">
												<div className="p-x-10">
													<div className="title-container">
														<span className="title d-block weight-600 m-b-5">{ escort.firstName }</span>
													</div>
													<div className="description-container f-s-16">
														<p className="description color-tertiary">{ escort.title }</p>
													</div>
												</div>
												{ escort.phone &&
													<div data-phone={ escort.phone } className="phone-container call-phone d-flex align-items-center p-y-10 p-x-10">
														<div className="icon m-r-5">
															<svg className="icon_phone">
																<use xlinkHref="/images/icons.svg#phone" />
															</svg>
														</div>
														<span className="phone weight-600">{ escort.phone }</span>
													</div>
												}
											</div>
										</div>
									</Link>
								) }

								{/*NO RESULTS*/}
								{!this.state.escortsTotal &&
									<div className={'color-primary p-y-10 text-center w-100'}>
										No entries found. {filter.count(this.state.filtersSelected) > 0 && <span>Please reset filter.</span>}
									</div>
								}

							</div>

							{this.state.escortsTotal > 0 && <Pagination total={this.state.escortsTotal} offset={this.state.offset} limit={this.state.limit} getData={this.getData}/>}

						</div>
					</div>
				</div>

				<LoadingSpinner loading={this.state.loading} />
			</div>
		);
	}
}
