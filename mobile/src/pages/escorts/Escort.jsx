import React, {Component} from 'react';
import escorts from "../../services/escorts.js";
import Breadcrumbs from "../../components/common/breadcrumbs";
import ProfileCarousel from "../../components/common/profile-carousel";
import Rates from "../../components/common/rates";
import breadcrumbs from "../../services/breadcrumbs.js";
import LoadingSpinner from "../../components/common/loading-spinner";
import {changeTitle} from '../../store/common';
import config from "../../lib/config";
import cities from "../../services/cities";
import position from "../../lib/position";
import Socials from "../../components/common/socials";

export default class Escort extends Component {
  constructor(props) {
    super(props);

    this.state = {
      links: [],
      loading: false,

      escortId: props.match.params.escortId,
      escortCategory: props.match.params.escortCategory,
      
      escort: {},
      city: {},

      info: [
          ['Location', ''],
          ['Age', ''],
          ['Nationality', ''],
          ['Weight', ''],
          ['Hair Color', ''],
          ['Eye Color', ''],
          ['Body Type', ''],
          ['Pregnant', ''],
          ['Kitty', ''],
          ['Massage', ''],
      ],
      availableTo: '',
      fetish: '',
      cards: '',
    };
  }

  getBreadcrumbs = async () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    let locationId = urlParams.get('locationId');

    if (!locationId && this.state.escort && this.state.escort.locationIds) {
      locationId = this.state.escort.locationIds[0];
    }

    this.setState({
      links: await breadcrumbs.getEscortBreadcrumbs(this.state.escortId, locationId)
    });
  }

  getEscort = async () => {
    let escort = await escorts.getById(this.state.escortId);
    changeTitle(escort.firstName || escort.title);

    config.debug('ESCORT', escort);

    // save escort if for position
    await position.savePage('escort');

    this.setState({
      escort: escort
    });
  }

  getCity = async () => {
    const {stateName, cityName} = this.props.match.params;
    let city = await cities.getByUrl(stateName, cityName);
    config.setCity(city);

    this.setState({
      city: city,
    });
  }

  fillInfo = () => {
    let data = [];

    let location = [];
    let locationState = '';
    if (this.state.escort.locations.length) {
      let loc = this.state.escort.locations[0];
      location.push(loc.cityName);
      location.push(loc.countryName);
      if (loc.stateName) {
        locationState = ` (${loc.stateName})`;
      }
    }
    data.push(['Location', location.join(', ') + locationState]);
    data.push(['Age', this.state.escort.age]);
    if (this.state.escort.ethnicityRawValue) {
      data.push(['Nationality', this.state.escort.ethnicity]);
    }

    let height = [];
    if (this.state.escort.heightFeet) {
      height.push(`${this.state.escort.heightFeet}'`);
    }
    if (this.state.escort.heightInches) {
      height.push(`${this.state.escort.heightInches}"`);
    }
    data.push(['Height', height.join(' ')]);

    data.push(['Weight', this.state.escort.weight + ' lbs']);

    if (this.state.escort.rawHairColor) {
      data.push(['Hair Color', this.state.escort.hairColor]);
    }

    if (this.state.escort.rawEyeColor) {
      data.push(['Eye Color', this.state.escort.eyeColor]);
    }

    if (this.state.escort.bodyTypeRawValue) {
      data.push(['Body Type', this.state.escort.bodyType]);
    }

    if (this.state.escort.isPregnant) {
      data.push(['Pregnant', 'Yes']);
    }

    if (this.state.escort.kittyRawValue) {
      data.push(['Kitty', this.state.escort.kitty]);
    }

    if (this.state.escort.kittyRawValue) {
      data.push(['Massage', 'Tantra Massage']);
    }

    this.setState({
      info: data
    });
  }

  fillAvailableTo = () => {
    let data = [];

    if (this.state.escort.isAvailableForMen) {
      data.push('Men');
    }

    if (this.state.escort.isAvailableForWomen) {
      data.push('Women');
    }

    if (this.state.escort.isAvailableForCouple) {
      data.push('Black');
    }

    if (this.state.escort.isAvailableForGroup) {
      data.push('Couple');
    }

    if (this.state.escort.isAvailableForBlack) {
      data.push('Group');
    }

    this.setState({
      availableTo: data.join(', ')
    });
  }

  fillFetish = () => {
    let data = [];

    if (this.state.escort.fetishDominant) {
      data.push('Dominant');
    }

    if (this.state.escort.fetishSubmissive) {
      data.push('Submissive');
    }

    if (this.state.escort.fetishSwitch) {
      data.push('Switch');
    }

    this.setState({
      fetish: data.join(', ')
    });
  }

  fillCards = () => {
    let data = [];

    if (this.state.escort.isVisaAccepted) {
      data.push('Visa/MC');
    }

    if (this.state.escort.isAmexAccepted) {
      data.push('AMEX');
    }

    if (this.state.escort.isDiscoverAccepted) {
      data.push('Discover');
    }

    this.setState({
      cards: data.join(', ')
    });
  }

  async componentDidMount() {
    this.setState({loading: true});

    await this.getEscort();
    await this.getCity();
    await this.getBreadcrumbs();

    this.fillInfo();
    this.fillAvailableTo();
    this.fillFetish();
    this.fillCards();

    this.setState({loading: false});
  }

  render() {

    return (
      <div className="content profile" id="escort-page">

          <Breadcrumbs links={this.state.links}/>

          <div className="profile-carousel-container position-relative bg-white">
            {
              (this.state.escort.images && this.state.escort.images.length) ||
              (this.state.escort.videos && this.state.escort.videos.length)
                  ? (
                      <ProfileCarousel
                        slides={{
                          videos: this.state.escort.videos,
                          images: this.state.escort.images
                        }}
                        srcPrefixImages={config.getImagePath('escort')}
                        srcPrefixVideos={config.getVideoPath('escort')}
                      />
                  ) : (
                      <div className="bg-white p-b-10">
                        <div className="no-photo">
                          <img src="/images/icons/no_photo.svg" alt="Not found"/>
                        </div>
                      </div>
                  )}
          </div>

          <div className="escort-information bg-white p-b-15">
            <div className="container">
              {this.state.escort.firstName ? (
                  <h1 className="weight-700 p-b-10 f-s-18">
                    {this.state.escort.firstName}
                  </h1>
              ): ''}

              {this.state.escort.phone ? (
                  <h2 className="f-s-18 weight-600 contact color-primary">
                    <a className="contact weight-600 color-primary d-inline-flex align-items-center" href={`tel:${this.state.escort.phone}`}>
                      <span className="icon m-r-5">
                        <svg className="icon_twitter" style={{width: "25px", height: "24px"}}>
                            <use xlinkHref="/images/icons.svg#phone"></use>
                        </svg>
                      </span>
                      <span>{this.state.escort.phone}</span>
                    </a>
                  </h2>
              ): ''}

            </div>
          </div>

          <div className="primary-tabs">
            <div className="primary-tabs-nav-container position-relative bg-white p-x-15 m-t-10 bs-primary">
              <ul className="nav nav-tabs scrolling-tabs d-flex flex-nowrap overflow-hidden justify-content-between"
                  id="primary-tab" role="tablist">
                <li className="nav-item d-flex align-items-center">
                  <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center active"
                     id="about-tab" data-toggle="tab" href="#about" role="tab"
                     aria-controls="about" aria-selected="true">About</a>
                </li>
                <li className="nav-item d-flex align-items-center">
                  <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center"
                     id="info-tab" data-toggle="tab" href="#info" role="tab"
                     aria-controls="info" aria-selected="true">Info</a>
                </li>
                <li className="nav-item d-flex align-items-center">
                  <a className="nav-link f-s-16 color-tertiary position-relative p-y-0 d-flex align-items-center"
                     id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                     aria-controls="contact" aria-selected="false">Contact</a>
                </li>
              </ul>
            </div>

            <div className="tab-content primary-tab-content">

              {/*ABOUT TAB*/}
              <div className="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="about-tab">
                <div className="tab-content-container bg-white">
                  <div className="container">
                    <div className="tab-content-container-inner about p-y-20 f-s-18">
                      <div style={{marginBottom: "50px"}}>
                        {this.state.escort.title}
                      </div>
                      <div dangerouslySetInnerHTML={{__html: this.state.escort.content}}/>
                    </div>
                  </div>
                </div>
              </div>

              {/*INFO TAB*/}
              <div className="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
                <div className="tab-content-container bg-white">
                  <div className="container">
                    <div className="tab-content-container-inner info p-y-20">
                      {
                        this.state.info.map((info, i) => {
                          if (info[1]) {
                            return <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10" key={i}>
                              <span className="left m-r-30">{info[0]}</span>
                              <span className="text-right right text-break break-word">{info[1]}</span>
                            </div>
                          } else {
                            return '';
                          }
                        })
                      }

                      {this.state.escort.available ? (
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                            <span className="left m-r-30">Available To</span>
                            <span className="text-right right text-break break-word">{this.state.availableTo}</span>
                          </div>
                      ): ''}

                      {this.state.cards ? (
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                            <span className="left m-r-30">Credit Cards</span>
                            <span className="text-right right text-break break-word">{this.state.cards}</span>
                          </div>
                      ): ''}

                      {
                        this.state.escort.bust &&
                        this.state.escort.waist &&
                        this.state.escort.hip
                            ? (
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                            <span className="left m-r-30">Measurements</span>
                            <span className="text-right right text-break break-word">
                              {this.state.escort.bust} - {this.state.escort.waist} - {this.state.escort.hip}
                            </span>
                          </div>
                      ): ''}

                      {this.state.escort.cupSize ? (
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                            <span className="left m-r-30">Cup size</span>
                            <span className="text-right right text-break break-word">
                                {this.state.escort.cupSize}
                            </span>
                          </div>
                      ): ''}

                      {this.state.escort.penisSize ? (
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                              <span className="left m-r-30">Penis size</span>
                              <span className="text-right right text-break break-word">
                                  {this.state.escort.penisSize}'
                              </span>
                          </div>
                      ): ''}
                    </div>

                    <div className="tab-content-container-inner info p-b-10">
                      <Rates
                        isIncallEnabled={this.state.escort.isIncallEnabled}
                        incallRate={this.state.escort.incallRate}
                        incallRates={[
                          ['1/2 Hour', this.state.escort.incallRateHalfHour],
                          ['1 Hour', this.state.escort.incallRateOneHour],
                          ['2 Hour', this.state.escort.incallRateTwoHours],
                          ['Overnight', this.state.escort.incallRateOvernight],
                        ]}
                        isOutcallEnabled={this.state.escort.isIncallEnabled}
                        outcallRate={this.state.escort.incallRate}
                        outcallRates={[
                          ['1/2 Hour', this.state.escort.outcallRateHalfHour],
                          ['1 Hour', this.state.escort.outcallRateOneHour],
                          ['2 Hour', this.state.escort.outcallRateTwoHours],
                          ['Overnight', this.state.escort.outcallRateOvernight],
                        ]}
                      />
                    </div>
                  </div>
                </div>
              </div>

              {/*CONTACT TAB*/}
              <div className="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <div className="tab-content-container bg-white">
                  <div className="container">
                    <div className="tab-content-container-inner contact p-y-20 ">
                      {this.state.escort.phone &&
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                            <span className="left m-r-30">Phone</span>
                            <span className="text-right right text-break break-word">
                                <a className="color-tertiary" href={`tel:${this.state.escort.phone}`}>
                                    {this.state.escort.phone}
                                </a>
                            </span>
                          </div>
                      }

                      {(config.useEscortEmail() && this.state.escort.email) &&
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                            <span className="left m-r-30">Email</span>
                            <span className="text-right right text-break break-word">
                                <a className="color-tertiary" href={`mailto:${this.state.escort.email}`}>
                                    {this.state.escort.email}
                                </a>
                            </span>
                          </div>
                      }

                      {this.state.escort.website &&
                          <div className="data-list-item d-flex align-items-center justify-content-between p-b-5 m-b-10">
                            <span className="left m-r-30">Website</span>
                            <span className="text-right right text-break break-word">
                                <a className="color-tertiary" rel="noopener noreferrer" target={'_blank'} href={this.state.escort.website}>
                                  {this.state.escort.website}
                                </a>
                            </span>
                          </div>
                      }

                      <Socials values={this.state.escort} />

                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <LoadingSpinner loading={this.state.loading} />
      </div>
    )
  }
}
