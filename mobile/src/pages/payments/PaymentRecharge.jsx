import React, {Component} from 'react';
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import {history} from "../../index";
import Select from "../../components/form/select";
import ButtonSubmit from "../../components/form/button-submit";
import axios from "../../services/api_init";
import db from "../../istore";

export default class PaymentRecharge extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            price: 1,
            prices: []
        };
    }

    setPrice = event => {
        event.preventDefault();
        this.setState({
            price: event.target.value
        });
    }

    submit = event => {
        event.preventDefault();

        let params = {
            'paymentType': 'recharge',
            'recharge': this.state.price,
            'src': 'recharge',
        };

        history.push(config.getPaymentUrl(params));
    }

    getListPrices = async () => {
        try {
            //const res = await axios.get(`/api/v1/options/ethnicity`);
            //let list = res.data.list;

            let list = [
                {value: 1, name: '$20.00'},
                {value: 2, name: '$50.00'},
                {value: 3, name: '$80.00'},
                {value: 4, name: '$100.00'},
                {value: 5, name: '$150.00'},
                {value: 6, name: '$200.00'},
            ];

            this.setState({
                prices: list
            });
        } catch (error) {
            config.catch(error);
        }
    }

    async componentDidMount() {
        await this.getListPrices()
    }

    render() {
        return (
            <div className="content buy-balance bg-seventh-gray">
                <div className="buy-top-ups-modal-head">
                    <div className="bg-primary color-white p-y-20">
                        <div className="container">
                            <h2 className="weight-700 lh-25 m-b-25 p-x-25 text-center">
                                Recharge your account balance
                            </h2>

                            <div className="paragraphs m-b-20 text-center">
                                <p>
                                    Recharge your account balance (budget), so you can buy classified ads or ad upgrades.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="p-y-20 flex-grow-1">
                        <div className="container">

                            <form action="#" method="POST" onSubmit={this.submit}>

                                <Select
                                    label={'Choose amount'}
                                    //classes={'m-b-0'}
                                    disabledOption={'Please choose'}
                                    id={'amount'}
                                    name={'amount'}
                                    data={this.state.prices}
                                    handleChange={this.setPrice}
                                    required={true}
                                    //value={this.state.escort.heightInches}
                                />

                                <ButtonSubmit disabled={!window.navigator.onLine} classes={'success'}/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
