import React, {Component} from 'react';
import axios from '../../services/api_init';
import Help from "../../components/text/help";
import Steps from "../adbuild/steps";
import adbuild from "../../lib/adbuild";
import LoadingSpinner from "../../components/common/loading-spinner";
import auth from "../../lib/auth";
import {changeTitle} from '../../store/common';
import config from "../../lib/config";
import business from "../../lib/business";
import Input from "../../components/form/input";
import Alert from "../../components/common/alert";
import form from "../../lib/form";
import Select from "../../components/form/select";
import ButtonSubmit from "../../components/form/button-submit";
import places from "../../services/places";
import escorts from "../../services/escorts";
import Modal from "../../components/common/modal";
import ButtonBordered from "../../components/form/button-bordered";
import payment from "../../lib/payment";
import FormCard from "../../components/common/FormCard";
import locations from "../../lib/locations";

export default class PaymentForm extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            loading: false,
            loadingPay: false,
            errorMessage: false,
            successMessage: false,

            intervalHandle: null,

            countries: [],
            place: {},
            escort: {},
            budget: 0,

            account: {},
            errors: {},

            countryDefault: 16046, // US by default

            // payment options available
            paymentTypes: ['budget','bc','cc'],
            paymentTypeBC: true,
            paymentTypeCC: true,
            paymentTypeBUDGET: true,
            paymentActive: 'bc', //default payment option opened

            payment: {
                ccFirstname: null,
                ccLastname: null,
                ccAddress: null,
                ccCity: null,
                ccState: null,
                ccZipcode: null,
                ccCountry: null,
                ccCc: null,
                ccMonth: null,
                ccYear: null,
                ccCvc2: null,
                paymentType: null,
                topUps: null,
                recurringPeriod: null,
            },

            // TODO: get real data from api
            // TEST DATA !!!
            bcAddress: '23r23f23f23g23g',
            bcAmount: '0.77777',
            bcAmountArrived: '0.22222',
            bcTransactions: ['11111111', '222222222'],
            bcStatus: 'Waiting for payment',

            checkPaymentCount: 0,
            checkPaymentMax: 5,

            promocodeError: false,
            promocodeSuccess: false,

            totalCost: false, // only for visual user info
            totalCostOrigin: false,
            totalCostError: false,

            src: new URLSearchParams(window.location.search).get('src'),
        };
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    getTotalCost = async (checkPromoCode = false) => {
        try {
            let params = payment.getParams(this.state.account.id);
            const res = await axios.get(`/api/v1/payments/total_cost`, {params});

            this.setState({
                totalCost: res.data.totalCost.toFixed(2),
                totalCostError: false,
            });

            return false;
        } catch (error) {
            config.catch(error);

            this.setState({
                loading: false,
                loadingPay: false,
            });

            if (!checkPromoCode) {
                // do not print general total cost err
                this.setState({
                    totalCostError: true,
                    errorMessage: 'Error calculating payment amount. Please contact support.',
                });
            }

            return error;
        }
    }

    checkPromocode = async (event) => {
        event.preventDefault();

        this.setState({
            promocodeError: false,
            promocodeSuccess: false,
        });

        if (!payment.getPromocode()) {
            this.setState({
                totalCost: this.state.totalCostOrigin, // revert amount
                promocodeError: 'Please enter valid promocode',
            });
            return null;
        }

        await this.getTotalCost(true)
            .then(error => {
                if (!error) {
                    if (parseFloat(this.state.totalCostOrigin) > parseFloat(this.state.totalCost)) {
                        this.setState({
                            promocodeError: false,
                            promocodeSuccess: 'Promocode successfully applied',
                        });
                    }
                } else {
                    let errorText = (error.response.data)? error.response.data.message : 'Promocode not valid';
                    if (error.response.status === 404) {
                        errorText = 'Promocode not found';
                    } else if (error.response.status === 400) {
                        errorText = 'Promocode not valid or expired';
                    }

                    this.setState({
                        totalCost: this.state.totalCostOrigin,  // revert amount
                        promocodeSuccess: false,
                        promocodeError: errorText,
                    });
                }
            })
    }

    fillPayment = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value.trim();
        let res = {[name]: value};

        if (name === 'ccCc') {
            // remove visual spaces from cc number
            res = {[name]: value.replace(/\s/g,'')};
        }

        this.setState({
            payment: Object.assign(this.state.payment, res),
            errors: Object.assign(this.state.errors, {[name]: false})
        });
    }

    validateBUDGET = () => {
        if (this.state.budget < this.state.totalCost) {
            this.setState({
                errorMessage: 'Your account balance is not enough to complete this purchase.'
            });
            return false;
        }

        return true;
    }

    validateCC = () => {
        let ok = true;

        // validate common inputs & selects
        let validate = {
            'ccFirstname': 'You must specify your first name',
            'ccLastname': 'You must specify your last name',
            'ccAddress': 'You must specify your address',
            'ccCity': 'You must specify your city name',
            'ccState': 'You must specify state or province',
            'ccZipcode': 'You must specify zip or postal code',
            'ccCountry': 'You must select your country',
            'ccCc': 'You must enter valid card number',
            'ccMonth': 'Invalid card month',
            'ccYear': 'Invalid card year',
            'ccCvc2': 'Invalid card CVV/CVC',
        };

        let [errors, errorsCount] = form.validateErrors(validate);
        this.setState({
            errors: errors
        });
        if (errorsCount) {
            ok = false;
        }

        return ok;
    }

    validatePayType = (payType) => {

        // validate pay type - is implemented
        if (this.state.paymentTypes.indexOf(payType) === -1) {
            this.setState({
                errorMessage: 'Payment type not implemented.'
            });
            return false;
        }

        // validate pay type - is active
        let stateKey = payType.toUpperCase();
        if (!this.state[`paymentType${stateKey}`]) {
            this.setState({
                errorMessage: 'Payment type not active.'
            });
            return false;
        }

        return true;
    }

    payBUDGET = async () => {
        try {
            const res = await axios.post('/payments/budget/write_off', this.state.payment);
            config.debug('BUDGET', res);

            if (res.data.id) {
                this.success();
            }
        } catch (error) {
            config.catch(error);

            this.setState({
                loadingPay: false,
                errorMessage: 'Error creating budget payment. Please try again later.',
            });
        }
    }

    payCC = async () => {
        try {
            const res = await axios.post('/api/v1/payments/pay', this.state.payment);
            config.debug('CC', res);

            const handle = setInterval(() => this.payCCCheck(res.data.id),3000);
            this.setState({
                intervalHandle: handle,
            })
        } catch (error) {
            config.catch(error);

            this.setState({
                loadingPay: false,
                errorMessage: 'Error creating credit card payment. Please try again later.',
            });
        }
    }

    payCCCheck = async paymentId => {
        // not success, tries finished
        if (this.state.checkPaymentCount === this.state.checkPaymentMax) {
            if (!this.state.errorMessage) {
                this.setState({
                    errorMessage: 'Payment processing error. Please try again later.',
                });
            }

            clearInterval(this.state.intervalHandle);

            this.setState({
                loadingPay: false,
            });

            return;
        }

        // try to pay
        try {
            const res = await axios.get(`/api/v1/payments/${paymentId}`);
            const pay = res.data;
            config.debug('CC check ' + this.state.checkPaymentCount, pay);

            if (pay.result === 'A') {
                this.success();
                return;
            }

            if (pay.result === 'D') {
                clearInterval(this.state.intervalHandle);

                this.setState({
                    loadingPay: false,
                    errorMessage: 'Sorry, your payment was declined. Please check payment details or try another card.',
                });

                return;
            }
        } catch (error) {
            config.catch(error);

            clearInterval(this.state.intervalHandle);

            this.setState({
                loadingPay: false,
                errorMessage: 'Error checking payment status',
            });
        }

        // increment try count
        let count = this.state.checkPaymentCount;
        this.setState({
            checkPaymentCount: ++count
        });
    }

    success = () => {
        clearInterval(this.state.intervalHandle);

        // clear business & escort unfinish cookies
        adbuild.clearCookie(true);
        business.clearCookieFinish();

        this.setState({
            loadingPay: false,
            successMessage: 'Congratulations! Your payment was successfully completed. Please allow a few minutes for site updates.',
        });

        // success, redirect to account
        setTimeout(async () => {
            window.location.href = auth.getLink('account');
        }, 3000)
    }

    submit = async (event) => {
        event.preventDefault();
        let payType = event.currentTarget.dataset.pay;

        this.setState({
            errors: {},
            errorMessage: null,
            successMessage: null,
            promocodeError: false,
            checkPaymentCount: 0,
        });

        await this.getTotalCost();

        // validate pay type
        if (!this.validatePayType(payType)) {
            return false;
        }

        // validate params
        if (!this[`validate${payType.toUpperCase()}`]()) {
            return false;
        }

        this.setState({
            loadingPay: true,
        });

        let params = payment.getParams(this.state.account.id);
        this.setState({
            payment: Object.assign(this.state.payment, params)
        });

        // process pay method
        await this[`pay${payType.toUpperCase()}`]();
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await auth.getAccount()
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })

        await this.getTotalCost();
        this.setState({ totalCostOrigin: this.state.totalCost });

        this.setState({
            countries: await locations.getCountries(0, 500),
            budget: await payment.getBudget(),
        });

        if (this.state.src === 'place') {
            this.setState({
                place: await places.getById(payment.getUrlParam('placeId'), true)
            });
        }

        if (this.state.src === 'escortPay') {
            this.setState({
                escort: await escorts.getById(payment.getUrlParam('escortId'), true)
            });
        }

        // set default country
        this.setState({
            payment: Object.assign(this.state.payment, {'ccCountry': this.state.countryDefault}),
        });

        this.setState({ loading: false });
    }

    isOnlyPayment(type) {
        switch (type) {
            case 'bc':
                if (!this.state.paymentTypeCC && !this.state.paymentTypeBUDGET) {
                    return true;
                }
            case 'cc':
                if (!this.state.paymentTypeBC && !this.state.paymentTypeBUDGET) {
                    return true;
                }
            case 'budget':
                if (!this.state.paymentTypeBC && !this.state.paymentTypeCC) {
                    return true;
                }
            default:
                return false;
        }
    }

    renderQRCode() {
        // TODO: generate qr code img src from api
        let imgSrc = '/images/qr.svg';
        return (
            <div className={'text-center m-b-10'}>
                <img src={imgSrc} alt=""/>
            </div>
        )
    }

    renderPaymentBC() {
        let buyLinks = [
            {
                'title': 'Cash.App',
                'icon': 'cashapp.svg',
                'link': 'https://cash.app/'
            },
            {
                'title': 'Coinbase.com',
                'icon': 'coinbase.svg',
                'link': 'https://www.coinbase.com/'
            },
            {
                'title': 'Abra.com',
                'icon': 'abra.png',
                'link': 'https://www.abra.com/'
            },
            {
                'title': 'Bitcoin.com',
                'icon': 'bitcoin.png',
                'link': 'https://www.bitcoin.com/'
            },
            {
                'title': 'BRD',
                'icon': 'brd.png',
                'link': 'https://brd.com/'
            },
            {
                'title': 'Bitcoin ATM Machines',
                'icon': false,
                'link': 'https://coinatmradar.com/bitcoin-atm-near-me/'
            },
        ];
        let active = (this.state.paymentActive === 'bc' || this.isOnlyPayment('bc'))? true : false

        if (this.state.paymentTypeBC) {
            return (
                <div className={`tab-pane fade ${active? 'show active' : ''}`} id="bitcoin" role="tabpanel" aria-labelledby="bitcoin-tab">

                    <form method="post" action="#" onSubmit={this.submit} data-pay={'bc'} id="payment_form">

                        <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className={'container'}>

                                {this.renderQRCode()}

                                {/*BC PAYMENT INFO*/}
                                <div className={'p-0'}>
                                    <p className={'text-center p-b-5'}>Please send <span className={'weight-600'}>{this.state.bcAmount}</span> BTC to this address</p>
                                    <Input
                                        classes={'p-b-15'}
                                        label={'Bitcoin Address'}
                                        id={'bcAddress'}
                                        name={'bcAddress'}
                                        error={this.state.errors.bcAddress}
                                        value={this.state.bcAddress}
                                        icon_copy={true}
                                        readonly={true}
                                    />
                                    <Input
                                        classes={'p-b-15'}
                                        label={'Amount'}
                                        id={'bcAmount'}
                                        name={'bcAmount'}
                                        error={this.state.errors.bcAmount}
                                        value={this.state.bcAmount}
                                        icon_copy={true}
                                        readonly={true}
                                    />
                                </div>

                                {/*BC TRANSACTION STATUS*/}
                                <div className={'p-y-15 f-s-16'}>
                                    <div className="data-list-item d-flex justify-content-between p-b-5 m-b-10">
                                        <span className="left">Amount to Pay</span>
                                        <span className="weight-700 right text-right m-l-5">{this.state.bcAmount} BTC</span>
                                    </div>

                                    {this.state.bcAmount !== this.state.bcAmountArrived &&
                                    <>
                                        <div className="data-list-item d-flex justify-content-between p-b-5 m-b-10">
                                            <span className="left">Arrived Amount</span>
                                            <span className="weight-700 right text-right m-l-5">{this.state.bcAmountArrived} BTC</span>
                                        </div>
                                        <div className="data-list-item d-flex justify-content-between p-b-5 m-b-10">
                                            <span className="left">Remains to Pay</span>
                                            <span className="weight-700 right text-right m-l-5">{this.state.bcAmount - this.state.bcAmountArrived} BTC</span>
                                        </div>
                                    </>
                                    }

                                    {this.state.bcTransactions.length > 0 &&
                                    <div className="data-list-item d-flex justify-content-between p-b-5 m-b-10">
                                        <span className="left">Transactions</span>
                                        <span className="weight-700 right text-right m-l-5">{this.state.bcTransactions.join(', ')}</span>
                                    </div>
                                    }

                                    <div className="data-list-item d-flex justify-content-between p-b-5 m-b-10">
                                        <span className="left">Status</span>
                                        <span className="weight-700 right text-right m-l-5 d-flex align-items-center" id="status">
                                    <img className={'m-r-5'} src="/images/processing.gif" width="30" height="30"/>
                                            {this.state.bcStatus}
                                </span>
                                    </div>
                                </div>

                                <Alert
                                    type={'warning'}
                                    classes={'f-s-16 m-b-10'}
                                    transparent={true}
                                    closable={false}
                                    message={'Please note, that if you use Bitcoin ATM to send money, the amount that arrives to your budget will be lessened by bitcoin processing fee and ATM service fee, the sum of these fees can be as much as 15%.'}
                                />

                                {/*BC BUY LINKS*/}
                                <div className={'p-t-15'}>
                                    <h3 className="weight-600 p-b-5">How do I buy Bitcoin?</h3>
                                    <p className={'m-b-10'}>You can buy bitcoin from several places</p>

                                    {buyLinks.map((item, index) => {
                                        return (
                                            <a key={index} target={'_blank'} href={item.link} className="data-list-item color-tertiary d-flex justify-content-between p-b-5 m-b-10">
                                                <span className="left p-y-5">
                                                    {item.title}
                                                    {item.icon && <img className={'m-l-5'} src={`/images/icons/bc_buy/${item.icon}`} alt={item.title}/>}
                                                </span>
                                                <div className="right arrow-right p-r-5">
                                                    <svg className="arrow-right-grey-icon">
                                                        <use xlinkHref="/images/icons.svg#arrow-right-grey"/>
                                                    </svg>
                                                </div>
                                            </a>
                                        )
                                    })
                                    }

                                    <Alert
                                        type={'success'}
                                        classes={'text-center m-t-15'}
                                        closable={false}
                                        message={'These are just a few places to buy bitcoin. There are thousands of places to purchase bitcoin in your area.'}
                                    />
                                </div>

                                {/*PAY LATER LINK*/}
                                {/*TODO: create logic for save & get pay later*/}
                                {/*<div className={'p-t-5 text-center'}>Don’t have Bitcoin now? <span className={'weight-700 color-success'}>Pay Later</span></div>*/}

                            </div>
                        </div>

                    </form>

                </div>
            )
        }

        return '';
    }

    renderPaymentCC() {
        let active = (this.state.paymentActive === 'cc' || this.isOnlyPayment('cc'))? true : false;

        if (this.state.paymentTypeCC) {
            return (
                <div className={`tab-pane fade ${active ? 'show active' : ''}`} id="creditcard" role="tabpanel" aria-labelledby="creditcard-tab">

                    <form method="post" action="#" onSubmit={this.submit} data-pay={'cc'} id="payment_form">

                        {/*PERSONAL INFO*/}
                        <div className="payment-form__personal wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                <div className="ad-creation-container">

                                    <h2 className="weight-700 color-primary p-b-10">Personal Details</h2>

                                    <Input
                                        classes={'p-b-15'}
                                        label={'First name'}
                                        id={'ccFirstname'}
                                        name={'ccFirstname'}
                                        placeholder={'Enter first name here'}
                                        error={this.state.errors.ccFirstname}
                                        //required={true}
                                        required_icon={true}
                                        maxCharacters={100}
                                        handleChange={this.fillPayment}
                                        data={{'data-pattern': '[A-Za-z]+'}}
                                        tabindex={2}
                                    />

                                    <Input
                                        classes={'p-b-15'}
                                        label={'Last name'}
                                        id={'ccLastname'}
                                        name={'ccLastname'}
                                        placeholder={'Enter last name here'}
                                        error={this.state.errors.ccLastname}
                                        //required={true}
                                        required_icon={true}
                                        maxCharacters={100}
                                        handleChange={this.fillPayment}
                                        data={{'data-pattern': '[A-Za-z]+'}}
                                        tabindex={3}
                                    />

                                    <Input
                                        classes={'p-b-15'}
                                        label={'Address'}
                                        id={'ccAddress'}
                                        name={'ccAddress'}
                                        placeholder={'Enter address here'}
                                        error={this.state.errors.ccAddress}
                                        //required={true}
                                        required_icon={true}
                                        maxCharacters={255}
                                        handleChange={this.fillPayment}
                                        data={{'data-pattern': '[\\w-\\s]+'}}
                                        tabindex={4}
                                    />

                                    <Input
                                        classes={'p-b-15'}
                                        label={'City'}
                                        id={'ccCity'}
                                        name={'ccCity'}
                                        placeholder={'Enter city here'}
                                        error={this.state.errors.ccCity}
                                        //required={true}
                                        required_icon={true}
                                        maxCharacters={100}
                                        handleChange={this.fillPayment}
                                        data={{'data-pattern': '[A-Za-z-\\s]+'}}
                                        tabindex={5}
                                    />

                                    <Input
                                        classes={'p-b-15'}
                                        label={'State/Province'}
                                        id={'ccState'}
                                        name={'ccState'}
                                        placeholder={'Enter state or province here'}
                                        error={this.state.errors.ccState}
                                        //required={true}
                                        required_icon={true}
                                        maxCharacters={100}
                                        handleChange={this.fillPayment}
                                        data={{'data-pattern': '[A-Za-z-\\s]+'}}
                                        tabindex={6}
                                    />

                                    <Input
                                        classes={'p-b-15'}
                                        label={'ZIP/Postal Code'}
                                        id={'ccZipcode'}
                                        name={'ccZipcode'}
                                        placeholder={'Enter ZIP/Postal code here'}
                                        error={this.state.errors.ccZipcode}
                                        //required={true}
                                        required_icon={true}
                                        maxCharacters={20}
                                        handleChange={this.fillPayment}
                                        data={{'data-pattern': '[\\w-\\s]+'}}
                                        tabindex={7}
                                    />

                                    <Select
                                        classes={'m-b-0'}
                                        label={'Country'}
                                        disabledOption={'Select country'}
                                        id={'ccCountry'}
                                        name={'ccCountry'}
                                        data={this.state.countries}
                                        //required={true}
                                        required_icon={true}
                                        handleChange={this.fillPayment}
                                        error_message={this.state.errors.ccCountry}
                                        tabindex={8}
                                        value={this.state.countryDefault}
                                    />

                                </div>
                            </div>
                        </div>

                        {/*PAYMENT INFO*/}
                        <div className="payment-form__payment wrapper-ad-creation-container bg-white p-y-15">
                            <div className="container">
                                <div className="ad-creation-container">

                                    <FormCard
                                        title={'Payment Details'}
                                        alert={'Info about prepaid/gift cards: We do NOT accept Vanilla Cards, we DO accept NetSpend cards'}
                                        handleChange={this.fillPayment}
                                        errors={this.state.errors}
                                        tabIndex={[9,10,11,12]}
                                    />

                                    <ButtonSubmit disabled={!window.navigator.onLine} classes={'success'} text={'Pay'}/>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            )
        }

        return '';
    }

    renderPaymentBudget() {
        let active = (this.state.paymentActive === 'budget' || this.isOnlyPayment('budget'))? true : false;

        if (this.state.paymentTypeBUDGET) {
            return (
                <div className={`tab-pane fade ${active ? 'show active' : ''}`} id="budget" role="tabpanel" aria-labelledby="budget-tab">

                    <form method="post" action="#" onSubmit={this.submit} data-pay={'budget'} id="payment_form">

                        {this.renderPromocode()}

                        <div className="payment-form__personal wrapper-ad-creation-container bg-white p-y-15 m-b-10">
                            <div className="container">
                                {this.state.budget < this.state.totalCost ? (
                                    <>
                                        <p>Your account balance is <span className={'weight-600 color-success'}>${this.state.budget}</span> and is not enough to complete this purchase.</p>
                                        <p>Please add balance to your account to continue.</p>
                                        <ButtonSubmit link={auth.getLink('recharge')} classes={'m-y-15'} text={'Recharge Account Balance'}/>
                                    </>
                                ) : (
                                    <>
                                        <p>Currently, your account balance is <span className={'weight-600 color-success'}>${this.state.budget}</span>.</p>
                                        <p>After payment, your account balance will be <span className="weight-600 color-success">${this.state.budget - this.state.totalCost}</span>.</p>
                                        <ButtonSubmit classes={'m-y-15 success'} text={'Pay From Account Balance'}/>
                                    </>
                                )}
                            </div>
                        </div>

                    </form>

                </div>
            )
        }

        return '';
    }

    renderPromocode() {
        return (
            <div className="wrapper-ad-creation-container bg-white p-y-15 m-b-10">
            <div className="container">
                <div className="ad-creation-container">
                    <h2 className="weight-700 color-primary p-b-10">Promocode</h2>
                    <p>
                        If you have a promocode, enter it in following field and tap 'Apply' button.
                    </p>
{/*                    <p>
                        If you don't have any promocode, you can ignore this and fill out the billing details below and tap 'Pay' button.
                    </p>*/}
                    <Input
                        classes={'m-t-10 p-b-5'}
                        id={'promocode'}
                        name={'promocode'}
                        type={'text'}
                        //value={}
                        maxCharacters={255}
                        label={'Promocode'}
                        placeholder={'Enter promocode here'}
                        error={this.state.promocodeError}
                        success={this.state.promocodeSuccess}
                        button={true}
                        buttonText={'Apply'}
                        buttonClasses={'success'}
                        buttonHandleClick={this.checkPromocode}
                        tabindex={1}
                    />
                </div>
            </div>
        </div>
        )
    }

    renderInfoMessage() {
        return (
            <div className={`wrapper-ad-creation-container bg-white p-y-15 m-b-10`}>
                <div className="container">

                    <div className="container bg-white">
                        <ButtonBordered
                            text={'Important Payment Info'}
                            data={{'data-toggle':'modal', 'data-target': '#payment-info-modal'}}
                        />
                    </div>

                    <Modal
                        id={'payment-info-modal'}
                        title={'Payment Info'}
                        fullScreen={true}
                        headerArrow={true}
                        content={
                            <div className="payment-info">
                                <div className={'payment-info__text'}>
                                    <p className={'m-b-10'}>Currently, only Bitcoin payments are accepted. Due to the high level of fake ads on the site we are banning thousands of accounts over the next 2 weeks.</p>
                                    <p className={'m-b-10'}>You will receive a link to video verify your ad soon. Make sure to click the link &amp; follow the instructions. All ads will stay live free of charge until we've finished our investigations. Your ads are being topped up once a day for free.</p>
                                    <p className={'m-b-10'}>This is an automatic process don't ask to pick a certain time. We can't &amp; won't do it. Once all the fake ads &amp; spammers are removed everything will be back to normal. This will insure real ads receive many more clients.</p>
                                    Thank you for your patience.
                                </div>
                                <div className={'payment-info__text'}>
                                    <p className={'m-b-10'}>En este momento los pagos en Bitcoin estan aceptando. Todo esto es debido al incremento de anuncios falsos en la pagina web, estamos betando muchas cuentas en los próximas 2 semanas.</p>
                                    <p className={'m-b-10'}>Recibirás un link pronto para el video de verificación de tu anuncio. Asegúrate de darle click y seguir las instrucciones.  Todos los anuncios estarán libres de cargos hasta que tengamos terminado nuestras investigaciones. Los perfiles ha sido topped up una vez al día totalmente gratis.</p>
                                    <p className={'m-b-10'}>Este es un proceso automático, pediremos el favor de no preguntar para escoger un tiempo estimado. No podemos y no lo haremos. Una vez todos los anuncios falsos y los spammers hayan sido removidos todo volverá a la normalidad. Esto hará que los anuncios reales reciban más clientes.</p>
                                    Muchas gracias por su paciencia.
                                </div>
                            </div>
                        }
                    />

                </div>
            </div>
        )
    }

    renderProductMessage() {
        return (
        <>
            {this.state.src === 'escort' &&
                <div className="bg-white m-b-10 p-y-20 p-b-10">
                    <Steps
                        steps={['Category', 'Your Location', 'About Ad', 'Checkout']}
                        active={4}
                    />
                </div>
            }

            {(this.state.src === 'escortPay' && this.state.escort.id) &&
                <div className='wrapper-ad-creation-container bg-white p-y-15 m-b-10'>
                    <div className="container">
                        <p>You are going to pay for your ad <span className={'weight-600'}>{this.state.escort.title}</span> in <span className={'weight-600'}>{this.state.escort.type}</span></p>
                    </div>
                </div>
            }

            {(this.state.src === 'place' && this.state.place.id) &&
                <div className='wrapper-ad-creation-container bg-white p-y-15 m-b-10'>
                    <div className="container">
                        <p>You are going to buy Owner Package for <span className={'weight-600'}>{this.state.place.name}, {this.state.place.phone1}</span></p>
                    </div>
                </div>
            }

            {this.state.src === 'topups' &&
                <div className='wrapper-ad-creation-container bg-white p-y-15 m-b-10'>
                    <div className="container">
                        <p>You are going to buy <span className={'weight-600'}>{payment.getUrlParam('topUps')} top-up credits</span> for your ads</p>
                    </div>
                </div>
            }

            {this.state.src === 'recharge' &&
                <div className='wrapper-ad-creation-container bg-white p-y-15 m-b-10'>
                    <div className="container">
                        <p>You are going to <span className={'weight-600'}>recharge your balance</span></p>
                        <ButtonBordered classes={'m-t-10'} link={auth.getLink('recharge')} text={'Change amount'}/>
                    </div>
                </div>
            }

        </>
        )
    }

    render() {
        return (
          <div className="content payment" id="payment-form">

            {this.state.errorMessage && <Alert closable={true} message={this.state.errorMessage} close={this.closeAlert}/>}
            {this.state.successMessage && <Alert closable={true} message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

            {this.renderProductMessage()}

            {this.state.totalCost &&
                <div className={`wrapper-ad-creation-container bg-white p-y-15 ${this.state.src? 'm-b-10' : 'm-y-10'}`}>
                    <div className="container">
                        <h2 className="weight-700 color-primary">You will be charged:
                            <span className={'color-success m-l-10'}>${this.state.totalCost}</span>
                            {(parseFloat(this.state.totalCostOrigin) > parseFloat(this.state.totalCost)) &&
                                <span className={'color-error weight-400 text-line-through m-l-10 f-s-16'}>${this.state.totalCostOrigin}</span>
                            }
                        </h2>
                    </div>
                </div>
            }

            {!this.state.totalCostError &&
              <>
                  {/*PAY OPTIONS TABS*/}
                  <div className={`wrapper-ad-creation-container bg-white p-t-15 m-b-10`}>

                      {(this.isOnlyPayment('bc') || this.isOnlyPayment('cc') || this.isOnlyPayment('budget')) ?
                          (<h2 className="weight-700 color-primary m-b-10 container">Available payment method:</h2>) :
                          (<h2 className="weight-700 color-primary m-b-10 container">Please select payment method:</h2>)
                      }

                      {/*TABS*/}
                      <div className="primary-tabs-nav-container position-relative">
                          <ul className="nav nav-tabs scrolling-tabs border-tabs d-flex flex-nowrap" id="primary-tab" role="tablist">
                              {this.state.paymentTypeBC &&
                                  <li className="nav-item d-flex align-items-center flex-fill">
                                      <a className={`nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1 ${this.state.paymentActive === 'bc' ? 'active' : ''}`}
                                         id="bitcoin-tab" data-toggle="tab" href="#bitcoin" role="tab"
                                         aria-controls="bitcoin" aria-selected="true">
                                          <img className={'m-r-5'} src="/images/icons/pay_option/bitcoin.png" alt="BC"/>
                                          Bitcoin
                                      </a>
                                  </li>
                              }
                              {this.state.paymentTypeCC &&
                                  <li className="nav-item d-flex align-items-center flex-fill">
                                      <a className={`nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1 ${this.state.paymentActive === 'cc' ? 'active' : ''}`}
                                         id="creditcard-tab" data-toggle="tab" href="#creditcard" role="tab"
                                         aria-controls="creditcard" aria-selected="false">
                                          <img className={'m-r-5'} src="/images/icons/pay_option/creditcard.png" alt="CC"/>
                                          Credit Card
                                      </a>
                                  </li>
                              }
                              {this.state.paymentTypeBUDGET &&
                                  <li className="nav-item d-flex align-items-center flex-fill">
                                      <a className={`nav-link f-s-16 weight-600 color-tertiary position-relative p-y-0 d-flex align-items-center justify-content-center flex-grow-1 ${this.state.paymentActive === 'budget' ? 'active' : ''}`}
                                         id="budget-tab" data-toggle="tab" href="#budget" role="tab"
                                         aria-controls="budget" aria-selected="false">
                                          <img className={'m-r-5'} src="/images/icons/pay_option/budget.png" alt="ACC"/>
                                          Account Balance
                                      </a>
                                  </li>
                              }
                          </ul>
                      </div>
                  </div>

                  {/*PAY OPTIONS CONTENT*/}
                  <div className="tab-content primary-tab-content">
                      {this.renderPaymentBC()}

                      {this.renderPaymentCC()}

                      {this.renderPaymentBudget()}
                  </div>
              </>
            }

            {this.renderInfoMessage()}

            <Help classes={'m-t-10'}/>

            <LoadingSpinner loading={this.state.loading}/>
            <LoadingSpinner loading={this.state.loadingPay} payment={true}/>
          </div>
        )
    }
}
