import React, {Component} from 'react';
import axios from '../../services/api_init';
import Input from "../../components/form/input";
import ButtonSubmit from "../../components/form/button-submit";
import Alert from "../../components/common/alert";
import auth from "../../lib/auth";
import { changeTitle } from '../../store/common';
import config from "../../lib/config";
import form from "../../lib/form";

export default class ContactUs extends Component {
    constructor(props) {
        super(props);

        changeTitle(config.getTitle(this));

        this.state = {
            errorMessage: null,
            successMessage: null,
            referenceUrl: document.referrer,
            account: {},
            errors: {},
        };
    }

    async componentDidMount() {
        await auth.getAccount(true)
          .then(account => {
              if (account) {
                  this.setState({
                      account: account,
                  });
              }
          })
    }

    /**
     * Validate common inputs & selects
     */
    validate = () => {
        let ok = true;

        let errors = {
            'name': 'You must include your name',
            'phone': 'You must include valid phone',
            'message': 'You must write some text',
        };

        for (let id in errors) {
            if (document.getElementById(id).value) {
                errors[id] = false
            } else {
                ok = false;
            }
        }

        let emailError = form.validateEmail(document.getElementById('email').value);
        if (emailError) {
            errors['email'] = emailError;
            ok = false;
        }

        this.setState({
            errors: errors
        });

        return ok;
    }

    submit = async (event) => {
        event.preventDefault();

        this.setState({
            errors: {},
            errorMessage: null,
            successMessage: null,
        });

        // validate form inputs
        if (!this.validate()) {
            return;
        }

        const form = event.currentTarget;
        const formData = new FormData(form);

        formData.set('referenceUrl', this.state.referenceUrl);
        formData.set('accountId', this.state.account.id);

        try {
            await axios.post('/api/v1/contact_form', formData);

            this.setState(
                {
                    successMessage : 'Your message was sent to us.'
                })
        } catch (error) {
            config.catch(error);

            this.setState({
                errorMessage: 'Error sending email. Please try again later.'
            });
        }
    }

    closeAlert = () => {
        this.setState({
            errorMessage: null,
            successMessage: null,
        });
    }

    render() {
        return (
            <>
            {this.state.errorMessage && <Alert message={this.state.errorMessage} close={this.closeAlert} type={'error'}/>}
            {this.state.successMessage && <Alert message={this.state.successMessage} close={this.closeAlert} type={'success'}/>}

            <div className="content contact-us bg-white m-b-20" id="contact-us-page">
                <form action="" method="POST" onSubmit={this.submit}>
                <div className="container">
                    <div className="contact-us-container p-t-20">
                        <Input
                            label={'Name'}
                            type={'text'}
                            id={'name'}
                            name={'name'}
                            value={this.state.account.name}
                            placeholder={'Enter name here'}
                            error={this.state.errors.name}
                            required={true}
                            required_icon={true}
                        />

                        <Input
                            label={'Email'}
                            type={'email'}
                            id={'email'}
                            name={'email'}
                            value={this.state.account.email}
                            placeholder={'Enter email here'}
                            error={this.state.errors.email}
                            required={true}
                            required_icon={true}
                        />

                        <Input
                            label={'Contact Phone Number'}
                            type={'text'}
                            id={'phone'}
                            name={'phone'}
                            value={''}
                            placeholder={'Enter phone number here'}
                            error={this.state.errors.phone}
                            required={false}
                            required_icon={true}
                        />

                        <Input
                            label={'Question/Concern/Comment'}
                            type={'text'}
                            id={'message'}
                            name={'message'}
                            value={''}
                            placeholder={'Enter here'}
                            error={this.state.errors.message}
                            required={true}
                            required_icon={true}
                        />

                        <ButtonSubmit
                            name={'submit'}
                            id={'contact_us_submit'}
                            text={'Submit'}
                        />
                    </div>
                </div>
            </form>
            </div>
            </>
        )
    }
}
