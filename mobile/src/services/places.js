import db from "../istore";
import axios from './api_init';
import config from "../lib/config";
import moment from "moment";

export default {
    getById: async (id, refresh = false) => {
        let place = await db.places.get(parseInt(id));

        if (refresh || !place) {
            try {
                const url = `/api/v1/places/${id}`;
                const res = await axios.get(url);
                place = res.data;

                await db.places.put(place);
            } catch (error) {
                config.catch(error);
            }
        }

        return place;
    },

    getInformation: async (id) => {
        let list = [];
        const information = await db.place_information
            .where({placeId: parseInt(id)})
            .toArray();

        if (information.length > 0) {
            list = information;
        } else {
            try {
                const url = `/api/v1/places/${id}/information`;
                const res = await axios.get(url);
                list = res.data.list;

                await db.place_information.bulkPut(list);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getAttributes: async (id) => {
        let list = [];
        const attributes = await db.place_attributes
            .where({placeId: parseInt(id)})
            .toArray();

        if (attributes.length > 0) {
            list = attributes;
        } else {
            try {
                const url = `/api/v1/places/${id}/attributes`;
                const res = await axios.get(url);
                list = res.data.list;

                await db.place_attributes.bulkPut(list);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getReviews: async (id, refresh = false) => {
        let list = [];
        const reviews = await db.place_reviews
            .where({placeId: parseInt(id)})
            .toArray();

        if (!refresh && reviews.length > 0) {
            list = reviews;
        } else {
            try {
                const url = `/api/v1/places/${id}/reviews`;
                const res = await axios.get(url);
                list = res.data.list;

                await db.place_reviews.bulkPut(list);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getWorkingHours: async (id) => {
        let list = [];
        const hours = await db.place_working_hours.get(id);

        if (hours) {
            list = hours.list
        } else {
            try {
                const url = `/api/v1/places/${id}/working_hours`;
                const res = await axios.get(url);
                list = res.data.list;

                await db.place_working_hours.put({
                    id: id,
                    list: res.data.list
                });
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getRates: async (id, refresh = false) => {
        let list = [];
        const rates = await db.place_rates
            .where({placeId: parseInt(id)})
            .toArray();

        if (!refresh && rates.length > 0) {
            list = rates;
        } else {
            try {
                const url = `/api/v1/places/${id}/rates`;
                const res = await axios.get(url);
                list = res.data.list;

                await db.place_rates.bulkPut(list);
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getTypes: async (id, type) => {
        const placeTypesId = `${type}-${id}`;
        let list = [];
        const placeTypes = await db.place_types.get(placeTypesId);

        if (placeTypes) {
            list = placeTypes.list
        } else {
            try {
                const url = `/api/v1/getPlacesTypesWithQuantityByLocationId?locationId=${id}&offset=0&limit=50`;
                const res = await axios.get(url);
                list = res.data.list;

                await db.place_types.put({
                    id: placeTypesId,
                    object_id: id,
                    type: type,
                    list: res.data.list
                }, placeTypesId);

                await db.sync_cities.put({
                    id: id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }
        return list;
    }
};
