import moment from 'moment';
import db from "../istore";
import axios from "./api_init";
import config from "../lib/config";

export default {
    run: function () {
        setInterval(async () => {
            if (!localStorage.getItem('sync')) {
                const cities = await db.sync_cities.toArray();
                for (const city of cities) {
                    await this.syncCity(city.id);
                }
            }
        }, 120000);
    },

    loadCity: async function (id) {
        try {
            const res = await axios.get(`/api/v1/cities/${id}`);
            await db.cities.put(res.data);
        } catch (error) {
            config.catch(error);
        }
    },

    loadEscortTypes: async function (id) {
        let offset = 0;
        let limit = 100;

        try {
            let params = {};
            const updated = localStorage.getItem(`updated-${id}`);
            if (updated) {
                params.update = updated;
            }
            let url = `/api/v1/getEscortsTypesWithQuantityByLocationId?locationId=${id}&offset=${offset}&limit=${limit}`;
            let res = await axios.get(url, {params});
            const escortTypesId = `city-${id}`;
            await db.escort_types.put({
                id: escortTypesId,
                object_id: id,
                type: 'city',
                list: res.data.list
            }, escortTypesId);
            for (const escortType of res.data.list) {
                url = `/api/v1/cities/${id}/escort_types/${escortType.slug}/sponsors`;
                res = await axios.get(url);

                for (const sponsor of res.data.list) {
                    await db.sponsors.put(sponsor);
                }
            }
        } catch (error) {
            config.catch(error);
        }
    },

    loadPlaceTypes: async function (id) {
        let offset = 0;
        let limit = 100;

        try {
            let params = {};
            const updated = localStorage.getItem(`updated-${id}`);
            if (updated) {
                params.update = updated;
            }
            const url = `/api/v1/getPlacesTypesWithQuantityByLocationId?locationId=${id}&offset=${offset}&limit=${limit}`;
            const res = await axios.get(url, {params});
            const placeTypesId = `city-${id}`;
            await db.place_types.put({
                id: placeTypesId,
                object_id: id,
                type: 'city',
                list: res.data.list
            }, placeTypesId);
        } catch (error) {
            config.catch(error);
        }
    },

    loadSponsors: async function (id) {
        let offset = 0;
        let limit = 100;

        try {
            let params = {};
            const updated = localStorage.getItem(`updated-${id}`);
            if (updated) {
                params.update = updated;
            }
            let url = `/api/v1/getEscortSponsorsByLocationId?locationId=${id}&offset=${offset}&limit=${limit}`;
            let res = await axios.get(url, {params});

            for (const sponsor of res.data.list) {
                await db.sponsor_links.put(sponsor);
            }
        } catch (error) {
            config.catch(error);
        }
    },

    loadEscorts: async function (id) {
        let offset = 0;
        let limit = 100;

        const escortTypes = await db.escort_types.get(`city-${id}`);
        const cache = await caches.open('asng-images');

        for (const escortType of escortTypes.list) {
            while (true) {
                let params = {
                    locationId: id,
                    type: escortType.url,
                    offset: offset,
                    limit: limit,
                };
                console.log(`Escort type: ${escortType.url}`);
                const updated = localStorage.getItem(`updated-${id}`);
                if (updated) {
                    params.update = updated;
                }
                let url = '/api/v1/escorts';

                try {
                    console.time('background escorts');
                    const res = await axios.get(url, {params});
                    if (!res.data) {
                        break;
                    }
                    res.data.list.forEach(escort => {
                        const imagesCount = escort.images.length >= 3 ? 3 : escort.images.length;
                        for (let index = 0; index < imagesCount; index++) {
                            cache.add(`/images/escorts/${escort.images[index]}`).catch(error => {
                                if (error.response && error.response.status !== 404) {
                                    console.log(error);
                                }
                            });
                        }
                    });
                    //console.timeLog('background escorts', res);

                    await db.escorts.bulkPut(res.data.list);
                    let total = offset + limit;
                    if (total >= res.data.total) {
                        break;
                    }
                } catch (error) {
                    config.catch(error);

                    if (error.response) {
                        console.log(error.response.code);
                    }
                } finally {
                    console.timeEnd('background escorts');
                }

                console.log(`escorts offset: ${offset}`);
                offset += limit;
            }
        }
    },

    deleteEscorts: async function (id) {
        let offset = 0;
        let limit = 500;

        const escortTypes = await db.escort_types.get(`city-${id}`);
        for (const escortType of escortTypes.list) {
            while (true) {
                let params = {
                    locationId: id,
                    type: escortType.url,
                    offset: offset,
                    limit: limit,
                    deleted: true,
                    status: 'deleted'
                };
                const updated = localStorage.getItem(`updated-${id}`);
                if (updated) {
                    params.update = updated;
                }

                try {
                    let url = '/api/v1/escorts/ids';
                    const res = await axios.get(url, {params});

                    if (!res.data) {
                        break;
                    }
                    await db.escorts.bulkDelete(res.data.list);

                    const total = offset + limit;
                    if (total >= res.data.total) {
                        break;
                    }
                } catch (error) {
                    config.catch(error);
                }

                offset += limit;
            }
        }
    },

    loadPlaces: async function (id) {
        let offset = 0;
        let limit = 100;

        try {
            const placeTypes = await db.place_types.get(`city-${id}`);
            for (const placeType of placeTypes.list) {
                while (true) {
                    let params = {
                        locationId: id,
                        typeId: placeType.id,
                        offset: offset,
                        limit: limit,
                    };
                    const updated = localStorage.getItem(`updated-${id}`);
                    if (updated) {
                        params.update = updated;
                    }
                    let url = '/api/v1/places';
                    const res = await axios.get(url, {params});
                    await db.places.bulkPut(res.data.list);

                    const total = offset + limit;
                    if (!res.data) {
                        break;
                    }
                    if (total >= res.data.total) {
                        break;
                    }
                    offset += limit;
                }
            }

        } catch (error) {
            config.catch(error);
        }
    },

    deletePlaces: async function (id) {
        let offset = 0;
        let limit = 100;

        try {
            const placeTypes = await db.place_types.get(`city-${id}`);
            for (const placeType of placeTypes.list) {
                while (true) {
                    let params = {
                        locationId: id,
                        typeId: placeType.id,
                        offset: offset,
                        limit: limit,
                        status: 'deleted'
                    };
                    const updated = localStorage.getItem(`updated-${id}`);
                    if (updated) {
                        params.update = updated;
                    }
                    let url = '/api/v1/places';
                    const res = await axios.get(url, {params});
                    const ids = res.data.list.map(place => place.id);
                    await db.places.bulkDelete(ids);

                    const total = offset + limit;
                    if (!res.data) {
                        break;
                    }
                    if (total >= res.data.total) {
                        break;
                    }
                    offset += limit;
                }
            }

        } catch (error) {
            config.catch(error);
        }
    },

    loadForums: async function (id) {

    },

    syncCity: async function (id) {

        let label = 'CITY' + id;
        if (!config.isProd()) {
            console.log(label + ' START');
            console.time(label);
        }

        // Load city
        if (!config.isProd()) console.time('loadCity');
        const cityLoader = await this.loadCity(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('loadCity');

        // Load escort types
        if (!config.isProd()) console.time('loadEscortTypes');
        const escortTypesLoader = await this.loadEscortTypes(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('loadEscortTypes');

        // Load place types
        if (!config.isProd()) console.time('loadPlaceTypes');
        const placeTypesLoader = await this.loadPlaceTypes(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('loadPlaceTypes');

        // Load sponsors
        if (!config.isProd()) console.time('loadSponsors');
        const sponsorsLoader = await this.loadSponsors(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('loadSponsors');

        // Load forums
        if (!config.isProd()) console.time('loadForums');
        const forumsLoader = await this.loadForums(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('loadForums');

        //await Promise.all([cityLoader, escortTypesLoader, placeTypesLoader, sponsorsLoader, forumsLoader]);

        // Load places
        if (!config.isProd()) console.time('loadPlaces');
        const placesLoader = await this.loadPlaces(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('loadPlaces');

        // Load escorts
        if (!config.isProd()) console.time('loadEscorts');
        const escortsLoader = await this.loadEscorts(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('loadEscorts');

        // Delete places
        if (!config.isProd()) console.time('deletePlaces');
        const placesEraser = await this.deletePlaces(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('deletePlaces');

        // Delete escorts
        if (!config.isProd()) console.time('deleteEscorts');
        const escortsEraser = await this.deleteEscorts(id).catch(error => config.catch(error));
        if (!config.isProd()) console.timeEnd('deleteEscorts');

        await Promise.all([cityLoader, escortTypesLoader, placeTypesLoader, sponsorsLoader, forumsLoader, placesLoader, escortsLoader, placesEraser, escortsEraser]).then(
            values => {
                // Set last updated date
                localStorage.setItem(`updated-${id}`, moment().format('YYYY-MM-DD HH:mm:ss'));

                if (!config.isProd()) {
                    console.timeEnd(label);
                    console.log(label + ' END');
                }

                return true;
            }
        )

    }
}
