import db from "../istore";
import axios from './api_init';
import moment from "moment";
import config from "../lib/config";

export default {
    getForums: async (id, type = 1, refresh = false) => {
        let list = [];
        const forums = await db.forum_links.where({locationId: parseInt(id)}).toArray();

        if (refresh === false && forums.length > 0) {
            list = forums;
        } else {
            try {
                const url = `/api/v1/getForumsByLocationId?locationId=${id}&offset=0&limit=50&type=${type}`;
                const res = await axios.get(url);
                list = res.data.list;

                for (const forum of res.data.list) {
                    await db.forum_links.put(forum);
                }

                await db.sync_cities.put({
                    id: id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    }
};
