import db from "../istore";
import axios from './api_init';
import moment from "moment";
import config from "../lib/config";

export default {
    getLink: async (id) => {
        let link = await db.m4mlinks.get(parseInt(id));

        if (!link) {
            try {
                const url = `/api/v1/cities/${id}/m4mescorts`;
                const res = await axios.get(url);
                link = res.data;

                await db.m4mlinks.put(link);

                await db.sync_cities.put({
                    id: id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }

        return link;
    }
};
