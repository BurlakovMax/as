import axios from './api_init';
import config from "../lib/config";

export default {

    getCitiesByStateId: async (stateId) => {
        try {
            const res = await axios.get('/api/v1/popularCities?stateId='+stateId);

            let list = [];
            if (res.data) {
                // convert cities object to list, with key = city id
                Object.keys(res.data.list).map((item, key) =>
                  list[res.data.list[item].id] = res.data.list[item]
                )
            } else {
                console.log(res);
            }

            return list;
        } catch (error) {
            config.catch(error);
        }
    },

    getCountriesWithState: async (format) => {
        try {
            const res = await axios.get('/api/v1/countries_with_state');

            let list = [];
            if (res.data) {
                if (format === 'step2') {
                    // parse list from api
                    res.data.map(item => {
                        list.push({
                            'value': item.id,
                            'text': item.name,
                            'class': 'weight-600 color-black',
                            'disabled': (item.hasStates || item.subCountries.length),
                        })
                        if (item.hasStates) {
                            item.states.map(item =>
                              list.push({
                                  'value': item.id,
                                  'text': item.name,
                              })
                            )
                        }
                        if (item.subCountries.length) {
                            item.subCountries.map(item =>
                              list.push({
                                  'value': item.id,
                                  'text': item.name,
                              })
                            )
                        }
                        return null;
                    })
                } else {
                    list = res.data
                }
            } else {
                console.log(res);
            }

            return list;
        } catch (error) {
            config.catch(error);
        }
    }
};
