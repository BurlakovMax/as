import {setOffline} from "../store/common";

export default {
    checkOffline: () => {
        window.addEventListener('online',  () => {setOffline(false)});
        window.addEventListener('offline', () => {setOffline(true)});
    }
};
