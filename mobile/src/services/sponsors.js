import db from "../istore";
import axios from './api_init';
import moment from 'moment';
import config from "../lib/config";

export default {
    getPlace: async (id) => {
        let list = [];
        const sponsors = await db.sponsor_links
            .where('locationIds').equals(id)
            .toArray();

        if (sponsors.length > 0) {
            list = sponsors;
        } else {
            try {
                const res = await axios.get(`/api/v1/getEscortSponsorsByLocationId?locationId=${id}&offset=0&limit=50`);
                list = res.data.list;

                await db.sponsor_links.bulkPut(list);

                await db.sync_cities.put({
                    id: id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getEscort: async (cityId, escortType) => {
        let list = [];
        const sponsors = await db.sponsors
            .where({typeSlug: escortType})
            .toArray();

        if (sponsors.length > 0) {
            list = sponsors.filter(sponsor => {
                let index = sponsor.locations.findIndex(location => location.locationId === parseInt(cityId));
                return index > -1;
            });
        } else {
            try {
                const res = await axios.get(`/api/v1/cities/${cityId}/escort_types/${escortType}/sponsors`);
                list = res.data.list;

                await db.sponsors.bulkPut(list);

                await db.sync_cities.put({
                    id: cityId,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    }
};
