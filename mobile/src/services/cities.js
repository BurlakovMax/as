import db from "../istore";
import axios from './api_init';
import moment from "moment";
import auth from "../lib/auth";
import config from "../lib/config";

export default {
    getById: async (id) => {
        let city = await db.cities.get(parseInt(id));

        if (!city) {
            try {
                const url = `/api/v1/cities/${id}`;
                const res = await axios.get(url);
                city = res.data;
                await db.cities.put(city);

                await db.sync_cities.put({
                    id: id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }
        return city;
    },
    getByUrl: async (stateName, cityName) => {
        let city = await db.cities.where({url: `/${stateName}/${cityName}/`}).first();

        if (!city) {
            try {
                const url = `/api/v1/city_by_url/${stateName}/${cityName}`;
                const res = await axios.get(url);
                city = res.data;
                await db.cities.put(city);

                await db.sync_cities.put({
                    id: city.id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);

                // city not found, go home
                if (error.response && error.response.status === 404) {
                    window.location.href = auth.getLink('home');
                }
            }
        }
        return city;
    }
};
