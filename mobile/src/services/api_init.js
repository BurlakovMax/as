import axios from 'axios';
import auth from "../lib/auth";

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const token = auth.getToken();
if (token !== null) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

axios.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        let message = null;

        if (error.response && error.response.status === 401) {
            auth.clearToken();
            if (this) {
                this.props.history.push(auth.getLink('signin'));
            } else {
                window.location.href = auth.getLink('signin');
            }
        } else if (error.response && error.response.status === 403) {
            message = error.response.data.message || "Your access is restricted and does not allow this action.";
        } else if (error.response && error.response.status === 422) {
            message = error.response.data.errors;
        } else if (error.response && (error.response.status === 500 || error.response.status === 503)) {
            message = "Server error. Contact Technical Support.";
        }
        return Promise.reject(message || error);
    }
);

export default axios;
