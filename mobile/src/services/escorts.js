import db from "../istore";
import axios from './api_init';
import moment from "moment";
import config from "../lib/config";

export default {
    getById: async (id, refresh = false) => {
        let escort = await db.escorts.get(parseInt(id));

        if (refresh || !escort) {
            try {
                const url = `/api/v1/escorts/${id}`;
                const res = await axios.get(url);
                escort = res.data;

                await db.escorts.put(escort);
            } catch (error) {
                config.catch(error);
            }
        }

        return escort;
    },

    getAdvertising: async (cityId, escortType) => {
        let list = [];
        const advertising = await db.advertising.where({typeSlug: escortType}).toArray();
        if (advertising.length > 0) {
            list = advertising.filter(adv => {
                let index = adv.locations.findIndex(location => location.locationId === parseInt(cityId));
                return index > -1;
            });
        } else {
            try {
                const url = `/api/v1/cities/${cityId}/escort_types/${escortType}/advertising`;
                const res = await axios.get(url);
                list = res.data.list;
                for (const adv of res.data.list) {
                    await db.advertising.put(adv);
                }

                await db.sync_cities.put({
                    id: cityId,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }

        return list;
    },

    getTypeById: async (typeId) => {
        let type = {};

        try {
            const res = await axios.get('/api/v1/escorts/types');

            res.data.map(item =>
                {
                    let id = Object.keys(item)[0];
                    if (parseInt(typeId) === parseInt(id)) {
                        type = {
                            'id': id,
                            'name': item[id].name,
                            'slug': item[id].slug,
                        }
                    }
                    return null;
                }
            )
        } catch (error) {
            config.catch(error);
        }

        return type;
    },

    getTypes: async (id, type) => {
        const escortTypesId = `${type}-${id}`;
        let list = [];
        const escortTypes = await db.escort_types.get(escortTypesId);

        if (escortTypes) {
            list = escortTypes.list
        } else {
            try {
                const url = `/api/v1/getEscortsTypesWithQuantityByLocationId?locationId=${id}&offset=0&limit=50`;
                const res = await axios.get(url);
                list = res.data.list;

                await db.escort_types.put({
                    id: escortTypesId,
                    object_id: id,
                    type: type,
                    list: res.data.list
                }, escortTypesId);

                await db.sync_cities.put({
                    id: id,
                    updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
                });
            } catch (error) {
                config.catch(error);
            }
        }
        return list;
    }
};
