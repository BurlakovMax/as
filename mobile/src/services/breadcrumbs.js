import db from "../istore";
import axios from './api_init';
import config from "../lib/config";

export default {
    getBreadcrumbs: async (id, type = 'city') => {
        const breadcrumbsId = `${type}-${id}`;
        let links = [];
        const breadcrumbs = await db.breadcrumbs.get(breadcrumbsId);

        if (breadcrumbs) {
            links = breadcrumbs.links
        } else {
            try {
                const url = `/api/v1/getBreadcrumbCityLinks?locationId=${id}&type=${type}`;
                const res = await axios.get(url);
                links = res.data.breadcrumb_links;

                await db.breadcrumbs.put({
                    id: breadcrumbsId,
                    object_id: id,
                    type: type,
                    links: links
                }, breadcrumbsId);

            } catch (error) {
                config.catch(error);
            }
        }
        return links;
    },

    getCategoryBreadcrumbs: async (id, type) => {
        const breadcrumbsId = `${type}-${id}`;
        let links = [];
        const breadcrumbs = await db.breadcrumbs.get(breadcrumbsId);

        if (breadcrumbs) {
            links = breadcrumbs.links
        } else {
            try {
                const url = `/api/v1/getBreadcrumbCityLinks?locationId=${id}&type=category&category=${type}`;
                const res = await axios.get(url);
                links = res.data.list;

                await db.breadcrumbs.put({
                    id: breadcrumbsId,
                    object_id: id,
                    type: type,
                    links: links
                }, breadcrumbsId);

            } catch (error) {
                config.catch(error);
            }
        }
        return links;
    },

    getEscortBreadcrumbs: async (id, cityId) => {
        const breadcrumbsId = `${cityId}-${id}`;
        let links = [];
        const breadcrumbs = await db.breadcrumbs.get(breadcrumbsId);

        if (breadcrumbs) {
            links = breadcrumbs.links
        } else {
            try {
                const url = `/api/v1/escorts/${id}/breadcrumbs?locationId=${cityId}`;
                const res = await axios.get(url);
                links = res.data.links;

                await db.breadcrumbs.put({
                    id: breadcrumbsId,
                    object_id: id,
                    type: 'escort',
                    links: links
                }, breadcrumbsId);

            } catch (error) {
                config.catch(error);
            }
        }
        return links;
    },

    getPlaceBreadcrumbs: async (id) => {
        const breadcrumbsId = `place-${id}`;
        let links = [];
        const breadcrumbs = await db.breadcrumbs.get(breadcrumbsId);

        if (breadcrumbs) {
            links = breadcrumbs.links
        } else {
            try {
                const url = `/api/v1/places/${id}/breadcrumbs`;
                const res = await axios.get(url);
                links = res.data.links;

                await db.breadcrumbs.put({
                    id: breadcrumbsId,
                    object_id: id,
                    type: 'place',
                    links: links
                }, breadcrumbsId);

            } catch (error) {
                config.catch(error);
            }
        }
        return links;
    }
};
