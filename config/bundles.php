<?php

$bundles = [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    App\Contacts\config\ContactsBundle::class => ['all' => true],
    App\Escorts\config\EscortsBundle::class => ['all' => true],
    App\Places\config\PlacesBundle::class => ['all' => true],
    App\Payments\config\PaymentsBundle::class => ['all' => true],
    App\Forums\config\ForumsBundle::class => ['all' => true],
    App\Locations\config\LocationsBundle::class => ['all' => true],
    App\Security\config\SecurityServiceBundle::class => ['all' => true],
    App\SecurityServiceSymfonyBridge\config\SecurityServiceSymfonyBridgeBundle::class => ['all' => true],
    App\EmailGate\config\EmailGateBundle::class => ['all' => true],
    App\SmsGate\config\SmsGateBundle::class => ['all' => true],
    LazyLemurs\DomainEventsBundle\DomainEventsBundle::class => ['all' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    App\Frontend\config\FrontendBundle::class => ['all' => true],
    LazyLemurs\DoctrineOdmTypes\NormalizableTypesBundle::class => ['all' => true],
    App\Support\config\SupportBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Sentry\SentryBundle\SentryBundle::class => ['all' => true],
    Twig\Extra\TwigExtraBundle\TwigExtraBundle::class => ['all' => true],
    App\ActivityLog\config\ActivityLogBundle::class => ['all' => true],
    SymfonyBundles\JsonRequestBundle\SymfonyBundlesJsonRequestBundle::class => ['all' => true],
];

if (getenv('APP_ENV') !== 'prod') {
    $bundles = array_merge(
        $bundles,
        [
            Nelmio\ApiDocBundle\NelmioApiDocBundle::class => ['dev' => true],
            Nelmio\CorsBundle\NelmioCorsBundle::class => ['all' => true],
            Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true],
        ]
    );
}

return $bundles;
