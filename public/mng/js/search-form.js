const urlParams = new URLSearchParams(window.location.search);

if (urlParams.has('search')) {
    document.getElementById('appendedInputButton').value = urlParams.get('search');
}

function fuzzyFilter(id)
{
    if (urlParams.has(id)) {
        document.getElementById(id).value = urlParams.get(id);
    }
}

function ordering(property, ordering)
{
    if (urlParams.has('property')) {
        if (urlParams.get('property') === property) {
            ordering = urlParams.get('ordering');

            switch (ordering) {
                case 'desc':
                    ordering = 'asc'
                    break;
                case 'asc':
                    ordering = 'desc'
                    break;
            }
        }

        const parser = document.createElement('a');
        parser.href = location.href;
        return location.href = parser.pathname + '?property=' + property + '&ordering=' + ordering;
    }

    return location.href = location.href + '?property=' + property + '&ordering=' + ordering;
}

document.addEventListener('DOMContentLoaded', function(){
    if (urlParams.has('property') && urlParams.has('ordering')) {
        let element = document.getElementById(urlParams.get('property'));
        element.classList.add(urlParams.get('ordering'));
    }
});
