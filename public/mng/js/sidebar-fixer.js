class SidebarFixer {
  _getSidebarLinks(parent) {
    return Array.from(parent.querySelectorAll('.c-sidebar-nav-item a:not(.c-sidebar-nav-dropdown-toggle)'));
  }
  _fixEscortWaitingList() {
    const urlParams = new URLSearchParams(window.location.search);
    if (parseInt(urlParams.get('status')) === 3) {
      setTimeout(() => {
        $('#escort_list').removeClass('c-active');
        $('#escort_list_status').addClass('c-active');
      }, 300);
    }
  }
  _fixChosenWhileInDeep() {
    const url = window.location.href.replace(/^\//,"").split("?")[0]
      .replace('https:', 'http:');
    this._getSidebarLinks(document).forEach((sidebarLink) => {
      if (url.includes(sidebarLink.href)) {
        sidebarLink.classList.add("c-active");
      }
    });
    Array.from(document.querySelectorAll(".c-sidebar-nav-dropdown")).forEach((sidebarDropdown) => {
      this._getSidebarLinks(sidebarDropdown).forEach((sidebarLink) => {
        if (url.includes(sidebarLink.href)) {
          sidebarDropdown.classList.add("c-show");
        }
      })
    });
  }
  fix() {
    this._fixChosenWhileInDeep();
    this._fixEscortWaitingList();
  }
}

$(document).ready(() => {
  const sidebarFixer = new SidebarFixer();
  sidebarFixer.fix();
});
