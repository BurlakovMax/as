# Adult Search


[![pipeline status](https://gitlab.com/lazy-lemurs/as/as-backend/badges/develop/pipeline.svg)](https://gitlab.com/lazy-lemurs/as/as-backend/commits/develop)


#### Проверка кода на ошибки
Запуск консольной команды через docker

```bash
docker exec [название контейнера] vendor/bin/phpstan analyse [директория]
```
Например,
```bash
docker exec as-backend_web_1 vendor/bin/phpstan analyse src
```
Также можно указывать уровень валидации от 0 до 7 (0 - простой, 7 - сложный) через ключ -l.
Для указания самого сложного уровня можно использовать -l max.

Например,
```bash
docker exec as-backend_web_1 vendor/bin/phpstan analyse -l 4 src
```

#### API документация
Документация открывается по ссылке:

```http request
http://[адрес сервера]/api/doc
```

Например,
```http request
http://localhost:81/api/doc
```

#### Запуск очереди сообщений

```bash
docker exec [название контейнера] php bin/console messenger:consume async
```

Например,
```bash
docker exec as-backend_web_1 php bin/console messenger:consume async
```
